.class public LX/2L1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/ClassLoader;

.field private static final b:Ljava/lang/String;

.field private static volatile j:LX/2L1;


# instance fields
.field public final c:LX/2L2;

.field public final d:LX/0Sh;

.field private final e:LX/03V;

.field private final f:LX/0Zb;

.field private final g:LX/0lC;

.field private final h:LX/0SG;

.field private final i:LX/0WV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 394643
    const-class v0, LX/2L1;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    sput-object v0, LX/2L1;->a:Ljava/lang/ClassLoader;

    .line 394644
    const-class v0, LX/2L1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2L1;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2L2;LX/0Sh;LX/03V;LX/0Zb;LX/0lC;LX/0SG;LX/0WV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394635
    iput-object p1, p0, LX/2L1;->c:LX/2L2;

    .line 394636
    iput-object p2, p0, LX/2L1;->d:LX/0Sh;

    .line 394637
    iput-object p3, p0, LX/2L1;->e:LX/03V;

    .line 394638
    iput-object p4, p0, LX/2L1;->f:LX/0Zb;

    .line 394639
    iput-object p5, p0, LX/2L1;->g:LX/0lC;

    .line 394640
    iput-object p6, p0, LX/2L1;->h:LX/0SG;

    .line 394641
    iput-object p7, p0, LX/2L1;->i:LX/0WV;

    .line 394642
    return-void
.end method

.method public static a(LX/0QB;)LX/2L1;
    .locals 11

    .prologue
    .line 394621
    sget-object v0, LX/2L1;->j:LX/2L1;

    if-nez v0, :cond_1

    .line 394622
    const-class v1, LX/2L1;

    monitor-enter v1

    .line 394623
    :try_start_0
    sget-object v0, LX/2L1;->j:LX/2L1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394624
    if-eqz v2, :cond_0

    .line 394625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394626
    new-instance v3, LX/2L1;

    invoke-static {v0}, LX/2L2;->a(LX/0QB;)LX/2L2;

    move-result-object v4

    check-cast v4, LX/2L2;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v10

    check-cast v10, LX/0WV;

    invoke-direct/range {v3 .. v10}, LX/2L1;-><init>(LX/2L2;LX/0Sh;LX/03V;LX/0Zb;LX/0lC;LX/0SG;LX/0WV;)V

    .line 394627
    move-object v0, v3

    .line 394628
    sput-object v0, LX/2L1;->j:LX/2L1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394631
    :cond_1
    sget-object v0, LX/2L1;->j:LX/2L1;

    return-object v0

    .line 394632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIJJIILjava/util/List;Z)LX/3G3;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIIIJJII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "LX/3G3;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 394582
    const/4 v4, 0x0

    .line 394583
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p18, :cond_3

    .line 394584
    :cond_0
    move/from16 v0, p8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 394585
    move/from16 v0, p9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 394586
    move/from16 v0, p10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 394587
    :try_start_0
    iget-object v3, p0, LX/2L1;->g:LX/0lC;

    const-class v7, LX/0w7;

    invoke-virtual {v3, v2, v7}, LX/0lC;->a([BLjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0w7;

    .line 394588
    const/4 v3, 0x0

    .line 394589
    invoke-static {p6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 394590
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {p6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v3, v5, v7}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, LX/0jT;

    .line 394591
    const-string v5, "OfflineModeDbHandler"

    invoke-static {v3, v5}, LX/1k0;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 394592
    :cond_1
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 394593
    new-instance v6, Lcom/facebook/offlinemode/db/FlattenableTags;

    invoke-direct {v6}, Lcom/facebook/offlinemode/db/FlattenableTags;-><init>()V

    .line 394594
    invoke-static {v5}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v7

    invoke-virtual {v6, v5, v7}, Lcom/facebook/offlinemode/db/FlattenableTags;->a(Ljava/nio/ByteBuffer;I)V

    .line 394595
    new-instance v5, LX/3G1;

    invoke-direct {v5}, LX/3G1;-><init>()V

    invoke-static {p5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const-class v8, LX/0zP;

    invoke-virtual {v7, v8}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/3G1;->a(Ljava/lang/Class;)LX/3G1;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/3G1;->a(LX/0w7;)LX/3G1;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/3G1;->a(LX/0jT;)LX/3G1;

    move-result-object v2

    invoke-virtual {v6}, Lcom/facebook/offlinemode/db/FlattenableTags;->a()LX/0Rf;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3G1;->a(LX/0Rf;)LX/3G1;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 394596
    :goto_0
    if-nez v2, :cond_4

    .line 394597
    const/4 v2, 0x0

    .line 394598
    :cond_2
    :goto_1
    return-object v2

    .line 394599
    :catch_0
    move-exception v2

    .line 394600
    iget-object v3, p0, LX/2L1;->e:LX/03V;

    sget-object v5, LX/2L1;->b:Ljava/lang/String;

    const-string v6, "Error while trying to deserialize a PendingRequest - invalid param json"

    invoke-virtual {v3, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394601
    move-object/from16 v0, p17

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v4

    .line 394602
    goto :goto_0

    .line 394603
    :catch_1
    move-exception v2

    .line 394604
    iget-object v3, p0, LX/2L1;->e:LX/03V;

    sget-object v5, LX/2L1;->b:Ljava/lang/String;

    const-string v6, "Error while trying to deserialize a PendingRequest - invalid query class name"

    invoke-virtual {v3, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394605
    move-object/from16 v0, p17

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v4

    .line 394606
    goto :goto_0

    .line 394607
    :catch_2
    move-exception v2

    .line 394608
    move-object/from16 v0, p17

    invoke-static {p0, v2, p2, v0}, LX/2L1;->a(LX/2L1;Ljava/lang/Exception;Ljava/lang/String;Ljava/util/List;)V

    move-object v2, v4

    .line 394609
    goto :goto_0

    .line 394610
    :cond_3
    :try_start_1
    move/from16 v0, p7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, LX/2L1;->a([B)Landroid/os/Bundle;

    move-result-object v2

    .line 394611
    new-instance v3, LX/4gv;

    invoke-direct {v3}, LX/4gv;-><init>()V

    invoke-virtual {v3, p4}, LX/4gv;->c(Ljava/lang/String;)LX/4gv;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/4gv;->a(Landroid/os/Bundle;)LX/4gv;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    goto :goto_0

    .line 394612
    :catch_3
    move-exception v2

    .line 394613
    move-object/from16 v0, p17

    invoke-static {p0, v2, p2, v0}, LX/2L1;->a(LX/2L1;Ljava/lang/Exception;Ljava/lang/String;Ljava/util/List;)V

    move-object v2, v4

    goto :goto_0

    .line 394614
    :cond_4
    invoke-virtual {v2, p2}, LX/3G2;->a(Ljava/lang/String;)LX/3G2;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/3G2;->b(Ljava/lang/String;)LX/3G2;

    move-result-object v2

    move-wide/from16 v0, p11

    invoke-virtual {v2, v0, v1}, LX/3G2;->b(J)LX/3G2;

    move-result-object v2

    move-wide/from16 v0, p13

    invoke-virtual {v2, v0, v1}, LX/3G2;->a(J)LX/3G2;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/3G2;->a(I)LX/3G2;

    move-result-object v2

    move/from16 v0, p16

    invoke-virtual {v2, v0}, LX/3G2;->b(I)LX/3G2;

    move-result-object v2

    invoke-virtual {v2}, LX/3G2;->a()LX/3G3;

    move-result-object v2

    check-cast v2, LX/3G4;

    .line 394615
    if-eqz p18, :cond_2

    .line 394616
    :try_start_2
    move/from16 v0, p7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, LX/2L1;->a([B)Landroid/os/Bundle;

    move-result-object v3

    .line 394617
    new-instance v4, LX/4gv;

    invoke-direct {v4}, LX/4gv;-><init>()V

    invoke-virtual {v4, p4}, LX/4gv;->c(Ljava/lang/String;)LX/4gv;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/4gv;->a(Landroid/os/Bundle;)LX/4gv;

    move-result-object v3

    .line 394618
    invoke-virtual {v3, p2}, LX/3G2;->a(Ljava/lang/String;)LX/3G2;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/3G2;->b(Ljava/lang/String;)LX/3G2;

    move-result-object v3

    move-wide/from16 v0, p11

    invoke-virtual {v3, v0, v1}, LX/3G2;->b(J)LX/3G2;

    move-result-object v3

    move-wide/from16 v0, p13

    invoke-virtual {v3, v0, v1}, LX/3G2;->a(J)LX/3G2;

    move-result-object v3

    move/from16 v0, p15

    invoke-virtual {v3, v0}, LX/3G2;->a(I)LX/3G2;

    move-result-object v3

    move/from16 v0, p16

    invoke-virtual {v3, v0}, LX/3G2;->b(I)LX/3G2;

    move-result-object v3

    invoke-virtual {v3}, LX/3G2;->a()LX/3G3;

    move-result-object v3

    check-cast v3, LX/4gw;

    invoke-virtual {v3, v2}, LX/4gw;->a(LX/3G4;)LX/4gw;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v2

    goto/16 :goto_1

    .line 394619
    :catch_4
    move-exception v3

    .line 394620
    move-object/from16 v0, p17

    invoke-static {p0, v3, p2, v0}, LX/2L1;->a(LX/2L1;Ljava/lang/Exception;Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_1
.end method

.method private static a([B)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 394577
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 394578
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p0

    invoke-virtual {v1, p0, v0, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 394579
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 394580
    sget-object v0, LX/2L1;->a:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 394581
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method private static a(LX/2L1;Ljava/lang/Exception;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394574
    iget-object v0, p0, LX/2L1;->e:LX/03V;

    sget-object v1, LX/2L1;->b:Ljava/lang/String;

    const-string v2, "Error while trying to deserialize a PendingRequest"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 394575
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394576
    return-void
.end method

.method private a(LX/3G3;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 394545
    const/4 v0, 0x0

    .line 394546
    instance-of v1, p1, LX/4gw;

    if-eqz v1, :cond_3

    .line 394547
    check-cast p1, LX/4gw;

    .line 394548
    sget-object v1, LX/2L5;->c:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/4gw;->h:Ljava/lang/String;

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394549
    iget-object v1, p1, LX/4gw;->i:Landroid/os/Bundle;

    .line 394550
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 394551
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 394552
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    .line 394553
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 394554
    move-object v1, v3

    .line 394555
    sget-object v2, LX/2L5;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 394556
    iget-object v1, p1, LX/4gw;->j:LX/3G4;

    move-object v1, v1

    .line 394557
    if-eqz v1, :cond_0

    .line 394558
    iget-object v0, p1, LX/4gw;->j:LX/3G4;

    move-object v0, v0

    .line 394559
    :cond_0
    sget-object v1, LX/2L5;->p:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object p1, v0

    .line 394560
    :goto_0
    if-eqz p1, :cond_2

    .line 394561
    sget-object v0, LX/2L5;->e:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/3G4;->h:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394562
    :try_start_0
    iget-object v0, p0, LX/2L1;->g:LX/0lC;

    iget-object v1, p1, LX/3G4;->i:LX/0w7;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 394563
    sget-object v1, LX/2L5;->f:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394564
    iget-object v0, p1, LX/3G4;->j:LX/0jT;

    if-eqz v0, :cond_1

    .line 394565
    sget-object v0, LX/2L5;->g:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/3G4;->j:LX/0jT;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394566
    iget-object v0, p1, LX/3G4;->j:LX/0jT;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    .line 394567
    sget-object v1, LX/2L5;->h:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 394568
    :cond_1
    new-instance v0, Lcom/facebook/offlinemode/db/FlattenableTags;

    iget-object v1, p1, LX/3G4;->k:LX/0Rf;

    invoke-direct {v0, v1}, Lcom/facebook/offlinemode/db/FlattenableTags;-><init>(LX/0Rf;)V

    invoke-static {v0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    .line 394569
    sget-object v1, LX/2L5;->i:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 394570
    :cond_2
    return-void

    .line 394571
    :cond_3
    check-cast p1, LX/3G4;

    goto :goto_0

    .line 394572
    :catch_0
    move-exception v0

    .line 394573
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c(Ljava/lang/String;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 394533
    iget-object v0, p0, LX/2L1;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 394534
    sget-object v0, LX/2L5;->b:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 394535
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LX/2L5;->a:LX/0U1;

    .line 394536
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 394537
    aput-object v1, v2, v0

    .line 394538
    iget-object v0, p0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "pending_request"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 394539
    sget-object v0, LX/2L5;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 394540
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 394541
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 394542
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 394543
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 394544
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3G3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394473
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2L1;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 394474
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 394475
    const-string v3, "pending_request"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 394476
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, LX/2L4;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, LX/2L5;->j:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 394477
    sget-object v2, LX/2L5;->a:LX/0U1;

    invoke-virtual {v2, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 394478
    sget-object v3, LX/2L5;->c:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v22

    .line 394479
    sget-object v3, LX/2L5;->e:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v23

    .line 394480
    sget-object v3, LX/2L5;->d:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 394481
    sget-object v3, LX/2L5;->f:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 394482
    sget-object v3, LX/2L5;->g:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v24

    .line 394483
    sget-object v3, LX/2L5;->h:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 394484
    sget-object v3, LX/2L5;->i:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 394485
    sget-object v3, LX/2L5;->j:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v25

    .line 394486
    sget-object v3, LX/2L5;->k:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v26

    .line 394487
    sget-object v3, LX/2L5;->l:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v27

    .line 394488
    sget-object v3, LX/2L5;->m:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v28

    .line 394489
    sget-object v3, LX/2L5;->n:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v29

    .line 394490
    sget-object v3, LX/2L5;->o:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v30

    .line 394491
    sget-object v3, LX/2L5;->b:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v31

    .line 394492
    sget-object v3, LX/2L5;->p:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v32

    .line 394493
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v33

    .line 394494
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v20

    .line 394495
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 394496
    move/from16 v0, v29

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 394497
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 394498
    move/from16 v0, v22

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 394499
    move/from16 v0, v23

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 394500
    move/from16 v0, v24

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 394501
    move/from16 v0, v30

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 394502
    move/from16 v0, v31

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 394503
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    sget-object v15, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 394504
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "offline_mode_operation_dropped_new_build_detected"

    invoke-direct {v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "offline"

    invoke-virtual {v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v8, "request_id"

    invoke-virtual {v6, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v8, "operation_type"

    invoke-virtual {v6, v8, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "old_android_build_fingerprint"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v6, "android_build_fingerprint"

    sget-object v7, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 394505
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2L1;->f:LX/0Zb;

    invoke-interface {v6, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 394506
    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 394507
    :catch_0
    move-exception v2

    .line 394508
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2L1;->e:LX/03V;

    sget-object v5, LX/2L1;->b:Ljava/lang/String;

    const-string v6, "Could not construct pending request."

    invoke-virtual {v3, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394509
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 394510
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 394511
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2L1;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 394512
    :cond_1
    :try_start_2
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2L1;->i:LX/0WV;

    invoke-virtual {v3}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 394513
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "offline_mode_operation_dropped_new_app_version_detected"

    invoke-direct {v3, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "offline"

    invoke-virtual {v3, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v6, "request_id"

    invoke-virtual {v3, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v6, "mutation_query_class"

    invoke-virtual {v3, v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v6, "old_app_version"

    invoke-virtual {v3, v6, v14}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v6, "app_version"

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2L1;->i:LX/0WV;

    invoke-virtual {v7}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 394514
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2L1;->f:LX/0Zb;

    invoke-interface {v6, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 394515
    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 394516
    :catchall_0
    move-exception v2

    move-object v3, v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 394517
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 394518
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2L1;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 394519
    :cond_2
    :try_start_3
    move/from16 v0, v26

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 394520
    move/from16 v0, v25

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 394521
    move/from16 v0, v27

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 394522
    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 394523
    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_3

    const/16 v21, 0x1

    :goto_3
    move-object/from16 v3, p0

    .line 394524
    invoke-direct/range {v3 .. v21}, LX/2L1;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIJJIILjava/util/List;Z)LX/3G3;

    move-result-object v3

    .line 394525
    if-eqz v3, :cond_0

    .line 394526
    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 394527
    :cond_3
    const/16 v21, 0x0

    goto :goto_3

    .line 394528
    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 394529
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 394530
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2L1;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 394531
    :cond_5
    throw v3

    .line 394532
    :cond_6
    invoke-virtual/range {v33 .. v33}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    return-object v2
.end method

.method public final declared-synchronized a(LX/3G3;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3G3;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394429
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2L1;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 394430
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 394431
    iget-wide v0, p1, LX/3G3;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, LX/3G3;->d:J

    .line 394432
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 394433
    sget-object v3, LX/2L5;->a:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394434
    sget-object v3, LX/2L5;->j:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 394435
    sget-object v0, LX/2L5;->k:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p1, LX/3G3;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 394436
    sget-object v0, LX/2L5;->l:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, LX/3G3;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394437
    sget-object v0, LX/2L5;->m:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, LX/3G3;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394438
    sget-object v0, LX/2L5;->n:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394439
    sget-object v0, LX/2L5;->o:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/2L1;->i:LX/0WV;

    invoke-virtual {v1}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394440
    invoke-direct {p0, p1, v2}, LX/2L1;->a(LX/3G3;Landroid/content/ContentValues;)V

    .line 394441
    iget-object v0, p0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 394442
    const v0, 0x7e2ec738

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394443
    :try_start_1
    iget-object v0, p1, LX/3G3;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 394444
    sget-object v0, LX/2L5;->b:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p1, LX/3G3;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394445
    sget-object v0, LX/2L5;->a:LX/0U1;

    iget-object v3, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 394446
    iget-object v3, p0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "pending_request"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v2, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 394447
    if-eqz v0, :cond_1

    .line 394448
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 394449
    const v2, 0x55f3c545

    :try_start_2
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 394450
    :goto_1
    monitor-exit p0

    return-object v0

    .line 394451
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/2L1;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v0

    goto/16 :goto_0

    .line 394452
    :cond_1
    :try_start_4
    iget-object v0, p1, LX/3G3;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/2L1;->c(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 394453
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 394454
    iget-object v2, p1, LX/3G3;->c:Ljava/lang/String;

    .line 394455
    iget-object v3, p0, LX/2L1;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 394456
    sget-object v3, LX/2L5;->b:LX/0U1;

    invoke-virtual {v3, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 394457
    iget-object v4, p0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "pending_request"

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394458
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    iget-object v2, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 394459
    const v2, -0x55be476f

    :try_start_5
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 394460
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 394461
    :cond_2
    :try_start_6
    const-string v0, "pending_request"

    const-string v3, ""

    const v4, 0x41213013

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x38a3058f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 394462
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 394463
    const v0, -0x4a643dd2

    :try_start_7
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 394464
    :goto_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 394465
    goto :goto_1

    .line 394466
    :catch_0
    move-exception v0

    .line 394467
    :try_start_8
    sget-object v2, LX/2L1;->b:Ljava/lang/String;

    const-string v3, "Unable to update offline db"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 394468
    const v0, -0x5b5410bd

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_2

    :catchall_1
    move-exception v0

    const v2, -0x1930d5b2

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 394469
    iget-object v0, p0, LX/2L1;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 394470
    sget-object v0, LX/2L5;->a:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 394471
    iget-object v1, p0, LX/2L1;->c:LX/2L2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "pending_request"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394472
    return-void
.end method
