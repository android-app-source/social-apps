.class public LX/231;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/232;
.implements LX/0tW;
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/232;",
        "LX/0tW;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

.field public b:Lcom/facebook/api/feed/FetchFeedParams;

.field public final c:LX/230;

.field private final d:LX/0tX;

.field private final e:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/233;

.field private i:Z

.field private j:Z

.field private k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/concurrent/ExecutorService;

.field public final m:LX/0tn;

.field public final n:LX/0ad;

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Z

.field private final s:LX/0rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/230;LX/0tX;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/233;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/0tn;LX/0ad;)V
    .locals 1
    .param p1    # LX/230;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsFlatBufferFromServerEnabled;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/FeedFetchExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/230;",
            "LX/0tX;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;",
            "LX/233;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tn;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 361538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361539
    iput-boolean v0, p0, LX/231;->i:Z

    .line 361540
    iput-boolean v0, p0, LX/231;->j:Z

    .line 361541
    iput-boolean v0, p0, LX/231;->o:Z

    .line 361542
    iput v0, p0, LX/231;->q:I

    .line 361543
    iput-boolean v0, p0, LX/231;->r:Z

    .line 361544
    new-instance v0, LX/235;

    invoke-direct {v0, p0}, LX/235;-><init>(LX/231;)V

    iput-object v0, p0, LX/231;->s:LX/0rl;

    .line 361545
    iput-object p1, p0, LX/231;->c:LX/230;

    .line 361546
    iput-object p2, p0, LX/231;->d:LX/0tX;

    .line 361547
    iput-object p3, p0, LX/231;->e:Landroid/os/Handler;

    .line 361548
    iput-object p4, p0, LX/231;->f:LX/0Ot;

    .line 361549
    iput-object p5, p0, LX/231;->g:LX/0Ot;

    .line 361550
    iput-object p6, p0, LX/231;->h:LX/233;

    .line 361551
    iput-object p7, p0, LX/231;->k:LX/0Or;

    .line 361552
    iput-object p8, p0, LX/231;->l:Ljava/util/concurrent/ExecutorService;

    .line 361553
    invoke-static {}, LX/231;->b()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    iput-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 361554
    iput-object p9, p0, LX/231;->m:LX/0tn;

    .line 361555
    iput-object p10, p0, LX/231;->n:LX/0ad;

    .line 361556
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 10

    .prologue
    .line 361557
    iget-boolean v0, p0, LX/231;->o:Z

    if-nez v0, :cond_1

    .line 361558
    sget-object v7, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    .line 361559
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361560
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/231;->o:Z

    .line 361561
    :cond_0
    :goto_0
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/231;->c:LX/230;

    .line 361562
    iget-wide v8, v2, LX/230;->e:J

    move-wide v4, v8

    .line 361563
    iget-boolean v6, p0, LX/231;->p:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZLX/0qw;)V

    .line 361564
    iget-object v1, p0, LX/231;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0pm;

    invoke-virtual {v1, v0}, LX/0pm;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 361565
    iget-object v0, p0, LX/231;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/187;

    invoke-virtual {v0, v1}, LX/187;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    .line 361566
    return-object v1

    .line 361567
    :cond_1
    sget-object v7, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    goto :goto_0
.end method

.method private static b()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 2

    .prologue
    .line 361568
    new-instance v0, LX/0uq;

    invoke-direct {v0}, LX/0uq;-><init>()V

    .line 361569
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 361570
    iput-object v1, v0, LX/0uq;->d:LX/0Px;

    .line 361571
    move-object v0, v0

    .line 361572
    new-instance v1, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    .line 361573
    iput-object v1, v0, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 361574
    move-object v0, v0

    .line 361575
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V
    .locals 3

    .prologue
    .line 361576
    invoke-direct {p0, p1}, LX/231;->b(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 361577
    iget-object v1, p0, LX/231;->c:LX/230;

    .line 361578
    iget-object v2, v1, LX/230;->f:LX/22z;

    move-object v1, v2

    .line 361579
    invoke-virtual {v1}, LX/22z;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 361580
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 361581
    iget-object v1, p0, LX/231;->c:LX/230;

    const/4 v2, 0x1

    .line 361582
    iput-boolean v2, v1, LX/230;->g:Z

    .line 361583
    :cond_0
    iget-object v1, p0, LX/231;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$4;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$4;-><init>(LX/231;Lcom/facebook/api/feed/FetchFeedResult;)V

    const v0, -0x1972be31

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 361584
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0rl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361585
    const-string v0, "feed_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361586
    :goto_0
    return-object p0

    .line 361587
    :cond_0
    const-string v0, "feedback_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361588
    iget-object p0, p0, LX/231;->s:LX/0rl;

    goto :goto_0

    .line 361589
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized GraphQL Subscriber:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 361590
    const-string v0, "GraphQLQueryExecutorFeedFetch.runBackendFetch"

    const v1, -0x2f14564d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361591
    :try_start_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v0, v0

    .line 361592
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 361593
    const v0, 0x35cc491

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-object v3

    .line 361594
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361595
    iget-object v1, v0, LX/230;->c:LX/0rn;

    move-object v0, v1

    .line 361596
    iget-object v1, p0, LX/231;->c:LX/230;

    .line 361597
    iget-object v2, v1, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v2

    .line 361598
    invoke-virtual {v0, v1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 361599
    iput-object p1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361600
    iget-object v0, p0, LX/231;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0rU;->TAIL:LX/0rU;

    .line 361601
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v1

    .line 361602
    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/231;->j:Z

    .line 361603
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361604
    iget-object v1, v0, LX/230;->c:LX/0rn;

    move-object v0, v1

    .line 361605
    iget-object v1, p0, LX/231;->c:LX/230;

    .line 361606
    iget-object v2, v1, LX/230;->d:Ljava/lang/String;

    move-object v1, v2

    .line 361607
    invoke-virtual {v0, p1, v1, p0}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;

    move-result-object v0

    .line 361608
    iget-boolean v1, v0, LX/0v6;->h:Z

    move v1, v1

    .line 361609
    iput-boolean v1, p0, LX/231;->p:Z

    .line 361610
    iget-object v1, p0, LX/231;->h:LX/233;

    .line 361611
    iget-wide v4, v1, LX/233;->e:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, LX/03g;->b(Z)V

    .line 361612
    iget-object v4, v1, LX/233;->a:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v1, LX/233;->e:J

    .line 361613
    iget-object v4, v1, LX/233;->b:LX/0Zb;

    const-string v5, "android_feed_streaming_reliability"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 361614
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 361615
    const-string v5, "event"

    const-string v6, "start"

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361616
    const-string v5, "fetch_type"

    .line 361617
    iget-object v6, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v6, v6

    .line 361618
    invoke-virtual {v6}, LX/0rU;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361619
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 361620
    :cond_1
    invoke-static {}, LX/231;->b()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v1

    iput-object v1, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 361621
    iget-object v1, p0, LX/231;->d:LX/0tX;

    iget-object v2, p0, LX/231;->l:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1, v0, v2}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361622
    const v0, -0xfb53f4e

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 361623
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 361624
    :catchall_0
    move-exception v0

    const v1, 0x5d1a043a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 361625
    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a()V
    .locals 10

    .prologue
    .line 361626
    const-string v0, "GraphQLQueryExecutorFeedFetch.onCompleted"

    const v1, 0x7f00b81a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361627
    :try_start_0
    iget-boolean v0, p0, LX/231;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 361628
    :cond_0
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-direct {p0, v0}, LX/231;->c(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V

    .line 361629
    :cond_1
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361630
    iget-object v1, v0, LX/230;->f:LX/22z;

    move-object v0, v1

    .line 361631
    invoke-virtual {v0}, LX/22z;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 361632
    iget-object v0, p0, LX/231;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$2;

    invoke-direct {v1, p0}, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$2;-><init>(LX/231;)V

    const v2, 0x18800b3f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 361633
    :cond_2
    iget-object v0, p0, LX/231;->h:LX/233;

    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    iget v2, p0, LX/231;->q:I

    iget-boolean v3, p0, LX/231;->r:Z

    const/4 v5, 0x1

    .line 361634
    iget-wide v6, v0, LX/233;->e:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-eqz v4, :cond_3

    move v4, v5

    :goto_0
    invoke-static {v4}, LX/03g;->b(Z)V

    .line 361635
    invoke-static {v0}, LX/233;->b(LX/233;)I

    move-result v4

    invoke-static {v0, v4, v5}, LX/233;->a(LX/233;IZ)V

    .line 361636
    const-string v4, "complete"

    invoke-static {v0, v4, v1, v2, v3}, LX/233;->a(LX/233;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;IZ)V

    .line 361637
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361638
    iget-object v1, v0, LX/230;->c:LX/0rn;

    move-object v0, v1

    .line 361639
    iget-object v1, p0, LX/231;->c:LX/230;

    .line 361640
    iget-object v2, v1, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v2

    .line 361641
    invoke-virtual {v0, v1}, LX/0rn;->b(Lcom/facebook/api/feed/FetchFeedParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361642
    const v0, 0x7b4857e2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361643
    return-void

    .line 361644
    :catchall_0
    move-exception v0

    const v1, 0x72bada13

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 361645
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 361646
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 361647
    const-string v0, "GraphQLQueryExecutorFeedFetch.onNext"

    const v1, 0x321598d7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361648
    :try_start_0
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361649
    iget-object v1, v0, LX/230;->c:LX/0rn;

    move-object v0, v1

    .line 361650
    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v1, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 361651
    if-nez v0, :cond_7

    .line 361652
    iget-object v0, p0, LX/231;->h:LX/233;

    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361653
    iget-object v2, v0, LX/233;->b:LX/0Zb;

    const-string v3, "android_feed_streaming_reliability"

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 361654
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 361655
    const-string v3, "event"

    const-string p1, "null_homestories"

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361656
    const-string v3, "fetch_type"

    .line 361657
    iget-object p1, v1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object p1, p1

    .line 361658
    invoke-virtual {p1}, LX/0rU;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361659
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 361660
    :cond_0
    sget-object v0, LX/0rU;->HEAD:LX/0rU;

    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361661
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v2

    .line 361662
    if-ne v0, v1, :cond_1

    .line 361663
    invoke-static {}, LX/231;->b()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    move-object v1, v0

    .line 361664
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    .line 361665
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    .line 361666
    iget-object v3, p0, LX/231;->h:LX/233;

    .line 361667
    iget-wide v4, v3, LX/233;->e:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, LX/03g;->b(Z)V

    .line 361668
    iget-object v4, v3, LX/233;->d:Landroid/util/SparseIntArray;

    iget-object v5, v3, LX/233;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    invoke-static {v3}, LX/233;->b(LX/233;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/util/SparseIntArray;->append(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361669
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 361670
    :cond_1
    const v0, -0xdd18aad

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361671
    :goto_3
    return-void

    .line 361672
    :cond_2
    :try_start_1
    iget v0, p0, LX/231;->q:I

    add-int/2addr v0, v2

    iput v0, p0, LX/231;->q:I

    .line 361673
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 361674
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/231;->r:Z

    .line 361675
    :cond_3
    iget-boolean v0, p0, LX/231;->j:Z

    if-nez v0, :cond_5

    if-lez v2, :cond_5

    .line 361676
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 361677
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 361678
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v2}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 361679
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/231;->j:Z

    .line 361680
    :cond_5
    iget-boolean v0, p0, LX/231;->j:Z

    if-eqz v0, :cond_6

    .line 361681
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 361682
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 361683
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 361684
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-nez v0, :cond_9

    .line 361685
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 361686
    :goto_4
    new-instance v3, LX/0uq;

    invoke-direct {v3}, LX/0uq;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 361687
    iput-object v2, v3, LX/0uq;->d:LX/0Px;

    .line 361688
    move-object v2, v3

    .line 361689
    iput-object v0, v2, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 361690
    move-object v0, v2

    .line 361691
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v2

    .line 361692
    iput-object v2, v0, LX/0uq;->c:Ljava/lang/String;

    .line 361693
    move-object v0, v0

    .line 361694
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m()Z

    move-result v2

    .line 361695
    iput-boolean v2, v0, LX/0uq;->f:Z

    .line 361696
    move-object v0, v0

    .line 361697
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    iput-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361698
    :goto_5
    const v0, 0x2e818430

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_3

    .line 361699
    :cond_6
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/231;->i:Z

    .line 361700
    invoke-direct {p0, v1}, LX/231;->c(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 361701
    :catchall_0
    move-exception v0

    const v1, 0x9d98f63

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_7
    move-object v1, v0

    goto/16 :goto_0

    .line 361702
    :cond_8
    :try_start_3
    const/4 v4, 0x0

    goto/16 :goto_2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361703
    :cond_9
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    .line 361704
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_4

    .line 361705
    :cond_a
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-static {v0}, LX/17L;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v3

    .line 361706
    iput-boolean v3, v0, LX/17L;->d:Z

    .line 361707
    move-object v0, v0

    .line 361708
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 361709
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v3

    .line 361710
    iput-object v3, v0, LX/17L;->c:Ljava/lang/String;

    .line 361711
    :cond_b
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_4
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 361712
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361713
    iget-object v1, v0, LX/230;->f:LX/22z;

    move-object v0, v1

    .line 361714
    invoke-virtual {v0}, LX/22z;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 361715
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 361716
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-direct {p0, v0}, LX/231;->c(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V

    .line 361717
    :cond_0
    iget-object v0, p0, LX/231;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/data/GraphQLQueryExecutorFeedFetch$3;-><init>(LX/231;Ljava/lang/Throwable;)V

    const v2, -0x5da069bd

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 361718
    :cond_1
    :goto_0
    iget-object v0, p0, LX/231;->h:LX/233;

    iget-object v1, p0, LX/231;->b:Lcom/facebook/api/feed/FetchFeedParams;

    iget v2, p0, LX/231;->q:I

    iget-boolean v3, p0, LX/231;->r:Z

    const/4 v5, 0x0

    .line 361719
    iget-wide v6, v0, LX/233;->e:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/03g;->b(Z)V

    .line 361720
    invoke-static {v0}, LX/233;->b(LX/233;)I

    move-result v4

    invoke-static {v0, v4, v5}, LX/233;->a(LX/233;IZ)V

    .line 361721
    const-string v4, "error"

    invoke-static {v0, v4, v1, v2, v3}, LX/233;->a(LX/233;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;IZ)V

    .line 361722
    iget-object v0, p0, LX/231;->c:LX/230;

    .line 361723
    iget-object v1, v0, LX/230;->c:LX/0rn;

    move-object v0, v1

    .line 361724
    iget-object v1, p0, LX/231;->c:LX/230;

    .line 361725
    iget-object v2, v1, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v2

    .line 361726
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1, v2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    .line 361727
    return-void

    .line 361728
    :cond_2
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 361729
    iget-object v0, p0, LX/231;->a:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-direct {p0, v0}, LX/231;->b(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)Lcom/facebook/api/feed/FetchFeedResult;

    goto :goto_0

    :cond_3
    move v4, v5

    .line 361730
    goto :goto_1
.end method
