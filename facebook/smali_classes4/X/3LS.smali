.class public LX/3LS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Kv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3Kv;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550556
    iput-object p1, p0, LX/3LS;->a:LX/0Or;

    .line 550557
    iput-object p2, p0, LX/3LS;->b:LX/0Or;

    .line 550558
    return-void
.end method

.method public static a(LX/0QB;)LX/3LS;
    .locals 3

    .prologue
    .line 550559
    new-instance v0, LX/3LS;

    const/16 v1, 0xd0c

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x12cb

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/3LS;-><init>(LX/0Or;LX/0Or;)V

    .line 550560
    move-object v0, v0

    .line 550561
    return-object v0
.end method
