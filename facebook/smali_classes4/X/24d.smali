.class public LX/24d;
.super Landroid/widget/RelativeLayout;
.source ""

# interfaces
.implements LX/24e;


# instance fields
.field public a:LX/24f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

.field public c:Lcom/facebook/attachments/photos/ui/PostPostBadge;

.field public d:LX/1eo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 367637
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 367638
    const-class v0, LX/24d;

    invoke-static {v0, p0}, LX/24d;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 367639
    const v0, 0x7f0d008a

    invoke-virtual {p0, v0}, LX/24d;->setId(I)V

    .line 367640
    invoke-virtual {p0}, LX/24d;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f38

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 367641
    const v0, 0x7f0d24e0

    invoke-virtual {p0, v0}, LX/24d;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    iput-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    .line 367642
    const v0, 0x7f0d24e1

    invoke-virtual {p0, v0}, LX/24d;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/photos/ui/PostPostBadge;

    iput-object v0, p0, LX/24d;->c:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    .line 367643
    iget-object v0, p0, LX/24d;->a:LX/24f;

    iget-object v1, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    iget-object p1, p0, LX/24d;->c:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {v0, p0, v1, p1}, LX/24f;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/attachments/photos/ui/PostPostBadge;)LX/1eo;

    move-result-object v0

    iput-object v0, p0, LX/24d;->d:LX/1eo;

    .line 367644
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/24d;

    const-class p0, LX/24f;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/24f;

    iput-object v1, p1, LX/24d;->a:LX/24f;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 367635
    iget-object v0, p0, LX/24d;->d:LX/1eo;

    invoke-virtual {v0, p1, p2}, LX/1eo;->a(II)V

    .line 367636
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 367633
    iget-object v0, p0, LX/24d;->d:LX/1eo;

    invoke-virtual {v0, p1, p2}, LX/1eo;->a(Ljava/lang/String;I)V

    .line 367634
    return-void
.end method

.method public getPhotoAttachmentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 367632
    iget-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    return-object v0
.end method

.method public getPostPostBadge()Lcom/facebook/attachments/photos/ui/PostPostBadge;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 367631
    iget-object v0, p0, LX/24d;->c:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    return-object v0
.end method

.method public getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;
    .locals 1

    .prologue
    .line 367617
    iget-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    return-object v0
.end method

.method public setActualImageFocusPoint(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 367629
    iget-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 367630
    return-void
.end method

.method public setController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 367627
    iget-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 367628
    return-void
.end method

.method public setOnBadgeClickListener(LX/24m;)V
    .locals 1
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 367624
    iget-object v0, p0, LX/24d;->d:LX/1eo;

    .line 367625
    iput-object p1, v0, LX/1eo;->f:LX/24m;

    .line 367626
    return-void
.end method

.method public setOnPhotoClickListener(LX/24m;)V
    .locals 1
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 367621
    iget-object v0, p0, LX/24d;->d:LX/1eo;

    .line 367622
    iput-object p1, v0, LX/1eo;->e:LX/24m;

    .line 367623
    return-void
.end method

.method public setPairedVideoUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 367620
    return-void
.end method

.method public setPhotoBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 367618
    iget-object v0, p0, LX/24d;->b:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 367619
    return-void
.end method
