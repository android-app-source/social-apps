.class public LX/25l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369937
    return-void
.end method

.method public static a([BII)Ljava/lang/String;
    .locals 2

    .prologue
    .line 369938
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, p1, p2, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 369939
    :catch_0
    move-exception v0

    .line 369940
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/io/InputStream;[BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 369941
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 369942
    array-length v1, p1

    if-eq v0, v1, :cond_0

    .line 369943
    new-instance v1, LX/K1p;

    array-length v2, p1

    invoke-direct {v1, p2, v2, v0}, LX/K1p;-><init>(Ljava/lang/String;II)V

    throw v1

    .line 369944
    :cond_0
    return-void
.end method
