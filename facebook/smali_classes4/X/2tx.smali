.class public final LX/2tx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2vL;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2vL",
        "<",
        "LX/2ty;",
        ">;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private a:LX/2ty;

.field private final b:LX/2kD;

.field private final c:LX/0SG;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JT;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;LX/2kD;LX/0SG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;",
            "LX/2kD;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 475361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2tx;->d:Ljava/util/Map;

    .line 475363
    new-instance v0, LX/2ty;

    invoke-direct {v0, p1}, LX/2ty;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, LX/2tx;->a:LX/2ty;

    .line 475364
    iput-object p2, p0, LX/2tx;->b:LX/2kD;

    .line 475365
    iput-object p3, p0, LX/2tx;->c:LX/0SG;

    .line 475366
    return-void
.end method

.method private a(Ljava/lang/String;LX/9JT;)V
    .locals 2

    .prologue
    .line 475302
    iget-object v0, p0, LX/2tx;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 475303
    if-nez v0, :cond_0

    .line 475304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 475305
    iget-object v1, p0, LX/2tx;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475306
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475307
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 475367
    check-cast p1, LX/2ty;

    .line 475368
    iget-object v0, p1, LX/2ty;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v0, v0

    .line 475369
    return v0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    .line 475370
    const/4 v2, 0x0

    .line 475371
    invoke-static {p2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 475372
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 475373
    const-string v2, "SimpleWriterStore.reflattenMutableFlatbuffer"

    invoke-virtual {v0, v2, p2}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 475374
    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 475375
    iget-object v0, p0, LX/2tx;->a:LX/2ty;

    return-object v0
.end method

.method public final a([J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 475376
    iget-object v0, p0, LX/2tx;->a:LX/2ty;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 475416
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 475377
    check-cast p1, LX/2ty;

    .line 475378
    iput p2, p1, LX/2ty;->a:I

    .line 475379
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 475380
    check-cast p1, LX/2ty;

    .line 475381
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 475382
    if-nez v0, :cond_0

    .line 475383
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "updateRowBaseBuffer called with null buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475384
    :cond_0
    iget-object v1, p0, LX/2tx;->b:LX/2kD;

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {p3}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v3

    iget-object v0, p0, LX/2tx;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/2kD;->b(Ljava/nio/ByteBuffer;LX/0w5;JZ)I

    move-result v0

    .line 475385
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v1

    .line 475386
    const-string v2, "confirmed"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v2

    .line 475387
    iget v3, v2, LX/9JQ;->d:I

    move v2, v3

    .line 475388
    invoke-static {v2, v0, v1}, LX/9JT;->a(III)LX/9JT;

    move-result-object v0

    .line 475389
    :goto_0
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v1

    .line 475390
    iget-object v2, v1, LX/9JQ;->c:Ljava/lang/String;

    move-object v1, v2

    .line 475391
    invoke-direct {p0, v1, v0}, LX/2tx;->a(Ljava/lang/String;LX/9JT;)V

    .line 475392
    return-void

    .line 475393
    :cond_1
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v2

    .line 475394
    iget v3, v2, LX/9JQ;->d:I

    move v2, v3

    .line 475395
    invoke-static {v2, v0, v1}, LX/9JT;->b(III)LX/9JT;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/Set;)Z
    .locals 6

    .prologue
    .line 475396
    check-cast p1, LX/2ty;

    const/4 v1, 0x0

    .line 475397
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v0

    .line 475398
    iget-object v2, v0, LX/9JQ;->i:[I

    move-object v2, v2

    .line 475399
    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 475400
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 475401
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    move v0, v1

    .line 475402
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_0

    .line 475403
    aget v5, v2, v0

    if-ne v4, v5, :cond_2

    .line 475404
    const/4 v1, 0x1

    .line 475405
    :cond_1
    return v1

    .line 475406
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    .line 475407
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 475408
    invoke-interface {p2}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v2

    .line 475409
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 475410
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reloadFromMutableFlatbuffer() called with null buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475411
    :cond_1
    new-instance v0, LX/15i;

    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v2}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 475412
    const-string v1, "SimpleWriterStore.reloadFromMutableFlatbuffer"

    invoke-virtual {v0, v1, p2}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 475413
    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 475414
    invoke-interface {p2}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0

    .line 475415
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 475357
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 475358
    check-cast p1, LX/2ty;

    .line 475359
    invoke-virtual {p1}, LX/2ty;->close()V

    .line 475360
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 475340
    check-cast p1, LX/2ty;

    .line 475341
    invoke-interface {p3}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 475342
    if-nez v0, :cond_0

    .line 475343
    const/4 v0, 0x0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 475344
    :goto_0
    return-void

    .line 475345
    :cond_0
    invoke-virtual {v0}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 475346
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/31S;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    .line 475347
    :goto_1
    const-string v1, "confirmed"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v1

    .line 475348
    iget p2, v1, LX/9JQ;->d:I

    move v1, p2

    .line 475349
    invoke-static {v1, v0}, LX/9JT;->a(I[B)LX/9JT;

    move-result-object v0

    .line 475350
    :goto_2
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v1

    .line 475351
    iget-object p1, v1, LX/9JQ;->c:Ljava/lang/String;

    move-object v1, p1

    .line 475352
    invoke-direct {p0, v1, v0}, LX/2tx;->a(Ljava/lang/String;LX/9JT;)V

    goto :goto_0

    .line 475353
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 475354
    :cond_2
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v1

    .line 475355
    iget p2, v1, LX/9JQ;->d:I

    move v1, p2

    .line 475356
    invoke-static {v1, v0}, LX/9JT;->b(I[B)LX/9JT;

    move-result-object v0

    goto :goto_2
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 475336
    check-cast p1, LX/2ty;

    .line 475337
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v0

    .line 475338
    iget-object p0, v0, LX/9JQ;->e:LX/0w5;

    move-object v0, p0

    .line 475339
    invoke-virtual {v0}, LX/0w5;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 475335
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 475331
    iget-object v0, p0, LX/2tx;->a:LX/2ty;

    if-eqz v0, :cond_0

    .line 475332
    iget-object v0, p0, LX/2tx;->a:LX/2ty;

    invoke-virtual {v0}, LX/2ty;->close()V

    .line 475333
    const/4 v0, 0x0

    iput-object v0, p0, LX/2tx;->a:LX/2ty;

    .line 475334
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 5

    .prologue
    .line 475322
    check-cast p1, LX/2ty;

    .line 475323
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v0

    .line 475324
    iget-object v1, v0, LX/9JQ;->a:LX/9JR;

    move-object v0, v1

    .line 475325
    iget-object v1, p0, LX/2tx;->b:LX/2kD;

    .line 475326
    iget v2, v0, LX/9JR;->b:I

    move v2, v2

    .line 475327
    invoke-virtual {v0}, LX/9JR;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v4

    .line 475328
    iget-object p0, v4, LX/9JQ;->e:LX/0w5;

    move-object v4, p0

    .line 475329
    iget-object p0, v0, LX/9JR;->c:[B

    move-object v0, p0

    .line 475330
    invoke-virtual {v1, v2, v3, v4, v0}, LX/2kD;->a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475321
    const-string v0, "SimpleEdgeStore"

    return-object v0
.end method

.method public final e()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JT;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 475320
    iget-object v0, p0, LX/2tx;->d:Ljava/util/Map;

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 475316
    check-cast p1, LX/2ty;

    .line 475317
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v0

    .line 475318
    iget-object p0, v0, LX/9JQ;->b:LX/9JR;

    move-object v0, p0

    .line 475319
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 475315
    return-void
.end method

.method public final g(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 475308
    check-cast p1, LX/2ty;

    .line 475309
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v0

    .line 475310
    iget-object v1, v0, LX/9JQ;->c:Ljava/lang/String;

    move-object v0, v1

    .line 475311
    invoke-virtual {p1}, LX/2ty;->a()LX/9JQ;

    move-result-object v1

    .line 475312
    iget p1, v1, LX/9JQ;->d:I

    move v1, p1

    .line 475313
    invoke-static {v1}, LX/9JT;->b(I)LX/9JT;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/2tx;->a(Ljava/lang/String;LX/9JT;)V

    .line 475314
    return-void
.end method

.method public final bridge synthetic h(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 475301
    return-void
.end method
