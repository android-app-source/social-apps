.class public final LX/3Eg;
.super LX/3Eh;
.source ""


# instance fields
.field public final synthetic a:LX/3DO;

.field public final synthetic b:LX/2ub;

.field public final synthetic c:LX/1rk;


# direct methods
.method public constructor <init>(LX/1rk;Ljava/lang/Long;LX/3DO;LX/2ub;)V
    .locals 0

    .prologue
    .line 537930
    iput-object p1, p0, LX/3Eg;->c:LX/1rk;

    iput-object p3, p0, LX/3Eg;->a:LX/3DO;

    iput-object p4, p0, LX/3Eg;->b:LX/2ub;

    invoke-direct {p0, p2}, LX/3Eh;-><init>(Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 537931
    invoke-super {p0, p1}, LX/3Eh;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 537932
    iget-object v0, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, v0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v0, LX/3DO;->FULL:LX/3DO;

    iget-object v2, p0, LX/3Eg;->a:LX/3DO;

    if-ne v0, v2, :cond_0

    const v0, 0x350002

    :goto_0
    const/4 v2, 0x2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 537933
    iget-object v0, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, p0, LX/3Eg;->b:LX/2ub;

    iget-object v2, p0, LX/3Eg;->a:LX/3DO;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/1rk;->a$redex0(LX/1rk;LX/2ub;LX/3DO;Z)V

    .line 537934
    iget-object v0, p0, LX/3Eg;->a:LX/3DO;

    sget-object v1, LX/3DO;->FULL:LX/3DO;

    if-ne v0, v1, :cond_1

    sget-object v0, LX/0hM;->D:LX/0Tn;

    .line 537935
    :goto_1
    iget-object v1, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, v1, LX/1rk;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 537936
    iget-object v1, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, v1, LX/1rk;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 537937
    return-void

    .line 537938
    :cond_0
    const v0, 0x350003

    goto :goto_0

    .line 537939
    :cond_1
    sget-object v0, LX/0hM;->C:LX/0Tn;

    goto :goto_1
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 537940
    iget-object v0, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, v0, LX/1rk;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v0, LX/3DO;->FULL:LX/3DO;

    iget-object v2, p0, LX/3Eg;->a:LX/3DO;

    if-ne v0, v2, :cond_0

    const v0, 0x350002

    :goto_0
    const/4 v2, 0x3

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 537941
    iget-object v0, p0, LX/3Eg;->c:LX/1rk;

    iget-object v1, p0, LX/3Eg;->b:LX/2ub;

    iget-object v2, p0, LX/3Eg;->a:LX/3DO;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/1rk;->a$redex0(LX/1rk;LX/2ub;LX/3DO;Z)V

    .line 537942
    invoke-super {p0, p1}, LX/3Eh;->onFailure(Ljava/lang/Throwable;)V

    .line 537943
    return-void

    .line 537944
    :cond_0
    const v0, 0x350003

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 537945
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/3Eg;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
