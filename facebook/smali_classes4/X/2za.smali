.class public LX/2za;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 483524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 483525
    const/16 v18, 0x0

    .line 483526
    const/4 v15, 0x0

    .line 483527
    const-wide/16 v16, 0x0

    .line 483528
    const/4 v14, 0x0

    .line 483529
    const/4 v13, 0x0

    .line 483530
    const/4 v12, 0x0

    .line 483531
    const/4 v11, 0x0

    .line 483532
    const/4 v10, 0x0

    .line 483533
    const/4 v9, 0x0

    .line 483534
    const/4 v8, 0x0

    .line 483535
    const/4 v7, 0x0

    .line 483536
    const/4 v6, 0x0

    .line 483537
    const/4 v5, 0x0

    .line 483538
    const/4 v4, 0x0

    .line 483539
    const/4 v3, 0x0

    .line 483540
    const/4 v2, 0x0

    .line 483541
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_12

    .line 483542
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 483543
    const/4 v2, 0x0

    .line 483544
    :goto_0
    return v2

    .line 483545
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_f

    .line 483546
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 483547
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 483548
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 483549
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 483550
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 483551
    :cond_1
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 483552
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 483553
    :cond_2
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 483554
    const/4 v2, 0x1

    .line 483555
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 483556
    :cond_3
    const-string v6, "gap_rule"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 483557
    const/4 v2, 0x1

    .line 483558
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move/from16 v19, v6

    goto :goto_1

    .line 483559
    :cond_4
    const-string v6, "gysjItems"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 483560
    invoke-static/range {p0 .. p1}, LX/2cS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 483561
    :cond_5
    const-string v6, "gysjTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 483562
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 483563
    :cond_6
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 483564
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 483565
    :cond_7
    const-string v6, "items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 483566
    invoke-static/range {p0 .. p1}, LX/2cS;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 483567
    :cond_8
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 483568
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 483569
    :cond_9
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 483570
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 483571
    :cond_a
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 483572
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 483573
    :cond_b
    const-string v6, "local_removed_items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 483574
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 483575
    :cond_c
    const-string v6, "parent_object"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 483576
    invoke-static/range {p0 .. p1}, LX/30j;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 483577
    :cond_d
    const-string v6, "cover_item"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 483578
    invoke-static/range {p0 .. p1}, LX/3nb;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 483579
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 483580
    :cond_f
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 483581
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 483582
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 483583
    if-eqz v3, :cond_10

    .line 483584
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 483585
    :cond_10
    if-eqz v8, :cond_11

    .line 483586
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 483587
    :cond_11
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 483588
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 483589
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 483590
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 483591
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 483592
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 483593
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 483594
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 483595
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 483596
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 483597
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move/from16 v19, v14

    move/from16 v20, v18

    move v14, v9

    move/from16 v18, v13

    move v13, v8

    move v9, v4

    move v8, v2

    move/from16 v22, v10

    move v10, v5

    move-wide/from16 v4, v16

    move/from16 v17, v12

    move/from16 v16, v11

    move v11, v6

    move v12, v7

    move v7, v15

    move/from16 v15, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 483598
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 483599
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 483600
    invoke-static {p0, v2}, LX/2za;->a(LX/15w;LX/186;)I

    move-result v1

    .line 483601
    if-eqz v0, :cond_0

    .line 483602
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 483603
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 483604
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 483605
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 483606
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 483607
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 483608
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xb

    const/4 v3, 0x0

    .line 483609
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 483610
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483611
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 483612
    const-string v0, "name"

    const-string v1, "GroupsYouShouldJoinFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483613
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 483614
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 483615
    if-eqz v0, :cond_0

    .line 483616
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483617
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 483618
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 483619
    if-eqz v0, :cond_1

    .line 483620
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483621
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 483622
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 483623
    cmp-long v2, v0, v6

    if-eqz v2, :cond_2

    .line 483624
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483625
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 483626
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 483627
    if-eqz v0, :cond_3

    .line 483628
    const-string v1, "gap_rule"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483629
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 483630
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483631
    if-eqz v0, :cond_4

    .line 483632
    const-string v1, "gysjItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483633
    invoke-static {p0, v0, p2, p3}, LX/2cS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 483634
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483635
    if-eqz v0, :cond_5

    .line 483636
    const-string v1, "gysjTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483637
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 483638
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 483639
    if-eqz v0, :cond_6

    .line 483640
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483641
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 483642
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483643
    if-eqz v0, :cond_7

    .line 483644
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483645
    invoke-static {p0, v0, p2, p3}, LX/2cS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 483646
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 483647
    if-eqz v0, :cond_8

    .line 483648
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483649
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 483650
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483651
    if-eqz v0, :cond_9

    .line 483652
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483653
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 483654
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 483655
    if-eqz v0, :cond_a

    .line 483656
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483657
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 483658
    :cond_a
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 483659
    if-eqz v0, :cond_b

    .line 483660
    const-string v0, "local_removed_items"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483661
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 483662
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483663
    if-eqz v0, :cond_c

    .line 483664
    const-string v1, "parent_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483665
    invoke-static {p0, v0, p2, p3}, LX/30j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 483666
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 483667
    if-eqz v0, :cond_d

    .line 483668
    const-string v1, "cover_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 483669
    invoke-static {p0, v0, p2, p3}, LX/3nb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 483670
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 483671
    return-void
.end method
