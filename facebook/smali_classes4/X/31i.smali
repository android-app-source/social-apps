.class public LX/31i;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:[D


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 487852
    const/4 v0, 0x4

    new-array v0, v0, [D

    sput-object v0, LX/31i;->e:[D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 487887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1

    .prologue
    .line 487889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487890
    iput-wide p1, p0, LX/31i;->c:D

    .line 487891
    iput-wide p3, p0, LX/31i;->a:D

    .line 487892
    iput-wide p5, p0, LX/31i;->d:D

    .line 487893
    iput-wide p7, p0, LX/31i;->b:D

    .line 487894
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 4

    .prologue
    .line 487888
    iget-wide v0, p0, LX/31i;->c:D

    iget-wide v2, p0, LX/31i;->d:D

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public final a(DDD)V
    .locals 13

    .prologue
    .line 487853
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 487854
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 487855
    sget-object v0, LX/31i;->e:[D

    const/4 v1, 0x0

    iget-wide v6, p0, LX/31i;->c:D

    sub-double v6, v6, p3

    aput-wide v6, v0, v1

    .line 487856
    sget-object v0, LX/31i;->e:[D

    const/4 v1, 0x1

    iget-wide v6, p0, LX/31i;->a:D

    sub-double v6, v6, p5

    aput-wide v6, v0, v1

    .line 487857
    sget-object v0, LX/31i;->e:[D

    const/4 v1, 0x2

    iget-wide v6, p0, LX/31i;->d:D

    sub-double v6, v6, p3

    aput-wide v6, v0, v1

    .line 487858
    sget-object v0, LX/31i;->e:[D

    const/4 v1, 0x3

    iget-wide v6, p0, LX/31i;->b:D

    sub-double v6, v6, p5

    aput-wide v6, v0, v1

    .line 487859
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    iput-wide v0, p0, LX/31i;->c:D

    .line 487860
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v0, p0, LX/31i;->d:D

    .line 487861
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    iput-wide v0, p0, LX/31i;->a:D

    .line 487862
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v0, p0, LX/31i;->b:D

    .line 487863
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x2

    if-gt v1, v0, :cond_5

    .line 487864
    const/4 v0, 0x1

    :goto_1
    const/4 v6, 0x3

    if-gt v0, v6, :cond_4

    .line 487865
    sget-object v6, LX/31i;->e:[D

    aget-wide v6, v6, v1

    mul-double/2addr v6, v2

    sget-object v8, LX/31i;->e:[D

    aget-wide v8, v8, v0

    mul-double/2addr v8, v4

    sub-double/2addr v6, v8

    .line 487866
    sget-object v8, LX/31i;->e:[D

    aget-wide v8, v8, v1

    mul-double/2addr v8, v4

    sget-object v10, LX/31i;->e:[D

    aget-wide v10, v10, v0

    mul-double/2addr v10, v2

    add-double/2addr v8, v10

    .line 487867
    iget-wide v10, p0, LX/31i;->c:D

    cmpg-double v10, v6, v10

    if-gez v10, :cond_0

    .line 487868
    iput-wide v6, p0, LX/31i;->c:D

    .line 487869
    :cond_0
    iget-wide v10, p0, LX/31i;->d:D

    cmpg-double v10, v10, v6

    if-gez v10, :cond_1

    .line 487870
    iput-wide v6, p0, LX/31i;->d:D

    .line 487871
    :cond_1
    iget-wide v6, p0, LX/31i;->a:D

    cmpg-double v6, v8, v6

    if-gez v6, :cond_2

    .line 487872
    iput-wide v8, p0, LX/31i;->a:D

    .line 487873
    :cond_2
    iget-wide v6, p0, LX/31i;->b:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    .line 487874
    iput-wide v8, p0, LX/31i;->b:D

    .line 487875
    :cond_3
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 487876
    :cond_4
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_0

    .line 487877
    :cond_5
    iget-wide v0, p0, LX/31i;->c:D

    add-double v0, v0, p3

    iput-wide v0, p0, LX/31i;->c:D

    .line 487878
    iget-wide v0, p0, LX/31i;->d:D

    add-double v0, v0, p3

    iput-wide v0, p0, LX/31i;->d:D

    .line 487879
    iget-wide v0, p0, LX/31i;->a:D

    add-double v0, v0, p5

    iput-wide v0, p0, LX/31i;->a:D

    .line 487880
    iget-wide v0, p0, LX/31i;->b:D

    add-double v0, v0, p5

    iput-wide v0, p0, LX/31i;->b:D

    .line 487881
    return-void
.end method

.method public final a(LX/31i;)V
    .locals 2

    .prologue
    .line 487882
    iget-wide v0, p1, LX/31i;->a:D

    iput-wide v0, p0, LX/31i;->a:D

    .line 487883
    iget-wide v0, p1, LX/31i;->b:D

    iput-wide v0, p0, LX/31i;->b:D

    .line 487884
    iget-wide v0, p1, LX/31i;->c:D

    iput-wide v0, p0, LX/31i;->c:D

    .line 487885
    iget-wide v0, p1, LX/31i;->d:D

    iput-wide v0, p0, LX/31i;->d:D

    .line 487886
    return-void
.end method

.method public final a(DD)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 487835
    iget-wide v2, p0, LX/31i;->c:D

    iget-wide v4, p0, LX/31i;->d:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->a:D

    iget-wide v4, p0, LX/31i;->b:D

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 487836
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, LX/31i;->c:D

    cmpg-double v1, v2, p1

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->d:D

    cmpg-double v1, p1, v2

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->a:D

    cmpg-double v1, v2, p3

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->b:D

    cmpg-double v1, p3, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()D
    .locals 4

    .prologue
    .line 487837
    iget-wide v0, p0, LX/31i;->a:D

    iget-wide v2, p0, LX/31i;->b:D

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public final c(LX/31i;)Z
    .locals 4

    .prologue
    .line 487838
    iget-wide v0, p0, LX/31i;->c:D

    iget-wide v2, p1, LX/31i;->d:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    iget-wide v0, p1, LX/31i;->c:D

    iget-wide v2, p0, LX/31i;->d:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    iget-wide v0, p0, LX/31i;->a:D

    iget-wide v2, p1, LX/31i;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    iget-wide v0, p1, LX/31i;->a:D

    iget-wide v2, p0, LX/31i;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    .line 487839
    iget-wide v0, p0, LX/31i;->c:D

    iget-wide v2, p1, LX/31i;->c:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 487840
    iget-wide v0, p1, LX/31i;->c:D

    iput-wide v0, p0, LX/31i;->c:D

    .line 487841
    :cond_0
    iget-wide v0, p0, LX/31i;->a:D

    iget-wide v2, p1, LX/31i;->a:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 487842
    iget-wide v0, p1, LX/31i;->a:D

    iput-wide v0, p0, LX/31i;->a:D

    .line 487843
    :cond_1
    iget-wide v0, p0, LX/31i;->d:D

    iget-wide v2, p1, LX/31i;->d:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 487844
    iget-wide v0, p1, LX/31i;->d:D

    iput-wide v0, p0, LX/31i;->d:D

    .line 487845
    :cond_2
    iget-wide v0, p0, LX/31i;->b:D

    iget-wide v2, p1, LX/31i;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 487846
    iget-wide v0, p1, LX/31i;->b:D

    iput-wide v0, p0, LX/31i;->b:D

    .line 487847
    :cond_3
    const/4 v0, 0x1

    .line 487848
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(LX/31i;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 487849
    iget-wide v2, p0, LX/31i;->c:D

    iget-wide v4, p0, LX/31i;->d:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->a:D

    iget-wide v4, p0, LX/31i;->b:D

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 487850
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, LX/31i;->c:D

    iget-wide v4, p1, LX/31i;->c:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, LX/31i;->c:D

    iget-wide v4, p0, LX/31i;->d:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->c:D

    iget-wide v4, p1, LX/31i;->d:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, LX/31i;->d:D

    iget-wide v4, p0, LX/31i;->d:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->a:D

    iget-wide v4, p1, LX/31i;->b:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, LX/31i;->b:D

    iget-wide v4, p0, LX/31i;->b:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p0, LX/31i;->a:D

    iget-wide v4, p1, LX/31i;->a:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, LX/31i;->a:D

    iget-wide v4, p0, LX/31i;->b:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 487851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/31i;->c:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/31i;->a:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/31i;->d:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/31i;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
