.class public final LX/2gG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/presence/ThreadPresenceManager;


# direct methods
.method public constructor <init>(Lcom/facebook/presence/ThreadPresenceManager;)V
    .locals 0

    .prologue
    .line 448209
    iput-object p1, p0, LX/2gG;->a:Lcom/facebook/presence/ThreadPresenceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x56d79465

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 448210
    const-string v1, "event"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object v1

    .line 448211
    sget-object v2, LX/3lV;->a:[I

    invoke-virtual {v1}, LX/2EU;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 448212
    :goto_0
    const v1, 0x2dd63cea

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 448213
    :pswitch_0
    iget-object v1, p0, LX/2gG;->a:Lcom/facebook/presence/ThreadPresenceManager;

    .line 448214
    invoke-static {v1}, Lcom/facebook/presence/ThreadPresenceManager;->b$redex0(Lcom/facebook/presence/ThreadPresenceManager;)V

    .line 448215
    iget-object v2, v1, Lcom/facebook/presence/ThreadPresenceManager;->s:LX/0Xu;

    invoke-interface {v2}, LX/0Xu;->q()LX/1M1;

    move-result-object v2

    invoke-interface {v2}, LX/1M1;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    .line 448216
    invoke-static {v1, v2}, Lcom/facebook/presence/ThreadPresenceManager;->d(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)V

    goto :goto_1

    .line 448217
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
