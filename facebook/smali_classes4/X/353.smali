.class public final LX/353;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/1x4;

.field public final synthetic d:LX/1x7;


# direct methods
.method public constructor <init>(LX/1x7;Ljava/lang/String;Landroid/content/Context;LX/1x4;)V
    .locals 0

    .prologue
    .line 496510
    iput-object p1, p0, LX/353;->d:LX/1x7;

    iput-object p2, p0, LX/353;->a:Ljava/lang/String;

    iput-object p3, p0, LX/353;->b:Landroid/content/Context;

    iput-object p4, p0, LX/353;->c:LX/1x4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 496511
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    const-string v2, "fetch_fail"

    const-string v3, "graphql_failure"

    invoke-static {v0, v1, v2, v3}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496512
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v0, v0, LX/1x7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1x6;->a(Ljava/lang/String;)V

    .line 496513
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->d:LX/1x7;

    iget-object v1, v1, LX/1x7;->o:Landroid/content/res/Resources;

    const v2, 0x7f081a8c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/353;->b:Landroid/content/Context;

    invoke-static {v0, v1, v2}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Landroid/content/Context;)V

    .line 496514
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 496515
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 496516
    if-eqz p1, :cond_2

    .line 496517
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 496518
    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;

    move-object v4, v0

    .line 496519
    :goto_0
    if-nez v4, :cond_3

    move v3, v2

    .line 496520
    :goto_1
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v0, v0, LX/1x7;->l:LX/0Uh;

    const/16 v5, 0x646

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 496521
    if-eqz v5, :cond_5

    .line 496522
    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v2

    :goto_2
    or-int/2addr v0, v3

    .line 496523
    :goto_3
    if-eqz v0, :cond_7

    .line 496524
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v0, v0, LX/1x7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1x6;->a(Ljava/lang/String;)V

    .line 496525
    if-eqz v4, :cond_1

    .line 496526
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->j()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v11, 0x0

    .line 496527
    cmp-long v7, v2, v11

    if-nez v7, :cond_b

    .line 496528
    :cond_1
    :goto_4
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    const-string v2, "no_show"

    const-string v3, "invalid_graphql_result"

    invoke-static {v0, v1, v2, v3}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496529
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->d:LX/1x7;

    iget-object v1, v1, LX/1x7;->o:Landroid/content/res/Resources;

    const v2, 0x7f081a8a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/353;->b:Landroid/content/Context;

    invoke-static {v0, v1, v2}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Landroid/content/Context;)V

    .line 496530
    :goto_5
    return-void

    :cond_2
    move-object v4, v0

    .line 496531
    goto :goto_0

    :cond_3
    move v3, v1

    .line 496532
    goto :goto_1

    :cond_4
    move v0, v1

    .line 496533
    goto :goto_2

    .line 496534
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v0

    if-nez v0, :cond_6

    :goto_6
    or-int v0, v3, v2

    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_6

    .line 496535
    :cond_7
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v1, p0, LX/353;->a:Ljava/lang/String;

    const-string v2, "fetch_succeed"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496536
    if-eqz v5, :cond_a

    .line 496537
    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->k()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 496538
    :goto_7
    iget-object v0, p0, LX/353;->d:LX/1x7;

    iget-object v0, v0, LX/1x7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    iget-object v2, p0, LX/353;->a:Ljava/lang/String;

    .line 496539
    iget-object v3, v0, LX/1x6;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gu;

    .line 496540
    invoke-interface {v3}, LX/0gu;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 496541
    invoke-interface {v3, v1}, LX/0gu;->a(LX/0Px;)V

    goto :goto_8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 496542
    :cond_9
    goto :goto_5

    .line 496543
    :catch_0
    move-exception v0

    .line 496544
    iget-object v1, p0, LX/353;->d:LX/1x7;

    iget-object v1, v1, LX/1x7;->i:LX/03V;

    sget-object v2, LX/1x7;->b:Ljava/lang/String;

    const-string v3, "NaRF:IntegrationPoint Model Fetch Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 496545
    :cond_a
    :try_start_1
    invoke-virtual {v4}, Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v1, v0

    goto :goto_7

    .line 496546
    :cond_b
    iget-object v7, v0, LX/1x7;->m:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v9, v2

    add-long/2addr v9, v7

    .line 496547
    sget-object v7, LX/1x7;->c:LX/0Tn;

    invoke-virtual {v7, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/0Tn;

    .line 496548
    iget-object v8, v0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8, v7, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v11

    .line 496549
    cmp-long v8, v11, v9

    if-gez v8, :cond_1

    .line 496550
    iget-object v8, v0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    invoke-interface {v8, v7, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    goto/16 :goto_4
.end method
