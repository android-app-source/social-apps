.class public LX/30m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/30m;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/2Oq;

.field private final c:LX/2Uq;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNG;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Zb;LX/2Uq;LX/2Oq;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/2Uq;",
            "LX/2Oq;",
            "LX/0Ot",
            "<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNG;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486037
    iput-object p1, p0, LX/30m;->a:LX/0Zb;

    .line 486038
    iput-object p2, p0, LX/30m;->c:LX/2Uq;

    .line 486039
    iput-object p3, p0, LX/30m;->b:LX/2Oq;

    .line 486040
    iput-object p4, p0, LX/30m;->d:LX/0Ot;

    .line 486041
    iput-object p5, p0, LX/30m;->e:LX/0Ot;

    .line 486042
    iput-object p6, p0, LX/30m;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 486043
    return-void
.end method

.method public static a(LX/0QB;)LX/30m;
    .locals 10

    .prologue
    .line 486044
    sget-object v0, LX/30m;->g:LX/30m;

    if-nez v0, :cond_1

    .line 486045
    const-class v1, LX/30m;

    monitor-enter v1

    .line 486046
    :try_start_0
    sget-object v0, LX/30m;->g:LX/30m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 486047
    if-eqz v2, :cond_0

    .line 486048
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 486049
    new-instance v3, LX/30m;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v5

    check-cast v5, LX/2Uq;

    invoke-static {v0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v6

    check-cast v6, LX/2Oq;

    const/16 v7, 0x39

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x29ab

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v9}, LX/30m;-><init>(LX/0Zb;LX/2Uq;LX/2Oq;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 486050
    move-object v0, v3

    .line 486051
    sput-object v0, LX/30m;->g:LX/30m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486052
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 486053
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 486054
    :cond_1
    sget-object v0, LX/30m;->g:LX/30m;

    return-object v0

    .line 486055
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 486056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 486057
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "sticker"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 486058
    iget-object v1, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 486059
    const/4 v1, 0x0

    .line 486060
    :goto_1
    move-object v0, v1

    .line 486061
    goto :goto_0

    .line 486062
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 486063
    iget-object p0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_2

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 486064
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486065
    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 486066
    :cond_0
    iget-object v0, p0, LX/30m;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 486067
    return-void
.end method

.method public static a(LX/30m;ZZLjava/lang/String;II)V
    .locals 3
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 486084
    const-string v0, "sms_takeover_send_message"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_resend"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "state_now"

    invoke-static {p0}, LX/30m;->d(LX/30m;)LX/3Kt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 486085
    invoke-static {v0, p1, p3, p4, p5}, LX/30m;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/lang/String;II)V

    .line 486086
    invoke-static {p0, v0}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 486087
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/lang/String;II)V
    .locals 1

    .prologue
    .line 486068
    const-string v0, "is_mms"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486069
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486070
    const-string v0, "mms_media_type"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486071
    const-string v0, "mms_media_count"

    invoke-virtual {p0, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486072
    :cond_0
    const/4 v0, 0x1

    if-le p4, v0, :cond_1

    .line 486073
    const-string v0, "recipient_count"

    invoke-virtual {p0, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486074
    :cond_1
    return-void
.end method

.method public static c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 486075
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/30m;->c:LX/2Uq;

    .line 486076
    const-string p0, "android_messenger_sms_takeover_rollout"

    invoke-static {v1, p0}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 486077
    const-string p0, "qe_group_rollout"

    const-string p1, "android_messenger_sms_takeover_rollout"

    invoke-static {v1, p1}, LX/2Uq;->b(LX/2Uq;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object p0

    .line 486078
    :goto_0
    move-object v1, p0

    .line 486079
    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0

    .line 486080
    :cond_0
    const-string p0, "android_messenger_sms_integration_upsell"

    invoke-static {v1, p0}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 486081
    const-string p0, "qe_group_upsell"

    const-string p1, "android_messenger_sms_integration_upsell"

    invoke-static {v1, p1}, LX/2Uq;->b(LX/2Uq;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object p0

    goto :goto_0

    .line 486082
    :cond_1
    sget-object p0, LX/0Rg;->a:LX/0Rg;

    move-object p0, p0

    .line 486083
    goto :goto_0
.end method

.method private static d(LX/30m;)LX/3Kt;
    .locals 1

    .prologue
    .line 486033
    iget-object v0, p0, LX/30m;->b:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 486034
    sget-object v0, LX/3Kt;->NONE:LX/3Kt;

    .line 486035
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/30m;->b:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->c()LX/3Kt;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 486031
    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v2, v1

    move v4, v1

    invoke-static/range {v0 .. v5}, LX/30m;->a(LX/30m;ZZLjava/lang/String;II)V

    .line 486032
    return-void
.end method

.method public final a(LX/FMK;Z)V
    .locals 3

    .prologue
    .line 485967
    const-string v0, "sms_takeover_block_contact"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 485968
    if-eqz p2, :cond_0

    const-string v0, "block"

    .line 485969
    :goto_0
    const-string v2, "action"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485970
    const-string v0, "call_context"

    invoke-virtual {p1}, LX/FMK;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485971
    invoke-static {p0, v1}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 485972
    return-void

    .line 485973
    :cond_0
    const-string v0, "unblock"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/3Kt;LX/3Kt;)V
    .locals 3

    .prologue
    .line 486024
    const-string v0, "sms_takeover_state_change"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 486025
    const-string v0, "call_context"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486026
    const-string v0, "state_before"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486027
    const-string v0, "state_now"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486028
    const-string v2, "is_badged"

    iget-object v0, p0, LX/30m;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNG;

    invoke-virtual {v0}, LX/FNG;->a()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486029
    invoke-static {p0, v1}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 486030
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 486019
    const-string v0, "sms_takeover_matching_banner"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 486020
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 486021
    const-string v1, "reason"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486022
    :cond_0
    invoke-static {p0, v0}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 486023
    return-void
.end method

.method public final a(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/List;ZZZIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZZIZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 485982
    const-string v1, "sms_takeover_message_sent"

    invoke-static {p0, v1}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_sent_succeed"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 485983
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v3, p1, p5, p8, v1}, LX/30m;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/lang/String;II)V

    .line 485984
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 485985
    const-string v1, "error_type"

    invoke-virtual {v3, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485986
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 485987
    const-string v1, "error_msg"

    invoke-virtual {v3, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485988
    :cond_0
    const-string v1, "message_size"

    invoke-virtual {v3, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485989
    const-string v1, "retry_attempt_count"

    invoke-virtual {v3, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485990
    const-string v1, "legacy"

    move/from16 v0, p10

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485991
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 485992
    const/4 v1, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/3MF;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 485993
    const-string v2, "recipient_address"

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485994
    :cond_1
    const-string v2, "roaming"

    iget-object v1, p0, LX/30m;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485995
    const-string v2, "sim_state"

    iget-object v1, p0, LX/30m;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    invoke-static {v1}, LX/0km;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485996
    const-string v2, "Unknown"

    .line 485997
    :try_start_0
    iget-object v1, p0, LX/30m;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v1

    invoke-static {v1}, LX/0km;->d(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 485998
    :goto_0
    const-string v2, "data_state"

    invoke-virtual {v3, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 485999
    const-string v1, "non_last_error"

    move/from16 v0, p11

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486000
    const-string v1, "multipart_supported"

    move/from16 v0, p12

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486001
    const-string v1, "number_of_parts"

    move/from16 v0, p13

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486002
    const-string v1, "state_now"

    invoke-static {p0}, LX/30m;->d(LX/30m;)LX/3Kt;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486003
    const-string v1, "previous_error"

    move-object/from16 v0, p18

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486004
    if-nez p2, :cond_2

    .line 486005
    const-string v1, "fast_fail"

    move/from16 v0, p14

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486006
    :cond_2
    invoke-static/range {p15 .. p15}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 486007
    const-string v1, "X-Mms-Response-Status"

    move-object/from16 v0, p15

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486008
    :cond_3
    invoke-static/range {p16 .. p16}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 486009
    const-string v1, "X-Mms-Response-Text"

    move-object/from16 v0, p16

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486010
    :cond_4
    const-string v1, "self_number_source"

    move-object/from16 v0, p17

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486011
    const/4 v1, 0x1

    move/from16 v0, p20

    if-le v0, v1, :cond_5

    if-ltz p19, :cond_5

    .line 486012
    const-string v1, "sending_sim_slot"

    move/from16 v0, p19

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486013
    const-string v1, "active_sim_slots"

    move/from16 v0, p21

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486014
    const-string v1, "max_sim_slots"

    move/from16 v0, p20

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 486015
    :cond_5
    invoke-static {p0, v3}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 486016
    return-void

    .line 486017
    :catch_0
    move-exception v1

    .line 486018
    const-string v4, "SmsTakeoverAnalyticsLogger"

    const-string v5, "Can not get data state."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v1, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 485979
    const-string v0, "sms_takeover_class_zero_message_action"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 485980
    invoke-static {p0, v0}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 485981
    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 485977
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, LX/30m;->a(LX/30m;ZZLjava/lang/String;II)V

    .line 485978
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 485974
    const-string v0, "sms_takeover_match"

    invoke-static {p0, v0}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "matched"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 485975
    invoke-static {p0, v0}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 485976
    return-void
.end method
