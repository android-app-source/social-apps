.class public LX/2Gd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2Gd;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2Gj;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388997
    const-class v0, LX/2Gd;

    sput-object v0, LX/2Gd;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2Gj;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388999
    iput-object p1, p0, LX/2Gd;->b:LX/0Ot;

    .line 389000
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gd;
    .locals 5

    .prologue
    .line 389001
    sget-object v0, LX/2Gd;->c:LX/2Gd;

    if-nez v0, :cond_1

    .line 389002
    const-class v1, LX/2Gd;

    monitor-enter v1

    .line 389003
    :try_start_0
    sget-object v0, LX/2Gd;->c:LX/2Gd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389004
    if-eqz v2, :cond_0

    .line 389005
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389006
    new-instance v3, LX/2Gd;

    .line 389007
    new-instance v4, LX/2Gf;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/2Gf;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 389008
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 389009
    invoke-direct {v3, v4}, LX/2Gd;-><init>(LX/0Ot;)V

    .line 389010
    move-object v0, v3

    .line 389011
    sput-object v0, LX/2Gd;->c:LX/2Gd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389012
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389013
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389014
    :cond_1
    sget-object v0, LX/2Gd;->c:LX/2Gd;

    return-object v0

    .line 389015
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2Gj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389017
    iget-object v0, p0, LX/2Gd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4

    .prologue
    .line 389018
    invoke-direct {p0}, LX/2Gd;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389019
    sget-object v0, LX/2Gd;->a:Ljava/lang/Class;

    const-string v1, "No feature status reporters found; is this dead code?"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 389020
    const/4 v0, 0x0

    .line 389021
    :goto_0
    return-object v0

    .line 389022
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "features_status"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 389023
    invoke-direct {p0}, LX/2Gd;->a()Ljava/util/Set;

    move-result-object v1

    .line 389024
    new-instance v3, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 389025
    new-instance p0, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 389026
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Gj;

    .line 389027
    invoke-virtual {v2}, LX/2Gj;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2}, LX/2Gj;->c()Z

    move-result p3

    invoke-virtual {v3, p2, p3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389028
    invoke-virtual {v2}, LX/2Gj;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2}, LX/2Gj;->a()LX/0lF;

    move-result-object v2

    invoke-virtual {p0, p2, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    .line 389029
    :cond_1
    const-string v2, "features"

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 389030
    const-string v2, "features_extra_data"

    invoke-virtual {v0, v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 389031
    goto :goto_0
.end method
