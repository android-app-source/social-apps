.class public LX/2Hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Hx;


# instance fields
.field private final a:LX/1wV;


# direct methods
.method public constructor <init>(LX/1wV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391331
    iput-object p1, p0, LX/2Hx;->a:LX/1wV;

    .line 391332
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hx;
    .locals 4

    .prologue
    .line 391333
    sget-object v0, LX/2Hx;->b:LX/2Hx;

    if-nez v0, :cond_1

    .line 391334
    const-class v1, LX/2Hx;

    monitor-enter v1

    .line 391335
    :try_start_0
    sget-object v0, LX/2Hx;->b:LX/2Hx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391336
    if-eqz v2, :cond_0

    .line 391337
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391338
    new-instance p0, LX/2Hx;

    invoke-static {v0}, LX/1wV;->b(LX/0QB;)LX/1wV;

    move-result-object v3

    check-cast v3, LX/1wV;

    invoke-direct {p0, v3}, LX/2Hx;-><init>(LX/1wV;)V

    .line 391339
    move-object v0, p0

    .line 391340
    sput-object v0, LX/2Hx;->b:LX/2Hx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391341
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391342
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391343
    :cond_1
    sget-object v0, LX/2Hx;->b:LX/2Hx;

    return-object v0

    .line 391344
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391345
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391346
    iget-object v0, p0, LX/2Hx;->a:LX/1wV;

    .line 391347
    iget-object v1, v0, LX/1wV;->a:LX/0ad;

    sget-short v2, LX/15r;->g:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 391348
    if-eqz v0, :cond_0

    .line 391349
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 391350
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1se;

    const-string v1, "/mobile_requests_count"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v1, LX/2C3;->ALWAYS:LX/2C3;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0
.end method
