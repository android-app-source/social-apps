.class public final LX/3Fs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 540020
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 540021
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 540022
    :goto_0
    return v1

    .line 540023
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 540024
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 540025
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 540026
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 540027
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 540028
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 540029
    invoke-static {p0, p1}, LX/3Ft;->b(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 540030
    :cond_2
    const-string v5, "page_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 540031
    invoke-static {p0, p1}, LX/3h6;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 540032
    :cond_3
    const-string v5, "sync_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 540033
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 540034
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 540035
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 540036
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 540037
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 540038
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 540039
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 540040
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540041
    if-eqz v0, :cond_0

    .line 540042
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540043
    invoke-static {p0, v0, p2, p3}, LX/3Ft;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 540044
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 540045
    if-eqz v0, :cond_1

    .line 540046
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540047
    invoke-static {p0, v0, p2}, LX/3h6;->a(LX/15i;ILX/0nX;)V

    .line 540048
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 540049
    if-eqz v0, :cond_2

    .line 540050
    const-string v1, "sync_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 540051
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 540052
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 540053
    return-void
.end method
