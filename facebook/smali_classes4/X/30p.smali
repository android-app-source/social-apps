.class public LX/30p;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486092
    return-void
.end method


# virtual methods
.method public final a(ILX/03R;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 486093
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v2

    move-object v2, v2

    .line 486094
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 486095
    :goto_0
    if-nez v2, :cond_2

    .line 486096
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Context not available"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 486097
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v2, v1

    .line 486098
    goto :goto_0

    .line 486099
    :cond_2
    const-string v3, "lyra_flags_store"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 486100
    const/16 v3, 0x4b

    if-ne p1, v3, :cond_5

    .line 486101
    const-string v1, "android_crash_lyra_hook_cxa_throw"

    .line 486102
    :cond_3
    :goto_2
    if-eqz v1, :cond_0

    .line 486103
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne p2, v3, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 486104
    :cond_5
    const/16 v3, 0x4a

    if-ne p1, v3, :cond_3

    .line 486105
    const-string v1, "android_crash_lyra_enable_backtraces"

    goto :goto_2
.end method
