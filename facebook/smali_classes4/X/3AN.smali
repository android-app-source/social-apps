.class public final LX/3AN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1TG;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1TG;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 525127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525128
    iput-object p1, p0, LX/3AN;->a:LX/0QB;

    .line 525129
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 525130
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3AN;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 525131
    packed-switch p2, :pswitch_data_0

    .line 525132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525133
    :pswitch_0
    invoke-static {p1}, LX/GT0;->a(LX/0QB;)LX/GT0;

    move-result-object v0

    .line 525134
    :goto_0
    return-object v0

    .line 525135
    :pswitch_1
    invoke-static {p1}, LX/3Uw;->a(LX/0QB;)LX/3Uw;

    move-result-object v0

    goto :goto_0

    .line 525136
    :pswitch_2
    invoke-static {p1}, LX/BxY;->a(LX/0QB;)LX/BxY;

    move-result-object v0

    goto :goto_0

    .line 525137
    :pswitch_3
    invoke-static {p1}, LX/1TF;->a(LX/0QB;)LX/1TF;

    move-result-object v0

    goto :goto_0

    .line 525138
    :pswitch_4
    invoke-static {p1}, LX/DCm;->a(LX/0QB;)LX/DCm;

    move-result-object v0

    goto :goto_0

    .line 525139
    :pswitch_5
    invoke-static {p1}, LX/JN2;->a(LX/0QB;)LX/JN2;

    move-result-object v0

    goto :goto_0

    .line 525140
    :pswitch_6
    invoke-static {p1}, LX/1TI;->a(LX/0QB;)LX/1TI;

    move-result-object v0

    goto :goto_0

    .line 525141
    :pswitch_7
    invoke-static {p1}, LX/1TK;->a(LX/0QB;)LX/1TK;

    move-result-object v0

    goto :goto_0

    .line 525142
    :pswitch_8
    invoke-static {p1}, LX/1TL;->a(LX/0QB;)LX/1TL;

    move-result-object v0

    goto :goto_0

    .line 525143
    :pswitch_9
    invoke-static {p1}, LX/1TM;->a(LX/0QB;)LX/1TM;

    move-result-object v0

    goto :goto_0

    .line 525144
    :pswitch_a
    invoke-static {p1}, LX/DDw;->a(LX/0QB;)LX/DDw;

    move-result-object v0

    goto :goto_0

    .line 525145
    :pswitch_b
    invoke-static {p1}, LX/1TP;->a(LX/0QB;)LX/1TP;

    move-result-object v0

    goto :goto_0

    .line 525146
    :pswitch_c
    invoke-static {p1}, LX/1TQ;->a(LX/0QB;)LX/1TQ;

    move-result-object v0

    goto :goto_0

    .line 525147
    :pswitch_d
    invoke-static {p1}, LX/1TS;->a(LX/0QB;)LX/1TS;

    move-result-object v0

    goto :goto_0

    .line 525148
    :pswitch_e
    invoke-static {p1}, LX/1TT;->a(LX/0QB;)LX/1TT;

    move-result-object v0

    goto :goto_0

    .line 525149
    :pswitch_f
    invoke-static {p1}, LX/1TV;->a(LX/0QB;)LX/1TV;

    move-result-object v0

    goto :goto_0

    .line 525150
    :pswitch_10
    invoke-static {p1}, LX/3YO;->a(LX/0QB;)LX/3YO;

    move-result-object v0

    goto :goto_0

    .line 525151
    :pswitch_11
    invoke-static {p1}, LX/3YV;->a(LX/0QB;)LX/3YV;

    move-result-object v0

    goto :goto_0

    .line 525152
    :pswitch_12
    invoke-static {p1}, LX/3YY;->a(LX/0QB;)LX/3YY;

    move-result-object v0

    goto :goto_0

    .line 525153
    :pswitch_13
    invoke-static {p1}, LX/JVc;->a(LX/0QB;)LX/JVc;

    move-result-object v0

    goto :goto_0

    .line 525154
    :pswitch_14
    invoke-static {p1}, LX/1Tb;->a(LX/0QB;)LX/1Tb;

    move-result-object v0

    goto :goto_0

    .line 525155
    :pswitch_15
    invoke-static {p1}, LX/1Tc;->a(LX/0QB;)LX/1Tc;

    move-result-object v0

    goto :goto_0

    .line 525156
    :pswitch_16
    invoke-static {p1}, LX/1Td;->a(LX/0QB;)LX/1Td;

    move-result-object v0

    goto :goto_0

    .line 525157
    :pswitch_17
    invoke-static {p1}, LX/GgH;->a(LX/0QB;)LX/GgH;

    move-result-object v0

    goto :goto_0

    .line 525158
    :pswitch_18
    invoke-static {p1}, LX/1Te;->a(LX/0QB;)LX/1Te;

    move-result-object v0

    goto :goto_0

    .line 525159
    :pswitch_19
    invoke-static {p1}, LX/1Tg;->a(LX/0QB;)LX/1Tg;

    move-result-object v0

    goto :goto_0

    .line 525160
    :pswitch_1a
    invoke-static {p1}, LX/1Th;->a(LX/0QB;)LX/1Th;

    move-result-object v0

    goto/16 :goto_0

    .line 525161
    :pswitch_1b
    invoke-static {p1}, LX/1Ti;->a(LX/0QB;)LX/1Ti;

    move-result-object v0

    goto/16 :goto_0

    .line 525162
    :pswitch_1c
    invoke-static {p1}, LX/1Tj;->a(LX/0QB;)LX/1Tj;

    move-result-object v0

    goto/16 :goto_0

    .line 525163
    :pswitch_1d
    invoke-static {p1}, LX/1Tk;->a(LX/0QB;)LX/1Tk;

    move-result-object v0

    goto/16 :goto_0

    .line 525164
    :pswitch_1e
    invoke-static {p1}, LX/1Tl;->a(LX/0QB;)LX/1Tl;

    move-result-object v0

    goto/16 :goto_0

    .line 525165
    :pswitch_1f
    invoke-static {p1}, LX/1Tn;->a(LX/0QB;)LX/1Tn;

    move-result-object v0

    goto/16 :goto_0

    .line 525166
    :pswitch_20
    invoke-static {p1}, LX/1U2;->a(LX/0QB;)LX/1U2;

    move-result-object v0

    goto/16 :goto_0

    .line 525167
    :pswitch_21
    new-instance v0, LX/DbO;

    invoke-direct {v0}, LX/DbO;-><init>()V

    .line 525168
    move-object v0, v0

    .line 525169
    move-object v0, v0

    .line 525170
    goto/16 :goto_0

    .line 525171
    :pswitch_22
    new-instance v0, LX/6m7;

    invoke-direct {v0}, LX/6m7;-><init>()V

    .line 525172
    move-object v0, v0

    .line 525173
    move-object v0, v0

    .line 525174
    goto/16 :goto_0

    .line 525175
    :pswitch_23
    invoke-static {p1}, LX/3a5;->a(LX/0QB;)LX/3a5;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 525176
    const/16 v0, 0x24

    return v0
.end method
