.class public LX/2IR;
.super LX/0Vx;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2IR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 391662
    const-class v0, LX/2IR;

    sput-object v0, LX/2IR;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2WA;LX/0rb;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391663
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 391664
    invoke-interface {p2, p0}, LX/0rb;->a(LX/0rf;)V

    .line 391665
    return-void
.end method

.method public static a(LX/0QB;)LX/2IR;
    .locals 5

    .prologue
    .line 391666
    sget-object v0, LX/2IR;->c:LX/2IR;

    if-nez v0, :cond_1

    .line 391667
    const-class v1, LX/2IR;

    monitor-enter v1

    .line 391668
    :try_start_0
    sget-object v0, LX/2IR;->c:LX/2IR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391669
    if-eqz v2, :cond_0

    .line 391670
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391671
    new-instance p0, LX/2IR;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v4

    check-cast v4, LX/0rb;

    invoke-direct {p0, v3, v4}, LX/2IR;-><init>(LX/2WA;LX/0rb;)V

    .line 391672
    move-object v0, p0

    .line 391673
    sput-object v0, LX/2IR;->c:LX/2IR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391674
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391675
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391676
    :cond_1
    sget-object v0, LX/2IR;->c:LX/2IR;

    return-object v0

    .line 391677
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391678
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391679
    const-string v0, "memory_analytic_counters"

    return-object v0
.end method

.method public final a(LX/32G;)V
    .locals 4

    .prologue
    .line 391680
    invoke-virtual {p1}, LX/32G;->name()Ljava/lang/String;

    .line 391681
    invoke-virtual {p1}, LX/32G;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v0, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 391682
    return-void
.end method
