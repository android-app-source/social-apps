.class public LX/36W;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/2yS;

.field private final b:LX/36X;

.field private final c:LX/2yT;


# direct methods
.method public constructor <init>(LX/2yS;LX/36X;LX/2yT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498922
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 498923
    iput-object p1, p0, LX/36W;->a:LX/2yS;

    .line 498924
    iput-object p2, p0, LX/36W;->b:LX/36X;

    .line 498925
    iput-object p3, p0, LX/36W;->c:LX/2yT;

    .line 498926
    return-void
.end method

.method public static a(LX/0QB;)LX/36W;
    .locals 6

    .prologue
    .line 498927
    const-class v1, LX/36W;

    monitor-enter v1

    .line 498928
    :try_start_0
    sget-object v0, LX/36W;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498929
    sput-object v2, LX/36W;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498930
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498931
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498932
    new-instance p0, LX/36W;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v3

    check-cast v3, LX/2yS;

    invoke-static {v0}, LX/36X;->a(LX/0QB;)LX/36X;

    move-result-object v4

    check-cast v4, LX/36X;

    const-class v5, LX/2yT;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2yT;

    invoke-direct {p0, v3, v4, v5}, LX/36W;-><init>(LX/2yS;LX/36X;LX/2yT;)V

    .line 498933
    move-object v0, p0

    .line 498934
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498935
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498936
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 498938
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 498939
    iget-object v0, p0, LX/36W;->c:LX/2yT;

    const/4 v1, 0x2

    iget-object v2, p0, LX/36W;->a:LX/2yS;

    invoke-virtual {v0, p4, v1, v2}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v0

    const v1, 0x7f081c48

    invoke-virtual {p1, v1}, LX/1De;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 498940
    iput-object v1, v0, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 498941
    move-object v0, v0

    .line 498942
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/AE0;->c(I)LX/AE0;

    move-result-object v0

    iget-object v1, p0, LX/36W;->b:LX/36X;

    .line 498943
    new-instance v2, LX/AEj;

    invoke-direct {v2, v1, p3}, LX/AEj;-><init>(LX/36X;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v1, v2

    .line 498944
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 498945
    move-object v0, v0

    .line 498946
    const v1, 0x7f020742

    invoke-virtual {v0, v1}, LX/AE0;->a(I)LX/AE0;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/AE0;->d(I)LX/AE0;

    move-result-object v0

    return-object v0
.end method
