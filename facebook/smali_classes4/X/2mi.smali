.class public LX/2mi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/res/Resources;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/121;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461151
    iput-object p1, p0, LX/2mi;->a:LX/0Or;

    .line 461152
    const v0, 0x7f080e4a

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2mi;->b:Ljava/lang/String;

    .line 461153
    return-void
.end method

.method public static a(LX/0QB;)LX/2mi;
    .locals 5

    .prologue
    .line 461154
    const-class v1, LX/2mi;

    monitor-enter v1

    .line 461155
    :try_start_0
    sget-object v0, LX/2mi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461156
    sput-object v2, LX/2mi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461157
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461158
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 461159
    new-instance v4, LX/2mi;

    const/16 v3, 0xbd8

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, p0, v3}, LX/2mi;-><init>(LX/0Or;Landroid/content/res/Resources;)V

    .line 461160
    move-object v0, v4

    .line 461161
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461162
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2mi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461163
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461164
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(LX/3FZ;LX/3FY;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3FZ;",
            "LX/3FY;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 461165
    iget-object v0, p1, LX/3FY;->a:LX/3GR;

    .line 461166
    iput-object p2, v0, LX/3GR;->b:Landroid/view/View;

    .line 461167
    iget-object v0, p1, LX/3FY;->c:LX/3GE;

    .line 461168
    iget-object v1, p0, LX/3FZ;->b:LX/0QK;

    invoke-interface {v1, p2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 461169
    if-eqz v1, :cond_0

    .line 461170
    instance-of p1, v1, LX/3JH;

    if-eqz p1, :cond_1

    .line 461171
    check-cast v1, LX/3JH;

    .line 461172
    iget-object p1, v1, LX/3JH;->b:LX/7gP;

    move-object p1, p1

    .line 461173
    invoke-virtual {p1, v0, v1}, LX/7gP;->a(Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 461174
    :cond_0
    :goto_0
    return-void

    .line 461175
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static final b(LX/3FZ;LX/3FY;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3FZ;",
            "LX/3FY;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 461176
    if-nez p1, :cond_0

    .line 461177
    :goto_0
    return-void

    .line 461178
    :cond_0
    iget-object v0, p1, LX/3FY;->a:LX/3GR;

    const/4 v1, 0x0

    .line 461179
    iput-object v1, v0, LX/3GR;->b:Landroid/view/View;

    .line 461180
    const/4 p1, 0x0

    .line 461181
    iget-object v0, p0, LX/3FZ;->b:LX/0QK;

    invoke-interface {v0, p2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 461182
    if-eqz v0, :cond_1

    .line 461183
    instance-of v1, v0, LX/3JH;

    if-eqz v1, :cond_2

    .line 461184
    check-cast v0, LX/3JH;

    .line 461185
    iget-object v1, v0, LX/3JH;->b:LX/7gP;

    move-object v1, v1

    .line 461186
    invoke-virtual {v1, p1, v0}, LX/7gP;->a(Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 461187
    :cond_1
    :goto_1
    goto :goto_0

    .line 461188
    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/3FZ;)LX/3FY;
    .locals 5

    .prologue
    .line 461189
    new-instance v1, LX/3FY;

    new-instance v2, LX/3GR;

    iget-object v0, p1, LX/3FZ;->a:Landroid/view/View$OnClickListener;

    invoke-direct {v2, v0}, LX/3GR;-><init>(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, LX/2mi;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    invoke-direct {v1, v2, v0}, LX/3FY;-><init>(LX/3GR;LX/121;)V

    .line 461190
    iget-object v0, v1, LX/3FY;->b:LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v3, p0, LX/2mi;->b:Ljava/lang/String;

    iget-object v4, v1, LX/3FY;->a:LX/3GR;

    invoke-virtual {v0, v2, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 461191
    new-instance v0, LX/3GE;

    iget-object v2, v1, LX/3FY;->b:LX/121;

    iget-object v3, v1, LX/3FY;->a:LX/3GR;

    invoke-direct {v0, v2, v3}, LX/3GE;-><init>(LX/121;LX/3GR;)V

    iput-object v0, v1, LX/3FY;->c:LX/3GE;

    .line 461192
    return-object v1
.end method
