.class public LX/2H0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0dC;

.field public final e:LX/2Ge;

.field public final f:LX/0WV;

.field public final g:LX/2H3;

.field public h:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389719
    const-class v0, LX/2H0;

    sput-object v0, LX/2H0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Ge;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0dC;LX/0WV;LX/2Gr;LX/0SG;)V
    .locals 1
    .param p1    # LX/2Ge;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ge;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0WV;",
            "LX/2Gr;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389711
    iput-object p2, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 389712
    iput-object p3, p0, LX/2H0;->c:LX/0Or;

    .line 389713
    iput-object p4, p0, LX/2H0;->d:LX/0dC;

    .line 389714
    iput-object p1, p0, LX/2H0;->e:LX/2Ge;

    .line 389715
    iput-object p5, p0, LX/2H0;->f:LX/0WV;

    .line 389716
    iget-object v0, p0, LX/2H0;->e:LX/2Ge;

    invoke-virtual {p6, v0}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v0

    iput-object v0, p0, LX/2H0;->g:LX/2H3;

    .line 389717
    iput-object p7, p0, LX/2H0;->h:LX/0SG;

    .line 389718
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 389707
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389708
    iget-object v2, v1, LX/2H3;->a:LX/0Tn;

    move-object v1, v2

    .line 389709
    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 389659
    iget-object v0, p0, LX/2H0;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 389660
    iget-object v2, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/2H0;->g:LX/2H3;

    .line 389661
    iget-object v4, v3, LX/2H3;->a:LX/0Tn;

    move-object v3, v4

    .line 389662
    invoke-interface {v2, v3, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/2H0;->g:LX/2H3;

    .line 389663
    iget-object v4, v3, LX/2H3;->d:LX/0Tn;

    move-object v3, v4

    .line 389664
    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/2H0;->g:LX/2H3;

    .line 389665
    iget-object v4, v3, LX/2H3;->c:LX/0Tn;

    move-object v3, v4

    .line 389666
    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389667
    iget-object v2, v1, LX/2H3;->j:LX/0Tn;

    move-object v1, v2

    .line 389668
    iget-object v2, p0, LX/2H0;->f:LX/0WV;

    invoke-virtual {v2}, LX/0WV;->b()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389669
    iget-object v2, v1, LX/2H3;->k:LX/0Tn;

    move-object v1, v2

    .line 389670
    iget-object v2, p0, LX/2H0;->d:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    .line 389671
    invoke-virtual {p0}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/2H0;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/2H0;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389672
    :cond_0
    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389673
    iget-object v2, v1, LX/2H3;->h:LX/0Tn;

    move-object v1, v2

    .line 389674
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 389675
    :cond_1
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 389676
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 389704
    iget-object v0, p0, LX/2H0;->f:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    iget-object v1, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/2H0;->g:LX/2H3;

    .line 389705
    iget-object v3, v2, LX/2H3;->j:LX/0Tn;

    move-object v2, v3

    .line 389706
    const/high16 v3, -0x80000000

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 389700
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389701
    iget-object v2, v1, LX/2H3;->k:LX/0Tn;

    move-object v1, v2

    .line 389702
    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389703
    iget-object v1, p0, LX/2H0;->d:LX/0dC;

    invoke-virtual {v1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 389697
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389698
    iget-object v2, v1, LX/2H3;->l:LX/0Tn;

    move-object v1, v2

    .line 389699
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 389720
    sget-object v0, LX/2XJ;->a:[I

    iget-object v1, p0, LX/2H0;->e:LX/2Ge;

    invoke-virtual {v1}, LX/2Ge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 389721
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported push notification service type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389722
    :pswitch_0
    invoke-virtual {p0}, LX/2H0;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 389723
    :goto_0
    return-object v0

    .line 389724
    :cond_0
    const-string v0, "https://android.googleapis.com/gcm/send"

    goto :goto_0

    .line 389725
    :pswitch_1
    const-string v0, "https://api.amazon.com/messaging/registrations/"

    goto :goto_0

    .line 389726
    :pswitch_2
    const-string v0, "https://nnapi.ovi.com/nnapi/2.0/send"

    goto :goto_0

    .line 389727
    :pswitch_3
    const-string v0, "https://www.facebook.com/"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 389683
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389684
    iget-object v2, v1, LX/2H3;->a:LX/0Tn;

    move-object v1, v2

    .line 389685
    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389686
    iget-object v2, v1, LX/2H3;->b:LX/0Tn;

    move-object v1, v2

    .line 389687
    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389688
    iget-object v2, v1, LX/2H3;->j:LX/0Tn;

    move-object v1, v2

    .line 389689
    invoke-interface {v0, v1, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389690
    iget-object v2, v1, LX/2H3;->k:LX/0Tn;

    move-object v1, v2

    .line 389691
    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389692
    iget-object v2, v1, LX/2H3;->d:LX/0Tn;

    move-object v1, v2

    .line 389693
    iget-object v2, p0, LX/2H0;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389694
    iget-object v2, v1, LX/2H3;->h:LX/0Tn;

    move-object v1, v2

    .line 389695
    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 389696
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 389680
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389681
    iget-object v2, v1, LX/2H3;->b:LX/0Tn;

    move-object v1, v2

    .line 389682
    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    .line 389677
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389678
    iget-object v2, v1, LX/2H3;->h:LX/0Tn;

    move-object v1, v2

    .line 389679
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final l()J
    .locals 4

    .prologue
    .line 389656
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389657
    iget-object v2, v1, LX/2H3;->c:LX/0Tn;

    move-object v1, v2

    .line 389658
    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 389652
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389653
    iget-object v2, v1, LX/2H3;->h:LX/0Tn;

    move-object v1, v2

    .line 389654
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 389655
    return-void
.end method

.method public final o()J
    .locals 4

    .prologue
    .line 389649
    iget-object v0, p0, LX/2H0;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H0;->g:LX/2H3;

    .line 389650
    iget-object v2, v1, LX/2H3;->i:LX/0Tn;

    move-object v1, v2

    .line 389651
    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method
