.class public LX/2aq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 425689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 425690
    const/16 v18, 0x0

    .line 425691
    const/16 v17, 0x0

    .line 425692
    const/16 v16, 0x0

    .line 425693
    const/4 v15, 0x0

    .line 425694
    const/4 v14, 0x0

    .line 425695
    const/4 v13, 0x0

    .line 425696
    const/4 v12, 0x0

    .line 425697
    const/4 v11, 0x0

    .line 425698
    const/4 v10, 0x0

    .line 425699
    const/4 v9, 0x0

    .line 425700
    const/4 v8, 0x0

    .line 425701
    const/4 v7, 0x0

    .line 425702
    const/4 v6, 0x0

    .line 425703
    const/4 v5, 0x0

    .line 425704
    const/4 v4, 0x0

    .line 425705
    const/4 v3, 0x0

    .line 425706
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 425707
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 425708
    const/4 v3, 0x0

    .line 425709
    :goto_0
    return v3

    .line 425710
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 425711
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_c

    .line 425712
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 425713
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 425714
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 425715
    const-string v20, "feedback_target"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 425716
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 425717
    :cond_2
    const-string v20, "full_relevant_comments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 425718
    invoke-static/range {p0 .. p1}, LX/2sx;->b(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 425719
    :cond_3
    const-string v20, "inapp_browser_prefetch_type"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 425720
    const/4 v7, 0x1

    .line 425721
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v16

    goto :goto_1

    .line 425722
    :cond_4
    const-string v20, "inapp_browser_prefetch_vpv_duration_threshold"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 425723
    const/4 v6, 0x1

    .line 425724
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto :goto_1

    .line 425725
    :cond_5
    const-string v20, "inapp_browser_prefetch_vpv_duration_threshold_wifi"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 425726
    const/4 v5, 0x1

    .line 425727
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto :goto_1

    .line 425728
    :cond_6
    const-string v20, "interesting_comments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 425729
    invoke-static/range {p0 .. p1}, LX/2sx;->b(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 425730
    :cond_7
    const-string v20, "read_likelihood"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 425731
    const/4 v4, 0x1

    .line 425732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-result-object v12

    goto/16 :goto_1

    .line 425733
    :cond_8
    const-string v20, "relevant_comments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 425734
    invoke-static/range {p0 .. p1}, LX/2sx;->b(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 425735
    :cond_9
    const-string v20, "relevant_reactors"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 425736
    invoke-static/range {p0 .. p1}, LX/4SP;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 425737
    :cond_a
    const-string v20, "inapp_browser_rapidfeedback_surveys"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 425738
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 425739
    :cond_b
    const-string v20, "inline_comments_interaction_likelihood"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 425740
    const/4 v3, 0x1

    .line 425741
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    move-result-object v8

    goto/16 :goto_1

    .line 425742
    :cond_c
    const/16 v19, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 425743
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 425744
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 425745
    if-eqz v7, :cond_d

    .line 425746
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 425747
    :cond_d
    if-eqz v6, :cond_e

    .line 425748
    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15, v7}, LX/186;->a(III)V

    .line 425749
    :cond_e
    if-eqz v5, :cond_f

    .line 425750
    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14, v6}, LX/186;->a(III)V

    .line 425751
    :cond_f
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 425752
    if-eqz v4, :cond_10

    .line 425753
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 425754
    :cond_10
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 425755
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 425756
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 425757
    if-eqz v3, :cond_11

    .line 425758
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 425759
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/16 v5, 0x9

    const/4 v4, 0x6

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 425760
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 425761
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 425762
    if-eqz v0, :cond_0

    .line 425763
    const-string v1, "feedback_target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425764
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 425765
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 425766
    if-eqz v0, :cond_1

    .line 425767
    const-string v1, "full_relevant_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425768
    invoke-static {p0, v0, p2, p3}, LX/2sx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 425769
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 425770
    if-eqz v0, :cond_2

    .line 425771
    const-string v0, "inapp_browser_prefetch_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425772
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 425773
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 425774
    if-eqz v0, :cond_3

    .line 425775
    const-string v1, "inapp_browser_prefetch_vpv_duration_threshold"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425776
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 425777
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 425778
    if-eqz v0, :cond_4

    .line 425779
    const-string v1, "inapp_browser_prefetch_vpv_duration_threshold_wifi"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425780
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 425781
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 425782
    if-eqz v0, :cond_5

    .line 425783
    const-string v1, "interesting_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425784
    invoke-static {p0, v0, p2, p3}, LX/2sx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 425785
    :cond_5
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 425786
    if-eqz v0, :cond_6

    .line 425787
    const-string v0, "read_likelihood"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425788
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 425789
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 425790
    if-eqz v0, :cond_7

    .line 425791
    const-string v1, "relevant_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425792
    invoke-static {p0, v0, p2, p3}, LX/2sx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 425793
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 425794
    if-eqz v0, :cond_8

    .line 425795
    const-string v1, "relevant_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425796
    invoke-static {p0, v0, p2, p3}, LX/4SP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 425797
    :cond_8
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 425798
    if-eqz v0, :cond_9

    .line 425799
    const-string v0, "inapp_browser_rapidfeedback_surveys"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425800
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 425801
    :cond_9
    invoke-virtual {p0, p1, v6, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 425802
    if-eqz v0, :cond_a

    .line 425803
    const-string v0, "inline_comments_interaction_likelihood"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 425804
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 425805
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 425806
    return-void
.end method
