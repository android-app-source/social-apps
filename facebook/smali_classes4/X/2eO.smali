.class public LX/2eO;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/2eY;",
        ">;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollViewMeasurePreparer;"
    }
.end annotation


# static fields
.field public static final a:LX/2eN;


# instance fields
.field public b:LX/2eL;

.field private c:LX/2ds;

.field public d:LX/2eN;

.field public e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 445265
    new-instance v0, LX/2eP;

    invoke-direct {v0}, LX/2eP;-><init>()V

    sput-object v0, LX/2eO;->a:LX/2eN;

    return-void
.end method

.method public constructor <init>(LX/2ds;LX/2eN;LX/2eL;)V
    .locals 2
    .param p2    # LX/2eN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2eL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445258
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 445259
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/2eO;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 445260
    iput-object p1, p0, LX/2eO;->c:LX/2ds;

    .line 445261
    iput-object p2, p0, LX/2eO;->d:LX/2eN;

    .line 445262
    iput-object p3, p0, LX/2eO;->b:LX/2eL;

    .line 445263
    iget-object v0, p0, LX/2eO;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 445264
    return-void
.end method

.method private a(Landroid/content/Context;I)LX/2eY;
    .locals 3

    .prologue
    .line 445253
    iget-object v0, p0, LX/2eO;->c:LX/2ds;

    invoke-virtual {v0, p2}, LX/2ds;->a(I)LX/2eR;

    move-result-object v0

    .line 445254
    const-string v1, "HScrollRecyclerViewAdapter.onCreateViewHolder"

    const v2, 0x7c94b274

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445255
    :try_start_0
    new-instance v1, LX/2eY;

    invoke-interface {v0, p1}, LX/2eS;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2eY;-><init>(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445256
    const v0, 0x11212633

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1

    :catchall_0
    move-exception v0

    const v1, 0x789e3970

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 445257
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/2eO;->a(Landroid/content/Context;I)LX/2eY;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;)V
    .locals 4

    .prologue
    .line 445240
    check-cast p1, LX/2eY;

    .line 445241
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 445242
    iget-object v0, p0, LX/2eO;->b:LX/2eL;

    invoke-virtual {p1}, LX/1a1;->e()I

    move-result v1

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    .line 445243
    invoke-static {v2}, LX/1aL;->b(Landroid/view/View;)LX/1Ra;

    move-result-object v3

    .line 445244
    if-nez v3, :cond_0

    .line 445245
    :goto_0
    return-void

    .line 445246
    :cond_0
    invoke-virtual {v0, v1}, LX/2eL;->a(I)LX/2eT;

    move-result-object v3

    .line 445247
    if-eqz v3, :cond_1

    .line 445248
    iget-object p0, v0, LX/2eL;->c:LX/0Ot;

    .line 445249
    iget-boolean p1, v3, LX/2eT;->c:Z

    if-nez p1, :cond_2

    .line 445250
    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/03V;

    const-string v0, "Unbind_Error"

    const-string v1, "Calling unbind without calling prepare/bind should never happen"

    invoke-virtual {p1, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445251
    :cond_1
    :goto_1
    invoke-static {v2}, LX/1aL;->a(Landroid/view/View;)LX/1Ra;

    goto :goto_0

    .line 445252
    :cond_2
    iget-object p1, v3, LX/2eT;->b:LX/1Ra;

    invoke-virtual {p1, v2}, LX/1Ra;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 445232
    check-cast p1, LX/2eY;

    .line 445233
    const-string v0, "HScrollRecyclerViewAdapter.onBindViewHolder"

    const v1, -0x540a999e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445234
    :try_start_0
    iget-object v0, p0, LX/2eO;->d:LX/2eN;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v0, v1}, LX/2eN;->a(Landroid/view/View;)V

    .line 445235
    iget-object v0, p0, LX/2eO;->d:LX/2eN;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-interface {v0, v1, v2, v3}, LX/2eN;->a(Landroid/view/View;II)V

    .line 445236
    iget-object v0, p0, LX/2eO;->b:LX/2eL;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, p2, v1}, LX/2eL;->a(ILandroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445237
    const v0, 0x76c7948

    invoke-static {v0}, LX/02m;->a(I)V

    .line 445238
    return-void

    .line 445239
    :catchall_0
    move-exception v0

    const v1, 0x4c0e197

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 445221
    iget-object v0, p0, LX/2eO;->b:LX/2eL;

    .line 445222
    invoke-virtual {v0, p1}, LX/2eL;->a(I)LX/2eT;

    move-result-object v1

    .line 445223
    if-nez v1, :cond_1

    .line 445224
    const/4 v1, 0x0

    .line 445225
    :goto_0
    move-object v0, v1

    .line 445226
    if-nez v0, :cond_0

    .line 445227
    const/4 v0, -0x1

    .line 445228
    :goto_1
    return v0

    :cond_0
    iget-object v1, p0, LX/2eO;->c:LX/2ds;

    invoke-virtual {v1, v0}, LX/2ds;->a(LX/2eR;)I

    move-result v0

    goto :goto_1

    .line 445229
    :cond_1
    iget-object v0, v1, LX/2eT;->a:LX/2eR;

    move-object v1, v0

    .line 445230
    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 445231
    iget-object v0, p0, LX/2eO;->b:LX/2eL;

    invoke-virtual {v0}, LX/2eL;->b()I

    move-result v0

    return v0
.end method
