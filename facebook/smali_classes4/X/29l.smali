.class public LX/29l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile o:LX/29l;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/0Uo;

.field private final d:LX/0Xw;

.field private final e:LX/0Zr;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0dN;

.field private final h:Landroid/os/Handler;

.field private i:Landroid/os/HandlerThread;

.field private j:Landroid/os/Handler;

.field private k:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public m:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private n:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 376619
    const-class v0, LX/29l;

    sput-object v0, LX/29l;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uo;LX/0Xw;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Zr;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376608
    iput-object p1, p0, LX/29l;->c:LX/0Uo;

    .line 376609
    iput-object p2, p0, LX/29l;->d:LX/0Xw;

    .line 376610
    iput-object p3, p0, LX/29l;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 376611
    iput-object p4, p0, LX/29l;->b:LX/0SG;

    .line 376612
    iput-object p5, p0, LX/29l;->e:LX/0Zr;

    .line 376613
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/29l;->h:Landroid/os/Handler;

    .line 376614
    iget-object v0, p0, LX/29l;->d:LX/0Xw;

    new-instance v1, LX/29n;

    invoke-direct {v1, p0}, LX/29n;-><init>(LX/29l;)V

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, LX/0Uo;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 376615
    iget-object v0, p0, LX/29l;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/29l;->l:Z

    .line 376616
    new-instance v0, LX/29o;

    invoke-direct {v0, p0}, LX/29o;-><init>(LX/29l;)V

    iput-object v0, p0, LX/29l;->g:LX/0dN;

    .line 376617
    iget-object v0, p0, LX/29l;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/29p;->a:LX/0Tn;

    iget-object v2, p0, LX/29l;->g:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 376618
    return-void
.end method

.method public static a(LX/0QB;)LX/29l;
    .locals 9

    .prologue
    .line 376594
    sget-object v0, LX/29l;->o:LX/29l;

    if-nez v0, :cond_1

    .line 376595
    const-class v1, LX/29l;

    monitor-enter v1

    .line 376596
    :try_start_0
    sget-object v0, LX/29l;->o:LX/29l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376597
    if-eqz v2, :cond_0

    .line 376598
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376599
    new-instance v3, LX/29l;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/29m;->a(LX/0QB;)LX/0Xw;

    move-result-object v5

    check-cast v5, LX/0Xw;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v8

    check-cast v8, LX/0Zr;

    invoke-direct/range {v3 .. v8}, LX/29l;-><init>(LX/0Uo;LX/0Xw;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Zr;)V

    .line 376600
    move-object v0, v3

    .line 376601
    sput-object v0, LX/29l;->o:LX/29l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376602
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376603
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376604
    :cond_1
    sget-object v0, LX/29l;->o:LX/29l;

    return-object v0

    .line 376605
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/29l;)V
    .locals 1

    .prologue
    .line 376590
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29l;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/29l;->l:Z

    .line 376591
    invoke-static {p0}, LX/29l;->b(LX/29l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376592
    monitor-exit p0

    return-void

    .line 376593
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/29l;Z)V
    .locals 3

    .prologue
    .line 376586
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/29l;->n:Z

    .line 376587
    iget-object v0, p0, LX/29l;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/debug/watchdog/UiThreadWatchdog$3;

    invoke-direct {v1, p0}, Lcom/facebook/debug/watchdog/UiThreadWatchdog$3;-><init>(LX/29l;)V

    const v2, 0x23d5b89d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376588
    monitor-exit p0

    return-void

    .line 376589
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/29l;)V
    .locals 2

    .prologue
    .line 376570
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/29l;->h()Z

    move-result v0

    .line 376571
    iget-boolean v1, p0, LX/29l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 376572
    :goto_0
    monitor-exit p0

    return-void

    .line 376573
    :cond_0
    if-eqz v0, :cond_1

    .line 376574
    :try_start_1
    iget-object v0, p0, LX/29l;->e:LX/0Zr;

    const-string v1, "UiThreadWatchdog"

    invoke-virtual {v0, v1}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/29l;->i:Landroid/os/HandlerThread;

    .line 376575
    iget-object v0, p0, LX/29l;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 376576
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/29l;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/29l;->j:Landroid/os/Handler;

    .line 376577
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/29l;->m:Z

    .line 376578
    invoke-static {p0}, LX/29l;->d$redex0(LX/29l;)V

    .line 376579
    invoke-static {p0}, LX/29l;->c$redex0(LX/29l;)V

    .line 376580
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, LX/Hwm;

    invoke-direct {v1, p0}, LX/Hwm;-><init>(LX/29l;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376582
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/29l;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 376583
    const/4 v0, 0x0

    iput-object v0, p0, LX/29l;->i:Landroid/os/HandlerThread;

    .line 376584
    const/4 v0, 0x0

    iput-object v0, p0, LX/29l;->j:Landroid/os/Handler;

    .line 376585
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/29l;->m:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized c$redex0(LX/29l;)V
    .locals 5

    .prologue
    .line 376566
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/29l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 376567
    :goto_0
    monitor-exit p0

    return-void

    .line 376568
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/29l;->j:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/debug/watchdog/UiThreadWatchdog$5;

    invoke-direct {v1, p0}, Lcom/facebook/debug/watchdog/UiThreadWatchdog$5;-><init>(LX/29l;)V

    const-wide/16 v2, 0xc8

    const v4, 0x597b0aa8

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized d$redex0(LX/29l;)V
    .locals 5

    .prologue
    .line 376562
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/29l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 376563
    :goto_0
    monitor-exit p0

    return-void

    .line 376564
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/29l;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/debug/watchdog/UiThreadWatchdog$6;

    invoke-direct {v1, p0}, Lcom/facebook/debug/watchdog/UiThreadWatchdog$6;-><init>(LX/29l;)V

    const-wide/16 v2, 0xc8

    const v4, 0x39d0605d

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376565
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f$redex0(LX/29l;)V
    .locals 2

    .prologue
    .line 376555
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/29l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 376556
    :goto_0
    monitor-exit p0

    return-void

    .line 376557
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/29l;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/29l;->l:Z

    .line 376558
    iget-boolean v0, p0, LX/29l;->l:Z

    if-eqz v0, :cond_1

    .line 376559
    invoke-static {p0}, LX/29l;->b(LX/29l;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376561
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/29l;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/29l;->k:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized g$redex0(LX/29l;)V
    .locals 5

    .prologue
    .line 376531
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/29l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 376532
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 376533
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/29l;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/29l;->l:Z

    .line 376534
    iget-boolean v0, p0, LX/29l;->l:Z

    if-eqz v0, :cond_2

    .line 376535
    invoke-static {p0}, LX/29l;->b(LX/29l;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376536
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376537
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/29l;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 376538
    iget-wide v2, p0, LX/29l;->k:J

    sub-long/2addr v0, v2

    .line 376539
    const-wide/16 v2, 0x258

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 376540
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 376541
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 376542
    const-string v4, "UI Thread has been stuck for more than "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376543
    const-string v0, "Current UI thread stack\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376544
    const-string v0, "  "

    .line 376545
    const/4 v1, 0x0

    :goto_1
    array-length v4, v2

    if-ge v1, v4, :cond_3

    .line 376546
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376547
    const-string v4, "\tat "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376548
    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376549
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376550
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 376551
    :cond_3
    sget-object v0, LX/29l;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized h()Z
    .locals 1

    .prologue
    .line 376554
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/29l;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/29l;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 376552
    iget-object v0, p0, LX/29l;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/29p;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {p0, v0}, LX/29l;->a$redex0(LX/29l;Z)V

    .line 376553
    return-void
.end method
