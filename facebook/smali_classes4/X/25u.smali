.class public LX/25u;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/25u;


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/25t;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 370021
    new-instance v0, LX/K1o;

    invoke-direct {v0, v1, v1}, LX/K1o;-><init>(Landroid/util/SparseArray;[B)V

    sput-object v0, LX/25u;->a:LX/25u;

    return-void
.end method

.method public constructor <init>(Landroid/util/SparseArray;[B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/25t;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 370017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370018
    iput-object p1, p0, LX/25u;->b:Landroid/util/SparseArray;

    .line 370019
    iput-object p2, p0, LX/25u;->c:[B

    .line 370020
    return-void
.end method


# virtual methods
.method public a(ILX/0XJ;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 370003
    iget-object v0, p0, LX/25u;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25t;

    .line 370004
    if-nez v0, :cond_0

    .line 370005
    const/4 v0, 0x0

    .line 370006
    :goto_0
    return-object v0

    .line 370007
    :cond_0
    iget-object v2, v0, LX/25t;->a:Landroid/util/SparseArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 370008
    if-eqz v2, :cond_1

    .line 370009
    sget-object p2, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 370010
    :cond_1
    iget-object v0, v0, LX/25t;->a:Landroid/util/SparseArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25s;

    .line 370011
    iget-object v2, v0, LX/25s;->b:[I

    array-length v2, v2

    new-array v3, v2, [Ljava/lang/String;

    move v2, v1

    .line 370012
    :goto_2
    iget-object v4, v0, LX/25s;->b:[I

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 370013
    iget-object v4, p0, LX/25u;->c:[B

    iget v5, v0, LX/25s;->a:I

    add-int/2addr v5, v2

    iget-object v6, v0, LX/25s;->b:[I

    aget v6, v6, v1

    invoke-static {v4, v5, v6}, LX/25l;->a([BII)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 370014
    iget-object v4, v0, LX/25s;->b:[I

    aget v4, v4, v1

    add-int/2addr v2, v4

    .line 370015
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v3

    .line 370016
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
