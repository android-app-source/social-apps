.class public LX/3NS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mi;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3NW;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0SG;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/03V;

.field private f:LX/3LI;

.field public g:LX/3NY;

.field public h:LX/3Mr;

.field private i:I

.field public j:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public k:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559338
    const-class v0, LX/3NS;

    sput-object v0, LX/3NS;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3NU;",
            ">;",
            "LX/0SG;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 559322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559323
    sget-object v0, LX/3Mr;->FINISHED:LX/3Mr;

    iput-object v0, p0, LX/3NS;->h:LX/3Mr;

    .line 559324
    iput v1, p0, LX/3NS;->i:I

    .line 559325
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 559326
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NU;

    .line 559327
    new-instance v5, LX/3NW;

    invoke-direct {v5, v0}, LX/3NW;-><init>(LX/3NU;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559328
    iget v5, p0, LX/3NS;->i:I

    .line 559329
    iget-boolean v6, v0, LX/3NU;->c:Z

    move v0, v6

    .line 559330
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v5

    iput v0, p0, LX/3NS;->i:I

    .line 559331
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 559332
    goto :goto_1

    .line 559333
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3NS;->b:LX/0Px;

    .line 559334
    iput-object p2, p0, LX/3NS;->c:LX/0SG;

    .line 559335
    iput-object p3, p0, LX/3NS;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 559336
    iput-object p4, p0, LX/3NS;->e:LX/03V;

    .line 559337
    return-void
.end method

.method public static a(LX/3OQ;)LX/DAR;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 559306
    instance-of v0, p0, LX/DAU;

    if-eqz v0, :cond_0

    .line 559307
    check-cast p0, LX/DAU;

    .line 559308
    iget-object v0, p0, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v0

    .line 559309
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 559310
    new-instance v1, LX/DAR;

    const-string v2, "thread_key"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, LX/DAR;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/DAR;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 559311
    :goto_0
    return-object v0

    .line 559312
    :cond_0
    instance-of v0, p0, LX/3OO;

    if-eqz v0, :cond_1

    .line 559313
    check-cast p0, LX/3OO;

    .line 559314
    iget-object v0, p0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 559315
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 559316
    invoke-static {v0}, LX/DAR;->a(Ljava/lang/String;)LX/DAR;

    move-result-object v0

    goto :goto_0

    .line 559317
    :cond_1
    instance-of v0, p0, LX/DAY;

    if-eqz v0, :cond_2

    .line 559318
    check-cast p0, LX/DAY;

    iget-object v0, p0, LX/DAY;->a:Lcom/facebook/user/model/User;

    .line 559319
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 559320
    invoke-static {v0}, LX/DAR;->a(Ljava/lang/String;)LX/DAR;

    move-result-object v0

    goto :goto_0

    .line 559321
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/3NS;LX/3NW;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 559297
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Bad results: ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 559298
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559299
    iget-object v4, v0, LX/3NW;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559300
    if-ne v0, p1, :cond_0

    .line 559301
    const-string v4, "*"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559302
    :cond_0
    const-string v4, " : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, LX/3NW;->d()LX/3Og;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559303
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559304
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559305
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/3NS;LX/3NW;Ljava/util/Set;LX/0Pz;)V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3NW;",
            "Ljava/util/Set",
            "<",
            "LX/DAR;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559271
    invoke-virtual {p1}, LX/3NW;->d()LX/3Og;

    move-result-object v0

    .line 559272
    if-eqz v0, :cond_5

    .line 559273
    iget-object v1, v0, LX/3Og;->a:LX/3Oh;

    move-object v1, v1

    .line 559274
    sget-object v2, LX/3Oh;->OK:LX/3Oh;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, LX/3NS;->j:Ljava/lang/CharSequence;

    .line 559275
    iget-object v2, v0, LX/3Og;->b:Ljava/lang/CharSequence;

    move-object v2, v2

    .line 559276
    invoke-static {v1, v2}, LX/3NS;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 559277
    iget-object v1, v0, LX/3Og;->c:LX/0Px;

    move-object v0, v1

    .line 559278
    iget-object v1, p1, LX/3NW;->c:Ljava/lang/String;

    move-object v1, v1

    .line 559279
    const/4 v5, 0x0

    .line 559280
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v7, v2

    :goto_0
    if-ge v7, v8, :cond_5

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DAQ;

    .line 559281
    const/4 v4, 0x0

    .line 559282
    iget-object v9, v2, LX/DAQ;->a:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v3, 0x0

    move v6, v3

    :goto_1
    if-ge v6, v10, :cond_4

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3OQ;

    .line 559283
    invoke-static {v3}, LX/3NS;->a(LX/3OQ;)LX/DAR;

    move-result-object p0

    .line 559284
    if-eqz p0, :cond_0

    invoke-interface {p2, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 559285
    :cond_0
    if-eqz p0, :cond_1

    .line 559286
    invoke-interface {p2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 559287
    :cond_1
    if-nez v5, :cond_2

    if-eqz v1, :cond_2

    .line 559288
    new-instance v5, LX/DAa;

    invoke-direct {v5, v1}, LX/DAa;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559289
    :cond_2
    if-nez v4, :cond_3

    iget-object v4, v2, LX/DAQ;->b:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 559290
    new-instance v4, LX/DAa;

    iget-object v5, v2, LX/DAQ;->b:Ljava/lang/String;

    invoke-direct {v4, v5}, LX/DAa;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559291
    :cond_3
    invoke-virtual {p3, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 559292
    const/4 v4, 0x1

    .line 559293
    const/4 v3, 0x1

    .line 559294
    :goto_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    move v4, v3

    goto :goto_1

    .line 559295
    :cond_4
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_0

    .line 559296
    :cond_5
    return-void

    :cond_6
    move v3, v4

    move v4, v5

    goto :goto_2
.end method

.method public static b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 2
    .param p0    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 559267
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 559268
    :cond_0
    :goto_0
    return v0

    .line 559269
    :cond_1
    if-eqz p1, :cond_0

    .line 559270
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d$redex0(LX/3NS;)V
    .locals 3

    .prologue
    .line 559253
    invoke-direct {p0}, LX/3NS;->e()LX/3Og;

    move-result-object v0

    .line 559254
    if-eqz v0, :cond_0

    .line 559255
    iget-object v1, p0, LX/3NS;->f:LX/3LI;

    iget-object v2, p0, LX/3NS;->j:Ljava/lang/CharSequence;

    invoke-interface {v1, v2, v0}, LX/3LI;->a(Ljava/lang/CharSequence;LX/3Og;)V

    .line 559256
    iget-object v1, p0, LX/3NS;->g:LX/3NY;

    if-eqz v1, :cond_0

    .line 559257
    sget-object v1, LX/3Or;->a:[I

    .line 559258
    iget-object v2, v0, LX/3Og;->a:LX/3Oh;

    move-object v2, v2

    .line 559259
    invoke-virtual {v2}, LX/3Oh;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 559260
    const/4 v0, 0x0

    .line 559261
    :goto_0
    iget-object v1, p0, LX/3NS;->g:LX/3NY;

    invoke-interface {v1, v0}, LX/3NY;->a(I)V

    .line 559262
    :cond_0
    return-void

    .line 559263
    :pswitch_0
    const/4 v0, -0x1

    .line 559264
    goto :goto_0

    .line 559265
    :pswitch_1
    iget v1, v0, LX/3Og;->d:I

    move v0, v1

    .line 559266
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()LX/3Og;
    .locals 18

    .prologue
    .line 559181
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->b:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3NW;

    invoke-virtual {v2}, LX/3NW;->d()LX/3Og;

    move-result-object v2

    .line 559182
    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3NS;->j:Ljava/lang/CharSequence;

    invoke-virtual {v2}, LX/3Og;->b()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, LX/3NS;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 559183
    :cond_0
    const/4 v2, 0x0

    .line 559184
    :cond_1
    :goto_0
    return-object v2

    .line 559185
    :cond_2
    invoke-virtual {v2}, LX/3Og;->a()LX/3Oh;

    move-result-object v3

    sget-object v4, LX/3Oh;->EMPTY_CONSTRAINT:LX/3Oh;

    if-eq v3, v4, :cond_1

    .line 559186
    invoke-virtual {v2}, LX/3Og;->a()LX/3Oh;

    move-result-object v3

    sget-object v4, LX/3Oh;->EXCEPTION:LX/3Oh;

    if-eq v3, v4, :cond_1

    .line 559187
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v6

    .line 559188
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 559189
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v8

    .line 559190
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->k:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_3

    .line 559191
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 559192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LX/3NS;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 559193
    :cond_3
    const/4 v4, 0x0

    .line 559194
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v9, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3NW;

    .line 559195
    invoke-virtual {v2}, LX/3NW;->e()LX/3Mr;

    move-result-object v3

    sget-object v10, LX/3Mr;->FINISHED:LX/3Mr;

    if-eq v3, v10, :cond_4

    invoke-virtual {v2}, LX/3NW;->h()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    invoke-virtual {v2}, LX/3NW;->f()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v3, v10, v12

    if-nez v3, :cond_8

    .line 559196
    :cond_5
    invoke-virtual {v2}, LX/3NW;->e()LX/3Mr;

    move-result-object v3

    sget-object v10, LX/3Mr;->FINISHED:LX/3Mr;

    if-ne v3, v10, :cond_6

    invoke-virtual {v2}, LX/3NW;->f()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v3, v10, v12

    if-nez v3, :cond_6

    .line 559197
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3NS;->e:LX/03V;

    const-string v10, "ContactPickerMergedFilter"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3NS;->a(LX/3NS;LX/3NW;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 559198
    :cond_6
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559199
    invoke-virtual {v2}, LX/3NW;->c()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 559200
    iget-object v2, v2, LX/3NW;->a:Ljava/lang/String;

    move-object v3, v2

    .line 559201
    :goto_2
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 559202
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3NW;

    .line 559203
    move-object/from16 v0, p0

    invoke-static {v0, v2, v6, v7}, LX/3NS;->a(LX/3NS;LX/3NW;Ljava/util/Set;LX/0Pz;)V

    .line 559204
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, LX/3NS;->i:I

    const/4 v4, 0x1

    if-le v2, v4, :cond_d

    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    if-eqz v3, :cond_d

    .line 559205
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 559206
    :cond_8
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    .line 559207
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 559208
    invoke-virtual {v2}, LX/3NW;->f()J

    move-result-wide v10

    .line 559209
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3NS;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v12

    .line 559210
    sub-long v14, v12, v10

    const-wide/16 v16, 0xfa0

    cmp-long v3, v14, v16

    if-ltz v3, :cond_c

    .line 559211
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3NW;

    .line 559212
    invoke-virtual {v3}, LX/3NW;->g()V

    .line 559213
    move-object/from16 v0, p0

    invoke-static {v0, v3, v6, v7}, LX/3NS;->a(LX/3NS;LX/3NW;Ljava/util/Set;LX/0Pz;)V

    goto :goto_3

    .line 559214
    :cond_9
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 559215
    :cond_a
    move-object/from16 v0, p0

    invoke-static {v0, v2, v6, v7}, LX/3NS;->a(LX/3NS;LX/3NW;Ljava/util/Set;LX/0Pz;)V

    .line 559216
    :cond_b
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_1

    .line 559217
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/contacts/picker/ContactPickerMergedFilter$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/facebook/contacts/picker/ContactPickerMergedFilter$3;-><init>(LX/3NS;)V

    const-wide/16 v14, 0xfa0

    sub-long v10, v12, v10

    sub-long v10, v14, v10

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v10, v11, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/3NS;->k:Ljava/util/concurrent/ScheduledFuture;

    move-object v3, v4

    .line 559218
    goto/16 :goto_2

    .line 559219
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3NS;->j:Ljava/lang/CharSequence;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v2

    goto/16 :goto_0

    :cond_e
    move-object v3, v4

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 559249
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559250
    iget-object v0, v0, LX/3NW;->b:LX/3Mi;

    invoke-interface {v0}, LX/3Mi;->a()V

    .line 559251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559252
    :cond_0
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559244
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559245
    iget-object v3, v0, LX/3NW;->b:LX/3Mi;

    move-object v0, v3

    .line 559246
    invoke-interface {v0, p1}, LX/3Mi;->a(LX/0Px;)V

    .line 559247
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559248
    :cond_0
    return-void
.end method

.method public final a(LX/3LI;)V
    .locals 5

    .prologue
    .line 559238
    iput-object p1, p0, LX/3NS;->f:LX/3LI;

    .line 559239
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559240
    iget-object v3, v0, LX/3NW;->b:LX/3Mi;

    move-object v3, v3

    .line 559241
    new-instance v4, LX/3NZ;

    invoke-direct {v4, p0, v0}, LX/3NZ;-><init>(LX/3NS;LX/3NW;)V

    invoke-interface {v3, v4}, LX/3Mi;->a(LX/3LI;)V

    .line 559242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559243
    :cond_0
    return-void
.end method

.method public final a(LX/3Md;)V
    .locals 4

    .prologue
    .line 559233
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559234
    iget-object v3, v0, LX/3NW;->b:LX/3Mi;

    move-object v0, v3

    .line 559235
    invoke-interface {v0, p1}, LX/3Mi;->a(LX/3Md;)V

    .line 559236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559237
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 559231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/3NS;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 559232
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/3NY;)V
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 559221
    iput-object p2, p0, LX/3NS;->g:LX/3NY;

    .line 559222
    iput-object p1, p0, LX/3NS;->j:Ljava/lang/CharSequence;

    .line 559223
    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3NW;

    .line 559224
    new-instance v3, LX/3Od;

    invoke-direct {v3, p0, v0}, LX/3Od;-><init>(LX/3NS;LX/3NW;)V

    .line 559225
    const/4 v5, 0x0

    iput-boolean v5, v0, LX/3NW;->g:Z

    .line 559226
    const/4 v5, 0x0

    iput-object v5, v0, LX/3NW;->h:LX/3Og;

    .line 559227
    const-wide/16 v5, -0x1

    iput-wide v5, v0, LX/3NW;->i:J

    .line 559228
    iget-object v4, v0, LX/3NW;->b:LX/3Mi;

    invoke-interface {v4, p1, v3}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 559229
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 559230
    :cond_0
    return-void
.end method

.method public final b()LX/3Mr;
    .locals 1

    .prologue
    .line 559220
    iget-object v0, p0, LX/3NS;->h:LX/3Mr;

    return-object v0
.end method
