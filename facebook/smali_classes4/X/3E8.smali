.class public LX/3E8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/3E8;


# instance fields
.field private final b:LX/3GN;

.field public final c:LX/2gU;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/09G;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2Tm;

.field public h:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3E8;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2S7;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 537028
    const-class v0, LX/3E8;

    sput-object v0, LX/3E8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3GN;LX/2gU;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/interfaces/RtcAppSignalingHandler;",
            "LX/2gU;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/09G;",
            ">;",
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537072
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 537073
    iput-object v0, p0, LX/3E8;->h:LX/0Ot;

    .line 537074
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 537075
    iput-object v0, p0, LX/3E8;->i:LX/0Ot;

    .line 537076
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 537077
    iput-object v0, p0, LX/3E8;->j:LX/0Ot;

    .line 537078
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 537079
    iput-object v0, p0, LX/3E8;->k:LX/0Ot;

    .line 537080
    iput-object p1, p0, LX/3E8;->b:LX/3GN;

    .line 537081
    iput-object p2, p0, LX/3E8;->c:LX/2gU;

    .line 537082
    iput-object p3, p0, LX/3E8;->e:LX/0Or;

    .line 537083
    iput-object p4, p0, LX/3E8;->f:LX/0Or;

    .line 537084
    iput-object p5, p0, LX/3E8;->d:LX/0Or;

    .line 537085
    return-void
.end method

.method public static a(LX/0QB;)LX/3E8;
    .locals 9

    .prologue
    .line 537056
    sget-object v0, LX/3E8;->l:LX/3E8;

    if-nez v0, :cond_1

    .line 537057
    const-class v1, LX/3E8;

    monitor-enter v1

    .line 537058
    :try_start_0
    sget-object v0, LX/3E8;->l:LX/3E8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537059
    if-eqz v2, :cond_0

    .line 537060
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537061
    new-instance v3, LX/3E8;

    invoke-static {v0}, LX/3GN;->a(LX/0QB;)LX/3GN;

    move-result-object v4

    check-cast v4, LX/3GN;

    invoke-static {v0}, LX/2gU;->a(LX/0QB;)LX/2gU;

    move-result-object v5

    check-cast v5, LX/2gU;

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1126

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3257

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/3E8;-><init>(LX/3GN;LX/2gU;LX/0Or;LX/0Or;LX/0Or;)V

    .line 537062
    const/16 v4, 0x140f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x10e4

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1111

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x10e1

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 537063
    iput-object v4, v3, LX/3E8;->h:LX/0Ot;

    iput-object v5, v3, LX/3E8;->i:LX/0Ot;

    iput-object v6, v3, LX/3E8;->j:LX/0Ot;

    iput-object v7, v3, LX/3E8;->k:LX/0Ot;

    .line 537064
    move-object v0, v3

    .line 537065
    sput-object v0, LX/3E8;->l:LX/3E8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537066
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537067
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537068
    :cond_1
    sget-object v0, LX/3E8;->l:LX/3E8;

    return-object v0

    .line 537069
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(JJ)LX/76H;
    .locals 7

    .prologue
    .line 537055
    new-instance v0, LX/EDM;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/EDM;-><init>(LX/3E8;JJ)V

    return-object v0
.end method

.method private a(JJJ)V
    .locals 9

    .prologue
    .line 537051
    iget-object v0, p0, LX/3E8;->g:LX/2Tm;

    if-eqz v0, :cond_0

    .line 537052
    invoke-static {p0}, LX/3E8;->b$redex0(LX/3E8;)V

    .line 537053
    iget-object v0, p0, LX/3E8;->g:LX/2Tm;

    const/4 v1, 0x0

    const-string v6, "thrift serialization error"

    const/4 v7, -0x1

    const-string v8, "client"

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v8}, LX/2Tm;->a(Ljava/lang/String;JJLjava/lang/String;ILjava/lang/String;)V

    .line 537054
    :cond_0
    return-void
.end method

.method private a(JJJLjava/lang/String;)Z
    .locals 5

    .prologue
    .line 537040
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 537041
    const-string v1, "to"

    invoke-virtual {v0, v1, p1, p2}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 537042
    const-string v1, "payload"

    invoke-virtual {v0, v1, p7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 537043
    const-string v1, "id"

    invoke-virtual {v0, v1, p5, p6}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 537044
    const-string v1, "fbtrace_meta"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 537045
    invoke-direct {p0, p3, p4, p5, p6}, LX/3E8;->a(JJ)LX/76H;

    move-result-object v1

    .line 537046
    iget-object v2, p0, LX/3E8;->c:LX/2gU;

    const-string v3, "/webrtc"

    sget-object v4, LX/2I2;->ACKNOWLEDGED_DELIVERY:LX/2I2;

    invoke-virtual {v2, v3, v0, v4, v1}, LX/2gU;->a(Ljava/lang/String;LX/0lF;LX/2I2;LX/76H;)I

    move-result v0

    .line 537047
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 537048
    invoke-direct/range {p0 .. p6}, LX/3E8;->b(JJJ)V

    .line 537049
    const/4 v0, 0x0

    .line 537050
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(JJJ[B)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 537029
    new-instance v1, LX/3ll;

    const-string v2, ""

    invoke-direct {v1, v2}, LX/3ll;-><init>(Ljava/lang/String;)V

    .line 537030
    :try_start_0
    new-instance v2, LX/1so;

    new-instance v3, LX/1sp;

    invoke-direct {v3}, LX/1sp;-><init>()V

    invoke-direct {v2, v3}, LX/1so;-><init>(LX/1sq;)V

    .line 537031
    invoke-virtual {v2, v1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 537032
    invoke-static {v1, p7}, LX/3E8;->a([B[B)[B

    move-result-object v1

    .line 537033
    invoke-direct {p0, p3, p4, p5, p6}, LX/3E8;->a(JJ)LX/76H;

    move-result-object v2

    .line 537034
    iget-object v3, p0, LX/3E8;->c:LX/2gU;

    const-string v4, "/t_rtc"

    sget-object v5, LX/2I2;->ACKNOWLEDGED_DELIVERY:LX/2I2;

    invoke-virtual {v3, v4, v1, v5, v2}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 537035
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 537036
    invoke-direct/range {p0 .. p6}, LX/3E8;->b(JJJ)V

    .line 537037
    :goto_0
    return v0

    .line 537038
    :catch_0
    invoke-direct/range {p0 .. p6}, LX/3E8;->a(JJJ)V

    goto :goto_0

    .line 537039
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a([B[B)[B
    .locals 4

    .prologue
    .line 537025
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 537026
    const/4 v1, 0x0

    array-length v2, p0

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 537027
    return-object v0
.end method

.method private b(JJJ)V
    .locals 9

    .prologue
    .line 537086
    iget-object v0, p0, LX/3E8;->g:LX/2Tm;

    if-eqz v0, :cond_0

    .line 537087
    invoke-static {p0}, LX/3E8;->b$redex0(LX/3E8;)V

    .line 537088
    iget-object v0, p0, LX/3E8;->g:LX/2Tm;

    const/4 v1, 0x0

    const-string v6, "Mqtt not available"

    const/4 v7, -0x1

    const-string v8, "MQTT"

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v8}, LX/2Tm;->a(Ljava/lang/String;JJLjava/lang/String;ILjava/lang/String;)V

    .line 537089
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/3E8;)V
    .locals 7

    .prologue
    .line 537022
    iget-object v0, p0, LX/3E8;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 537023
    iget-object v0, p0, LX/3E8;->g:LX/2Tm;

    iget-object v2, p0, LX/3E8;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/webrtc/IWebrtcConfigInterface;

    iget-object v2, p0, LX/3E8;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    iget-object v2, p0, LX/3E8;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

    move-object v2, v1

    move-object v3, v1

    invoke-virtual/range {v0 .. v6}, LX/2Tm;->a(Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;LX/EDx;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;)V

    .line 537024
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 537018
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "voip_camp_on/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 537019
    iget-object v0, p0, LX/3E8;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/09G;

    new-instance v2, LX/EDK;

    invoke-direct {v2, p0}, LX/EDK;-><init>(LX/3E8;)V

    invoke-virtual {v0, v1, v2}, LX/09G;->a(Ljava/lang/String;LX/0TF;)V

    .line 537020
    iget-object v0, p0, LX/3E8;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcSignalingSender$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcSignalingSender$1;-><init>(LX/3E8;J)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 537021
    return-void
.end method

.method public final sendMultiwaySignalingMessage(Ljava/lang/String;Ljava/lang/String;[B)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 536987
    new-instance v1, LX/3ll;

    const-string v2, ""

    invoke-direct {v1, v2}, LX/3ll;-><init>(Ljava/lang/String;)V

    .line 536988
    :try_start_0
    new-instance v2, LX/1so;

    new-instance v3, LX/1sp;

    invoke-direct {v3}, LX/1sp;-><init>()V

    invoke-direct {v2, v3}, LX/1so;-><init>(LX/1sq;)V

    .line 536989
    invoke-virtual {v2, v1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 536990
    invoke-static {v1, p3}, LX/3E8;->a([B[B)[B

    move-result-object v1

    .line 536991
    new-instance v2, LX/EDN;

    invoke-direct {v2, p0, p2, p1}, LX/EDN;-><init>(LX/3E8;Ljava/lang/String;Ljava/lang/String;)V

    .line 536992
    iget-object v3, p0, LX/3E8;->c:LX/2gU;

    const-string v4, "/t_rtc_multi"

    sget-object v5, LX/2I2;->ACKNOWLEDGED_DELIVERY:LX/2I2;

    invoke-virtual {v3, v4, v1, v5, v2}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 536993
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 536994
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 536995
    :catch_0
    goto :goto_0
.end method

.method public final sendOfferToPeer(JJJ[B)Z
    .locals 11

    .prologue
    .line 537009
    iget-object v2, p0, LX/3E8;->b:LX/3GN;

    if-eqz v2, :cond_0

    .line 537010
    iget-object v2, p0, LX/3E8;->b:LX/3GN;

    invoke-virtual {v2}, LX/3GN;->a()LX/EGJ;

    move-result-object v2

    .line 537011
    if-eqz v2, :cond_0

    .line 537012
    :try_start_0
    new-instance v3, LX/1so;

    new-instance v4, LX/1sp;

    invoke-direct {v4}, LX/1sp;-><init>()V

    invoke-direct {v3, v4}, LX/1so;-><init>(LX/1sq;)V

    .line 537013
    invoke-virtual {v3, v2}, LX/1so;->a(LX/1u2;)[B

    move-result-object v2

    .line 537014
    move-object/from16 v0, p7

    invoke-static {v0, v2}, LX/3E8;->a([B[B)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p7

    move-object/from16 v10, p7

    :goto_0
    move-object v3, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    .line 537015
    invoke-virtual/range {v3 .. v10}, LX/3E8;->sendThriftToPeer(JJJ[B)Z

    move-result v2

    :goto_1
    return v2

    .line 537016
    :catch_0
    invoke-direct/range {p0 .. p6}, LX/3E8;->a(JJJ)V

    .line 537017
    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    move-object/from16 v10, p7

    goto :goto_0
.end method

.method public final sendThriftToPeer(JJJ[B)Z
    .locals 1

    .prologue
    .line 537008
    invoke-direct/range {p0 .. p7}, LX/3E8;->a(JJJ[B)Z

    move-result v0

    return v0
.end method

.method public final sendThriftToSelf(JJ[B)Z
    .locals 9

    .prologue
    .line 537003
    const-wide/16 v2, 0x0

    .line 537004
    iget-object v0, p0, LX/3E8;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 537005
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 537006
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    :cond_0
    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    .line 537007
    invoke-direct/range {v1 .. v8}, LX/3E8;->a(JJJ[B)Z

    move-result v0

    return v0
.end method

.method public final sendToPeer(JJJLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 537002
    invoke-direct/range {p0 .. p7}, LX/3E8;->a(JJJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final sendToSelf(JJLjava/lang/String;)Z
    .locals 9

    .prologue
    .line 536998
    iget-object v0, p0, LX/3E8;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 536999
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 537000
    const/4 v0, 0x0

    .line 537001
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, LX/3E8;->a(JJJLjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final setWebrtcManager(LX/2Tm;)V
    .locals 0

    .prologue
    .line 536996
    iput-object p1, p0, LX/3E8;->g:LX/2Tm;

    .line 536997
    return-void
.end method
