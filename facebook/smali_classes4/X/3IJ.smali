.class public final LX/3IJ;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 546016
    iput-object p1, p0, LX/3IJ;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546017
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 546018
    iget-object v0, p0, LX/3IJ;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_0

    .line 546019
    :goto_0
    :pswitch_0
    return-void

    .line 546020
    :cond_0
    sget-object v0, LX/7Nm;->a:[I

    iget-object v1, p0, LX/3IJ;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    .line 546021
    iget-object p1, v1, LX/2pb;->y:LX/2qV;

    move-object v1, p1

    .line 546022
    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 546023
    iget-object v0, p0, LX/3IJ;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 p1, 0x0

    .line 546024
    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->N:Z

    if-nez v1, :cond_1

    .line 546025
    const-string v1, "V360"

    const-string v2, "Video360Plugin id:%d pauseRendering()"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v3, p1

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 546026
    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1}, LX/2qW;->c()V

    .line 546027
    invoke-static {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->x(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 546028
    iput-boolean p1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->G:Z

    .line 546029
    :cond_1
    iget-object v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    invoke-virtual {v1}, LX/3IP;->a()V

    .line 546030
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
