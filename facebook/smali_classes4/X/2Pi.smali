.class public LX/2Pi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Pi;


# instance fields
.field private a:LX/1Ei;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ei",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/CSH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406981
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/2Pi;->a:LX/1Ei;

    .line 406982
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Pi;->b:Ljava/util/HashMap;

    .line 406983
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pi;
    .locals 3

    .prologue
    .line 406968
    sget-object v0, LX/2Pi;->c:LX/2Pi;

    if-nez v0, :cond_1

    .line 406969
    const-class v1, LX/2Pi;

    monitor-enter v1

    .line 406970
    :try_start_0
    sget-object v0, LX/2Pi;->c:LX/2Pi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406971
    if-eqz v2, :cond_0

    .line 406972
    :try_start_1
    new-instance v0, LX/2Pi;

    invoke-direct {v0}, LX/2Pi;-><init>()V

    .line 406973
    move-object v0, v0

    .line 406974
    sput-object v0, LX/2Pi;->c:LX/2Pi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406977
    :cond_1
    sget-object v0, LX/2Pi;->c:LX/2Pi;

    return-object v0

    .line 406978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 406964
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 406965
    iget-object v0, p0, LX/2Pi;->a:LX/1Ei;

    invoke-virtual {v0, p1}, LX/1Ei;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406966
    monitor-exit p0

    return-void

    .line 406967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Z[B)V
    .locals 2

    .prologue
    .line 406960
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->a:LX/1Ei;

    invoke-virtual {v0, p2, p1}, LX/1Ei;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406961
    iget-object v0, p0, LX/2Pi;->b:Ljava/util/HashMap;

    new-instance v1, LX/CSH;

    invoke-direct {v1, p2, p1, p3, p4}, LX/CSH;-><init>(Ljava/lang/String;Ljava/lang/String;Z[B)V

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406962
    monitor-exit p0

    return-void

    .line 406963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/CSH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 406955
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSH;

    .line 406956
    iget-object v2, p0, LX/2Pi;->b:Ljava/util/HashMap;

    iget-object v3, v0, LX/CSH;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406957
    iget-object v2, p0, LX/2Pi;->a:LX/1Ei;

    iget-object v3, v0, LX/CSH;->a:Ljava/lang/String;

    iget-object v0, v0, LX/CSH;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/1Ei;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 406958
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 406959
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->a:LX/1Ei;

    invoke-virtual {v0, p1}, LX/1Ei;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406943
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->a:LX/1Ei;

    invoke-virtual {v0}, LX/1Ei;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 406949
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406950
    if-nez v0, :cond_0

    .line 406951
    const/4 v0, 0x0

    .line 406952
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, v0, LX/CSH;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406953
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 406944
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Pi;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406945
    if-nez v0, :cond_0

    .line 406946
    const/4 v0, 0x0

    .line 406947
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, v0, LX/CSH;->d:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406948
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
