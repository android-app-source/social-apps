.class public LX/3ML;
.super LX/0Tv;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554306
    const-string v0, "smstakeover_schema"

    const/16 v1, 0x10

    new-instance v2, LX/3MM;

    invoke-direct {v2}, LX/3MM;-><init>()V

    new-instance v3, LX/3MJ;

    invoke-direct {v3}, LX/3MJ;-><init>()V

    new-instance v4, LX/3MN;

    invoke-direct {v4}, LX/3MN;-><init>()V

    new-instance v5, LX/3MO;

    invoke-direct {v5}, LX/3MO;-><init>()V

    new-instance v6, LX/3MP;

    invoke-direct {v6}, LX/3MP;-><init>()V

    invoke-static {v2, v3, v4, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 554307
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 554308
    :goto_0
    if-ge p2, p3, :cond_1

    .line 554309
    add-int/lit8 v0, p2, 0x1

    .line 554310
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    .line 554311
    const-string v1, "ALTER TABLE thread_read_stat RENAME TO threads_table"

    const p2, 0x321c416e

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x4cee49cd

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554312
    :cond_0
    :goto_1
    move p2, v0

    .line 554313
    goto :goto_0

    .line 554314
    :cond_1
    return-void

    .line 554315
    :cond_2
    const/4 v1, 0x3

    if-ne p2, v1, :cond_3

    .line 554316
    const-string v1, "threads_table"

    sget-object p2, LX/3MJ;->b:LX/0U1;

    invoke-static {v1, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p2, 0x6c48bec2

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x17939525

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554317
    goto :goto_1

    .line 554318
    :cond_3
    const/4 v1, 0x4

    if-eq p2, v1, :cond_0

    .line 554319
    const/4 v1, 0x5

    if-ne p2, v1, :cond_4

    .line 554320
    const-string v1, "block_table"

    sget-object p0, LX/3MN;->c:LX/0Px;

    sget-object p2, LX/3MN;->d:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x7431fca9

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x4f12b6f7

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554321
    goto :goto_1

    .line 554322
    :cond_4
    const/4 v1, 0x6

    if-ne p2, v1, :cond_5

    .line 554323
    const-string v1, "server_spam_list"

    sget-object p0, LX/3MO;->b:LX/0Px;

    sget-object p2, LX/3MO;->c:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x38a9957f

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x29c5ebd9

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554324
    goto :goto_1

    .line 554325
    :cond_5
    const/4 v1, 0x7

    if-ne p2, v1, :cond_6

    .line 554326
    const-string v1, "threads_table"

    invoke-static {v1}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x9a6019e

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5c374ca8    # -2.1759998E-17f

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554327
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->j:LX/0Px;

    sget-object p2, LX/3MM;->k:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x2ddf5491

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x1f24a6a1

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554328
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->h:LX/0Px;

    sget-object p2, LX/3MJ;->i:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x782afc68

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x3814aad0

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554329
    goto/16 :goto_1

    .line 554330
    :cond_6
    const/16 v1, 0x8

    if-ne p2, v1, :cond_7

    .line 554331
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->d:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x2ea65058

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x52907370

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554332
    goto/16 :goto_1

    .line 554333
    :cond_7
    const/16 v1, 0x9

    if-ne p2, v1, :cond_8

    .line 554334
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->e:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x1ad98806

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x8c1a8ae

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554335
    goto/16 :goto_1

    .line 554336
    :cond_8
    const/16 v1, 0xa

    if-ne p2, v1, :cond_9

    .line 554337
    const-string v1, "spam_stats"

    invoke-static {v1}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x6eb6d536

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x7a21f071

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554338
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->c:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x2c0f468f

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x4faf167d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554339
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->d:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x315d11f2

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x59bfd3cc

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554340
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->e:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x40724a97

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x715e5130

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554341
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->f:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x3c57e081

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x52e6aa22

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554342
    const-string v1, "address_table"

    sget-object p0, LX/3MJ;->g:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x43446d1a

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x76aa7e5d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554343
    goto/16 :goto_1

    .line 554344
    :cond_9
    const/16 v1, 0xb

    if-ne p2, v1, :cond_a

    .line 554345
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->f:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x2ef70ee9

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x6b697a69

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554346
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->g:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x7cbeb4cf

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x3329cf54

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554347
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->h:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x2f5a971

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x1782395f

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554348
    goto/16 :goto_1

    .line 554349
    :cond_a
    const/16 v1, 0xc

    if-ne p2, v1, :cond_b

    .line 554350
    const-string v1, "threads_table"

    sget-object p0, LX/3MM;->i:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x503e2071

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x4297b3ff

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554351
    goto/16 :goto_1

    .line 554352
    :cond_b
    const/16 v1, 0xd

    if-ne p2, v1, :cond_c

    .line 554353
    const-string v1, "sms_business_address_list"

    sget-object p0, LX/3MP;->c:LX/0Px;

    sget-object p2, LX/3MP;->d:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x65c418df

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x577cb77d

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554354
    goto/16 :goto_1

    .line 554355
    :cond_c
    const/16 v1, 0xe

    if-ne p2, v1, :cond_d

    .line 554356
    const-string v1, "sms_business_address_list"

    sget-object p0, LX/3MP;->c:LX/0Px;

    sget-object p2, LX/3MP;->d:LX/0sv;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-static {v1, p0, p2}, LX/0Tz;->b(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    const p0, 0x20e4b877

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x7c4149d8

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554357
    goto/16 :goto_1

    .line 554358
    :cond_d
    const/16 v1, 0xf

    if-ne p2, v1, :cond_0

    .line 554359
    const-string v1, "sms_business_address_list"

    sget-object p0, LX/3MP;->b:LX/0U1;

    invoke-static {v1, p0}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object v1

    const p0, -0x774ba5d6

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x5dd670a7

    invoke-static {v1}, LX/03h;->a(I)V

    .line 554360
    goto/16 :goto_1
.end method
