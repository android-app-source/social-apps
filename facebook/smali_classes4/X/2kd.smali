.class public final LX/2kd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ke;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2ke",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V
    .locals 0

    .prologue
    .line 456433
    iput-object p1, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 456426
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    .line 456427
    iget-object v1, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v1, p1, p2}, LX/2iv;->b(II)V

    .line 456428
    if-eqz v0, :cond_0

    .line 456429
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 456430
    :cond_0
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456431
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-static {v0, p1}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2kM;)V

    .line 456432
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 456421
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    .line 456422
    iget-object v1, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v1, p1, p2}, LX/2iv;->a(II)V

    .line 456423
    if-eqz v0, :cond_0

    .line 456424
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->H:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 456425
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 456419
    iget-object v0, p0, LX/2kd;->a:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->g:LX/2iv;

    invoke-virtual {v0, p1, p2}, LX/2iv;->c(II)V

    .line 456420
    return-void
.end method
