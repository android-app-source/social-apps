.class public final enum LX/2RV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2RV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2RV;

.field public static final enum CONTACT:LX/2RV;

.field public static final enum USER:LX/2RV;


# instance fields
.field public final searchTableContentPath:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 409889
    new-instance v0, LX/2RV;

    const-string v1, "CONTACT"

    const-string v2, "search"

    invoke-direct {v0, v1, v3, v2}, LX/2RV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RV;->CONTACT:LX/2RV;

    .line 409890
    new-instance v0, LX/2RV;

    const-string v1, "USER"

    const-string v2, "userSearch"

    invoke-direct {v0, v1, v4, v2}, LX/2RV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RV;->USER:LX/2RV;

    .line 409891
    const/4 v0, 0x2

    new-array v0, v0, [LX/2RV;

    sget-object v1, LX/2RV;->CONTACT:LX/2RV;

    aput-object v1, v0, v3

    sget-object v1, LX/2RV;->USER:LX/2RV;

    aput-object v1, v0, v4

    sput-object v0, LX/2RV;->$VALUES:[LX/2RV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 409892
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 409893
    iput-object p3, p0, LX/2RV;->searchTableContentPath:Ljava/lang/String;

    .line 409894
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2RV;
    .locals 1

    .prologue
    .line 409895
    const-class v0, LX/2RV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2RV;

    return-object v0
.end method

.method public static values()[LX/2RV;
    .locals 1

    .prologue
    .line 409896
    sget-object v0, LX/2RV;->$VALUES:[LX/2RV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2RV;

    return-object v0
.end method
