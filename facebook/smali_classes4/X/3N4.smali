.class public LX/3N4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/3N5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3N6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558177
    return-void
.end method

.method public static a(LX/0QB;)LX/3N4;
    .locals 5

    .prologue
    .line 558178
    const-class v1, LX/3N4;

    monitor-enter v1

    .line 558179
    :try_start_0
    sget-object v0, LX/3N4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 558180
    sput-object v2, LX/3N4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 558181
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558182
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 558183
    new-instance p0, LX/3N4;

    invoke-direct {p0}, LX/3N4;-><init>()V

    .line 558184
    invoke-static {v0}, LX/3N5;->a(LX/0QB;)LX/3N5;

    move-result-object v3

    check-cast v3, LX/3N5;

    invoke-static {v0}, LX/3N6;->a(LX/0QB;)LX/3N6;

    move-result-object v4

    check-cast v4, LX/3N6;

    .line 558185
    iput-object v3, p0, LX/3N4;->a:LX/3N5;

    iput-object v4, p0, LX/3N4;->b:LX/3N6;

    .line 558186
    move-object v0, p0

    .line 558187
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 558188
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3N4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558189
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 558190
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 558157
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v1

    invoke-static {v1}, LX/3N6;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Landroid/net/Uri;

    move-result-object v0

    .line 558158
    if-eqz v0, :cond_0

    .line 558159
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->d(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    .line 558160
    :goto_0
    return-object v0

    .line 558161
    :cond_0
    iget-object v0, p0, LX/3N4;->a:LX/3N5;

    invoke-virtual {v0}, LX/3N5;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 558162
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$StickerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$StickerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$StickerModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 558163
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$StickerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$StickerModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 558164
    invoke-static {v0}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 558165
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 558166
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->dd_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    invoke-static {v0}, LX/DgF;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Landroid/net/Uri;

    move-result-object v0

    .line 558167
    if-eqz v0, :cond_2

    .line 558168
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 558169
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;

    .line 558170
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 558171
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageVideoAttachmentModel$StreamingImageThumbnailModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 558172
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 558173
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$LastMessageModel$NodesModel$BlobAttachmentsModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ImageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto/16 :goto_0

    .line 558174
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 558175
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 558128
    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v1}, LX/3N6;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Landroid/net/Uri;

    move-result-object v0

    .line 558129
    if-eqz v0, :cond_1

    .line 558130
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->d(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    .line 558131
    :cond_0
    :goto_0
    return-object v0

    .line 558132
    :cond_1
    iget-object v0, p0, LX/3N4;->a:LX/3N5;

    invoke-virtual {v0}, LX/3N5;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558133
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 558134
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    .line 558135
    :goto_1
    move-object v0, v0

    .line 558136
    if-nez v0, :cond_0

    .line 558137
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 558138
    invoke-static {p2}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v2

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gez v2, :cond_8

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 558139
    if-eqz v0, :cond_2

    .line 558140
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    goto :goto_0

    .line 558141
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 558142
    :cond_3
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/DgF;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Landroid/net/Uri;

    move-result-object v0

    .line 558143
    if-eqz v0, :cond_4

    .line 558144
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_1

    .line 558145
    :cond_4
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 558146
    iget-object p0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-eqz p0, :cond_5

    .line 558147
    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 558148
    iget-object v1, v0, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    move-object v0, v1

    .line 558149
    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_1

    .line 558150
    :cond_5
    iget-object p0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz p0, :cond_6

    iget-object p0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object p0, p0, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    if-eqz p0, :cond_6

    .line 558151
    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v0, v0, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    sget-object p0, LX/5dQ;->SMALL_PREVIEW:LX/5dQ;

    invoke-virtual {v0, p0}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a(LX/5dQ;)Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v0

    .line 558152
    if-eqz v0, :cond_6

    .line 558153
    iget-object v1, v0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object v0, v1

    .line 558154
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_1

    .line 558155
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 558156
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method
