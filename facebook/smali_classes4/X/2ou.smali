.class public LX/2ou;
.super LX/2ol;
.source ""


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/2qV;

.field public final c:LX/04g;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/2qV;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 467337
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2ou;-><init>(Ljava/lang/String;LX/2qV;LX/04g;)V

    .line 467338
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/2qV;LX/04g;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 467331
    invoke-direct {p0}, LX/2ol;-><init>()V

    .line 467332
    iput-object p1, p0, LX/2ou;->a:Ljava/lang/String;

    .line 467333
    iput-object p2, p0, LX/2ou;->b:LX/2qV;

    .line 467334
    iput-object p3, p0, LX/2ou;->c:LX/04g;

    .line 467335
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 467336
    const-string v0, "%s: %s - %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/2ou;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LX/2ou;->b:LX/2qV;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
