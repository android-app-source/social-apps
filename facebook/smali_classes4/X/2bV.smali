.class public LX/2bV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/2bV;


# instance fields
.field private final b:LX/0Zb;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0xB;

.field public final e:Lcom/facebook/notifications/jewel/JewelCountHelper;

.field private final f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final g:LX/1rj;

.field public final h:LX/0lC;

.field private final i:LX/1ro;

.field public final j:LX/2c4;

.field private final k:LX/1rU;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 436835
    const-class v0, LX/2bV;

    sput-object v0, LX/2bV;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0xB;Lcom/facebook/notifications/jewel/JewelCountHelper;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1ri;LX/0lC;LX/1ro;LX/2c4;LX/1rU;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0xB;",
            "Lcom/facebook/notifications/jewel/JewelCountHelper;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/1ri;",
            "LX/0lC;",
            "LX/1ro;",
            "LX/2c4;",
            "LX/1rU;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 436821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 436822
    iput-object p1, p0, LX/2bV;->b:LX/0Zb;

    .line 436823
    iput-object p2, p0, LX/2bV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 436824
    iput-object p3, p0, LX/2bV;->d:LX/0xB;

    .line 436825
    iput-object p4, p0, LX/2bV;->e:Lcom/facebook/notifications/jewel/JewelCountHelper;

    .line 436826
    iput-object p5, p0, LX/2bV;->f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 436827
    iget-object v0, p6, LX/1ri;->a:LX/1rj;

    move-object v0, v0

    .line 436828
    iput-object v0, p0, LX/2bV;->g:LX/1rj;

    .line 436829
    iput-object p7, p0, LX/2bV;->h:LX/0lC;

    .line 436830
    iput-object p8, p0, LX/2bV;->i:LX/1ro;

    .line 436831
    iput-object p9, p0, LX/2bV;->j:LX/2c4;

    .line 436832
    iput-object p10, p0, LX/2bV;->k:LX/1rU;

    .line 436833
    iput-object p11, p0, LX/2bV;->l:LX/0Or;

    .line 436834
    return-void
.end method

.method public static a(LX/0QB;)LX/2bV;
    .locals 15

    .prologue
    .line 436808
    sget-object v0, LX/2bV;->m:LX/2bV;

    if-nez v0, :cond_1

    .line 436809
    const-class v1, LX/2bV;

    monitor-enter v1

    .line 436810
    :try_start_0
    sget-object v0, LX/2bV;->m:LX/2bV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 436811
    if-eqz v2, :cond_0

    .line 436812
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 436813
    new-instance v3, LX/2bV;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v6

    check-cast v6, LX/0xB;

    invoke-static {v0}, Lcom/facebook/notifications/jewel/JewelCountHelper;->a(LX/0QB;)Lcom/facebook/notifications/jewel/JewelCountHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v8

    check-cast v8, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {v0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v9

    check-cast v9, LX/1ri;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v10

    check-cast v10, LX/0lC;

    invoke-static {v0}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v11

    check-cast v11, LX/1ro;

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v12

    check-cast v12, LX/2c4;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v13

    check-cast v13, LX/1rU;

    const/16 v14, 0x19e

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/2bV;-><init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0xB;Lcom/facebook/notifications/jewel/JewelCountHelper;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1ri;LX/0lC;LX/1ro;LX/2c4;LX/1rU;LX/0Or;)V

    .line 436814
    move-object v0, v3

    .line 436815
    sput-object v0, LX/2bV;->m:LX/2bV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 436816
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 436817
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 436818
    :cond_1
    sget-object v0, LX/2bV;->m:LX/2bV;

    return-object v0

    .line 436819
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 436820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2bV;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 436801
    iget-object v0, p0, LX/2bV;->k:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 436802
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 436803
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 436804
    new-instance v3, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    invoke-direct {v3, v0, p2}, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 436805
    :cond_0
    iget-object v0, p0, LX/2bV;->i:LX/1ro;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ro;->a(LX/0Px;)V

    .line 436806
    :goto_1
    return-void

    .line 436807
    :cond_1
    iget-object v0, p0, LX/2bV;->f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Z)I

    goto :goto_1
.end method

.method public static e(LX/2bV;Ljava/lang/String;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 436786
    :try_start_0
    iget-object v1, p0, LX/2bV;->h:LX/0lC;

    invoke-virtual {v1, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 436787
    const-string v2, "graphql_ids"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 436788
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0lF;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 436789
    :cond_0
    :goto_0
    return-object v0

    .line 436790
    :cond_1
    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 436791
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 436792
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 436793
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 436794
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 436795
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 436796
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 436797
    :catch_0
    move-exception v1

    .line 436798
    sget-object v2, LX/2bV;->a:Ljava/lang/Class;

    const-string v3, "Failed to read mqtt message"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 436799
    :catch_1
    move-exception v1

    .line 436800
    sget-object v2, LX/2bV;->a:Ljava/lang/Class;

    const-string v3, "Failed to read graphql ids in mqtt message"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static f(LX/2bV;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 436782
    iget-object v0, p0, LX/2bV;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 436783
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436784
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 436785
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 436736
    const-string v0, "/notifications_sync"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SYNC:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 436737
    :cond_0
    const/4 v0, 0x2

    .line 436738
    :goto_0
    move v0, v0

    .line 436739
    iget-object v1, p0, LX/2bV;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 436740
    :cond_1
    :goto_1
    return-void

    .line 436741
    :cond_2
    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 436742
    :pswitch_0
    invoke-static {p0, p2}, LX/2bV;->e(LX/2bV;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 436743
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 436744
    :goto_2
    goto :goto_1

    .line 436745
    :pswitch_1
    invoke-static {p0, p2}, LX/2bV;->e(LX/2bV;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 436746
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 436747
    :goto_3
    goto :goto_1

    .line 436748
    :pswitch_2
    :try_start_0
    const-string v2, "mqtt_notifications_sync_received"

    invoke-static {p0, v2}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436749
    iget-object v2, p0, LX/2bV;->h:LX/0lC;

    invoke-virtual {v2, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 436750
    const-string v3, "u"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 436751
    const-string v3, "mqtt_notifications_unseen_count"

    invoke-static {p0, v3}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436752
    const-string v3, "u"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    .line 436753
    iget-object v4, p0, LX/2bV;->d:LX/0xB;

    sget-object v5, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v4, v5, v3}, LX/0xB;->a(LX/12j;I)V

    .line 436754
    :cond_3
    const-string v3, "n"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 436755
    const-string v3, "i"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const-wide/16 v4, 0x7530

    invoke-static {v3, v4, v5}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v4

    .line 436756
    iget-object v3, p0, LX/2bV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v6, LX/2uc;->b:LX/0Tn;

    invoke-interface {v3, v6, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 436757
    const-string v3, "mqtt_notifications_sync_new"

    invoke-static {p0, v3}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436758
    iget-object v4, p0, LX/2bV;->g:LX/1rj;

    iget-object v3, p0, LX/2bV;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    sget-object v5, LX/2ub;->MQTT_NEW:LX/2ub;

    const/4 v6, 0x0

    invoke-interface {v4, v3, v5, v6}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 436759
    :cond_4
    const-string v3, "s"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 436760
    const-string v3, "mqtt_notifications_sync_full"

    invoke-static {p0, v3}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436761
    iget-object v4, p0, LX/2bV;->g:LX/1rj;

    iget-object v3, p0, LX/2bV;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    sget-object v5, LX/2ub;->MQTT_FULL:LX/2ub;

    const/4 v6, 0x0

    invoke-interface {v4, v3, v5, v6}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 436762
    :cond_5
    const-string v3, "f"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 436763
    const-string v3, "mqtt_notifications_fetch_counts"

    invoke-static {p0, v3}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436764
    iget-object v3, p0, LX/2bV;->e:Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-virtual {v3}, Lcom/facebook/notifications/jewel/JewelCountHelper;->a()V

    .line 436765
    :cond_6
    const-string v3, "r"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 436766
    const-string v2, "mqtt_notifications_fetch_seen_state"

    invoke-static {p0, v2}, LX/2bV;->f(LX/2bV;Ljava/lang/String;)V

    .line 436767
    iget-object v3, p0, LX/2bV;->g:LX/1rj;

    iget-object v2, p0, LX/2bV;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v3, v2}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436768
    :cond_7
    :goto_4
    goto/16 :goto_1

    .line 436769
    :cond_8
    const-string v0, "/notifications_read"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_READ:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 436770
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 436771
    :cond_a
    const-string v0, "/notifications_seen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SEEN:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 436772
    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 436773
    :cond_c
    const/4 v0, 0x3

    goto/16 :goto_0

    .line 436774
    :cond_d
    iget-object v1, p0, LX/2bV;->j:LX/2c4;

    .line 436775
    iget-object p1, v1, LX/2c4;->i:LX/1rx;

    invoke-virtual {p1}, LX/1rx;->j()Z

    move-result p1

    if-eqz p1, :cond_e

    .line 436776
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_e

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 436777
    invoke-virtual {v1, p1}, LX/2c4;->b(Ljava/lang/String;)V

    goto :goto_5

    .line 436778
    :cond_e
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, v0, v1}, LX/2bV;->a(LX/2bV;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    goto/16 :goto_2

    .line 436779
    :cond_f
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, v0, v1}, LX/2bV;->a(LX/2bV;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    goto/16 :goto_3

    .line 436780
    :catch_0
    move-exception v2

    .line 436781
    sget-object v3, LX/2bV;->a:Ljava/lang/Class;

    const-string v4, "Failed to read mqtt message"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
