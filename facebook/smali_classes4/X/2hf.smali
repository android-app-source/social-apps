.class public LX/2hf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2hf;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/03V;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/2hi;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450294
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 450295
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hi;

    .line 450296
    invoke-interface {v0}, LX/2hi;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    goto :goto_0

    .line 450297
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/2hf;->a:LX/0P1;

    .line 450298
    iput-object p2, p0, LX/2hf;->b:LX/03V;

    .line 450299
    return-void
.end method

.method public static a(LX/0QB;)LX/2hf;
    .locals 6

    .prologue
    .line 450318
    sget-object v0, LX/2hf;->c:LX/2hf;

    if-nez v0, :cond_1

    .line 450319
    const-class v1, LX/2hf;

    monitor-enter v1

    .line 450320
    :try_start_0
    sget-object v0, LX/2hf;->c:LX/2hf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450321
    if-eqz v2, :cond_0

    .line 450322
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 450323
    new-instance v4, LX/2hf;

    .line 450324
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/2hg;

    invoke-direct {p0, v0}, LX/2hg;-><init>(LX/0QB;)V

    invoke-direct {v3, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v3

    .line 450325
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v5, v3}, LX/2hf;-><init>(Ljava/util/Set;LX/03V;)V

    .line 450326
    move-object v0, v4

    .line 450327
    sput-object v0, LX/2hf;->c:LX/2hf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450328
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450329
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450330
    :cond_1
    sget-object v0, LX/2hf;->c:LX/2hf;

    return-object v0

    .line 450331
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450332
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 450300
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 450301
    const-string v0, "qp_definition"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 450302
    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v0

    .line 450303
    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v0, v1, :cond_0

    move-object v1, v2

    .line 450304
    :goto_0
    return-object v1

    .line 450305
    :cond_0
    iget-object v1, p0, LX/2hf;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 450306
    if-nez v0, :cond_1

    move-object v1, v2

    .line 450307
    goto :goto_0

    .line 450308
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 450309
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 450310
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 450311
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 450312
    :catch_0
    move-exception v0

    .line 450313
    iget-object v1, p0, LX/2hf;->b:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/2hf;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_instantiation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to create QP fragment"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 450314
    goto :goto_0

    .line 450315
    :catch_1
    move-exception v0

    .line 450316
    iget-object v1, p0, LX/2hf;->b:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/2hf;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_access"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to create QP fragment"

    invoke-virtual {v1, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 450317
    goto :goto_0
.end method
