.class public final LX/2Br;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field public a:LX/0Up;

.field public final synthetic b:LX/2W8;

.field public final synthetic c:LX/0Sf;


# direct methods
.method public constructor <init>(LX/0Sf;LX/2W8;)V
    .locals 0

    .prologue
    .line 381542
    iput-object p1, p0, LX/2Br;->c:LX/0Sf;

    iput-object p2, p0, LX/2Br;->b:LX/2W8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 381543
    const-string v0, "FbAppInitializer-LowPriUIThread"

    const v3, 0x5f4cd11d

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381544
    :try_start_0
    iget-object v0, p0, LX/2Br;->a:LX/0Up;

    if-nez v0, :cond_1

    .line 381545
    iget-object v0, p0, LX/2Br;->b:LX/2W8;

    invoke-interface {v0}, LX/2W8;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381546
    iget-object v0, p0, LX/2Br;->b:LX/2W8;

    invoke-interface {v0}, LX/2W8;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    iput-object v0, p0, LX/2Br;->a:LX/0Up;

    .line 381547
    iget-object v0, p0, LX/2Br;->a:LX/0Up;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 381548
    iget-object v0, p0, LX/2Br;->b:LX/2W8;

    invoke-interface {v0}, LX/2W8;->remove()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 381549
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/2Br;->a:LX/0Up;

    if-eqz v0, :cond_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381550
    const v0, 0x1d262574

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v1

    :goto_1
    return v0

    .line 381551
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/2Br;->a:LX/0Up;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 381552
    const v3, -0x15c1a2d8

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 381553
    :try_start_3
    iget-object v0, p0, LX/2Br;->a:LX/0Up;

    invoke-interface {v0}, LX/0Up;->init()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381554
    const v0, -0x4e632355

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 381555
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Br;->a:LX/0Up;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 381556
    :catch_0
    move-exception v0

    .line 381557
    :try_start_5
    iget-object v1, p0, LX/2Br;->c:LX/0Sf;

    .line 381558
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v4, Lcom/facebook/common/init/impl/FbAppInitializerInternal$7;

    invoke-direct {v4, v1, v0}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$7;-><init>(LX/0Sf;Ljava/lang/Throwable;)V

    const v5, -0x7f4dd49e

    invoke-static {v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 381559
    const v0, 0x3ed524f9

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v2

    goto :goto_1

    .line 381560
    :catchall_0
    move-exception v0

    const v1, -0x451dacee

    :try_start_6
    invoke-static {v1}, LX/02m;->a(I)V

    .line 381561
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Br;->a:LX/0Up;

    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 381562
    :catchall_1
    move-exception v0

    const v1, 0x561b285f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381563
    :cond_2
    :try_start_7
    iget-object v0, p0, LX/2Br;->b:LX/2W8;

    invoke-interface {v0}, LX/2W8;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 381564
    const v0, 0x4b4830b0    # 1.3119664E7f

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v1

    goto :goto_1

    .line 381565
    :cond_3
    const v0, -0x57e74841

    invoke-static {v0}, LX/02m;->a(I)V

    move v0, v2

    goto :goto_1
.end method
