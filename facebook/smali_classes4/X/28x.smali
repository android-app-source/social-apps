.class public final LX/28x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile F:LX/28x;

.field public static final a:LX/0Tn;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/293;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private B:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/IiA;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Dhq;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field

.field private volatile D:J

.field public volatile E:Lcom/facebook/messaging/model/folders/FolderCounts;

.field public final c:Landroid/content/Context;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/os/Handler;

.field public final f:LX/0c4;

.field private final g:LX/2aX;

.field private final h:LX/2aX;

.field public final i:Z

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gZ;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QL;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QU;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/297;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Q2;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2bC;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/orca/notify/MessagingNotificationHandler;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/2aZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/2BG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 375126
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "processed_logout_notification"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/28x;->a:LX/0Tn;

    .line 375127
    const-class v0, LX/28x;

    sput-object v0, LX/28x;->b:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/0Or;LX/0c4;Ljava/lang/Boolean;LX/0Or;)V
    .locals 2
    .param p3    # LX/0c4;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsAccountSwitchingAvailable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "Lcom/facebook/multiprocess/peer/state/StatefulPeerManager;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375139
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375140
    iput-object v0, p0, LX/28x;->k:LX/0Ot;

    .line 375141
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375142
    iput-object v0, p0, LX/28x;->l:LX/0Ot;

    .line 375143
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375144
    iput-object v0, p0, LX/28x;->m:LX/0Ot;

    .line 375145
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375146
    iput-object v0, p0, LX/28x;->n:LX/0Ot;

    .line 375147
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375148
    iput-object v0, p0, LX/28x;->o:LX/0Ot;

    .line 375149
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375150
    iput-object v0, p0, LX/28x;->p:LX/0Ot;

    .line 375151
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375152
    iput-object v0, p0, LX/28x;->q:LX/0Ot;

    .line 375153
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375154
    iput-object v0, p0, LX/28x;->r:LX/0Ot;

    .line 375155
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375156
    iput-object v0, p0, LX/28x;->s:LX/0Ot;

    .line 375157
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375158
    iput-object v0, p0, LX/28x;->t:LX/0Ot;

    .line 375159
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375160
    iput-object v0, p0, LX/28x;->u:LX/0Ot;

    .line 375161
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375162
    iput-object v0, p0, LX/28x;->v:LX/0Ot;

    .line 375163
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 375164
    iput-object v0, p0, LX/28x;->w:LX/0Ot;

    .line 375165
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/28x;->C:Ljava/util/Map;

    .line 375166
    iput-object p1, p0, LX/28x;->c:Landroid/content/Context;

    .line 375167
    iput-object p2, p0, LX/28x;->d:LX/0Or;

    .line 375168
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/28x;->e:Landroid/os/Handler;

    .line 375169
    iput-object p3, p0, LX/28x;->f:LX/0c4;

    .line 375170
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/28x;->i:Z

    .line 375171
    iput-object p5, p0, LX/28x;->j:LX/0Or;

    .line 375172
    new-instance v0, LX/28y;

    invoke-direct {v0, p0}, LX/28y;-><init>(LX/28x;)V

    iput-object v0, p0, LX/28x;->g:LX/2aX;

    .line 375173
    new-instance v0, LX/2aY;

    invoke-direct {v0, p0}, LX/2aY;-><init>(LX/28x;)V

    iput-object v0, p0, LX/28x;->h:LX/2aX;

    .line 375174
    return-void
.end method

.method public static a(LX/0QB;)LX/28x;
    .locals 3

    .prologue
    .line 375128
    sget-object v0, LX/28x;->F:LX/28x;

    if-nez v0, :cond_1

    .line 375129
    const-class v1, LX/28x;

    monitor-enter v1

    .line 375130
    :try_start_0
    sget-object v0, LX/28x;->F:LX/28x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375131
    if-eqz v2, :cond_0

    .line 375132
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/28x;->b(LX/0QB;)LX/28x;

    move-result-object v0

    sput-object v0, LX/28x;->F:LX/28x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375133
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375134
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375135
    :cond_1
    sget-object v0, LX/28x;->F:LX/28x;

    return-object v0

    .line 375136
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/28x;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/2aZ;LX/2BG;LX/293;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/28x;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2gZ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3QL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3QU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/297;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Q2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2bC;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/orca/notify/MessagingNotificationHandler;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2aZ;",
            "LX/2BG;",
            "LX/293;",
            "LX/0Or",
            "<",
            "LX/IiA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 374986
    iput-object p1, p0, LX/28x;->k:LX/0Ot;

    iput-object p2, p0, LX/28x;->l:LX/0Ot;

    iput-object p3, p0, LX/28x;->m:LX/0Ot;

    iput-object p4, p0, LX/28x;->n:LX/0Ot;

    iput-object p5, p0, LX/28x;->o:LX/0Ot;

    iput-object p6, p0, LX/28x;->p:LX/0Ot;

    iput-object p7, p0, LX/28x;->q:LX/0Ot;

    iput-object p8, p0, LX/28x;->r:LX/0Ot;

    iput-object p9, p0, LX/28x;->s:LX/0Ot;

    iput-object p10, p0, LX/28x;->t:LX/0Ot;

    iput-object p11, p0, LX/28x;->u:LX/0Ot;

    iput-object p12, p0, LX/28x;->v:LX/0Ot;

    iput-object p13, p0, LX/28x;->w:LX/0Ot;

    iput-object p14, p0, LX/28x;->x:LX/0Or;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/28x;->y:LX/2aZ;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/28x;->z:LX/2BG;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/28x;->A:LX/293;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/28x;->B:LX/0Or;

    return-void
.end method

.method private static a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 375124
    iget-object v0, p0, LX/28x;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v3}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/3QL;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375125
    return-void
.end method

.method public static a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V
    .locals 3

    .prologue
    .line 375117
    iget-object v0, p0, LX/28x;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R9;

    .line 375118
    invoke-virtual {v0, p1}, LX/3R9;->a(Lcom/facebook/messaging/notify/MessagingNotification;)V

    goto :goto_0

    .line 375119
    :cond_0
    iget-object v0, p0, LX/28x;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R9;

    .line 375120
    iget-boolean v2, p1, Lcom/facebook/messaging/notify/MessagingNotification;->a:Z

    move v2, v2

    .line 375121
    if-nez v2, :cond_1

    .line 375122
    invoke-virtual {v0, p1}, LX/3R9;->b(Lcom/facebook/messaging/notify/MessagingNotification;)V

    goto :goto_1

    .line 375123
    :cond_1
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;LX/3B4;)V
    .locals 10

    .prologue
    .line 375104
    iget-object v1, p0, LX/28x;->C:Ljava/util/Map;

    monitor-enter v1

    .line 375105
    :try_start_0
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dhq;

    .line 375106
    if-nez v0, :cond_1

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 375107
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dhq;

    move-object v4, v0

    .line 375108
    :goto_0
    monitor-exit v1

    .line 375109
    if-nez v4, :cond_0

    .line 375110
    :goto_1
    return-void

    .line 375111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 375112
    :cond_0
    iget-object v0, p0, LX/28x;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3QL;

    .line 375113
    iget-object v0, v4, LX/Dhq;->h:Ljava/lang/String;

    move-object v2, v0

    .line 375114
    invoke-virtual {p2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/28x;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 375115
    iget-wide v8, v4, LX/Dhq;->b:J

    move-wide v4, v8

    .line 375116
    sub-long v4, v6, v4

    iget-object v6, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, LX/3QL;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v4, v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/28x;
    .locals 21

    .prologue
    .line 375101
    new-instance v2, LX/28x;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0xce8

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v5

    check-cast v5, LX/0c4;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    const/16 v7, 0x14db

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/28x;-><init>(Landroid/content/Context;LX/0Or;LX/0c4;Ljava/lang/Boolean;LX/0Or;)V

    .line 375102
    const/16 v3, 0x245

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x271

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x409

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xf9a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x17d

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xcd1

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xd72

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xd79

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xff2

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x9e

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2Yt;->a(LX/0QB;)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xd9d

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/2aZ;->a(LX/0QB;)LX/2aZ;

    move-result-object v17

    check-cast v17, LX/2aZ;

    invoke-static/range {p0 .. p0}, LX/2BG;->a(LX/0QB;)LX/2BG;

    move-result-object v18

    check-cast v18, LX/2BG;

    invoke-static/range {p0 .. p0}, LX/293;->a(LX/0QB;)LX/293;

    move-result-object v19

    check-cast v19, LX/293;

    const/16 v20, 0x2815

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {v2 .. v20}, LX/28x;->a(LX/28x;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/2aZ;LX/2BG;LX/293;LX/0Or;)V

    .line 375103
    return-object v2
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;LX/3B4;)LX/Dhq;
    .locals 7

    .prologue
    .line 375175
    iget-object v2, p0, LX/28x;->C:Ljava/util/Map;

    monitor-enter v2

    .line 375176
    :try_start_0
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dhq;

    .line 375177
    if-nez v0, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 375178
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dhq;

    .line 375179
    :cond_0
    if-nez v0, :cond_2

    .line 375180
    new-instance v0, LX/Dhq;

    invoke-direct {v0}, LX/Dhq;-><init>()V

    move-object v1, v0

    .line 375181
    :goto_0
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375182
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 375183
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375184
    :cond_1
    invoke-virtual {p2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v0

    .line 375185
    iput-object v0, v1, LX/Dhq;->h:Ljava/lang/String;

    .line 375186
    iget-object v0, p0, LX/28x;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 375187
    iput-wide v4, v1, LX/Dhq;->b:J

    .line 375188
    const/4 v0, 0x0

    .line 375189
    iput-boolean v0, v1, LX/Dhq;->a:Z

    .line 375190
    monitor-exit v2

    return-object v1

    .line 375191
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private static b(LX/28x;Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 11

    .prologue
    .line 375087
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    .line 375088
    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375089
    const-string v0, "silent"

    .line 375090
    iget-boolean v1, v1, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    if-nez v1, :cond_2

    .line 375091
    const-string v0, "buz"

    move-object v1, v0

    .line 375092
    :goto_0
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v2, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    .line 375093
    iget-object v0, p0, LX/28x;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    const/4 v10, 0x0

    .line 375094
    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 375095
    if-eqz v4, :cond_3

    .line 375096
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "message_type"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v4}, LX/5e9;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v5}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    .line 375097
    :goto_1
    if-eqz v2, :cond_0

    .line 375098
    const-string v4, "source"

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375099
    :cond_0
    iget-object v4, v0, LX/3QL;->f:LX/2bC;

    const-string v5, "notif_received"

    const-string v8, "message_id"

    iget-object v9, v3, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    move-object v6, v1

    invoke-virtual/range {v4 .. v10}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375100
    :cond_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    move-object v7, v10

    goto :goto_1
.end method

.method public static declared-synchronized c(LX/28x;Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 375026
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/28x;->b(LX/28x;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 375027
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    .line 375028
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 375029
    iget-object v5, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    .line 375030
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375031
    if-nez v3, :cond_0

    .line 375032
    :goto_0
    monitor-exit p0

    return-void

    .line 375033
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/28x;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Q2;

    invoke-virtual {v2}, LX/3Q2;->a()Ljava/lang/String;

    move-result-object v2

    .line 375034
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 375035
    const-string v0, "no_user"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 375036
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 375037
    :cond_1
    :try_start_2
    iget-object v2, p0, LX/28x;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-nez v2, :cond_2

    .line 375038
    const-string v0, "logged_out_user"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto :goto_0

    .line 375039
    :cond_2
    iget-object v2, p0, LX/28x;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Og;

    invoke-virtual {v2, v3}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 375040
    if-eqz v2, :cond_12

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v4, LX/6ek;->MONTAGE:LX/6ek;

    if-ne v2, v4, :cond_12

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 375041
    if-eqz v2, :cond_3

    .line 375042
    const-string v0, "notifications_disabled_for_montage"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto :goto_0

    .line 375043
    :cond_3
    iget-object v2, p0, LX/28x;->A:LX/293;

    .line 375044
    iget-object v4, v2, LX/293;->a:LX/0ad;

    sget-short v6, LX/JnP;->a:S

    const/4 v9, 0x0

    invoke-interface {v4, v6, v9}, LX/0ad;->a(SZ)Z

    move-result v4

    move v2, v4

    .line 375045
    if-eqz v2, :cond_4

    iget-object v2, p0, LX/28x;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/0db;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v4

    const/4 v6, 0x0

    invoke-interface {v2, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, LX/28x;->B:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IiA;

    invoke-virtual {v2, v1}, LX/IiA;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 375046
    new-instance v2, Lcom/facebook/messaging/notify/MentionNotification;

    sget-object v3, LX/3RF;->MENTION:LX/3RF;

    invoke-direct {v2, v0, v1, v3}, Lcom/facebook/messaging/notify/MentionNotification;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;LX/3RF;)V

    invoke-static {p0, v2}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 375047
    invoke-direct {p0}, LX/28x;->m()V

    goto/16 :goto_0

    .line 375048
    :cond_4
    invoke-static {p0}, LX/28x;->n(LX/28x;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 375049
    const-string v0, "notifications_disabled"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 375050
    :cond_5
    iget-object v2, p0, LX/28x;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/297;

    invoke-virtual {v2, v3}, LX/297;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v9

    .line 375051
    invoke-virtual {v9}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v2

    if-nez v2, :cond_6

    .line 375052
    const-string v0, "notifications_disabled_thread"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 375053
    :cond_6
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, LX/28x;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Og;

    .line 375054
    if-eqz v1, :cond_13

    iget-object v4, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v4, :cond_13

    iget-object v4, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2, v4}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/2OQ;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_13

    const/4 v4, 0x1

    :goto_2
    move v2, v4

    .line 375055
    if-eqz v2, :cond_7

    .line 375056
    const-string v0, "notification_dropped_message_read_locally"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 375057
    :cond_7
    iget-object v2, p0, LX/28x;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v10

    iput-wide v10, p0, LX/28x;->D:J

    .line 375058
    iget-object v2, p0, LX/28x;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->l()Z

    move-result v2

    if-nez v2, :cond_c

    .line 375059
    sget-object v4, LX/DiC;->NOT_IN_APP:LX/DiC;

    .line 375060
    :goto_3
    iget-object v2, v5, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-direct {p0, v1, v2}, LX/28x;->a(Lcom/facebook/messaging/model/messages/Message;LX/3B4;)V

    .line 375061
    iget-object v2, v5, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-direct {p0, v1, v2}, LX/28x;->b(Lcom/facebook/messaging/model/messages/Message;LX/3B4;)LX/Dhq;

    move-result-object v6

    .line 375062
    invoke-virtual {v6}, LX/Dhq;->l()Z

    move-result v10

    .line 375063
    if-nez v10, :cond_8

    .line 375064
    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v11, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v12, p0, LX/28x;->f:LX/0c4;

    .line 375065
    invoke-static {v2}, LX/0cB;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v12, v13, v11}, LX/0c4;->a(Landroid/net/Uri;Ljava/lang/Object;)V

    .line 375066
    :cond_8
    invoke-static {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-boolean v2, v9, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    if-nez v2, :cond_a

    iget-object v2, p0, LX/28x;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/3Kr;->ae:LX/0Tn;

    iget-object v3, p0, LX/28x;->w:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Or;

    const/4 v11, 0x0

    .line 375067
    iget-object v12, v3, LX/2Or;->a:LX/0Uh;

    const/16 v13, 0x62b

    invoke-virtual {v12, v13, v11}, LX/0Uh;->a(IZ)Z

    move-result v12

    if-nez v12, :cond_9

    const/4 v11, 0x1

    :cond_9
    move v3, v11

    .line 375068
    invoke-interface {v2, v9, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_f

    .line 375069
    :cond_a
    :goto_4
    if-eqz v7, :cond_b

    .line 375070
    const/4 v2, 0x1

    .line 375071
    iput-boolean v2, v6, LX/Dhq;->a:Z

    .line 375072
    :cond_b
    iget-object v2, p0, LX/28x;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->d:Lcom/facebook/messaging/model/threads/GroupMessageInfo;

    iget-object v8, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-object v9, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    invoke-static/range {v0 .. v9}, LX/3QU;->a(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/GroupMessageInfo;LX/DiC;Lcom/facebook/push/PushProperty;LX/Dhq;ZLcom/facebook/messaging/push/flags/ServerMessageAlertFlags;LX/DiB;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v0

    .line 375073
    invoke-static {p0, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/notify/MessagingNotification;)V

    .line 375074
    invoke-virtual {v6}, LX/Dhq;->l()Z

    move-result v0

    .line 375075
    if-eqz v0, :cond_10

    if-nez v10, :cond_10

    .line 375076
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "user_alerted_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/DiC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    .line 375077
    :goto_5
    invoke-direct {p0}, LX/28x;->m()V

    goto/16 :goto_0

    .line 375078
    :cond_c
    iget-object v2, p0, LX/28x;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uo;

    const-wide/16 v10, 0x2710

    invoke-virtual {v2, v10, v11}, LX/0Uo;->b(J)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 375079
    sget-object v4, LX/DiC;->IN_APP_ACTIVE_10S:LX/DiC;

    goto/16 :goto_3

    .line 375080
    :cond_d
    iget-object v2, p0, LX/28x;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uo;

    const-wide/16 v10, 0x7530

    invoke-virtual {v2, v10, v11}, LX/0Uo;->b(J)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 375081
    sget-object v4, LX/DiC;->IN_APP_ACTIVE_30S:LX/DiC;

    goto/16 :goto_3

    .line 375082
    :cond_e
    sget-object v4, LX/DiC;->IN_APP_IDLE:LX/DiC;

    goto/16 :goto_3

    :cond_f
    move v7, v8

    .line 375083
    goto :goto_4

    .line 375084
    :cond_10
    if-nez v10, :cond_11

    .line 375085
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "user_not_alerted_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/DiC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V

    goto :goto_5

    .line 375086
    :cond_11
    const-string v0, "has_recent_message"

    invoke-static {p0, v1, v5, v0}, LX/28x;->a(LX/28x;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/push/PushProperty;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :cond_12
    :try_start_3
    const/4 v2, 0x0

    goto/16 :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public static c(LX/28x;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 375016
    iget-object v0, p0, LX/28x;->E:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 375017
    if-nez v0, :cond_1

    .line 375018
    :cond_0
    :goto_0
    return-void

    .line 375019
    :cond_1
    iget v0, v0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    if-nez v0, :cond_0

    .line 375020
    iget-wide v0, p0, LX/28x;->D:J

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    .line 375021
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 375022
    cmp-long v4, v0, v2

    if-gtz v4, :cond_2

    .line 375023
    invoke-virtual {p0, p1}, LX/28x;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 375024
    :cond_2
    sub-long/2addr v0, v2

    .line 375025
    iget-object v2, p0, LX/28x;->e:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/orca/notify/MessagesNotificationManager$4;

    invoke-direct {v3, p0, v0, v1}, Lcom/facebook/orca/notify/MessagesNotificationManager$4;-><init>(LX/28x;J)V

    const v4, -0x43010cd9

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method private m()V
    .locals 12

    .prologue
    .line 375003
    iget-object v1, p0, LX/28x;->C:Ljava/util/Map;

    monitor-enter v1

    .line 375004
    :try_start_0
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v2, 0x64

    if-ge v0, v2, :cond_0

    .line 375005
    monitor-exit v1

    .line 375006
    :goto_0
    return-void

    .line 375007
    :cond_0
    iget-object v0, p0, LX/28x;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 375008
    iget-object v0, p0, LX/28x;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 375009
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 375010
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dhq;

    .line 375011
    iget-wide v10, v0, LX/Dhq;->b:J

    move-wide v6, v10

    .line 375012
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 375013
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v2, v6

    const-wide/32 v8, 0x36ee80

    cmp-long v0, v6, v8

    if-lez v0, :cond_1

    .line 375014
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 375015
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static n(LX/28x;)Z
    .locals 1

    .prologue
    .line 375002
    iget-object v0, p0, LX/28x;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/297;

    invoke-virtual {v0}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 374998
    iget-object v0, p0, LX/28x;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R9;

    .line 374999
    invoke-virtual {v0, p1, p2}, LX/3R9;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto :goto_0

    .line 375000
    :cond_0
    iget-object v0, p0, LX/28x;->f:LX/0c4;

    invoke-static {p1, v0}, LX/6bW;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0c4;)V

    .line 375001
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 374995
    iget-object v0, p0, LX/28x;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R9;

    .line 374996
    invoke-virtual {v0, p1}, LX/3R9;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 374997
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 374987
    iget-object v0, p0, LX/28x;->y:LX/2aZ;

    invoke-virtual {v0}, LX/2aZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374988
    :goto_0
    return-void

    .line 374989
    :cond_0
    iget-object v0, p0, LX/28x;->f:LX/0c4;

    const-string v1, "peer://msg_notification_unread_count/clear_thread"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, LX/28x;->g:LX/2aX;

    invoke-virtual {v0, v1, v2}, LX/0c4;->a(Landroid/net/Uri;LX/2aX;)V

    .line 374990
    iget-object v0, p0, LX/28x;->f:LX/0c4;

    sget-object v1, LX/0cB;->h:Landroid/net/Uri;

    iget-object v2, p0, LX/28x;->h:LX/2aX;

    invoke-virtual {v0, v1, v2}, LX/0c4;->a(Landroid/net/Uri;LX/2aX;)V

    .line 374991
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 374992
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 374993
    new-instance v1, LX/2GP;

    iget-object v2, p0, LX/28x;->c:Landroid/content/Context;

    invoke-direct {v1, p0, v2, v0}, LX/2GP;-><init>(LX/28x;Landroid/content/Context;Landroid/content/IntentFilter;)V

    .line 374994
    invoke-virtual {v1}, LX/14l;->a()V

    goto :goto_0
.end method
