.class public abstract LX/3Gn;
.super LX/3Ga;
.source ""


# instance fields
.field public b:Landroid/widget/LinearLayout;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Landroid/widget/ImageView;

.field public e:Ljava/lang/Boolean;

.field public f:Z

.field private o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 541813
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541814
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 541815
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/3Gn;->a(ZZ)V

    .line 541816
    return-void
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    .line 541817
    iget-object v0, p0, LX/3Gn;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Gn;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, p1, :cond_1

    .line 541818
    :cond_0
    :goto_0
    return-void

    .line 541819
    :cond_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/3Gn;->e:Ljava/lang/Boolean;

    .line 541820
    if-eqz p1, :cond_3

    const v0, 0x7f020d88

    .line 541821
    :goto_1
    invoke-virtual {p0, p1}, LX/3Gn;->b(Z)I

    move-result v1

    .line 541822
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 541823
    iget-object v2, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 541824
    :cond_2
    iget-object v1, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 541825
    invoke-virtual {p0, p1}, LX/3Gn;->c(Z)V

    .line 541826
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/3Gn;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 541827
    if-eqz p2, :cond_0

    .line 541828
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/3Gn;->o:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/resources/ui/FbTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 541829
    :cond_3
    const v0, 0x7f020d87

    goto :goto_1
.end method

.method public abstract b(Z)I
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 541811
    iget-boolean v0, p0, LX/3Gn;->f:Z

    return v0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 541812
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 541806
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 541807
    if-eqz v0, :cond_0

    .line 541808
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 541809
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541810
    :cond_0
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 541805
    const v0, 0x7f030925

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 541804
    const v0, 0x7f0315a0

    return v0
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 541798
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getWidth()I

    move-result v0

    .line 541799
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 541800
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 541801
    new-instance v2, LX/BwK;

    invoke-direct {v2, p0, v0}, LX/BwK;-><init>(LX/3Gn;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 541802
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 541803
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 541793
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 541794
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 541795
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->requestLayout()V

    .line 541796
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->invalidate()V

    .line 541797
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 541788
    const v0, 0x7f0d176b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    .line 541789
    const v0, 0x7f0d176e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 541790
    const v0, 0x7f0d176c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3Gn;->d:Landroid/widget/ImageView;

    .line 541791
    new-instance v0, Lcom/facebook/feed/video/inline/BaseInlineButtonPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/BaseInlineButtonPlugin$1;-><init>(LX/3Gn;)V

    iput-object v0, p0, LX/3Gn;->o:Ljava/lang/Runnable;

    .line 541792
    return-void
.end method
