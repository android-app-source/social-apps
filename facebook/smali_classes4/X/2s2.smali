.class public LX/2s2;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/0jJ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472268
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 472269
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0in;)LX/0jJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/system/model/ComposerModelImpl;",
            "LX/0in",
            "<",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;>;)",
            "LX/0jJ;"
        }
    .end annotation

    .prologue
    .line 472270
    new-instance v0, LX/0jJ;

    const-class v1, LX/HvE;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HvE;

    const-class v2, LX/2zF;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2zF;

    const-class v3, LX/HvH;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HvH;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/0jJ;-><init>(LX/HvE;LX/2zF;LX/HvH;LX/0Sh;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0in;)V

    .line 472271
    return-object v0
.end method
