.class public LX/2Gr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Gr;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/push/registration/C2DMService;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/push/registration/ADMService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/push/registration/NNAService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/push/registration/FbnsService;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/push/registration/FbnsLiteService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2H3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389492
    iput-object p1, p0, LX/2Gr;->a:LX/0Or;

    .line 389493
    iput-object p2, p0, LX/2Gr;->b:LX/0Or;

    .line 389494
    iput-object p3, p0, LX/2Gr;->c:LX/0Or;

    .line 389495
    iput-object p4, p0, LX/2Gr;->d:LX/0Or;

    .line 389496
    iput-object p5, p0, LX/2Gr;->e:LX/0Or;

    .line 389497
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gr;
    .locals 9

    .prologue
    .line 389498
    sget-object v0, LX/2Gr;->f:LX/2Gr;

    if-nez v0, :cond_1

    .line 389499
    const-class v1, LX/2Gr;

    monitor-enter v1

    .line 389500
    :try_start_0
    sget-object v0, LX/2Gr;->f:LX/2Gr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389501
    if-eqz v2, :cond_0

    .line 389502
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389503
    new-instance v3, LX/2Gr;

    const/16 v4, 0xfeb

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x301f

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3023

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xff8

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xffb

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2Gr;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 389504
    move-object v0, v3

    .line 389505
    sput-object v0, LX/2Gr;->f:LX/2Gr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389506
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389507
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389508
    :cond_1
    sget-object v0, LX/2Gr;->f:LX/2Gr;

    return-object v0

    .line 389509
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2Ge;)LX/2H3;
    .locals 2

    .prologue
    .line 389511
    sget-object v0, LX/2H1;->a:[I

    invoke-virtual {p1}, LX/2Ge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 389512
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown push service type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389513
    :pswitch_0
    iget-object v0, p0, LX/2Gr;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H3;

    .line 389514
    :goto_0
    return-object v0

    .line 389515
    :pswitch_1
    iget-object v0, p0, LX/2Gr;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H3;

    goto :goto_0

    .line 389516
    :pswitch_2
    iget-object v0, p0, LX/2Gr;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H3;

    goto :goto_0

    .line 389517
    :pswitch_3
    iget-object v0, p0, LX/2Gr;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H3;

    goto :goto_0

    .line 389518
    :pswitch_4
    iget-object v0, p0, LX/2Gr;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2H3;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
