.class public LX/3C0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/3C0;


# instance fields
.field public final b:LX/3C1;

.field public final c:Landroid/content/Context;

.field public final d:LX/0aU;

.field public final e:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529577
    const-class v0, LX/3C0;

    sput-object v0, LX/3C0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3C1;Landroid/content/Context;LX/0aU;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529579
    iput-object p1, p0, LX/3C0;->b:LX/3C1;

    .line 529580
    iput-object p2, p0, LX/3C0;->c:Landroid/content/Context;

    .line 529581
    iput-object p3, p0, LX/3C0;->d:LX/0aU;

    .line 529582
    iput-object p4, p0, LX/3C0;->e:LX/0W3;

    .line 529583
    return-void
.end method

.method public static a(LX/0QB;)LX/3C0;
    .locals 7

    .prologue
    .line 529584
    sget-object v0, LX/3C0;->f:LX/3C0;

    if-nez v0, :cond_1

    .line 529585
    const-class v1, LX/3C0;

    monitor-enter v1

    .line 529586
    :try_start_0
    sget-object v0, LX/3C0;->f:LX/3C0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529587
    if-eqz v2, :cond_0

    .line 529588
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 529589
    new-instance p0, LX/3C0;

    .line 529590
    new-instance v6, LX/3C1;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v6, v3, v4, v5}, LX/3C1;-><init>(Landroid/content/Context;LX/03V;Ljava/util/concurrent/ExecutorService;)V

    .line 529591
    move-object v3, v6

    .line 529592
    check-cast v3, LX/3C1;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v5

    check-cast v5, LX/0aU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3C0;-><init>(LX/3C1;Landroid/content/Context;LX/0aU;LX/0W3;)V

    .line 529593
    move-object v0, p0

    .line 529594
    sput-object v0, LX/3C0;->f:LX/3C0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529597
    :cond_1
    sget-object v0, LX/3C0;->f:LX/3C0;

    return-object v0

    .line 529598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
