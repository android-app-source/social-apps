.class public LX/2HC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/2HC;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0s6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390141
    const-class v0, LX/2HC;

    sput-object v0, LX/2HC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0s6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390143
    iput-object p1, p0, LX/2HC;->b:Landroid/content/Context;

    .line 390144
    iput-object p2, p0, LX/2HC;->c:LX/0s6;

    .line 390145
    return-void
.end method

.method public static a(LX/2HC;Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 390146
    iget-object v1, p0, LX/2HC;->c:LX/0s6;

    invoke-virtual {v1, p1, v0}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2HC;
    .locals 5

    .prologue
    .line 390147
    sget-object v0, LX/2HC;->d:LX/2HC;

    if-nez v0, :cond_1

    .line 390148
    const-class v1, LX/2HC;

    monitor-enter v1

    .line 390149
    :try_start_0
    sget-object v0, LX/2HC;->d:LX/2HC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390150
    if-eqz v2, :cond_0

    .line 390151
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390152
    new-instance p0, LX/2HC;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-direct {p0, v3, v4}, LX/2HC;-><init>(Landroid/content/Context;LX/0s6;)V

    .line 390153
    move-object v0, p0

    .line 390154
    sput-object v0, LX/2HC;->d:LX/2HC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390155
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390156
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390157
    :cond_1
    sget-object v0, LX/2HC;->d:LX/2HC;

    return-object v0

    .line 390158
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
