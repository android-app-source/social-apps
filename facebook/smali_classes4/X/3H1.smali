.class public LX/3H1;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "LX/BSe;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3H2;


# direct methods
.method public constructor <init>(LX/3H2;LX/2ml;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542621
    iget v0, p2, LX/2ml;->c:I

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 542622
    iput-object p1, p0, LX/3H1;->a:LX/3H2;

    .line 542623
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 542624
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/3H1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542625
    if-nez v0, :cond_0

    .line 542626
    :goto_0
    monitor-exit p0

    return-void

    .line 542627
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/BSe;->a()V

    .line 542628
    invoke-virtual {p0, p1}, LX/3H1;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 542629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/3Fh;Z)V
    .locals 11

    .prologue
    .line 542630
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/3H1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSe;

    .line 542631
    if-nez v0, :cond_0

    .line 542632
    iget-object v0, p0, LX/3H1;->a:LX/3H2;

    .line 542633
    new-instance v1, LX/BSe;

    invoke-static {v0}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v3

    check-cast v3, LX/09G;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    const/16 v2, 0x1591

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {v0}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object v9

    check-cast v9, LX/3H4;

    invoke-static {v0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v10

    check-cast v10, LX/2ml;

    move-object v2, p1

    invoke-direct/range {v1 .. v10}, LX/BSe;-><init>(Ljava/lang/String;LX/09G;LX/0lC;LX/03V;LX/0Sh;LX/0Or;LX/0kL;LX/3H4;LX/2ml;)V

    .line 542634
    move-object v0, v1

    .line 542635
    invoke-virtual {p0, p1, v0}, LX/3H1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542636
    :cond_0
    iget-object v1, v0, LX/BSe;->b:LX/09G;

    iget-object v2, v0, LX/BSe;->i:Ljava/lang/String;

    new-instance v3, LX/BSc;

    invoke-direct {v3, v0, p3, p2}, LX/BSc;-><init>(LX/BSe;ZLX/3Fh;)V

    new-instance v4, LX/BSd;

    invoke-direct {v4, v0, p3}, LX/BSd;-><init>(LX/BSe;Z)V

    invoke-virtual {v1, v2, v3, v4}, LX/09G;->a(Ljava/lang/String;LX/0TF;LX/09K;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542637
    monitor-exit p0

    return-void

    .line 542638
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 542639
    check-cast p3, LX/BSe;

    .line 542640
    if-eqz p3, :cond_0

    .line 542641
    invoke-virtual {p3}, LX/BSe;->a()V

    .line 542642
    :cond_0
    return-void
.end method
