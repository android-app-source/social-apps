.class public LX/2Dv;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/280;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/280;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 384824
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/280;
    .locals 7

    .prologue
    .line 384812
    sget-object v0, LX/2Dv;->a:LX/280;

    if-nez v0, :cond_1

    .line 384813
    const-class v1, LX/2Dv;

    monitor-enter v1

    .line 384814
    :try_start_0
    sget-object v0, LX/2Dv;->a:LX/280;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384815
    if-eqz v2, :cond_0

    .line 384816
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384817
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/27v;->a(LX/0QB;)LX/27v;

    move-result-object v4

    check-cast v4, LX/27v;

    invoke-static {v0}, LX/27x;->a(LX/0QB;)LX/27x;

    move-result-object v5

    check-cast v5, LX/27x;

    invoke-static {v0}, LX/27y;->a(LX/0QB;)LX/27y;

    move-result-object v6

    check-cast v6, LX/27y;

    invoke-static {v0}, LX/27z;->b(LX/0QB;)LX/27z;

    move-result-object p0

    check-cast p0, LX/27z;

    invoke-static {v3, v4, v5, v6, p0}, LX/26u;->a(Landroid/content/Context;LX/27v;LX/27x;LX/27y;LX/27z;)LX/280;

    move-result-object v3

    move-object v0, v3

    .line 384818
    sput-object v0, LX/2Dv;->a:LX/280;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384819
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384820
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384821
    :cond_1
    sget-object v0, LX/2Dv;->a:LX/280;

    return-object v0

    .line 384822
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 384811
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/27v;->a(LX/0QB;)LX/27v;

    move-result-object v1

    check-cast v1, LX/27v;

    invoke-static {p0}, LX/27x;->a(LX/0QB;)LX/27x;

    move-result-object v2

    check-cast v2, LX/27x;

    invoke-static {p0}, LX/27y;->a(LX/0QB;)LX/27y;

    move-result-object v3

    check-cast v3, LX/27y;

    invoke-static {p0}, LX/27z;->b(LX/0QB;)LX/27z;

    move-result-object v4

    check-cast v4, LX/27z;

    invoke-static {v0, v1, v2, v3, v4}, LX/26u;->a(Landroid/content/Context;LX/27v;LX/27x;LX/27y;LX/27z;)LX/280;

    move-result-object v0

    return-object v0
.end method
