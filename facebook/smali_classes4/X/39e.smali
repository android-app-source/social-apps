.class public LX/39e;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C4A;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/39e",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C4A;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523593
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 523594
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/39e;->b:LX/0Zi;

    .line 523595
    iput-object p1, p0, LX/39e;->a:LX/0Ot;

    .line 523596
    return-void
.end method

.method public static a(LX/0QB;)LX/39e;
    .locals 4

    .prologue
    .line 523605
    const-class v1, LX/39e;

    monitor-enter v1

    .line 523606
    :try_start_0
    sget-object v0, LX/39e;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523607
    sput-object v2, LX/39e;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523608
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523609
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523610
    new-instance v3, LX/39e;

    const/16 p0, 0x1ef0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/39e;-><init>(LX/0Ot;)V

    .line 523611
    move-object v0, v3

    .line 523612
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523613
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523614
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523603
    invoke-static {}, LX/1dS;->b()V

    .line 523604
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x28a9c56c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 523597
    iget-object v1, p0, LX/39e;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 523598
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 523599
    invoke-virtual {p1}, LX/1De;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const p0, 0x7f0102a0

    const/4 p2, 0x1

    invoke-virtual {v2, p0, v1, p2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 523600
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v2

    iput v2, p5, LX/1no;->a:I

    .line 523601
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 523602
    const/16 v1, 0x1f

    const v2, -0x7e8d4973

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523556
    iget-object v0, p0, LX/39e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 523557
    new-instance v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {v0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 523558
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 10

    .prologue
    .line 523568
    check-cast p2, LX/C49;

    .line 523569
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 523570
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 523571
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 523572
    iget-object v0, p0, LX/39e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4A;

    iget-object v1, p2, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C49;->b:LX/1Po;

    iget-boolean v3, p2, LX/C49;->c:Z

    iget v4, p2, LX/C49;->d:I

    .line 523573
    iget-object v8, v0, LX/C4A;->a:LX/20g;

    invoke-virtual {v8, v1, v2, v3}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Z)LX/21I;

    move-result-object v8

    .line 523574
    iput-object v8, v5, LX/1np;->a:Ljava/lang/Object;

    .line 523575
    iget-object v8, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 523576
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    .line 523577
    iget-object v9, v0, LX/C4A;->e:LX/21T;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result p0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result p1

    invoke-static {v8}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v8

    invoke-virtual {v9, p0, p1, v8}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v8

    .line 523578
    invoke-virtual {v8, v4}, LX/21Y;->a(I)LX/21c;

    move-result-object v9

    .line 523579
    invoke-virtual {v8, v9}, LX/21Y;->a(LX/21c;)[F

    move-result-object v8

    .line 523580
    iput-object v8, v6, LX/1np;->a:Ljava/lang/Object;

    .line 523581
    invoke-static {v9}, LX/21c;->hasIcons(LX/21c;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 523582
    iput-object v8, v7, LX/1np;->a:Ljava/lang/Object;

    .line 523583
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523584
    check-cast v0, LX/21I;

    iput-object v0, p2, LX/C49;->e:LX/21I;

    .line 523585
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 523586
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523587
    check-cast v0, [F

    iput-object v0, p2, LX/C49;->f:[F

    .line 523588
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 523589
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523590
    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p2, LX/C49;->g:Ljava/lang/Boolean;

    .line 523591
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 523592
    return-void
.end method

.method public final c(LX/1De;)LX/C48;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 523616
    new-instance v1, LX/C49;

    invoke-direct {v1, p0}, LX/C49;-><init>(LX/39e;)V

    .line 523617
    iget-object v2, p0, LX/39e;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C48;

    .line 523618
    if-nez v2, :cond_0

    .line 523619
    new-instance v2, LX/C48;

    invoke-direct {v2, p0}, LX/C48;-><init>(LX/39e;)V

    .line 523620
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C48;->a$redex0(LX/C48;LX/1De;IILX/C49;)V

    .line 523621
    move-object v1, v2

    .line 523622
    move-object v0, v1

    .line 523623
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 523567
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 523566
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 523563
    check-cast p3, LX/C49;

    .line 523564
    iget-object v0, p0, LX/39e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4A;

    move-object v1, p2

    check-cast v1, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v2, p3, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p3, LX/C49;->h:LX/1zt;

    iget-object v4, p3, LX/C49;->e:LX/21I;

    iget-object v5, p3, LX/C49;->f:[F

    check-cast v5, [F

    iget-object v6, p3, LX/C49;->g:Ljava/lang/Boolean;

    invoke-virtual/range {v0 .. v6}, LX/C4A;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1zt;LX/21I;[FLjava/lang/Boolean;)V

    .line 523565
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 523560
    iget-object v0, p0, LX/39e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4A;

    check-cast p2, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 523561
    iget-object p0, v0, LX/C4A;->d:LX/20K;

    invoke-static {p2, p0}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 523562
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 523559
    const/16 v0, 0xf

    return v0
.end method
