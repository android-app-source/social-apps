.class public LX/2LS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Landroid/content/ComponentName;

.field private static final c:Landroid/content/ComponentName;

.field private static final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:LX/2LT;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 395059
    const-class v0, LX/2LS;

    sput-object v0, LX/2LS;->a:Ljava/lang/Class;

    .line 395060
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.launcher"

    const-string v2, "com.android.launcher2.Launcher"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2LS;->b:Landroid/content/ComponentName;

    .line 395061
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2LS;->c:Landroid/content/ComponentName;

    .line 395062
    const-string v0, "com.htc.launcher"

    const-string v1, "com.sec.android.app.twlauncher"

    const-string v2, "com.sec.android.app.launcher"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2LS;->f:LX/0Px;

    .line 395063
    const-string v0, "com.google.android.googlequicksearchbox"

    const-string v1, "com.android.launcher"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2LS;->g:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395056
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/2LS;->d:Landroid/content/Context;

    .line 395057
    new-instance v0, LX/2LT;

    iget-object v1, p0, LX/2LS;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2LT;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/2LS;->e:LX/2LT;

    .line 395058
    return-void
.end method

.method public static b(LX/0QB;)LX/2LS;
    .locals 2

    .prologue
    .line 395064
    new-instance v1, LX/2LS;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/2LS;-><init>(Landroid/content/Context;)V

    .line 395065
    return-object v1
.end method

.method public static h(LX/2LS;)Landroid/content/ComponentName;
    .locals 8

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x2

    .line 395034
    iget-object v0, p0, LX/2LS;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 395035
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 395036
    const-string v4, "android.intent.category.HOME"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 395037
    const/high16 v4, 0x10000

    :try_start_0
    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 395038
    const/4 v1, 0x0

    .line 395039
    const/4 v0, 0x0

    .line 395040
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v4, v1

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 395041
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v7, "com.facebook."

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 395042
    new-instance v5, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 395043
    if-ge v1, v2, :cond_1

    sget-object v0, LX/2LS;->f:LX/0Px;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    move-object v4, v5

    .line 395044
    goto :goto_0

    .line 395045
    :catch_0
    sget-object v4, LX/2LS;->c:Landroid/content/ComponentName;

    .line 395046
    :goto_1
    return-object v4

    .line 395047
    :cond_1
    if-ge v1, v3, :cond_2

    sget-object v0, LX/2LS;->g:LX/0Px;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v3

    move-object v4, v5

    .line 395048
    goto :goto_0

    .line 395049
    :cond_2
    if-gtz v1, :cond_5

    .line 395050
    const/4 v0, 0x1

    move-object v1, v5

    :goto_2
    move-object v4, v1

    move v1, v0

    .line 395051
    goto :goto_0

    .line 395052
    :cond_3
    if-nez v4, :cond_4

    .line 395053
    sget-object v4, LX/2LS;->b:Landroid/content/ComponentName;

    .line 395054
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Chosen launcher "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " of priority "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    move v0, v1

    move-object v1, v4

    goto :goto_2
.end method

.method public static i(LX/2LS;)Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 395027
    iget-object v0, p0, LX/2LS;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 395028
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 395029
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 395030
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 395031
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 395032
    new-instance v0, Landroid/content/ComponentName;

    iget-object v2, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 395033
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/2LS;->c:Landroid/content/ComponentName;

    goto :goto_0
.end method
