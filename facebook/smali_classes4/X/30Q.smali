.class public final LX/30Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/30V;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/30V;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 484360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484361
    iput-object p1, p0, LX/30Q;->a:LX/0QB;

    .line 484362
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 484363
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/30Q;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 484364
    packed-switch p2, :pswitch_data_0

    .line 484365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484366
    :pswitch_0
    new-instance v1, LX/30U;

    const/16 v0, 0x1787

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const/16 v0, 0x144d

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-direct {v1, p0, p2, v0}, LX/30U;-><init>(LX/0Or;LX/0Or;LX/0W3;)V

    .line 484367
    move-object v0, v1

    .line 484368
    :goto_0
    return-object v0

    .line 484369
    :pswitch_1
    new-instance v0, LX/30W;

    const/16 v1, 0x1a0a

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 p0, 0x1478

    invoke-static {p1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v0, v1, p0}, LX/30W;-><init>(LX/0Or;LX/0Or;)V

    .line 484370
    move-object v0, v0

    .line 484371
    goto :goto_0

    .line 484372
    :pswitch_2
    new-instance v1, LX/2In;

    const/16 v0, 0x1a0c

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const/16 v0, 0x438

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, p0, p2, v0}, LX/2In;-><init>(LX/0Or;LX/0Or;LX/0Uh;)V

    .line 484373
    move-object v0, v1

    .line 484374
    goto :goto_0

    .line 484375
    :pswitch_3
    new-instance p0, LX/30X;

    invoke-static {p1}, LX/30Y;->b(LX/0QB;)LX/30Y;

    move-result-object v0

    check-cast v0, LX/30Y;

    invoke-static {p1}, LX/30Z;->a(LX/0QB;)LX/30Z;

    move-result-object v1

    check-cast v1, LX/30Z;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 p2, 0x1a0d

    invoke-static {p1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {p0, v0, v1, v2, p2}, LX/30X;-><init>(LX/30Y;LX/30Z;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 484376
    move-object v0, p0

    .line 484377
    goto :goto_0

    .line 484378
    :pswitch_4
    new-instance v2, LX/3EG;

    const/16 v0, 0x1a85

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-static {p1}, LX/1Ar;->b(LX/0QB;)LX/1Ar;

    move-result-object v1

    check-cast v1, LX/1Ar;

    invoke-direct {v2, p0, v0, v1}, LX/3EG;-><init>(LX/0Or;LX/0W3;LX/1Ar;)V

    .line 484379
    move-object v0, v2

    .line 484380
    goto :goto_0

    .line 484381
    :pswitch_5
    new-instance v1, LX/2J9;

    const/16 v0, 0x1a95

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v0

    check-cast v0, LX/0fW;

    const/16 p0, 0x12cb

    invoke-static {p1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v1, v2, v0, p0}, LX/2J9;-><init>(LX/0Or;LX/0fW;LX/0Or;)V

    .line 484382
    move-object v0, v1

    .line 484383
    goto :goto_0

    .line 484384
    :pswitch_6
    new-instance v1, LX/3EH;

    invoke-direct {v1}, LX/3EH;-><init>()V

    .line 484385
    invoke-static {p1}, LX/1b3;->b(LX/0QB;)LX/1b3;

    move-result-object v0

    check-cast v0, LX/1b3;

    const/16 v2, 0x1bd8

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 484386
    iput-object v0, v1, LX/3EH;->b:LX/1b3;

    iput-object v2, v1, LX/3EH;->c:LX/0Or;

    .line 484387
    move-object v0, v1

    .line 484388
    goto/16 :goto_0

    .line 484389
    :pswitch_7
    new-instance v1, LX/2JF;

    const/16 v0, 0x1d81

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v2, v0}, LX/2JF;-><init>(LX/0Or;LX/0ad;)V

    .line 484390
    move-object v0, v1

    .line 484391
    goto/16 :goto_0

    .line 484392
    :pswitch_8
    new-instance p0, LX/2JG;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v1

    check-cast v1, LX/0VT;

    const/16 v2, 0xab3

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0WX;->a(LX/0QB;)LX/0UW;

    move-result-object v2

    check-cast v2, LX/0UW;

    invoke-direct {p0, v0, v1, p2, v2}, LX/2JG;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0VT;LX/0Or;LX/0UW;)V

    .line 484393
    move-object v0, p0

    .line 484394
    goto/16 :goto_0

    .line 484395
    :pswitch_9
    new-instance v0, LX/3EI;

    const/16 v1, 0x2357

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3EI;-><init>(LX/0Or;)V

    .line 484396
    move-object v0, v0

    .line 484397
    goto/16 :goto_0

    .line 484398
    :pswitch_a
    new-instance v0, LX/2JH;

    const/16 v1, 0x2358

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2JH;-><init>(LX/0Or;)V

    .line 484399
    move-object v0, v0

    .line 484400
    goto/16 :goto_0

    .line 484401
    :pswitch_b
    new-instance v1, LX/2JI;

    invoke-static {p1}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v0

    check-cast v0, LX/0sT;

    const/16 v2, 0x235a

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2JI;-><init>(LX/0sT;LX/0Or;)V

    .line 484402
    move-object v0, v1

    .line 484403
    goto/16 :goto_0

    .line 484404
    :pswitch_c
    new-instance v1, LX/2JJ;

    const/16 v0, 0x25d1

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, v0}, LX/2JJ;-><init>(LX/0Or;LX/0Uh;)V

    .line 484405
    move-object v0, v1

    .line 484406
    goto/16 :goto_0

    .line 484407
    :pswitch_d
    new-instance v0, LX/2JK;

    const/16 v1, 0x25d7

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2JK;-><init>(LX/0Or;)V

    .line 484408
    move-object v0, v0

    .line 484409
    goto/16 :goto_0

    .line 484410
    :pswitch_e
    new-instance v1, LX/2JN;

    const/16 v0, 0xca5

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v2, v0}, LX/2JN;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 484411
    move-object v0, v1

    .line 484412
    goto/16 :goto_0

    .line 484413
    :pswitch_f
    new-instance v0, LX/2JO;

    const/16 v1, 0x262d

    invoke-static {p1, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x14d6

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2JO;-><init>(LX/0Or;LX/0Or;)V

    .line 484414
    move-object v0, v0

    .line 484415
    goto/16 :goto_0

    .line 484416
    :pswitch_10
    new-instance v0, LX/2JP;

    const/16 v1, 0x2657

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2JP;-><init>(LX/0Or;)V

    .line 484417
    move-object v0, v0

    .line 484418
    goto/16 :goto_0

    .line 484419
    :pswitch_11
    new-instance v1, LX/2JR;

    const/16 v0, 0x286c

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v0, 0x1545

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/2JS;->a(LX/0QB;)LX/2JS;

    move-result-object v0

    check-cast v0, LX/2JS;

    invoke-direct {v1, v2, p0, v0}, LX/2JR;-><init>(LX/0Or;LX/0Or;LX/2JS;)V

    .line 484420
    move-object v0, v1

    .line 484421
    goto/16 :goto_0

    .line 484422
    :pswitch_12
    new-instance v1, LX/2JV;

    const/16 v0, 0x2a72

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v0, 0xdf4

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, p0, v0}, LX/2JV;-><init>(LX/0Or;LX/0Or;LX/0Uh;)V

    .line 484423
    move-object v0, v1

    .line 484424
    goto/16 :goto_0

    .line 484425
    :pswitch_13
    new-instance v2, LX/2JW;

    invoke-direct {v2}, LX/2JW;-><init>()V

    .line 484426
    invoke-static {p1}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v0

    check-cast v0, LX/10M;

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 p0, 0xe63

    invoke-static {p1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 484427
    iput-object v0, v2, LX/2JW;->b:LX/10M;

    iput-object v1, v2, LX/2JW;->c:LX/0Uh;

    iput-object p0, v2, LX/2JW;->d:LX/0Or;

    .line 484428
    move-object v0, v2

    .line 484429
    goto/16 :goto_0

    .line 484430
    :pswitch_14
    new-instance p0, LX/2JX;

    invoke-direct {p0}, LX/2JX;-><init>()V

    .line 484431
    invoke-static {p1}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v0

    check-cast v0, LX/10M;

    invoke-static {p1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 p2, 0x2af4

    invoke-static {p1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    .line 484432
    iput-object v0, p0, LX/2JX;->b:LX/10M;

    iput-object v1, p0, LX/2JX;->c:LX/0WJ;

    iput-object v2, p0, LX/2JX;->d:LX/0Uh;

    iput-object p2, p0, LX/2JX;->e:LX/0Or;

    .line 484433
    move-object v0, p0

    .line 484434
    goto/16 :goto_0

    .line 484435
    :pswitch_15
    new-instance v0, LX/2JY;

    invoke-direct {v0}, LX/2JY;-><init>()V

    .line 484436
    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p1}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v2

    check-cast v2, LX/10M;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/2JZ;->b(LX/0QB;)LX/2JZ;

    move-result-object v4

    check-cast v4, LX/2JZ;

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    const/16 p2, 0xe69

    invoke-static {p1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    .line 484437
    iput-object v1, v0, LX/2JY;->b:LX/0SG;

    iput-object v2, v0, LX/2JY;->c:LX/10M;

    iput-object v3, v0, LX/2JY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v0, LX/2JY;->e:LX/2JZ;

    iput-object p0, v0, LX/2JY;->f:LX/0Uh;

    iput-object p2, v0, LX/2JY;->g:LX/0Or;

    .line 484438
    move-object v0, v0

    .line 484439
    goto/16 :goto_0

    .line 484440
    :pswitch_16
    new-instance v1, LX/2Ja;

    const/16 v0, 0x31ae

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, v0}, LX/2Ja;-><init>(LX/0Or;LX/0Uh;)V

    .line 484441
    move-object v0, v1

    .line 484442
    goto/16 :goto_0

    .line 484443
    :pswitch_17
    new-instance v1, LX/2Jb;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const/16 v2, 0x32c1

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2Jb;-><init>(LX/0ad;LX/0Or;)V

    .line 484444
    move-object v0, v1

    .line 484445
    goto/16 :goto_0

    .line 484446
    :pswitch_18
    new-instance v0, LX/2Jc;

    const/16 v1, 0x3584

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Jc;-><init>(LX/0Or;)V

    .line 484447
    move-object v0, v0

    .line 484448
    goto/16 :goto_0

    .line 484449
    :pswitch_19
    new-instance v0, LX/2Jd;

    const/16 v1, 0x392e

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x38a

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xf9a

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xac0

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Jd;-><init>(LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 484450
    move-object v0, v0

    .line 484451
    goto/16 :goto_0

    .line 484452
    :pswitch_1a
    new-instance v0, LX/2Je;

    const/16 v1, 0x3930

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x15ad

    invoke-static {p1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xf9a

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xac0

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Je;-><init>(LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 484453
    move-object v0, v0

    .line 484454
    goto/16 :goto_0

    .line 484455
    :pswitch_1b
    new-instance v2, LX/2Jf;

    const/16 v3, 0x3931

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x387

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xf9a

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13c1

    invoke-static {p1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xac0

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/2Jf;-><init>(LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;)V

    .line 484456
    move-object v0, v2

    .line 484457
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 484458
    const/16 v0, 0x1c

    return v0
.end method
