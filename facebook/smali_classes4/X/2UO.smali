.class public LX/2UO;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile o:LX/2UO;


# instance fields
.field private final c:LX/30Y;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0SG;

.field private final h:LX/00H;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/30I;

.field private final k:LX/1Ml;

.field private final l:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

.field private final m:LX/2UP;

.field public final n:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415885
    const-class v0, LX/2UO;

    sput-object v0, LX/2UO;->a:Ljava/lang/Class;

    .line 415886
    const-class v0, Lcom/facebook/contacts/background/ContactsTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2UO;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/30Y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0SG;LX/00H;LX/0Or;LX/30I;LX/1Ml;Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/2UP;LX/0Uh;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadBackgroundTaskEnabled;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsCCUServerSyncEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/30Y;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0SG;",
            "LX/00H;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/30I;",
            "LX/1Ml;",
            "Lcom/facebook/contacts/upload/ContinuousContactUploadClient;",
            "LX/2UP;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415897
    const-string v0, "ContractsUploadBackgroundTask"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415898
    iput-object p1, p0, LX/2UO;->c:LX/30Y;

    .line 415899
    iput-object p2, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415900
    iput-object p3, p0, LX/2UO;->e:LX/0Or;

    .line 415901
    iput-object p4, p0, LX/2UO;->f:LX/0Or;

    .line 415902
    iput-object p5, p0, LX/2UO;->g:LX/0SG;

    .line 415903
    iput-object p6, p0, LX/2UO;->h:LX/00H;

    .line 415904
    iput-object p7, p0, LX/2UO;->i:LX/0Or;

    .line 415905
    iput-object p8, p0, LX/2UO;->j:LX/30I;

    .line 415906
    iput-object p9, p0, LX/2UO;->k:LX/1Ml;

    .line 415907
    iput-object p10, p0, LX/2UO;->l:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    .line 415908
    iput-object p11, p0, LX/2UO;->m:LX/2UP;

    .line 415909
    iput-object p12, p0, LX/2UO;->n:LX/0Uh;

    .line 415910
    return-void
.end method

.method public static a(LX/0QB;)LX/2UO;
    .locals 3

    .prologue
    .line 415887
    sget-object v0, LX/2UO;->o:LX/2UO;

    if-nez v0, :cond_1

    .line 415888
    const-class v1, LX/2UO;

    monitor-enter v1

    .line 415889
    :try_start_0
    sget-object v0, LX/2UO;->o:LX/2UO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415890
    if-eqz v2, :cond_0

    .line 415891
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2UO;->b(LX/0QB;)LX/2UO;

    move-result-object v0

    sput-object v0, LX/2UO;->o:LX/2UO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415892
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415893
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415894
    :cond_1
    sget-object v0, LX/2UO;->o:LX/2UO;

    return-object v0

    .line 415895
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2UO;
    .locals 13

    .prologue
    .line 415883
    new-instance v0, LX/2UO;

    invoke-static {p0}, LX/30Y;->b(LX/0QB;)LX/30Y;

    move-result-object v1

    check-cast v1, LX/30Y;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x30d

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x30c

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, LX/00H;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/00H;

    const/16 v7, 0x15e7

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v8

    check-cast v8, LX/30I;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v9

    check-cast v9, LX/1Ml;

    invoke-static {p0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(LX/0QB;)Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    move-result-object v10

    check-cast v10, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static {p0}, LX/2UP;->a(LX/0QB;)LX/2UP;

    move-result-object v11

    check-cast v11, LX/2UP;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v0 .. v12}, LX/2UO;-><init>(LX/30Y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0SG;LX/00H;LX/0Or;LX/30I;LX/1Ml;Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/2UP;LX/0Uh;)V

    .line 415884
    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 415911
    iget-object v0, p0, LX/2UO;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 415882
    sget-object v0, LX/2UO;->b:LX/0Rf;

    return-object v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415834
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 14

    .prologue
    const-wide/16 v8, -0x1

    const-wide/32 v10, 0x5265c00

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 415845
    iget-object v0, p0, LX/2UO;->c:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415846
    :cond_0
    :goto_0
    return v3

    .line 415847
    :cond_1
    iget-object v0, p0, LX/2UO;->k:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415848
    iget-object v0, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->h:LX/0Tn;

    invoke-interface {v0, v1, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 415849
    iget-object v0, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->d:LX/0Tn;

    invoke-interface {v0, v1, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 415850
    cmp-long v0, v0, v10

    if-gez v0, :cond_2

    const-wide/32 v0, 0xea60

    .line 415851
    :goto_1
    iget-object v6, p0, LX/2UO;->g:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long/2addr v6, v4

    cmp-long v0, v6, v0

    if-ltz v0, :cond_0

    .line 415852
    iget-object v0, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->h:LX/0Tn;

    iget-object v6, p0, LX/2UO;->g:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v0, v1, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 415853
    iget-object v0, p0, LX/2UO;->h:LX/00H;

    .line 415854
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 415855
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_6

    .line 415856
    iget-object v0, p0, LX/2UO;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 415857
    iget-object v1, p0, LX/2UO;->j:LX/30I;

    iget-object v6, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, LX/30I;->a(Z)V

    move v1, v2

    .line 415858
    :goto_2
    iget-object v0, p0, LX/2UO;->j:LX/30I;

    invoke-virtual {v0}, LX/30I;->a()Z

    move-result v0

    .line 415859
    iget-object v6, p0, LX/2UO;->m:LX/2UP;

    .line 415860
    iget-object v7, v6, LX/2UP;->a:LX/0Zb;

    new-instance v12, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v13, "ccu_setting"

    invoke-direct {v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v13, "ccu_status"

    invoke-virtual {v12, v13, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    .line 415861
    const-string v13, "contacts_upload"

    move-object v13, v13

    .line 415862
    iput-object v13, v12, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415863
    move-object v12, v12

    .line 415864
    invoke-interface {v7, v12}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415865
    if-eqz v0, :cond_0

    .line 415866
    iget-object v0, p0, LX/2UO;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v3}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415867
    iget-object v0, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/32V;->f:LX/0Tn;

    invoke-interface {v0, v6, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 415868
    iget-object v0, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/0uQ;->d:LX/0Tn;

    invoke-interface {v0, v8, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 415869
    iget-object v0, p0, LX/2UO;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v10

    sub-long v6, v10, v6

    cmp-long v0, v6, v8

    if-ltz v0, :cond_0

    .line 415870
    invoke-direct {p0}, LX/2UO;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415871
    iget-object v0, p0, LX/2UO;->l:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    .line 415872
    iput-wide v4, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->o:J

    .line 415873
    move v3, v2

    .line 415874
    goto/16 :goto_0

    .line 415875
    :cond_2
    const-wide/32 v0, 0x1b77400

    goto/16 :goto_1

    .line 415876
    :cond_3
    if-nez v1, :cond_4

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 415877
    iget-object v5, p0, LX/2UO;->n:LX/0Uh;

    const/16 v6, 0x214

    invoke-virtual {v5, v6, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/32V;->p:LX/0Tn;

    invoke-interface {v5, v6, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v5

    if-nez v5, :cond_7

    .line 415878
    iget-object v4, p0, LX/2UO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/32V;->p:LX/0Tn;

    invoke-interface {v4, v5, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 415879
    :goto_3
    move v0, v0

    .line 415880
    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, LX/2UO;->l:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-virtual {v0, v1}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    move v3, v2

    .line 415881
    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto/16 :goto_2

    :cond_7
    move v0, v4

    goto :goto_3
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415835
    iget-object v0, p0, LX/2UO;->h:LX/00H;

    .line 415836
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 415837
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    const-string v0, "contacts_upload_friend_finder"

    .line 415838
    :goto_0
    iget-object v1, p0, LX/2UO;->l:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-direct {p0}, LX/2UO;->k()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Ljava/lang/String;Z)LX/1ML;

    move-result-object v1

    .line 415839
    if-nez v1, :cond_1

    .line 415840
    const/4 v0, 0x0

    .line 415841
    :goto_1
    return-object v0

    .line 415842
    :cond_0
    const-string v0, "contacts_upload_messaging"

    goto :goto_0

    .line 415843
    :cond_1
    new-instance v0, LX/3f0;

    sget-object v2, LX/2UO;->a:Ljava/lang/Class;

    invoke-direct {v0, v2}, LX/3f0;-><init>(Ljava/lang/Class;)V

    .line 415844
    invoke-static {v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method
