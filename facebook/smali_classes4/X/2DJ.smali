.class public LX/2DJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zh;

.field private final b:LX/2u6;


# direct methods
.method public constructor <init>(LX/0Zh;LX/2u6;)V
    .locals 0

    .prologue
    .line 384010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384011
    iput-object p1, p0, LX/2DJ;->a:LX/0Zh;

    .line 384012
    iput-object p2, p0, LX/2DJ;->b:LX/2u6;

    .line 384013
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 384014
    iget-object v0, p0, LX/2DJ;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 384015
    const-string v0, "writeNewSessionData"

    const v2, 0x51d5877f

    invoke-static {v0, v2}, LX/03q;->a(Ljava/lang/String;I)V

    .line 384016
    :try_start_0
    const-string v0, "session_id"

    iget-object v2, p0, LX/2DJ;->b:LX/2u6;

    .line 384017
    iget-object v3, v2, LX/2u6;->a:Ljava/lang/String;

    move-object v2, v3

    .line 384018
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 384019
    const-string v0, "seq"

    iget-object v2, p0, LX/2DJ;->b:LX/2u6;

    invoke-virtual {v2}, LX/2u6;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 384020
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 384021
    const-string v0, "uid"

    iget-object v2, p0, LX/2DJ;->b:LX/2u6;

    .line 384022
    iget-object v3, v2, LX/2u6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 384023
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 384024
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, LX/0nC;->b(Ljava/io/Writer;LX/0nA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384025
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 384026
    const v0, 0x3d0510e3

    invoke-static {v0}, LX/03q;->a(I)V

    .line 384027
    return-void

    .line 384028
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    .line 384029
    const v1, -0x700f24cd

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method
