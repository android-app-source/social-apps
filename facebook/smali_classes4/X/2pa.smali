.class public LX/2pa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:D


# direct methods
.method private constructor <init>(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;LX/0Rf;D)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;D)V"
        }
    .end annotation

    .prologue
    .line 468423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468424
    iput-object p1, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 468425
    iput-object p2, p0, LX/2pa;->b:LX/0P1;

    .line 468426
    iput-object p3, p0, LX/2pa;->c:LX/0Rf;

    .line 468427
    iput-wide p4, p0, LX/2pa;->d:D

    .line 468428
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;LX/0Rf;DB)V
    .locals 0

    .prologue
    .line 468439
    invoke-direct/range {p0 .. p5}, LX/2pa;-><init>(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;LX/0Rf;D)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 468438
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 468437
    iget-wide v0, p0, LX/2pa;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/2pa;->d:D

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 468436
    iget-wide v0, p0, LX/2pa;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/2pa;->d:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 468440
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 468435
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->k()Z

    move-result v0

    return v0
.end method

.method public final f()LX/3HY;
    .locals 2

    .prologue
    .line 468431
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    .line 468432
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "VideoPlayerViewSizeKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3HY;

    .line 468433
    if-eqz v0, :cond_0

    .line 468434
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    goto :goto_0
.end method

.method public final g()LX/2pZ;
    .locals 1

    .prologue
    .line 468430
    invoke-static {p0}, LX/2pZ;->a(LX/2pa;)LX/2pZ;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468429
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoPlayerParams : ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
