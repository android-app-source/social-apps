.class public LX/2GX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0lC;

.field private static c:LX/0W3;

.field private static d:Z

.field private static f:Ljava/util/Random;


# instance fields
.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388830
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    sput-object v0, LX/2GX;->b:LX/0lC;

    .line 388831
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/2GX;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/0W3;Ljava/util/Random;)V
    .locals 1
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param

    .prologue
    .line 388832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388833
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2GX;->e:Ljava/util/Map;

    .line 388834
    sget-boolean v0, LX/2GX;->d:Z

    if-nez v0, :cond_0

    .line 388835
    sput-object p1, LX/2GX;->c:LX/0W3;

    .line 388836
    sput-object p2, LX/2GX;->f:Ljava/util/Random;

    .line 388837
    invoke-static {}, LX/2GX;->b()V

    .line 388838
    :cond_0
    return-void
.end method

.method private static declared-synchronized b()V
    .locals 9

    .prologue
    .line 388839
    const-class v2, LX/2GX;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, LX/2GX;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 388840
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 388841
    :cond_1
    :try_start_1
    sget-object v0, LX/2GX;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388842
    sget-object v0, LX/2GX;->c:LX/0W3;

    sget-wide v4, LX/0X5;->fy:J

    const-string v1, ""

    invoke-interface {v0, v4, v5, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 388843
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 388844
    :try_start_2
    sget-object v0, LX/2GX;->b:LX/0lC;

    invoke-virtual {v0, v3}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 388845
    if-eqz v0, :cond_2

    .line 388846
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v4

    .line 388847
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 388848
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;
    :try_end_2
    .catch LX/2aQ; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388849
    :try_start_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 388850
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    invoke-virtual {v1}, LX/0lF;->C()I

    move-result v1

    .line 388851
    if-nez v1, :cond_3

    .line 388852
    const-string v1, "LocalStatsHistogramSampler.class"

    const-string v5, "Bad sample rate %d for markerId: %s."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v1, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/2aQ; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 388853
    :catch_0
    move-exception v1

    .line 388854
    :try_start_4
    const-string v5, "LocalStatsHistogramSampler.class"

    const-string v6, "NumberFormatException in readTree(), bad markerId: %s."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v5, v1, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch LX/2aQ; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 388855
    :catch_1
    move-exception v0

    .line 388856
    :try_start_5
    const-string v1, "LocalStatsHistogramSampler.class"

    const-string v4, "JsonParseException in readTree(), config string %s is not a json string."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v1, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 388857
    :cond_2
    :goto_2
    const/4 v0, 0x1

    sput-boolean v0, LX/2GX;->d:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 388858
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 388859
    :cond_3
    :try_start_6
    sget-object v6, LX/2GX;->a:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_0
    .catch LX/2aQ; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 388860
    :catch_2
    move-exception v0

    .line 388861
    :try_start_7
    const-string v1, "LocalStatsHistogramSampler.class"

    const-string v3, "IO Exception in readTree()."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388813
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GX;->e:Ljava/util/Map;

    .line 388814
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/2GX;->e:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388815
    monitor-exit p0

    return-object v0

    .line 388816
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 388817
    iget-object v2, p0, LX/2GX;->e:Ljava/util/Map;

    monitor-enter v2

    .line 388818
    :try_start_0
    iget-object v0, p0, LX/2GX;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388819
    iget-object v0, p0, LX/2GX;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 388820
    :goto_0
    monitor-exit v2

    .line 388821
    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 388822
    :cond_0
    sget-object v0, LX/2GX;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 388823
    sget-object v0, LX/2GX;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 388824
    :goto_2
    move v0, v0

    .line 388825
    if-eq v0, v1, :cond_1

    .line 388826
    sget-object v3, LX/2GX;->f:Ljava/util/Random;

    invoke-virtual {v3, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 388827
    :cond_1
    iget-object v3, p0, LX/2GX;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 388828
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 388829
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    sget-object v0, LX/2GX;->a:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2
.end method
