.class public LX/2zW;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/ExperimentManager;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/ExperimentManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 483496
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/ExperimentManager;
    .locals 4

    .prologue
    .line 483484
    sget-object v0, LX/2zW;->a:Lcom/facebook/compactdisk/ExperimentManager;

    if-nez v0, :cond_1

    .line 483485
    const-class v1, LX/2zW;

    monitor-enter v1

    .line 483486
    :try_start_0
    sget-object v0, LX/2zW;->a:Lcom/facebook/compactdisk/ExperimentManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483487
    if-eqz v2, :cond_0

    .line 483488
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483489
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v3, p0}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/ExperimentManager;

    move-result-object v3

    move-object v0, v3

    .line 483490
    sput-object v0, LX/2zW;->a:Lcom/facebook/compactdisk/ExperimentManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483491
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483492
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483493
    :cond_1
    sget-object v0, LX/2zW;->a:Lcom/facebook/compactdisk/ExperimentManager;

    return-object v0

    .line 483494
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 483483
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v0, v1}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/ExperimentManager;

    move-result-object v0

    return-object v0
.end method
