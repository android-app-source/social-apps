.class public final LX/3IG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field public final synthetic a:LX/3IF;


# direct methods
.method public constructor <init>(LX/3IF;)V
    .locals 0

    .prologue
    .line 545777
    iput-object p1, p0, LX/3IG;->a:LX/3IF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 545774
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    sget-object v1, LX/2qn;->ZOOMING:LX/2qn;

    .line 545775
    iput-object v1, v0, LX/3IF;->d:LX/2qn;

    .line 545776
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    iget-object v0, v0, LX/3IF;->c:LX/3IB;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-interface {v0, v1}, LX/3IB;->a(F)Z

    move-result v0

    return v0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 545771
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    sget-object v1, LX/2qn;->ZOOM_STARTED:LX/2qn;

    .line 545772
    iput-object v1, v0, LX/3IF;->d:LX/2qn;

    .line 545773
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    iget-object v0, v0, LX/3IF;->c:LX/3IB;

    invoke-interface {v0}, LX/3IB;->c()Z

    move-result v0

    return v0
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 545767
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    sget-object v1, LX/2qn;->ZOOM_ENDED:LX/2qn;

    .line 545768
    iput-object v1, v0, LX/3IF;->d:LX/2qn;

    .line 545769
    iget-object v0, p0, LX/3IG;->a:LX/3IF;

    iget-object v0, v0, LX/3IF;->c:LX/3IB;

    invoke-interface {v0}, LX/3IB;->d()V

    .line 545770
    return-void
.end method
