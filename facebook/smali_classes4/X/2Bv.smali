.class public LX/2Bv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile l:LX/2Bv;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0a8;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/io/File;

.field public final f:Landroid/content/Context;

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Lcom/facebook/forker/Process;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:LX/0dN;

.field private k:LX/0aB;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 381687
    sget-object v0, LX/0eJ;->b:LX/0Tn;

    sget-object v1, LX/0eJ;->g:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2Bv;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0a8;LX/0Or;)V
    .locals 3
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/diagnostics/IsDebugLogsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381791
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    .line 381792
    iput-object p1, p0, LX/2Bv;->f:Landroid/content/Context;

    .line 381793
    iput-object p2, p0, LX/2Bv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 381794
    iput-object p3, p0, LX/2Bv;->c:LX/0a8;

    .line 381795
    iput-object p4, p0, LX/2Bv;->d:LX/0Or;

    .line 381796
    iget-object v0, p0, LX/2Bv;->f:Landroid/content/Context;

    const-string v1, "logcat_flash_logs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/2Bv;->e:Ljava/io/File;

    .line 381797
    return-void
.end method

.method public static a(LX/0QB;)LX/2Bv;
    .locals 7

    .prologue
    .line 381777
    sget-object v0, LX/2Bv;->l:LX/2Bv;

    if-nez v0, :cond_1

    .line 381778
    const-class v1, LX/2Bv;

    monitor-enter v1

    .line 381779
    :try_start_0
    sget-object v0, LX/2Bv;->l:LX/2Bv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 381780
    if-eqz v2, :cond_0

    .line 381781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 381782
    new-instance v6, LX/2Bv;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v5

    check-cast v5, LX/0a8;

    const/16 p0, 0x1469

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/2Bv;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0a8;LX/0Or;)V

    .line 381783
    move-object v0, v6

    .line 381784
    sput-object v0, LX/2Bv;->l:LX/2Bv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381785
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 381786
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 381787
    :cond_1
    sget-object v0, LX/2Bv;->l:LX/2Bv;

    return-object v0

    .line 381788
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 381789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 381768
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 381769
    if-eqz v1, :cond_1

    .line 381770
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 381771
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 381772
    invoke-static {v3}, LX/2Bv;->a(Ljava/io/File;)V

    .line 381773
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381774
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 381775
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 381776
    return-void
.end method

.method private static a([Ljava/io/File;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 381766
    new-instance v0, LX/44F;

    invoke-direct {v0}, LX/44F;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 381767
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 381765
    const-string v0, "lock"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized c(LX/2Bv;)V
    .locals 1

    .prologue
    .line 381759
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Bv;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2Bv;->g:Z

    .line 381760
    iget-boolean v0, p0, LX/2Bv;->g:Z

    if-eqz v0, :cond_0

    .line 381761
    invoke-direct {p0}, LX/2Bv;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381762
    :goto_0
    monitor-exit p0

    return-void

    .line 381763
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/2Bv;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 381754
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2Bv;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 381755
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 381756
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/2Bv;->i:Z

    .line 381757
    new-instance v0, Lcom/facebook/common/diagnostics/LogcatFbSdcardLogger$3;

    invoke-direct {v0, p0}, Lcom/facebook/common/diagnostics/LogcatFbSdcardLogger$3;-><init>(LX/2Bv;)V

    const-string v1, "logcat-manager"

    const v2, -0x1e7570cd

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381758
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static e(LX/2Bv;)V
    .locals 4

    .prologue
    .line 381747
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/2Bv;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/lock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 381748
    const/4 v1, 0x0

    .line 381749
    :try_start_0
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    .line 381750
    invoke-static {p0}, LX/2Bv;->f(LX/2Bv;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 381751
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 381752
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381753
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private static f(LX/2Bv;)V
    .locals 5

    .prologue
    .line 381728
    monitor-enter p0

    .line 381729
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2Bv;->i:Z

    .line 381730
    iget-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2Bv;->g:Z

    if-nez v0, :cond_1

    .line 381731
    :cond_0
    monitor-exit p0

    .line 381732
    :goto_0
    return-void

    .line 381733
    :cond_1
    new-instance v0, LX/086;

    const-string v1, "/system/bin/logcat"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/086;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "-v"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "threadtime"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "-f"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/2Bv;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/logs"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "-r4096"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "-n4"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/086;->addArguments([Ljava/lang/String;)LX/086;

    move-result-object v0

    invoke-virtual {v0}, LX/086;->create()Lcom/facebook/forker/Process;

    move-result-object v0

    iput-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    .line 381734
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 381735
    :try_start_1
    invoke-virtual {v0}, Lcom/facebook/forker/Process;->waitForUninterruptibly()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 381736
    invoke-virtual {v0}, Lcom/facebook/forker/Process;->destroy()V

    .line 381737
    monitor-enter p0

    .line 381738
    :try_start_2
    iget-object v1, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    if-ne v1, v0, :cond_2

    .line 381739
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    .line 381740
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 381741
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 381742
    :catchall_2
    move-exception v1

    invoke-virtual {v0}, Lcom/facebook/forker/Process;->destroy()V

    .line 381743
    monitor-enter p0

    .line 381744
    :try_start_4
    iget-object v2, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    if-ne v2, v0, :cond_3

    .line 381745
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    .line 381746
    :cond_3
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 1

    .prologue
    .line 381722
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2Bv;->g:Z

    .line 381723
    iget-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 381724
    :goto_0
    monitor-exit p0

    return-void

    .line 381725
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;

    invoke-virtual {v0}, Lcom/facebook/forker/Process;->destroy()V

    .line 381726
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Bv;->h:Lcom/facebook/forker/Process;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381727
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381707
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Bv;->g:Z

    if-nez v0, :cond_1

    .line 381708
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 381709
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 381710
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Bv;->e:Ljava/io/File;

    if-nez v0, :cond_2

    .line 381711
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 381712
    :cond_2
    iget-object v0, p0, LX/2Bv;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 381713
    if-nez v2, :cond_3

    .line 381714
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 381715
    :cond_3
    invoke-static {v2}, LX/2Bv;->a([Ljava/io/File;)V

    .line 381716
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 381717
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 381718
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/2Bv;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 381719
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381720
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 381721
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 5

    .prologue
    .line 381698
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2Bv;->g()V

    .line 381699
    iget-object v0, p0, LX/2Bv;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 381700
    if-eqz v1, :cond_1

    .line 381701
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 381702
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2Bv;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 381703
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381704
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381705
    :cond_1
    monitor-exit p0

    return-void

    .line 381706
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized init()V
    .locals 3

    .prologue
    .line 381688
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/2Bw;

    invoke-direct {v0, p0}, LX/2Bw;-><init>(LX/2Bv;)V

    iput-object v0, p0, LX/2Bv;->j:LX/0dN;

    .line 381689
    iget-object v0, p0, LX/2Bv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Bv;->a:Ljava/util/Set;

    iget-object v2, p0, LX/2Bv;->j:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 381690
    new-instance v0, LX/2Bx;

    invoke-direct {v0, p0}, LX/2Bx;-><init>(LX/2Bv;)V

    iput-object v0, p0, LX/2Bv;->k:LX/0aB;

    .line 381691
    iget-object v0, p0, LX/2Bv;->c:LX/0a8;

    iget-object v1, p0, LX/2Bv;->k:LX/0aB;

    const/16 v2, 0x321

    invoke-virtual {v0, v1, v2}, LX/0a8;->a(LX/0aB;I)V

    .line 381692
    invoke-static {p0}, LX/2Bv;->c(LX/2Bv;)V

    .line 381693
    iget-object v0, p0, LX/2Bv;->f:Landroid/content/Context;

    const-string v1, "logcat"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 381694
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381695
    :goto_0
    monitor-exit p0

    return-void

    .line 381696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 381697
    :cond_0
    invoke-static {v0}, LX/2Bv;->a(Ljava/io/File;)V

    goto :goto_0
.end method
