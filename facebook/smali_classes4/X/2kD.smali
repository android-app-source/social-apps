.class public LX/2kD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:LX/2kE;

.field private final c:LX/0tD;

.field private d:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mNextFileNumber"
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 455440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455441
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/2kD;->d:Ljava/lang/Integer;

    .line 455442
    iput-object p1, p0, LX/2kD;->a:Ljava/io/File;

    .line 455443
    new-instance v0, LX/2kE;

    iget-object v1, p0, LX/2kD;->a:Ljava/io/File;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/2kE;-><init>(Ljava/io/File;LX/15j;)V

    iput-object v0, p0, LX/2kD;->b:LX/2kE;

    .line 455444
    new-instance v0, LX/0tD;

    iget-object v1, p0, LX/2kD;->a:Ljava/io/File;

    invoke-direct {v0, v1}, LX/0tD;-><init>(Ljava/io/File;)V

    iput-object v0, p0, LX/2kD;->c:LX/0tD;

    .line 455445
    return-void
.end method

.method public static a(Ljava/io/File;)LX/2kD;
    .locals 1

    .prologue
    .line 455438
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    .line 455439
    new-instance v0, LX/2kD;

    invoke-direct {v0, p0}, LX/2kD;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method private a(Ljava/io/File;Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/nio/ByteBuffer;",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;JZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 455423
    if-nez p2, :cond_0

    .line 455424
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 455425
    :goto_0
    return-object v0

    .line 455426
    :cond_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-static {v0, p3, p4, p5}, LX/1lJ;->a(ILX/0w5;J)[B

    move-result-object v0

    .line 455427
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 455428
    :try_start_0
    invoke-static {v2, p2}, LX/31S;->a(Ljava/io/FileOutputStream;Ljava/nio/ByteBuffer;)I

    .line 455429
    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 455430
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 455431
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 455432
    if-eqz p6, :cond_1

    .line 455433
    iget-object v1, p0, LX/2kD;->c:LX/0tD;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, LX/0tD;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;[B)V

    .line 455434
    iget-object v0, p0, LX/2kD;->c:LX/0tD;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tD;->a(Ljava/lang/String;)V

    .line 455435
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 455436
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455437
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static d(LX/2kD;)V
    .locals 6

    .prologue
    .line 455414
    iget-object v1, p0, LX/2kD;->d:Ljava/lang/Integer;

    monitor-enter v1

    .line 455415
    :try_start_0
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 455416
    if-eqz v2, :cond_0

    .line 455417
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455418
    :try_start_1
    iget-object v5, p0, LX/2kD;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, LX/2kD;->d:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455419
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 455420
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/2kD;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 455421
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/2kD;->d:Ljava/lang/Integer;

    .line 455422
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    goto :goto_1
.end method

.method private e()I
    .locals 3

    .prologue
    .line 455409
    iget-object v1, p0, LX/2kD;->d:Ljava/lang/Integer;

    monitor-enter v1

    .line 455410
    :try_start_0
    iget-object v0, p0, LX/2kD;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 455411
    invoke-static {p0}, LX/2kD;->d(LX/2kD;)V

    .line 455412
    :cond_0
    iget-object v0, p0, LX/2kD;->d:Ljava/lang/Integer;

    iget-object v2, p0, LX/2kD;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, LX/2kD;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1

    return v0

    .line 455413
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(I",
            "Ljava/lang/String;",
            "LX/0w5;",
            "[B)TT;"
        }
    .end annotation

    .prologue
    .line 455393
    iget-object v0, p0, LX/2kD;->b:LX/2kE;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2kE;->a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 455408
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    return-object v0
.end method

.method public final a(Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;JZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 455406
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-static {v0}, LX/39R;->a(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    .line 455407
    invoke-direct/range {v0 .. v6}, LX/2kD;->a(Ljava/io/File;Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 455404
    iget-object v0, p0, LX/2kD;->c:LX/0tD;

    invoke-virtual {v0, p1}, LX/0tD;->a(Ljava/lang/String;)V

    .line 455405
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;LX/0w5;JZ)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;JZ)I"
        }
    .end annotation

    .prologue
    .line 455398
    invoke-direct {p0}, LX/2kD;->e()I

    move-result v7

    .line 455399
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 455400
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v0

    .line 455401
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    .line 455402
    invoke-direct/range {v0 .. v6}, LX/2kD;->a(Ljava/io/File;Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;

    .line 455403
    return v7
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 455395
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-static {v0}, LX/39R;->c(Ljava/io/File;)V

    .line 455396
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 455397
    return-void
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 455394
    iget-object v0, p0, LX/2kD;->a:Ljava/io/File;

    invoke-static {v0}, LX/39R;->b(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method
