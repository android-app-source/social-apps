.class public final LX/2po;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2qg;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2pb;


# direct methods
.method public constructor <init>(LX/2pb;)V
    .locals 0

    .prologue
    .line 468948
    iput-object p1, p0, LX/2po;->a:LX/2pb;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2qg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468949
    const-class v0, LX/2qg;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 468950
    check-cast p1, LX/2qg;

    .line 468951
    iget-object v0, p1, LX/2qg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2qg;->a:Ljava/lang/String;

    iget-object v1, p0, LX/2po;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468952
    iget-object v0, p0, LX/2po;->a:LX/2pb;

    iget-object v1, p1, LX/2qg;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468953
    iget-object p0, v0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468954
    iput-object v1, v0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468955
    iget-object p1, v0, LX/2pb;->l:LX/19j;

    iget-boolean p1, p1, LX/19j;->T:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p0, p1, :cond_0

    .line 468956
    iget-object p0, v0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, p1, :cond_0

    .line 468957
    iget-object p0, v0, LX/2pb;->E:LX/2q7;

    sget-object p1, LX/04g;->BY_LIVE_POLLER_TRANSITION:LX/04g;

    invoke-interface {p0, p1}, LX/2q7;->e(LX/04g;)V

    .line 468958
    :cond_0
    return-void
.end method
