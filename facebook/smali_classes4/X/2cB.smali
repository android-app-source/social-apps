.class public final LX/2cB;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/2Pk;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector:LX/2cB;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Pk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440387
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 440388
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2cB;
    .locals 4

    .prologue
    .line 440389
    sget-object v0, LX/2cB;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector:LX/2cB;

    if-nez v0, :cond_1

    .line 440390
    const-class v1, LX/2cB;

    monitor-enter v1

    .line 440391
    :try_start_0
    sget-object v0, LX/2cB;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector:LX/2cB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440392
    if-eqz v2, :cond_0

    .line 440393
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440394
    new-instance v3, LX/2cB;

    const/16 p0, 0xea6

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2cB;-><init>(LX/0Ot;)V

    .line 440395
    move-object v0, v3

    .line 440396
    sput-object v0, LX/2cB;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector:LX/2cB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440397
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440398
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440399
    :cond_1
    sget-object v0, LX/2cB;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager_OmnistoreComponentManagerBroadcastReceiverRegistration__INJECTED_BY_TemplateInjector:LX/2cB;

    return-object v0

    .line 440400
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public bridge synthetic onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 440402
    check-cast p3, LX/2Pk;

    .line 440403
    iget-object v0, p3, LX/2Pk;->mBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$1;

    invoke-direct {v1, p0, p3}, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$1;-><init>(LX/2cB;LX/2Pk;)V

    const v2, -0xd2be679

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 440404
    iget-object v0, p3, LX/2Pk;->mBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;

    invoke-direct {v1, p0, p3}, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;-><init>(LX/2cB;LX/2Pk;)V

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 440405
    return-void
.end method
