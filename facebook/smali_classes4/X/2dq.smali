.class public LX/2dq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1V8;

.field private final c:LX/1DR;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1DR;LX/1V8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444382
    iput-object p1, p0, LX/2dq;->a:Landroid/content/Context;

    .line 444383
    iput-object p2, p0, LX/2dq;->c:LX/1DR;

    .line 444384
    iput-object p3, p0, LX/2dq;->b:LX/1V8;

    .line 444385
    return-void
.end method

.method public static b(LX/0QB;)LX/2dq;
    .locals 4

    .prologue
    .line 444399
    new-instance v3, LX/2dq;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v1

    check-cast v1, LX/1DR;

    invoke-static {p0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v2

    check-cast v2, LX/1V8;

    invoke-direct {v3, v0, v1, v2}, LX/2dq;-><init>(Landroid/content/Context;LX/1DR;LX/1V8;)V

    .line 444400
    return-object v3
.end method


# virtual methods
.method public final a(FLX/1Ua;Z)LX/2eF;
    .locals 3

    .prologue
    .line 444393
    iget-object v0, p0, LX/2dq;->b:LX/1V8;

    invoke-virtual {v0, p2}, LX/1V8;->a(LX/1Ua;)LX/1Ub;

    move-result-object v0

    .line 444394
    iget-object v1, v0, LX/1Ub;->d:LX/1Uc;

    move-object v0, v1

    .line 444395
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Uc;->a(I)F

    move-result v0

    .line 444396
    iget-object v1, p0, LX/2dq;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    invoke-static {v1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 444397
    iget-object v1, p0, LX/2dq;->c:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    sub-int v0, v1, v0

    .line 444398
    invoke-static {p1, v0, p3}, LX/2eF;->a(FIZ)LX/2eF;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Ua;)LX/2eF;
    .locals 3

    .prologue
    .line 444386
    iget-object v0, p0, LX/2dq;->b:LX/1V8;

    invoke-virtual {v0, p1}, LX/1V8;->a(LX/1Ua;)LX/1Ub;

    move-result-object v0

    .line 444387
    iget-object v1, v0, LX/1Ub;->d:LX/1Uc;

    move-object v0, v1

    .line 444388
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Uc;->a(I)F

    move-result v0

    .line 444389
    iget-object v1, p0, LX/2dq;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    invoke-static {v1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 444390
    iget-object v1, p0, LX/2dq;->c:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    sub-int v0, v1, v0

    .line 444391
    iget-object v1, p0, LX/2dq;->a:Landroid/content/Context;

    int-to-float v2, v0

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 444392
    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, LX/2eF;->a(FIZ)LX/2eF;

    move-result-object v0

    return-object v0
.end method
