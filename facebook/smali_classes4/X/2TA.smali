.class public final LX/2TA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/0ad;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/0ad;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 413802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413803
    iput-object p1, p0, LX/2TA;->a:LX/0ad;

    .line 413804
    iput-object p2, p0, LX/2TA;->b:Landroid/content/pm/PackageManager;

    .line 413805
    iput-object p3, p0, LX/2TA;->c:Ljava/lang/String;

    .line 413806
    return-void
.end method

.method public static b(LX/0QB;)LX/2TA;
    .locals 4

    .prologue
    .line 413800
    new-instance v3, LX/2TA;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/2TA;-><init>(LX/0ad;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 413801
    return-object v3
.end method


# virtual methods
.method public final init()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 413794
    iget-object v0, p0, LX/2TA;->c:Ljava/lang/String;

    const-string v3, "com.facebook.katana"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2TA;->c:Ljava/lang/String;

    const-string v3, "com.facebook.wakizashi"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 413795
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2TA;->a:LX/0ad;

    sget-short v3, LX/0wf;->a:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413796
    iget-object v0, p0, LX/2TA;->b:Landroid/content/pm/PackageManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, p0, LX/2TA;->c:Ljava/lang/String;

    const-string v4, "com.facebook.timeline.stagingground.ProfilePictureShareActivityAlias"

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 413797
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 413798
    goto :goto_0

    .line 413799
    :cond_2
    iget-object v0, p0, LX/2TA;->b:Landroid/content/pm/PackageManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, p0, LX/2TA;->c:Ljava/lang/String;

    const-string v4, "com.facebook.timeline.stagingground.ProfilePictureShareActivityAlias"

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_1
.end method
