.class public LX/29E;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0kb;

.field public final b:LX/0qj;


# direct methods
.method public constructor <init>(LX/0kb;LX/0qj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375997
    iput-object p1, p0, LX/29E;->a:LX/0kb;

    .line 375998
    iput-object p2, p0, LX/29E;->b:LX/0qj;

    .line 375999
    return-void
.end method

.method public static final varargs a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p0    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376000
    invoke-static {p1}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 376001
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 376002
    invoke-interface {v0, p0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 376003
    :cond_0
    return-object v0
.end method

.method public static varargs a([Ljava/lang/String;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375989
    array-length v0, p0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 375990
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Map must have an even (or zero) number of parameters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375991
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 375992
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 375993
    aget-object v2, p0, v0

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p0, v3

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375994
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 375995
    :cond_1
    return-object v1
.end method

.method public static b(LX/0QB;)LX/29E;
    .locals 3

    .prologue
    .line 375970
    new-instance v2, LX/29E;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-static {p0}, LX/0qj;->b(LX/0QB;)LX/0qj;

    move-result-object v1

    check-cast v1, LX/0qj;

    invoke-direct {v2, v0, v1}, LX/29E;-><init>(LX/0kb;LX/0qj;)V

    .line 375971
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375972
    iget-object v0, p0, LX/29E;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    iget-object v1, p0, LX/29E;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->o()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    const/4 v2, 0x0

    .line 375973
    if-eqz v0, :cond_1

    .line 375974
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    .line 375975
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v4

    .line 375976
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v3

    .line 375977
    :goto_0
    if-eqz v1, :cond_0

    .line 375978
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 375979
    :cond_0
    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 375980
    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 375981
    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 375982
    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 375983
    const-string v6, "network_is_connected"

    iget-object v7, p0, LX/29E;->b:LX/0qj;

    invoke-virtual {v7}, LX/0qj;->a()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375984
    const-string v6, "network_type"

    invoke-interface {p1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375985
    const-string v5, "network_subtype"

    invoke-interface {p1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375986
    const-string v4, "network_extra_info"

    invoke-interface {p1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375987
    const-string v3, "network_wifi_rssi"

    invoke-interface {p1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375988
    return-void

    :cond_1
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto :goto_0
.end method
