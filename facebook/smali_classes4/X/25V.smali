.class public LX/25V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 369576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369577
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/25V;->a:LX/026;

    .line 369578
    return-void
.end method

.method public static a(LX/0QB;)LX/25V;
    .locals 3

    .prologue
    .line 369583
    const-class v1, LX/25V;

    monitor-enter v1

    .line 369584
    :try_start_0
    sget-object v0, LX/25V;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 369585
    sput-object v2, LX/25V;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 369586
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369587
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 369588
    new-instance v0, LX/25V;

    invoke-direct {v0}, LX/25V;-><init>()V

    .line 369589
    move-object v0, v0

    .line 369590
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 369591
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/25V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369592
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 369593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 369581
    iget-object v0, p0, LX/25V;->a:LX/026;

    invoke-virtual {v0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369582
    return-void
.end method

.method public final a(Ljava/lang/String;)[I
    .locals 1

    .prologue
    .line 369580
    iget-object v0, p0, LX/25V;->a:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 369579
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/25V;->a:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
