.class public LX/2O7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/2O9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2O9",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private b:LX/2OA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2OA",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2O9;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2O9",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 400462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400463
    iput-object p1, p0, LX/2O7;->a:LX/2O9;

    .line 400464
    new-instance v0, LX/2OA;

    invoke-direct {v0}, LX/2OA;-><init>()V

    iput-object v0, p0, LX/2O7;->b:LX/2OA;

    .line 400465
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 400466
    iget-object v0, p0, LX/2O7;->b:LX/2OA;

    new-instance v1, LX/2OD;

    invoke-direct {v1, p0, p1}, LX/2OD;-><init>(LX/2O7;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, LX/2OA;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 400467
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2O7;->b:LX/2OA;

    invoke-virtual {v0}, LX/2OA;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400468
    monitor-exit p0

    return-void

    .line 400469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
