.class public LX/2OY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:LX/2OW;

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/DdS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2OW;)V
    .locals 1

    .prologue
    .line 402364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402365
    iput-object p1, p0, LX/2OY;->a:LX/2OW;

    .line 402366
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OY;->b:LX/01J;

    .line 402367
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OY;->c:LX/01J;

    .line 402368
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OY;->d:LX/01J;

    .line 402369
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 3

    .prologue
    .line 402359
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402360
    iget-object v0, p0, LX/2OY;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402361
    if-eqz v0, :cond_0

    .line 402362
    iget-object v1, p0, LX/2OY;->c:LX/01J;

    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 402363
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402357
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402358
    iget-object v0, p0, LX/2OY;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 402370
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402371
    const/4 v0, 0x0

    iget-object v1, p0, LX/2OY;->d:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 402372
    iget-object v0, p0, LX/2OY;->d:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdS;

    .line 402373
    invoke-virtual {v0}, LX/DdS;->c()V

    .line 402374
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 402375
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 2

    .prologue
    .line 402353
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402354
    iget-object v0, p0, LX/2OY;->b:LX/01J;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402355
    iget-object v0, p0, LX/2OY;->c:LX/01J;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402356
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402350
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 402351
    invoke-virtual {p0, v0}, LX/2OY;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    goto :goto_0

    .line 402352
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402348
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402349
    iget-object v0, p0, LX/2OY;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/DdS;
    .locals 2

    .prologue
    .line 402340
    iget-object v0, p0, LX/2OY;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402341
    if-nez p1, :cond_0

    .line 402342
    const-string v0, "ThreadSummariesCache"

    const-string v1, "ensuring null threadId ThreadLocalState"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 402343
    :cond_0
    iget-object v0, p0, LX/2OY;->d:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdS;

    .line 402344
    if-nez v0, :cond_1

    .line 402345
    new-instance v0, LX/DdS;

    invoke-direct {v0, p1}, LX/DdS;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402346
    iget-object v1, p0, LX/2OY;->d:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402347
    :cond_1
    return-object v0
.end method
