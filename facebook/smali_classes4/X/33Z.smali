.class public LX/33Z;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/CS7;",
        "LX/CS6;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/33Z;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 493850
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 493851
    return-void
.end method

.method public static a(LX/0QB;)LX/33Z;
    .locals 3

    .prologue
    .line 493852
    sget-object v0, LX/33Z;->a:LX/33Z;

    if-nez v0, :cond_1

    .line 493853
    const-class v1, LX/33Z;

    monitor-enter v1

    .line 493854
    :try_start_0
    sget-object v0, LX/33Z;->a:LX/33Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 493855
    if-eqz v2, :cond_0

    .line 493856
    :try_start_1
    new-instance v0, LX/33Z;

    invoke-direct {v0}, LX/33Z;-><init>()V

    .line 493857
    move-object v0, v0

    .line 493858
    sput-object v0, LX/33Z;->a:LX/33Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493859
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 493860
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 493861
    :cond_1
    sget-object v0, LX/33Z;->a:LX/33Z;

    return-object v0

    .line 493862
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 493863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
