.class public LX/2JA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0dz;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/1Ck;

.field public final d:LX/0lB;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27j;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/03V;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dz;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ck;LX/0lB;LX/0Ot;LX/03V;)V
    .locals 0
    .param p5    # LX/0Ot;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0dz;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1Ck;",
            "LX/0lB;",
            "LX/0Ot",
            "<",
            "LX/27j;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392586
    iput-object p1, p0, LX/2JA;->a:LX/0dz;

    .line 392587
    iput-object p2, p0, LX/2JA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 392588
    iput-object p3, p0, LX/2JA;->c:LX/1Ck;

    .line 392589
    iput-object p4, p0, LX/2JA;->d:LX/0lB;

    .line 392590
    iput-object p5, p0, LX/2JA;->e:LX/0Ot;

    .line 392591
    iput-object p6, p0, LX/2JA;->f:LX/03V;

    .line 392592
    return-void
.end method

.method public static a(LX/0QB;)LX/2JA;
    .locals 1

    .prologue
    .line 392584
    invoke-static {p0}, LX/2JA;->b(LX/0QB;)LX/2JA;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2JA;
    .locals 7

    .prologue
    .line 392593
    new-instance v0, LX/2JA;

    invoke-static {p0}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v1

    check-cast v1, LX/0dz;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-interface {p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v5

    const/16 v6, 0xc65

    invoke-static {v5, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct/range {v0 .. v6}, LX/2JA;-><init>(LX/0dz;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ck;LX/0lB;LX/0Ot;LX/03V;)V

    .line 392594
    return-object v0
.end method

.method private d()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 392574
    iget-object v0, p0, LX/2JA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/288;->b:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392575
    :try_start_0
    iget-object v2, p0, LX/2JA;->d:LX/0lB;

    invoke-virtual {v2}, LX/0lD;->b()LX/0lp;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0lp;->a(Ljava/lang/String;)LX/15w;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 392576
    :try_start_1
    new-instance v0, LX/28D;

    invoke-direct {v0, p0}, LX/28D;-><init>(LX/2JA;)V

    invoke-virtual {v3, v0}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 392577
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 392578
    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, LX/15w;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 392579
    :cond_0
    :goto_0
    return-object v0

    .line 392580
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 392581
    :catchall_0
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    :goto_1
    if-eqz v3, :cond_1

    if-eqz v2, :cond_2

    :try_start_4
    invoke-virtual {v3}, LX/15w;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_2
    :try_start_5
    throw v0

    .line 392582
    :catch_1
    move-object v0, v1

    goto :goto_0

    .line 392583
    :catch_2
    move-exception v3

    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, LX/15w;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 392569
    invoke-direct {p0}, LX/2JA;->d()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2JA;->g:LX/0Px;

    .line 392570
    iget-object v0, p0, LX/2JA;->g:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2JA;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 392571
    :goto_0
    return-void

    .line 392572
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 392573
    iget-object v1, p0, LX/2JA;->c:LX/1Ck;

    sget-object v2, LX/28H;->FETCH_LANGUAGE_LIST:LX/28H;

    invoke-virtual {v2}, LX/28H;->name()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/28I;

    invoke-direct {v3, p0}, LX/28I;-><init>(LX/2JA;)V

    new-instance v4, LX/28J;

    invoke-direct {v4, p0, v0}, LX/28J;-><init>(LX/2JA;Ljava/util/List;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392564
    iget-object v0, p0, LX/2JA;->g:LX/0Px;

    if-eqz v0, :cond_0

    .line 392565
    iget-object v0, p0, LX/2JA;->g:LX/0Px;

    .line 392566
    :goto_0
    return-object v0

    .line 392567
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 392568
    goto :goto_0
.end method
