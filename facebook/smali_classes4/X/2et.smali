.class public LX/2et;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/17Q;

.field private final b:LX/0Zb;

.field private final c:LX/23P;

.field private final d:Landroid/content/res/Resources;

.field private final e:LX/2eu;


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;LX/23P;Landroid/content/res/Resources;LX/2eu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445974
    iput-object p1, p0, LX/2et;->a:LX/17Q;

    .line 445975
    iput-object p2, p0, LX/2et;->b:LX/0Zb;

    .line 445976
    iput-object p3, p0, LX/2et;->c:LX/23P;

    .line 445977
    iput-object p4, p0, LX/2et;->d:Landroid/content/res/Resources;

    .line 445978
    iput-object p5, p0, LX/2et;->e:LX/2eu;

    .line 445979
    return-void
.end method

.method public static a(LX/0QB;)LX/2et;
    .locals 9

    .prologue
    .line 445980
    const-class v1, LX/2et;

    monitor-enter v1

    .line 445981
    :try_start_0
    sget-object v0, LX/2et;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445982
    sput-object v2, LX/2et;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445983
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445984
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445985
    new-instance v3, LX/2et;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v6

    check-cast v6, LX/23P;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2eu;->a(LX/0QB;)LX/2eu;

    move-result-object v8

    check-cast v8, LX/2eu;

    invoke-direct/range {v3 .. v8}, LX/2et;-><init>(LX/17Q;LX/0Zb;LX/23P;Landroid/content/res/Resources;LX/2eu;)V

    .line 445986
    move-object v0, v3

    .line 445987
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445988
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2et;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445989
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 445995
    iget-object v0, p0, LX/2et;->c:LX/23P;

    iget-object v1, p0, LX/2et;->d:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1Pr;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ">(TE;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 445991
    new-instance v0, LX/2e7;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/2e8;

    sget-object v3, LX/3ka;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, v4, v3}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-direct {v0, v1, v2}, LX/2e7;-><init>(Ljava/lang/String;LX/2e8;)V

    .line 445992
    invoke-interface {p0, v0, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    .line 445993
    new-instance v1, LX/2e8;

    sget-object v2, LX/3ka;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v1, v4, v2}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-interface {p0, v0, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 445994
    return-void
.end method

.method private static a(LX/2et;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/1Pc;LX/2dx;LX/3mj;)V
    .locals 13
    .param p4    # LX/1Pc;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/2dx;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "LX/2ep;",
            "TE;",
            "LX/2dx;",
            "LX/3mj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 445956
    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 445957
    if-nez v2, :cond_0

    .line 445958
    :goto_0
    return-void

    .line 445959
    :cond_0
    new-instance v4, LX/2en;

    invoke-interface {p2}, LX/1Fa;->k()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    iget-object v3, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v4, v1, v3}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 445960
    move-object/from16 v0, p3

    iget-object v1, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    move v11, v1

    .line 445961
    :goto_1
    if-eqz v11, :cond_1

    .line 445962
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v1}, LX/17Q;->g(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 445963
    iget-object v3, p0, LX/2et;->b:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 445964
    if-eqz p5, :cond_1

    .line 445965
    invoke-virtual/range {p5 .. p5}, LX/2dx;->a()V

    :cond_1
    move-object/from16 v1, p4

    .line 445966
    check-cast v1, LX/1Pr;

    invoke-static {v1, p1}, LX/2et;->a(LX/1Pr;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 445967
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v12

    sget-object v8, LX/2h7;->PYMK_FEED:LX/2h7;

    move-object/from16 v0, p3

    iget-object v9, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    new-instance v1, LX/DFI;

    move-object v2, p0

    move-object/from16 v3, p4

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, LX/DFI;-><init>(LX/2et;LX/1Pc;LX/2en;LX/2ep;LX/3mj;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    move-object/from16 v5, p4

    move-object v6, v10

    move-object v7, v12

    move-object v10, v1

    invoke-interface/range {v5 .. v10}, LX/1Pc;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v2

    move-object/from16 v1, p4

    .line 445968
    check-cast v1, LX/1Pr;

    new-instance v3, LX/2ep;

    iget-object v5, v2, LX/5Oh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-boolean v2, v2, LX/5Oh;->b:Z

    invoke-direct {v3, v5, v2}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-interface {v1, v4, v3}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 445969
    if-eqz v11, :cond_3

    if-eqz p6, :cond_3

    .line 445970
    const/4 v1, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, LX/3mj;->a(Z)V

    goto :goto_0

    .line 445971
    :cond_2
    const/4 v1, 0x0

    move v11, v1

    goto :goto_1

    .line 445972
    :cond_3
    check-cast p4, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z
    .locals 1

    .prologue
    .line 445928
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/2dx;LX/1Pc;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "LX/2ep;",
            "LX/2dx;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 445954
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-static/range {v0 .. v6}, LX/2et;->a(LX/2et;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/1Pc;LX/2dx;LX/3mj;)V

    .line 445955
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/3mj;LX/1Pc;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "LX/2ep;",
            "LX/3mj;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 445952
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v6, p4

    invoke-static/range {v0 .. v6}, LX/2et;->a(LX/2et;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/1Pc;LX/2dx;LX/3mj;)V

    .line 445953
    return-void
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/2f5;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 445929
    iget-object v0, p0, LX/2et;->e:LX/2eu;

    .line 445930
    iget-boolean v1, v0, LX/2eu;->a:Z

    move v0, v1

    .line 445931
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 445932
    sget-object v1, LX/2f4;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v6, v5

    move v3, v5

    move-object v1, v2

    .line 445933
    :goto_0
    new-instance v0, LX/2f5;

    invoke-direct/range {v0 .. v6}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    return-object v0

    .line 445934
    :pswitch_0
    if-eqz v0, :cond_0

    .line 445935
    iget-object v0, p0, LX/2et;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 445936
    const v0, 0x7f020b08

    .line 445937
    :goto_1
    const v2, 0x7f080f7b

    invoke-direct {p0, v2}, LX/2et;->a(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 445938
    const v2, 0x7f080f7c

    invoke-direct {p0, v2}, LX/2et;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 445939
    const v3, 0x7f020b77

    move v6, v0

    move-object v7, v1

    move-object v1, v4

    move-object v4, v7

    .line 445940
    goto :goto_0

    .line 445941
    :cond_0
    iget-object v0, p0, LX/2et;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a00a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 445942
    const v0, 0x7f020b0b

    goto :goto_1

    .line 445943
    :pswitch_1
    const v0, 0x7f080f7d

    invoke-direct {p0, v0}, LX/2et;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 445944
    const v0, 0x7f080017

    invoke-direct {p0, v0}, LX/2et;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 445945
    iget-object v0, p0, LX/2et;->d:Landroid/content/res/Resources;

    const v3, 0x7f0a00a3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 445946
    const v6, 0x7f020b0b

    .line 445947
    const v3, 0x7f02089f

    goto :goto_0

    .line 445948
    :pswitch_2
    iget-object v0, p0, LX/2et;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a00a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 445949
    const v6, 0x7f020b0b

    .line 445950
    const v0, 0x7f080f76

    invoke-direct {p0, v0}, LX/2et;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 445951
    const v3, 0x7f020b7c

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
