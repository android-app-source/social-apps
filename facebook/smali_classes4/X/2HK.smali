.class public LX/2HK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/2HK;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2HR;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2Gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390279
    const-class v0, LX/2HK;

    sput-object v0, LX/2HK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/2Gc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2HR;",
            ">;",
            "LX/2Gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390276
    iput-object p1, p0, LX/2HK;->b:LX/0Ot;

    .line 390277
    iput-object p2, p0, LX/2HK;->c:LX/2Gc;

    .line 390278
    return-void
.end method

.method public static a(LX/0QB;)LX/2HK;
    .locals 5

    .prologue
    .line 390262
    sget-object v0, LX/2HK;->d:LX/2HK;

    if-nez v0, :cond_1

    .line 390263
    const-class v1, LX/2HK;

    monitor-enter v1

    .line 390264
    :try_start_0
    sget-object v0, LX/2HK;->d:LX/2HK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390265
    if-eqz v2, :cond_0

    .line 390266
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390267
    new-instance v4, LX/2HK;

    const/16 v3, 0xff9

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v3

    check-cast v3, LX/2Gc;

    invoke-direct {v4, p0, v3}, LX/2HK;-><init>(LX/0Ot;LX/2Gc;)V

    .line 390268
    move-object v0, v4

    .line 390269
    sput-object v0, LX/2HK;->d:LX/2HK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390270
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390271
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390272
    :cond_1
    sget-object v0, LX/2HK;->d:LX/2HK;

    return-object v0

    .line 390273
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390274
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 390261
    iget-object v0, p0, LX/2HK;->c:LX/2Gc;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Ge;
    .locals 1

    .prologue
    .line 390260
    sget-object v0, LX/2Ge;->FBNS:LX/2Ge;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 390257
    invoke-direct {p0}, LX/2HK;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390258
    :goto_0
    return-void

    .line 390259
    :cond_0
    iget-object v0, p0, LX/2HK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HR;

    invoke-virtual {v0, p1}, LX/2HR;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 390248
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 390254
    invoke-direct {p0}, LX/2HK;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390255
    :goto_0
    return-void

    .line 390256
    :cond_0
    iget-object v0, p0, LX/2HK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HR;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HR;->a(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 390251
    invoke-direct {p0}, LX/2HK;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390252
    :goto_0
    return-void

    .line 390253
    :cond_0
    iget-object v0, p0, LX/2HK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2HR;->a(Z)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 390249
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2HK;->a(Ljava/lang/String;)V

    .line 390250
    return-void
.end method
