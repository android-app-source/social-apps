.class public LX/39c;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2vJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/39c",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2vJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523497
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 523498
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/39c;->b:LX/0Zi;

    .line 523499
    iput-object p1, p0, LX/39c;->a:LX/0Ot;

    .line 523500
    return-void
.end method

.method public static a(LX/0QB;)LX/39c;
    .locals 4

    .prologue
    .line 523486
    const-class v1, LX/39c;

    monitor-enter v1

    .line 523487
    :try_start_0
    sget-object v0, LX/39c;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523488
    sput-object v2, LX/39c;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523489
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523490
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523491
    new-instance v3, LX/39c;

    const/16 p0, 0x860

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/39c;-><init>(LX/0Ot;)V

    .line 523492
    move-object v0, v3

    .line 523493
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523494
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523495
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523496
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 523406
    check-cast p2, LX/39t;

    .line 523407
    iget-object v0, p0, LX/39c;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vJ;

    iget-object v2, p2, LX/39t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/39t;->b:LX/20X;

    iget-object v4, p2, LX/39t;->c:LX/1Wk;

    iget-boolean v5, p2, LX/39t;->d:Z

    iget-object v6, p2, LX/39t;->e:LX/1Pr;

    iget v7, p2, LX/39t;->f:I

    iget v8, p2, LX/39t;->g:I

    iget v9, p2, LX/39t;->h:I

    iget v10, p2, LX/39t;->i:F

    iget v11, p2, LX/39t;->j:F

    iget v12, p2, LX/39t;->k:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, LX/2vJ;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/20X;LX/1Wk;ZLX/1Pr;IIIFFF)LX/1Dg;

    move-result-object v0

    .line 523408
    return-object v0
.end method

.method public final a(LX/1De;II)LX/39u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II)",
            "LX/39c",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523479
    new-instance v0, LX/39t;

    invoke-direct {v0, p0}, LX/39t;-><init>(LX/39c;)V

    .line 523480
    iget-object v1, p0, LX/39c;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/39u;

    .line 523481
    if-nez v1, :cond_0

    .line 523482
    new-instance v1, LX/39u;

    invoke-direct {v1, p0}, LX/39u;-><init>(LX/39c;)V

    .line 523483
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/39u;->a$redex0(LX/39u;LX/1De;IILX/39t;)V

    .line 523484
    move-object v0, v1

    .line 523485
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523477
    invoke-static {}, LX/1dS;->b()V

    .line 523478
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/39u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/39c",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 523476
    invoke-virtual {p0, p1, v0, v0}, LX/39c;->a(LX/1De;II)LX/39u;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 523409
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 523410
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 523411
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 523412
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 523413
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 523414
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 523415
    iget-object v0, p0, LX/39c;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 p1, 0x0

    const/4 v8, 0x0

    .line 523416
    sget-object v7, LX/03r;->BasicFooterButtonComponent:[I

    invoke-virtual {v0, v7, v8}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 523417
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v10

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_6

    .line 523418
    invoke-virtual {v9, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v11

    .line 523419
    const/16 p0, 0x5

    if-ne v11, p0, :cond_1

    .line 523420
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 523421
    iput-object v11, v1, LX/1np;->a:Ljava/lang/Object;

    .line 523422
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 523423
    :cond_1
    const/16 p0, 0x6

    if-ne v11, p0, :cond_2

    .line 523424
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 523425
    iput-object v11, v2, LX/1np;->a:Ljava/lang/Object;

    .line 523426
    goto :goto_1

    .line 523427
    :cond_2
    const/16 p0, 0x2

    if-ne v11, p0, :cond_3

    .line 523428
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 523429
    iput-object v11, v4, LX/1np;->a:Ljava/lang/Object;

    .line 523430
    goto :goto_1

    .line 523431
    :cond_3
    const/16 p0, 0x3

    if-ne v11, p0, :cond_4

    .line 523432
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 523433
    iput-object v11, v5, LX/1np;->a:Ljava/lang/Object;

    .line 523434
    goto :goto_1

    .line 523435
    :cond_4
    const/16 p0, 0x4

    if-ne v11, p0, :cond_5

    .line 523436
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 523437
    iput-object v11, v6, LX/1np;->a:Ljava/lang/Object;

    .line 523438
    goto :goto_1

    .line 523439
    :cond_5
    const/16 p0, 0x1

    if-ne v11, p0, :cond_0

    .line 523440
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 523441
    iput-object v11, v3, LX/1np;->a:Ljava/lang/Object;

    .line 523442
    goto :goto_1

    .line 523443
    :cond_6
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 523444
    check-cast p2, LX/39t;

    .line 523445
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523446
    if-eqz v0, :cond_7

    .line 523447
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523448
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/39t;->f:I

    .line 523449
    :cond_7
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 523450
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523451
    if-eqz v0, :cond_8

    .line 523452
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523453
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/39t;->g:I

    .line 523454
    :cond_8
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 523455
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523456
    if-eqz v0, :cond_9

    .line 523457
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523458
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/39t;->h:I

    .line 523459
    :cond_9
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 523460
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523461
    if-eqz v0, :cond_a

    .line 523462
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523463
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/39t;->i:F

    .line 523464
    :cond_a
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 523465
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523466
    if-eqz v0, :cond_b

    .line 523467
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523468
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/39t;->j:F

    .line 523469
    :cond_b
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 523470
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523471
    if-eqz v0, :cond_c

    .line 523472
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 523473
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/39t;->k:F

    .line 523474
    :cond_c
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 523475
    return-void
.end method
