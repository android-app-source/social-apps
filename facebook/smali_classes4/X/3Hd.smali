.class public LX/3Hd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field private static volatile j:LX/3Hd;


# instance fields
.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Landroid/os/Handler;

.field public final f:Landroid/content/Context;

.field public final g:LX/3He;

.field public h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 544250
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "facecastdisplay.debugoverlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 544251
    sput-object v0, LX/3Hd;->a:LX/0Tn;

    const-string v1, "positionX"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Hd;->b:LX/0Tn;

    .line 544252
    sget-object v0, LX/3Hd;->a:LX/0Tn;

    const-string v1, "positionY"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Hd;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 544244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 544245
    iput-object p1, p0, LX/3Hd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 544246
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/3Hd;->e:Landroid/os/Handler;

    .line 544247
    iput-object p2, p0, LX/3Hd;->f:Landroid/content/Context;

    .line 544248
    new-instance v0, LX/3He;

    invoke-direct {v0, p0}, LX/3He;-><init>(LX/3Hd;)V

    iput-object v0, p0, LX/3Hd;->g:LX/3He;

    .line 544249
    return-void
.end method

.method public static a(LX/0QB;)LX/3Hd;
    .locals 5

    .prologue
    .line 544253
    sget-object v0, LX/3Hd;->j:LX/3Hd;

    if-nez v0, :cond_1

    .line 544254
    const-class v1, LX/3Hd;

    monitor-enter v1

    .line 544255
    :try_start_0
    sget-object v0, LX/3Hd;->j:LX/3Hd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 544256
    if-eqz v2, :cond_0

    .line 544257
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 544258
    new-instance p0, LX/3Hd;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/3Hd;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V

    .line 544259
    move-object v0, p0

    .line 544260
    sput-object v0, LX/3Hd;->j:LX/3Hd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544261
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 544262
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 544263
    :cond_1
    sget-object v0, LX/3Hd;->j:LX/3Hd;

    return-object v0

    .line 544264
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 544265
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/3Hd;)LX/Acp;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 544239
    iget-object v0, p0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    if-nez v0, :cond_0

    .line 544240
    const/4 v0, 0x0

    .line 544241
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544242
    iget-object p0, v0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    move-object v0, p0

    .line 544243
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 544220
    invoke-virtual {p0}, LX/3Hd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 544221
    :goto_0
    return-void

    .line 544222
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 544223
    if-nez v0, :cond_1

    .line 544224
    iget-object v0, p0, LX/3Hd;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayViewController$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayViewController$1;-><init>(LX/3Hd;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    const v2, 0x54ec6cc0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 544225
    :cond_1
    invoke-static {p0}, LX/3Hd;->e(LX/3Hd;)LX/Acp;

    move-result-object v0

    .line 544226
    if-nez v0, :cond_3

    .line 544227
    invoke-virtual {p0}, LX/3Hd;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/3Hd;->i:Z

    if-nez v0, :cond_2

    invoke-static {p0}, LX/3Hd;->e(LX/3Hd;)LX/Acp;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 544228
    :cond_2
    :goto_2
    goto :goto_0

    .line 544229
    :cond_3
    iget-object v1, v0, LX/Acp;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;

    .line 544230
    if-nez v1, :cond_4

    .line 544231
    iget-object v1, v0, LX/Acp;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f0305c3

    const/4 p0, 0x0

    invoke-virtual {v1, v2, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;

    .line 544232
    invoke-virtual {v1, p1}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->setTitle(Ljava/lang/CharSequence;)V

    .line 544233
    iput-object v0, v1, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->e:LX/Acl;

    .line 544234
    invoke-virtual {v0, v1}, LX/Acp;->addView(Landroid/view/View;)V

    .line 544235
    iget-object v2, v0, LX/Acp;->a:Ljava/util/Map;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544236
    :cond_4
    invoke-virtual {v1, p2, p3}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 544237
    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 544238
    :cond_6
    iget-object v0, p0, LX/3Hd;->f:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/3Hd;->f:Landroid/content/Context;

    const-class p1, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, LX/3Hd;->g:LX/3He;

    const/4 p1, 0x1

    const p2, -0x5936068a

    invoke-static {v0, v1, v2, p1, p2}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v0

    iput-boolean v0, p0, LX/3Hd;->i:Z

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 544219
    iget-object v0, p0, LX/3Hd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1CA;->z:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
