.class public final enum LX/2HQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2HQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2HQ;

.field public static final enum ALWAYS:LX/2HQ;

.field public static final enum APP_USE:LX/2HQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 390376
    new-instance v0, LX/2HQ;

    const-string v1, "APP_USE"

    invoke-direct {v0, v1, v2}, LX/2HQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2HQ;->APP_USE:LX/2HQ;

    .line 390377
    new-instance v0, LX/2HQ;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v3}, LX/2HQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2HQ;->ALWAYS:LX/2HQ;

    .line 390378
    const/4 v0, 0x2

    new-array v0, v0, [LX/2HQ;

    sget-object v1, LX/2HQ;->APP_USE:LX/2HQ;

    aput-object v1, v0, v2

    sget-object v1, LX/2HQ;->ALWAYS:LX/2HQ;

    aput-object v1, v0, v3

    sput-object v0, LX/2HQ;->$VALUES:[LX/2HQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 390375
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2HQ;
    .locals 1

    .prologue
    .line 390373
    const-class v0, LX/2HQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2HQ;

    return-object v0
.end method

.method public static values()[LX/2HQ;
    .locals 1

    .prologue
    .line 390374
    sget-object v0, LX/2HQ;->$VALUES:[LX/2HQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2HQ;

    return-object v0
.end method
