.class public final enum LX/2Fs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Fs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Fs;

.field public static final enum SERVICE_DISABLED:LX/2Fs;

.field public static final enum SERVICE_ENABLED:LX/2Fs;

.field public static final enum SERVICE_INVALID:LX/2Fs;

.field public static final enum SERVICE_MISSING:LX/2Fs;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 387365
    new-instance v0, LX/2Fs;

    const-string v1, "SERVICE_ENABLED"

    invoke-direct {v0, v1, v2}, LX/2Fs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Fs;->SERVICE_ENABLED:LX/2Fs;

    .line 387366
    new-instance v0, LX/2Fs;

    const-string v1, "SERVICE_DISABLED"

    invoke-direct {v0, v1, v3}, LX/2Fs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Fs;->SERVICE_DISABLED:LX/2Fs;

    .line 387367
    new-instance v0, LX/2Fs;

    const-string v1, "SERVICE_INVALID"

    invoke-direct {v0, v1, v4}, LX/2Fs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Fs;->SERVICE_INVALID:LX/2Fs;

    .line 387368
    new-instance v0, LX/2Fs;

    const-string v1, "SERVICE_MISSING"

    invoke-direct {v0, v1, v5}, LX/2Fs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    .line 387369
    const/4 v0, 0x4

    new-array v0, v0, [LX/2Fs;

    sget-object v1, LX/2Fs;->SERVICE_ENABLED:LX/2Fs;

    aput-object v1, v0, v2

    sget-object v1, LX/2Fs;->SERVICE_DISABLED:LX/2Fs;

    aput-object v1, v0, v3

    sget-object v1, LX/2Fs;->SERVICE_INVALID:LX/2Fs;

    aput-object v1, v0, v4

    sget-object v1, LX/2Fs;->SERVICE_MISSING:LX/2Fs;

    aput-object v1, v0, v5

    sput-object v0, LX/2Fs;->$VALUES:[LX/2Fs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 387370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Fs;
    .locals 1

    .prologue
    .line 387371
    const-class v0, LX/2Fs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Fs;

    return-object v0
.end method

.method public static values()[LX/2Fs;
    .locals 1

    .prologue
    .line 387372
    sget-object v0, LX/2Fs;->$VALUES:[LX/2Fs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Fs;

    return-object v0
.end method
