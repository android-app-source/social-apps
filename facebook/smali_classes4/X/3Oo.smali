.class public final enum LX/3Oo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Oo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Oo;

.field public static final enum NULL_STATE:LX/3Oo;

.field public static final enum PEOPLE_TAB:LX/3Oo;

.field public static final enum SEARCH:LX/3Oo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560798
    new-instance v0, LX/3Oo;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v2}, LX/3Oo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oo;->SEARCH:LX/3Oo;

    .line 560799
    new-instance v0, LX/3Oo;

    const-string v1, "NULL_STATE"

    invoke-direct {v0, v1, v3}, LX/3Oo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oo;->NULL_STATE:LX/3Oo;

    .line 560800
    new-instance v0, LX/3Oo;

    const-string v1, "PEOPLE_TAB"

    invoke-direct {v0, v1, v4}, LX/3Oo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oo;->PEOPLE_TAB:LX/3Oo;

    .line 560801
    const/4 v0, 0x3

    new-array v0, v0, [LX/3Oo;

    sget-object v1, LX/3Oo;->SEARCH:LX/3Oo;

    aput-object v1, v0, v2

    sget-object v1, LX/3Oo;->NULL_STATE:LX/3Oo;

    aput-object v1, v0, v3

    sget-object v1, LX/3Oo;->PEOPLE_TAB:LX/3Oo;

    aput-object v1, v0, v4

    sput-object v0, LX/3Oo;->$VALUES:[LX/3Oo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560802
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Oo;
    .locals 1

    .prologue
    .line 560803
    const-class v0, LX/3Oo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Oo;

    return-object v0
.end method

.method public static values()[LX/3Oo;
    .locals 1

    .prologue
    .line 560804
    sget-object v0, LX/3Oo;->$VALUES:[LX/3Oo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Oo;

    return-object v0
.end method
