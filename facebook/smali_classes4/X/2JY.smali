.class public LX/2JY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field public b:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/10M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2JZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 393043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393044
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 393045
    iget-object v1, p0, LX/2JY;->c:LX/10M;

    invoke-virtual {v1}, LX/10M;->d()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, LX/2JY;->e:LX/2JZ;

    .line 393046
    iget-object v2, v1, LX/2JZ;->a:LX/10M;

    invoke-virtual {v2}, LX/10M;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 393047
    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2JZ;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 393048
    const/4 v2, 0x1

    .line 393049
    :goto_0
    move v1, v2

    .line 393050
    if-eqz v1, :cond_1

    .line 393051
    iget-object v3, p0, LX/2JY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1CA;->r:LX/0Tn;

    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 393052
    iget-object v5, p0, LX/2JY;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/32 v7, 0x6ddd00

    add-long/2addr v3, v7

    cmp-long v3, v5, v3

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 393053
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2JY;->f:LX/0Uh;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 393054
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393055
    iget-object v0, p0, LX/2JY;->g:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 393056
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 393057
    const-wide/32 v0, 0x6ddd00

    return-wide v0
.end method
