.class public final enum LX/2K4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2K4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2K4;

.field public static final enum IS_NOT_SESSIONLESS:LX/2K4;

.field public static final enum IS_SESSIONLESS:LX/2K4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 393364
    new-instance v0, LX/2K4;

    const-string v1, "IS_SESSIONLESS"

    invoke-direct {v0, v1, v2}, LX/2K4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2K4;->IS_SESSIONLESS:LX/2K4;

    .line 393365
    new-instance v0, LX/2K4;

    const-string v1, "IS_NOT_SESSIONLESS"

    invoke-direct {v0, v1, v3}, LX/2K4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2K4;->IS_NOT_SESSIONLESS:LX/2K4;

    .line 393366
    const/4 v0, 0x2

    new-array v0, v0, [LX/2K4;

    sget-object v1, LX/2K4;->IS_SESSIONLESS:LX/2K4;

    aput-object v1, v0, v2

    sget-object v1, LX/2K4;->IS_NOT_SESSIONLESS:LX/2K4;

    aput-object v1, v0, v3

    sput-object v0, LX/2K4;->$VALUES:[LX/2K4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 393367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2K4;
    .locals 1

    .prologue
    .line 393368
    const-class v0, LX/2K4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2K4;

    return-object v0
.end method

.method public static values()[LX/2K4;
    .locals 1

    .prologue
    .line 393369
    sget-object v0, LX/2K4;->$VALUES:[LX/2K4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2K4;

    return-object v0
.end method
