.class public LX/2ae;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/interstitial/api/FetchInterstitialsParams;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/2af;

.field private final b:LX/119;


# direct methods
.method public constructor <init>(LX/2af;LX/119;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424783
    iput-object p1, p0, LX/2ae;->a:LX/2af;

    .line 424784
    iput-object p2, p0, LX/2ae;->b:LX/119;

    .line 424785
    return-void
.end method

.method public static b(LX/0QB;)LX/2ae;
    .locals 3

    .prologue
    .line 424786
    new-instance v2, LX/2ae;

    .line 424787
    new-instance v0, LX/2af;

    invoke-direct {v0}, LX/2af;-><init>()V

    .line 424788
    move-object v0, v0

    .line 424789
    move-object v0, v0

    .line 424790
    check-cast v0, LX/2af;

    invoke-static {p0}, LX/119;->a(LX/0QB;)LX/119;

    move-result-object v1

    check-cast v1, LX/119;

    invoke-direct {v2, v0, v1}, LX/2ae;-><init>(LX/2af;LX/119;)V

    .line 424791
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 424792
    check-cast p1, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    .line 424793
    const-string v0, "fetch_interstitials"

    .line 424794
    iget-object v1, p1, Lcom/facebook/interstitial/api/FetchInterstitialsParams;->a:LX/0Px;

    invoke-static {v1}, LX/2bQ;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 424795
    const-string v2, "SELECT %s FROM %s WHERE nux_id IN %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string p0, "rank, nux_id, nux_data"

    aput-object p0, v3, v4

    const/4 v4, 0x1

    const-string p0, "user_nux_status"

    aput-object p0, v3, v4

    const/4 v4, 0x2

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 424796
    move-object v1, v1

    .line 424797
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v3, LX/14S;->JSONPARSER:LX/14S;

    .line 424798
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v9

    .line 424799
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "q"

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424800
    new-instance v4, LX/14N;

    const-string v6, "GET"

    const-string v7, "fql"

    move-object v5, v0

    move-object v8, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    move-object v0, v4

    .line 424801
    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 424802
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 424803
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 424804
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 424805
    :goto_0
    invoke-virtual {v0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 424806
    :cond_0
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 424807
    :cond_1
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 424808
    iget-object v1, p0, LX/2ae;->b:LX/119;

    sget-object v2, LX/16M;->SERVER:LX/16M;

    invoke-virtual {v1, v2, v0}, LX/119;->a(LX/16M;LX/15w;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
