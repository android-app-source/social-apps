.class public LX/2Yr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0ug;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Wd;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421709
    const-class v0, LX/2Yr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Yr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;LX/0Wd;LX/0Ot;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0ug;",
            ">;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 421702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421703
    iput-object p1, p0, LX/2Yr;->b:Ljava/util/Set;

    .line 421704
    iput-object p2, p0, LX/2Yr;->c:LX/0Wd;

    .line 421705
    iput-object p3, p0, LX/2Yr;->d:LX/0Ot;

    .line 421706
    iput-object p4, p0, LX/2Yr;->e:Ljava/util/Map;

    .line 421707
    iput-object p5, p0, LX/2Yr;->f:Ljava/util/Map;

    .line 421708
    return-void
.end method


# virtual methods
.method public final onConfigChanged([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 421699
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 421700
    :cond_0
    :goto_0
    return-void

    .line 421701
    :cond_1
    iget-object v0, p0, LX/2Yr;->c:LX/0Wd;

    new-instance v1, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;-><init>(LX/2Yr;[Ljava/lang/String;)V

    const v2, -0x6a2345ef

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
