.class public final LX/38P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/37k;

.field public final c:Landroid/os/Handler;

.field private final d:Landroid/content/pm/PackageManager;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3Fm;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public final g:Landroid/content/BroadcastReceiver;

.field public final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/37k;)V
    .locals 1

    .prologue
    .line 520924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520925
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/38P;->e:Ljava/util/ArrayList;

    .line 520926
    new-instance v0, LX/38Q;

    invoke-direct {v0, p0}, LX/38Q;-><init>(LX/38P;)V

    iput-object v0, p0, LX/38P;->g:Landroid/content/BroadcastReceiver;

    .line 520927
    new-instance v0, Landroid/support/v7/media/RegisteredMediaRouteProviderWatcher$2;

    invoke-direct {v0, p0}, Landroid/support/v7/media/RegisteredMediaRouteProviderWatcher$2;-><init>(LX/38P;)V

    iput-object v0, p0, LX/38P;->h:Ljava/lang/Runnable;

    .line 520928
    iput-object p1, p0, LX/38P;->a:Landroid/content/Context;

    .line 520929
    iput-object p2, p0, LX/38P;->b:LX/37k;

    .line 520930
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/38P;->c:Landroid/os/Handler;

    .line 520931
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/38P;->d:Landroid/content/pm/PackageManager;

    .line 520932
    return-void
.end method

.method public static b(LX/38P;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 520933
    iget-boolean v1, p0, LX/38P;->f:Z

    if-nez v1, :cond_1

    .line 520934
    :cond_0
    return-void

    .line 520935
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.MediaRouteProviderService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 520936
    iget-object v2, p0, LX/38P;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 520937
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 520938
    if-eqz v0, :cond_7

    .line 520939
    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v4, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    .line 520940
    iget-object v5, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 520941
    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_9

    .line 520942
    iget-object v5, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3Fm;

    .line 520943
    iget-object v8, v5, LX/3Fm;->b:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, v5, LX/3Fm;->b:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v8, 0x1

    :goto_2
    move v5, v8

    .line 520944
    if-eqz v5, :cond_8

    move v5, v6

    .line 520945
    :goto_3
    move v3, v5

    .line 520946
    if-gez v3, :cond_2

    .line 520947
    new-instance v3, LX/3Fm;

    iget-object v4, p0, LX/38P;->a:Landroid/content/Context;

    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v6, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4, v5}, LX/3Fm;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 520948
    invoke-virtual {v3}, LX/3Fm;->f()V

    .line 520949
    iget-object v4, p0, LX/38P;->e:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v4, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 520950
    iget-object v1, p0, LX/38P;->b:LX/37k;

    invoke-interface {v1, v3}, LX/37k;->a(LX/37v;)V

    move v1, v0

    .line 520951
    goto :goto_0

    :cond_2
    if-lt v3, v1, :cond_7

    .line 520952
    iget-object v0, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Fm;

    .line 520953
    invoke-virtual {v0}, LX/3Fm;->f()V

    .line 520954
    iget-object v4, v0, LX/3Fm;->g:LX/67N;

    if-nez v4, :cond_3

    invoke-static {v0}, LX/3Fm;->k(LX/3Fm;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 520955
    invoke-static {v0}, LX/3Fm;->m(LX/3Fm;)V

    .line 520956
    invoke-static {v0}, LX/3Fm;->l(LX/3Fm;)V

    .line 520957
    :cond_3
    iget-object v4, p0, LX/38P;->e:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, 0x1

    invoke-static {v4, v3, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    :goto_4
    move v1, v0

    .line 520958
    goto/16 :goto_0

    .line 520959
    :cond_4
    iget-object v0, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 520960
    iget-object v0, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_5
    if-lt v2, v1, :cond_0

    .line 520961
    iget-object v0, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Fm;

    .line 520962
    iget-object v3, p0, LX/38P;->b:LX/37k;

    invoke-interface {v3, v0}, LX/37k;->b(LX/37v;)V

    .line 520963
    iget-object v3, p0, LX/38P;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 520964
    iget-boolean v3, v0, LX/3Fm;->e:Z

    if-eqz v3, :cond_6

    .line 520965
    sget-boolean v3, LX/3Fm;->a:Z

    if-eqz v3, :cond_5

    .line 520966
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Stopping"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520967
    :cond_5
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/3Fm;->e:Z

    .line 520968
    invoke-static {v0}, LX/3Fm;->j(LX/3Fm;)V

    .line 520969
    :cond_6
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_4

    .line 520970
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 520971
    :cond_9
    const/4 v5, -0x1

    goto/16 :goto_3

    :cond_a
    const/4 v8, 0x0

    goto/16 :goto_2
.end method
