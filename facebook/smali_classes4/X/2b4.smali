.class public final LX/2b4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 428090
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 428091
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428092
    if-eqz v0, :cond_0

    .line 428093
    const-string v1, "feature_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428094
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428095
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 428096
    if-eqz v0, :cond_1

    .line 428097
    const-string v1, "gravity_learn_more_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428098
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428099
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428100
    if-eqz v0, :cond_2

    .line 428101
    const-string v1, "location_tracking_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428102
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428103
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428104
    if-eqz v0, :cond_3

    .line 428105
    const-string v1, "notifications_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428106
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428107
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428108
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 428160
    const/16 v29, 0x0

    .line 428161
    const/16 v28, 0x0

    .line 428162
    const/16 v27, 0x0

    .line 428163
    const/16 v26, 0x0

    .line 428164
    const/16 v25, 0x0

    .line 428165
    const/16 v24, 0x0

    .line 428166
    const/16 v23, 0x0

    .line 428167
    const/16 v22, 0x0

    .line 428168
    const/16 v21, 0x0

    .line 428169
    const/16 v20, 0x0

    .line 428170
    const/16 v19, 0x0

    .line 428171
    const/16 v18, 0x0

    .line 428172
    const/16 v17, 0x0

    .line 428173
    const/16 v16, 0x0

    .line 428174
    const/4 v15, 0x0

    .line 428175
    const/4 v14, 0x0

    .line 428176
    const/4 v13, 0x0

    .line 428177
    const/4 v12, 0x0

    .line 428178
    const/4 v11, 0x0

    .line 428179
    const/4 v10, 0x0

    .line 428180
    const/4 v9, 0x0

    .line 428181
    const/4 v8, 0x0

    .line 428182
    const/4 v7, 0x0

    .line 428183
    const/4 v6, 0x0

    .line 428184
    const/4 v5, 0x0

    .line 428185
    const/4 v4, 0x0

    .line 428186
    const/4 v3, 0x0

    .line 428187
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    .line 428188
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 428189
    const/4 v3, 0x0

    .line 428190
    :goto_0
    return v3

    .line 428191
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 428192
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_15

    .line 428193
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v30

    .line 428194
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 428195
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    if-eqz v30, :cond_1

    .line 428196
    const-string v31, "all_phones"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_2

    .line 428197
    invoke-static/range {p0 .. p1}, LX/3R0;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 428198
    :cond_2
    const-string v31, "email_addresses"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 428199
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v28

    goto :goto_1

    .line 428200
    :cond_3
    const-string v31, "gender"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 428201
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/facebook/graphql/enums/GraphQLGender;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    goto :goto_1

    .line 428202
    :cond_4
    const-string v31, "id"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 428203
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto :goto_1

    .line 428204
    :cond_5
    const-string v31, "is_deactivated_allowed_on_messenger"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 428205
    const/4 v9, 0x1

    .line 428206
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 428207
    :cond_6
    const-string v31, "is_messenger_only_deactivated"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 428208
    const/4 v8, 0x1

    .line 428209
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 428210
    :cond_7
    const-string v31, "is_minor"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 428211
    const/4 v7, 0x1

    .line 428212
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 428213
    :cond_8
    const-string v31, "is_mobile_pushable"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 428214
    const/4 v6, 0x1

    .line 428215
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 428216
    :cond_9
    const-string v31, "is_partial"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 428217
    const/4 v5, 0x1

    .line 428218
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 428219
    :cond_a
    const-string v31, "is_work_user"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 428220
    const/4 v4, 0x1

    .line 428221
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 428222
    :cond_b
    const-string v31, "messaging_geo"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    .line 428223
    invoke-static/range {p0 .. p1}, LX/32W;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 428224
    :cond_c
    const-string v31, "messenger_only_deactivated_matched_user"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 428225
    invoke-static/range {p0 .. p1}, LX/42J;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 428226
    :cond_d
    const-string v31, "montage_thread_fbid"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 428227
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 428228
    :cond_e
    const-string v31, "name"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 428229
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 428230
    :cond_f
    const-string v31, "profile_picture_is_silhouette"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 428231
    const/4 v3, 0x1

    .line 428232
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 428233
    :cond_10
    const-string v31, "squareProfilePicBig"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 428234
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 428235
    :cond_11
    const-string v31, "squareProfilePicHuge"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 428236
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 428237
    :cond_12
    const-string v31, "squareProfilePicSmall"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_13

    .line 428238
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 428239
    :cond_13
    const-string v31, "structured_name"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_14

    .line 428240
    invoke-static/range {p0 .. p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 428241
    :cond_14
    const-string v31, "username"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 428242
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 428243
    :cond_15
    const/16 v30, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 428244
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 428245
    const/16 v29, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 428246
    const/16 v28, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 428247
    const/16 v27, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 428248
    if-eqz v9, :cond_16

    .line 428249
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 428250
    :cond_16
    if-eqz v8, :cond_17

    .line 428251
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 428252
    :cond_17
    if-eqz v7, :cond_18

    .line 428253
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 428254
    :cond_18
    if-eqz v6, :cond_19

    .line 428255
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 428256
    :cond_19
    if-eqz v5, :cond_1a

    .line 428257
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 428258
    :cond_1a
    if-eqz v4, :cond_1b

    .line 428259
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 428260
    :cond_1b
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 428261
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 428262
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 428263
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 428264
    if-eqz v3, :cond_1c

    .line 428265
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->a(IZ)V

    .line 428266
    :cond_1c
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 428267
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 428268
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 428269
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 428270
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 428271
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 428272
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 428273
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428274
    if-eqz v0, :cond_1

    .line 428275
    const-string v1, "all_phones"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428276
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 428277
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 428278
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/38I;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 428279
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 428280
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 428281
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 428282
    if-eqz v0, :cond_2

    .line 428283
    const-string v0, "email_addresses"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428284
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 428285
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 428286
    if-eqz v0, :cond_3

    .line 428287
    const-string v0, "gender"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428288
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428289
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 428290
    if-eqz v0, :cond_4

    .line 428291
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428292
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428293
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428294
    if-eqz v0, :cond_5

    .line 428295
    const-string v1, "is_deactivated_allowed_on_messenger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428296
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428297
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428298
    if-eqz v0, :cond_6

    .line 428299
    const-string v1, "is_messenger_only_deactivated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428300
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428301
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428302
    if-eqz v0, :cond_7

    .line 428303
    const-string v1, "is_minor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428304
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428305
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428306
    if-eqz v0, :cond_8

    .line 428307
    const-string v1, "is_mobile_pushable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428308
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428309
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428310
    if-eqz v0, :cond_9

    .line 428311
    const-string v1, "is_partial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428312
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428313
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428314
    if-eqz v0, :cond_a

    .line 428315
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428316
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428317
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428318
    if-eqz v0, :cond_d

    .line 428319
    const-string v1, "messaging_geo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428320
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 428321
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 428322
    if-eqz v1, :cond_b

    .line 428323
    const-string v2, "current_location_prediction"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428324
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428325
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 428326
    if-eqz v1, :cond_c

    .line 428327
    const-string v2, "home_location_prediction"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428328
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428329
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428330
    :cond_d
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428331
    if-eqz v0, :cond_e

    .line 428332
    const-string v1, "messenger_only_deactivated_matched_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428333
    invoke-static {p0, v0, p2, p3}, LX/42J;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 428334
    :cond_e
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 428335
    if-eqz v0, :cond_f

    .line 428336
    const-string v1, "montage_thread_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428337
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428338
    :cond_f
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 428339
    if-eqz v0, :cond_10

    .line 428340
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428341
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428342
    :cond_10
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428343
    if-eqz v0, :cond_11

    .line 428344
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428345
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428346
    :cond_11
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428347
    if-eqz v0, :cond_12

    .line 428348
    const-string v1, "squareProfilePicBig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428349
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 428350
    :cond_12
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428351
    if-eqz v0, :cond_13

    .line 428352
    const-string v1, "squareProfilePicHuge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428353
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 428354
    :cond_13
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428355
    if-eqz v0, :cond_14

    .line 428356
    const-string v1, "squareProfilePicSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428357
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 428358
    :cond_14
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428359
    if-eqz v0, :cond_15

    .line 428360
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428361
    invoke-static {p0, v0, p2, p3}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 428362
    :cond_15
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 428363
    if-eqz v0, :cond_16

    .line 428364
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428365
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428366
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428367
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 428132
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 428133
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 428134
    :goto_0
    return v1

    .line 428135
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_6

    .line 428136
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 428137
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 428138
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 428139
    const-string v11, "is_currently_selected"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 428140
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 428141
    :cond_1
    const-string v11, "is_most_recent"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 428142
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 428143
    :cond_2
    const-string v11, "is_primary"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 428144
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 428145
    :cond_3
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 428146
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 428147
    :cond_4
    const-string v11, "option_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 428148
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 428149
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 428150
    :cond_6
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 428151
    if-eqz v4, :cond_7

    .line 428152
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 428153
    :cond_7
    if-eqz v3, :cond_8

    .line 428154
    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 428155
    :cond_8
    if-eqz v0, :cond_9

    .line 428156
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 428157
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 428158
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 428159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 428109
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 428110
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428111
    if-eqz v0, :cond_0

    .line 428112
    const-string v1, "is_currently_selected"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428113
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428114
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428115
    if-eqz v0, :cond_1

    .line 428116
    const-string v1, "is_most_recent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428117
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428118
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 428119
    if-eqz v0, :cond_2

    .line 428120
    const-string v1, "is_primary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428121
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 428122
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 428123
    if-eqz v0, :cond_3

    .line 428124
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428125
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 428126
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 428127
    if-eqz v0, :cond_4

    .line 428128
    const-string v0, "option_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428129
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428130
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428131
    return-void
.end method
