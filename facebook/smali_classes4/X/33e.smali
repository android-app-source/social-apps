.class public interface abstract LX/33e;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/ZeroOptinParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptinParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/zero/sdk/request/ZeroOptoutParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;"
        }
    .end annotation
.end method
