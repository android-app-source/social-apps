.class public final LX/2kQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kR;


# instance fields
.field public final synthetic a:LX/2jx;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2nf;",
            "Landroid/util/SparseArray",
            "<TTEdge;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2jx;)V
    .locals 1

    .prologue
    .line 456020
    iput-object p1, p0, LX/2kQ;->a:LX/2jx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456021
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/2kQ;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(LX/2nf;)V
    .locals 8
    .param p1    # LX/2nf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const v4, 0x920009

    .line 456022
    iget-object v0, p0, LX/2kQ;->a:LX/2jx;

    iget-object v0, v0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 456023
    iget-object v0, p0, LX/2kQ;->a:LX/2jx;

    iget-object v0, v0, LX/2jx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 456024
    iget-object v1, p0, LX/2kQ;->a:LX/2jx;

    iget-object v1, v1, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 456025
    iget-object v1, p0, LX/2kQ;->a:LX/2jx;

    iget-object v1, v1, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "DiskConnectionStore"

    invoke-interface {v1, v4, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 456026
    if-nez p1, :cond_0

    .line 456027
    iget-object v1, p0, LX/2kQ;->a:LX/2jx;

    iget-object v1, v1, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x4

    invoke-interface {v1, v4, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 456028
    :goto_0
    return-void

    .line 456029
    :cond_0
    iget-object v1, p0, LX/2kQ;->b:Ljava/util/Map;

    iget-object v2, p0, LX/2kQ;->a:LX/2jx;

    invoke-interface {p1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 456030
    iget-object v5, v2, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->b()V

    .line 456031
    iget v5, v2, LX/2jx;->h:I

    if-lez v5, :cond_2

    .line 456032
    const-string v5, "CHANGED_ROW_IDS"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v5

    .line 456033
    const-string v6, "Update"

    const/4 v7, 0x1

    invoke-static {v6, v5, p1, v7}, LX/2jx;->a(Ljava/lang/String;[JLX/2nf;Z)Landroid/util/SparseArray;

    move-result-object v5

    .line 456034
    if-nez v5, :cond_1

    .line 456035
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 456036
    :cond_1
    :goto_1
    move-object v2, v5

    .line 456037
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456038
    iget-object v1, p0, LX/2kQ;->a:LX/2jx;

    iget-object v1, v1, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0xbd

    invoke-interface {v1, v4, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 456039
    iget-object v1, p0, LX/2kQ;->a:LX/2jx;

    iget-object v1, v1, LX/2jx;->u:LX/2kK;

    new-instance v2, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;-><init>(LX/2kQ;ILX/2nf;)V

    invoke-virtual {v1, p1, v2}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
