.class public LX/3Ku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private b:J

.field private final c:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 549396
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3Ku;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549398
    iput-object p1, p0, LX/3Ku;->c:LX/0SG;

    .line 549399
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Ku;->b:J

    .line 549400
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ku;
    .locals 7

    .prologue
    .line 549401
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 549402
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 549403
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 549404
    if-nez v1, :cond_0

    .line 549405
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549406
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 549407
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 549408
    sget-object v1, LX/3Ku;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 549409
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 549410
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 549411
    :cond_1
    if-nez v1, :cond_4

    .line 549412
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 549413
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 549414
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 549415
    new-instance p0, LX/3Ku;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {p0, v1}, LX/3Ku;-><init>(LX/0SG;)V

    .line 549416
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 549417
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 549418
    if-nez v1, :cond_2

    .line 549419
    sget-object v0, LX/3Ku;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ku;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 549420
    :goto_1
    if-eqz v0, :cond_3

    .line 549421
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549422
    :goto_3
    check-cast v0, LX/3Ku;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 549423
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 549424
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 549425
    :catchall_1
    move-exception v0

    .line 549426
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 549427
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 549428
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 549429
    :cond_2
    :try_start_8
    sget-object v0, LX/3Ku;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ku;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private declared-synchronized d()V
    .locals 4

    .prologue
    .line 549430
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Ku;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/3Ku;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 549431
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Ku;->a:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549432
    :cond_0
    monitor-exit p0

    return-void

    .line 549433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 549434
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Ku;->a:LX/0Px;

    .line 549435
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3Ku;->b:J

    .line 549436
    return-void
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 549437
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3Ku;->a:LX/0Px;

    .line 549438
    iget-object v0, p0, LX/3Ku;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Ku;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549439
    monitor-exit p0

    return-void

    .line 549440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 549441
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Ku;->d()V

    .line 549442
    iget-object v0, p0, LX/3Ku;->a:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 549443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()J
    .locals 2

    .prologue
    .line 549444
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/3Ku;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 549445
    invoke-virtual {p0}, LX/3Ku;->a()V

    .line 549446
    return-void
.end method
