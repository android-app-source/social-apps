.class public LX/2OW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private final b:LX/2OX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 402329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402330
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, LX/2OW;->a:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 402331
    new-instance v0, LX/2OX;

    invoke-direct {v0, p0}, LX/2OX;-><init>(LX/2OW;)V

    iput-object v0, p0, LX/2OW;->b:LX/2OX;

    .line 402332
    return-void
.end method


# virtual methods
.method public final a()LX/2OX;
    .locals 1

    .prologue
    .line 402333
    iget-object v0, p0, LX/2OW;->a:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 402334
    iget-object v0, p0, LX/2OW;->b:LX/2OX;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 402335
    iget-object v0, p0, LX/2OW;->a:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->isHeldByCurrentThread()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 402336
    return-void
.end method
