.class public LX/3Je;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 547785
    if-eqz p1, :cond_0

    .line 547786
    return-object p0

    .line 547787
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Illegal argument for %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a([[[FI)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 547788
    if-gt p1, v3, :cond_2

    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    :cond_0
    move v1, v3

    .line 547789
    :cond_1
    :goto_0
    return v1

    .line 547790
    :cond_2
    add-int/lit8 v0, p1, -0x1

    array-length v2, p0

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 547791
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_4

    .line 547792
    aget-object v2, p0, v0

    array-length v2, v2

    if-ne v2, v5, :cond_1

    move v2, v1

    .line 547793
    :goto_2
    aget-object v4, p0, v0

    array-length v4, v4

    if-ge v2, v4, :cond_3

    .line 547794
    aget-object v4, p0, v0

    aget-object v4, v4, v2

    array-length v4, v4

    if-ne v4, v5, :cond_1

    .line 547795
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 547796
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v1, v3

    .line 547797
    goto :goto_0
.end method
