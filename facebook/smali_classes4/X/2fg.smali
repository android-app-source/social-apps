.class public final LX/2fg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2fd;

.field public final synthetic b:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic c:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/2fd;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 0

    .prologue
    .line 447049
    iput-object p1, p0, LX/2fg;->c:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iput-object p2, p0, LX/2fg;->a:LX/2fd;

    iput-object p3, p0, LX/2fg;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4c0fde13

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 447050
    iget-object v1, p0, LX/2fg;->c:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->c:LX/0gh;

    const-string v2, "tap_saved_feed_reminder_unit"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 447051
    iget-object v1, p0, LX/2fg;->c:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iget-object v2, p0, LX/2fg;->a:LX/2fd;

    .line 447052
    iget-object v4, v2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    .line 447053
    invoke-static {v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 447054
    new-instance v5, LX/2oI;

    invoke-direct {v5}, LX/2oI;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    .line 447055
    iput-object v6, v5, LX/2oI;->N:Ljava/lang/String;

    .line 447056
    move-object v5, v5

    .line 447057
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->G()Ljava/lang/String;

    move-result-object v4

    .line 447058
    iput-object v4, v5, LX/2oI;->aT:Ljava/lang/String;

    .line 447059
    move-object v4, v5

    .line 447060
    iget-object v5, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->k:LX/D3w;

    invoke-virtual {v4}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, LX/04D;->SAVED_REMINDER:LX/04D;

    invoke-virtual {v5, v4, v6, v7}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    .line 447061
    :goto_0
    iget-object v1, p0, LX/2fg;->c:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b:LX/0Zb;

    iget-object v2, p0, LX/2fg;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 447062
    const v1, -0x7d391565

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 447063
    :cond_0
    if-eqz v4, :cond_4

    iget-object v5, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->f:LX/1nG;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 447064
    if-eqz v5, :cond_1

    .line 447065
    iget-object v5, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->d:LX/1nA;

    invoke-static {v4}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v4, v6}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_0

    .line 447066
    :cond_1
    invoke-static {v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 447067
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v4

    .line 447068
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 447069
    sget-object v5, LX/0ax;->hn:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 447070
    :goto_2
    iget-object v5, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->e:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 447071
    :cond_2
    sget-object v5, LX/0ax;->hm:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 447072
    :cond_3
    iget-object v5, v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->e:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method
