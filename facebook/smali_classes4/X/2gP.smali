.class public final enum LX/2gP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2gP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2gP;

.field public static final enum ATTEMPT:LX/2gP;

.field public static final enum AUTH_NO_USER:LX/2gP;

.field public static final enum FAILED:LX/2gP;

.field public static final enum INVALID_TOKEN:LX/2gP;

.field public static final enum MISSING_COMPONENT:LX/2gP;

.field public static final enum NO_TOKEN:LX/2gP;

.field public static final enum PERMISSION_DENIED:LX/2gP;

.field public static final enum SERVER_FAILED:LX/2gP;

.field public static final enum SKIP_CURRENT_REGISTRATION:LX/2gP;

.field public static final enum SUCCESS:LX/2gP;

.field public static final enum WRONG_USER:LX/2gP;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 448308
    new-instance v0, LX/2gP;

    const-string v1, "ATTEMPT"

    invoke-direct {v0, v1, v3}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->ATTEMPT:LX/2gP;

    .line 448309
    new-instance v0, LX/2gP;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->SUCCESS:LX/2gP;

    .line 448310
    new-instance v0, LX/2gP;

    const-string v1, "AUTH_NO_USER"

    invoke-direct {v0, v1, v5}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->AUTH_NO_USER:LX/2gP;

    .line 448311
    new-instance v0, LX/2gP;

    const-string v1, "WRONG_USER"

    invoke-direct {v0, v1, v6}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->WRONG_USER:LX/2gP;

    .line 448312
    new-instance v0, LX/2gP;

    const-string v1, "NO_TOKEN"

    invoke-direct {v0, v1, v7}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->NO_TOKEN:LX/2gP;

    .line 448313
    new-instance v0, LX/2gP;

    const-string v1, "INVALID_TOKEN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->INVALID_TOKEN:LX/2gP;

    .line 448314
    new-instance v0, LX/2gP;

    const-string v1, "FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->FAILED:LX/2gP;

    .line 448315
    new-instance v0, LX/2gP;

    const-string v1, "PERMISSION_DENIED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->PERMISSION_DENIED:LX/2gP;

    .line 448316
    new-instance v0, LX/2gP;

    const-string v1, "SERVER_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->SERVER_FAILED:LX/2gP;

    .line 448317
    new-instance v0, LX/2gP;

    const-string v1, "SKIP_CURRENT_REGISTRATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->SKIP_CURRENT_REGISTRATION:LX/2gP;

    .line 448318
    new-instance v0, LX/2gP;

    const-string v1, "MISSING_COMPONENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/2gP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2gP;->MISSING_COMPONENT:LX/2gP;

    .line 448319
    const/16 v0, 0xb

    new-array v0, v0, [LX/2gP;

    sget-object v1, LX/2gP;->ATTEMPT:LX/2gP;

    aput-object v1, v0, v3

    sget-object v1, LX/2gP;->SUCCESS:LX/2gP;

    aput-object v1, v0, v4

    sget-object v1, LX/2gP;->AUTH_NO_USER:LX/2gP;

    aput-object v1, v0, v5

    sget-object v1, LX/2gP;->WRONG_USER:LX/2gP;

    aput-object v1, v0, v6

    sget-object v1, LX/2gP;->NO_TOKEN:LX/2gP;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2gP;->INVALID_TOKEN:LX/2gP;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2gP;->FAILED:LX/2gP;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2gP;->PERMISSION_DENIED:LX/2gP;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2gP;->SERVER_FAILED:LX/2gP;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2gP;->SKIP_CURRENT_REGISTRATION:LX/2gP;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2gP;->MISSING_COMPONENT:LX/2gP;

    aput-object v2, v0, v1

    sput-object v0, LX/2gP;->$VALUES:[LX/2gP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 448320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2gP;
    .locals 1

    .prologue
    .line 448307
    const-class v0, LX/2gP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2gP;

    return-object v0
.end method

.method public static values()[LX/2gP;
    .locals 1

    .prologue
    .line 448306
    sget-object v0, LX/2gP;->$VALUES:[LX/2gP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2gP;

    return-object v0
.end method
