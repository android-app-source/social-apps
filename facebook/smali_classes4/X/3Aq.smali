.class public LX/3Aq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 526330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 526331
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 526332
    if-eqz v0, :cond_0

    .line 526333
    const-string v1, "token_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526334
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 526335
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 526336
    if-eqz v0, :cond_1

    .line 526337
    const-string v0, "token_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526338
    const-class v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526339
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 526340
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 526341
    const/4 v0, 0x0

    .line 526342
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 526343
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 526344
    :goto_0
    return v1

    .line 526345
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_3

    .line 526346
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 526347
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 526348
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 526349
    const-string v7, "token_position"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 526350
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 526351
    :cond_1
    const-string v7, "token_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 526352
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 526353
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 526354
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 526355
    if-eqz v3, :cond_4

    .line 526356
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 526357
    :cond_4
    if-eqz v0, :cond_5

    .line 526358
    invoke-virtual {p1, v2, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 526359
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v0, v1

    goto :goto_1
.end method
