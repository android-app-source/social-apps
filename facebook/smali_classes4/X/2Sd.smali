.class public LX/2Sd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/2Sd;


# instance fields
.field private final b:LX/0Yw;

.field private final c:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412978
    const-class v0, LX/2Sd;

    sput-object v0, LX/2Sd;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412980
    iput-object p1, p0, LX/2Sd;->c:LX/0W3;

    .line 412981
    new-instance v0, LX/0Yw;

    const/4 v1, 0x0

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/2Sd;->b:LX/0Yw;

    .line 412982
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sd;
    .locals 4

    .prologue
    .line 412965
    sget-object v0, LX/2Sd;->d:LX/2Sd;

    if-nez v0, :cond_1

    .line 412966
    const-class v1, LX/2Sd;

    monitor-enter v1

    .line 412967
    :try_start_0
    sget-object v0, LX/2Sd;->d:LX/2Sd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412968
    if-eqz v2, :cond_0

    .line 412969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412970
    new-instance p0, LX/2Sd;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/2Sd;-><init>(LX/0W3;)V

    .line 412971
    move-object v0, p0

    .line 412972
    sput-object v0, LX/2Sd;->d:LX/2Sd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412973
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412974
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412975
    :cond_1
    sget-object v0, LX/2Sd;->d:LX/2Sd;

    return-object v0

    .line 412976
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 412959
    new-instance v0, Ljava/io/File;

    const-string v1, "search_events_json.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 412960
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 412961
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 412962
    :try_start_0
    iget-object v1, p0, LX/2Sd;->b:LX/0Yw;

    invoke-virtual {v1}, LX/0Yw;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 412963
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 412964
    invoke-static {v2, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/7CQ;)V
    .locals 1

    .prologue
    .line 412983
    const-string v0, ""

    invoke-virtual {p0, p1, p2, v0}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 412984
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 412954
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "timestamp"

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {p2}, LX/7CQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "target"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 412955
    iget-object v1, p0, LX/2Sd;->b:LX/0Yw;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Yw;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412956
    :goto_0
    return-void

    .line 412957
    :catch_0
    move-exception v0

    .line 412958
    sget-object v1, LX/2Sd;->a:Ljava/lang/Class;

    const-string v2, "Exception creating search event json log"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412946
    :try_start_0
    invoke-direct {p0, p1}, LX/2Sd;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 412947
    const-string v1, "search_events_json.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 412948
    :goto_0
    return-object v0

    .line 412949
    :catch_0
    move-exception v0

    .line 412950
    throw v0

    .line 412951
    :catch_1
    move-exception v0

    .line 412952
    sget-object v1, LX/2Sd;->a:Ljava/lang/Class;

    const-string v2, "Exception saving search events log"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 412953
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412940
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 412941
    :try_start_0
    invoke-direct {p0, p1}, LX/2Sd;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 412942
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "search_events_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412943
    return-object v0

    .line 412944
    :catch_0
    move-exception v0

    .line 412945
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to write search events log file"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 412938
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 412939
    iget-object v0, p0, LX/2Sd;->c:LX/0W3;

    sget-wide v2, LX/0X5;->bi:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
