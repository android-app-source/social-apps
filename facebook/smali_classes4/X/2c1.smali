.class public final LX/2c1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2c0;


# instance fields
.field public final synthetic a:LX/339;


# direct methods
.method public constructor <init>(LX/339;)V
    .locals 0

    .prologue
    .line 439998
    iput-object p1, p0, LX/2c1;->a:LX/339;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 439994
    iget-object v0, p0, LX/2c1;->a:LX/339;

    iget-object v0, v0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2bs;->j:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 439995
    if-eqz v0, :cond_0

    const-wide/16 v3, 0x0

    .line 439996
    iget-object v1, p0, LX/2c1;->a:LX/339;

    iget-object v1, v1, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2bs;->k:LX/0Tn;

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 439997
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 439999
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 439993
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 439991
    iget-object v0, p0, LX/2c1;->a:LX/339;

    iget-object v0, v0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2bs;->k:LX/0Tn;

    iget-object v0, p0, LX/2c1;->a:LX/339;

    iget-object v0, v0, LX/339;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 439992
    return-void
.end method
