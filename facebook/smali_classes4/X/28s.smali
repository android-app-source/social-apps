.class public LX/28s;
.super LX/16B;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/28t;

.field private final d:LX/0Xl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 374857
    const-class v0, LX/28s;

    sput-object v0, LX/28s;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/28t;LX/0Xl;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374858
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374859
    iput-object p1, p0, LX/28s;->b:Landroid/content/Context;

    .line 374860
    iput-object p2, p0, LX/28s;->c:LX/28t;

    .line 374861
    iput-object p3, p0, LX/28s;->d:LX/0Xl;

    .line 374862
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 3
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 374863
    if-eqz p1, :cond_2

    .line 374864
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->d()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->NO:LX/03R;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 374865
    :goto_0
    if-eqz v0, :cond_0

    .line 374866
    iget-object v0, p0, LX/28s;->c:LX/28t;

    iget-object v1, p0, LX/28s;->b:Landroid/content/Context;

    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v2

    .line 374867
    iget-object p0, v2, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    move-object v2, p0

    .line 374868
    invoke-virtual {v0, v1, v2}, LX/28t;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    .line 374869
    :cond_0
    :goto_1
    return-void

    .line 374870
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 374871
    :cond_2
    sget-object v0, LX/28s;->a:Ljava/lang/Class;

    const-string v1, "AuthenticationResult is unexpectedly null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 374872
    iget-object v0, p0, LX/28s;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/28t;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 374873
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 374874
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/2b2;->D:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 374875
    iget-object v1, p0, LX/28s;->d:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 374876
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 374877
    iget-object v0, p0, LX/28s;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/28t;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 374878
    return-void
.end method
