.class public LX/2Rw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2Rw;


# instance fields
.field private final a:LX/0Xl;

.field public final b:LX/0kb;

.field private final c:Landroid/net/ConnectivityManager;

.field public final d:LX/0SG;

.field private e:LX/0Yb;

.field public f:LX/2Ry;

.field public g:J


# direct methods
.method public constructor <init>(LX/0Xl;LX/0kb;Landroid/net/ConnectivityManager;LX/0SG;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410356
    sget-object v0, LX/2Ry;->UNKNOWN:LX/2Ry;

    iput-object v0, p0, LX/2Rw;->f:LX/2Ry;

    .line 410357
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2Rw;->g:J

    .line 410358
    iput-object p1, p0, LX/2Rw;->a:LX/0Xl;

    .line 410359
    iput-object p2, p0, LX/2Rw;->b:LX/0kb;

    .line 410360
    iput-object p3, p0, LX/2Rw;->c:Landroid/net/ConnectivityManager;

    .line 410361
    iput-object p4, p0, LX/2Rw;->d:LX/0SG;

    .line 410362
    return-void
.end method

.method public static a(LX/0QB;)LX/2Rw;
    .locals 7

    .prologue
    .line 410363
    sget-object v0, LX/2Rw;->h:LX/2Rw;

    if-nez v0, :cond_1

    .line 410364
    const-class v1, LX/2Rw;

    monitor-enter v1

    .line 410365
    :try_start_0
    sget-object v0, LX/2Rw;->h:LX/2Rw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410366
    if-eqz v2, :cond_0

    .line 410367
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410368
    new-instance p0, LX/2Rw;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Rw;-><init>(LX/0Xl;LX/0kb;Landroid/net/ConnectivityManager;LX/0SG;)V

    .line 410369
    move-object v0, p0

    .line 410370
    sput-object v0, LX/2Rw;->h:LX/2Rw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410371
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410372
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410373
    :cond_1
    sget-object v0, LX/2Rw;->h:LX/2Rw;

    return-object v0

    .line 410374
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410375
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/2Rw;)LX/2Ry;
    .locals 1

    .prologue
    .line 410376
    :try_start_0
    iget-object v0, p0, LX/2Rw;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/2Ry;->METERED:LX/2Ry;

    .line 410377
    :goto_0
    return-object v0

    .line 410378
    :cond_0
    sget-object v0, LX/2Ry;->UNMETERED:LX/2Ry;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 410379
    :catch_0
    sget-object v0, LX/2Ry;->UNKNOWN:LX/2Ry;

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 410380
    iget-object v0, p0, LX/2Rw;->a:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/2Rz;

    invoke-direct {v2, p0}, LX/2Rz;-><init>(LX/2Rw;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Rw;->e:LX/0Yb;

    .line 410381
    iget-object v0, p0, LX/2Rw;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410382
    invoke-static {p0}, LX/2Rw;->b$redex0(LX/2Rw;)LX/2Ry;

    move-result-object v0

    iput-object v0, p0, LX/2Rw;->f:LX/2Ry;

    .line 410383
    :cond_0
    iget-object v0, p0, LX/2Rw;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 410384
    return-void
.end method
