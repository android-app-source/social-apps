.class public LX/36A;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements LX/36B;


# static fields
.field private static final a:Landroid/graphics/Paint;

.field private static final b:Landroid/graphics/RectF;


# instance fields
.field private c:LX/36C;

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 498215
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LX/36A;->a:Landroid/graphics/Paint;

    .line 498216
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/36A;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 498217
    new-instance v0, LX/36C;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/36C;-><init>(LX/36C;)V

    invoke-direct {p0, v0}, LX/36A;-><init>(LX/36C;)V

    .line 498218
    return-void
.end method

.method public constructor <init>(LX/36C;)V
    .locals 1

    .prologue
    .line 498219
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 498220
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/36A;->d:Z

    .line 498221
    iput-object p1, p0, LX/36A;->c:LX/36C;

    .line 498222
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/36C;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p1, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    if-eqz v0, :cond_0

    .line 498223
    iget-object v0, p1, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    invoke-virtual {v0, p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->a(LX/36B;)V

    .line 498224
    :cond_0
    return-void
.end method

.method private a()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 498228
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->b:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    sget-object v0, LX/36A;->a:Landroid/graphics/Paint;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->b:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private b()Landroid/graphics/Paint;
    .locals 3

    .prologue
    .line 498225
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->b:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 498226
    iget-object v0, p0, LX/36A;->c:LX/36C;

    new-instance v1, Landroid/graphics/Paint;

    sget-object v2, LX/36A;->a:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, LX/36C;->b:Landroid/graphics/Paint;

    .line 498227
    :cond_0
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->b:Landroid/graphics/Paint;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 498234
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iput-object p1, v0, LX/36C;->a:Landroid/graphics/Bitmap;

    .line 498235
    invoke-virtual {p0}, LX/36A;->invalidateSelf()V

    .line 498236
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 498229
    invoke-virtual {p0}, LX/36A;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 498230
    sget-object v1, LX/36A;->b:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, LX/36A;->c:LX/36C;

    iget v3, v3, LX/36C;->c:I

    int-to-float v3, v3

    iget-object v4, p0, LX/36A;->c:LX/36C;

    iget v4, v4, LX/36C;->i:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget-object v4, p0, LX/36A;->c:LX/36C;

    iget v4, v4, LX/36C;->d:I

    int-to-float v4, v4

    iget-object v5, p0, LX/36A;->c:LX/36C;

    iget v5, v5, LX/36C;->i:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget-object v5, p0, LX/36A;->c:LX/36C;

    iget v5, v5, LX/36C;->e:I

    int-to-float v5, v5

    iget-object v6, p0, LX/36A;->c:LX/36C;

    iget v6, v6, LX/36C;->i:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget-object v5, p0, LX/36A;->c:LX/36C;

    iget v5, v5, LX/36C;->f:I

    int-to-float v5, v5

    iget-object v6, p0, LX/36A;->c:LX/36C;

    iget v6, v6, LX/36C;->i:F

    mul-float/2addr v5, v6

    sub-float/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 498231
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 498232
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget-object v0, v0, LX/36C;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget-object v1, v1, LX/36C;->k:Landroid/graphics/Rect;

    sget-object v2, LX/36A;->b:Landroid/graphics/RectF;

    invoke-direct {p0}, LX/36A;->a()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 498233
    :cond_0
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 498213
    invoke-direct {p0}, LX/36A;->a()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    .prologue
    .line 498214
    invoke-direct {p0}, LX/36A;->a()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 498212
    iget-object v0, p0, LX/36A;->c:LX/36C;

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 498209
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget v0, v0, LX/36C;->d:I

    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget v1, v1, LX/36C;->f:I

    add-int/2addr v0, v1

    .line 498210
    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget-object v1, v1, LX/36C;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v0, v1

    .line 498211
    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget v1, v1, LX/36C;->i:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 498206
    iget-object v0, p0, LX/36A;->c:LX/36C;

    iget v0, v0, LX/36C;->c:I

    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget v1, v1, LX/36C;->e:I

    add-int/2addr v0, v1

    .line 498207
    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget-object v1, v1, LX/36C;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    .line 498208
    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget v1, v1, LX/36C;->i:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getOpacity()I
    .locals 3

    .prologue
    const/4 v0, -0x3

    .line 498203
    iget-object v1, p0, LX/36A;->c:LX/36C;

    iget-object v1, v1, LX/36C;->a:Landroid/graphics/Bitmap;

    .line 498204
    if-nez v1, :cond_1

    .line 498205
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/36A;->getAlpha()I

    move-result v1

    const/16 v2, 0xff

    if-lt v1, v2, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 17

    .prologue
    .line 498168
    sget-object v2, LX/03r;->TextureRegionDrawable:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 498169
    const/16 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 498170
    const/16 v3, 0x3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    .line 498171
    const/16 v3, 0x4

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 498172
    const/16 v3, 0x5

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    .line 498173
    const/16 v3, 0x6

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    .line 498174
    const/16 v3, 0x7

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v9

    .line 498175
    const/16 v3, 0x8

    const/4 v10, 0x0

    invoke-virtual {v2, v3, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    .line 498176
    const/16 v3, 0x9

    const/4 v11, 0x0

    invoke-virtual {v2, v3, v11}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v11

    .line 498177
    const/16 v3, 0xa

    const/4 v12, 0x0

    invoke-virtual {v2, v3, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    .line 498178
    const/16 v3, 0xb

    const/4 v13, 0x0

    invoke-virtual {v2, v3, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v13

    .line 498179
    const/16 v3, 0x0

    const/4 v14, 0x0

    invoke-virtual {v2, v3, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 498180
    invoke-virtual/range {p1 .. p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    iget v14, v14, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 498181
    move-object/from16 v0, p0

    iget-object v15, v0, LX/36A;->c:LX/36C;

    int-to-float v14, v14

    int-to-float v0, v3

    move/from16 v16, v0

    div-float v14, v14, v16

    iput v14, v15, LX/36C;->i:F

    .line 498182
    const/16 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v2, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 498183
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v3}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 498184
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 498185
    instance-of v2, v3, Landroid/graphics/drawable/BitmapDrawable;

    .line 498186
    if-nez v2, :cond_0

    instance-of v14, v3, Lcom/facebook/fbui/drawable/NetworkDrawable;

    if-nez v14, :cond_0

    .line 498187
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v3, "app-region.atlas should resolve to a BitmapDrawable or NetworkDrawable"

    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 498188
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, LX/36A;->c:LX/36C;

    iput v8, v14, LX/36C;->c:I

    .line 498189
    move-object/from16 v0, p0

    iget-object v8, v0, LX/36A;->c:LX/36C;

    iput v9, v8, LX/36C;->d:I

    .line 498190
    move-object/from16 v0, p0

    iget-object v8, v0, LX/36A;->c:LX/36C;

    iput v10, v8, LX/36C;->e:I

    .line 498191
    move-object/from16 v0, p0

    iget-object v8, v0, LX/36A;->c:LX/36C;

    iput v11, v8, LX/36C;->f:I

    .line 498192
    move-object/from16 v0, p0

    iget-object v8, v0, LX/36A;->c:LX/36C;

    iget-object v8, v8, LX/36C;->k:Landroid/graphics/Rect;

    invoke-virtual {v8, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 498193
    move-object/from16 v0, p0

    iget-object v4, v0, LX/36A;->c:LX/36C;

    iput v12, v4, LX/36C;->h:I

    .line 498194
    move-object/from16 v0, p0

    iget-object v4, v0, LX/36A;->c:LX/36C;

    iput v13, v4, LX/36C;->g:I

    .line 498195
    if-eqz v2, :cond_3

    .line 498196
    move-object/from16 v0, p0

    iget-object v4, v0, LX/36A;->c:LX/36C;

    move-object v2, v3

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v4, LX/36C;->a:Landroid/graphics/Bitmap;

    .line 498197
    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    iput-object v3, v2, LX/36C;->l:Landroid/graphics/drawable/Drawable$ConstantState;

    .line 498198
    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    iget-object v2, v2, LX/36C;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/36A;->c:LX/36C;

    iget v3, v3, LX/36C;->h:I

    if-ne v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    iget-object v2, v2, LX/36C;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/36A;->c:LX/36C;

    iget v3, v3, LX/36C;->g:I

    if-eq v2, v3, :cond_2

    .line 498199
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/36A;->c:LX/36C;

    iget-object v3, v3, LX/36C;->a:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/36A;->c:LX/36C;

    iget v4, v4, LX/36C;->h:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/36A;->c:LX/36C;

    iget v5, v5, LX/36C;->g:I

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, LX/36C;->a:Landroid/graphics/Bitmap;

    .line 498200
    :cond_2
    :goto_0
    return-void

    .line 498201
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    check-cast v3, Lcom/facebook/fbui/drawable/NetworkDrawable;

    iput-object v3, v2, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    .line 498202
    move-object/from16 v0, p0

    iget-object v2, v0, LX/36A;->c:LX/36C;

    iget-object v2, v2, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/drawable/NetworkDrawable;->a(LX/36B;)V

    goto :goto_0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 498164
    iget-boolean v0, p0, LX/36A;->d:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 498165
    new-instance v0, LX/36C;

    iget-object v1, p0, LX/36A;->c:LX/36C;

    invoke-direct {v0, v1}, LX/36C;-><init>(LX/36C;)V

    iput-object v0, p0, LX/36A;->c:LX/36C;

    .line 498166
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/36A;->d:Z

    .line 498167
    :cond_0
    return-object p0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 498161
    invoke-direct {p0}, LX/36A;->b()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 498162
    invoke-virtual {p0}, LX/36A;->invalidateSelf()V

    .line 498163
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 498158
    invoke-direct {p0}, LX/36A;->b()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 498159
    invoke-virtual {p0}, LX/36A;->invalidateSelf()V

    .line 498160
    return-void
.end method
