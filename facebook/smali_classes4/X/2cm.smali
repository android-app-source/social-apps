.class public LX/2cm;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9hh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8xt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 441838
    const-class v0, LX/2cm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2cm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9hh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8xt;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 441839
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 441840
    iput-object p1, p0, LX/2cm;->b:LX/0Ot;

    .line 441841
    iput-object p2, p0, LX/2cm;->c:LX/0Ot;

    .line 441842
    iput-object p3, p0, LX/2cm;->d:LX/03V;

    .line 441843
    return-void
.end method

.method public static a(LX/0QB;)LX/2cm;
    .locals 1

    .prologue
    .line 441844
    invoke-static {p0}, LX/2cm;->b(LX/0QB;)LX/2cm;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 441845
    const-string v0, "media_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 441846
    if-nez v3, :cond_0

    .line 441847
    :goto_0
    return-void

    .line 441848
    :cond_0
    const-string v0, "extra_xed_location"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 441849
    if-eqz v0, :cond_1

    move-object v2, v1

    .line 441850
    :goto_1
    if-eqz p2, :cond_3

    .line 441851
    iget-object v0, p0, LX/2cm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9hh;

    const/4 p2, 0x0

    .line 441852
    new-instance v4, LX/9hd;

    invoke-direct {v4, v0, v3, v2, v1}, LX/9hd;-><init>(LX/9hh;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V

    .line 441853
    iget-object v5, v0, LX/9hh;->f:LX/0Uh;

    const/16 p0, 0x405

    invoke-virtual {v5, p0, p2}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 441854
    const/4 v5, 0x2

    new-array v5, v5, [LX/4VT;

    new-instance p0, LX/9hX;

    invoke-static {v2, v1}, LX/9hi;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p1

    invoke-direct {p0, v3, p1}, LX/9hX;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlace;)V

    aput-object p0, v5, p2

    const/4 p0, 0x1

    new-instance p1, LX/9hW;

    invoke-static {v2, v1}, LX/9hi;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object p2

    invoke-direct {p1, v3, p2}, LX/9hW;-><init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V

    aput-object p1, v5, p0

    invoke-static {v0, v4, v5}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 441855
    :goto_2
    goto :goto_0

    .line 441856
    :cond_1
    const-string v0, "text_only_place"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 441857
    const-string v0, "text_only_place"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    .line 441858
    :cond_2
    const-string v0, "extra_place"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v2, v0

    .line 441859
    goto :goto_1

    .line 441860
    :cond_3
    iget-object v0, p0, LX/2cm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9hh;

    const/4 p2, 0x0

    .line 441861
    new-instance v4, LX/9he;

    invoke-direct {v4, v0, v3, v2, v1}, LX/9he;-><init>(LX/9hh;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V

    .line 441862
    iget-object v5, v0, LX/9hh;->f:LX/0Uh;

    const/16 p0, 0x405

    invoke-virtual {v5, p0, p2}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 441863
    const/4 v5, 0x2

    new-array v5, v5, [LX/4VT;

    new-instance p0, LX/9i1;

    invoke-static {v2, v1}, LX/9hi;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p1

    invoke-direct {p0, v3, p1}, LX/9i1;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlace;)V

    aput-object p0, v5, p2

    const/4 p0, 0x1

    new-instance p1, LX/9i0;

    invoke-static {v2, v1}, LX/9hi;->c(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object p2

    invoke-direct {p1, v3, p2}, LX/9i0;-><init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;)V

    aput-object p1, v5, p0

    invoke-static {v0, v4, v5}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 441864
    :goto_3
    goto/16 :goto_0

    :cond_4
    iget-object v5, v0, LX/9hh;->d:LX/9hs;

    .line 441865
    new-instance p0, LX/9hk;

    new-instance p1, LX/9ho;

    invoke-direct {p1, v5, v3, v2, v1}, LX/9ho;-><init>(LX/9hs;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V

    invoke-direct {p0, v3, p1}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    move-object v5, p0

    .line 441866
    invoke-static {v0, v4, v5}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2

    :cond_5
    iget-object v5, v0, LX/9hh;->d:LX/9hs;

    .line 441867
    new-instance p0, LX/9hk;

    new-instance p1, LX/9hp;

    invoke-direct {p1, v5, v3, v2, v1}, LX/9hp;-><init>(LX/9hs;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V

    invoke-direct {p0, v3, p1}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    move-object v5, p0

    .line 441868
    invoke-static {v0, v4, v5}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/2cm;
    .locals 4

    .prologue
    .line 441869
    new-instance v1, LX/2cm;

    const/16 v0, 0x2e6e

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x18a9

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v2, v3, v0}, LX/2cm;-><init>(LX/0Ot;LX/0Ot;LX/03V;)V

    .line 441870
    return-object v1
.end method

.method private b(Landroid/content/Intent;)V
    .locals 13

    .prologue
    .line 441871
    const-string v0, "comment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 441872
    const-string v0, "extra_place"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 441873
    if-eqz v4, :cond_0

    if-nez v0, :cond_1

    .line 441874
    :cond_0
    :goto_0
    return-void

    .line 441875
    :cond_1
    const-string v1, "story_for_social_search"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 441876
    const-string v2, "feedback_for_social_search"

    invoke-static {p1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 441877
    iget-object v3, p0, LX/2cm;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8xt;

    const/4 v5, 0x0

    .line 441878
    if-eqz v1, :cond_3

    .line 441879
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    .line 441880
    new-instance v7, LX/8xr;

    invoke-direct {v7, v3, v6, v1, v4}, LX/8xr;-><init>(LX/8xt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    move-object v11, v7

    .line 441881
    :goto_1
    new-instance v6, LX/4XY;

    invoke-direct {v6}, LX/4XY;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 441882
    iput-object v7, v6, LX/4XY;->aT:Ljava/lang/String;

    .line 441883
    move-object v6, v6

    .line 441884
    const-string v7, "PLACEHOLDER_LOADING_PAGE_ID"

    .line 441885
    iput-object v7, v6, LX/4XY;->ag:Ljava/lang/String;

    .line 441886
    move-object v6, v6

    .line 441887
    invoke-virtual {v6}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v8

    .line 441888
    if-nez v2, :cond_6

    .line 441889
    const/4 v6, 0x0

    .line 441890
    :goto_2
    move-object v6, v6

    .line 441891
    if-eqz v6, :cond_2

    .line 441892
    iget-object v7, v3, LX/8xt;->f:LX/1K9;

    new-instance v9, LX/8q4;

    iget-object v10, v6, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v12, v6, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v12}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v7, v9}, LX/1K9;->a(LX/1KJ;)V

    .line 441893
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v7

    if-eqz v6, :cond_4

    iget-object v10, v6, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    :goto_3
    move-object v5, v3

    move-object v6, v4

    move-object v9, v1

    invoke-virtual/range {v5 .. v11}, LX/8xt;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLComment;LX/0TF;)V

    .line 441894
    goto :goto_0

    .line 441895
    :cond_3
    if-eqz v2, :cond_5

    .line 441896
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    .line 441897
    new-instance v7, LX/8xs;

    invoke-direct {v7, v3, v6, v2, v4}, LX/8xs;-><init>(LX/8xt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    move-object v11, v7

    .line 441898
    goto :goto_1

    :cond_4
    move-object v10, v5

    .line 441899
    goto :goto_3

    :cond_5
    move-object v11, v5

    goto :goto_1

    :cond_6
    iget-object v6, v3, LX/8xt;->d:LX/20j;

    invoke-virtual {v6, v2, v8, v4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)LX/6PS;

    move-result-object v6

    goto :goto_2
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 441900
    const/16 v0, 0x138a

    invoke-static {v0, p1, p2, p3}, LX/2ck;->a(IIILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 441901
    :goto_0
    return-void

    .line 441902
    :cond_0
    invoke-virtual {p0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 441903
    const-string v0, "launcher_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 441904
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 441905
    iget-object v0, p0, LX/2cm;->d:LX/03V;

    sget-object v1, LX/2cm;->a:Ljava/lang/String;

    const-string v2, "No matching LauncherType found"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441906
    :goto_1
    return-void

    .line 441907
    :sswitch_0
    const-string v4, "edit_photo_location"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "suggest_photo_location"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "add_location_comment_place_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 441908
    :pswitch_0
    invoke-direct {p0, p1, v2}, LX/2cm;->a(Landroid/content/Intent;Z)V

    goto :goto_1

    .line 441909
    :pswitch_1
    invoke-direct {p0, p1, v1}, LX/2cm;->a(Landroid/content/Intent;Z)V

    goto :goto_1

    .line 441910
    :pswitch_2
    invoke-direct {p0, p1}, LX/2cm;->b(Landroid/content/Intent;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0xb90670e -> :sswitch_2
        0x607ef13d -> :sswitch_1
        0x77dd55f7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
