.class public LX/3Fb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3FS;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/3J2;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/3J2;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 539744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539745
    iput-object p1, p0, LX/3Fb;->a:LX/3J2;

    .line 539746
    iput-object p2, p0, LX/3Fb;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 539747
    return-void
.end method

.method public static a(LX/0QB;)LX/3Fb;
    .locals 5

    .prologue
    .line 539748
    const-class v1, LX/3Fb;

    monitor-enter v1

    .line 539749
    :try_start_0
    sget-object v0, LX/3Fb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 539750
    sput-object v2, LX/3Fb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 539751
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539752
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 539753
    new-instance p0, LX/3Fb;

    invoke-static {v0}, LX/3J2;->a(LX/0QB;)LX/3J2;

    move-result-object v3

    check-cast v3, LX/3J2;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4}, LX/3Fb;-><init>(LX/3J2;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 539754
    move-object v0, p0

    .line 539755
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 539756
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Fb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539757
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 539758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3Fa;)V
    .locals 1

    .prologue
    .line 539759
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Fa;->c:LX/3FS;

    .line 539760
    return-void
.end method

.method public static a(LX/3Fa;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Fa;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 539761
    move-object v0, p1

    check-cast v0, LX/3FS;

    iget-object v1, p0, LX/3Fa;->b:LX/7QP;

    invoke-interface {v0, v1}, LX/3FS;->a(LX/7QP;)V

    .line 539762
    check-cast p1, LX/3FS;

    iput-object p1, p0, LX/3Fa;->c:LX/3FS;

    .line 539763
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;)LX/3Fa;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/3Fa;"
        }
    .end annotation

    .prologue
    .line 539764
    iget-object v0, p0, LX/3Fb;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1mI;->k:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 539765
    new-instance v1, LX/3Fa;

    invoke-direct {v1, v0}, LX/3Fa;-><init>(Ljava/lang/String;)V

    .line 539766
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539767
    iget-object v0, p0, LX/3Fb;->a:LX/3J2;

    iget-object v2, v1, LX/3Fa;->a:Ljava/lang/String;

    new-instance p1, LX/C2H;

    invoke-direct {p1, p0, v1}, LX/C2H;-><init>(LX/3Fb;LX/3Fa;)V

    invoke-virtual {v0, p2, v2, p1}, LX/3J2;->a(Ljava/lang/String;Ljava/lang/String;LX/Bu6;)V

    .line 539768
    :cond_0
    return-object v1
.end method
