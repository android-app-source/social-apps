.class public final LX/2Iz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0U1;

.field public static final B:LX/0U1;

.field public static final C:LX/0U1;

.field public static final D:LX/0U1;

.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392450
    new-instance v0, LX/0U1;

    const-string v1, "internal_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->a:LX/0U1;

    .line 392451
    new-instance v0, LX/0U1;

    const-string v1, "contact_id"

    const-string v2, "TEXT UNIQUE"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->b:LX/0U1;

    .line 392452
    new-instance v0, LX/0U1;

    const-string v1, "fbid"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->c:LX/0U1;

    .line 392453
    new-instance v0, LX/0U1;

    const-string v1, "first_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->d:LX/0U1;

    .line 392454
    new-instance v0, LX/0U1;

    const-string v1, "last_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->e:LX/0U1;

    .line 392455
    new-instance v0, LX/0U1;

    const-string v1, "display_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->f:LX/0U1;

    .line 392456
    new-instance v0, LX/0U1;

    const-string v1, "small_picture_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->g:LX/0U1;

    .line 392457
    new-instance v0, LX/0U1;

    const-string v1, "big_picture_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->h:LX/0U1;

    .line 392458
    new-instance v0, LX/0U1;

    const-string v1, "huge_picture_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->i:LX/0U1;

    .line 392459
    new-instance v0, LX/0U1;

    const-string v1, "small_picture_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->j:LX/0U1;

    .line 392460
    new-instance v0, LX/0U1;

    const-string v1, "big_picture_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->k:LX/0U1;

    .line 392461
    new-instance v0, LX/0U1;

    const-string v1, "huge_picture_size"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->l:LX/0U1;

    .line 392462
    new-instance v0, LX/0U1;

    const-string v1, "communication_rank"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->m:LX/0U1;

    .line 392463
    new-instance v0, LX/0U1;

    const-string v1, "is_mobile_pushable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->n:LX/0U1;

    .line 392464
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_user"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->o:LX/0U1;

    .line 392465
    new-instance v0, LX/0U1;

    const-string v1, "messenger_install_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->p:LX/0U1;

    .line 392466
    new-instance v0, LX/0U1;

    const-string v1, "added_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->q:LX/0U1;

    .line 392467
    new-instance v0, LX/0U1;

    const-string v1, "phonebook_section_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->r:LX/0U1;

    .line 392468
    new-instance v0, LX/0U1;

    const-string v1, "is_on_viewer_contact_list"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->s:LX/0U1;

    .line 392469
    new-instance v0, LX/0U1;

    const-string v1, "viewer_connection_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->t:LX/0U1;

    .line 392470
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->u:LX/0U1;

    .line 392471
    new-instance v0, LX/0U1;

    const-string v1, "link_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->v:LX/0U1;

    .line 392472
    new-instance v0, LX/0U1;

    const-string v1, "is_indexed"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->w:LX/0U1;

    .line 392473
    new-instance v0, LX/0U1;

    const-string v1, "data"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->x:LX/0U1;

    .line 392474
    new-instance v0, LX/0U1;

    const-string v1, "bday_day"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->y:LX/0U1;

    .line 392475
    new-instance v0, LX/0U1;

    const-string v1, "bday_month"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->z:LX/0U1;

    .line 392476
    new-instance v0, LX/0U1;

    const-string v1, "is_partial"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->A:LX/0U1;

    .line 392477
    new-instance v0, LX/0U1;

    const-string v1, "is_memorialized"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->B:LX/0U1;

    .line 392478
    new-instance v0, LX/0U1;

    const-string v1, "messenger_invite_priority"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->C:LX/0U1;

    .line 392479
    new-instance v0, LX/0U1;

    const-string v1, "last_fetch_time_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2Iz;->D:LX/0U1;

    return-void
.end method
