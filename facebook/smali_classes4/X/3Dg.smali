.class public LX/3Dg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/37C;

.field private final d:J

.field private final e:LX/1FQ;

.field public f:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/io/OutputStream;

.field private i:Ljava/io/InputStream;

.field private j:Ljava/io/OutputStream;

.field private k:Ljava/io/InputStream;

.field private l:LX/7PE;

.field public m:LX/7Os;

.field private n:I

.field public o:J

.field public p:J

.field private q:J

.field private r:Z

.field private s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535380
    const-class v0, LX/3Dg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Dg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ln;LX/37C;LX/3Da;JLX/1FQ;)V
    .locals 0
    .param p6    # LX/1FQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "LX/37C;",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;J",
            "Lcom/facebook/imagepipeline/memory/ByteArrayPool;",
            ")V"
        }
    .end annotation

    .prologue
    .line 535373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535374
    iput-object p1, p0, LX/3Dg;->b:LX/1Ln;

    .line 535375
    iput-object p2, p0, LX/3Dg;->c:LX/37C;

    .line 535376
    iput-object p3, p0, LX/3Dg;->f:LX/3Da;

    .line 535377
    iput-wide p4, p0, LX/3Dg;->d:J

    .line 535378
    iput-object p6, p0, LX/3Dg;->e:LX/1FQ;

    .line 535379
    return-void
.end method

.method public static a(LX/3Dg;[BII)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 535351
    const v1, 0x8000

    if-gt p3, v1, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 535352
    monitor-enter p0

    .line 535353
    :try_start_0
    iget-boolean v0, p0, LX/3Dg;->g:Z

    if-eqz v0, :cond_1

    .line 535354
    new-instance v0, LX/7Og;

    const-string v1, "Fetch cancelled"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/7Og;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 535355
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 535356
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 535357
    :cond_1
    :try_start_1
    iget-wide v0, p0, LX/3Dg;->q:J

    iget-wide v2, p0, LX/3Dg;->o:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 535358
    add-int/2addr v0, p3

    const v1, 0x32000

    if-le v0, v1, :cond_3

    .line 535359
    iget-wide v0, p0, LX/3Dg;->p:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    .line 535360
    iget-boolean v0, p0, LX/3Dg;->r:Z

    if-eqz v0, :cond_4

    .line 535361
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Dg;->s:Z

    .line 535362
    :cond_2
    :goto_1
    iget-wide v0, p0, LX/3Dg;->p:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_3

    .line 535363
    iget-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    .line 535364
    iget-wide v0, p0, LX/3Dg;->o:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/3Dg;->o:J

    .line 535365
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535366
    iget-object v0, p0, LX/3Dg;->h:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 535367
    iget-object v0, p0, LX/3Dg;->j:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 535368
    monitor-enter p0

    .line 535369
    :try_start_2
    iget-wide v0, p0, LX/3Dg;->q:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/3Dg;->q:J

    .line 535370
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 535371
    :cond_4
    :try_start_3
    invoke-static {p0}, LX/3Dg;->d$redex0(LX/3Dg;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 535372
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 5
    .param p0    # Ljava/io/Closeable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 535346
    if-nez p0, :cond_0

    .line 535347
    :goto_0
    return-void

    .line 535348
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 535349
    :catch_0
    move-exception v0

    .line 535350
    sget-object v1, LX/3Dg;->a:Ljava/lang/String;

    const-string v2, "Could not close %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/3Dg;[BII)I
    .locals 10

    .prologue
    const/4 v0, 0x1

    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    .line 535381
    const v2, 0x8000

    if-gt p3, v2, :cond_1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 535382
    monitor-enter p0

    .line 535383
    :try_start_0
    iget-wide v0, p0, LX/3Dg;->p:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_2

    .line 535384
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Dg;->r:Z

    .line 535385
    iget-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    .line 535386
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535387
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 535388
    monitor-enter p0

    .line 535389
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, LX/3Dg;->r:Z

    .line 535390
    iget-boolean v1, p0, LX/3Dg;->s:Z

    if-eqz v1, :cond_3

    .line 535391
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/3Dg;->s:Z

    .line 535392
    invoke-static {p0}, LX/3Dg;->d$redex0(LX/3Dg;)V

    .line 535393
    :cond_0
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 535394
    return v0

    :cond_1
    move v0, v1

    .line 535395
    goto :goto_0

    .line 535396
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/3Dg;->k:Ljava/io/InputStream;

    .line 535397
    iget-wide v2, p0, LX/3Dg;->q:J

    iget-wide v4, p0, LX/3Dg;->p:J

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    goto :goto_1

    .line 535398
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 535399
    :cond_3
    :try_start_3
    iget-wide v2, p0, LX/3Dg;->p:J

    cmp-long v1, v2, v8

    if-ltz v1, :cond_4

    .line 535400
    iget-wide v2, p0, LX/3Dg;->p:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/3Dg;->p:J

    .line 535401
    iget-wide v2, p0, LX/3Dg;->q:J

    iget-wide v4, p0, LX/3Dg;->p:J

    sub-long/2addr v2, v4

    .line 535402
    iget-wide v4, p0, LX/3Dg;->p:J

    iget-wide v6, p0, LX/3Dg;->o:J

    sub-long/2addr v4, v6

    .line 535403
    const-wide/32 v6, 0x8000

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    cmp-long v1, v4, v8

    if-ltz v1, :cond_0

    .line 535404
    iget-object v1, p0, LX/3Dg;->k:Ljava/io/InputStream;

    invoke-static {v1}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535405
    const/4 v1, 0x0

    iput-object v1, p0, LX/3Dg;->k:Ljava/io/InputStream;

    .line 535406
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/3Dg;->p:J

    .line 535407
    iget-object v1, p0, LX/3Dg;->i:Ljava/io/InputStream;

    invoke-virtual {v1, v4, v5}, Ljava/io/InputStream;->skip(J)J

    .line 535408
    iget-wide v2, p0, LX/3Dg;->o:J

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/3Dg;->o:J

    .line 535409
    iget v1, p0, LX/3Dg;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/3Dg;->n:I

    goto :goto_2

    .line 535410
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 535411
    :cond_4
    :try_start_4
    iget-wide v2, p0, LX/3Dg;->o:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/3Dg;->o:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2
.end method

.method private static d$redex0(LX/3Dg;)V
    .locals 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 535307
    iget-wide v0, p0, LX/3Dg;->p:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 535308
    iget-wide v0, p0, LX/3Dg;->o:J

    iput-wide v0, p0, LX/3Dg;->p:J

    .line 535309
    iget-object v0, p0, LX/3Dg;->f:LX/3Da;

    iget-wide v2, p0, LX/3Dg;->p:J

    invoke-interface {v0, v2, v3}, LX/3Da;->b(J)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, LX/3Dg;->k:Ljava/io/InputStream;

    .line 535310
    iget v0, p0, LX/3Dg;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/3Dg;->n:I

    .line 535311
    return-void

    .line 535312
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/3Dg;)V
    .locals 1

    .prologue
    .line 535339
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/3Dg;->g:Z

    .line 535340
    iget-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    invoke-static {v0}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535341
    iget-object v0, p0, LX/3Dg;->k:Ljava/io/InputStream;

    invoke-static {v0}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535342
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    .line 535343
    const/4 v0, 0x0

    iput-object v0, p0, LX/3Dg;->k:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535344
    monitor-exit p0

    return-void

    .line 535345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f(LX/3Dg;)V
    .locals 1

    .prologue
    .line 535313
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Dg;->h:Ljava/io/OutputStream;

    invoke-static {v0}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535314
    iget-object v0, p0, LX/3Dg;->j:Ljava/io/OutputStream;

    invoke-static {v0}, LX/3Dg;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535315
    monitor-exit p0

    return-void

    .line 535316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/7Os;
    .locals 1

    .prologue
    .line 535317
    iget-object v0, p0, LX/3Dg;->m:LX/7Os;

    return-object v0
.end method

.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 535318
    :try_start_0
    iget-object v0, p0, LX/3Dg;->f:LX/3Da;

    if-nez v0, :cond_0

    .line 535319
    iget-object v0, p0, LX/3Dg;->b:LX/1Ln;

    iget-object v1, p0, LX/3Dg;->c:LX/37C;

    invoke-interface {v0, v1, p1}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v0

    iput-object v0, p0, LX/3Dg;->f:LX/3Da;

    .line 535320
    :cond_0
    iget-object v0, p0, LX/3Dg;->f:LX/3Da;

    iget-wide v2, p0, LX/3Dg;->d:J

    invoke-interface {v0, v2, v3}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, LX/3Dg;->j:Ljava/io/OutputStream;

    .line 535321
    iget-object v0, p0, LX/3Dg;->e:LX/1FQ;

    if-eqz v0, :cond_1

    .line 535322
    new-instance v1, LX/7P0;

    iget-object v0, p0, LX/3Dg;->e:LX/1FQ;

    const v2, 0x32000

    invoke-interface {v0, v2}, LX/1FS;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v2, p0, LX/3Dg;->e:LX/1FQ;

    invoke-direct {v1, v0, v2}, LX/7P0;-><init>([BLX/1FN;)V

    .line 535323
    iget-object v0, v1, LX/7P0;->b:Ljava/io/InputStream;

    iput-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    .line 535324
    iget-object v0, v1, LX/7P0;->a:Ljava/io/OutputStream;

    iput-object v0, p0, LX/3Dg;->h:Ljava/io/OutputStream;

    .line 535325
    :goto_0
    iget-wide v0, p0, LX/3Dg;->d:J

    iput-wide v0, p0, LX/3Dg;->q:J

    .line 535326
    iget-wide v0, p0, LX/3Dg;->d:J

    iput-wide v0, p0, LX/3Dg;->o:J

    .line 535327
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3Dg;->p:J

    .line 535328
    const/4 v0, 0x0

    iput v0, p0, LX/3Dg;->n:I

    .line 535329
    new-instance v0, LX/7PE;

    invoke-direct {v0, p0}, LX/7PE;-><init>(LX/3Dg;)V

    iput-object v0, p0, LX/3Dg;->l:LX/7PE;

    .line 535330
    new-instance v0, LX/7Os;

    iget-object v1, p0, LX/3Dg;->l:LX/7PE;

    invoke-direct {v0, p1, v1}, LX/7Os;-><init>(LX/3Dd;Ljava/io/InputStream;)V

    iput-object v0, p0, LX/3Dg;->m:LX/7Os;

    .line 535331
    new-instance v0, LX/7PF;

    invoke-direct {v0, p0}, LX/7PF;-><init>(LX/3Dg;)V

    return-object v0

    .line 535332
    :cond_1
    new-instance v0, Ljava/io/PipedInputStream;

    const v1, 0x32000

    invoke-direct {v0, v1}, Ljava/io/PipedInputStream;-><init>(I)V

    iput-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    .line 535333
    new-instance v1, Ljava/io/PipedOutputStream;

    iget-object v0, p0, LX/3Dg;->i:Ljava/io/InputStream;

    check-cast v0, Ljava/io/PipedInputStream;

    invoke-direct {v1, v0}, Ljava/io/PipedOutputStream;-><init>(Ljava/io/PipedInputStream;)V

    iput-object v1, p0, LX/3Dg;->h:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 535334
    :catch_0
    move-exception v0

    .line 535335
    iget-object v1, p0, LX/3Dg;->j:Ljava/io/OutputStream;

    invoke-static {v1}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535336
    iget-object v1, p0, LX/3Dg;->i:Ljava/io/InputStream;

    invoke-static {v1}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535337
    iget-object v1, p0, LX/3Dg;->h:Ljava/io/OutputStream;

    invoke-static {v1}, LX/3Dg;->a(Ljava/io/Closeable;)V

    .line 535338
    throw v0
.end method
