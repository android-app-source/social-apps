.class public LX/2gV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2Hv;

.field public final d:LX/0SG;

.field public final e:LX/0So;

.field public final f:LX/0ZR;

.field public final g:LX/2Bs;

.field private final h:LX/0Sh;

.field private final i:LX/2gW;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/76H;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:LX/1tH;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448490
    const-class v0, LX/2gV;

    sput-object v0, LX/2gV;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2Hv;LX/0SG;LX/2Bs;LX/0Sh;LX/0So;LX/0ZR;)V
    .locals 1

    .prologue
    .line 448502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448503
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2gV;->j:Ljava/util/Set;

    .line 448504
    iput-object p1, p0, LX/2gV;->b:Landroid/content/Context;

    .line 448505
    iput-object p2, p0, LX/2gV;->c:LX/2Hv;

    .line 448506
    iput-object p3, p0, LX/2gV;->d:LX/0SG;

    .line 448507
    iput-object p4, p0, LX/2gV;->g:LX/2Bs;

    .line 448508
    iput-object p5, p0, LX/2gV;->h:LX/0Sh;

    .line 448509
    iput-object p6, p0, LX/2gV;->e:LX/0So;

    .line 448510
    iput-object p7, p0, LX/2gV;->f:LX/0ZR;

    .line 448511
    new-instance v0, LX/2gW;

    invoke-direct {v0, p0}, LX/2gW;-><init>(LX/2gV;)V

    iput-object v0, p0, LX/2gV;->i:LX/2gW;

    .line 448512
    return-void
.end method

.method private declared-synchronized a(LX/76H;)V
    .locals 1

    .prologue
    .line 448513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2gV;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448514
    monitor-exit p0

    return-void

    .line 448515
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/2gV;LX/1tH;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 448516
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2gV;->k:LX/1tH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448517
    monitor-exit p0

    return-void

    .line 448518
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/2gV;Ljava/lang/String;[BLX/76J;)LX/76M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[B",
            "LX/76J",
            "<TT;>;)",
            "LX/76M",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 448519
    const-wide/16 v0, 0x1388

    invoke-virtual {p0, v0, v1}, LX/2gV;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448520
    sget-object v0, LX/76L;->MQTT_FAILED_TO_CONNECT:LX/76L;

    iget-object v1, p0, LX/2gV;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/76M;->a(LX/76L;J)LX/76M;

    move-result-object v0

    .line 448521
    :goto_0
    return-object v0

    .line 448522
    :cond_0
    invoke-virtual {p3}, LX/76J;->c()V

    .line 448523
    :try_start_0
    iget-object v0, p0, LX/2gV;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 448524
    sget-object v0, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/2gV;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 448525
    :goto_1
    if-nez v0, :cond_2

    .line 448526
    sget-object v0, LX/76L;->MQTT_PUBLISH_FAILED:LX/76L;

    invoke-static {v0, v2, v3}, LX/76M;->a(LX/76L;J)LX/76M;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 448527
    invoke-virtual {p3}, LX/76J;->d()V

    goto :goto_0

    .line 448528
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 448529
    :cond_2
    const-wide/16 v0, 0xbb8

    :try_start_1
    invoke-virtual {p3, v0, v1}, LX/76J;->a(J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 448530
    sget-object v0, LX/76L;->MQTT_DID_NOT_RECEIVE_RESPONSE:LX/76L;

    invoke-static {v0, v2, v3}, LX/76M;->a(LX/76L;J)LX/76M;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 448531
    invoke-virtual {p3}, LX/76J;->d()V

    goto :goto_0

    .line 448532
    :catch_0
    move-exception v0

    .line 448533
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 448534
    invoke-static {v0, v2, v3}, LX/76M;->a(Ljava/lang/Exception;J)LX/76M;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 448535
    invoke-virtual {p3}, LX/76J;->d()V

    goto :goto_0

    .line 448536
    :cond_3
    :try_start_3
    iget-object v0, p3, LX/76J;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 448537
    invoke-static {v0, v2, v3}, LX/76M;->a(Ljava/lang/Object;J)LX/76M;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 448538
    invoke-virtual {p3}, LX/76J;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p3}, LX/76J;->d()V

    throw v0
.end method

.method public static declared-synchronized b(LX/2gV;LX/76H;)V
    .locals 1

    .prologue
    .line 448539
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2gV;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448540
    monitor-exit p0

    return-void

    .line 448541
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h(LX/2gV;)LX/1tH;
    .locals 1

    .prologue
    .line 448556
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2gV;->i()V

    .line 448557
    iget-object v0, p0, LX/2gV;->k:LX/1tH;

    .line 448558
    if-nez v0, :cond_0

    .line 448559
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448561
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method private declared-synchronized i()V
    .locals 1

    .prologue
    .line 448542
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2gV;->l:Z

    if-nez v0, :cond_0

    .line 448543
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448545
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized j(LX/2gV;)V
    .locals 1

    .prologue
    .line 448546
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2gV;->l:Z

    if-eqz v0, :cond_0

    .line 448547
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448549
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 448550
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2gV;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76H;

    .line 448551
    invoke-interface {v0}, LX/76H;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 448552
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448553
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2gV;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448554
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;LX/2I2;LX/76H;)I
    .locals 1
    .param p4    # LX/76H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448555
    invoke-virtual {p2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3, p4}, LX/2gV;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    .locals 3
    .param p4    # LX/76H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448491
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v1

    .line 448492
    const/4 v0, 0x0

    .line 448493
    if-eqz p4, :cond_0

    .line 448494
    new-instance v0, LX/76I;

    invoke-direct {v0, p0, p4}, LX/76I;-><init>(LX/2gV;LX/76H;)V

    .line 448495
    invoke-direct {p0, p4}, LX/2gV;->a(LX/76H;)V

    .line 448496
    :cond_0
    invoke-virtual {p3}, LX/2I2;->getValue()I

    move-result v2

    invoke-interface {v1, p1, p2, v2, v0}, LX/1tH;->a(Ljava/lang/String;[BILX/76B;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;LX/0lF;Ljava/lang/String;LX/FGX;)LX/76M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/0lF;",
            "Ljava/lang/String;",
            "Lcom/facebook/push/mqtt/service/response/JsonMqttResponseProcessor$Callback",
            "<TT;>;)",
            "LX/76M",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 448497
    :try_start_0
    iget-object v0, p0, LX/2gV;->c:LX/2Hv;

    invoke-virtual {v0, p3, p4}, LX/2Hv;->a(Ljava/lang/String;LX/FGX;)LX/76J;

    move-result-object v0

    .line 448498
    invoke-virtual {p2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {p0, p1, v1, v0}, LX/2gV;->b(LX/2gV;Ljava/lang/String;[BLX/76J;)LX/76M;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 448499
    :goto_0
    return-object v0

    .line 448500
    :catch_0
    move-exception v0

    .line 448501
    iget-object v1, p0, LX/2gV;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/76M;->a(Ljava/lang/Exception;J)LX/76M;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 448429
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448430
    invoke-interface {v0, p1, p2}, LX/1tH;->a(J)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;LX/0lF;J)Z
    .locals 9

    .prologue
    .line 448431
    invoke-virtual {p2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v3

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, LX/2gV;->a(Ljava/lang/String;[BJJ)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;[BJJ)Z
    .locals 9

    .prologue
    .line 448432
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448433
    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v6, p5

    invoke-interface/range {v0 .. v7}, LX/1tH;->a(Ljava/lang/String;[BJLX/76B;J)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;[BJLX/76H;JLjava/lang/Integer;)Z
    .locals 9
    .param p5    # LX/76H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448434
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448435
    const/4 v5, 0x0

    .line 448436
    if-eqz p5, :cond_0

    .line 448437
    new-instance v5, LX/76I;

    invoke-direct {v5, p0, p5}, LX/76I;-><init>(LX/2gV;LX/76H;)V

    .line 448438
    invoke-direct {p0, p5}, LX/2gV;->a(LX/76H;)V

    .line 448439
    :cond_0
    if-eqz p8, :cond_1

    invoke-static/range {p8 .. p8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v6, p6

    invoke-interface/range {v0 .. v8}, LX/1tH;->a(Ljava/lang/String;[BJLX/76B;JLjava/lang/String;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 448440
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448441
    invoke-interface {v0}, LX/1tH;->b()Z

    move-result v0

    return v0
.end method

.method public final b(J)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 448442
    iget-object v0, p0, LX/2gV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 448443
    invoke-static {p0}, LX/2gV;->j(LX/2gV;)V

    .line 448444
    iget-object v0, p0, LX/2gV;->g:LX/2Bs;

    iget-object v1, p0, LX/2gV;->b:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, LX/2gV;->i:LX/2gW;

    const-string v5, "MqttPushServiceClientImpl"

    invoke-virtual/range {v0 .. v5}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;ILjava/lang/String;)LX/2XW;

    move-result-object v0

    .line 448445
    iget-boolean v1, v0, LX/2XW;->a:Z

    if-nez v1, :cond_0

    move v4, v6

    .line 448446
    :goto_0
    return v4

    .line 448447
    :cond_0
    iget-object v1, v0, LX/2XW;->b:Landroid/os/IBinder;

    if-eqz v1, :cond_2

    .line 448448
    iget-object v1, p0, LX/2gV;->i:LX/2gW;

    iget-object v0, v0, LX/2XW;->b:Landroid/os/IBinder;

    invoke-virtual {v1, v0}, LX/2gW;->a(Landroid/os/IBinder;)V

    .line 448449
    :cond_1
    monitor-enter p0

    .line 448450
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/2gV;->l:Z

    .line 448451
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 448452
    :cond_2
    iget-object v0, p0, LX/2gV;->i:LX/2gW;

    .line 448453
    iget-object v1, v0, LX/2gW;->b:Ljava/util/concurrent/CountDownLatch;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, p1, p2, v2}, LX/0Sa;->a(Ljava/util/concurrent/CountDownLatch;JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    move v0, v1

    .line 448454
    if-nez v0, :cond_1

    move v4, v6

    .line 448455
    goto :goto_0
.end method

.method public final declared-synchronized c()LX/1Mb;
    .locals 1

    .prologue
    .line 448456
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2gV;->i()V

    .line 448457
    iget-object v0, p0, LX/2gV;->k:LX/1tH;

    .line 448458
    if-nez v0, :cond_0

    .line 448459
    sget-object v0, LX/1Mb;->DISCONNECTED:LX/1Mb;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448460
    :goto_0
    monitor-exit p0

    return-object v0

    .line 448461
    :cond_0
    :try_start_1
    invoke-interface {v0}, LX/1tH;->c()Ljava/lang/String;

    move-result-object v0

    .line 448462
    invoke-static {v0}, LX/1Mb;->valueOf(Ljava/lang/String;)LX/1Mb;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 448463
    :catch_0
    :try_start_2
    sget-object v0, LX/1Mb;->DISCONNECTED:LX/1Mb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 448464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448465
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448466
    invoke-interface {v0}, LX/1tH;->d()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 448467
    :goto_0
    monitor-exit p0

    return-object v0

    .line 448468
    :catch_0
    move-exception v0

    .line 448469
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 448470
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448471
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 448472
    invoke-interface {v0}, LX/1tH;->e()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 448473
    :goto_0
    monitor-exit p0

    return-object v0

    .line 448474
    :catch_0
    move-exception v0

    .line 448475
    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 448476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 7

    .prologue
    .line 448477
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2gV;->l:Z

    if-eqz v0, :cond_0

    .line 448478
    iget-object v0, p0, LX/2gV;->i:LX/2gW;

    .line 448479
    iget-object v1, p0, LX/2gV;->f:LX/0ZR;

    new-instance v2, LX/2Yu;

    iget-object v3, p0, LX/2gV;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    const-string v5, "ServiceUnbound (MqttPushServiceClientManager)"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-direct {v2, v3, v4, v5, v6}, LX/2Yu;-><init>(JLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, LX/0ZK;->a(LX/0ki;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448480
    :try_start_1
    iget-object v1, p0, LX/2gV;->g:LX/2Bs;

    invoke-virtual {v1, v0}, LX/2Bs;->a(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448481
    :goto_0
    :try_start_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2gV;->l:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 448482
    :cond_0
    monitor-exit p0

    return-void

    .line 448483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448484
    :catch_0
    move-exception v1

    .line 448485
    sget-object v2, LX/2gV;->a:Ljava/lang/Class;

    const-string v3, "Exception unbinding"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final declared-synchronized g()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 448486
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2gV;->k:LX/1tH;

    .line 448487
    invoke-direct {p0}, LX/2gV;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448488
    monitor-exit p0

    return-void

    .line 448489
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
