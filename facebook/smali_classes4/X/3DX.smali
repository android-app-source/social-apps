.class public final LX/3DX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3DY;
.implements LX/3DW;


# instance fields
.field private final a:LX/3DW;

.field private final b:LX/0So;

.field private c:J


# direct methods
.method public constructor <init>(LX/3DW;LX/0So;)V
    .locals 2

    .prologue
    .line 535186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535187
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3DX;->c:J

    .line 535188
    iput-object p1, p0, LX/3DX;->a:LX/3DW;

    .line 535189
    iput-object p2, p0, LX/3DX;->b:LX/0So;

    .line 535190
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 535185
    iget-wide v0, p0, LX/3DX;->c:J

    return-wide v0
.end method

.method public final a(JJ)V
    .locals 7

    .prologue
    .line 535177
    iget-wide v0, p0, LX/3DX;->c:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/3DX;->c:J

    .line 535178
    iget-object v0, p0, LX/3DX;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sget-wide v2, LX/1Lv;->b:J

    sub-long/2addr v0, v2

    .line 535179
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 535180
    iget-wide v2, p0, LX/3DX;->c:J

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    div-long v0, v2, v0

    .line 535181
    sput-wide v0, LX/1Lv;->c:J

    .line 535182
    :cond_0
    return-void
.end method

.method public final a(JJLX/3Di;)V
    .locals 7

    .prologue
    .line 535183
    iget-object v1, p0, LX/3DX;->a:LX/3DW;

    new-instance v6, LX/3Dj;

    invoke-direct {v6, p0, p5}, LX/3Dj;-><init>(LX/3DX;LX/3Di;)V

    move-wide v2, p1

    move-wide v4, p3

    invoke-interface/range {v1 .. v6}, LX/3DW;->a(JJLX/3Di;)V

    .line 535184
    return-void
.end method
