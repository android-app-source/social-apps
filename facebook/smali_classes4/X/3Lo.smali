.class public LX/3Lo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/3Lo;


# instance fields
.field private final b:LX/0W9;

.field private final c:LX/3Lp;

.field private final d:LX/3Lt;

.field private final e:LX/3Lu;

.field private f:LX/03R;

.field private g:Ljava/text/BreakIterator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 552165
    const-class v0, LX/3Lo;

    sput-object v0, LX/3Lo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W9;LX/3Lp;LX/3Lt;LX/3Lu;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552015
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/3Lo;->f:LX/03R;

    .line 552016
    iput-object p1, p0, LX/3Lo;->b:LX/0W9;

    .line 552017
    iput-object p2, p0, LX/3Lo;->c:LX/3Lp;

    .line 552018
    iput-object p3, p0, LX/3Lo;->d:LX/3Lt;

    .line 552019
    iput-object p4, p0, LX/3Lo;->e:LX/3Lu;

    .line 552020
    invoke-static {}, Ljava/text/BreakIterator;->getCharacterInstance()Ljava/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, LX/3Lo;->g:Ljava/text/BreakIterator;

    .line 552021
    return-void
.end method

.method public static a(LX/0QB;)LX/3Lo;
    .locals 7

    .prologue
    .line 552022
    sget-object v0, LX/3Lo;->h:LX/3Lo;

    if-nez v0, :cond_1

    .line 552023
    const-class v1, LX/3Lo;

    monitor-enter v1

    .line 552024
    :try_start_0
    sget-object v0, LX/3Lo;->h:LX/3Lo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 552025
    if-eqz v2, :cond_0

    .line 552026
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 552027
    new-instance p0, LX/3Lo;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {v0}, LX/3Lp;->a(LX/0QB;)LX/3Lp;

    move-result-object v4

    check-cast v4, LX/3Lp;

    invoke-static {v0}, LX/3Lr;->a(LX/0QB;)LX/3Lt;

    move-result-object v5

    check-cast v5, LX/3Lt;

    invoke-static {v0}, LX/3Lu;->a(LX/0QB;)LX/3Lu;

    move-result-object v6

    check-cast v6, LX/3Lu;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3Lo;-><init>(LX/0W9;LX/3Lp;LX/3Lt;LX/3Lu;)V

    .line 552028
    move-object v0, p0

    .line 552029
    sput-object v0, LX/3Lo;->h:LX/3Lo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 552030
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 552031
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 552032
    :cond_1
    sget-object v0, LX/3Lo;->h:LX/3Lo;

    return-object v0

    .line 552033
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 552034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/3hE;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 552035
    invoke-static {p0, p1}, LX/3Lo;->c(LX/3Lo;LX/3hE;)Ljava/lang/String;

    move-result-object v0

    .line 552036
    if-nez v0, :cond_0

    .line 552037
    const/4 v0, 0x0

    .line 552038
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3Lo;->e:LX/3Lu;

    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 552039
    invoke-static {v1}, LX/3Lu;->b(LX/3Lu;)V

    .line 552040
    :try_start_0
    iget-object v2, v1, LX/3Lu;->k:LX/3hH;

    invoke-virtual {v2, v0}, LX/3hH;->a(Ljava/lang/String;)I

    move-result v2

    .line 552041
    iget-object v3, v1, LX/3Lu;->k:LX/3hH;

    invoke-virtual {v3, v2}, LX/3hH;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 552042
    :goto_1
    move-object v0, v2

    .line 552043
    goto :goto_0

    .line 552044
    :catch_0
    move-exception v2

    .line 552045
    sget-object v3, LX/3Lu;->f:Ljava/lang/Class;

    const-string v4, "Internal error getting label for %s"

    new-array p0, p0, [Ljava/lang/Object;

    aput-object v0, p0, p1

    invoke-static {v3, v2, v4, p0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 552046
    :goto_2
    const/4 v2, 0x0

    goto :goto_1

    .line 552047
    :catch_1
    move-exception v2

    .line 552048
    sget-object v3, LX/3Lu;->f:Ljava/lang/Class;

    const-string v4, "Access error getting label for %s"

    new-array p0, p0, [Ljava/lang/Object;

    aput-object v0, p0, p1

    invoke-static {v3, v2, v4, p0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private static a(LX/3Lo;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CatchGeneralException"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 552049
    iget-object v0, p0, LX/3Lo;->f:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552050
    iget-object v0, p0, LX/3Lo;->f:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 552051
    :goto_0
    return v0

    .line 552052
    :cond_0
    :try_start_0
    const-string v0, "SELECT GET_PHONEBOOK_INDEX(\'A\', \'en_US\')"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 552053
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/3Lo;->f:LX/03R;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552054
    if-eqz v1, :cond_1

    .line 552055
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552056
    :cond_1
    :goto_1
    iget-object v0, p0, LX/3Lo;->f:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    goto :goto_0

    .line 552057
    :catch_0
    move-exception v0

    .line 552058
    :try_start_1
    sget-object v2, LX/03R;->NO:LX/03R;

    iput-object v2, p0, LX/3Lo;->f:LX/03R;

    .line 552059
    sget-object v2, LX/3Lo;->a:Ljava/lang/Class;

    const-string v3, ""

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 552060
    if-eqz v1, :cond_1

    .line 552061
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 552062
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 552063
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private static b(LX/3Lo;LX/3hE;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 552064
    invoke-static {p0, p1}, LX/3Lo;->c(LX/3Lo;LX/3hE;)Ljava/lang/String;

    move-result-object v0

    .line 552065
    iget-object v1, p0, LX/3Lo;->g:Ljava/text/BreakIterator;

    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 552066
    iget-object v1, p0, LX/3Lo;->g:Ljava/text/BreakIterator;

    invoke-virtual {v1}, Ljava/text/BreakIterator;->first()I

    move-result v1

    .line 552067
    iget-object v2, p0, LX/3Lo;->g:Ljava/text/BreakIterator;

    invoke-virtual {v2}, Ljava/text/BreakIterator;->next()I

    move-result v2

    .line 552068
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 552069
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 552070
    :cond_0
    return-object v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 552071
    invoke-static {p0, p2}, LX/3Lo;->b(LX/3Lo;LX/3hE;)Ljava/lang/String;

    move-result-object v0

    .line 552072
    if-eqz v0, :cond_1

    invoke-static {p0, p1}, LX/3Lo;->a(LX/3Lo;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 552073
    const-string v2, "SELECT GET_PHONEBOOK_INDEX(%s, %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, p0, LX/3Lo;->b:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 552074
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 552075
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 552076
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 552077
    :cond_0
    if-eqz v1, :cond_1

    .line 552078
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 552079
    :cond_1
    return-object v0

    .line 552080
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 552081
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private static c(LX/3Lo;LX/3hE;)Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 552082
    new-instance v1, LX/3hG;

    invoke-direct {v1}, LX/3hG;-><init>()V

    .line 552083
    iget-object v0, p1, LX/3hE;->b:Ljava/lang/String;

    iput-object v0, v1, LX/3hG;->b:Ljava/lang/String;

    .line 552084
    iget-object v0, p1, LX/3hE;->c:Ljava/lang/String;

    iput-object v0, v1, LX/3hG;->d:Ljava/lang/String;

    .line 552085
    iput v2, v1, LX/3hG;->f:I

    .line 552086
    iget-object v0, p1, LX/3hE;->e:Ljava/lang/String;

    iput-object v0, v1, LX/3hG;->i:Ljava/lang/String;

    .line 552087
    iget-object v0, p1, LX/3hE;->f:Ljava/lang/String;

    iput-object v0, v1, LX/3hG;->g:Ljava/lang/String;

    .line 552088
    iput v2, v1, LX/3hG;->j:I

    .line 552089
    iget-object v0, p1, LX/3hE;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 552090
    iget-object v0, p1, LX/3hE;->d:Ljava/lang/String;

    const/4 v2, 0x0

    .line 552091
    if-nez v0, :cond_5

    .line 552092
    :cond_0
    :goto_0
    move v0, v2

    .line 552093
    iput v0, v1, LX/3hG;->j:I

    .line 552094
    iget-object v0, p0, LX/3Lo;->d:LX/3Lt;

    const/4 v6, 0x0

    const/4 v12, 0x0

    .line 552095
    iget-object v7, v1, LX/3hG;->g:Ljava/lang/String;

    iget-object v8, v1, LX/3hG;->h:Ljava/lang/String;

    iget-object v9, v1, LX/3hG;->i:Ljava/lang/String;

    const/4 v11, 0x1

    move-object v5, v0

    move-object v10, v6

    move v13, v12

    invoke-static/range {v5 .. v13}, LX/3Lt;->a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 552096
    :cond_1
    :goto_1
    return-object v0

    .line 552097
    :cond_2
    iget-object v0, p0, LX/3Lo;->d:LX/3Lt;

    iget-object v3, p1, LX/3hE;->a:Ljava/lang/String;

    const/4 v2, 0x0

    .line 552098
    if-nez v3, :cond_9

    .line 552099
    :cond_3
    :goto_2
    move v2, v2

    .line 552100
    invoke-virtual {v0, v2}, LX/3Lt;->a(I)I

    move-result v0

    iput v0, v1, LX/3hG;->f:I

    .line 552101
    iget-object v0, p0, LX/3Lo;->d:LX/3Lt;

    .line 552102
    if-eqz v4, :cond_13

    iget-object v6, v1, LX/3hG;->a:Ljava/lang/String;

    .line 552103
    :goto_3
    iget v5, v1, LX/3hG;->f:I

    packed-switch v5, :pswitch_data_0

    .line 552104
    if-eqz v4, :cond_14

    .line 552105
    iget-object v7, v1, LX/3hG;->b:Ljava/lang/String;

    iget-object v8, v1, LX/3hG;->c:Ljava/lang/String;

    iget-object v9, v1, LX/3hG;->d:Ljava/lang/String;

    iget-object v10, v1, LX/3hG;->e:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object v5, v0

    invoke-static/range {v5 .. v13}, LX/3Lt;->a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v5

    .line 552106
    :goto_4
    move-object v0, v5

    .line 552107
    iget-object v2, p0, LX/3Lo;->e:LX/3Lu;

    invoke-virtual {v2}, LX/3Lu;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 552108
    iget v2, v1, LX/3hG;->f:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    iget v2, v1, LX/3hG;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 552109
    :cond_4
    iget-object v2, p0, LX/3Lo;->c:LX/3Lp;

    iget v1, v1, LX/3hG;->f:I

    .line 552110
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 552111
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, LX/3Lp;->a(LX/3Lp;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, LX/3Lp;->b(LX/3Lp;Ljava/lang/Integer;)LX/3Lq;

    move-result-object v1

    move-object v3, v1

    .line 552112
    invoke-virtual {v3, v0}, LX/3Lq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 552113
    goto :goto_1

    .line 552114
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    move v3, v2

    .line 552115
    :goto_5
    if-ge v3, v4, :cond_0

    .line 552116
    invoke-static {v0, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v5

    .line 552117
    invoke-static {v5}, Ljava/lang/Character;->isLetter(I)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 552118
    invoke-static {v5}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v6

    .line 552119
    invoke-static {v6}, LX/3Lt;->d(Ljava/lang/Character$UnicodeBlock;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 552120
    const/4 v2, 0x4

    goto/16 :goto_0

    .line 552121
    :cond_6
    invoke-static {v6}, LX/3Lt;->c(Ljava/lang/Character$UnicodeBlock;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 552122
    const/4 v2, 0x5

    goto/16 :goto_0

    .line 552123
    :cond_7
    invoke-static {v6}, LX/3Lt;->a(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 552124
    const/4 v2, 0x3

    goto/16 :goto_0

    .line 552125
    :cond_8
    invoke-static {v5}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 552126
    goto :goto_5

    .line 552127
    :cond_9
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    move v5, v2

    .line 552128
    :goto_6
    if-ge v5, v6, :cond_3

    .line 552129
    invoke-static {v3, v5}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v7

    .line 552130
    invoke-static {v7}, Ljava/lang/Character;->isLetter(I)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 552131
    invoke-static {v7}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    .line 552132
    invoke-static {v2}, LX/3Lt;->a(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 552133
    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq v2, v8, :cond_a

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-ne v2, v8, :cond_f

    :cond_a
    const/4 v8, 0x1

    :goto_7
    move v8, v8

    .line 552134
    if-eqz v8, :cond_b

    .line 552135
    invoke-static {v7}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v2, v5

    .line 552136
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    .line 552137
    :goto_8
    if-ge v2, v5, :cond_12

    .line 552138
    invoke-static {v3, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v6

    .line 552139
    invoke-static {v6}, Ljava/lang/Character;->isLetter(I)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 552140
    invoke-static {v6}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v7

    .line 552141
    invoke-static {v7}, LX/3Lt;->d(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 552142
    const/4 v5, 0x4

    .line 552143
    :goto_9
    move v2, v5

    .line 552144
    goto/16 :goto_2

    .line 552145
    :cond_b
    invoke-static {v2}, LX/3Lt;->d(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 552146
    const/4 v2, 0x4

    goto/16 :goto_2

    .line 552147
    :cond_c
    invoke-static {v2}, LX/3Lt;->c(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 552148
    const/4 v2, 0x5

    goto/16 :goto_2

    .line 552149
    :cond_d
    const/4 v2, 0x1

    .line 552150
    :cond_e
    invoke-static {v7}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    add-int/2addr v5, v7

    .line 552151
    goto :goto_6

    :cond_f
    const/4 v8, 0x0

    goto :goto_7

    .line 552152
    :cond_10
    invoke-static {v7}, LX/3Lt;->c(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 552153
    const/4 v5, 0x5

    goto :goto_9

    .line 552154
    :cond_11
    invoke-static {v6}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/2addr v2, v6

    .line 552155
    goto :goto_8

    .line 552156
    :cond_12
    const/4 v5, 0x2

    goto :goto_9

    .line 552157
    :cond_13
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 552158
    :pswitch_0
    iget-object v7, v1, LX/3hG;->d:Ljava/lang/String;

    iget-object v8, v1, LX/3hG;->c:Ljava/lang/String;

    iget-object v9, v1, LX/3hG;->b:Ljava/lang/String;

    iget-object v10, v1, LX/3hG;->e:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v0

    invoke-static/range {v5 .. v13}, LX/3Lt;->a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 552159
    :pswitch_1
    iget-object v7, v1, LX/3hG;->d:Ljava/lang/String;

    iget-object v8, v1, LX/3hG;->c:Ljava/lang/String;

    iget-object v9, v1, LX/3hG;->b:Ljava/lang/String;

    iget-object v10, v1, LX/3hG;->e:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v0

    invoke-static/range {v5 .. v13}, LX/3Lt;->a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 552160
    :cond_14
    iget-object v7, v1, LX/3hG;->d:Ljava/lang/String;

    iget-object v8, v1, LX/3hG;->b:Ljava/lang/String;

    iget-object v9, v1, LX/3hG;->c:Ljava/lang/String;

    iget-object v10, v1, LX/3hG;->e:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object v5, v0

    invoke-static/range {v5 .. v13}, LX/3Lt;->a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 552161
    iget-object v0, p0, LX/3Lo;->e:LX/3Lu;

    invoke-virtual {v0}, LX/3Lu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552162
    invoke-direct {p0, p2}, LX/3Lo;->a(LX/3hE;)Ljava/lang/String;

    move-result-object v0

    .line 552163
    if-eqz v0, :cond_0

    .line 552164
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, LX/3Lo;->b(Landroid/database/sqlite/SQLiteDatabase;LX/3hE;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
