.class public final LX/27q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/account/recovery/common/model/AccountRecoveryData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 373170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 373171
    new-instance v0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-direct {v0, p1}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 373172
    new-array v0, p1, [Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    return-object v0
.end method
