.class public LX/2LK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/1rV;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2LK;


# instance fields
.field private final a:LX/0TD;

.field private final b:LX/1qw;

.field public final c:LX/0iA;


# direct methods
.method public constructor <init>(LX/0TD;LX/1qw;LX/0iA;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394809
    iput-object p1, p0, LX/2LK;->a:LX/0TD;

    .line 394810
    iput-object p2, p0, LX/2LK;->b:LX/1qw;

    .line 394811
    iput-object p3, p0, LX/2LK;->c:LX/0iA;

    .line 394812
    return-void
.end method

.method public static a(LX/0QB;)LX/2LK;
    .locals 6

    .prologue
    .line 394813
    sget-object v0, LX/2LK;->d:LX/2LK;

    if-nez v0, :cond_1

    .line 394814
    const-class v1, LX/2LK;

    monitor-enter v1

    .line 394815
    :try_start_0
    sget-object v0, LX/2LK;->d:LX/2LK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394816
    if-eqz v2, :cond_0

    .line 394817
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394818
    new-instance p0, LX/2LK;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v4

    check-cast v4, LX/1qw;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-direct {p0, v3, v4, v5}, LX/2LK;-><init>(LX/0TD;LX/1qw;LX/0iA;)V

    .line 394819
    move-object v0, p0

    .line 394820
    sput-object v0, LX/2LK;->d:LX/2LK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394821
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394822
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394823
    :cond_1
    sget-object v0, LX/2LK;->d:LX/2LK;

    return-object v0

    .line 394824
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/Locale;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 394807
    iget-object v0, p0, LX/2LK;->a:LX/0TD;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialDataCleaner$1;

    invoke-direct {v1, p0}, Lcom/facebook/interstitial/manager/InterstitialDataCleaner$1;-><init>(LX/2LK;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 394805
    iget-object v0, p0, LX/2LK;->b:LX/1qw;

    invoke-virtual {v0, p0}, LX/1qw;->a(LX/1rV;)V

    .line 394806
    return-void
.end method
