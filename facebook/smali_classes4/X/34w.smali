.class public LX/34w;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Zb;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/03V;

.field public final e:LX/34x;

.field public final f:LX/0wM;

.field public final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496170
    const-class v0, LX/34w;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/34w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/34x;LX/0wM;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496179
    iput-object p1, p0, LX/34w;->b:LX/0Zb;

    .line 496180
    iput-object p2, p0, LX/34w;->c:Lcom/facebook/content/SecureContextHelper;

    .line 496181
    iput-object p3, p0, LX/34w;->d:LX/03V;

    .line 496182
    iput-object p4, p0, LX/34w;->e:LX/34x;

    .line 496183
    iput-object p5, p0, LX/34w;->f:LX/0wM;

    .line 496184
    iput-object p6, p0, LX/34w;->g:LX/0ad;

    .line 496185
    return-void
.end method

.method public static synthetic a(LX/34w;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 8

    .prologue
    .line 496186
    new-instance v1, LX/DDr;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, LX/DDr;-><init>(LX/34w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    move-object v0, v1

    .line 496187
    return-object v0
.end method

.method public static a$redex0(LX/34w;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 496171
    iget-object v0, p0, LX/34w;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 496172
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 496173
    invoke-static {p3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 496174
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 496175
    const-string v2, "for_sale_item_id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 496176
    :cond_0
    const-string v1, "num"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "tracking"

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 496177
    :cond_1
    return-void
.end method
