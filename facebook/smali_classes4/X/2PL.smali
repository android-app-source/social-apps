.class public LX/2PL;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "LX/2PD;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2PL;


# instance fields
.field private final c:LX/2PJ;

.field private final d:LX/0SF;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406370
    const-class v0, LX/2PL;

    sput-object v0, LX/2PL;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406319
    const/16 v0, 0x12

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 406320
    iput-object p1, p0, LX/2PL;->c:LX/2PJ;

    .line 406321
    iput-object p2, p0, LX/2PL;->d:LX/0SF;

    .line 406322
    iput-object p3, p0, LX/2PL;->e:LX/0Or;

    .line 406323
    return-void
.end method

.method public static a(LX/0QB;)LX/2PL;
    .locals 6

    .prologue
    .line 406357
    sget-object v0, LX/2PL;->f:LX/2PL;

    if-nez v0, :cond_1

    .line 406358
    const-class v1, LX/2PL;

    monitor-enter v1

    .line 406359
    :try_start_0
    sget-object v0, LX/2PL;->f:LX/2PL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406360
    if-eqz v2, :cond_0

    .line 406361
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406362
    new-instance v5, LX/2PL;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2PL;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 406363
    move-object v0, v5

    .line 406364
    sput-object v0, LX/2PL;->f:LX/2PL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406365
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406366
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406367
    :cond_1
    sget-object v0, LX/2PL;->f:LX/2PL;

    return-object v0

    .line 406368
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b([B)V
    .locals 2

    .prologue
    .line 406354
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PD;

    .line 406355
    invoke-interface {v0, p1}, LX/2PD;->a([B)V

    goto :goto_0

    .line 406356
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 406353
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 8

    .prologue
    .line 406337
    if-nez p1, :cond_1

    .line 406338
    sget-object v0, LX/2PL;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise batch lookup response"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406339
    :cond_0
    :goto_0
    return-void

    .line 406340
    :cond_1
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 406341
    :cond_2
    sget-object v0, LX/2PL;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise BatchLookupResponse"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406342
    iget-object v0, p1, LX/Dph;->nonce:[B

    invoke-direct {p0, v0}, LX/2PL;->b([B)V

    goto :goto_0

    .line 406343
    :cond_3
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    .line 406344
    iget-object v0, p1, LX/Dph;->nonce:[B

    invoke-direct {p0, v0}, LX/2PL;->b([B)V

    goto :goto_0

    .line 406345
    :cond_4
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0}, LX/Dpi;->g()LX/DpC;

    move-result-object v0

    .line 406346
    iget-object v0, v0, LX/DpC;->lookup_results:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 406347
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 406348
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpK;

    .line 406349
    iget-object v1, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2PD;

    .line 406350
    iget-object v5, p1, LX/Dph;->nonce:[B

    iget-object v6, p1, LX/Dph;->date_micros:Ljava/lang/Long;

    const/4 v7, 0x1

    invoke-interface {v1, v5, v6, v0, v7}, LX/2PD;->a([BLjava/lang/Long;LX/DpK;Z)V

    goto :goto_1

    .line 406351
    :cond_7
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PD;

    .line 406352
    iget-object v2, p1, LX/Dph;->nonce:[B

    invoke-interface {v0, v2}, LX/2PD;->b([B)V

    goto :goto_2
.end method

.method public final declared-synchronized a([BLjava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 406324
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 406325
    sget-object v0, LX/2PL;->b:Ljava/lang/Class;

    const-string v1, "Stored procedure sender not available for batch lookup"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406326
    const/4 v0, 0x0

    .line 406327
    :goto_0
    monitor-exit p0

    return v0

    .line 406328
    :cond_0
    :try_start_1
    new-instance v7, LX/DpB;

    invoke-direct {v7, p2}, LX/DpB;-><init>(Ljava/util/List;)V

    .line 406329
    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, LX/DpM;

    iget-object v0, p0, LX/2PL;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, p0, LX/2PL;->c:LX/2PJ;

    invoke-virtual {v4}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v0, p0, LX/2PL;->d:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    const/16 v6, 0x15

    .line 406330
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 406331
    invoke-static {v0, v7}, LX/DpO;->b(LX/DpO;LX/DpB;)V

    .line 406332
    move-object v7, v0

    .line 406333
    move-object v8, p1

    invoke-static/range {v1 .. v8}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v0

    .line 406334
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2PI;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406335
    const/4 v0, 0x1

    goto :goto_0

    .line 406336
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
