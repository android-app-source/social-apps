.class public LX/2eL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/2eT;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2eK;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Rb;

.field public e:LX/1PW;


# direct methods
.method public constructor <init>(LX/2eK;LX/0Ot;)V
    .locals 3
    .param p1    # LX/2eK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2eK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2eL;->a:Ljava/util/ArrayList;

    .line 445181
    iput-object p1, p0, LX/2eL;->b:LX/2eK;

    .line 445182
    iput-object p2, p0, LX/2eL;->c:LX/0Ot;

    .line 445183
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/2eL;->b:LX/2eK;

    invoke-virtual {v1}, LX/2eK;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 445184
    iget-object v1, p0, LX/2eL;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445186
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/2eT;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 445187
    iget-object v0, p0, LX/2eL;->e:LX/1PW;

    const-string v1, "Environment must be set in advance."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445188
    iget-object v0, p0, LX/2eL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/2eL;->b:LX/2eK;

    invoke-virtual {v0}, LX/2eK;->e()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 445189
    :cond_0
    const/4 v0, 0x0

    .line 445190
    :cond_1
    :goto_0
    return-object v0

    .line 445191
    :cond_2
    iget-object v0, p0, LX/2eL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2eT;

    .line 445192
    if-nez v0, :cond_1

    .line 445193
    iget-object v0, p0, LX/2eL;->b:LX/2eK;

    .line 445194
    iget-object v2, v0, LX/2eK;->a:LX/2eJ;

    iget-object v1, v0, LX/2eK;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v1}, LX/2eJ;->a(I)LX/1RC;

    move-result-object v2

    .line 445195
    new-instance v3, LX/2eQ;

    invoke-direct {v3, v0, v2}, LX/2eQ;-><init>(LX/2eK;LX/1RC;)V

    iget-object v4, v0, LX/2eK;->a:LX/2eJ;

    iget-object v1, v0, LX/2eK;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v4, v1}, LX/2eJ;->b(I)Ljava/lang/Object;

    move-result-object v1

    .line 445196
    new-instance v4, LX/2eT;

    new-instance v0, LX/1Rc;

    invoke-direct {v0, v1, v2}, LX/1Rc;-><init>(Ljava/lang/Object;LX/1Nt;)V

    invoke-direct {v4, v3, v0}, LX/2eT;-><init>(LX/2eR;LX/1Ra;)V

    move-object v1, v4

    .line 445197
    move-object v0, v1

    .line 445198
    iget-object v1, p0, LX/2eL;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;)V
    .locals 3

    .prologue
    .line 445199
    const-string v0, "PageItems.bindPageItem"

    const v1, 0x33f0e9a8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445200
    :try_start_0
    invoke-virtual {p0, p1}, LX/2eL;->a(I)LX/2eT;

    move-result-object v0

    .line 445201
    if-eqz v0, :cond_0

    .line 445202
    iget-object v1, p0, LX/2eL;->d:LX/1Rb;

    iget-object v2, p0, LX/2eL;->e:LX/1PW;

    invoke-virtual {v0, v1, v2, p2}, LX/2eT;->a(LX/1Rb;LX/1PW;Landroid/view/View;)V

    .line 445203
    iget-object v1, v0, LX/2eT;->b:LX/1Ra;

    move-object v1, v1

    .line 445204
    iget-object v0, p0, LX/2eL;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p2, v1, v0}, LX/1aL;->a(Landroid/view/View;LX/1Ra;LX/03V;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445205
    :cond_0
    const v0, 0x57c08252

    invoke-static {v0}, LX/02m;->a(I)V

    .line 445206
    return-void

    .line 445207
    :catchall_0
    move-exception v0

    const v1, 0x1cce6451

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 445208
    iget-object v0, p0, LX/2eL;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 445209
    const-string v0, "PageItems.preparePageItem"

    const v1, -0x2d3d18c7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 445210
    :try_start_0
    invoke-virtual {p0, p1}, LX/2eL;->a(I)LX/2eT;

    move-result-object v0

    .line 445211
    if-eqz v0, :cond_0

    .line 445212
    iget-object v1, p0, LX/2eL;->d:LX/1Rb;

    iget-object v2, p0, LX/2eL;->e:LX/1PW;

    invoke-virtual {v0, v1, v2}, LX/2eT;->a(LX/1Rb;LX/1PW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445213
    :cond_0
    const v0, -0x6b0a057c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 445214
    return-void

    .line 445215
    :catchall_0
    move-exception v0

    const v1, -0x5d71bdfe

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
