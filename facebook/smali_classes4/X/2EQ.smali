.class public final enum LX/2EQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2EQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2EQ;

.field public static final enum CAPTIVE_PORTAL:LX/2EQ;

.field public static final enum NOT_CAPTIVE_PORTAL:LX/2EQ;

.field public static final enum NOT_CHECKED:LX/2EQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 385415
    new-instance v0, LX/2EQ;

    const-string v1, "NOT_CAPTIVE_PORTAL"

    invoke-direct {v0, v1, v2}, LX/2EQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2EQ;->NOT_CAPTIVE_PORTAL:LX/2EQ;

    .line 385416
    new-instance v0, LX/2EQ;

    const-string v1, "CAPTIVE_PORTAL"

    invoke-direct {v0, v1, v3}, LX/2EQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    .line 385417
    new-instance v0, LX/2EQ;

    const-string v1, "NOT_CHECKED"

    invoke-direct {v0, v1, v4}, LX/2EQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2EQ;->NOT_CHECKED:LX/2EQ;

    .line 385418
    const/4 v0, 0x3

    new-array v0, v0, [LX/2EQ;

    sget-object v1, LX/2EQ;->NOT_CAPTIVE_PORTAL:LX/2EQ;

    aput-object v1, v0, v2

    sget-object v1, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    aput-object v1, v0, v3

    sget-object v1, LX/2EQ;->NOT_CHECKED:LX/2EQ;

    aput-object v1, v0, v4

    sput-object v0, LX/2EQ;->$VALUES:[LX/2EQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 385419
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2EQ;
    .locals 1

    .prologue
    .line 385420
    const-class v0, LX/2EQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2EQ;

    return-object v0
.end method

.method public static values()[LX/2EQ;
    .locals 1

    .prologue
    .line 385421
    sget-object v0, LX/2EQ;->$VALUES:[LX/2EQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2EQ;

    return-object v0
.end method
