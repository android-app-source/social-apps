.class public final LX/2A7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 376945
    new-instance v0, LX/0U1;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->a:LX/0U1;

    .line 376946
    new-instance v0, LX/0U1;

    const-string v1, "notif_id"

    const-string v2, "TEXT UNIQUE ON CONFLICT REPLACE"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->b:LX/0U1;

    .line 376947
    new-instance v0, LX/0U1;

    const-string v1, "recipient_id"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->c:LX/0U1;

    .line 376948
    new-instance v0, LX/0U1;

    const-string v1, "seen_state"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->d:LX/0U1;

    .line 376949
    new-instance v0, LX/0U1;

    const-string v1, "updated"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->e:LX/0U1;

    .line 376950
    new-instance v0, LX/0U1;

    const-string v1, "cache_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->f:LX/0U1;

    .line 376951
    new-instance v0, LX/0U1;

    const-string v1, "cursor"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->g:LX/0U1;

    .line 376952
    new-instance v0, LX/0U1;

    const-string v1, "gql_payload"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->h:LX/0U1;

    .line 376953
    new-instance v0, LX/0U1;

    const-string v1, "profile_picture_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->i:LX/0U1;

    .line 376954
    new-instance v0, LX/0U1;

    const-string v1, "icon_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->j:LX/0U1;

    .line 376955
    new-instance v0, LX/0U1;

    const-string v1, "summary_graphql_text_with_entities"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->k:LX/0U1;

    .line 376956
    new-instance v0, LX/0U1;

    const-string v1, "short_summary_graphql_text_with_entities"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->l:LX/0U1;

    .line 376957
    new-instance v0, LX/0U1;

    const-string v1, "notif_option_row"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->m:LX/0U1;

    .line 376958
    new-instance v0, LX/0U1;

    const-string v1, "highlight_state"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->n:LX/0U1;

    .line 376959
    new-instance v0, LX/0U1;

    const-string v1, "importance_reason_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->o:LX/0U1;

    .line 376960
    new-instance v0, LX/0U1;

    const-string v1, "importance_score"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->p:LX/0U1;

    .line 376961
    new-instance v0, LX/0U1;

    const-string v1, "importance_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->q:LX/0U1;

    .line 376962
    new-instance v0, LX/0U1;

    const-string v1, "reaction_unit"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->r:LX/0U1;

    .line 376963
    new-instance v0, LX/0U1;

    const-string v1, "is_rich_notif_collapsed"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->s:LX/0U1;

    .line 376964
    new-instance v0, LX/0U1;

    const-string v1, "notif_option_sets"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->t:LX/0U1;

    .line 376965
    new-instance v0, LX/0U1;

    const-string v1, "seen_state_session_number"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->u:LX/0U1;

    .line 376966
    new-instance v0, LX/0U1;

    const-string v1, "highlight_operations"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->v:LX/0U1;

    .line 376967
    new-instance v0, LX/0U1;

    const-string v1, "num_impressions"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->w:LX/0U1;

    .line 376968
    new-instance v0, LX/0U1;

    const-string v1, "eligible_buckets"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->x:LX/0U1;

    .line 376969
    new-instance v0, LX/0U1;

    const-string v1, "is_local"

    const-string v2, "INT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2A7;->y:LX/0U1;

    return-void
.end method
