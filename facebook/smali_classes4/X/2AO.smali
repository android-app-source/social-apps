.class public final LX/2AO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 0

    .prologue
    .line 377290
    iput-object p1, p0, LX/2AO;->a:LX/2CH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x691a69e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 377291
    iget-object v1, p0, LX/2AO;->a:LX/2CH;

    .line 377292
    const-string v2, "extra_message"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/Message;

    .line 377293
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 377294
    iget-object p0, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 377295
    iget-object v2, v1, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3hU;

    .line 377296
    if-eqz v2, :cond_0

    iget-boolean p1, v2, LX/3hU;->a:Z

    if-eqz p1, :cond_0

    .line 377297
    const/4 p1, 0x0

    iput-boolean p1, v2, LX/3hU;->a:Z

    .line 377298
    invoke-static {v1, p0}, LX/2CH;->h(LX/2CH;Lcom/facebook/user/model/UserKey;)V

    .line 377299
    :cond_0
    const/16 v1, 0x27

    const v2, 0x819847d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
