.class public LX/2DL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2DL;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384055
    iput-object p1, p0, LX/2DL;->a:LX/0SG;

    .line 384056
    iput-object p2, p0, LX/2DL;->b:LX/0Ot;

    .line 384057
    return-void
.end method

.method public static a(LX/0QB;)LX/2DL;
    .locals 5

    .prologue
    .line 384058
    sget-object v0, LX/2DL;->c:LX/2DL;

    if-nez v0, :cond_1

    .line 384059
    const-class v1, LX/2DL;

    monitor-enter v1

    .line 384060
    :try_start_0
    sget-object v0, LX/2DL;->c:LX/2DL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384061
    if-eqz v2, :cond_0

    .line 384062
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384063
    new-instance v4, LX/2DL;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 p0, 0x8a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2DL;-><init>(LX/0SG;LX/0Ot;)V

    .line 384064
    move-object v0, v4

    .line 384065
    sput-object v0, LX/2DL;->c:LX/2DL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384066
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384067
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384068
    :cond_1
    sget-object v0, LX/2DL;->c:LX/2DL;

    return-object v0

    .line 384069
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/analytics/HoneyAnalyticsEvent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 384071
    iget-object v0, p0, LX/2DL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CZ;

    iget-object v1, p0, LX/2DL;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 384072
    iget-object v1, v0, LX/2CZ;->c:LX/0Uh;

    const/16 v4, 0x134

    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 384073
    if-nez v1, :cond_0

    iget-boolean v4, v0, LX/2CZ;->j:Z

    if-nez v4, :cond_1

    :cond_0
    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2CZ;->a:LX/0kd;

    invoke-virtual {v1}, LX/0kd;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 384074
    :cond_1
    iget-object v1, v0, LX/2CZ;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, LX/2CZ;->a(LX/2CZ;JLjava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 384075
    :goto_0
    move-object v0, v1

    .line 384076
    return-object v0

    :cond_2
    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method
