.class public LX/2GB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2EM;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2GB;


# instance fields
.field public final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:LX/0SG;

.field private final c:LX/0kb;

.field private final d:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/42d;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0kb;LX/0SG;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadWakeup;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387886
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    .line 387887
    iput-object p1, p0, LX/2GB;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 387888
    iput-object p2, p0, LX/2GB;->c:LX/0kb;

    .line 387889
    iput-object p3, p0, LX/2GB;->b:LX/0SG;

    .line 387890
    return-void
.end method

.method public static a(LX/0QB;)LX/2GB;
    .locals 6

    .prologue
    .line 387900
    sget-object v0, LX/2GB;->f:LX/2GB;

    if-nez v0, :cond_1

    .line 387901
    const-class v1, LX/2GB;

    monitor-enter v1

    .line 387902
    :try_start_0
    sget-object v0, LX/2GB;->f:LX/2GB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387903
    if-eqz v2, :cond_0

    .line 387904
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387905
    new-instance p0, LX/2GB;

    invoke-static {v0}, LX/2a1;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/2GB;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0kb;LX/0SG;)V

    .line 387906
    move-object v0, p0

    .line 387907
    sput-object v0, LX/2GB;->f:LX/2GB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387908
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387909
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387910
    :cond_1
    sget-object v0, LX/2GB;->f:LX/2GB;

    return-object v0

    .line 387911
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 387930
    iget-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 387931
    iget-object v0, p0, LX/2GB;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/backgroundtasks/RadioBasedBackgroundTaskRunner$1;

    invoke-direct {v1, p0}, Lcom/facebook/backgroundtasks/RadioBasedBackgroundTaskRunner$1;-><init>(LX/2GB;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 387932
    return-void

    .line 387933
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/2GB;Ljava/lang/Runnable;LX/0am;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387913
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387914
    iget-object v0, p0, LX/2GB;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 387915
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 387916
    iget-object v0, p0, LX/2GB;->a:Ljava/util/concurrent/ScheduledExecutorService;

    const v1, -0x3dbda406

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387917
    :goto_0
    monitor-exit p0

    return-void

    .line 387918
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2GB;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 387919
    iget-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 387920
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387921
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/2GB;->a(J)V

    .line 387922
    :cond_1
    :goto_1
    iget-object v1, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    new-instance v4, LX/42d;

    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p2

    :cond_2
    invoke-direct {v4, p0, p1, p2}, LX/42d;-><init>(LX/2GB;Ljava/lang/Runnable;LX/0am;)V

    invoke-virtual {v1, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 387923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 387924
    :cond_3
    :try_start_2
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387925
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 387926
    iget-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42d;

    iget-object v0, v0, LX/42d;->b:LX/0am;

    .line 387927
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    add-long v6, v4, v2

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v6, v0

    if-gez v0, :cond_1

    .line 387928
    :cond_4
    invoke-direct {p0}, LX/2GB;->c()V

    .line 387929
    invoke-direct {p0, v4, v5}, LX/2GB;->a(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 387891
    iget-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 387892
    iget-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 387893
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 387894
    return-void
.end method

.method public static declared-synchronized d(LX/2GB;)V
    .locals 1

    .prologue
    .line 387895
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387896
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GB;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 387897
    invoke-static {p0}, LX/2GB;->e(LX/2GB;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387898
    monitor-exit p0

    return-void

    .line 387899
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized e(LX/2GB;)V
    .locals 3

    .prologue
    .line 387868
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387869
    iget-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42d;

    .line 387870
    iget-object v1, p0, LX/2GB;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v0, v0, LX/42d;->a:Ljava/lang/Runnable;

    const v2, -0x775e40c9

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 387871
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 387872
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 387873
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GB;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387874
    invoke-direct {p0}, LX/2GB;->c()V

    .line 387875
    invoke-static {p0}, LX/2GB;->e(LX/2GB;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387876
    :cond_0
    monitor-exit p0

    return-void

    .line 387877
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 387878
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/2GB;->a(LX/2GB;Ljava/lang/Runnable;LX/0am;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387879
    monitor-exit p0

    return-void

    .line 387880
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 387881
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/2GB;->a(LX/2GB;Ljava/lang/Runnable;LX/0am;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387882
    monitor-exit p0

    return-void

    .line 387883
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 0

    .prologue
    .line 387884
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
