.class public LX/2qw;
.super LX/3zn;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2qw;


# instance fields
.field private final a:LX/3zp;

.field private final b:LX/3zq;

.field private final c:LX/3zs;

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3zq;LX/3zp;LX/3zs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 471522
    invoke-direct {p0}, LX/3zn;-><init>()V

    .line 471523
    iput-object p2, p0, LX/2qw;->a:LX/3zp;

    .line 471524
    iput-object p1, p0, LX/2qw;->b:LX/3zq;

    .line 471525
    iput-object p3, p0, LX/2qw;->c:LX/3zs;

    .line 471526
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/2qw;->d:Landroid/util/SparseArray;

    .line 471527
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2qw;->e:Ljava/util/Set;

    .line 471528
    return-void
.end method

.method public static a(LX/0QB;)LX/2qw;
    .locals 6

    .prologue
    .line 471529
    sget-object v0, LX/2qw;->f:LX/2qw;

    if-nez v0, :cond_1

    .line 471530
    const-class v1, LX/2qw;

    monitor-enter v1

    .line 471531
    :try_start_0
    sget-object v0, LX/2qw;->f:LX/2qw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 471532
    if-eqz v2, :cond_0

    .line 471533
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 471534
    new-instance p0, LX/2qw;

    invoke-static {v0}, LX/3zq;->a(LX/0QB;)LX/3zq;

    move-result-object v3

    check-cast v3, LX/3zq;

    invoke-static {v0}, LX/3zp;->a(LX/0QB;)LX/3zp;

    move-result-object v4

    check-cast v4, LX/3zp;

    invoke-static {v0}, LX/3zs;->a(LX/0QB;)LX/3zs;

    move-result-object v5

    check-cast v5, LX/3zs;

    invoke-direct {p0, v3, v4, v5}, LX/2qw;-><init>(LX/3zq;LX/3zp;LX/3zs;)V

    .line 471535
    move-object v0, p0

    .line 471536
    sput-object v0, LX/2qw;->f:LX/2qw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471537
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 471538
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 471539
    :cond_1
    sget-object v0, LX/2qw;->f:LX/2qw;

    return-object v0

    .line 471540
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 471541
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x310e00f0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 471542
    iget-object v0, p0, LX/2qw;->c:LX/3zs;

    .line 471543
    iget-object v2, v0, LX/3zs;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v2, v2

    .line 471544
    iget-object v0, p0, LX/2qw;->d:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 471545
    if-eqz v0, :cond_0

    .line 471546
    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 471547
    :goto_0
    iget-object v0, p0, LX/2qw;->b:LX/3zq;

    sget-object v3, LX/3zr;->BUTTON_CLICK:LX/3zr;

    .line 471548
    new-instance p0, Lcom/facebook/analytics/useractions/utils/UserActionEvent;

    invoke-direct {p0, v3, v2}, Lcom/facebook/analytics/useractions/utils/UserActionEvent;-><init>(LX/3zr;Ljava/lang/String;)V

    invoke-static {v0, p0}, LX/3zq;->a(LX/3zq;Lcom/facebook/analytics/useractions/utils/UserActionEvent;)V

    .line 471549
    const v0, -0x466cab03

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 471550
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    goto :goto_0
.end method
