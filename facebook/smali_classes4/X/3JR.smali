.class public final LX/3JR;
.super LX/3JO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3JO",
        "<",
        "LX/3Jf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547715
    invoke-direct {p0}, LX/3JO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/util/JsonReader;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 547700
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 547701
    new-instance v1, LX/3JS;

    invoke-direct {v1}, LX/3JS;-><init>()V

    .line 547702
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547703
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 547704
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 547705
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 547706
    :sswitch_0
    const-string v3, "property"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "key_values"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "timing_curves"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "anchor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    .line 547707
    :pswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3JT;->valueOf(Ljava/lang/String;)LX/3JT;

    move-result-object v0

    iput-object v0, v1, LX/3JS;->a:LX/3JT;

    goto :goto_0

    .line 547708
    :pswitch_1
    sget-object v0, LX/3JU;->a:LX/3JO;

    invoke-virtual {v0, p1}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, LX/3JS;->b:Ljava/util/List;

    goto :goto_0

    .line 547709
    :pswitch_2
    invoke-static {p1}, LX/3JX;->b(Landroid/util/JsonReader;)[[[F

    move-result-object v0

    iput-object v0, v1, LX/3JS;->c:[[[F

    goto :goto_0

    .line 547710
    :pswitch_3
    invoke-static {p1}, LX/3JX;->a(Landroid/util/JsonReader;)[F

    move-result-object v0

    iput-object v0, v1, LX/3JS;->d:[F

    goto :goto_0

    .line 547711
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 547712
    new-instance v0, LX/3Jf;

    iget-object v2, v1, LX/3JS;->a:LX/3JT;

    iget-object v3, v1, LX/3JS;->b:Ljava/util/List;

    iget-object p0, v1, LX/3JS;->c:[[[F

    iget-object p1, v1, LX/3JS;->d:[F

    invoke-direct {v0, v2, v3, p0, p1}, LX/3Jf;-><init>(LX/3JT;Ljava/util/List;[[[F[F)V

    move-object v0, v0

    .line 547713
    move-object v0, v0

    .line 547714
    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x5b3ddd07 -> :sswitch_2
        -0x543d3d4b -> :sswitch_3
        -0x3b32222b -> :sswitch_0
        0x2375e302 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
