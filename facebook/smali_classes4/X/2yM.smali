.class public final LX/2yM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2yN",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2yJ;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2yJ;)V
    .locals 0

    .prologue
    .line 481693
    iput-object p1, p0, LX/2yM;->a:LX/2yJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481694
    return-void
.end method

.method public constructor <init>(LX/2yJ;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 481690
    iput-object p1, p0, LX/2yM;->a:LX/2yJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481691
    iput-object p2, p0, LX/2yM;->b:Ljava/util/Map;

    .line 481692
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 481662
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481663
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481664
    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481665
    instance-of v0, p2, LX/C8i;

    if-eqz v0, :cond_0

    .line 481666
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 481667
    iget-object v0, p0, LX/2yM;->a:LX/2yJ;

    iget-object v0, v0, LX/2yJ;->b:LX/17V;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const-string v4, "native_newsfeed"

    check-cast p2, LX/C8i;

    invoke-interface {p2}, LX/C8i;->a()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 481668
    :goto_0
    return-object v0

    .line 481669
    :cond_0
    invoke-static {p1}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 481670
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 481671
    :goto_1
    invoke-static {v1}, LX/2mt;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481672
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    const-string v1, "open_graph"

    .line 481673
    invoke-static {p1}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 481674
    invoke-static {v3}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_5

    .line 481675
    :cond_1
    const/4 v4, 0x0

    .line 481676
    :goto_2
    move-object v0, v4

    .line 481677
    goto :goto_0

    .line 481678
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 481679
    :cond_3
    iget-object v0, p0, LX/2yM;->a:LX/2yJ;

    iget-object v0, v0, LX/2yJ;->b:LX/17V;

    iget-object v1, p0, LX/2yM;->a:LX/2yJ;

    iget-object v1, v1, LX/2yJ;->c:LX/2mt;

    invoke-virtual {v1, p1}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 481680
    invoke-static {p1}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    const-string v4, "native_newsfeed"

    iget-object v5, p0, LX/2yM;->b:Ljava/util/Map;

    .line 481681
    invoke-virtual {v0, v1, v2, v3, v4}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 481682
    if-eqz v5, :cond_4

    .line 481683
    invoke-virtual {p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 481684
    :cond_4
    move-object v0, p0

    .line 481685
    goto :goto_0

    .line 481686
    :cond_5
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "open_open_graph_object"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "tracking"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "unit_type"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 481687
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 481688
    move-object v4, v4

    .line 481689
    goto :goto_2
.end method
