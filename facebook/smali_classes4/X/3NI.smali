.class public LX/3NI;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3NF;


# direct methods
.method public constructor <init>(LX/0Zr;LX/3NF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558958
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558959
    iput-object p2, p0, LX/3NI;->c:LX/3NF;

    .line 558960
    return-void
.end method

.method public static a(LX/0QB;)LX/3NI;
    .locals 1

    .prologue
    .line 558961
    invoke-static {p0}, LX/3NI;->b(LX/0QB;)LX/3NI;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3NI;
    .locals 3

    .prologue
    .line 558962
    new-instance v2, LX/3NI;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {p0}, LX/3NF;->b(LX/0QB;)LX/3NF;

    move-result-object v1

    check-cast v1, LX/3NF;

    invoke-direct {v2, v0, v1}, LX/3NI;-><init>(LX/0Zr;LX/3NF;)V

    .line 558963
    return-object v2
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 558964
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 558965
    :goto_0
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 558966
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558967
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 558968
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I

    move-object v0, v1

    .line 558969
    :goto_1
    return-object v0

    .line 558970
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 558971
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "M"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 558972
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 558973
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 558974
    iput v5, v1, LX/39y;->b:I

    move-object v0, v1

    .line 558975
    goto :goto_1

    .line 558976
    :cond_2
    const/4 v2, 0x0

    .line 558977
    :try_start_0
    iget-object v0, p0, LX/3NI;->c:LX/3NF;

    invoke-virtual {v0}, LX/3NF;->a()Lcom/facebook/user/model/User;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 558978
    :goto_2
    if-eqz v0, :cond_4

    .line 558979
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 558980
    iget-object v3, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v3, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    .line 558981
    if-eqz v0, :cond_3

    .line 558982
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558983
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 558984
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iput v2, v1, LX/39y;->b:I

    .line 558985
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 558986
    goto :goto_1

    .line 558987
    :catch_0
    move-exception v0

    .line 558988
    const-string v3, "ContactPickerServerAgentPageFilter"

    const-string v4, "exception with filtering commerce pages"

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_2

    .line 558989
    :cond_4
    iput v5, v1, LX/39y;->b:I

    .line 558990
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 558991
    goto :goto_1
.end method
