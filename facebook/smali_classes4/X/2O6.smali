.class public LX/2O6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2FC;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/2FC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 400452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2O6;->a:Ljava/util/ArrayList;

    .line 400454
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/2FC;)V
    .locals 1

    .prologue
    .line 400459
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2O6;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400460
    monitor-exit p0

    return-void

    .line 400461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized invalidate()V
    .locals 2

    .prologue
    .line 400455
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2O6;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2FC;

    .line 400456
    invoke-interface {v0}, LX/2FC;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 400457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 400458
    :cond_0
    monitor-exit p0

    return-void
.end method
