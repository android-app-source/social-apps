.class public final enum LX/2rw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2rw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2rw;

.field public static final enum EVENT:LX/2rw;

.field public static final enum FRIENDLIST:LX/2rw;

.field public static final enum FUNDRAISER:LX/2rw;

.field public static final enum GROUP:LX/2rw;

.field public static final enum MARKETPLACE:LX/2rw;

.field public static final enum OTHER:LX/2rw;

.field public static final enum PAGE:LX/2rw;

.field public static final enum PAGE_RECOMMENDATION:LX/2rw;

.field public static final enum UNDIRECTED:LX/2rw;

.field public static final enum USER:LX/2rw;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 472160
    new-instance v0, LX/2rw;

    const-string v1, "OTHER"

    const-string v2, "other"

    invoke-direct {v0, v1, v4, v2}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->OTHER:LX/2rw;

    .line 472161
    new-instance v0, LX/2rw;

    const-string v1, "UNDIRECTED"

    const-string v2, "feed"

    invoke-direct {v0, v1, v5, v2}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    .line 472162
    new-instance v0, LX/2rw;

    const-string v1, "USER"

    const-string v2, "wall"

    invoke-direct {v0, v1, v6, v2}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->USER:LX/2rw;

    .line 472163
    new-instance v0, LX/2rw;

    const-string v1, "GROUP"

    const-string v2, "group"

    invoke-direct {v0, v1, v7, v2}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->GROUP:LX/2rw;

    .line 472164
    new-instance v0, LX/2rw;

    const-string v1, "EVENT"

    const-string v2, "event"

    invoke-direct {v0, v1, v8, v2}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->EVENT:LX/2rw;

    .line 472165
    new-instance v0, LX/2rw;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    const-string v3, "page"

    invoke-direct {v0, v1, v2, v3}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->PAGE:LX/2rw;

    .line 472166
    new-instance v0, LX/2rw;

    const-string v1, "FRIENDLIST"

    const/4 v2, 0x6

    const-string v3, "friendlist"

    invoke-direct {v0, v1, v2, v3}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->FRIENDLIST:LX/2rw;

    .line 472167
    new-instance v0, LX/2rw;

    const-string v1, "PAGE_RECOMMENDATION"

    const/4 v2, 0x7

    const-string v3, "recommendation"

    invoke-direct {v0, v1, v2, v3}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->PAGE_RECOMMENDATION:LX/2rw;

    .line 472168
    new-instance v0, LX/2rw;

    const-string v1, "MARKETPLACE"

    const/16 v2, 0x8

    const-string v3, "marketplace"

    invoke-direct {v0, v1, v2, v3}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->MARKETPLACE:LX/2rw;

    .line 472169
    new-instance v0, LX/2rw;

    const-string v1, "FUNDRAISER"

    const/16 v2, 0x9

    const-string v3, "fundraiser"

    invoke-direct {v0, v1, v2, v3}, LX/2rw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2rw;->FUNDRAISER:LX/2rw;

    .line 472170
    const/16 v0, 0xa

    new-array v0, v0, [LX/2rw;

    sget-object v1, LX/2rw;->OTHER:LX/2rw;

    aput-object v1, v0, v4

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    aput-object v1, v0, v5

    sget-object v1, LX/2rw;->USER:LX/2rw;

    aput-object v1, v0, v6

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    aput-object v1, v0, v7

    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2rw;->PAGE:LX/2rw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2rw;->FRIENDLIST:LX/2rw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2rw;->PAGE_RECOMMENDATION:LX/2rw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2rw;->MARKETPLACE:LX/2rw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2rw;->FUNDRAISER:LX/2rw;

    aput-object v2, v0, v1

    sput-object v0, LX/2rw;->$VALUES:[LX/2rw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 472186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 472187
    iput-object p3, p0, LX/2rw;->analyticsName:Ljava/lang/String;

    .line 472188
    return-void
.end method

.method public static convertToObjectType(LX/2rw;)I
    .locals 2

    .prologue
    .line 472180
    sget-object v0, LX/5Rp;->a:[I

    invoke-virtual {p0}, LX/2rw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 472181
    const v0, 0x285feb

    :goto_0
    return v0

    .line 472182
    :pswitch_0
    const v0, 0x403827a

    goto :goto_0

    .line 472183
    :pswitch_1
    const v0, 0x41e065f

    goto :goto_0

    .line 472184
    :pswitch_2
    const v0, 0x25d6af

    goto :goto_0

    .line 472185
    :pswitch_3
    const v0, -0x4e6785e3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static fromString(Ljava/lang/String;)LX/2rw;
    .locals 6

    .prologue
    .line 472174
    invoke-static {p0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 472175
    invoke-static {}, LX/2rw;->values()[LX/2rw;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 472176
    iget-object v5, v4, LX/2rw;->analyticsName:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 472177
    return-object v4

    .line 472178
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 472179
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown TargetType value: \'%s\'"

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static fromString(Ljava/lang/String;LX/2rw;)LX/2rw;
    .locals 1

    .prologue
    .line 472189
    :try_start_0
    invoke-static {p0}, LX/2rw;->fromString(Ljava/lang/String;)LX/2rw;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 472190
    :goto_0
    return-object p1

    :catch_0
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2rw;
    .locals 1

    .prologue
    .line 472173
    const-class v0, LX/2rw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2rw;

    return-object v0
.end method

.method public static values()[LX/2rw;
    .locals 1

    .prologue
    .line 472172
    sget-object v0, LX/2rw;->$VALUES:[LX/2rw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2rw;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472171
    iget-object v0, p0, LX/2rw;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
