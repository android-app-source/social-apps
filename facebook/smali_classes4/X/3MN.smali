.class public final LX/3MN;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0sv;

.field private static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 554378
    new-instance v0, LX/0U1;

    const-string v1, "phoneNumber"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MN;->a:LX/0U1;

    .line 554379
    new-instance v0, LX/0U1;

    const-string v1, "ts"

    const-string v2, "INTEGER DEFAULT 0"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3MN;->b:LX/0U1;

    .line 554380
    sget-object v0, LX/3MN;->a:LX/0U1;

    sget-object v1, LX/3MN;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MN;->c:LX/0Px;

    .line 554381
    new-instance v0, LX/0su;

    sget-object v1, LX/3MN;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    .line 554382
    sput-object v0, LX/3MN;->d:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3MN;->e:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 554383
    const-string v0, "block_table"

    sget-object v1, LX/3MN;->c:LX/0Px;

    sget-object v2, LX/3MN;->e:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 554384
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 554385
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 554386
    const-string v0, "block_table"

    const-string v1, "INDEX_PHONE_NUMBER"

    sget-object v2, LX/3MN;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0xdc6ab49

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x13472ca2

    invoke-static {v0}, LX/03h;->a(I)V

    .line 554387
    return-void
.end method
