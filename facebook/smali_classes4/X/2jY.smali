.class public LX/2jY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/Runnable;

.field private B:Z

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public final c:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/9qT;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Long;

.field public g:J

.field public h:J

.field public i:J

.field public j:J

.field public k:J

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:J

.field public v:Z

.field public w:Z

.field public x:Landroid/os/Bundle;

.field public y:Lcom/facebook/reaction/ReactionQueryParams;

.field private z:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/CfG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 453847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453848
    iput-object v0, p0, LX/2jY;->e:Ljava/lang/String;

    .line 453849
    iput-object v0, p0, LX/2jY;->f:Ljava/lang/Long;

    .line 453850
    iput-wide v2, p0, LX/2jY;->g:J

    .line 453851
    iput-wide v2, p0, LX/2jY;->h:J

    .line 453852
    iput-wide v2, p0, LX/2jY;->i:J

    .line 453853
    iput-wide v2, p0, LX/2jY;->j:J

    .line 453854
    iput-wide v2, p0, LX/2jY;->k:J

    .line 453855
    iput-boolean v1, p0, LX/2jY;->l:Z

    .line 453856
    iput-object v0, p0, LX/2jY;->m:Ljava/lang/String;

    .line 453857
    iput-boolean v1, p0, LX/2jY;->n:Z

    .line 453858
    iput-boolean v1, p0, LX/2jY;->o:Z

    .line 453859
    iput-boolean v1, p0, LX/2jY;->p:Z

    .line 453860
    iput-boolean v1, p0, LX/2jY;->q:Z

    .line 453861
    iput-object v0, p0, LX/2jY;->s:Ljava/lang/String;

    .line 453862
    iput-object v0, p0, LX/2jY;->t:Ljava/lang/String;

    .line 453863
    iput-wide v2, p0, LX/2jY;->u:J

    .line 453864
    iput-boolean v1, p0, LX/2jY;->v:Z

    .line 453865
    iput-boolean v1, p0, LX/2jY;->w:Z

    .line 453866
    iput-object v0, p0, LX/2jY;->x:Landroid/os/Bundle;

    .line 453867
    iput-object v0, p0, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    .line 453868
    iput-object v0, p0, LX/2jY;->z:Ljava/lang/ref/WeakReference;

    .line 453869
    iput-object v0, p0, LX/2jY;->A:Ljava/lang/Runnable;

    .line 453870
    iput-boolean v1, p0, LX/2jY;->B:Z

    .line 453871
    iput-object p1, p0, LX/2jY;->a:Ljava/lang/String;

    .line 453872
    iput-object p2, p0, LX/2jY;->b:Ljava/lang/String;

    .line 453873
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/2jY;->c:Lcom/google/common/util/concurrent/SettableFuture;

    .line 453874
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    .line 453875
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 453812
    iget-boolean v0, p0, LX/2jY;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2jY;->b:Ljava/lang/String;

    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final E()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 453813
    iput-boolean v0, p0, LX/2jY;->p:Z

    .line 453814
    iput-boolean v0, p0, LX/2jY;->n:Z

    .line 453815
    return-void
.end method

.method public final G()Ljava/lang/Long;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    .line 453816
    iget-object v3, p0, LX/2jY;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/2jY;->f:Ljava/lang/Long;

    if-nez v3, :cond_1

    .line 453817
    :cond_0
    :goto_0
    return-object v0

    .line 453818
    :cond_1
    iget-wide v4, p0, LX/2jY;->k:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_3

    iget-object v3, p0, LX/2jY;->b:Ljava/lang/String;

    .line 453819
    const-string v4, "ANDROID_FEED_CHECKIN_SUGGESTION"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_2
    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 453820
    if-nez v3, :cond_0

    .line 453821
    :cond_3
    iget-object v0, p0, LX/2jY;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 453822
    iget-wide v4, p0, LX/2jY;->k:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 453823
    iget-object v0, p0, LX/2jY;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, LX/2jY;->k:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 453824
    goto :goto_2

    :cond_5
    move v1, v2

    .line 453825
    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 453826
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 453827
    iput-boolean v1, p0, LX/2jY;->o:Z

    .line 453828
    iput-boolean v1, p0, LX/2jY;->v:Z

    .line 453829
    return-void
.end method

.method public final a(LX/9qT;)V
    .locals 2
    .param p1    # LX/9qT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 453830
    if-nez p1, :cond_0

    .line 453831
    iput-boolean v0, p0, LX/2jY;->o:Z

    .line 453832
    :goto_0
    return-void

    .line 453833
    :cond_0
    iget-object v1, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 453834
    invoke-interface {p1}, LX/9qT;->d()LX/0us;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LX/9qT;->d()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LX/9qT;->d()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, LX/2jY;->o:Z

    goto :goto_0
.end method

.method public final a(LX/CfG;)V
    .locals 1

    .prologue
    .line 453835
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2jY;->z:Ljava/lang/ref/WeakReference;

    .line 453836
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 453837
    if-eqz p1, :cond_1

    .line 453838
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 453839
    const-string v1, "com.facebook.STREAM_PUBLISH_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453840
    const-string v1, "extra_target_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, LX/2jY;->u:J

    .line 453841
    :cond_0
    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453842
    const-string v0, "graphql_story"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 453843
    if-eqz v0, :cond_1

    .line 453844
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2jY;->s:Ljava/lang/String;

    .line 453845
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2jY;->t:Ljava/lang/String;

    .line 453846
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V
    .locals 0
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453876
    if-eqz p1, :cond_0

    .line 453877
    iput-object p1, p0, LX/2jY;->r:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 453878
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 453879
    iput-object p1, p0, LX/2jY;->A:Ljava/lang/Runnable;

    .line 453880
    return-void
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453881
    iget-object v0, p0, LX/2jY;->c:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method public final c()LX/8Y9;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 453882
    iget-object v1, p0, LX/2jY;->x:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 453883
    :cond_0
    :goto_0
    return-object v0

    .line 453884
    :cond_1
    iget-object v1, p0, LX/2jY;->x:Landroid/os/Bundle;

    const-string v2, "entry_point"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 453885
    if-ltz v1, :cond_0

    .line 453886
    invoke-static {}, LX/8Y9;->values()[LX/8Y9;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453887
    iget-object v0, p0, LX/2jY;->x:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 453888
    const/4 v0, 0x0

    .line 453889
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2jY;->x:Landroid/os/Bundle;

    const-string v1, "gravity_suggestifier_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453809
    iget-object v0, p0, LX/2jY;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 453810
    iput-boolean p1, p0, LX/2jY;->w:Z

    .line 453811
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453779
    invoke-virtual {p0}, LX/2jY;->h()LX/9qT;

    move-result-object v0

    .line 453780
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9qT;->d()LX/0us;

    move-result-object v1

    if-nez v1, :cond_1

    .line 453781
    :cond_0
    const/4 v0, 0x0

    .line 453782
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0}, LX/9qT;->d()LX/0us;

    move-result-object v0

    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()LX/9qT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453783
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453784
    const/4 v0, 0x0

    .line 453785
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    goto :goto_0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 453786
    iget-wide v0, p0, LX/2jY;->g:J

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 453787
    iget-wide v0, p0, LX/2jY;->h:J

    return-wide v0
.end method

.method public final n()Lcom/facebook/reaction/ReactionQueryParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453788
    iget-object v0, p0, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/9qT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453789
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 453790
    iget-wide v0, p0, LX/2jY;->i:J

    return-wide v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 453808
    iget-wide v0, p0, LX/2jY;->j:J

    return-wide v0
.end method

.method public final r()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 453791
    invoke-virtual {p0}, LX/2jY;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 453792
    iget-wide v4, p0, LX/2jY;->i:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 453793
    :cond_0
    iget-wide v4, p0, LX/2jY;->k:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 453794
    iget-wide v0, p0, LX/2jY;->i:J

    iget-wide v2, p0, LX/2jY;->k:J

    sub-long/2addr v0, v2

    return-wide v0

    :cond_1
    move v0, v2

    .line 453795
    goto :goto_0

    :cond_2
    move v1, v2

    .line 453796
    goto :goto_1
.end method

.method public final t()LX/CfG;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453797
    iget-object v0, p0, LX/2jY;->z:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2jY;->z:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfG;

    goto :goto_0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453798
    iget-object v0, p0, LX/2jY;->x:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2jY;->x:Landroid/os/Bundle;

    const-string v1, "source_entity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .prologue
    .line 453799
    iget-object v0, p0, LX/2jY;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final w()J
    .locals 2

    .prologue
    .line 453800
    iget-wide v0, p0, LX/2jY;->k:J

    return-wide v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 453801
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 453802
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 453803
    :goto_0
    return v0

    .line 453804
    :cond_0
    iget-object v0, p0, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 453805
    invoke-interface {v0}, LX/9qT;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 453806
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 453807
    goto :goto_0
.end method
