.class public LX/3CW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 530222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 530219
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 530220
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 530221
    :cond_0
    return p0
.end method

.method public static a(II)Z
    .locals 1

    .prologue
    .line 530216
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 530217
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 530218
    :cond_0
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 530213
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 530214
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 530215
    :cond_0
    return p0
.end method

.method public static b(II)I
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 530200
    if-ne p0, v0, :cond_0

    .line 530201
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 530202
    :cond_0
    if-le p0, p1, :cond_2

    .line 530203
    const/4 v0, 0x1

    .line 530204
    :cond_1
    :goto_0
    return v0

    .line 530205
    :cond_2
    if-lt p0, p1, :cond_1

    .line 530206
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(II)Z
    .locals 1

    .prologue
    .line 530212
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(I)[Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 530207
    new-array v1, p0, [Ljava/lang/Integer;

    .line 530208
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    .line 530209
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 530210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 530211
    :cond_0
    return-object v1
.end method
