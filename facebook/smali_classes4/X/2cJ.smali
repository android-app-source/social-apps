.class public final LX/2cJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final IGNORED_INFO:LX/2cJ;


# instance fields
.field public final collectionName:Lcom/facebook/omnistore/CollectionName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final subscriptionParams:LX/2cY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final subscriptionState:LX/2cK;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 440643
    new-instance v0, LX/2cJ;

    sget-object v1, LX/2cK;->IGNORED:LX/2cK;

    invoke-direct {v0, v1, v2, v2}, LX/2cJ;-><init>(LX/2cK;Lcom/facebook/omnistore/CollectionName;LX/2cY;)V

    sput-object v0, LX/2cJ;->IGNORED_INFO:LX/2cJ;

    return-void
.end method

.method private constructor <init>(LX/2cK;Lcom/facebook/omnistore/CollectionName;LX/2cY;)V
    .locals 0
    .param p2    # Lcom/facebook/omnistore/CollectionName;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/2cY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440639
    iput-object p1, p0, LX/2cJ;->subscriptionState:LX/2cK;

    .line 440640
    iput-object p2, p0, LX/2cJ;->collectionName:Lcom/facebook/omnistore/CollectionName;

    .line 440641
    iput-object p3, p0, LX/2cJ;->subscriptionParams:LX/2cY;

    .line 440642
    return-void
.end method

.method public static forDeleteExistingSubscription(Lcom/facebook/omnistore/CollectionName;)LX/2cJ;
    .locals 3

    .prologue
    .line 440637
    new-instance v0, LX/2cJ;

    sget-object v1, LX/2cK;->UNSUBSCRIBED:LX/2cK;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, LX/2cJ;-><init>(LX/2cK;Lcom/facebook/omnistore/CollectionName;LX/2cY;)V

    return-object v0
.end method

.method public static forOpenSubscription(Lcom/facebook/omnistore/CollectionName;)LX/2cJ;
    .locals 1

    .prologue
    .line 440635
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;LX/2cY;)LX/2cJ;

    move-result-object v0

    return-object v0
.end method

.method public static forOpenSubscription(Lcom/facebook/omnistore/CollectionName;LX/2cY;)LX/2cJ;
    .locals 2
    .param p1    # LX/2cY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440636
    new-instance v0, LX/2cJ;

    sget-object v1, LX/2cK;->SUBSCRIBED:LX/2cK;

    invoke-direct {v0, v1, p0, p1}, LX/2cJ;-><init>(LX/2cK;Lcom/facebook/omnistore/CollectionName;LX/2cY;)V

    return-object v0
.end method
