.class public LX/27y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/27y;


# instance fields
.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:LX/0Zb;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/31y;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 373345
    const-class v0, LX/27y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/27y;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0Zb;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373347
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/27y;->d:Ljava/util/ArrayList;

    .line 373348
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/27y;->e:Z

    .line 373349
    iput-object p1, p0, LX/27y;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 373350
    iput-object p2, p0, LX/27y;->c:LX/0Zb;

    .line 373351
    return-void
.end method

.method public static a(LX/0QB;)LX/27y;
    .locals 5

    .prologue
    .line 373352
    sget-object v0, LX/27y;->f:LX/27y;

    if-nez v0, :cond_1

    .line 373353
    const-class v1, LX/27y;

    monitor-enter v1

    .line 373354
    :try_start_0
    sget-object v0, LX/27y;->f:LX/27y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 373355
    if-eqz v2, :cond_0

    .line 373356
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 373357
    new-instance p0, LX/27y;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/27y;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0Zb;)V

    .line 373358
    move-object v0, p0

    .line 373359
    sput-object v0, LX/27y;->f:LX/27y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373360
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 373361
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 373362
    :cond_1
    sget-object v0, LX/27y;->f:LX/27y;

    return-object v0

    .line 373363
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 373364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/27y;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 373365
    monitor-enter p0

    .line 373366
    :try_start_0
    iget-object v2, p0, LX/27y;->d:Ljava/util/ArrayList;

    .line 373367
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/27y;->d:Ljava/util/ArrayList;

    .line 373368
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/27y;->e:Z

    .line 373369
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373370
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31y;

    .line 373371
    invoke-static {p0, v0}, LX/27y;->b(LX/27y;LX/31y;)V

    .line 373372
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 373373
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 373374
    :cond_0
    return-void
.end method

.method private static b(LX/27y;LX/31y;)V
    .locals 10

    .prologue
    .line 373375
    instance-of v0, p1, LX/4h7;

    if-eqz v0, :cond_1

    .line 373376
    check-cast p1, LX/4h7;

    .line 373377
    iget-object v0, p0, LX/27y;->c:LX/0Zb;

    const-string v1, "phoneid_sync_stats"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 373378
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373379
    const-string v1, "phone_id"

    .line 373380
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373381
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    const-string v1, "src_pkg"

    .line 373382
    iget-object v2, p1, LX/31y;->b:Ljava/lang/String;

    move-object v2, v2

    .line 373383
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p1}, LX/31y;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "duration"

    invoke-virtual {p1}, LX/31y;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "prev_phone_id"

    .line 373384
    iget-object v2, p1, LX/4h7;->c:LX/282;

    move-object v2, v2

    .line 373385
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    const-string v1, "sync_medium"

    .line 373386
    iget-object v2, p1, LX/4h7;->d:Ljava/lang/String;

    move-object v2, v2

    .line 373387
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 373388
    :cond_0
    :goto_0
    return-void

    .line 373389
    :cond_1
    instance-of v0, p1, LX/4hB;

    if-eqz v0, :cond_3

    .line 373390
    check-cast p1, LX/4hB;

    const/4 v3, 0x0

    .line 373391
    iget-object v2, p0, LX/27y;->c:LX/0Zb;

    const-string v4, "sfdid_sync_stats"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 373392
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 373393
    iget-object v4, p1, LX/4hB;->b:LX/4hA;

    move-object v4, v4

    .line 373394
    iget-object v5, p1, LX/4hB;->c:LX/4hA;

    move-object v5, v5

    .line 373395
    const-string v6, "src_pkg"

    .line 373396
    iget-object v7, p1, LX/31y;->b:Ljava/lang/String;

    move-object v7, v7

    .line 373397
    invoke-virtual {v2, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v6, "status"

    invoke-virtual {p1}, LX/31y;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "creation_time"

    if-nez v4, :cond_4

    move-object v2, v3

    :goto_1
    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v6

    const-string v7, "generator_pkg"

    if-nez v4, :cond_5

    move-object v2, v3

    :goto_2
    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "prev_generator_pkg"

    if-nez v5, :cond_6

    move-object v2, v3

    :goto_3
    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "generator_action"

    if-nez v4, :cond_7

    move-object v2, v3

    :goto_4
    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v4, "prev_generator_action"

    if-nez v5, :cond_8

    :goto_5
    invoke-virtual {v2, v4, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    invoke-virtual {v2}, LX/0oG;->d()V

    .line 373398
    :cond_2
    goto :goto_0

    .line 373399
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported Response type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373400
    :cond_4
    iget-wide v8, v4, LX/4hA;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    :cond_5
    iget-object v2, v4, LX/4hA;->c:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget-object v2, v5, LX/4hA;->c:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-object v2, v4, LX/4hA;->d:Ljava/lang/String;

    goto :goto_4

    :cond_8
    iget-object v3, v5, LX/4hA;->d:Ljava/lang/String;

    goto :goto_5
.end method


# virtual methods
.method public final declared-synchronized a(LX/31y;)V
    .locals 5

    .prologue
    .line 373401
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/27y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373402
    iget-boolean v0, p0, LX/27y;->e:Z

    if-nez v0, :cond_0

    .line 373403
    iget-object v0, p0, LX/27y;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/device_id/FbPhoneIdSyncStatsReporter$StatsReportingRunnable;

    invoke-direct {v1, p0}, Lcom/facebook/device_id/FbPhoneIdSyncStatsReporter$StatsReportingRunnable;-><init>(LX/27y;)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 373404
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/27y;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373405
    :cond_0
    monitor-exit p0

    return-void

    .line 373406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
