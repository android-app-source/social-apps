.class public LX/2Lx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0W9;

.field public final b:Z

.field public final c:LX/00H;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0W9;Ljava/lang/Boolean;LX/00H;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/work/config/community/WorkCommunitySubdomain;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395824
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2Lx;->b:Z

    .line 395825
    iput-object p3, p0, LX/2Lx;->c:LX/00H;

    .line 395826
    iput-object p1, p0, LX/2Lx;->a:LX/0W9;

    .line 395827
    iput-object p4, p0, LX/2Lx;->d:Ljava/lang/String;

    .line 395828
    return-void
.end method

.method public static a(LX/0QB;)LX/2Lx;
    .locals 1

    .prologue
    .line 395822
    invoke-static {p0}, LX/2Lx;->b(LX/0QB;)LX/2Lx;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Lx;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 395821
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Lx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".m.%s/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Lx;
    .locals 5

    .prologue
    .line 395819
    new-instance v4, LX/2Lx;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    const-class v2, LX/00H;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/00H;

    invoke-static {p0}, LX/2Dn;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2Lx;-><init>(LX/0W9;Ljava/lang/Boolean;LX/00H;Ljava/lang/String;)V

    .line 395820
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 395799
    const-string v0, "https://m.%s/"

    .line 395800
    iget-boolean v1, p0, LX/2Lx;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Lx;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 395801
    const-string v0, "https"

    invoke-static {p0, v0}, LX/2Lx;->a(LX/2Lx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 395802
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1, v0}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 395803
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 395804
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x2f

    if-ne v0, v2, :cond_2

    .line 395805
    const/4 v0, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, p2, v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 395806
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 395807
    :cond_2
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 395808
    iget-object v0, p0, LX/2Lx;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v0

    .line 395809
    const-string v1, "http://m.%s/"

    .line 395810
    iget-boolean v2, p0, LX/2Lx;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/2Lx;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 395811
    const-string v1, "http"

    invoke-static {p0, v1}, LX/2Lx;->a(LX/2Lx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 395812
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, v1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 395813
    iget-object v1, p0, LX/2Lx;->c:LX/00H;

    invoke-virtual {v1}, LX/00H;->c()Ljava/lang/String;

    move-result-object v1

    .line 395814
    goto :goto_0

    .line 395815
    :goto_0
    const-string v3, "cid"

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    .line 395816
    const-string v1, "locale"

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 395817
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 395818
    return-object v0
.end method
