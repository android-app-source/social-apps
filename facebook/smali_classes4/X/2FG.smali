.class public LX/2FG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/view/accessibility/AccessibilityManager;

.field public final c:LX/03V;

.field public final d:Landroid/content/ContentResolver;

.field public final e:Landroid/content/res/Configuration;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 386748
    const-class v0, LX/2FG;

    sput-object v0, LX/2FG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/view/accessibility/AccessibilityManager;LX/03V;Landroid/content/ContentResolver;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386750
    iput-object p1, p0, LX/2FG;->b:Landroid/view/accessibility/AccessibilityManager;

    .line 386751
    iput-object p2, p0, LX/2FG;->c:LX/03V;

    .line 386752
    iput-object p3, p0, LX/2FG;->d:Landroid/content/ContentResolver;

    .line 386753
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object v0, p0, LX/2FG;->e:Landroid/content/res/Configuration;

    .line 386754
    return-void
.end method

.method public static a(Landroid/accessibilityservice/AccessibilityServiceInfo;)LX/0m9;
    .locals 7

    .prologue
    .line 386755
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 386756
    const-string v0, "event_type"

    iget v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 386757
    const-string v0, "feedback_type"

    iget v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    const/4 v6, 0x1

    .line 386758
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 386759
    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386760
    :goto_0
    if-lez v2, :cond_1

    .line 386761
    invoke-static {v2}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v4

    shl-int v4, v6, v4

    .line 386762
    xor-int/lit8 v5, v4, -0x1

    and-int/2addr v2, v5

    .line 386763
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-le v5, v6, :cond_0

    .line 386764
    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386765
    :cond_0
    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 386766
    :sswitch_0
    const-string v4, "FEEDBACK_SPOKEN"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386767
    :sswitch_1
    const-string v4, "FEEDBACK_AUDIBLE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386768
    :sswitch_2
    const-string v4, "FEEDBACK_HAPTIC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386769
    :sswitch_3
    const-string v4, "FEEDBACK_GENERIC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386770
    :sswitch_4
    const-string v4, "FEEDBACK_VISUAL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386771
    :cond_1
    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386772
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 386773
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386774
    const-string v0, "id"

    invoke-virtual {p0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386775
    iget v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    .line 386776
    sparse-switch v0, :sswitch_data_1

    .line 386777
    const/4 v2, 0x0

    :goto_1
    move-object v0, v2

    .line 386778
    if-eqz v0, :cond_2

    .line 386779
    const-string v2, "flags"

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386780
    :cond_2
    const-string v0, "notification_timeout"

    iget-wide v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    invoke-virtual {v1, v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 386781
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    .line 386782
    if-eqz v2, :cond_4

    array-length v0, v2

    if-lez v0, :cond_4

    .line 386783
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 386784
    const/4 v0, 0x0

    :goto_2
    array-length v4, v2

    if-ge v0, v4, :cond_3

    .line 386785
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "package_name_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aget-object v5, v2, v0

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386786
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 386787
    :cond_3
    const-string v0, "package_names"

    invoke-virtual {v1, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 386788
    :cond_4
    return-object v1

    .line 386789
    :sswitch_5
    const-string v2, "DEFAULT"

    goto :goto_1

    .line 386790
    :sswitch_6
    const-string v2, "FLAG_INCLUDE_NOT_IMPORTANT_VIEWS"

    goto :goto_1

    .line 386791
    :sswitch_7
    const-string v2, "FLAG_REQUEST_TOUCH_EXPLORATION_MODE"

    goto :goto_1

    .line 386792
    :sswitch_8
    const-string v2, "FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY"

    goto :goto_1

    .line 386793
    :sswitch_9
    const-string v2, "FLAG_REPORT_VIEW_IDS"

    goto :goto_1

    .line 386794
    :sswitch_a
    const-string v2, "FLAG_REQUEST_FILTER_KEY_EVENTS"

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_1
        0x8 -> :sswitch_4
        0x10 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_6
        0x4 -> :sswitch_7
        0x8 -> :sswitch_8
        0x10 -> :sswitch_9
        0x20 -> :sswitch_a
    .end sparse-switch
.end method
