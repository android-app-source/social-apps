.class public LX/37t;
.super LX/37u;
.source ""

# interfaces
.implements LX/37w;
.implements LX/37x;


# static fields
.field private static final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/lang/Object;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/lang/Object;

.field public e:I

.field public f:Z

.field public g:Z

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/38G;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67Q;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/37l;

.field private m:LX/67M;

.field private n:LX/38F;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 502671
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 502672
    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 502673
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 502674
    sput-object v1, LX/37t;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502675
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 502676
    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 502677
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 502678
    sput-object v1, LX/37t;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502679
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/37l;)V
    .locals 3

    .prologue
    .line 502594
    invoke-direct {p0, p1}, LX/37u;-><init>(Landroid/content/Context;)V

    .line 502595
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    .line 502596
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/37t;->i:Ljava/util/ArrayList;

    .line 502597
    iput-object p2, p0, LX/37t;->l:LX/37l;

    .line 502598
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v0, v0

    .line 502599
    iput-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    .line 502600
    invoke-virtual {p0}, LX/37t;->h()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/37t;->b:Ljava/lang/Object;

    .line 502601
    new-instance v0, LX/38E;

    invoke-direct {v0, p0}, LX/38E;-><init>(LX/37x;)V

    move-object v0, v0

    .line 502602
    iput-object v0, p0, LX/37t;->c:Ljava/lang/Object;

    .line 502603
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 502604
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    const v2, 0x7f081a19

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 502605
    check-cast v1, Landroid/media/MediaRouter;

    invoke-virtual {v1, v0, v2}, Landroid/media/MediaRouter;->createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;

    move-result-object p1

    move-object v0, p1

    .line 502606
    iput-object v0, p0, LX/37t;->d:Ljava/lang/Object;

    .line 502607
    invoke-direct {p0}, LX/37t;->j()V

    .line 502608
    return-void
.end method

.method private a(LX/38G;)V
    .locals 3

    .prologue
    .line 502609
    new-instance v0, LX/38H;

    iget-object v1, p1, LX/38G;->b:Ljava/lang/String;

    iget-object v2, p1, LX/38G;->a:Ljava/lang/Object;

    invoke-static {p0, v2}, LX/37t;->k(LX/37t;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/38H;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 502610
    invoke-virtual {p0, p1, v0}, LX/37t;->a(LX/38G;LX/38H;)V

    .line 502611
    invoke-virtual {v0}, LX/38H;->a()LX/383;

    move-result-object v0

    iput-object v0, p1, LX/38G;->c:LX/383;

    .line 502612
    return-void
.end method

.method public static b(LX/37t;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 502613
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502614
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502615
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    iget-object v0, v0, LX/38G;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 502616
    :goto_1
    return v0

    .line 502617
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502618
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private e(LX/384;)I
    .locals 3

    .prologue
    .line 502619
    iget-object v0, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502620
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502621
    iget-object v0, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67Q;

    iget-object v0, v0, LX/67Q;->a:LX/384;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 502622
    :goto_1
    return v0

    .line 502623
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502624
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private f(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    .line 502625
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .line 502626
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 502627
    invoke-virtual {p0}, LX/37t;->i()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    move v0, v4

    .line 502628
    :goto_0
    if-eqz v0, :cond_2

    const-string v0, "DEFAULT_ROUTE"

    .line 502629
    :goto_1
    invoke-static {p0, v0}, LX/37t;->b(LX/37t;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_3

    .line 502630
    :goto_2
    move-object v0, v0

    .line 502631
    new-instance v1, LX/38G;

    invoke-direct {v1, p1, v0}, LX/38G;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502632
    invoke-direct {p0, v1}, LX/37t;->a(LX/38G;)V

    .line 502633
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502634
    const/4 v0, 0x1

    .line 502635
    :goto_3
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    :cond_1
    move v0, v5

    .line 502636
    goto :goto_0

    .line 502637
    :cond_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ROUTE_%08x"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, p1}, LX/37t;->k(LX/37t;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 502638
    :goto_4
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s_%d"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 502639
    invoke-static {p0, v3}, LX/37t;->b(LX/37t;Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_4

    move-object v0, v3

    .line 502640
    goto :goto_2

    .line 502641
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method private static j(Ljava/lang/Object;)LX/67Q;
    .locals 2

    .prologue
    .line 502642
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v0, v0

    .line 502643
    instance-of v1, v0, LX/67Q;

    if-eqz v1, :cond_0

    check-cast v0, LX/67Q;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 6

    .prologue
    .line 502570
    const/4 v0, 0x0

    .line 502571
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    .line 502572
    check-cast v1, Landroid/media/MediaRouter;

    .line 502573
    invoke-virtual {v1}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v3

    .line 502574
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 502575
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 502576
    invoke-virtual {v1, v2}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502577
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 502578
    :cond_0
    move-object v1, v4

    .line 502579
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 502580
    invoke-direct {p0, v2}, LX/37t;->f(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 502581
    goto :goto_1

    .line 502582
    :cond_1
    if-eqz v0, :cond_2

    .line 502583
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502584
    :cond_2
    return-void
.end method

.method public static k(LX/37t;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 502644
    iget-object v0, p0, LX/37v;->a:Landroid/content/Context;

    move-object v0, v0

    .line 502645
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p0

    move-object v0, p0

    .line 502646
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/389;
    .locals 2

    .prologue
    .line 502647
    invoke-static {p0, p1}, LX/37t;->b(LX/37t;Ljava/lang/String;)I

    move-result v0

    .line 502648
    if-ltz v0, :cond_0

    .line 502649
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502650
    new-instance v1, LX/38O;

    iget-object v0, v0, LX/38G;->a:Ljava/lang/Object;

    invoke-direct {v1, p0, v0}, LX/38O;-><init>(LX/37t;Ljava/lang/Object;)V

    move-object v0, v1

    .line 502651
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/384;)V
    .locals 3

    .prologue
    .line 502652
    invoke-virtual {p1}, LX/384;->m()LX/37v;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 502653
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/37t;->d:Ljava/lang/Object;

    .line 502654
    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;

    move-result-object v2

    move-object v0, v2

    .line 502655
    new-instance v1, LX/67Q;

    invoke-direct {v1, p1, v0}, LX/67Q;-><init>(LX/384;Ljava/lang/Object;)V

    .line 502656
    invoke-static {v0, v1}, LX/4tq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502657
    iget-object v2, p0, LX/37t;->c:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/7YH;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502658
    invoke-virtual {p0, v1}, LX/37t;->a(LX/67Q;)V

    .line 502659
    iget-object v2, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502660
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    .line 502661
    check-cast v1, Landroid/media/MediaRouter;

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v1, v0}, Landroid/media/MediaRouter;->addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 502662
    :cond_0
    :goto_0
    return-void

    .line 502663
    :cond_1
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, LX/3Fl;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 502664
    invoke-virtual {p0, v0}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502665
    if-ltz v0, :cond_0

    .line 502666
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502667
    iget-object v0, v0, LX/38G;->b:Ljava/lang/String;

    .line 502668
    iget-object v1, p1, LX/384;->b:Ljava/lang/String;

    move-object v1, v1

    .line 502669
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502670
    invoke-virtual {p1}, LX/384;->k()V

    goto :goto_0
.end method

.method public a(LX/38G;LX/38H;)V
    .locals 2

    .prologue
    .line 502550
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502551
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v1

    move v0, v1

    .line 502552
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 502553
    sget-object v1, LX/37t;->j:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, LX/38H;->a(Ljava/util/Collection;)LX/38H;

    .line 502554
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 502555
    sget-object v0, LX/37t;->k:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, LX/38H;->a(Ljava/util/Collection;)LX/38H;

    .line 502556
    :cond_1
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502557
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v1

    move v0, v1

    .line 502558
    invoke-virtual {p2, v0}, LX/38H;->a(I)LX/38H;

    .line 502559
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502560
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackStream()I

    move-result v1

    move v0, v1

    .line 502561
    invoke-virtual {p2, v0}, LX/38H;->b(I)LX/38H;

    .line 502562
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/4tq;->d(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p2, v0}, LX/38H;->c(I)LX/38H;

    .line 502563
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502564
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v1

    move v0, v1

    .line 502565
    invoke-virtual {p2, v0}, LX/38H;->d(I)LX/38H;

    .line 502566
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502567
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v1

    move v0, v1

    .line 502568
    invoke-virtual {p2, v0}, LX/38H;->e(I)LX/38H;

    .line 502569
    return-void
.end method

.method public a(LX/67Q;)V
    .locals 2

    .prologue
    .line 502680
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502681
    iget-object p0, v1, LX/384;->d:Ljava/lang/String;

    move-object v1, p0

    .line 502682
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    .line 502683
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502684
    iget p0, v1, LX/384;->i:I

    move v1, p0

    .line 502685
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackType(I)V

    .line 502686
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502687
    iget p0, v1, LX/384;->j:I

    move v1, p0

    .line 502688
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackStream(I)V

    .line 502689
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502690
    iget p0, v1, LX/384;->l:I

    move v1, p0

    .line 502691
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolume(I)V

    .line 502692
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502693
    iget p0, v1, LX/384;->m:I

    move v1, p0

    .line 502694
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeMax(I)V

    .line 502695
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502696
    iget p0, v1, LX/384;->k:I

    move v1, p0

    .line 502697
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeHandling(I)V

    .line 502698
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 502699
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, LX/3Fl;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 502700
    :cond_0
    :goto_0
    return-void

    .line 502701
    :cond_1
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    .line 502702
    if-eqz v0, :cond_2

    .line 502703
    iget-object v0, v0, LX/67Q;->a:LX/384;

    invoke-virtual {v0}, LX/384;->k()V

    goto :goto_0

    .line 502704
    :cond_2
    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502705
    if-ltz v0, :cond_0

    .line 502706
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502707
    iget-object v1, p0, LX/37t;->l:LX/37l;

    iget-object v0, v0, LX/38G;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, LX/37l;->a(Ljava/lang/String;)LX/384;

    move-result-object v0

    .line 502708
    if-eqz v0, :cond_0

    .line 502709
    invoke-virtual {v0}, LX/384;->k()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 502710
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    .line 502711
    if-eqz v0, :cond_0

    .line 502712
    iget-object v0, v0, LX/67Q;->a:LX/384;

    .line 502713
    invoke-static {}, LX/37i;->b()V

    .line 502714
    sget-object v1, LX/37i;->a:LX/37j;

    iget p0, v0, LX/384;->m:I

    const/4 p1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 502715
    iget-object p1, v1, LX/37j;->n:LX/384;

    if-ne v0, p1, :cond_0

    iget-object p1, v1, LX/37j;->o:LX/389;

    if-eqz p1, :cond_0

    .line 502716
    iget-object p1, v1, LX/37j;->o:LX/389;

    invoke-virtual {p1, p0}, LX/389;->a(I)V

    .line 502717
    :cond_0
    return-void
.end method

.method public final b(LX/384;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 502585
    invoke-virtual {p1}, LX/384;->m()LX/37v;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 502586
    invoke-direct {p0, p1}, LX/37t;->e(LX/384;)I

    move-result v0

    .line 502587
    if-ltz v0, :cond_0

    .line 502588
    iget-object v1, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67Q;

    .line 502589
    iget-object v1, v0, LX/67Q;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/4tq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502590
    iget-object v1, v0, LX/67Q;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/7YH;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502591
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    iget-object v0, v0, LX/67Q;->b:Ljava/lang/Object;

    .line 502592
    check-cast v1, Landroid/media/MediaRouter;

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v1, v0}, Landroid/media/MediaRouter;->removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 502593
    :cond_0
    return-void
.end method

.method public final b(LX/38A;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 502429
    if-eqz p1, :cond_5

    .line 502430
    invoke-virtual {p1}, LX/38A;->a()LX/38T;

    move-result-object v1

    .line 502431
    invoke-virtual {v1}, LX/38T;->a()Ljava/util/List;

    move-result-object v3

    .line 502432
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    .line 502433
    :goto_0
    if-ge v2, v4, :cond_2

    .line 502434
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 502435
    const-string v5, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 502436
    or-int/lit8 v0, v1, 0x1

    .line 502437
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 502438
    :cond_0
    const-string v5, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502439
    or-int/lit8 v0, v1, 0x2

    goto :goto_1

    .line 502440
    :cond_1
    const/high16 v0, 0x800000

    or-int/2addr v0, v1

    goto :goto_1

    .line 502441
    :cond_2
    invoke-virtual {p1}, LX/38A;->b()Z

    move-result v0

    .line 502442
    :goto_2
    iget v2, p0, LX/37t;->e:I

    if-ne v2, v1, :cond_3

    iget-boolean v2, p0, LX/37t;->f:Z

    if-eq v2, v0, :cond_4

    .line 502443
    :cond_3
    iput v1, p0, LX/37t;->e:I

    .line 502444
    iput-boolean v0, p0, LX/37t;->f:Z

    .line 502445
    invoke-virtual {p0}, LX/37t;->g()V

    .line 502446
    invoke-direct {p0}, LX/37t;->j()V

    .line 502447
    :cond_4
    return-void

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 502448
    invoke-direct {p0, p1}, LX/37t;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502449
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502450
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 502451
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    .line 502452
    if-eqz v0, :cond_0

    .line 502453
    iget-object v0, v0, LX/67Q;->a:LX/384;

    .line 502454
    invoke-static {}, LX/37i;->b()V

    .line 502455
    if-eqz p2, :cond_0

    .line 502456
    sget-object p0, LX/37i;->a:LX/37j;

    .line 502457
    iget-object p1, p0, LX/37j;->n:LX/384;

    if-ne v0, p1, :cond_0

    iget-object p1, p0, LX/37j;->o:LX/389;

    if-eqz p1, :cond_0

    .line 502458
    iget-object p1, p0, LX/37j;->o:LX/389;

    invoke-virtual {p1, p2}, LX/389;->b(I)V

    .line 502459
    :cond_0
    return-void
.end method

.method public final c(LX/384;)V
    .locals 2

    .prologue
    .line 502460
    invoke-virtual {p1}, LX/384;->m()LX/37v;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 502461
    invoke-direct {p0, p1}, LX/37t;->e(LX/384;)I

    move-result v0

    .line 502462
    if-ltz v0, :cond_0

    .line 502463
    iget-object v1, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67Q;

    .line 502464
    invoke-virtual {p0, v0}, LX/37t;->a(LX/67Q;)V

    .line 502465
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 502466
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    if-nez v0, :cond_0

    .line 502467
    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502468
    if-ltz v0, :cond_0

    .line 502469
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 502470
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502471
    :cond_0
    return-void
.end method

.method public final d(LX/384;)V
    .locals 2

    .prologue
    .line 502472
    invoke-static {}, LX/37i;->b()V

    .line 502473
    sget-object v0, LX/37i;->a:LX/37j;

    invoke-virtual {v0}, LX/37j;->b()LX/384;

    move-result-object v0

    if-ne v0, p1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 502474
    if-nez v0, :cond_1

    .line 502475
    :cond_0
    :goto_1
    return-void

    .line 502476
    :cond_1
    invoke-virtual {p1}, LX/384;->m()LX/37v;

    move-result-object v0

    if-eq v0, p0, :cond_2

    .line 502477
    invoke-direct {p0, p1}, LX/37t;->e(LX/384;)I

    move-result v0

    .line 502478
    if-ltz v0, :cond_0

    .line 502479
    iget-object v1, p0, LX/37t;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67Q;

    .line 502480
    iget-object v0, v0, LX/67Q;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/37t;->h(Ljava/lang/Object;)V

    goto :goto_1

    .line 502481
    :cond_2
    iget-object v0, p1, LX/384;->b:Ljava/lang/String;

    move-object v0, v0

    .line 502482
    invoke-static {p0, v0}, LX/37t;->b(LX/37t;Ljava/lang/String;)I

    move-result v0

    .line 502483
    if-ltz v0, :cond_0

    .line 502484
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502485
    iget-object v0, v0, LX/38G;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/37t;->h(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 502486
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    if-nez v0, :cond_0

    .line 502487
    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502488
    if-ltz v0, :cond_0

    .line 502489
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502490
    invoke-direct {p0, v0}, LX/37t;->a(LX/38G;)V

    .line 502491
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502492
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 502493
    invoke-static {p1}, LX/37t;->j(Ljava/lang/Object;)LX/67Q;

    move-result-object v0

    if-nez v0, :cond_0

    .line 502494
    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502495
    if-ltz v0, :cond_0

    .line 502496
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502497
    invoke-static {p1}, LX/4tq;->d(Ljava/lang/Object;)I

    move-result v1

    .line 502498
    iget-object v2, v0, LX/38G;->c:LX/383;

    invoke-virtual {v2}, LX/383;->i()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 502499
    new-instance v2, LX/38H;

    iget-object v3, v0, LX/38G;->c:LX/383;

    invoke-direct {v2, v3}, LX/38H;-><init>(LX/383;)V

    invoke-virtual {v2, v1}, LX/38H;->c(I)LX/38H;

    move-result-object v1

    invoke-virtual {v1}, LX/38H;->a()LX/383;

    move-result-object v1

    iput-object v1, v0, LX/38G;->c:LX/383;

    .line 502500
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502501
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 502502
    new-instance v2, LX/38N;

    invoke-direct {v2}, LX/38N;-><init>()V

    .line 502503
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 502504
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 502505
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    iget-object v0, v0, LX/38G;->c:LX/383;

    invoke-virtual {v2, v0}, LX/38N;->a(LX/383;)LX/38N;

    .line 502506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 502507
    :cond_0
    invoke-virtual {v2}, LX/38N;->a()LX/382;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/37v;->a(LX/382;)V

    .line 502508
    return-void
.end method

.method public final g(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 502509
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 502510
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 502511
    iget-object v0, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    iget-object v0, v0, LX/38G;->a:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 502512
    :goto_1
    return v0

    .line 502513
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 502514
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public g()V
    .locals 3

    .prologue
    .line 502515
    iget-boolean v0, p0, LX/37t;->g:Z

    if-eqz v0, :cond_0

    .line 502516
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/37t;->g:Z

    .line 502517
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/37t;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/3Fl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502518
    :cond_0
    iget v0, p0, LX/37t;->e:I

    if-eqz v0, :cond_1

    .line 502519
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/37t;->g:Z

    .line 502520
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    iget v1, p0, LX/37t;->e:I

    iget-object v2, p0, LX/37t;->b:Ljava/lang/Object;

    .line 502521
    check-cast v0, Landroid/media/MediaRouter;

    check-cast v2, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    .line 502522
    :cond_1
    return-void
.end method

.method public h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 502523
    new-instance v0, LX/38D;

    invoke-direct {v0, p0}, LX/38D;-><init>(LX/37w;)V

    move-object v0, v0

    .line 502524
    return-object v0
.end method

.method public h(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 502525
    iget-object v0, p0, LX/37t;->m:LX/67M;

    if-nez v0, :cond_0

    .line 502526
    new-instance v0, LX/67M;

    invoke-direct {v0}, LX/67M;-><init>()V

    iput-object v0, p0, LX/37t;->m:LX/67M;

    .line 502527
    :cond_0
    iget-object v0, p0, LX/37t;->m:LX/67M;

    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    const v2, 0x800003

    .line 502528
    check-cast v1, Landroid/media/MediaRouter;

    .line 502529
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    .line 502530
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v3

    .line 502531
    const/high16 v4, 0x800000

    and-int/2addr v3, v4

    if-nez v3, :cond_1

    .line 502532
    iget-object v3, v0, LX/67M;->a:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_2

    .line 502533
    :try_start_0
    iget-object v3, v0, LX/67M;->a:Ljava/lang/reflect/Method;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 502534
    :goto_0
    return-void

    .line 502535
    :catch_0
    move-exception v3

    .line 502536
    const-string v4, "MediaRouterJellybean"

    const-string v5, "Cannot programmatically select non-user route.  Media routing may not work."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 502537
    :cond_1
    :goto_1
    invoke-virtual {v1, v2, p1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    .line 502538
    :catch_1
    move-exception v3

    .line 502539
    const-string v4, "MediaRouterJellybean"

    const-string v5, "Cannot programmatically select non-user route.  Media routing may not work."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 502540
    :cond_2
    const-string v3, "MediaRouterJellybean"

    const-string v4, "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public i()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 502541
    iget-object v0, p0, LX/37t;->n:LX/38F;

    if-nez v0, :cond_0

    .line 502542
    new-instance v0, LX/38F;

    invoke-direct {v0}, LX/38F;-><init>()V

    iput-object v0, p0, LX/37t;->n:LX/38F;

    .line 502543
    :cond_0
    iget-object v0, p0, LX/37t;->n:LX/38F;

    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    const/4 p0, 0x0

    .line 502544
    check-cast v1, Landroid/media/MediaRouter;

    .line 502545
    iget-object v2, v0, LX/38F;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    .line 502546
    :try_start_0
    iget-object v2, v0, LX/38F;->a:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 502547
    :goto_0
    move-object v0, v2

    .line 502548
    return-object v0

    :catch_0
    :cond_1
    :goto_1
    invoke-virtual {v1, p0}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v2

    goto :goto_0

    .line 502549
    :catch_1
    goto :goto_1
.end method
