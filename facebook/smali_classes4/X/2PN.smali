.class public final LX/2PN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2PD;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;)V
    .locals 0

    .prologue
    .line 406387
    iput-object p1, p0, LX/2PN;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([B)V
    .locals 2

    .prologue
    .line 406388
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a:Ljava/lang/Class;

    const-string v1, "Multi-endpoint batch lookup failed"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406389
    return-void
.end method

.method public final a([BLjava/lang/Long;LX/DpK;Z)V
    .locals 6

    .prologue
    .line 406390
    iget-object v0, p0, LX/2PN;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v0, v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->h:LX/2PE;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    iget-object v2, p3, LX/DpK;->msg_to:LX/DpM;

    iget-object v2, v2, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, LX/DpK;->msg_to:LX/DpM;

    iget-object v3, v3, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v4, p3, LX/DpK;->suggested_codename:Ljava/lang/String;

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/2PE;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 406391
    iget-object v0, p0, LX/2PN;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a([BLjava/lang/Long;LX/DpK;Z)V

    .line 406392
    return-void
.end method

.method public final b([B)V
    .locals 1

    .prologue
    .line 406385
    iget-object v0, p0, LX/2PN;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b([B)V

    .line 406386
    return-void
.end method
