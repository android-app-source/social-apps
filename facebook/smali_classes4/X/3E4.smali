.class public LX/3E4;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3E4;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2z7;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>(Landroid/os/Looper;LX/0Zb;LX/0Or;LX/0Ot;)V
    .locals 1
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/config/IsVpvSequenceIdEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2z7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536859
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 536860
    const/4 v0, -0x1

    iput v0, p0, LX/3E4;->d:I

    .line 536861
    iput-object p2, p0, LX/3E4;->a:LX/0Zb;

    .line 536862
    iput-object p3, p0, LX/3E4;->b:LX/0Or;

    .line 536863
    iput-object p4, p0, LX/3E4;->c:LX/0Ot;

    .line 536864
    return-void
.end method

.method public static a(LX/0QB;)LX/3E4;
    .locals 7

    .prologue
    .line 536865
    sget-object v0, LX/3E4;->e:LX/3E4;

    if-nez v0, :cond_1

    .line 536866
    const-class v1, LX/3E4;

    monitor-enter v1

    .line 536867
    :try_start_0
    sget-object v0, LX/3E4;->e:LX/3E4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 536868
    if-eqz v2, :cond_0

    .line 536869
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 536870
    new-instance v5, LX/3E4;

    invoke-static {v0}, LX/0Zp;->b(LX/0QB;)Landroid/os/Looper;

    move-result-object v3

    check-cast v3, Landroid/os/Looper;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v6, 0x1443

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x83

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, LX/3E4;-><init>(Landroid/os/Looper;LX/0Zb;LX/0Or;LX/0Ot;)V

    .line 536871
    move-object v0, v5

    .line 536872
    sput-object v0, LX/3E4;->e:LX/3E4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536873
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 536874
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 536875
    :cond_1
    sget-object v0, LX/3E4;->e:LX/3E4;

    return-object v0

    .line 536876
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 536877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 536878
    iget-object v0, p0, LX/3E4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536879
    iget v0, p0, LX/3E4;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/3E4;->d:I

    .line 536880
    iget v0, p0, LX/3E4;->d:I

    rem-int/lit8 v0, v0, 0xb

    if-nez v0, :cond_0

    .line 536881
    iget-object v0, p0, LX/3E4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2z7;

    const/4 v1, 0x0

    .line 536882
    iget-object v2, v0, LX/2z7;->b:LX/3zZ;

    sget-object p0, LX/3zT;->a:LX/3zU;

    invoke-virtual {v2, p0, v1}, LX/2Iu;->a(LX/0To;I)I

    move-result v2

    iput v2, v0, LX/2z7;->a:I

    .line 536883
    iget v2, v0, LX/2z7;->a:I

    const p0, 0x7fffffff

    if-ne v2, p0, :cond_1

    :goto_0
    iput v1, v0, LX/2z7;->a:I

    .line 536884
    iget-object v1, v0, LX/2z7;->b:LX/3zZ;

    sget-object v2, LX/3zT;->a:LX/3zU;

    iget p0, v0, LX/2z7;->a:I

    invoke-virtual {v1, v2, p0}, LX/2Iu;->b(LX/0To;I)V

    .line 536885
    iget v1, v0, LX/2z7;->a:I

    move v0, v1

    .line 536886
    const-string v1, "seq_id"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536887
    :cond_0
    return-void

    .line 536888
    :cond_1
    iget v1, v0, LX/2z7;->a:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 536889
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, LX/3E4;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3E4;->sendMessage(Landroid/os/Message;)Z

    .line 536890
    return-void
.end method

.method public final b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 536891
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, LX/3E4;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3E4;->sendMessage(Landroid/os/Message;)Z

    .line 536892
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 536893
    const/4 v1, 0x0

    .line 536894
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 536895
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536896
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536897
    :goto_0
    invoke-direct {p0, v0}, LX/3E4;->c(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 536898
    if-eqz v1, :cond_0

    .line 536899
    iget-object v1, p0, LX/3E4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 536900
    :goto_1
    return-void

    .line 536901
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 536902
    const/4 v1, 0x1

    .line 536903
    goto :goto_0

    .line 536904
    :cond_0
    iget-object v1, p0, LX/3E4;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
