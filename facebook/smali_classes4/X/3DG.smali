.class public final enum LX/3DG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3DG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3DG;

.field public static final enum BEEPER:LX/3DG;

.field public static final enum HOME:LX/3DG;

.field public static final enum JEWEL:LX/3DG;

.field public static final enum LOCK_SCREEN:LX/3DG;

.field public static final enum PERMALINK:LX/3DG;

.field public static final enum SYSTEM_TRAY:LX/3DG;

.field public static final enum WEARABLE_DETAIL:LX/3DG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 532078
    new-instance v0, LX/3DG;

    const-string v1, "JEWEL"

    invoke-direct {v0, v1, v3}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->JEWEL:LX/3DG;

    .line 532079
    new-instance v0, LX/3DG;

    const-string v1, "SYSTEM_TRAY"

    invoke-direct {v0, v1, v4}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->SYSTEM_TRAY:LX/3DG;

    .line 532080
    new-instance v0, LX/3DG;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v5}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->HOME:LX/3DG;

    .line 532081
    new-instance v0, LX/3DG;

    const-string v1, "PERMALINK"

    invoke-direct {v0, v1, v6}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->PERMALINK:LX/3DG;

    .line 532082
    new-instance v0, LX/3DG;

    const-string v1, "LOCK_SCREEN"

    invoke-direct {v0, v1, v7}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->LOCK_SCREEN:LX/3DG;

    .line 532083
    new-instance v0, LX/3DG;

    const-string v1, "BEEPER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->BEEPER:LX/3DG;

    .line 532084
    new-instance v0, LX/3DG;

    const-string v1, "WEARABLE_DETAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3DG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DG;->WEARABLE_DETAIL:LX/3DG;

    .line 532085
    const/4 v0, 0x7

    new-array v0, v0, [LX/3DG;

    sget-object v1, LX/3DG;->JEWEL:LX/3DG;

    aput-object v1, v0, v3

    sget-object v1, LX/3DG;->SYSTEM_TRAY:LX/3DG;

    aput-object v1, v0, v4

    sget-object v1, LX/3DG;->HOME:LX/3DG;

    aput-object v1, v0, v5

    sget-object v1, LX/3DG;->PERMALINK:LX/3DG;

    aput-object v1, v0, v6

    sget-object v1, LX/3DG;->LOCK_SCREEN:LX/3DG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3DG;->BEEPER:LX/3DG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3DG;->WEARABLE_DETAIL:LX/3DG;

    aput-object v2, v0, v1

    sput-object v0, LX/3DG;->$VALUES:[LX/3DG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 532086
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3DG;
    .locals 1

    .prologue
    .line 532087
    const-class v0, LX/3DG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3DG;

    return-object v0
.end method

.method public static values()[LX/3DG;
    .locals 1

    .prologue
    .line 532088
    sget-object v0, LX/3DG;->$VALUES:[LX/3DG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3DG;

    return-object v0
.end method
