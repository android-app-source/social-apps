.class public LX/2LU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LP;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/03V;

.field private final d:LX/2LS;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 395082
    const-string v0, "content://com.huawei.android.launcher.settings/badge/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/2LU;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/launcherbadges/AppLaunchClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395084
    iput-object p1, p0, LX/2LU;->b:Landroid/content/Context;

    .line 395085
    iput-object p2, p0, LX/2LU;->c:LX/03V;

    .line 395086
    iput-object p3, p0, LX/2LU;->d:LX/2LS;

    .line 395087
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2LU;->e:Ljava/lang/String;

    .line 395088
    iput-object p4, p0, LX/2LU;->f:Ljava/lang/String;

    .line 395089
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    .line 395090
    return-void
.end method


# virtual methods
.method public final a(I)LX/03R;
    .locals 5

    .prologue
    .line 395091
    iget-object v0, p0, LX/2LU;->g:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 395092
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/2LU;->d:LX/2LS;

    .line 395093
    invoke-static {v0}, LX/2LS;->i(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.huawei.android.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 395094
    if-eqz v0, :cond_1

    .line 395095
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    .line 395096
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2LU;->g:LX/03R;

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_2

    .line 395097
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 395098
    :goto_1
    return-object v0

    .line 395099
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    goto :goto_0

    .line 395100
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 395101
    const-string v1, "package"

    iget-object v2, p0, LX/2LU;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 395102
    const-string v1, "class"

    iget-object v2, p0, LX/2LU;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 395103
    const-string v1, "badgenumber"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 395104
    :try_start_0
    iget-object v1, p0, LX/2LU;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, LX/2LU;->a:Landroid/net/Uri;

    const-string v3, "change_badge"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 395105
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_1

    .line 395106
    :catch_0
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    .line 395107
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_1

    .line 395108
    :catch_1
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    .line 395109
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_1

    .line 395110
    :catch_2
    move-exception v0

    .line 395111
    iget-object v1, p0, LX/2LU;->c:LX/03V;

    const-string v2, "huawei_badging"

    const-string v3, "Failed to set app badge count."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395112
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LU;->g:LX/03R;

    .line 395113
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_1
.end method
