.class public LX/2HT;
.super LX/2Gj;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field public final c:LX/1sd;

.field public final d:LX/2HV;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/1sd;LX/2HV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390473
    invoke-direct {p0}, LX/2Gj;-><init>()V

    .line 390474
    iput-object p1, p0, LX/2HT;->a:Landroid/content/Context;

    .line 390475
    iput-object p2, p0, LX/2HT;->b:Landroid/content/pm/PackageManager;

    .line 390476
    iput-object p3, p0, LX/2HT;->c:LX/1sd;

    .line 390477
    iput-object p4, p0, LX/2HT;->d:LX/2HV;

    .line 390478
    return-void
.end method

.method public static a(LX/2HT;Ljava/lang/String;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 390479
    :try_start_0
    iget-object v0, p0, LX/2HT;->b:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 390480
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 390481
    :goto_0
    return-object v0

    :catch_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0m9;Ljava/lang/String;LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m9;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390460
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390461
    sget-object v0, LX/2HY;->MISSING:LX/2HY;

    invoke-virtual {v0}, LX/2HY;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390462
    :goto_0
    return-void

    .line 390463
    :cond_0
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 390464
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v0, :cond_1

    .line 390465
    sget-object v0, LX/2HY;->DISABLED:LX/2HY;

    invoke-virtual {v0}, LX/2HY;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 390466
    :cond_1
    sget-object v0, LX/2HY;->ACTIVE:LX/2HY;

    invoke-virtual {v0}, LX/2HY;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0
.end method

.method public static b(LX/0m9;Ljava/lang/String;LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m9;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390467
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390468
    :goto_0
    return-void

    .line 390469
    :cond_0
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 390470
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 390471
    sget-object v0, LX/JvA;->PRELOADED:LX/JvA;

    invoke-virtual {v0}, LX/JvA;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 390472
    :cond_1
    sget-object v0, LX/JvA;->SIDELOADED:LX/JvA;

    invoke-virtual {v0}, LX/JvA;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0
.end method

.method public static c(LX/0m9;Ljava/lang/String;LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m9;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390482
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390483
    :goto_0
    return-void

    .line 390484
    :cond_0
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 390485
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {p0, p1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 4

    .prologue
    .line 390437
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 390438
    iget-object v1, p0, LX/2HT;->d:LX/2HV;

    .line 390439
    iget-object v2, v1, LX/2HV;->c:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/2HV;->c:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->d()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 390440
    if-eqz v2, :cond_1

    .line 390441
    iget-object v2, v1, LX/2HV;->a:Landroid/content/Context;

    invoke-static {v1, v2}, LX/2HV;->a(LX/2HV;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 390442
    :goto_1
    move-object v1, v2

    .line 390443
    if-eqz v1, :cond_0

    .line 390444
    const-string v2, "oxygen_preload_id"

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390445
    :cond_0
    const-string v1, "sdk_level"

    iget-object v2, p0, LX/2HT;->c:LX/1sd;

    invoke-virtual {v2}, LX/1sd;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 390446
    iget-object v1, p0, LX/2HT;->a:Landroid/content/Context;

    invoke-static {v1}, LX/29I;->a(Landroid/content/Context;)LX/2BP;

    move-result-object v1

    .line 390447
    const-string v2, "tos_should_accept"

    iget-boolean v3, v1, LX/2BP;->a:Z

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 390448
    const-string v2, "tos_should_show_explicit"

    iget-boolean v1, v1, LX/2BP;->b:Z

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 390449
    sget-object v1, LX/1ZP;->a:Ljava/lang/String;

    invoke-static {p0, v1}, LX/2HT;->a(LX/2HT;Ljava/lang/String;)LX/0am;

    move-result-object v1

    .line 390450
    const-string v2, "app_manager_state"

    invoke-static {v0, v2, v1}, LX/2HT;->a(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390451
    const-string v2, "app_manager_origin"

    invoke-static {v0, v2, v1}, LX/2HT;->b(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390452
    const-string v2, "app_manager_version_code"

    invoke-static {v0, v2, v1}, LX/2HT;->c(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390453
    sget-object v1, LX/1ZP;->c:Ljava/lang/String;

    invoke-static {p0, v1}, LX/2HT;->a(LX/2HT;Ljava/lang/String;)LX/0am;

    move-result-object v1

    .line 390454
    const-string v2, "installer_state"

    invoke-static {v0, v2, v1}, LX/2HT;->a(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390455
    const-string v2, "installer_origin"

    invoke-static {v0, v2, v1}, LX/2HT;->b(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390456
    const-string v2, "installer_version_code"

    invoke-static {v0, v2, v1}, LX/2HT;->c(LX/0m9;Ljava/lang/String;LX/0am;)V

    .line 390457
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390458
    const-string v0, "fpp_available"

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 390459
    iget-object v0, p0, LX/2HT;->c:LX/1sd;

    invoke-virtual {v0}, LX/1sd;->c()Z

    move-result v0

    return v0
.end method
