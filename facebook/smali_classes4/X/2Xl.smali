.class public LX/2Xl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/41y;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/00I;

.field private final b:LX/2Xm;

.field private final c:LX/0hw;

.field private final d:LX/0dC;


# direct methods
.method public constructor <init>(LX/00I;LX/2Xm;LX/0hw;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420152
    iput-object p1, p0, LX/2Xl;->a:LX/00I;

    .line 420153
    iput-object p2, p0, LX/2Xl;->b:LX/2Xm;

    .line 420154
    iput-object p3, p0, LX/2Xl;->c:LX/0hw;

    .line 420155
    iput-object p4, p0, LX/2Xl;->d:LX/0dC;

    .line 420156
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xl;
    .locals 5

    .prologue
    .line 420157
    new-instance v4, LX/2Xl;

    const-class v0, LX/00I;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00I;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v1

    check-cast v1, LX/2Xm;

    invoke-static {p0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v2

    check-cast v2, LX/0hw;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2Xl;-><init>(LX/00I;LX/2Xm;LX/0hw;LX/0dC;)V

    .line 420158
    move-object v0, v4

    .line 420159
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 420160
    check-cast p1, LX/41y;

    .line 420161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 420162
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "adid"

    iget-object v3, p0, LX/2Xl;->c:LX/0hw;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420163
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420164
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/2Xl;->d:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420165
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "access_token"

    iget-object v3, p1, LX/41y;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420166
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "new_app_id"

    iget-object v0, p1, LX/41y;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/41y;->c:Ljava/lang/String;

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420167
    iget-boolean v0, p1, LX/41y;->d:Z

    if-eqz v0, :cond_0

    .line 420168
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_session_cookies"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420169
    :cond_0
    iget-object v0, p1, LX/41y;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 420170
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    iget-object v3, p1, LX/41y;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420171
    :goto_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 420172
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 420173
    move-object v0, v0

    .line 420174
    const-string v2, "POST"

    .line 420175
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 420176
    move-object v0, v0

    .line 420177
    const-string v2, "method/auth.getSessionForApp"

    .line 420178
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 420179
    move-object v0, v0

    .line 420180
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 420181
    move-object v0, v0

    .line 420182
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 420183
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 420184
    move-object v0, v0

    .line 420185
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 420186
    :cond_1
    iget-object v0, p0, LX/2Xl;->a:LX/00I;

    invoke-interface {v0}, LX/00I;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420187
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_machine_id"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 420188
    check-cast p1, LX/41y;

    .line 420189
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 420190
    iget-object v0, p0, LX/2Xl;->b:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    const/4 v2, 0x0

    iget-boolean v3, p1, LX/41y;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
