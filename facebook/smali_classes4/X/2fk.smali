.class public LX/2fk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2fl;


# direct methods
.method public constructor <init>(LX/2fl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447309
    iput-object p1, p0, LX/2fk;->a:LX/2fl;

    .line 447310
    return-void
.end method

.method public static a(LX/0QB;)LX/2fk;
    .locals 1

    .prologue
    .line 447311
    invoke-static {p0}, LX/2fk;->b(LX/0QB;)LX/2fk;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2fk;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;LX/0P1;)Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;"
        }
    .end annotation

    .prologue
    .line 447312
    invoke-virtual {p2}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447313
    :goto_0
    return-object p1

    .line 447314
    :cond_0
    new-instance v1, LX/4Yh;

    invoke-direct {v1}, LX/4Yh;-><init>()V

    .line 447315
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 447316
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->b:LX/0Px;

    .line 447317
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 447318
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->d:Ljava/lang/String;

    .line 447319
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 447320
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 447321
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 447322
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yh;->h:Ljava/lang/String;

    .line 447323
    invoke-static {v1, p1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 447324
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Yh;->i:LX/0x2;

    .line 447325
    move-object v0, v1

    .line 447326
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v2

    .line 447327
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447328
    :goto_1
    move-object v1, v2

    .line 447329
    iput-object v1, v0, LX/4Yh;->b:LX/0Px;

    .line 447330
    move-object v0, v0

    .line 447331
    new-instance v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;-><init>(LX/4Yh;)V

    .line 447332
    move-object p1, v1

    .line 447333
    goto :goto_0

    .line 447334
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 447335
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, p0, :cond_4

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 447336
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const p1, -0x3625f733

    if-ne v3, p1, :cond_3

    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 447337
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    :goto_3
    invoke-static {v1, v3}, LX/2fl;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 447338
    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 447339
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_3

    .line 447340
    :cond_3
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 447341
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/2fk;
    .locals 2

    .prologue
    .line 447342
    new-instance v1, LX/2fk;

    invoke-static {p0}, LX/2fl;->a(LX/0QB;)LX/2fl;

    move-result-object v0

    check-cast v0, LX/2fl;

    invoke-direct {v1, v0}, LX/2fk;-><init>(LX/2fl;)V

    .line 447343
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;LX/0P1;)Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.System.currentTimeMillis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;"
        }
    .end annotation

    .prologue
    .line 447344
    invoke-virtual {p2}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447345
    :goto_0
    return-object p1

    .line 447346
    :cond_0
    new-instance v7, LX/4Yf;

    invoke-direct {v7}, LX/4Yf;-><init>()V

    .line 447347
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 447348
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->s()LX/0Px;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->b:LX/0Px;

    .line 447349
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->c:Ljava/lang/String;

    .line 447350
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->d:Ljava/lang/String;

    .line 447351
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->F_()J

    move-result-wide v8

    iput-wide v8, v7, LX/4Yf;->e:J

    .line 447352
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->t()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->f:Ljava/lang/String;

    .line 447353
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->u()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->g:Ljava/lang/String;

    .line 447354
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->v()I

    move-result v6

    iput v6, v7, LX/4Yf;->h:I

    .line 447355
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->w()LX/0Px;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->i:LX/0Px;

    .line 447356
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->j:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 447357
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->y()LX/0Px;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->k:LX/0Px;

    .line 447358
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 447359
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->A()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->m:Ljava/lang/String;

    .line 447360
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 447361
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, LX/4Yf;->o:Ljava/lang/String;

    .line 447362
    invoke-static {v7, p1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 447363
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->L_()LX/0x2;

    move-result-object v6

    invoke-virtual {v6}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0x2;

    iput-object v6, v7, LX/4Yf;->p:LX/0x2;

    .line 447364
    move-object v2, v7

    .line 447365
    invoke-static {p1}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v3

    .line 447366
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 447367
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 447368
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 447369
    invoke-static {p0, v0, p2}, LX/2fk;->a(LX/2fk;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;LX/0P1;)Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 447370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 447371
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 447372
    iput-object v0, v2, LX/4Yf;->k:LX/0Px;

    .line 447373
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 447374
    iput-wide v0, v2, LX/4Yf;->e:J

    .line 447375
    new-instance v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-direct {v0, v2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;-><init>(LX/4Yf;)V

    .line 447376
    move-object p1, v0

    .line 447377
    goto/16 :goto_0
.end method
