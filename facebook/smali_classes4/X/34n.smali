.class public LX/34n;
.super LX/34o;
.source ""


# static fields
.field private static final f:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final g:J

.field private final h:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 495682
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/34n;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;LX/0Rf;Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;LX/03V;LX/16H;LX/0iA;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Rf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/16H;",
            "LX/0iA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495683
    move-object v0, p0

    move-object v1, p5

    move-object v2, p3

    move-object v3, p6

    move-object v4, p7

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/34o;-><init>(LX/03V;LX/0Rf;LX/16H;LX/0iA;Ljava/lang/String;)V

    .line 495684
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/34n;->g:J

    .line 495685
    iput-object p4, p0, LX/34n;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    .line 495686
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 495687
    invoke-virtual {p0, p1}, LX/34o;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v0

    .line 495688
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 495689
    :goto_0
    return v0

    .line 495690
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495691
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 495692
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/34n;->h:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v3

    if-eq v2, v3, :cond_2

    :cond_1
    move v0, v1

    .line 495693
    goto :goto_0

    .line 495694
    :cond_2
    iget-wide v2, p0, LX/34n;->g:J

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->a()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    iget-wide v2, p0, LX/34n;->g:J

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    :cond_3
    move v0, v1

    .line 495695
    goto :goto_0

    .line 495696
    :cond_4
    iget-object v0, p0, LX/34o;->e:LX/0iA;

    sget-object v2, LX/34n;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/1YY;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 495697
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method
