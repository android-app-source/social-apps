.class public LX/2Oc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2OW;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/2OW;)V
    .locals 1

    .prologue
    .line 402456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402457
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    .line 402458
    iput-object p1, p0, LX/2Oc;->a:LX/2OW;

    .line 402459
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402460
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402461
    iget-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 402462
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 402463
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 402464
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402465
    iput-boolean p1, p0, LX/2Oc;->c:Z

    .line 402466
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 1

    .prologue
    .line 402467
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402468
    iget-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 402469
    const/4 v0, 0x0

    .line 402470
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 402471
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402472
    iget-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 402473
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402474
    iget-boolean v0, p0, LX/2Oc;->c:Z

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 402475
    iget-object v0, p0, LX/2Oc;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402476
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Oc;->b:Ljava/util/List;

    .line 402477
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Oc;->c:Z

    .line 402478
    return-void
.end method
