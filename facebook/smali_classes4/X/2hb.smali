.class public LX/2hb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2hb;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450186
    iput-object p1, p0, LX/2hb;->a:LX/0Zb;

    .line 450187
    return-void
.end method

.method public static a(LX/0QB;)LX/2hb;
    .locals 4

    .prologue
    .line 450188
    sget-object v0, LX/2hb;->b:LX/2hb;

    if-nez v0, :cond_1

    .line 450189
    const-class v1, LX/2hb;

    monitor-enter v1

    .line 450190
    :try_start_0
    sget-object v0, LX/2hb;->b:LX/2hb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450191
    if-eqz v2, :cond_0

    .line 450192
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 450193
    new-instance p0, LX/2hb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2hb;-><init>(LX/0Zb;)V

    .line 450194
    move-object v0, p0

    .line 450195
    sput-object v0, LX/2hb;->b:LX/2hb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450196
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450197
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450198
    :cond_1
    sget-object v0, LX/2hb;->b:LX/2hb;

    return-object v0

    .line 450199
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450200
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2hb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 450201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "qid\": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", \"mf_story_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", \"ego_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 450202
    new-instance v1, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/162;-><init>(LX/0mC;)V

    .line 450203
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 450204
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 450205
    iget-object v1, p0, LX/2hb;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 450206
    return-void
.end method


# virtual methods
.method public final b(Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 4

    .prologue
    .line 450207
    iget-object v0, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v0, v0

    .line 450208
    if-nez v0, :cond_0

    .line 450209
    :goto_0
    return-void

    .line 450210
    :cond_0
    const-string v0, "pymk_add"

    .line 450211
    iget-object v1, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v1, v1

    .line 450212
    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/2hb;->a(LX/2hb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
