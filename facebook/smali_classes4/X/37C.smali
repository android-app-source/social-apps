.class public LX/37C;
.super LX/2WG;
.source ""


# instance fields
.field public final a:I

.field private final b:Landroid/net/Uri;

.field private final c:LX/1bh;


# direct methods
.method public constructor <init>(Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 500851
    invoke-direct {p0}, LX/2WG;-><init>()V

    .line 500852
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 500853
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LX/37C;->b:Landroid/net/Uri;

    .line 500854
    iput p2, p0, LX/37C;->a:I

    .line 500855
    invoke-static {p1}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 500856
    new-instance p2, LX/1ed;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, LX/1ed;-><init>(Ljava/lang/String;)V

    move-object v0, p2

    .line 500857
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bh;

    iput-object v0, p0, LX/37C;->c:LX/1bh;

    .line 500858
    return-void

    .line 500859
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 1

    .prologue
    .line 500868
    iget-object v0, p0, LX/37C;->c:LX/1bh;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 500869
    iget v0, p0, LX/37C;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 500861
    if-ne p1, p0, :cond_0

    .line 500862
    const/4 v0, 0x1

    .line 500863
    :goto_0
    return v0

    .line 500864
    :cond_0
    instance-of v0, p1, LX/37C;

    if-eqz v0, :cond_1

    .line 500865
    check-cast p1, LX/37C;

    .line 500866
    iget-object v0, p0, LX/37C;->c:LX/1bh;

    iget-object v1, p1, LX/37C;->c:LX/1bh;

    invoke-interface {v0, v1}, LX/1bh;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 500867
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 500860
    iget-object v0, p0, LX/37C;->c:LX/1bh;

    invoke-interface {v0}, LX/1bh;->hashCode()I

    move-result v0

    return v0
.end method
