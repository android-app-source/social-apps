.class public LX/2PP;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "Lcom/facebook/messaging/tincan/outbound/TincanPrimaryDeviceSelectionListener;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2PP;


# instance fields
.field public final c:LX/2PJ;

.field public final d:LX/0SF;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406476
    const-class v0, LX/2PP;

    sput-object v0, LX/2PP;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406471
    const/16 v0, 0xe

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 406472
    iput-object p1, p0, LX/2PP;->c:LX/2PJ;

    .line 406473
    iput-object p2, p0, LX/2PP;->d:LX/0SF;

    .line 406474
    iput-object p3, p0, LX/2PP;->e:LX/0Or;

    .line 406475
    return-void
.end method

.method public static a(LX/0QB;)LX/2PP;
    .locals 6

    .prologue
    .line 406447
    sget-object v0, LX/2PP;->f:LX/2PP;

    if-nez v0, :cond_1

    .line 406448
    const-class v1, LX/2PP;

    monitor-enter v1

    .line 406449
    :try_start_0
    sget-object v0, LX/2PP;->f:LX/2PP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406450
    if-eqz v2, :cond_0

    .line 406451
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406452
    new-instance v5, LX/2PP;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2PP;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 406453
    move-object v0, v5

    .line 406454
    sput-object v0, LX/2PP;->f:LX/2PP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406455
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406456
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406457
    :cond_1
    sget-object v0, LX/2PP;->f:LX/2PP;

    return-object v0

    .line 406458
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 406470
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 3
    .param p1    # LX/Dph;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 406460
    if-nez p1, :cond_0

    .line 406461
    sget-object v0, LX/2PP;->b:Ljava/lang/Class;

    const-string v1, "Error deserialising \'set primary device\' response"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406462
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406463
    invoke-virtual {v0}, LX/2PA;->i()V

    goto :goto_0

    .line 406464
    :cond_0
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 406465
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406466
    invoke-virtual {v0}, LX/2PA;->g()V

    goto :goto_1

    .line 406467
    :cond_1
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PA;

    .line 406468
    invoke-virtual {v0}, LX/2PA;->h()V

    goto :goto_2

    .line 406469
    :cond_2
    return-void
.end method
