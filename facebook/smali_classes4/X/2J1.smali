.class public LX/2J1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Sh;

.field public final c:LX/2Iv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 392518
    const-class v0, LX/2J1;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2J1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/2Iv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392485
    iput-object p1, p0, LX/2J1;->b:LX/0Sh;

    .line 392486
    iput-object p2, p0, LX/2J1;->c:LX/2Iv;

    .line 392487
    return-void
.end method

.method public static a(LX/0QB;)LX/2J1;
    .locals 3

    .prologue
    .line 392488
    new-instance v2, LX/2J1;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v1

    check-cast v1, LX/2Iv;

    invoke-direct {v2, v0, v1}, LX/2J1;-><init>(LX/0Sh;LX/2Iv;)V

    .line 392489
    move-object v0, v2

    .line 392490
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 392491
    iget-object v0, p0, LX/2J1;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 392492
    iget-object v0, p0, LX/2J1;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "contacts_upload_snapshot"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 392493
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Eju;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392494
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392495
    :goto_0
    return-void

    .line 392496
    :cond_0
    const-string v0, "UpdatePhoneAddressBookSnapshot(%d)"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x61793ddb

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 392497
    :try_start_0
    iget-object v0, p0, LX/2J1;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 392498
    const v0, 0x678be7fc

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 392499
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eju;

    .line 392500
    sget-object v3, LX/Ejv;->a:[I

    .line 392501
    iget-object v4, v0, LX/Eju;->c:LX/Ejt;

    move-object v4, v4

    .line 392502
    invoke-virtual {v4}, LX/Ejt;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 392503
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown change type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 392504
    iget-object v4, v0, LX/Eju;->c:LX/Ejt;

    move-object v0, v4

    .line 392505
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392506
    :catchall_0
    move-exception v0

    const v2, 0x44af9765

    :try_start_2
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392507
    :catchall_1
    move-exception v0

    const v1, -0x39f21024

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 392508
    :pswitch_0
    :try_start_3
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 392509
    const-string v6, "local_contact_id"

    iget-wide v7, v0, LX/Eju;->a:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 392510
    const-string v6, "contact_hash"

    iget-object v7, v0, LX/Eju;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392511
    iget-object v6, p0, LX/2J1;->c:LX/2Iv;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "contacts_upload_snapshot"

    const/4 v8, 0x0

    const v9, -0x495ecc29

    invoke-static {v9}, LX/03h;->a(I)V

    invoke-virtual {v6, v7, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v5, 0x48ab2919

    invoke-static {v5}, LX/03h;->a(I)V

    .line 392512
    goto :goto_1

    .line 392513
    :pswitch_1
    iget-object v5, p0, LX/2J1;->c:LX/2Iv;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "contacts_upload_snapshot"

    const-string v7, "local_contact_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v11, v0, LX/Eju;->a:J

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 392514
    goto/16 :goto_1

    .line 392515
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 392516
    const v0, -0x7d9c502a

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 392517
    const v0, -0x3141b36c

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
