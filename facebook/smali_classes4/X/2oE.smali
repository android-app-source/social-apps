.class public LX/2oE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/net/Uri;

.field public c:Landroid/net/Uri;

.field public d:Ljava/lang/String;

.field public e:LX/097;

.field public f:Landroid/graphics/RectF;

.field public g:LX/2oF;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 465788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465789
    new-instance v0, Landroid/graphics/RectF;

    sget-object v1, Lcom/facebook/video/engine/VideoDataSource;->a:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, LX/2oE;->f:Landroid/graphics/RectF;

    .line 465790
    sget-object v0, LX/2oF;->NONE:LX/2oF;

    iput-object v0, p0, LX/2oE;->g:LX/2oF;

    .line 465791
    return-void
.end method


# virtual methods
.method public final a(LX/097;)LX/2oE;
    .locals 0

    .prologue
    .line 465786
    iput-object p1, p0, LX/2oE;->e:LX/097;

    .line 465787
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/2oE;
    .locals 0

    .prologue
    .line 465784
    iput-object p1, p0, LX/2oE;->a:Landroid/net/Uri;

    .line 465785
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/2oE;
    .locals 0

    .prologue
    .line 465792
    iput-object p1, p0, LX/2oE;->d:Ljava/lang/String;

    .line 465793
    return-object p0
.end method

.method public final b(Landroid/net/Uri;)LX/2oE;
    .locals 0

    .prologue
    .line 465782
    iput-object p1, p0, LX/2oE;->b:Landroid/net/Uri;

    .line 465783
    return-object p0
.end method

.method public final c(Landroid/net/Uri;)LX/2oE;
    .locals 0

    .prologue
    .line 465780
    iput-object p1, p0, LX/2oE;->c:Landroid/net/Uri;

    .line 465781
    return-object p0
.end method

.method public final h()Lcom/facebook/video/engine/VideoDataSource;
    .locals 1

    .prologue
    .line 465779
    new-instance v0, Lcom/facebook/video/engine/VideoDataSource;

    invoke-direct {v0, p0}, Lcom/facebook/video/engine/VideoDataSource;-><init>(LX/2oE;)V

    return-object v0
.end method
