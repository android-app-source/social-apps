.class public LX/2OC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:Lcom/facebook/compactdisk/StoreManagerFactory;

.field public final b:Lcom/facebook/compactdisk/DiskCache;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/FHa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400666
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2OC;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/0Ot;)V
    .locals 6
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/compactdisk/StoreManagerFactory;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400656
    iput-object p1, p0, LX/2OC;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 400657
    iput-object p2, p0, LX/2OC;->c:LX/0Ot;

    .line 400658
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    const-string v1, "two_phase_states_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    new-instance v1, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v1}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    new-instance v2, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    const-wide/32 v4, 0x3f480

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v1

    new-instance v2, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    const-wide/32 v4, 0x6400000

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    const-wide/32 v4, 0x1400000

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/EvictionConfig;->lowSpaceMaxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    .line 400659
    const/4 v1, 0x0

    .line 400660
    :try_start_0
    iget-object v2, p0, LX/2OC;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-virtual {v2, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 400661
    :goto_0
    iput-object v0, p0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    .line 400662
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    .line 400663
    return-void

    .line 400664
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 400665
    iget-object v0, p0, LX/2OC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "Failed to initialize DiskCache for PhaseTwoUploadCache"

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2OC;
    .locals 8

    .prologue
    .line 400626
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 400627
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 400628
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 400629
    if-nez v1, :cond_0

    .line 400630
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400631
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 400632
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 400633
    sget-object v1, LX/2OC;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 400634
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 400635
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 400636
    :cond_1
    if-nez v1, :cond_4

    .line 400637
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 400638
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 400639
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 400640
    new-instance v7, LX/2OC;

    invoke-static {v0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/StoreManagerFactory;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v7, v1, p0}, LX/2OC;-><init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/0Ot;)V

    .line 400641
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400642
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 400643
    if-nez v1, :cond_2

    .line 400644
    sget-object v0, LX/2OC;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2OC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 400645
    :goto_1
    if-eqz v0, :cond_3

    .line 400646
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400647
    :goto_3
    check-cast v0, LX/2OC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 400648
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 400649
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 400650
    :catchall_1
    move-exception v0

    .line 400651
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 400652
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 400653
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 400654
    :cond_2
    :try_start_8
    sget-object v0, LX/2OC;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2OC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/2OC;)V
    .locals 3

    .prologue
    .line 400616
    iget-object v0, p0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    if-nez v0, :cond_0

    .line 400617
    :goto_0
    return-void

    .line 400618
    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 400619
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 400620
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 400621
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 400622
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    move-object v0, v1

    .line 400623
    iget-object v1, p0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    const-string v2, "phase_two_states_key"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 400624
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 400625
    iget-object v0, p0, LX/2OC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "two_phase_state_serialization_failed"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(LX/2OC;)V
    .locals 1

    .prologue
    .line 400612
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 400613
    iget-object v0, p0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    if-eqz v0, :cond_0

    .line 400614
    iget-object v0, p0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->clear()V

    .line 400615
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 400667
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    new-instance v1, LX/FHa;

    invoke-direct {v1, p1, p2}, LX/FHa;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400668
    invoke-static {p0}, LX/2OC;->b(LX/2OC;)V

    .line 400669
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 400609
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400610
    invoke-static {p0}, LX/2OC;->b(LX/2OC;)V

    .line 400611
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 400604
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400605
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHa;

    .line 400606
    iget-object p1, v0, LX/FHa;->retryCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 400607
    invoke-static {p0}, LX/2OC;->b(LX/2OC;)V

    .line 400608
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 400599
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 400600
    const/4 v0, -0x1

    .line 400601
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2OC;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHa;

    .line 400602
    iget-object p0, v0, LX/FHa;->retryCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result p0

    move v0, p0

    .line 400603
    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 400597
    invoke-static {p0}, LX/2OC;->c(LX/2OC;)V

    .line 400598
    return-void
.end method
