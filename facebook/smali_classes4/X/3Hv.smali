.class public final LX/3Hv;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/31H;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/31H;)V
    .locals 1

    .prologue
    .line 545219
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 545220
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3Hv;->a:Ljava/lang/ref/WeakReference;

    .line 545221
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 545222
    iget-object v0, p0, LX/3Hv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31H;

    .line 545223
    if-nez v0, :cond_0

    .line 545224
    :goto_0
    return-void

    .line 545225
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 545226
    :pswitch_0
    iget-object v2, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    if-nez v2, :cond_2

    .line 545227
    :cond_1
    :goto_1
    goto :goto_0

    .line 545228
    :cond_2
    iget-object v2, v0, LX/31H;->f:LX/3Hv;

    const/4 v3, 0x1

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, LX/3Hv;->sendEmptyMessageDelayed(IJ)Z

    .line 545229
    iget v2, v0, LX/31H;->o:I

    iget-object v3, v0, LX/31H;->a:LX/3Hu;

    iget v3, v3, LX/3Hu;->c:I

    const/4 v4, 0x0

    .line 545230
    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    if-eqz v5, :cond_3

    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    .line 545231
    iget-object v6, v5, LX/2pb;->y:LX/2qV;

    move-object v5, v6

    .line 545232
    sget-object v6, LX/2qV;->PLAYING:LX/2qV;

    if-eq v5, v6, :cond_4

    .line 545233
    :cond_3
    :goto_2
    move v2, v4

    .line 545234
    if-eqz v2, :cond_1

    iget-object v2, v0, LX/31H;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, v0, LX/31H;->s:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 545235
    iget-object v2, v0, LX/31H;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, v0, LX/31H;->s:Ljava/lang/String;

    iget-object v4, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->j()I

    move-result v4

    iget-object v5, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->k()I

    move-result v5

    const-string v6, "NONLIVE_POST_ROLL"

    iget-object v7, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->m()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;IILjava/lang/String;I)V

    goto :goto_1

    .line 545236
    :cond_4
    iget-object v5, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->h()I

    move-result v5

    .line 545237
    sub-int/2addr v5, v2

    .line 545238
    if-ltz v5, :cond_3

    if-ge v5, v3, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
