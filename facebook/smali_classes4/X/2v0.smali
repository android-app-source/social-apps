.class public LX/2v0;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/3he;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477165
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 477166
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;FFFLX/3hc;LX/3hd;)LX/3he;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ":",
            "LX/1Po;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/1Pr;",
            ":",
            "LX/1Ps;",
            ":",
            "LX/1Pt;",
            ":",
            "LX/1Pn;",
            ">(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;FFF",
            "LX/3hc;",
            "LX/3hd;",
            ")",
            "LX/3he",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 477167
    new-instance v0, LX/3he;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    invoke-static/range {p0 .. p0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v8

    check-cast v8, LX/3hW;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v12

    check-cast v12, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/3ho;->a(LX/0QB;)LX/3ho;

    move-result-object v13

    check-cast v13, LX/3ho;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v14

    check-cast v14, LX/0So;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0ti;->a(LX/0QB;)LX/0ti;

    move-result-object v16

    check-cast v16, LX/0ti;

    invoke-static/range {p0 .. p0}, LX/2di;->a(LX/0QB;)LX/2di;

    move-result-object v17

    check-cast v17, LX/2di;

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v17}, LX/3he;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;FFFLX/3hc;LX/3hd;Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;LX/3hW;Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;LX/0Zb;Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;LX/17Q;LX/3ho;LX/0So;Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;LX/0ti;LX/2di;)V

    .line 477168
    return-object v0
.end method
