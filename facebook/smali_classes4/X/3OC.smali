.class public final LX/3OC;
.super LX/13D;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile b:LX/3OC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 560167
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->DIVEBAR_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3OC;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/13J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 560168
    invoke-direct {p0, p1}, LX/13D;-><init>(LX/13J;)V

    .line 560169
    return-void
.end method

.method public static a(LX/0QB;)LX/3OC;
    .locals 4

    .prologue
    .line 560170
    sget-object v0, LX/3OC;->b:LX/3OC;

    if-nez v0, :cond_1

    .line 560171
    const-class v1, LX/3OC;

    monitor-enter v1

    .line 560172
    :try_start_0
    sget-object v0, LX/3OC;->b:LX/3OC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 560173
    if-eqz v2, :cond_0

    .line 560174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 560175
    new-instance p0, LX/3OC;

    const-class v3, LX/13J;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13J;

    invoke-direct {p0, v3}, LX/3OC;-><init>(LX/13J;)V

    .line 560176
    move-object v0, p0

    .line 560177
    sput-object v0, LX/3OC;->b:LX/3OC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 560179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 560180
    :cond_1
    sget-object v0, LX/3OC;->b:LX/3OC;

    return-object v0

    .line 560181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 560182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 560183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560184
    const-string v0, "1824"

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 560185
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560186
    const-string v0, "Divebar"

    return-object v0
.end method

.method public final l()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560187
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->DIVEBAR_HEADER_MEDIUM:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
