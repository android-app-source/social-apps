.class public LX/2kE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/MappedByteBuffer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "sModelFiles"
    .end annotation
.end field

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "sModelClasses"
    .end annotation
.end field


# instance fields
.field private final c:Ljava/io/File;

.field private final d:LX/15j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 455504
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/2kE;->a:Ljava/util/HashMap;

    .line 455505
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/2kE;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;LX/15j;)V
    .locals 1

    .prologue
    .line 455500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455501
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/2kE;->c:Ljava/io/File;

    .line 455502
    iput-object p2, p0, LX/2kE;->d:LX/15j;

    .line 455503
    return-void
.end method

.method public static a(LX/2kE;[BLjava/lang/String;)LX/0w5;
    .locals 2

    .prologue
    .line 455493
    if-eqz p1, :cond_0

    .line 455494
    :try_start_0
    invoke-static {p1}, LX/0w5;->a([B)LX/0w5;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 455495
    :goto_0
    return-object v0

    .line 455496
    :catch_0
    move-exception v0

    .line 455497
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2kE;->a(Ljava/lang/Exception;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 455498
    :cond_0
    invoke-static {p2}, LX/2kE;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 455499
    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Exception;Ljava/lang/String;)Ljava/io/IOException;
    .locals 4

    .prologue
    .line 455492
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to resolve Flattenable subclass with name \'%s\'"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3

    .prologue
    .line 455506
    sget-object v1, LX/2kE;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 455507
    :try_start_0
    sget-object v0, LX/2kE;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 455508
    monitor-exit v1

    .line 455509
    if-eqz v0, :cond_0

    .line 455510
    :goto_0
    return-object v0

    .line 455511
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 455512
    :cond_0
    :try_start_1
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 455513
    sget-object v1, LX/2kE;->b:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 455514
    :try_start_2
    sget-object v2, LX/2kE;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455515
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_1

    .line 455516
    :catch_0
    move-exception v0

    .line 455517
    :goto_1
    invoke-static {v0, p0}, LX/2kE;->a(Ljava/lang/Exception;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 455518
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 455484
    sget-object v1, LX/2kE;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 455485
    :try_start_0
    sget-object v0, LX/2kE;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 455486
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455487
    sget-object v1, LX/2kE;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 455488
    :try_start_1
    sget-object v0, LX/2kE;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 455489
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 455490
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 455491
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/2kE;Ljava/lang/String;)Ljava/nio/MappedByteBuffer;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 455446
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455447
    sget-object v1, LX/2kE;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 455448
    :try_start_0
    sget-object v0, LX/2kE;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/MappedByteBuffer;

    .line 455449
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455450
    if-eqz v0, :cond_0

    .line 455451
    invoke-virtual {v0}, Ljava/nio/MappedByteBuffer;->isLoaded()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 455452
    :goto_0
    return-object v0

    .line 455453
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 455454
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/2kE;->c:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 455455
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 455456
    :try_start_2
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 455457
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result-object v0

    .line 455458
    :try_start_4
    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v1

    .line 455459
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/MappedByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 455460
    invoke-static {v1}, LX/1lJ;->a(Ljava/nio/ByteBuffer;)I

    .line 455461
    sget-object v2, LX/2kE;->a:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 455462
    :try_start_5
    sget-object v3, LX/2kE;->a:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455463
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 455464
    if-eqz v0, :cond_1

    :try_start_6
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 455465
    :cond_1
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    move-object v0, v1

    goto :goto_0

    .line 455466
    :catchall_1
    move-exception v1

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v1
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 455467
    :catch_0
    move-exception v1

    :try_start_a
    throw v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 455468
    :catchall_2
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v2, :cond_3

    :try_start_b
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :cond_2
    :goto_2
    :try_start_c
    throw v1
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 455469
    :catch_1
    move-exception v0

    :try_start_d
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 455470
    :catchall_3
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    :goto_3
    if-eqz v6, :cond_4

    :try_start_e
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2

    :goto_4
    :try_start_f
    throw v0
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2

    :catch_2
    move-exception v0

    .line 455471
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error mapping model file \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 455472
    :catch_3
    move-exception v0

    :try_start_10
    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 455473
    :catchall_4
    move-exception v0

    goto :goto_3

    .line 455474
    :cond_3
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    goto :goto_2

    .line 455475
    :catch_4
    move-exception v1

    :try_start_11
    invoke-static {v6, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_2

    goto :goto_4

    .line 455476
    :catchall_5
    move-exception v1

    move-object v2, v6

    goto :goto_1
.end method


# virtual methods
.method public final a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(I",
            "Ljava/lang/String;",
            "LX/0w5;",
            "[B)TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 455478
    invoke-static {p0, p2}, LX/2kE;->b(LX/2kE;Ljava/lang/String;)Ljava/nio/MappedByteBuffer;

    move-result-object v1

    .line 455479
    if-eqz p4, :cond_0

    .line 455480
    invoke-static {p4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 455481
    :goto_0
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    iget-object v5, p0, LX/2kE;->d:LX/15j;

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 455482
    const-string v1, "ModelReader"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 455483
    invoke-virtual {p3, v0, p1}, LX/0w5;->a(LX/15i;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    return-object v0

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;[BLjava/lang/String;[B)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(I",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            "[B)TT;"
        }
    .end annotation

    .prologue
    .line 455477
    invoke-static {p0, p3, p4}, LX/2kE;->a(LX/2kE;[BLjava/lang/String;)LX/0w5;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p5}, LX/2kE;->a(ILjava/lang/String;LX/0w5;[B)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method
