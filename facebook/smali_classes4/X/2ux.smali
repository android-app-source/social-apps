.class public final enum LX/2ux;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ux;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ux;

.field public static final enum ENCRYPTED:LX/2ux;

.field public static final enum FIXED_KEY:LX/2ux;

.field public static final enum FIXED_KEY_128:LX/2ux;

.field public static final enum NONE:LX/2ux;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 477123
    new-instance v0, LX/2ux;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/2ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ux;->NONE:LX/2ux;

    .line 477124
    new-instance v0, LX/2ux;

    const-string v1, "ENCRYPTED"

    invoke-direct {v0, v1, v3}, LX/2ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ux;->ENCRYPTED:LX/2ux;

    .line 477125
    new-instance v0, LX/2ux;

    const-string v1, "FIXED_KEY"

    invoke-direct {v0, v1, v4}, LX/2ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ux;->FIXED_KEY:LX/2ux;

    .line 477126
    new-instance v0, LX/2ux;

    const-string v1, "FIXED_KEY_128"

    invoke-direct {v0, v1, v5}, LX/2ux;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ux;->FIXED_KEY_128:LX/2ux;

    .line 477127
    const/4 v0, 0x4

    new-array v0, v0, [LX/2ux;

    sget-object v1, LX/2ux;->NONE:LX/2ux;

    aput-object v1, v0, v2

    sget-object v1, LX/2ux;->ENCRYPTED:LX/2ux;

    aput-object v1, v0, v3

    sget-object v1, LX/2ux;->FIXED_KEY:LX/2ux;

    aput-object v1, v0, v4

    sget-object v1, LX/2ux;->FIXED_KEY_128:LX/2ux;

    aput-object v1, v0, v5

    sput-object v0, LX/2ux;->$VALUES:[LX/2ux;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 477128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ux;
    .locals 1

    .prologue
    .line 477129
    const-class v0, LX/2ux;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ux;

    return-object v0
.end method

.method public static values()[LX/2ux;
    .locals 1

    .prologue
    .line 477130
    sget-object v0, LX/2ux;->$VALUES:[LX/2ux;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ux;

    return-object v0
.end method
