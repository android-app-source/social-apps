.class public LX/2u6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private c:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 475506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475507
    iput-object p2, p0, LX/2u6;->a:Ljava/lang/String;

    .line 475508
    iput-object p1, p0, LX/2u6;->b:Ljava/lang/String;

    .line 475509
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/2u6;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 475510
    return-void
.end method


# virtual methods
.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 475511
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2u6;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
