.class public LX/2Tb;
.super LX/1M0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/1M0",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final b:LX/0Xu;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 414495
    invoke-direct {p0}, LX/1M0;-><init>()V

    .line 414496
    iput-object p1, p0, LX/2Tb;->b:LX/0Xu;

    .line 414497
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414493
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 414494
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 414473
    const-string v0, "occurrences"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 414474
    if-nez p2, :cond_0

    .line 414475
    invoke-virtual {p0, p1}, LX/2Tb;->a(Ljava/lang/Object;)I

    move-result v0

    .line 414476
    :goto_0
    return v0

    .line 414477
    :cond_0
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 414478
    if-nez v0, :cond_1

    move v0, v1

    .line 414479
    goto :goto_0

    .line 414480
    :cond_1
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    .line 414481
    if-lt p2, v2, :cond_3

    .line 414482
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    :cond_2
    move v0, v2

    .line 414483
    goto :goto_0

    .line 414484
    :cond_3
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 414485
    :goto_1
    if-ge v1, p2, :cond_2

    .line 414486
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 414487
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 414488
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final b()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 414492
    new-instance v0, LX/2Tf;

    iget-object v1, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v1}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/2Tf;-><init>(LX/2Tb;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 414491
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 414498
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->g()V

    .line 414499
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414490
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 414489
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 414472
    new-instance v0, LX/2Te;

    invoke-direct {v0, p0}, LX/2Te;-><init>(LX/2Tb;)V

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 414471
    iget-object v0, p0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
