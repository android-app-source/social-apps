.class public LX/30K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/30K;


# instance fields
.field private a:[LX/30L;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484294
    const/16 v0, 0xc

    new-array v0, v0, [LX/30L;

    iput-object v0, p0, LX/30K;->a:[LX/30L;

    .line 484295
    const/4 v0, 0x0

    iput v0, p0, LX/30K;->b:I

    .line 484296
    return-void
.end method

.method public static a(LX/0QB;)LX/30K;
    .locals 3

    .prologue
    .line 484297
    sget-object v0, LX/30K;->c:LX/30K;

    if-nez v0, :cond_1

    .line 484298
    const-class v1, LX/30K;

    monitor-enter v1

    .line 484299
    :try_start_0
    sget-object v0, LX/30K;->c:LX/30K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 484300
    if-eqz v2, :cond_0

    .line 484301
    :try_start_1
    new-instance v0, LX/30K;

    invoke-direct {v0}, LX/30K;-><init>()V

    .line 484302
    move-object v0, v0

    .line 484303
    sput-object v0, LX/30K;->c:LX/30K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 484304
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 484305
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 484306
    :cond_1
    sget-object v0, LX/30K;->c:LX/30K;

    return-object v0

    .line 484307
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 484308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/30L;)V
    .locals 3

    .prologue
    .line 484309
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/30K;->a:[LX/30L;

    iget v1, p0, LX/30K;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/30K;->b:I

    rem-int/lit8 v1, v1, 0xc

    aput-object p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484310
    monitor-exit p0

    return-void

    .line 484311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()[LX/30L;
    .locals 2

    .prologue
    .line 484312
    const/16 v0, 0xc

    new-array v1, v0, [LX/30L;

    .line 484313
    monitor-enter p0

    .line 484314
    :try_start_0
    iget v0, p0, LX/30K;->b:I

    if-nez v0, :cond_0

    .line 484315
    const/4 v0, 0x0

    monitor-exit p0

    .line 484316
    :goto_0
    return-object v0

    .line 484317
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/30K;->b:I

    .line 484318
    iget-object v0, p0, LX/30K;->a:[LX/30L;

    .line 484319
    iput-object v1, p0, LX/30K;->a:[LX/30L;

    .line 484320
    monitor-exit p0

    goto :goto_0

    .line 484321
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
