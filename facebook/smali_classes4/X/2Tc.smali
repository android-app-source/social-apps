.class public final LX/2Tc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field private static final a:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414530
    new-instance v0, LX/2Td;

    invoke-direct {v0}, LX/2Td;-><init>()V

    sput-object v0, LX/2Tc;->a:LX/1sm;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 414529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 414526
    instance-of v0, p0, LX/1M1;

    if-eqz v0, :cond_0

    .line 414527
    check-cast p0, LX/1M1;

    invoke-interface {p0}, LX/1M1;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 414528
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xb

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;I)LX/4wx;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;I)",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 414525
    new-instance v0, LX/50E;

    invoke-direct {v0, p0, p1}, LX/50E;-><init>(Ljava/lang/Object;I)V

    return-object v0
.end method

.method public static a(LX/1M1;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1M1",
            "<TE;>;)",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 414531
    new-instance v0, LX/50F;

    invoke-interface {p0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/50F;-><init>(LX/1M1;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public static a(LX/1M1;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<*>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 414514
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 414515
    :goto_0
    return v0

    .line 414516
    :cond_0
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_5

    .line 414517
    check-cast p1, LX/1M1;

    .line 414518
    invoke-interface {p0}, LX/1M1;->size()I

    move-result v0

    invoke-interface {p1}, LX/1M1;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-interface {p0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v0, v2

    .line 414519
    goto :goto_0

    .line 414520
    :cond_2
    invoke-interface {p1}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 414521
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    if-eq v4, v0, :cond_3

    move v0, v2

    .line 414522
    goto :goto_0

    :cond_4
    move v0, v1

    .line 414523
    goto :goto_0

    :cond_5
    move v0, v2

    .line 414524
    goto :goto_0
.end method

.method public static a(LX/1M1;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1M1",
            "<TE;>;",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 414505
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414506
    const/4 v0, 0x0

    .line 414507
    :goto_0
    return v0

    .line 414508
    :cond_0
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_1

    .line 414509
    check-cast p1, LX/1M1;

    move-object v0, p1

    .line 414510
    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 414511
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, LX/1M1;->a(Ljava/lang/Object;I)I

    goto :goto_1

    .line 414512
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 414513
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/1M1;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 414503
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_0

    check-cast p1, LX/1M1;

    invoke-interface {p1}, LX/1M1;->d()Ljava/util/Set;

    move-result-object p1

    .line 414504
    :cond_0
    invoke-interface {p0}, LX/1M1;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public static c(LX/1M1;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 414500
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 414501
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_0

    check-cast p1, LX/1M1;

    invoke-interface {p1}, LX/1M1;->d()Ljava/util/Set;

    move-result-object p1

    .line 414502
    :cond_0
    invoke-interface {p0}, LX/1M1;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
