.class public final LX/2c2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/336;


# direct methods
.method public constructor <init>(LX/336;)V
    .locals 0

    .prologue
    .line 440000
    iput-object p1, p0, LX/2c2;->a:LX/336;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440001
    iget-object v0, p0, LX/2c2;->a:LX/336;

    iget-object v0, v0, LX/336;->a:LX/338;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchViewerLoginAudienceInfo"

    .line 440002
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 440003
    move-object v0, v0

    .line 440004
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 440005
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440006
    const-string v0, "fetchViewerLoginAudienceInfo"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;

    .line 440007
    if-nez v0, :cond_0

    .line 440008
    :goto_0
    return-void

    .line 440009
    :cond_0
    iget-object v1, p0, LX/2c2;->a:LX/336;

    iget-object v1, v1, LX/336;->b:LX/339;

    invoke-virtual {v0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$FetchAudienceInfoModel;->a()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/339;->a(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V

    goto :goto_0
.end method
