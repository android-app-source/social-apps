.class public LX/2kw;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Vf;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/0Vf;",
        "LX/1OO",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:LX/1DS;

.field private final e:LX/1Db;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field public h:LX/1Rq;

.field public i:LX/2kx;

.field public j:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 456661
    const v0, 0x7f0d01d7

    sput v0, LX/2kw;->a:I

    .line 456662
    const v0, 0x7f0d01d6

    sput v0, LX/2kw;->b:I

    .line 456663
    const v0, 0x7f0d01d8

    sput v0, LX/2kw;->c:I

    return-void
.end method

.method public constructor <init>(LX/1PW;LX/1DZ;LX/0g1;LX/1DS;LX/1Db;LX/0Ot;)V
    .locals 2
    .param p1    # LX/1PW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1DZ;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0g1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1PW;",
            "LX/1DZ",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;",
            "LX/0g1;",
            "LX/1DS;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456646
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 456647
    iput-object p4, p0, LX/2kw;->d:LX/1DS;

    .line 456648
    iput-object p5, p0, LX/2kw;->e:LX/1Db;

    .line 456649
    iput-object p6, p0, LX/2kw;->f:LX/0Ot;

    .line 456650
    sget-object v0, LX/2kx;->LOADING:LX/2kx;

    iput-object v0, p0, LX/2kw;->i:LX/2kx;

    .line 456651
    iget-object v0, p0, LX/2kw;->d:LX/1DS;

    iget-object v1, p0, LX/2kw;->f:LX/0Ot;

    invoke-virtual {v0, v1, p3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 456652
    iput-object p1, v0, LX/1Ql;->f:LX/1PW;

    .line 456653
    move-object v0, v0

    .line 456654
    iput-object p2, v0, LX/1Ql;->e:LX/1DZ;

    .line 456655
    move-object v0, v0

    .line 456656
    invoke-virtual {v0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v0

    iput-object v0, p0, LX/2kw;->h:LX/1Rq;

    .line 456657
    instance-of v0, p3, LX/0g0;

    iput-boolean v0, p0, LX/2kw;->g:Z

    .line 456658
    iget-boolean v0, p0, LX/2kw;->g:Z

    if-eqz v0, :cond_0

    .line 456659
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    new-instance v1, LX/2ky;

    invoke-direct {v1, p0}, LX/2ky;-><init>(LX/2kw;)V

    invoke-interface {v0, v1}, LX/1OQ;->a(LX/1OD;)V

    .line 456660
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 456631
    sget v0, LX/2kw;->a:I

    if-ne p2, v0, :cond_0

    .line 456632
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03150c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 456633
    new-instance v0, LX/2ll;

    invoke-direct {v0, v1}, LX/2ll;-><init>(Landroid/view/View;)V

    .line 456634
    :goto_0
    return-object v0

    .line 456635
    :cond_0
    sget v0, LX/2kw;->c:I

    if-ne p2, v0, :cond_2

    .line 456636
    new-instance v0, LX/2ll;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 456637
    const v2, 0x7f03150b

    const/4 p1, 0x0

    invoke-static {v1, v2, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 456638
    const v2, 0x7f0d19d4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const p2, 0x7f08006f

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 456639
    iget-object v2, p0, LX/2kw;->j:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1

    .line 456640
    iget-object v2, p0, LX/2kw;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 456641
    :cond_1
    move-object v1, p1

    .line 456642
    invoke-direct {v0, v1}, LX/2ll;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 456643
    :cond_2
    sget v0, LX/2kw;->b:I

    if-ne p2, v0, :cond_3

    .line 456644
    new-instance v0, LX/2ll;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/2ll;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 456645
    :cond_3
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0, p1, p2}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 456628
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 456629
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 456630
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 456625
    instance-of v0, p1, LX/2ll;

    if-eqz v0, :cond_0

    .line 456626
    :goto_0
    return-void

    .line 456627
    :cond_0
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0, p1, p2}, LX/1OQ;->a(LX/1a1;I)V

    goto :goto_0
.end method

.method public final a(LX/2kx;)V
    .locals 1

    .prologue
    .line 456622
    iput-object p1, p0, LX/2kw;->i:LX/2kx;

    .line 456623
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 456624
    return-void
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 456664
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 456665
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 456617
    iget-boolean v0, p0, LX/2kw;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 456618
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 456619
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 456620
    return-void

    .line 456621
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 456614
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 456615
    const/4 v0, 0x0

    .line 456616
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 456606
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 456607
    iget-object v0, p0, LX/2kw;->i:LX/2kx;

    sget-object v1, LX/2kx;->LOADING:LX/2kx;

    if-ne v0, v1, :cond_0

    .line 456608
    sget v0, LX/2kw;->a:I

    .line 456609
    :goto_0
    return v0

    .line 456610
    :cond_0
    iget-object v0, p0, LX/2kw;->i:LX/2kx;

    sget-object v1, LX/2kx;->FAILED:LX/2kx;

    if-ne v0, v1, :cond_1

    .line 456611
    sget v0, LX/2kw;->c:I

    goto :goto_0

    .line 456612
    :cond_1
    sget v0, LX/2kw;->b:I

    goto :goto_0

    .line 456613
    :cond_2
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OP;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 456603
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/1OO;->ii_()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 456605
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 456604
    iget-object v0, p0, LX/2kw;->h:LX/1Rq;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    return v0
.end method
