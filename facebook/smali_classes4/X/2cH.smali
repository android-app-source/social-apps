.class public LX/2cH;
.super LX/2h2;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h2;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;",
        "Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440608
    const-class v0, LX/2cH;

    sput-object v0, LX/2cH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440609
    invoke-direct {p0}, LX/2h2;-><init>()V

    .line 440610
    iput-object p1, p0, LX/2cH;->b:LX/0lC;

    .line 440611
    return-void
.end method

.method public static b(LX/0QB;)LX/2cH;
    .locals 2

    .prologue
    .line 440612
    new-instance v1, LX/2cH;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-direct {v1, v0}, LX/2cH;-><init>(LX/0lC;)V

    .line 440613
    return-object v1
.end method

.method private static b(Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440614
    invoke-static {p0}, LX/2h2;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;

    move-result-object v1

    .line 440615
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    .line 440616
    iget-object v3, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 440617
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440618
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "force_refresh"

    .line 440619
    iget-boolean v0, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    move v0, v0

    .line 440620
    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440621
    return-object v1

    .line 440622
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 440623
    check-cast p1, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    .line 440624
    invoke-static {p1}, LX/2cH;->b(Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;)Ljava/util/List;

    move-result-object v4

    .line 440625
    new-instance v0, LX/14N;

    const-string v1, "fetchZeroHeaderRequest"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroHeaderRequest"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 440626
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 440627
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 440628
    iget-object v1, p0, LX/2cH;->b:LX/0lC;

    iget-object v2, p0, LX/2cH;->b:LX/0lC;

    invoke-virtual {v0, v2}, LX/0lF;->a(LX/0lD;)LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/2cH;->b:LX/0lC;

    .line 440629
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 440630
    const-class v3, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;

    .line 440631
    return-object v0
.end method
