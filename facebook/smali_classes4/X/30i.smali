.class public LX/30i;
.super LX/0Tw;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/30i;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 485109
    const-string v0, "new_ccu_upload"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0Tw;-><init>(Ljava/lang/String;I)V

    .line 485110
    return-void
.end method

.method public static a(LX/0QB;)LX/30i;
    .locals 3

    .prologue
    .line 485111
    sget-object v0, LX/30i;->a:LX/30i;

    if-nez v0, :cond_1

    .line 485112
    const-class v1, LX/30i;

    monitor-enter v1

    .line 485113
    :try_start_0
    sget-object v0, LX/30i;->a:LX/30i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 485114
    if-eqz v2, :cond_0

    .line 485115
    :try_start_1
    new-instance v0, LX/30i;

    invoke-direct {v0}, LX/30i;-><init>()V

    .line 485116
    move-object v0, v0

    .line 485117
    sput-object v0, LX/30i;->a:LX/30i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 485118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 485119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 485120
    :cond_1
    sget-object v0, LX/30i;->a:LX/30i;

    return-object v0

    .line 485121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 485122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 485123
    const-string v0, "CREATE TABLE contacts_upload_snapshot (local_contact_id INTEGER PRIMARY KEY, contact_hash TEXT)"

    const v1, -0x50256b89

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xc9ded92

    invoke-static {v0}, LX/03h;->a(I)V

    .line 485124
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 485125
    const-string v0, "DROP TABLE IF EXISTS contacts_upload_snapshot"

    const v1, -0x1ce832bb

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x39176509

    invoke-static {v0}, LX/03h;->a(I)V

    .line 485126
    invoke-virtual {p0, p1}, LX/30i;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 485127
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 485128
    const-string v0, "contacts_upload_snapshot"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 485129
    return-void
.end method
