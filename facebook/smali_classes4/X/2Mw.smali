.class public LX/2Mw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Xp;

.field private final e:LX/0WJ;

.field public final f:LX/2Mx;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 397890
    const-class v0, LX/2Mw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Mw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0Xp;LX/0WJ;LX/2Mx;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397892
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 397893
    iput-object v0, p0, LX/2Mw;->g:LX/0Ot;

    .line 397894
    iput-object p1, p0, LX/2Mw;->b:Ljava/util/concurrent/Executor;

    .line 397895
    iput-object p2, p0, LX/2Mw;->c:Ljava/util/concurrent/Executor;

    .line 397896
    iput-object p3, p0, LX/2Mw;->d:LX/0Xp;

    .line 397897
    iput-object p4, p0, LX/2Mw;->e:LX/0WJ;

    .line 397898
    iput-object p5, p0, LX/2Mw;->f:LX/2Mx;

    .line 397899
    return-void
.end method

.method public static b(LX/0QB;)LX/2Mw;
    .locals 9

    .prologue
    .line 397900
    new-instance v0, LX/2Mw;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v3

    check-cast v3, LX/0Xp;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    .line 397901
    new-instance v8, LX/2Mx;

    invoke-direct {v8}, LX/2Mx;-><init>()V

    .line 397902
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v7

    check-cast v7, LX/2My;

    .line 397903
    iput-object v5, v8, LX/2Mx;->a:LX/0SG;

    iput-object v6, v8, LX/2Mx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v7, v8, LX/2Mx;->c:LX/2My;

    .line 397904
    move-object v5, v8

    .line 397905
    check-cast v5, LX/2Mx;

    invoke-direct/range {v0 .. v5}, LX/2Mw;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/0Xp;LX/0WJ;LX/2Mx;)V

    .line 397906
    const/16 v1, 0xafd

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 397907
    iput-object v1, v0, LX/2Mw;->g:LX/0Ot;

    .line 397908
    return-object v0
.end method

.method public static c(LX/2Mw;)J
    .locals 4

    .prologue
    .line 397909
    iget-object v0, p0, LX/2Mw;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/2Mw;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 397910
    iget-wide v2, v0, Lcom/facebook/user/model/User;->N:J

    move-wide v0, v2

    .line 397911
    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 397912
    invoke-static {p0}, LX/2Mw;->c(LX/2Mw;)J

    move-result-wide v2

    .line 397913
    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    cmp-long v0, v2, p1

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 397914
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 397915
    iget-object v0, p0, LX/2Mw;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 397916
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    invoke-virtual {v1, v0}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v0

    .line 397917
    iput-wide p1, v0, LX/0XI;->X:J

    .line 397918
    iget-object v1, p0, LX/2Mw;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0WJ;->a(Lcom/facebook/user/model/User;)V

    .line 397919
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->I:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 397920
    iget-object v1, p0, LX/2Mw;->d:LX/0Xp;

    invoke-virtual {v1, v0}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 397921
    :cond_1
    return-void

    .line 397922
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
