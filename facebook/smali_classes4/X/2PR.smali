.class public LX/2PR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field private static volatile t:LX/2PR;


# instance fields
.field private final c:LX/0c8;

.field private final d:LX/0Xl;

.field public final e:Landroid/os/Handler;

.field public final f:LX/0xB;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Uh;

.field public final j:Landroid/content/Context;

.field private final k:LX/0iA;

.field public final l:LX/2PS;

.field public final m:LX/0SG;

.field public final n:Ljava/lang/Runnable;

.field private o:LX/0cJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:LX/0Yd;

.field public r:Z

.field public s:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 406579
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "inboxjewelfetchcount"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2PR;->a:LX/0Tn;

    .line 406580
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "badgeable_qp_last_seen_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2PR;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0c8;LX/0Xl;Landroid/os/Handler;LX/0xB;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;Landroid/content/Context;LX/0iA;LX/2PS;LX/0SG;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0c8;",
            "LX/0Xl;",
            "Landroid/os/Handler;",
            "LX/0xB;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/content/Context;",
            "LX/0iA;",
            "LX/2PS;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406566
    new-instance v0, Lcom/facebook/notifications/common/DiodeBadgeSyncManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/common/DiodeBadgeSyncManager$1;-><init>(LX/2PR;)V

    iput-object v0, p0, LX/2PR;->n:Ljava/lang/Runnable;

    .line 406567
    iput-object p1, p0, LX/2PR;->c:LX/0c8;

    .line 406568
    iput-object p2, p0, LX/2PR;->d:LX/0Xl;

    .line 406569
    iput-object p3, p0, LX/2PR;->e:Landroid/os/Handler;

    .line 406570
    iput-object p4, p0, LX/2PR;->f:LX/0xB;

    .line 406571
    iput-object p5, p0, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 406572
    iput-object p6, p0, LX/2PR;->h:LX/0Or;

    .line 406573
    iput-object p7, p0, LX/2PR;->i:LX/0Uh;

    .line 406574
    iput-object p8, p0, LX/2PR;->j:Landroid/content/Context;

    .line 406575
    iput-object p9, p0, LX/2PR;->k:LX/0iA;

    .line 406576
    iput-object p10, p0, LX/2PR;->l:LX/2PS;

    .line 406577
    iput-object p11, p0, LX/2PR;->m:LX/0SG;

    .line 406578
    return-void
.end method

.method public static a(LX/0QB;)LX/2PR;
    .locals 15

    .prologue
    .line 406549
    sget-object v0, LX/2PR;->t:LX/2PR;

    if-nez v0, :cond_1

    .line 406550
    const-class v1, LX/2PR;

    monitor-enter v1

    .line 406551
    :try_start_0
    sget-object v0, LX/2PR;->t:LX/2PR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406552
    if-eqz v2, :cond_0

    .line 406553
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406554
    new-instance v3, LX/2PR;

    invoke-static {v0}, LX/0c8;->a(LX/0QB;)LX/0c8;

    move-result-object v4

    check-cast v4, LX/0c8;

    invoke-static {v0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v7

    check-cast v7, LX/0xB;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v12

    check-cast v12, LX/0iA;

    .line 406555
    new-instance v14, LX/2PS;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct {v14, v13}, LX/2PS;-><init>(LX/0ad;)V

    .line 406556
    move-object v13, v14

    .line 406557
    check-cast v13, LX/2PS;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v14

    check-cast v14, LX/0SG;

    invoke-direct/range {v3 .. v14}, LX/2PR;-><init>(LX/0c8;LX/0Xl;Landroid/os/Handler;LX/0xB;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;Landroid/content/Context;LX/0iA;LX/2PS;LX/0SG;)V

    .line 406558
    move-object v0, v3

    .line 406559
    sput-object v0, LX/2PR;->t:LX/2PR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406560
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406561
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406562
    :cond_1
    sget-object v0, LX/2PR;->t:LX/2PR;

    return-object v0

    .line 406563
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private g()LX/0cJ;
    .locals 4

    .prologue
    .line 406546
    iget-object v0, p0, LX/2PR;->o:LX/0cJ;

    if-nez v0, :cond_0

    .line 406547
    iget-object v0, p0, LX/2PR;->c:LX/0c8;

    sget-object v1, LX/2PT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/2PR;->d:LX/0Xl;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0c8;->a(Ljava/lang/String;LX/0Xl;Z)LX/0cJ;

    move-result-object v0

    iput-object v0, p0, LX/2PR;->o:LX/0cJ;

    .line 406548
    :cond_0
    iget-object v0, p0, LX/2PR;->o:LX/0cJ;

    return-object v0
.end method

.method public static declared-synchronized h(LX/2PR;)V
    .locals 1

    .prologue
    .line 406542
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2PR;->p:Z

    .line 406543
    invoke-static {p0}, LX/2PR;->i(LX/2PR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406544
    monitor-exit p0

    return-void

    .line 406545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static i(LX/2PR;)V
    .locals 4

    .prologue
    .line 406581
    iget-object v0, p0, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2PR;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 406582
    iget-object v1, p0, LX/2PR;->f:LX/0xB;

    sget-object v2, LX/12j;->INBOX:LX/12j;

    iget-boolean v3, p0, LX/2PR;->r:Z

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-virtual {v1, v2, v0}, LX/0xB;->a(LX/12j;I)V

    .line 406583
    return-void
.end method

.method public static j(LX/2PR;)V
    .locals 9

    .prologue
    .line 406534
    iget-boolean v0, p0, LX/2PR;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2PR;->l:LX/2PS;

    .line 406535
    iget-object v1, v0, LX/2PS;->a:LX/0ad;

    sget-short v2, LX/15r;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 406536
    if-eqz v0, :cond_0

    .line 406537
    iget-object v3, p0, LX/2PR;->m:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-object v5, p0, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/2PR;->b:LX/0Tn;

    const-wide/16 v7, 0x0

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-wide v5, p0, LX/2PR;->s:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 406538
    if-nez v0, :cond_1

    .line 406539
    :cond_0
    :goto_1
    return-void

    .line 406540
    :cond_1
    iget-object v0, p0, LX/2PR;->k:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB_BADGEABLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/3kR;

    invoke-virtual {v0, v1, v2}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v0

    iput-boolean v0, p0, LX/2PR;->r:Z

    .line 406541
    invoke-static {p0}, LX/2PR;->i(LX/2PR;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 406507
    invoke-direct {p0}, LX/2PR;->g()LX/0cJ;

    move-result-object v0

    invoke-interface {v0}, LX/0cJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 406508
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2PR;->p:Z

    .line 406509
    :goto_0
    return-void

    .line 406510
    :cond_0
    const/4 v0, 0x0

    sget v1, LX/2PT;->b:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 406511
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 406512
    const-string v2, "key_message_action"

    const-string v3, "action_badge_request"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406513
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 406514
    invoke-direct {p0}, LX/2PR;->g()LX/0cJ;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0cJ;->a(Landroid/os/Message;)V

    .line 406515
    iget-object v0, p0, LX/2PR;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/2PR;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    const v4, -0x63a3db5b

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 406529
    iget-boolean v0, p0, LX/2PR;->p:Z

    if-nez v0, :cond_0

    .line 406530
    iget-object v1, p0, LX/2PR;->f:LX/0xB;

    sget-object v2, LX/12j;->INBOX:LX/12j;

    iget-boolean v0, p0, LX/2PR;->r:Z

    if-eqz v0, :cond_1

    add-int/lit8 v0, p1, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0xB;->a(LX/12j;I)V

    .line 406531
    :cond_0
    iget-object v0, p0, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2PR;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 406532
    return-void

    :cond_1
    move v0, p1

    .line 406533
    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 406528
    iget-object v0, p0, LX/2PR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2PR;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final init()V
    .locals 8

    .prologue
    .line 406516
    invoke-direct {p0}, LX/2PR;->g()LX/0cJ;

    move-result-object v0

    .line 406517
    new-instance v1, LX/2PU;

    invoke-direct {v1, p0}, LX/2PU;-><init>(LX/2PR;)V

    invoke-interface {v0, v1}, LX/0cJ;->a(LX/0cN;)V

    .line 406518
    sget v1, LX/2PT;->b:I

    new-instance v2, LX/2PV;

    invoke-direct {v2, p0}, LX/2PV;-><init>(LX/2PR;)V

    invoke-interface {v0, v1, v2}, LX/0cJ;->a(ILX/0cM;)V

    .line 406519
    invoke-interface {v0}, LX/0Up;->init()V

    .line 406520
    iget-object v4, p0, LX/2PR;->l:LX/2PS;

    .line 406521
    iget-object v5, v4, LX/2PS;->a:LX/0ad;

    sget v6, LX/15r;->b:I

    const/4 v7, 0x7

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    move v4, v5

    .line 406522
    int-to-long v4, v4

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    iput-wide v4, p0, LX/2PR;->s:J

    .line 406523
    invoke-static {p0}, LX/2PR;->j(LX/2PR;)V

    .line 406524
    iget-object v4, p0, LX/2PR;->q:LX/0Yd;

    if-eqz v4, :cond_0

    .line 406525
    :goto_0
    return-void

    .line 406526
    :cond_0
    new-instance v4, LX/0Yd;

    const-string v5, "ACTION_BADGEABLE_DIODE_PROMOTION_FETCHED"

    new-instance v6, LX/2PW;

    invoke-direct {v6, p0}, LX/2PW;-><init>(LX/2PR;)V

    invoke-direct {v4, v5, v6}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v4, p0, LX/2PR;->q:LX/0Yd;

    .line 406527
    iget-object v4, p0, LX/2PR;->j:Landroid/content/Context;

    iget-object v5, p0, LX/2PR;->q:LX/0Yd;

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "ACTION_BADGEABLE_DIODE_PROMOTION_FETCHED"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method
