.class public LX/29I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 376082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376083
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/2BP;
    .locals 8

    .prologue
    .line 376084
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 376085
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 376086
    new-instance v3, Landroid/content/ComponentName;

    sget-object v4, LX/29J;->a:Ljava/lang/String;

    const-string v5, "com.facebook.oxygen.appmanager.firstparty.tos.ShouldAcceptTos"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 376087
    new-instance v4, Landroid/content/ComponentName;

    sget-object v5, LX/29J;->a:Ljava/lang/String;

    const-string v6, "com.facebook.oxygen.appmanager.firstparty.tos.ShouldShowExplicitTos"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 376088
    :try_start_0
    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v5

    .line 376089
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    .line 376090
    if-eqz v5, :cond_0

    if-nez v1, :cond_5

    .line 376091
    :cond_0
    invoke-static {p0, v3}, LX/29I;->a(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0, v4}, LX/29I;->a(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 376092
    new-instance v0, LX/2BP;

    const/4 v1, 0x1

    const/4 v3, 0x1

    sget-object v4, LX/29k;->DEFAULT_COMPONENT_STATE:LX/29k;

    invoke-direct {v0, v1, v3, v4}, LX/2BP;-><init>(ZZLX/29k;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 376093
    :goto_0
    move-object v0, v0

    .line 376094
    if-eqz v0, :cond_2

    .line 376095
    :cond_1
    :goto_1
    return-object v0

    .line 376096
    :cond_2
    const/4 v3, 0x0

    .line 376097
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 376098
    :try_start_1
    sget-object v1, LX/29J;->a:Ljava/lang/String;

    const/16 v2, 0xc0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    .line 376099
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 376100
    array-length v0, v1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_b

    .line 376101
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->UNEXPECTED_SIGNATURES_STATE:LX/29k;

    invoke-direct {v0, v3, v3, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    .line 376102
    :goto_2
    move-object v0, v0

    .line 376103
    if-nez v0, :cond_1

    .line 376104
    const/4 v3, 0x1

    .line 376105
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 376106
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 376107
    const/4 v2, 0x5

    if-ne v1, v2, :cond_f

    .line 376108
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 376109
    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "302"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 376110
    :cond_3
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->FALLBACK_V13_EU_CANADA:LX/29k;

    invoke-direct {v0, v3, v3, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    .line 376111
    :goto_3
    move-object v0, v0

    .line 376112
    goto :goto_1

    .line 376113
    :cond_4
    :try_start_2
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 376114
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 376115
    new-instance v5, Landroid/content/ComponentName;

    sget-object v6, LX/29J;->a:Ljava/lang/String;

    const-string v7, "com.facebook.oxygen.appmanager.firstparty.tos.ShouldShowTos"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 376116
    :try_start_3
    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    .line 376117
    if-nez v3, :cond_9

    .line 376118
    invoke-static {p0, v5}, LX/29I;->a(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 376119
    new-instance v0, LX/2BP;

    const/4 v3, 0x1

    const/4 v5, 0x1

    sget-object v6, LX/29k;->DEFAULT_COMPONENT_STATE:LX/29k;

    invoke-direct {v0, v3, v5, v6}, LX/2BP;-><init>(ZZLX/29k;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 376120
    :goto_4
    :try_start_4
    move-object v0, v0

    .line 376121
    goto :goto_0

    .line 376122
    :cond_5
    if-ne v5, v0, :cond_6

    move v3, v0

    .line 376123
    :goto_5
    if-ne v1, v0, :cond_7

    move v1, v0

    .line 376124
    :goto_6
    new-instance v0, LX/2BP;

    sget-object v4, LX/29k;->EXPLICIT_COMPONENT_STATE:LX/29k;

    invoke-direct {v0, v3, v1, v4}, LX/2BP;-><init>(ZZLX/29k;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 376125
    :catch_0
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->APPMANAGER_NOT_INSTALLED:LX/29k;

    invoke-direct {v0, v2, v2, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    goto/16 :goto_0

    :cond_6
    move v3, v2

    .line 376126
    goto :goto_5

    :cond_7
    move v1, v2

    .line 376127
    goto :goto_6

    .line 376128
    :catch_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    :try_start_5
    move-object v0, v1

    .line 376129
    goto :goto_4

    .line 376130
    :cond_9
    if-ne v3, v0, :cond_a

    move v3, v0

    .line 376131
    :goto_7
    new-instance v0, LX/2BP;

    sget-object v5, LX/29k;->EXPLICIT_COMPONENT_STATE:LX/29k;

    invoke-direct {v0, v3, v3, v5}, LX/2BP;-><init>(ZZLX/29k;)V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    goto :goto_4

    .line 376132
    :catch_2
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->APPMANAGER_NOT_INSTALLED:LX/29k;

    invoke-direct {v0, v4, v4, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    goto :goto_4

    :cond_a
    move v3, v4

    .line 376133
    goto :goto_7

    .line 376134
    :catch_3
    move-object v0, v1

    goto :goto_4
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 376135
    :catch_4
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->APPMANAGER_NOT_INSTALLED:LX/29k;

    invoke-direct {v0, v3, v3, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    goto/16 :goto_2

    .line 376136
    :cond_b
    sget-boolean v0, LX/1ZQ;->a:Z

    move v0, v0

    .line 376137
    if-eqz v0, :cond_c

    sget-object v0, LX/1ZR;->b:Landroid/content/pm/Signature;

    .line 376138
    :goto_8
    aget-object v1, v1, v3

    invoke-virtual {v1, v0}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 376139
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 376140
    :cond_c
    sget-object v0, LX/1ZR;->a:Landroid/content/pm/Signature;

    goto :goto_8

    .line 376141
    :cond_d
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->OLD_SIGNATURE:LX/29k;

    invoke-direct {v0, v3, v3, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    goto/16 :goto_2

    .line 376142
    :cond_e
    new-instance v0, LX/2BP;

    const/4 v1, 0x0

    sget-object v2, LX/29k;->FALLBACK_V13_OUTSIDE_EU_CANADA:LX/29k;

    invoke-direct {v0, v3, v1, v2}, LX/2BP;-><init>(ZZLX/29k;)V

    goto/16 :goto_3

    .line 376143
    :cond_f
    new-instance v0, LX/2BP;

    sget-object v1, LX/29k;->FALLBACK_V13_NO_SIM:LX/29k;

    invoke-direct {v0, v3, v3, v1}, LX/2BP;-><init>(ZZLX/29k;)V

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 376144
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 376145
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376146
    const/4 v0, 0x1

    .line 376147
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method
