.class public LX/2hX;
.super LX/2hY;
.source ""


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/0Sh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2dj;LX/2do;LX/0Sh;LX/2hZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450061
    invoke-direct {p0, p2, p3, p5}, LX/2hY;-><init>(LX/2dj;LX/2do;LX/2hZ;)V

    .line 450062
    iput-object p1, p0, LX/2hX;->c:Landroid/content/Context;

    .line 450063
    iput-object p4, p0, LX/2hX;->d:LX/0Sh;

    .line 450064
    return-void
.end method

.method public static a(LX/0QB;)LX/2hX;
    .locals 1

    .prologue
    .line 450065
    invoke-static {p0}, LX/2hX;->b(LX/0QB;)LX/2hX;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2hX;
    .locals 6

    .prologue
    .line 450066
    new-instance v0, LX/2hX;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v2

    check-cast v2, LX/2dj;

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v3

    check-cast v3, LX/2do;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/2hZ;->b(LX/0QB;)LX/2hZ;

    move-result-object v5

    check-cast v5, LX/2hZ;

    invoke-direct/range {v0 .. v5}, LX/2hX;-><init>(Landroid/content/Context;LX/2dj;LX/2do;LX/0Sh;LX/2hZ;)V

    .line 450067
    return-object v0
.end method

.method public static b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 7

    .prologue
    .line 450068
    iget-object v6, p0, LX/2hX;->d:LX/0Sh;

    new-instance v0, Lcom/facebook/friends/controllers/FriendingButtonController$6;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/friends/controllers/FriendingButtonController$6;-><init>(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v6, v0}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 450069
    return-void
.end method


# virtual methods
.method public final a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/0ju;
    .locals 7

    .prologue
    .line 450070
    new-instance v0, LX/84I;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/84I;-><init>(LX/2hX;JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 450071
    new-instance v1, LX/84J;

    invoke-direct {v1, p0}, LX/84J;-><init>(LX/2hX;)V

    .line 450072
    new-instance v2, LX/84K;

    invoke-direct {v2, p0, p1, p2, p4}, LX/84K;-><init>(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 450073
    new-instance v3, LX/0ju;

    iget-object v4, p0, LX/2hX;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v4, 0x7f080019

    invoke-virtual {v3, v4, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v3, 0x7f080017

    invoke-virtual {v0, v3, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 450074
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450075
    invoke-virtual {p0, p1, p2, p4, p5}, LX/2hX;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/0ju;

    move-result-object v0

    .line 450076
    iget-object v1, p0, LX/2hX;->c:Landroid/content/Context;

    const v2, 0x7f080fa1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object p3, v2, p1

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 450077
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 450078
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 450079
    :goto_0
    return-void

    .line 450080
    :cond_0
    invoke-virtual {p0, p1, p2, p4, p5}, LX/2hX;->b(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0
.end method

.method public final b(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 9

    .prologue
    .line 450081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450082
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    iget-object v1, p3, LX/2h7;->removeFriendRef:LX/2hB;

    invoke-virtual {v0, p1, p2, v1}, LX/2dj;->a(JLX/2hB;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450083
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v7, v0

    .line 450084
    :goto_0
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v4, v0}, LX/2hX;->b(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 450085
    iget-object v8, p0, LX/2hX;->d:LX/0Sh;

    new-instance v0, LX/84L;

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/84L;-><init>(LX/2hX;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/2h7;)V

    invoke-virtual {v8, v7, v0}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 450086
    :cond_0
    return-void

    .line 450087
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 450088
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    iget-object v1, p3, LX/2h7;->friendRequestCancelRef:LX/2h9;

    invoke-virtual {v0, p1, p2, v1}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450089
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v7, v0

    goto :goto_0

    .line 450090
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 450091
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    sget-object v1, LX/2na;->CONFIRM:LX/2na;

    iget-object v2, p3, LX/2h7;->friendRequestResponseRef:LX/2hA;

    invoke-virtual {v0, p1, p2, v1, v2}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450092
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v7, v0

    goto :goto_0

    .line 450093
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450094
    iget-object v1, p0, LX/2hY;->a:LX/2dj;

    iget-object v4, p3, LX/2h7;->friendRequestHowFound:LX/2h8;

    iget-object v5, p3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 450095
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v7, v0

    goto :goto_0
.end method
