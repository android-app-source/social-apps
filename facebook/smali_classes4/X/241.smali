.class public LX/241;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2uR;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/34Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 366468
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/241;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/34Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366465
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 366466
    iput-object p1, p0, LX/241;->b:LX/0Ot;

    .line 366467
    return-void
.end method

.method public static a(LX/0QB;)LX/241;
    .locals 4

    .prologue
    .line 366444
    const-class v1, LX/241;

    monitor-enter v1

    .line 366445
    :try_start_0
    sget-object v0, LX/241;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366446
    sput-object v2, LX/241;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366449
    new-instance v3, LX/241;

    const/16 p0, 0x3af

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/241;-><init>(LX/0Ot;)V

    .line 366450
    move-object v0, v3

    .line 366451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/241;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 366457
    check-cast p2, LX/34P;

    .line 366458
    iget-object v0, p0, LX/241;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/34P;->a:LX/0Px;

    const/4 v1, 0x0

    .line 366459
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    .line 366460
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    move v2, v1

    :goto_0
    if-ge v2, p2, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1X1;

    .line 366461
    invoke-interface {p0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 366462
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 366463
    :cond_0
    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 366464
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 366455
    invoke-static {}, LX/1dS;->b()V

    .line 366456
    const/4 v0, 0x0

    return-object v0
.end method
