.class public LX/2gk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Tn;

.field private static final c:[Ljava/lang/String;


# instance fields
.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6ZU;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:Ljava/util/concurrent/Executor;

.field public final g:LX/2bj;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:LX/0i4;

.field private final k:LX/03V;

.field public final l:LX/0y3;

.field public final m:LX/0Uh;

.field public n:LX/0yG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 448870
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2gk;->a:LX/0Px;

    .line 448871
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "on_login_gms_dialog_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2gk;->b:LX/0Tn;

    .line 448872
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, LX/2gk;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/2bj;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0i4;LX/03V;LX/0y3;LX/0Uh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6ZU;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/util/concurrent/Executor;",
            "LX/2bj;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0i4;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0y3;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448859
    iput-object p1, p0, LX/2gk;->d:LX/0Or;

    .line 448860
    iput-object p2, p0, LX/2gk;->e:Lcom/facebook/content/SecureContextHelper;

    .line 448861
    iput-object p3, p0, LX/2gk;->f:Ljava/util/concurrent/Executor;

    .line 448862
    iput-object p4, p0, LX/2gk;->g:LX/2bj;

    .line 448863
    iput-object p5, p0, LX/2gk;->h:LX/0Or;

    .line 448864
    iput-object p6, p0, LX/2gk;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 448865
    iput-object p7, p0, LX/2gk;->j:LX/0i4;

    .line 448866
    iput-object p8, p0, LX/2gk;->k:LX/03V;

    .line 448867
    iput-object p9, p0, LX/2gk;->l:LX/0y3;

    .line 448868
    iput-object p10, p0, LX/2gk;->m:LX/0Uh;

    .line 448869
    return-void
.end method

.method public static d(LX/2gk;)LX/0Tn;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 448853
    iget-object v0, p0, LX/2gk;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 448854
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 448855
    iget-object v0, p0, LX/2gk;->k:LX/03V;

    const-string v1, "location_setting_resurrection_on_login_fetch_user_id_fail"

    const-string v2, "Cannot fetch logged in user id"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448856
    const/4 v0, 0x0

    .line 448857
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, LX/2gk;->b:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 448852
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 448842
    iget-object v0, p0, LX/2gk;->l:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    iput-object v0, p0, LX/2gk;->n:LX/0yG;

    .line 448843
    iget-object v0, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FIRST_NEWSFEED_AFTER_LOGIN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/2gk;->n:LX/0yG;

    const/4 v1, 0x0

    .line 448844
    invoke-static {p0}, LX/2gk;->d(LX/2gk;)LX/0Tn;

    move-result-object v2

    .line 448845
    if-nez v2, :cond_3

    .line 448846
    :cond_0
    :goto_0
    move v0, v1

    .line 448847
    if-nez v0, :cond_2

    .line 448848
    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 448849
    :goto_1
    return-object v0

    :cond_2
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_1

    .line 448850
    :cond_3
    iget-object p1, p0, LX/2gk;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 448851
    if-nez v2, :cond_0

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v2, :cond_0

    iget-object v2, p0, LX/2gk;->m:LX/0Uh;

    const/16 p1, 0x581

    invoke-virtual {v2, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 448841
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 448835
    iget-object v0, p0, LX/2gk;->g:LX/2bj;

    const-string v1, "location_opt_in_on_login_gms_ls_dialog_start"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 448836
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 448837
    if-nez v0, :cond_0

    .line 448838
    iget-object v0, p0, LX/2gk;->k:LX/03V;

    const-class v1, LX/2gk;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Needed an Activity object but this controller did not run within an activity"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 448839
    :goto_0
    return-void

    .line 448840
    :cond_0
    iget-object v1, p0, LX/2gk;->j:LX/0i4;

    invoke-virtual {v1, v0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    sget-object v1, LX/2gk;->c:[Ljava/lang/String;

    new-instance v2, LX/GVI;

    invoke-direct {v2, p0, p1}, LX/GVI;-><init>(LX/2gk;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 448832
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448834
    const-string v0, "3931"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448833
    sget-object v0, LX/2gk;->a:LX/0Px;

    return-object v0
.end method
