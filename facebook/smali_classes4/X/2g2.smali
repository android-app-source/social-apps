.class public LX/2g2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2g3;

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(LX/2g3;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447772
    iput-object p1, p0, LX/2g2;->a:LX/2g3;

    .line 447773
    iput-object p2, p0, LX/2g2;->b:LX/03V;

    .line 447774
    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;Z)LX/03R;
    .locals 2

    .prologue
    .line 447775
    sget-object v0, LX/77V;->a:[I

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 447776
    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    :goto_0
    return-object v0

    .line 447777
    :pswitch_0
    if-nez p1, :cond_0

    .line 447778
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 447779
    :pswitch_1
    if-eqz p1, :cond_0

    .line 447780
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 447781
    :pswitch_2
    if-eqz p1, :cond_0

    .line 447782
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2g2;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 7
    .param p0    # LX/2g2;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 447783
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->type:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    .line 447784
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    if-eq v3, v0, :cond_0

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->filters:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->clauses:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447785
    :cond_0
    new-instance v0, LX/77W;

    invoke-direct {v0}, LX/77W;-><init>()V

    throw v0

    .line 447786
    :cond_1
    iget-object v4, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->filters:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 447787
    iget-object v6, p0, LX/2g2;->a:LX/2g3;

    invoke-virtual {v6, v0, p2, p3}, LX/2g3;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    .line 447788
    invoke-static {v3, v0}, LX/2g2;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;Z)LX/03R;

    move-result-object v0

    .line 447789
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 447790
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 447791
    :goto_1
    return v0

    .line 447792
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 447793
    :cond_3
    iget-object v4, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->clauses:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 447794
    invoke-static {p0, v0, p2, p3}, LX/2g2;->a(LX/2g2;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    .line 447795
    invoke-static {v3, v0}, LX/2g2;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;Z)LX/03R;

    move-result-object v0

    .line 447796
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 447797
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    goto :goto_1

    .line 447798
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 447799
    :cond_5
    sget-object v0, LX/77V;->a:[I

    invoke-virtual {v3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 447800
    new-instance v0, LX/77W;

    invoke-direct {v0}, LX/77W;-><init>()V

    throw v0

    :pswitch_0
    move v0, v1

    .line 447801
    goto :goto_1

    .line 447802
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/2g2;
    .locals 3

    .prologue
    .line 447803
    new-instance v2, LX/2g2;

    invoke-static {p0}, LX/2g3;->a(LX/0QB;)LX/2g3;

    move-result-object v0

    check-cast v0, LX/2g3;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/2g2;-><init>(LX/2g3;LX/03V;)V

    .line 447804
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;)Ljava/util/Map;
    .locals 8
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 447805
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 447806
    iget-object v4, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->filters:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 447807
    iget-object v6, p0, LX/2g2;->a:LX/2g3;

    const/4 v7, 0x0

    invoke-virtual {v6, v0, p1, v7}, LX/2g3;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v6

    .line 447808
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v3, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447809
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 447810
    :cond_0
    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->clauses:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 447811
    invoke-virtual {p0, p1, v0}, LX/2g2;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;)Ljava/util/Map;

    move-result-object v0

    .line 447812
    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 447813
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 447814
    :cond_1
    return-object v3
.end method
