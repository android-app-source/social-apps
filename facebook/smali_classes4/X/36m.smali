.class public LX/36m;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Z

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field public d:Landroid/widget/ImageView;

.field public e:Lcom/facebook/widget/text/FakeCursorHook;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 500072
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/36m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 500073
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 500066
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 500067
    const p1, 0x7f03090e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 500068
    const p1, 0x7f0d173c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, LX/36m;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 500069
    const p1, 0x7f0d173d

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/36m;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 500070
    const p1, 0x7f0d173e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/36m;->d:Landroid/widget/ImageView;

    .line 500071
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 500065
    iget-boolean v0, p0, LX/36m;->b:Z

    return v0
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 500054
    iget-object v0, p0, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    if-nez v0, :cond_0

    .line 500055
    :goto_0
    return-void

    .line 500056
    :cond_0
    iget-object v0, p0, LX/36m;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->b(LX/636;)V

    .line 500057
    iget-object v0, p0, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    const/4 p0, 0x0

    .line 500058
    iget-boolean v1, v0, Lcom/facebook/widget/text/FakeCursorHook;->d:Z

    if-nez v1, :cond_1

    .line 500059
    :goto_1
    goto :goto_0

    .line 500060
    :cond_1
    iget-object v1, v0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 500061
    iget-boolean v1, v0, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    if-nez v1, :cond_2

    .line 500062
    iget-object v1, v0, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    iget v2, v0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    const/16 v3, 0x14

    iget v4, v0, Lcom/facebook/widget/text/FakeCursorHook;->f:I

    iget v5, v0, Lcom/facebook/widget/text/FakeCursorHook;->g:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    iget v5, v0, Lcom/facebook/widget/text/FakeCursorHook;->e:I

    add-int/lit8 v5, v5, -0x14

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/widget/text/BetterTextView;->invalidate(IIII)V

    .line 500063
    :cond_2
    iput-boolean p0, v0, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    .line 500064
    iput-boolean p0, v0, Lcom/facebook/widget/text/FakeCursorHook;->d:Z

    goto :goto_1
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 500052
    iput-boolean p1, p0, LX/36m;->b:Z

    .line 500053
    return-void
.end method
