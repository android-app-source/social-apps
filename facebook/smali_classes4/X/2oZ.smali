.class public final LX/2oZ;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ot;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 0

    .prologue
    .line 467153
    iput-object p1, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;B)V
    .locals 0

    .prologue
    .line 467154
    invoke-direct {p0, p1}, LX/2oZ;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 467155
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 467156
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->P:Z

    .line 467157
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    if-nez v0, :cond_0

    .line 467158
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 467159
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    .line 467160
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 467161
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 467162
    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v1

    .line 467163
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    .line 467164
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    if-eqz v0, :cond_0

    .line 467165
    iget-object v0, p0, LX/2oZ;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 467166
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467167
    const-class v0, LX/2ot;

    return-object v0
.end method

.method public final bridge synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 467168
    invoke-direct {p0}, LX/2oZ;->b()V

    return-void
.end method
