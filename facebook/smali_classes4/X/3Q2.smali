.class public LX/3Q2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Gq;

.field private final b:LX/2Gc;


# direct methods
.method public constructor <init>(LX/2Gq;LX/2Gc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562699
    iput-object p1, p0, LX/3Q2;->a:LX/2Gq;

    .line 562700
    iput-object p2, p0, LX/3Q2;->b:LX/2Gc;

    .line 562701
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q2;
    .locals 1

    .prologue
    .line 562702
    invoke-static {p0}, LX/3Q2;->b(LX/0QB;)LX/3Q2;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Q2;
    .locals 3

    .prologue
    .line 562703
    new-instance v2, LX/3Q2;

    invoke-static {p0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v0

    check-cast v0, LX/2Gq;

    invoke-static {p0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v1

    check-cast v1, LX/2Gc;

    invoke-direct {v2, v0, v1}, LX/3Q2;-><init>(LX/2Gq;LX/2Gc;)V

    .line 562704
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 562705
    iget-object v0, p0, LX/3Q2;->a:LX/2Gq;

    .line 562706
    invoke-static {}, LX/2Gc;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 562707
    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    .line 562708
    :goto_0
    move-object v1, v1

    .line 562709
    invoke-virtual {v0, v1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    .line 562710
    iget-object v1, v0, LX/2H0;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 562711
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 562712
    :goto_1
    move-object v0, v1

    .line 562713
    return-object v0

    .line 562714
    :cond_0
    invoke-static {}, LX/2Gc;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562715
    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    goto :goto_0

    .line 562716
    :cond_1
    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, LX/2H0;->i()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
