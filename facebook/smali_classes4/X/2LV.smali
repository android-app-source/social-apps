.class public LX/2LV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LP;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/03V;

.field private final d:LX/2LS;

.field private final e:Ljava/lang/String;

.field private f:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 395114
    const-string v0, "content://com.android.badge/badge"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/2LV;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/launcherbadges/AppLaunchClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395116
    iput-object p1, p0, LX/2LV;->b:Landroid/content/Context;

    .line 395117
    iput-object p2, p0, LX/2LV;->c:LX/03V;

    .line 395118
    iput-object p3, p0, LX/2LV;->d:LX/2LS;

    .line 395119
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2LV;->e:Ljava/lang/String;

    .line 395120
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2LV;->f:LX/03R;

    .line 395121
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 395122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/2LV;->d:LX/2LS;

    .line 395123
    invoke-static {v0}, LX/2LS;->i(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string p0, "com.oppo.launcher"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 395124
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/03R;
    .locals 5

    .prologue
    .line 395125
    iget-object v0, p0, LX/2LV;->f:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 395126
    invoke-direct {p0}, LX/2LV;->a()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2LV;->f:LX/03R;

    .line 395127
    :cond_0
    iget-object v0, p0, LX/2LV;->f:LX/03R;

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_1

    .line 395128
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 395129
    :goto_0
    return-object v0

    .line 395130
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 395131
    const-string v1, "app_badge_packageName"

    iget-object v2, p0, LX/2LV;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 395132
    const-string v1, "app_badge_count"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 395133
    :try_start_0
    iget-object v1, p0, LX/2LV;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, LX/2LV;->a:Landroid/net/Uri;

    const-string v3, "setAppBadgeCount"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 395134
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 395135
    :catch_0
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LV;->f:LX/03R;

    .line 395136
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 395137
    :catch_1
    move-exception v0

    .line 395138
    iget-object v1, p0, LX/2LV;->c:LX/03V;

    const-string v2, "oppo_badging"

    const-string v3, "Failed to set app badge count."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395139
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LV;->f:LX/03R;

    .line 395140
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method
