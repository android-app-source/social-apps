.class public final LX/3Fh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V
    .locals 0

    .prologue
    .line 539817
    iput-object p1, p0, LX/3Fh;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/BSa;)V
    .locals 13

    .prologue
    .line 539818
    sget-object v0, LX/D6n;->a:[I

    iget-object v1, p1, LX/BSa;->b:LX/BSZ;

    invoke-virtual {v1}, LX/BSZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 539819
    :goto_0
    return-void

    .line 539820
    :pswitch_0
    iget-object v0, p0, LX/3Fh;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const/4 v3, 0x0

    .line 539821
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    iget-object v4, p1, LX/BSa;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 539822
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Gv;

    .line 539823
    :goto_1
    iget-object v4, p1, LX/BSa;->a:Ljava/lang/String;

    iget v5, p1, LX/BSa;->e:I

    invoke-virtual {v0, v4, v5}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/3Gv;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539824
    iget-object v6, p1, LX/BSa;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;

    move-result-object v8

    .line 539825
    iget-wide v6, p1, LX/BSa;->c:J

    iput-wide v6, v8, LX/D6o;->c:J

    .line 539826
    iget-object v7, p1, LX/BSa;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g:LX/2ml;

    iget v9, v6, LX/2ml;->g:I

    iget-object v6, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g:LX/2ml;

    iget v10, v6, LX/2ml;->h:I

    const-string v11, "LIVE"

    iget v12, p1, LX/BSa;->e:I

    move-object v6, v0

    invoke-static/range {v6 .. v12}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;LX/D6o;IILjava/lang/String;I)V

    .line 539827
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->n:Ljava/util/Map;

    iget-object v4, p1, LX/BSa;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 539828
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Gz;

    .line 539829
    :goto_2
    if-eqz v2, :cond_0

    .line 539830
    iget-object v3, v2, LX/3Gz;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v3, v3, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->n:LX/3HT;

    new-instance v4, LX/Adb;

    invoke-direct {v4}, LX/Adb;-><init>()V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 539831
    :cond_0
    goto :goto_0

    .line 539832
    :pswitch_1
    iget-object v0, p0, LX/3Fh;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    .line 539833
    iget-object v2, p1, LX/BSa;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->i(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)LX/D6o;

    move-result-object v4

    .line 539834
    iget-wide v2, p1, LX/BSa;->d:J

    iput-wide v2, v4, LX/D6o;->d:J

    .line 539835
    iget-wide v2, p1, LX/BSa;->c:J

    const-wide/16 v6, -0x1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 539836
    iget-wide v2, p1, LX/BSa;->c:J

    iput-wide v2, v4, LX/D6o;->c:J

    .line 539837
    :cond_1
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    iget-object v3, p1, LX/BSa;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 539838
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Gv;

    .line 539839
    :goto_3
    if-eqz v2, :cond_3

    .line 539840
    iget v3, p1, LX/BSa;->e:I

    .line 539841
    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v5, v5, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v5, :cond_2

    .line 539842
    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v5, v5, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iput v3, v5, LX/D6v;->g:I

    .line 539843
    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v5, v5, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    invoke-virtual {v5}, LX/D6v;->a()V

    .line 539844
    :cond_2
    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-static {v5}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->getLiveStreamingFormat(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)LX/BSW;

    move-result-object v5

    sget-object v6, LX/BSW;->HLS:LX/BSW;

    if-ne v5, v6, :cond_3

    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    if-eqz v5, :cond_3

    .line 539845
    iget-object v5, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    const/4 v6, 0x1

    .line 539846
    iput-boolean v6, v5, LX/2pb;->M:Z

    .line 539847
    :cond_3
    iget-object v3, p1, LX/BSa;->a:Ljava/lang/String;

    iget v5, p1, LX/BSa;->e:I

    invoke-virtual {v0, v3, v5}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v2, :cond_5

    iget-wide v6, p1, LX/BSa;->d:J

    .line 539848
    iget-object v9, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v9, v9, LX/2oy;->j:LX/2pb;

    if-eqz v9, :cond_4

    iget-object v9, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v9, v9, LX/2oy;->j:LX/2pb;

    invoke-virtual {v9}, LX/2pb;->i()I

    move-result v9

    if-gtz v9, :cond_9

    .line 539849
    :cond_4
    invoke-virtual {v2}, LX/3Gv;->a()Z

    move-result v9

    .line 539850
    :goto_4
    move v2, v9

    .line 539851
    if-eqz v2, :cond_5

    .line 539852
    iget-object v3, p1, LX/BSa;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g:LX/2ml;

    iget v5, v2, LX/2ml;->g:I

    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g:LX/2ml;

    iget v6, v2, LX/2ml;->h:I

    const-string v7, "LIVE"

    iget v8, p1, LX/BSa;->e:I

    move-object v2, v0

    invoke-static/range {v2 .. v8}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;LX/D6o;IILjava/lang/String;I)V

    .line 539853
    :cond_5
    goto/16 :goto_0

    :cond_6
    move-object v2, v3

    .line 539854
    goto/16 :goto_1

    :cond_7
    move-object v2, v3

    .line 539855
    goto/16 :goto_2

    .line 539856
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    :cond_9
    iget-object v9, v2, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v9, v9, LX/2oy;->j:LX/2pb;

    invoke-virtual {v9}, LX/2pb;->i()I

    move-result v9

    int-to-long v9, v9

    cmp-long v9, v9, v6

    if-gez v9, :cond_a

    const/4 v9, 0x1

    goto :goto_4

    :cond_a
    const/4 v9, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
