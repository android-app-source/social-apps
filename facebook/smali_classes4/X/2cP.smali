.class public LX/2cP;
.super LX/0aB;
.source ""

# interfaces
.implements LX/2cF;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/2cP;


# instance fields
.field private final b:LX/0a8;

.field private final c:LX/0Uh;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/117;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2cQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440745
    const-class v0, LX/2cP;

    sput-object v0, LX/2cP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0a8;LX/0Uh;LX/0Or;LX/0Or;LX/117;LX/0Ot;LX/2cQ;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;",
            "LX/117;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/2cQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440746
    invoke-direct {p0}, LX/0aB;-><init>()V

    .line 440747
    iput-object p1, p0, LX/2cP;->b:LX/0a8;

    .line 440748
    iput-object p2, p0, LX/2cP;->c:LX/0Uh;

    .line 440749
    iput-object p3, p0, LX/2cP;->d:LX/0Or;

    .line 440750
    iput-object p4, p0, LX/2cP;->e:LX/0Or;

    .line 440751
    iput-object p5, p0, LX/2cP;->f:LX/117;

    .line 440752
    iput-object p6, p0, LX/2cP;->g:LX/0Ot;

    .line 440753
    iput-object p7, p0, LX/2cP;->h:LX/2cQ;

    .line 440754
    return-void
.end method

.method public static a(LX/0QB;)LX/2cP;
    .locals 11

    .prologue
    .line 440755
    sget-object v0, LX/2cP;->i:LX/2cP;

    if-nez v0, :cond_1

    .line 440756
    const-class v1, LX/2cP;

    monitor-enter v1

    .line 440757
    :try_start_0
    sget-object v0, LX/2cP;->i:LX/2cP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440758
    if-eqz v2, :cond_0

    .line 440759
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440760
    new-instance v3, LX/2cP;

    invoke-static {v0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v4

    check-cast v4, LX/0a8;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0x15e8

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xea6

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/116;->b(LX/0QB;)LX/116;

    move-result-object v8

    check-cast v8, LX/117;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/2cQ;->a(LX/0QB;)LX/2cQ;

    move-result-object v10

    check-cast v10, LX/2cQ;

    invoke-direct/range {v3 .. v10}, LX/2cP;-><init>(LX/0a8;LX/0Uh;LX/0Or;LX/0Or;LX/117;LX/0Ot;LX/2cQ;)V

    .line 440761
    move-object v0, v3

    .line 440762
    sput-object v0, LX/2cP;->i:LX/2cP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440763
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440764
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440765
    :cond_1
    sget-object v0, LX/2cP;->i:LX/2cP;

    return-object v0

    .line 440766
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440767
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;I)V
    .locals 1

    .prologue
    .line 440768
    iget-object v0, p0, LX/2cP;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Pk;

    invoke-virtual {v0, p0}, LX/2Pk;->checkComponentSubscription(LX/2cF;)V

    .line 440769
    return-void
.end method

.method public final indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 1

    .prologue
    .line 440770
    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    return-object v0
.end method

.method public final onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V
    .locals 1

    .prologue
    .line 440771
    iget-object v0, p0, LX/2cP;->h:LX/2cQ;

    invoke-virtual {v0, p1}, LX/2cQ;->a(Lcom/facebook/omnistore/Collection;)V

    .line 440772
    return-void
.end method

.method public final onCollectionInvalidated()V
    .locals 2

    .prologue
    .line 440773
    iget-object v0, p0, LX/2cP;->h:LX/2cQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2cQ;->a(Lcom/facebook/omnistore/Collection;)V

    .line 440774
    return-void
.end method

.method public final onDeltasReceived(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440775
    iget-object v0, p0, LX/2cP;->h:LX/2cQ;

    invoke-virtual {v0}, LX/2cQ;->a()V

    .line 440776
    return-void
.end method

.method public final provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;
    .locals 6

    .prologue
    .line 440777
    iget-object v0, p0, LX/2cP;->b:LX/0a8;

    const/16 v1, 0x193

    invoke-virtual {v0, p0, v1}, LX/0a8;->a(LX/0aB;I)V

    .line 440778
    const-string v0, "fql_user_nux_status"

    invoke-virtual {p1, v0}, Lcom/facebook/omnistore/Omnistore;->createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v1

    iget-object v0, p0, LX/2cP;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addDeviceId()Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->build()Lcom/facebook/omnistore/CollectionName;

    move-result-object v0

    .line 440779
    iget-object v1, p0, LX/2cP;->c:LX/0Uh;

    const/16 v2, 0x193

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 440780
    const-string v2, ""

    .line 440781
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 440782
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 440783
    new-instance v5, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/162;-><init>(LX/0mC;)V

    .line 440784
    iget-object v1, p0, LX/2cP;->f:LX/117;

    invoke-virtual {v1}, LX/117;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 440785
    invoke-virtual {v5, v1}, LX/162;->g(Ljava/lang/String;)LX/162;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 440786
    :catch_0
    move-exception v1

    move-object v3, v1

    .line 440787
    iget-object v1, p0, LX/2cP;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v4, LX/2cP;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 440788
    :goto_1
    new-instance v2, LX/2cX;

    invoke-direct {v2}, LX/2cX;-><init>()V

    .line 440789
    iput-object v1, v2, LX/2cX;->mCollectionParams:Ljava/lang/String;

    .line 440790
    move-object v1, v2

    .line 440791
    const-string v2, "namespace com.facebook.interstitial.omnistore;\n\ntable UserNuxStatus {\n nux_id:string; \n rank:int; \n nux_data:string;\n fetch_time:long;\n}\n\nroot_type UserNuxStatus;\n"

    .line 440792
    iput-object v2, v1, LX/2cX;->mIdl:Ljava/lang/String;

    .line 440793
    move-object v1, v1

    .line 440794
    invoke-virtual {v1}, LX/2cX;->build()LX/2cY;

    move-result-object v1

    move-object v1, v1

    .line 440795
    invoke-static {v0, v1}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;LX/2cY;)LX/2cJ;

    move-result-object v0

    .line 440796
    :goto_2
    return-object v0

    .line 440797
    :cond_0
    sget-object v0, LX/2cJ;->IGNORED_INFO:LX/2cJ;

    move-object v0, v0

    .line 440798
    goto :goto_2

    .line 440799
    :cond_1
    :try_start_1
    const-string v1, "nux_ids"

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 440800
    const-string v1, "render_object_list_query_params"

    invoke-virtual {v3, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 440801
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1
.end method
