.class public LX/3QW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3QW;


# instance fields
.field public a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565199
    iput-object p1, p0, LX/3QW;->a:LX/0Uh;

    .line 565200
    return-void
.end method

.method public static a(LX/0QB;)LX/3QW;
    .locals 4

    .prologue
    .line 565180
    sget-object v0, LX/3QW;->b:LX/3QW;

    if-nez v0, :cond_1

    .line 565181
    const-class v1, LX/3QW;

    monitor-enter v1

    .line 565182
    :try_start_0
    sget-object v0, LX/3QW;->b:LX/3QW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565183
    if-eqz v2, :cond_0

    .line 565184
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565185
    new-instance p0, LX/3QW;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/3QW;-><init>(LX/0Uh;)V

    .line 565186
    move-object v0, p0

    .line 565187
    sput-object v0, LX/3QW;->b:LX/3QW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565188
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565189
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565190
    :cond_1
    sget-object v0, LX/3QW;->b:LX/3QW;

    return-object v0

    .line 565191
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 565197
    iget-object v0, p0, LX/3QW;->a:LX/0Uh;

    const/16 v1, 0x165

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 565193
    invoke-virtual {p0}, LX/3QW;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 565194
    :cond_0
    :goto_0
    return v0

    .line 565195
    :cond_1
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 565196
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-boolean v0, v0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    goto :goto_0
.end method
