.class public LX/2n3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field private static final b:LX/0Tn;


# instance fields
.field private final c:LX/0WJ;

.field public final d:Ljava/lang/String;

.field private final e:LX/14x;

.field public final f:LX/1Be;

.field private final g:LX/1Bn;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:Z

.field private final k:LX/2nC;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/13Q;

.field private final n:LX/1BA;

.field public final o:LX/0ad;

.field public final p:LX/01T;

.field private final q:LX/0Uh;

.field private final r:LX/2nE;

.field private final s:LX/2n4;

.field private final t:LX/2nD;

.field private final u:LX/1Bg;

.field private final v:LX/1Br;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 462513
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fb_android/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "in_app_browser_profiling"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2n3;->a:LX/0Tn;

    .line 462514
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fb_android/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "in_app_browser_debug_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2n3;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/01T;LX/0WV;LX/1Be;LX/1Bn;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2nC;LX/13Q;LX/1BA;LX/0Or;LX/14x;Ljava/lang/Boolean;LX/0Or;LX/0ad;LX/0Uh;LX/2nD;Landroid/os/Handler;LX/2nE;LX/2n4;LX/1Bg;LX/1Br;)V
    .locals 2
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsQuoteShareEntryPointEnabled;
        .end annotation
    .end param
    .param p17    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/01T;",
            "LX/0WV;",
            "LX/1Be;",
            "LX/1Bn;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2nC;",
            "LX/13Q;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/14x;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2nD;",
            "Landroid/os/Handler;",
            "LX/2nE;",
            "LX/2n4;",
            "LX/1Bg;",
            "LX/1Br;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462847
    iput-object p1, p0, LX/2n3;->c:LX/0WJ;

    .line 462848
    iput-object p2, p0, LX/2n3;->p:LX/01T;

    .line 462849
    move-object/from16 v0, p20

    iput-object v0, p0, LX/2n3;->u:LX/1Bg;

    .line 462850
    invoke-static {p2, p3}, LX/1Bg;->a(LX/01T;LX/0WV;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2n3;->d:Ljava/lang/String;

    .line 462851
    iput-object p4, p0, LX/2n3;->f:LX/1Be;

    .line 462852
    iput-object p5, p0, LX/2n3;->g:LX/1Bn;

    .line 462853
    iput-object p6, p0, LX/2n3;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 462854
    iput-object p7, p0, LX/2n3;->k:LX/2nC;

    .line 462855
    iput-object p8, p0, LX/2n3;->m:LX/13Q;

    .line 462856
    iput-object p9, p0, LX/2n3;->n:LX/1BA;

    .line 462857
    iput-object p10, p0, LX/2n3;->h:LX/0Or;

    .line 462858
    iput-object p11, p0, LX/2n3;->e:LX/14x;

    .line 462859
    invoke-virtual {p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/2n3;->j:Z

    .line 462860
    iput-object p13, p0, LX/2n3;->l:LX/0Or;

    .line 462861
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2n3;->o:LX/0ad;

    .line 462862
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2n3;->q:LX/0Uh;

    .line 462863
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2n3;->t:LX/2nD;

    .line 462864
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2n3;->r:LX/2nE;

    .line 462865
    move-object/from16 v0, p19

    iput-object v0, p0, LX/2n3;->s:LX/2n4;

    .line 462866
    move-object/from16 v0, p21

    iput-object v0, p0, LX/2n3;->v:LX/1Br;

    .line 462867
    return-void
.end method

.method public static a(LX/0QB;)LX/2n3;
    .locals 1

    .prologue
    .line 462845
    invoke-static {p0}, LX/2n3;->b(LX/0QB;)LX/2n3;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0DW;Landroid/content/Context;ZZ)V
    .locals 4

    .prologue
    .line 462833
    if-eqz p3, :cond_0

    .line 462834
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02080f

    const-string v2, "SHARE_TIMELINE"

    invoke-virtual {p1, v0, v1, v2}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462835
    :cond_0
    invoke-static {p0}, LX/2n3;->b(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    .line 462836
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, LX/2n3;->j:Z

    if-eqz v0, :cond_4

    const v0, 0x7f081cf0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020174

    const-string v2, "SHARE_MESSENGER"

    invoke-virtual {p1, v0, v1, v2}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462837
    :cond_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ced

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02016e

    const-string v2, "COPY_LINK"

    invoke-virtual {p1, v0, v1, v2}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    move-result-object v0

    const-string v1, "MENU_OPEN_WITH"

    invoke-virtual {v0, v1}, LX/0DW;->a(Ljava/lang/String;)LX/0DW;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081cf1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020183

    const-string v3, "SAVE_LINK"

    invoke-virtual {v0, v1, v2, v3}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462838
    invoke-static {p0}, LX/2n3;->e(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462839
    const-string v0, "Clear Debug Overlay"

    const v1, 0x7f02016a

    const-string v2, "CLEAR_DEBUG_OVERLAY"

    invoke-virtual {p1, v0, v1, v2}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462840
    :cond_2
    iget-object v0, p0, LX/2n3;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 462841
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0DW;->c(Z)LX/0DW;

    .line 462842
    const-string v0, "[fb-only] Open in Main Process"

    const v1, 0x7f020177

    const-string v2, "OPEN_IN_MAIN_PROCESS"

    invoke-virtual {p1, v0, v1, v2}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462843
    :cond_3
    return-void

    .line 462844
    :cond_4
    const v0, 0x7f081cef

    goto :goto_0
.end method

.method private static a(LX/0DW;Ljava/lang/String;ZI)V
    .locals 6

    .prologue
    .line 462822
    const-string v2, "SHARE_TIMELINE"

    const-string v5, "facebook.com"

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p2

    .line 462823
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 462824
    const-string p1, "KEY_LABEL"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462825
    if-eqz v5, :cond_0

    .line 462826
    const-string p1, "KEY_BLACKLIST_DOMAIN"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462827
    :cond_0
    const-string p1, "action"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462828
    if-eqz v3, :cond_1

    .line 462829
    const-string p1, "KEY_ICON_RES"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 462830
    :cond_1
    const-string p1, "ACTION_BUTTON_ONLY"

    invoke-virtual {p0, p1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 462831
    iget-object p1, v0, LX/0DW;->a:Landroid/content/Intent;

    const-string p2, "BrowserLiteIntent.EXTRA_ACTION_BUTTON"

    invoke-virtual {p1, p2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 462832
    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 462776
    const-string v0, "extra_survey_config"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 462777
    if-nez v0, :cond_0

    .line 462778
    :goto_0
    return-void

    .line 462779
    :cond_0
    const-string v1, "extra_survey_config"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462780
    iget-object v1, p0, LX/2n3;->t:LX/2nD;

    const/4 v4, 0x0

    .line 462781
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 462782
    array-length v7, v0

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_5

    aget-object v2, v0, v5

    .line 462783
    const/4 v3, 0x0

    const/4 v12, 0x1

    .line 462784
    const-string v8, ":"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 462785
    array-length v9, v8

    const/4 v10, 0x3

    if-eq v9, v10, :cond_7

    .line 462786
    :cond_1
    :goto_2
    move-object v8, v3

    .line 462787
    if-eqz v8, :cond_3

    .line 462788
    iget v2, v8, LX/Baf;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 462789
    new-instance v2, LX/Bag;

    invoke-direct {v2}, LX/Bag;-><init>()V

    .line 462790
    iget v3, v8, LX/Baf;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v6, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462791
    :goto_3
    iget-object v9, v8, LX/Baf;->b:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    :goto_4
    packed-switch v3, :pswitch_data_0

    .line 462792
    :cond_3
    :goto_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 462793
    :cond_4
    iget v2, v8, LX/Baf;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bag;

    goto :goto_3

    .line 462794
    :sswitch_0
    const-string v10, "NONE"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v3, v4

    goto :goto_4

    :sswitch_1
    const-string v10, "TTI"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v3, 0x1

    goto :goto_4

    :sswitch_2
    const-string v10, "FULL_LOAD"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v3, 0x2

    goto :goto_4

    .line 462795
    :pswitch_0
    iget-object v3, v8, LX/Baf;->c:Ljava/lang/String;

    iput-object v3, v2, LX/Bag;->a:Ljava/lang/String;

    goto :goto_5

    .line 462796
    :pswitch_1
    iget-object v3, v8, LX/Baf;->c:Ljava/lang/String;

    iput-object v3, v2, LX/Bag;->b:Ljava/lang/String;

    goto :goto_5

    .line 462797
    :pswitch_2
    iget-object v3, v8, LX/Baf;->c:Ljava/lang/String;

    iput-object v3, v2, LX/Bag;->c:Ljava/lang/String;

    goto :goto_5

    .line 462798
    :cond_5
    iget-object v2, v1, LX/2nD;->a:Ljava/util/Map;

    invoke-interface {v2, p2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462799
    iget-object v0, p0, LX/2n3;->t:LX/2nD;

    .line 462800
    iget-object v1, v0, LX/2nD;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 462801
    if-nez v1, :cond_9

    .line 462802
    :cond_6
    goto/16 :goto_0

    .line 462803
    :cond_7
    const/4 v9, 0x0

    :try_start_0
    aget-object v9, v8, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 462804
    const-string v10, "NONE"

    aget-object v11, v8, v12

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    const-string v10, "TTI"

    aget-object v11, v8, v12

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    const-string v10, "FULL_LOAD"

    aget-object v11, v8, v12

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 462805
    :cond_8
    new-instance v3, LX/Baf;

    invoke-direct {v3}, LX/Baf;-><init>()V

    .line 462806
    iput v9, v3, LX/Baf;->a:I

    .line 462807
    aget-object v9, v8, v12

    iput-object v9, v3, LX/Baf;->b:Ljava/lang/String;

    .line 462808
    const/4 v9, 0x2

    aget-object v8, v8, v9

    iput-object v8, v3, LX/Baf;->c:Ljava/lang/String;

    goto/16 :goto_2

    .line 462809
    :catch_0
    goto/16 :goto_2

    .line 462810
    :cond_9
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 462811
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 462812
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bag;

    .line 462813
    iget-object v5, v2, LX/Bag;->a:Ljava/lang/String;

    invoke-static {v5, v3}, LX/2nD;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 462814
    iget-object v5, v2, LX/Bag;->a:Ljava/lang/String;

    invoke-static {v0, v5}, LX/2nD;->d(LX/2nD;Ljava/lang/String;)V

    .line 462815
    iget-object v5, v2, LX/Bag;->a:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 462816
    :cond_b
    iget-object v5, v2, LX/Bag;->b:Ljava/lang/String;

    invoke-static {v5, v3}, LX/2nD;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 462817
    iget-object v5, v2, LX/Bag;->b:Ljava/lang/String;

    invoke-static {v0, v5}, LX/2nD;->d(LX/2nD;Ljava/lang/String;)V

    .line 462818
    iget-object v5, v2, LX/Bag;->b:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 462819
    :cond_c
    iget-object v5, v2, LX/Bag;->c:Ljava/lang/String;

    invoke-static {v5, v3}, LX/2nD;->a(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 462820
    iget-object v5, v2, LX/Bag;->c:Ljava/lang/String;

    invoke-static {v0, v5}, LX/2nD;->d(LX/2nD;Ljava/lang/String;)V

    .line 462821
    iget-object v2, v2, LX/Bag;->c:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x145c9 -> :sswitch_1
        0x24a738 -> :sswitch_0
        0x43e29c96 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 462775
    const-string v0, "watch_browse_ads"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "watch_browse"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2n3;
    .locals 23

    .prologue
    .line 462868
    new-instance v1, LX/2n3;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v4

    check-cast v4, LX/0WV;

    invoke-static/range {p0 .. p0}, LX/1Be;->a(LX/0QB;)LX/1Be;

    move-result-object v5

    check-cast v5, LX/1Be;

    invoke-static/range {p0 .. p0}, LX/1Bn;->a(LX/0QB;)LX/1Bn;

    move-result-object v6

    check-cast v6, LX/1Bn;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2nC;->a(LX/0QB;)LX/2nC;

    move-result-object v8

    check-cast v8, LX/2nC;

    invoke-static/range {p0 .. p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v9

    check-cast v9, LX/13Q;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v10

    check-cast v10, LX/1BA;

    const/16 v11, 0x2fd

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v12

    check-cast v12, LX/14x;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    const/16 v14, 0x375

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/2nD;->a(LX/0QB;)LX/2nD;

    move-result-object v17

    check-cast v17, LX/2nD;

    invoke-static/range {p0 .. p0}, LX/0kY;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v18

    check-cast v18, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/2nE;->a(LX/0QB;)LX/2nE;

    move-result-object v19

    check-cast v19, LX/2nE;

    invoke-static/range {p0 .. p0}, LX/2n4;->a(LX/0QB;)LX/2n4;

    move-result-object v20

    check-cast v20, LX/2n4;

    invoke-static/range {p0 .. p0}, LX/1Bg;->a(LX/0QB;)LX/1Bg;

    move-result-object v21

    check-cast v21, LX/1Bg;

    invoke-static/range {p0 .. p0}, LX/1Br;->a(LX/0QB;)LX/1Br;

    move-result-object v22

    check-cast v22, LX/1Br;

    invoke-direct/range {v1 .. v22}, LX/2n3;-><init>(LX/0WJ;LX/01T;LX/0WV;LX/1Be;LX/1Bn;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2nC;LX/13Q;LX/1BA;LX/0Or;LX/14x;Ljava/lang/Boolean;LX/0Or;LX/0ad;LX/0Uh;LX/2nD;Landroid/os/Handler;LX/2nE;LX/2n4;LX/1Bg;LX/1Br;)V

    .line 462869
    return-object v1
.end method

.method private b(LX/0DW;Ljava/lang/String;)LX/D3R;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 462738
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->e:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 462739
    new-instance v2, LX/D3R;

    invoke-direct {v2}, LX/D3R;-><init>()V

    .line 462740
    if-nez v0, :cond_0

    .line 462741
    iput-boolean v4, v2, LX/D3R;->a:Z

    .line 462742
    const-string v0, "not_in_qe"

    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    move-object v0, v2

    .line 462743
    :goto_0
    return-object v0

    .line 462744
    :cond_0
    iget-object v0, p0, LX/2n3;->g:LX/1Bn;

    invoke-virtual {v0, p2}, LX/1Bn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462745
    iget-object v1, p0, LX/2n3;->v:LX/1Br;

    const/4 v3, 0x0

    .line 462746
    iget-object v5, v1, LX/1Br;->g:LX/0ad;

    sget-short v6, LX/1Bm;->e:S

    const/4 p2, 0x0

    invoke-interface {v5, v6, p2}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_9

    .line 462747
    :cond_1
    :goto_1
    move-object v3, v3

    .line 462748
    if-nez v3, :cond_4

    .line 462749
    iput-boolean v4, v2, LX/D3R;->a:Z

    .line 462750
    iget-object v1, p0, LX/2n3;->v:LX/1Br;

    const/4 v3, 0x0

    .line 462751
    iget-object v4, v1, LX/1Br;->g:LX/0ad;

    sget-short v5, LX/1Bm;->e:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_a

    .line 462752
    :cond_2
    :goto_2
    move-object v0, v3

    .line 462753
    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    .line 462754
    iget-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 462755
    const-string v0, "no_prefetch"

    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    :cond_3
    move-object v0, v2

    .line 462756
    goto :goto_0

    .line 462757
    :cond_4
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->v:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 462758
    if-eqz v1, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "[FB-Only] "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v3, LX/7hu;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v4, v3, LX/7hu;->b:Ljava/lang/String;

    if-eqz v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "[From HTML] "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v3, LX/7hu;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {p1, v0, v4, v1}, LX/0DW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    .line 462759
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->f:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 462760
    if-eqz v0, :cond_5

    .line 462761
    iget v0, v3, LX/7hu;->e:I

    .line 462762
    iget-object v1, p1, LX/0DW;->a:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_PREVIEW_JUMP_DESTINATION"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462763
    :cond_5
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget v1, LX/1Bm;->h:I

    const/16 v4, 0x10

    invoke-interface {v0, v1, v4}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 462764
    iget-object v1, p1, LX/0DW;->a:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_PREVIEW_MAX_LINES"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462765
    iget-object v0, v3, LX/7hu;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 462766
    iget-object v0, v3, LX/7hu;->d:Ljava/lang/String;

    .line 462767
    iget-object v1, p1, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_PREVIEW_FAVICON_URL"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462768
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/D3R;->a:Z

    move-object v0, v2

    .line 462769
    goto/16 :goto_0

    .line 462770
    :cond_7
    iget-object v0, v3, LX/7hu;->a:Ljava/lang/String;

    goto :goto_3

    :cond_8
    iget-object v1, v3, LX/7hu;->c:Ljava/lang/String;

    goto :goto_4

    .line 462771
    :cond_9
    iget-object v5, v1, LX/1Br;->b:Landroid/util/LruCache;

    if-eqz v5, :cond_1

    .line 462772
    iget-object v3, v1, LX/1Br;->b:Landroid/util/LruCache;

    invoke-virtual {v3, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7hu;

    goto/16 :goto_1

    .line 462773
    :cond_a
    iget-object v4, v1, LX/1Br;->c:Landroid/util/LruCache;

    if-eqz v4, :cond_2

    .line 462774
    iget-object v3, v1, LX/1Br;->c:Landroid/util/LruCache;

    invoke-virtual {v3, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public static b(LX/2n3;)Z
    .locals 1

    .prologue
    .line 462737
    iget-object v0, p0, LX/2n3;->e:LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2n3;->e:LX/14x;

    invoke-virtual {v0}, LX/14x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/0DW;Ljava/lang/String;)LX/D3R;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 462715
    new-instance v2, LX/D3R;

    invoke-direct {v2}, LX/D3R;-><init>()V

    .line 462716
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->t:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 462717
    if-nez v0, :cond_0

    .line 462718
    iput-boolean v6, v2, LX/D3R;->a:Z

    .line 462719
    const-string v0, "not_in_qe"

    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    move-object v0, v2

    .line 462720
    :goto_0
    return-object v0

    .line 462721
    :cond_0
    iget-object v0, p0, LX/2n3;->r:LX/2nE;

    .line 462722
    iget-object v1, v0, LX/2nE;->a:Landroid/util/LruCache;

    invoke-virtual {v1, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7hz;

    move-object v1, v1

    .line 462723
    if-nez v1, :cond_1

    .line 462724
    iput-boolean v6, v2, LX/D3R;->a:Z

    .line 462725
    const-string v0, "no_og_info"

    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    move-object v0, v2

    .line 462726
    goto :goto_0

    .line 462727
    :cond_1
    iget-object v0, v1, LX/7hz;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v3, p0, LX/2n3;->o:LX/0ad;

    sget v4, LX/1Bm;->s:I

    const/16 v5, 0x14

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 462728
    iput-boolean v6, v2, LX/D3R;->a:Z

    .line 462729
    const-string v0, "body_too_short"

    iput-object v0, v2, LX/D3R;->b:Ljava/lang/String;

    move-object v0, v2

    .line 462730
    goto :goto_0

    .line 462731
    :cond_2
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v3, LX/1Bm;->v:S

    invoke-interface {v0, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 462732
    iget-object v0, p0, LX/2n3;->g:LX/1Bn;

    invoke-virtual {v0, p2}, LX/1Bn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 462733
    if-eqz v3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "[FB-Only] "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, LX/7hz;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v5, p0, LX/2n3;->s:LX/2n4;

    const-string v6, ""

    invoke-virtual {v5, v4, v6}, LX/2n4;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[From OG] "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, LX/7hz;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {p1, v0, v4, v1}, LX/0DW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    .line 462734
    const/4 v0, 0x1

    iput-boolean v0, v2, LX/D3R;->a:Z

    move-object v0, v2

    .line 462735
    goto :goto_0

    .line 462736
    :cond_3
    iget-object v0, v1, LX/7hz;->a:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v1, v1, LX/7hz;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method private d(LX/0DW;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 462696
    iget-object v1, p0, LX/2n3;->f:LX/1Be;

    invoke-virtual {v1}, LX/1Be;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 462697
    :cond_0
    :goto_0
    return-object v0

    .line 462698
    :cond_1
    iget-object v1, p0, LX/2n3;->f:LX/1Be;

    .line 462699
    :try_start_0
    iget-object v2, v1, LX/1Be;->j:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 462700
    iget-object v2, v1, LX/1Be;->j:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 462701
    :goto_1
    iget-object v1, p0, LX/2n3;->g:LX/1Bn;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, LX/1Bn;->a(Ljava/lang/String;Z)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    move-result-object v1

    .line 462702
    if-eqz v1, :cond_0

    .line 462703
    iget-object v0, p1, LX/0DW;->a:Landroid/content/Intent;

    const-string v2, "BrowserLiteIntent.EXTRA_PREFETCH_INFO"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 462704
    iget-object v0, p0, LX/2n3;->f:LX/1Be;

    .line 462705
    iget-object v2, v1, Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;->a:Ljava/lang/String;

    move-object v1, v2

    .line 462706
    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 462707
    iget-object p0, v0, LX/1Be;->k:LX/0ad;

    sget-short p1, LX/1Bi;->n:S

    invoke-interface {p0, p1, v3}, LX/0ad;->a(SZ)Z

    move-result p0

    if-nez p0, :cond_3

    .line 462708
    :cond_2
    :goto_2
    const-string v0, "html"

    goto :goto_0

    :catch_0
    goto :goto_1

    .line 462709
    :cond_3
    if-eqz v2, :cond_5

    iget-object p0, v0, LX/1Be;->k:LX/0ad;

    sget-short p1, LX/1Bi;->o:S

    invoke-interface {p0, p1, v3}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 462710
    :goto_3
    if-nez v3, :cond_4

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 462711
    :cond_4
    new-instance p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;

    invoke-direct {p0, v0, p2, v1, v3}, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;-><init>(LX/1Be;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 462712
    iget-object v3, v0, LX/1Be;->j:Landroid/os/Handler;

    move-object v3, v3

    .line 462713
    const p1, -0x5a659e02

    invoke-static {v3, p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_2

    .line 462714
    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public static e(LX/2n3;)Z
    .locals 3

    .prologue
    .line 462695
    iget-object v0, p0, LX/2n3;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2n3;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 462515
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 462516
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->C:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 462517
    if-eqz v0, :cond_1

    .line 462518
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/browser/lite/BrowserLiteFallbackActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 462519
    :goto_0
    new-instance v0, LX/0DW;

    invoke-direct {v0}, LX/0DW;-><init>()V

    .line 462520
    const-string v1, "custom_user_agent_suffix"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 462521
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_26

    .line 462522
    const-string v3, "custom_user_agent_suffix"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462523
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/2n3;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 462524
    :goto_1
    move-object v1, v1

    .line 462525
    invoke-virtual {v0, v1}, LX/0DW;->e(Ljava/lang/String;)LX/0DW;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, LX/0DW;->a(Ljava/util/Locale;)LX/0DW;

    move-result-object v12

    .line 462526
    const-string v0, "BrowserLiteIntent.EXTRA_ANIMATION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 462527
    const v0, 0x7f0400db

    const v1, 0x7f040030

    const v3, 0x7f0400b6

    const v4, 0x7f0400ee

    invoke-virtual {v12, v0, v1, v3, v4}, LX/0DW;->a(IIII)LX/0DW;

    .line 462528
    :cond_0
    iget-object v0, p0, LX/2n3;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 462529
    invoke-virtual {v12, v5}, LX/0DW;->b(Z)LX/0DW;

    .line 462530
    invoke-virtual {v12}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 462531
    :goto_2
    return-void

    .line 462532
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    .line 462533
    :cond_2
    iget-object v0, p0, LX/2n3;->u:LX/1Bg;

    invoke-virtual {v0, v12}, LX/1Bg;->a(LX/0DW;)V

    .line 462534
    iget-object v0, p0, LX/2n3;->f:LX/1Be;

    .line 462535
    invoke-static {v0}, LX/1Be;->o(LX/1Be;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1Be;->G:Ljava/util/Map;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1Be;->G:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 462536
    :cond_3
    const/4 v1, 0x0

    .line 462537
    :goto_3
    move-object v4, v1

    .line 462538
    if-nez v4, :cond_27

    .line 462539
    const/4 v0, 0x0

    .line 462540
    :goto_4
    move v9, v0

    .line 462541
    iget-object v0, p0, LX/2n3;->p:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_2a

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 462542
    if-eqz v0, :cond_4

    .line 462543
    const/4 v0, 0x0

    invoke-virtual {v12, v0}, LX/0DW;->b(Z)LX/0DW;

    .line 462544
    invoke-virtual {v12}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    goto :goto_2

    .line 462545
    :cond_4
    const-string v0, "should_hide_menu"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    .line 462546
    const-string v0, "should_hide_menu"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462547
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->y:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 462548
    iget-object v1, p0, LX/2n3;->o:LX/0ad;

    sget-short v3, LX/1Bm;->z:S

    invoke-interface {v1, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 462549
    iget-object v3, p0, LX/2n3;->o:LX/0ad;

    sget-short v4, LX/1Bm;->w:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 462550
    iget-object v4, p0, LX/2n3;->o:LX/0ad;

    sget-short v6, LX/1Bm;->A:S

    invoke-interface {v4, v6, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 462551
    if-eqz v4, :cond_1d

    .line 462552
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081cf3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 462553
    invoke-static {v12, v0, v11, v5}, LX/2n3;->a(LX/0DW;Ljava/lang/String;ZI)V

    .line 462554
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02080f

    const-string v3, "SHARE_TIMELINE"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462555
    invoke-static {p0}, LX/2n3;->b(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 462556
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, LX/2n3;->j:Z

    if-eqz v0, :cond_2b

    const v0, 0x7f081cf0

    :goto_6
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020174

    const-string v3, "SHARE_MESSENGER"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462557
    :cond_5
    const-string v0, "MENU_OPEN_WITH"

    .line 462558
    const/4 v1, -0x1

    const/4 v3, 0x0

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462559
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ced

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02016e

    const-string v3, "COPY_LINK"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462560
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081cf1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020183

    const-string v3, "SAVE_LINK"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462561
    invoke-static {p0}, LX/2n3;->e(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 462562
    const-string v0, "Clear Debug Overlay"

    const v1, 0x7f02016a

    const-string v3, "CLEAR_DEBUG_OVERLAY"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462563
    :cond_6
    iget-object v0, p0, LX/2n3;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 462564
    const/4 v0, 0x1

    invoke-virtual {v12, v0}, LX/0DW;->c(Z)LX/0DW;

    .line 462565
    const-string v0, "[fb-only] Open in Main Process"

    const v1, 0x7f020177

    const-string v3, "OPEN_IN_MAIN_PROCESS"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462566
    :cond_7
    :goto_7
    const-string v0, "messenger"

    const-string v1, "iab_origin"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 462567
    const-string v0, "iab_origin"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462568
    iget-boolean v0, p0, LX/2n3;->j:Z

    if-eqz v0, :cond_24

    const-string v0, "THEME_WORK_CHAT"

    :goto_8
    invoke-virtual {v12, v0}, LX/0DW;->d(Ljava/lang/String;)LX/0DW;

    .line 462569
    invoke-virtual {v12, v5}, LX/0DW;->b(Z)LX/0DW;

    .line 462570
    :cond_8
    const-string v0, "post_url_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462571
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 462572
    const-string v1, "post_url_data"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462573
    iget-object v1, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_POST_DATA"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462574
    :cond_9
    const-string v0, "extra_js_to_execute"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462575
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 462576
    const-string v1, "extra_js_to_execute"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 462577
    iget-object v1, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462578
    :cond_a
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 462579
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 462580
    const-string v3, "session_id"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462581
    invoke-virtual {v12, v0}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    .line 462582
    move-object v0, v1

    .line 462583
    invoke-direct {p0, p1, v0}, LX/2n3;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 462584
    const/4 v0, 0x1

    .line 462585
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 462586
    if-eqz v1, :cond_2c

    .line 462587
    :cond_b
    :goto_9
    move v0, v0

    .line 462588
    if-eqz v0, :cond_c

    .line 462589
    invoke-virtual {v12, v11}, LX/0DW;->a(Z)LX/0DW;

    .line 462590
    :cond_c
    const/4 v0, 0x0

    .line 462591
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_d

    iget-object v1, p0, LX/2n3;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2n3;->a:LX/0Tn;

    invoke-interface {v1, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v0, 0x1

    :cond_d
    move v0, v0

    .line 462592
    if-eqz v0, :cond_e

    .line 462593
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_IN_APP_BROWSER_PROFILING_ENABLED"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462594
    :cond_e
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 462595
    move v0, v0

    .line 462596
    if-eqz v0, :cond_f

    .line 462597
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_QUOTE_SHARE_ENTRY_POINT_ENABLED"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462598
    :cond_f
    invoke-static {p0}, LX/2n3;->e(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 462599
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_DEBUG_OVERLAY_ENABLED"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462600
    :cond_10
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget v1, LX/1Bm;->S:I

    invoke-interface {v0, v1, v5}, LX/0ad;->a(II)I

    move-result v0

    .line 462601
    if-eqz v0, :cond_11

    .line 462602
    iget-object v1, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_USE_ALTERNATIVE_TITLE_BAR_COLOR_SCHEME"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462603
    :cond_11
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget v1, LX/1Bm;->Y:I

    const/4 v3, -0x1

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    .line 462604
    if-lez v0, :cond_12

    .line 462605
    iget-object v1, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_VIDEO_TIME_SPENT_INTERVAL"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462606
    :cond_12
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 462607
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->T:S

    const/4 v6, 0x0

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 462608
    if-eqz v0, :cond_13

    .line 462609
    invoke-virtual {v12, v4}, LX/0DW;->i(Z)LX/0DW;

    .line 462610
    :cond_13
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->O:S

    const/4 v6, 0x0

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 462611
    if-eqz v0, :cond_2e

    .line 462612
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_ENABLED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462613
    :cond_14
    :goto_a
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 462614
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v3, LX/1Bm;->W:S

    invoke-interface {v0, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 462615
    if-eqz v0, :cond_17

    .line 462616
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_TEXT_ZOOM_ENABLED"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462617
    iget-object v0, p0, LX/2n3;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1C0;->i:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 462618
    iget-object v3, p0, LX/2n3;->o:LX/0ad;

    sget-short v4, LX/1Bm;->X:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 462619
    iget-object v3, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v4, "BrowserLiteIntent.EXTRA_ULTRA_TEXT_ZOOM_OUT_ENABLED"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462620
    :cond_15
    :goto_b
    if-eq v0, v1, :cond_16

    iget-object v1, p0, LX/2n3;->o:LX/0ad;

    sget-short v3, LX/1Bm;->U:S

    invoke-interface {v1, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 462621
    iget-object v1, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_SAVED_TEXT_ZOOM_LEVEL"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462622
    :cond_16
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->V:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 462623
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_TEXT_AUTOSIZING_ENABLED"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462624
    :cond_17
    invoke-static {v2}, LX/1H1;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 462625
    invoke-direct {p0, v12, v1}, LX/2n3;->d(LX/0DW;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 462626
    const/4 v7, 0x0

    .line 462627
    if-nez v2, :cond_19

    .line 462628
    iget-object v0, p0, LX/2n3;->f:LX/1Be;

    .line 462629
    invoke-virtual {v0}, LX/1Be;->b()Z

    move-result v3

    if-nez v3, :cond_36

    .line 462630
    sget-object v3, LX/1Bu;->DISABLED:LX/1Bu;

    .line 462631
    :cond_18
    :goto_c
    move-object v0, v3

    .line 462632
    invoke-virtual {v0}, LX/1Bu;->toLogString()Ljava/lang/String;

    move-result-object v7

    .line 462633
    iget-object v0, p0, LX/2n3;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_19

    .line 462634
    if-eqz v7, :cond_19

    .line 462635
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v3, "BrowserLiteIntent.EXTRA_NO_PREFETCH_REASON"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462636
    :cond_19
    const-string v0, "iab_click_source"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 462637
    const-string v0, "browser_metrics_join_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 462638
    invoke-direct {p0, v12, v1}, LX/2n3;->b(LX/0DW;Ljava/lang/String;)LX/D3R;

    move-result-object v8

    .line 462639
    iget-boolean v0, v8, LX/D3R;->a:Z

    if-nez v0, :cond_1a

    .line 462640
    invoke-direct {p0, v12, v1}, LX/2n3;->c(LX/0DW;Ljava/lang/String;)LX/D3R;

    .line 462641
    :cond_1a
    iget-object v0, p0, LX/2n3;->k:LX/2nC;

    const-string v4, "tracking_codes"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v6, v8, LX/D3R;->a:Z

    iget-object v8, v8, LX/D3R;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v10}, LX/2nC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 462642
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 462643
    invoke-static {v3}, LX/2n3;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_37

    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->I:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 462644
    invoke-virtual {v12, v4}, LX/0DW;->d(Z)LX/0DW;

    .line 462645
    :cond_1b
    :goto_d
    iget-object v0, p0, LX/2n3;->q:LX/0Uh;

    const/16 v1, 0xcb

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 462646
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_LOG_FB_TRACKING_REQUEST"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462647
    :cond_1c
    invoke-virtual {v12}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 462648
    :cond_1d
    if-nez v0, :cond_1e

    if-eqz v1, :cond_21

    .line 462649
    :cond_1e
    if-eqz v1, :cond_1f

    .line 462650
    const v0, 0x7f0209c5

    invoke-virtual {v12, v0}, LX/0DW;->f(I)LX/0DW;

    .line 462651
    :cond_1f
    if-nez v3, :cond_20

    move v0, v11

    :goto_e
    invoke-direct {p0, v12, p2, v11, v0}, LX/2n3;->a(LX/0DW;Landroid/content/Context;ZZ)V

    goto/16 :goto_7

    :cond_20
    move v0, v5

    goto :goto_e

    .line 462652
    :cond_21
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081cf3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 462653
    if-eqz v3, :cond_25

    .line 462654
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081cf4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 462655
    const v0, 0x7f0209c5

    .line 462656
    :goto_f
    invoke-static {v12, v1, v5, v0}, LX/2n3;->a(LX/0DW;Ljava/lang/String;ZI)V

    .line 462657
    if-nez v3, :cond_23

    move v0, v11

    :goto_10
    invoke-direct {p0, v12, p2, v5, v0}, LX/2n3;->a(LX/0DW;Landroid/content/Context;ZZ)V

    .line 462658
    if-eqz v3, :cond_7

    .line 462659
    invoke-static {p0}, LX/2n3;->b(LX/2n3;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 462660
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02080f

    const-string v3, "SHARE_TIMELINE"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462661
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, LX/2n3;->j:Z

    if-eqz v0, :cond_38

    const v0, 0x7f081cf0

    :goto_11
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020174

    const-string v3, "SHARE_MESSENGER"

    invoke-virtual {v12, v0, v1, v3}, LX/0DW;->a(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 462662
    :cond_22
    goto/16 :goto_7

    :cond_23
    move v0, v5

    .line 462663
    goto :goto_10

    .line 462664
    :cond_24
    const-string v0, "THEME_MESSENGER_FB4A"

    goto/16 :goto_8

    :cond_25
    move-object v1, v0

    move v0, v5

    goto :goto_f

    :cond_26
    iget-object v1, p0, LX/2n3;->d:Ljava/lang/String;

    goto/16 :goto_1

    .line 462665
    :cond_27
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v3

    .line 462666
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_12
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 462667
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v12, v1, v0}, LX/0DW;->a(Ljava/lang/String;Ljava/util/ArrayList;)LX/0DW;

    goto :goto_12

    .line 462668
    :cond_28
    invoke-interface {v4}, Ljava/util/Map;->clear()V

    move v0, v3

    .line 462669
    goto/16 :goto_4

    :cond_29
    iget-object v1, v0, LX/1Be;->G:Ljava/util/Map;

    goto/16 :goto_3

    :cond_2a
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 462670
    :cond_2b
    const v0, 0x7f081cef

    goto/16 :goto_6

    :cond_2c
    iget-object v1, p0, LX/2n3;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v1, v3, :cond_2d

    iget-object v1, p0, LX/2n3;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/6G4;->c:LX/0Tn;

    invoke-interface {v1, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_b

    :cond_2d
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 462671
    :cond_2e
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->R:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 462672
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462673
    goto/16 :goto_a

    .line 462674
    :cond_2f
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->Q:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 462675
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_WITH_BACK_ARROW_ENABLED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462676
    goto/16 :goto_a

    .line 462677
    :cond_30
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->P:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 462678
    iget-object v0, v12, LX/0DW;->a:Landroid/content/Intent;

    const-string v1, "BrowserLiteIntent.EXTRA_IS_BURD_V1_WHITE_CHROME_ENABLED"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462679
    invoke-virtual {v12, v4}, LX/0DW;->i(Z)LX/0DW;

    goto/16 :goto_a

    .line 462680
    :cond_31
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->K:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 462681
    const-string v0, "BrowserLiteIntent.CLOSE_BUTTON_ICON_BACK_ARROW"

    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_BRIGHT"

    invoke-virtual {v12, v0, v1}, LX/0DW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    goto/16 :goto_a

    .line 462682
    :cond_32
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->L:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 462683
    const-string v0, "BrowserLiteIntent.CLOSE_BUTTON_ICON_CROSS"

    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_BRIGHT"

    invoke-virtual {v12, v0, v1}, LX/0DW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    goto/16 :goto_a

    .line 462684
    :cond_33
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->M:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 462685
    const-string v0, "BrowserLiteIntent.CLOSE_BUTTON_ICON_BACK_ARROW"

    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_DARK"

    invoke-virtual {v12, v0, v1}, LX/0DW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    goto/16 :goto_a

    .line 462686
    :cond_34
    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->N:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 462687
    const-string v0, "BrowserLiteIntent.CLOSE_BUTTON_ICON_CROSS"

    const-string v1, "BrowserLiteIntent.URL_TEXT_COLOR_DARK"

    invoke-virtual {v12, v0, v1}, LX/0DW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0DW;

    goto/16 :goto_a

    .line 462688
    :cond_35
    if-eq v0, v1, :cond_15

    const/16 v3, 0x64

    if-ge v0, v3, :cond_15

    move v0, v1

    .line 462689
    goto/16 :goto_b

    .line 462690
    :cond_36
    iget-object v3, v0, LX/1Be;->F:Landroid/util/LruCache;

    invoke-virtual {v3, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Bu;

    .line 462691
    if-nez v3, :cond_18

    sget-object v3, LX/1Bu;->NO_HOOKUP:LX/1Bu;

    goto/16 :goto_c

    .line 462692
    :cond_37
    invoke-static {v3}, LX/2n3;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LX/2n3;->o:LX/0ad;

    sget-short v1, LX/1Bm;->J:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 462693
    invoke-virtual {v12, v4}, LX/0DW;->d(Z)LX/0DW;

    goto/16 :goto_d

    .line 462694
    :cond_38
    const v0, 0x7f081cef

    goto/16 :goto_11
.end method
