.class public LX/2GU;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/localstats/LocalStatsHistogramContainer;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/2GV;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 388706
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2GV;
    .locals 5

    .prologue
    .line 388707
    sget-object v0, LX/2GU;->a:LX/2GV;

    if-nez v0, :cond_1

    .line 388708
    const-class v1, LX/2GU;

    monitor-enter v1

    .line 388709
    :try_start_0
    sget-object v0, LX/2GU;->a:LX/2GV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388710
    if-eqz v2, :cond_0

    .line 388711
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388712
    invoke-static {v0}, LX/13R;->a(LX/0QB;)LX/13S;

    move-result-object v3

    check-cast v3, LX/13S;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object p0

    check-cast p0, Ljava/util/Random;

    invoke-static {v3, v4, p0}, LX/1B8;->b(LX/13S;LX/0W3;Ljava/util/Random;)LX/2GV;

    move-result-object v3

    move-object v0, v3

    .line 388713
    sput-object v0, LX/2GU;->a:LX/2GV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388714
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388715
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388716
    :cond_1
    sget-object v0, LX/2GU;->a:LX/2GV;

    return-object v0

    .line 388717
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 388719
    invoke-static {p0}, LX/13R;->a(LX/0QB;)LX/13S;

    move-result-object v0

    check-cast v0, LX/13S;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v2

    check-cast v2, Ljava/util/Random;

    invoke-static {v0, v1, v2}, LX/1B8;->b(LX/13S;LX/0W3;Ljava/util/Random;)LX/2GV;

    move-result-object v0

    return-object v0
.end method
