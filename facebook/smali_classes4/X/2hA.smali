.class public final enum LX/2hA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2hA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2hA;

.field public static final enum ENTITY_CARDS:LX/2hA;

.field public static final enum FEED_FRIENDABLE_HEADER:LX/2hA;

.field public static final enum FRIENDING_CARD:LX/2hA;

.field public static final enum FRIENDING_RADAR:LX/2hA;

.field public static final enum FRIENDS_CENTER_FRIENDS:LX/2hA;

.field public static final enum FRIENDS_CENTER_REQUESTS:LX/2hA;

.field public static final enum FRIENDS_CENTER_SEARCH:LX/2hA;

.field public static final enum FRIENDS_TAB:LX/2hA;

.field public static final enum FRIEND_LIST_PROFILE:LX/2hA;

.field public static final enum FRIEND_REQUEST_PUSH_NOTIFICATION:LX/2hA;

.field public static final enum INCOMING_FR_QP:LX/2hA;

.field public static final enum MEMORIAL_CONTACT_TOOLS:LX/2hA;

.field public static final enum MESSENGER:LX/2hA;

.field public static final enum MOBILE_JEWEL:LX/2hA;

.field public static final enum NEARBY_FRIENDS:LX/2hA;

.field public static final enum NEWSFEED_FRIEND_REQUESTS:LX/2hA;

.field public static final enum NUX:LX/2hA;

.field public static final enum PROFILE:LX/2hA;

.field public static final enum PROFILE_BROWSER:LX/2hA;

.field public static final enum PROFILE_BROWSER_EVENTS:LX/2hA;

.field public static final enum PYMK_FEED:LX/2hA;

.field public static final enum QR_CODE:LX/2hA;

.field public static final enum QUICK_PROMOTION:LX/2hA;

.field public static final enum SEARCH:LX/2hA;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 449831
    new-instance v0, LX/2hA;

    const-string v1, "ENTITY_CARDS"

    const-string v2, "entity_cards"

    invoke-direct {v0, v1, v4, v2}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->ENTITY_CARDS:LX/2hA;

    .line 449832
    new-instance v0, LX/2hA;

    const-string v1, "FEED_FRIENDABLE_HEADER"

    const-string v2, "feed_friendable_header"

    invoke-direct {v0, v1, v5, v2}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FEED_FRIENDABLE_HEADER:LX/2hA;

    .line 449833
    new-instance v0, LX/2hA;

    const-string v1, "FRIEND_LIST_PROFILE"

    const-string v2, "friend_list_profile"

    invoke-direct {v0, v1, v6, v2}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIEND_LIST_PROFILE:LX/2hA;

    .line 449834
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDING_CARD"

    const-string v2, "friending_card"

    invoke-direct {v0, v1, v7, v2}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDING_CARD:LX/2hA;

    .line 449835
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDING_RADAR"

    const-string v2, "friending_radar"

    invoke-direct {v0, v1, v8, v2}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDING_RADAR:LX/2hA;

    .line 449836
    new-instance v0, LX/2hA;

    const-string v1, "FRIEND_REQUEST_PUSH_NOTIFICATION"

    const/4 v2, 0x5

    const-string v3, "friend_request_push_notification"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIEND_REQUEST_PUSH_NOTIFICATION:LX/2hA;

    .line 449837
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDS_CENTER_FRIENDS"

    const/4 v2, 0x6

    const-string v3, "friends_center_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDS_CENTER_FRIENDS:LX/2hA;

    .line 449838
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDS_CENTER_REQUESTS"

    const/4 v2, 0x7

    const-string v3, "m_find_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    .line 449839
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDS_CENTER_SEARCH"

    const/16 v2, 0x8

    const-string v3, "friends_center_search"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDS_CENTER_SEARCH:LX/2hA;

    .line 449840
    new-instance v0, LX/2hA;

    const-string v1, "FRIENDS_TAB"

    const/16 v2, 0x9

    const-string v3, "friends_tab"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->FRIENDS_TAB:LX/2hA;

    .line 449841
    new-instance v0, LX/2hA;

    const-string v1, "INCOMING_FR_QP"

    const/16 v2, 0xa

    const-string v3, "incoming_fr_qp"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->INCOMING_FR_QP:LX/2hA;

    .line 449842
    new-instance v0, LX/2hA;

    const-string v1, "MEMORIAL_CONTACT_TOOLS"

    const/16 v2, 0xb

    const-string v3, "memorial_contact_tools"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->MEMORIAL_CONTACT_TOOLS:LX/2hA;

    .line 449843
    new-instance v0, LX/2hA;

    const-string v1, "MESSENGER"

    const/16 v2, 0xc

    const-string v3, "messenger"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->MESSENGER:LX/2hA;

    .line 449844
    new-instance v0, LX/2hA;

    const-string v1, "MOBILE_JEWEL"

    const/16 v2, 0xd

    const-string v3, "m_jewel"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    .line 449845
    new-instance v0, LX/2hA;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xe

    const-string v3, "nearby_friends"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->NEARBY_FRIENDS:LX/2hA;

    .line 449846
    new-instance v0, LX/2hA;

    const-string v1, "NEWSFEED_FRIEND_REQUESTS"

    const/16 v2, 0xf

    const-string v3, "newsfeed_friend_requests"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->NEWSFEED_FRIEND_REQUESTS:LX/2hA;

    .line 449847
    new-instance v0, LX/2hA;

    const-string v1, "NUX"

    const/16 v2, 0x10

    const-string v3, "/gettingstarted.php"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->NUX:LX/2hA;

    .line 449848
    new-instance v0, LX/2hA;

    const-string v1, "PROFILE"

    const/16 v2, 0x11

    const-string v3, "/profile.php"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->PROFILE:LX/2hA;

    .line 449849
    new-instance v0, LX/2hA;

    const-string v1, "PROFILE_BROWSER"

    const/16 v2, 0x12

    const-string v3, "profile_browser"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->PROFILE_BROWSER:LX/2hA;

    .line 449850
    new-instance v0, LX/2hA;

    const-string v1, "PROFILE_BROWSER_EVENTS"

    const/16 v2, 0x13

    const-string v3, "profile_browser_events"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->PROFILE_BROWSER_EVENTS:LX/2hA;

    .line 449851
    new-instance v0, LX/2hA;

    const-string v1, "PYMK_FEED"

    const/16 v2, 0x14

    const-string v3, "pymk_feed"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->PYMK_FEED:LX/2hA;

    .line 449852
    new-instance v0, LX/2hA;

    const-string v1, "QR_CODE"

    const/16 v2, 0x15

    const-string v3, "qr_code"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->QR_CODE:LX/2hA;

    .line 449853
    new-instance v0, LX/2hA;

    const-string v1, "QUICK_PROMOTION"

    const/16 v2, 0x16

    const-string v3, "quick_promotion"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->QUICK_PROMOTION:LX/2hA;

    .line 449854
    new-instance v0, LX/2hA;

    const-string v1, "SEARCH"

    const/16 v2, 0x17

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/2hA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2hA;->SEARCH:LX/2hA;

    .line 449855
    const/16 v0, 0x18

    new-array v0, v0, [LX/2hA;

    sget-object v1, LX/2hA;->ENTITY_CARDS:LX/2hA;

    aput-object v1, v0, v4

    sget-object v1, LX/2hA;->FEED_FRIENDABLE_HEADER:LX/2hA;

    aput-object v1, v0, v5

    sget-object v1, LX/2hA;->FRIEND_LIST_PROFILE:LX/2hA;

    aput-object v1, v0, v6

    sget-object v1, LX/2hA;->FRIENDING_CARD:LX/2hA;

    aput-object v1, v0, v7

    sget-object v1, LX/2hA;->FRIENDING_RADAR:LX/2hA;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2hA;->FRIEND_REQUEST_PUSH_NOTIFICATION:LX/2hA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2hA;->FRIENDS_CENTER_FRIENDS:LX/2hA;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2hA;->FRIENDS_CENTER_SEARCH:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2hA;->FRIENDS_TAB:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2hA;->INCOMING_FR_QP:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2hA;->MEMORIAL_CONTACT_TOOLS:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2hA;->MESSENGER:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2hA;->MOBILE_JEWEL:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2hA;->NEARBY_FRIENDS:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2hA;->NEWSFEED_FRIEND_REQUESTS:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2hA;->NUX:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/2hA;->PROFILE:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/2hA;->PROFILE_BROWSER:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/2hA;->PROFILE_BROWSER_EVENTS:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/2hA;->PYMK_FEED:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/2hA;->QR_CODE:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/2hA;->QUICK_PROMOTION:LX/2hA;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/2hA;->SEARCH:LX/2hA;

    aput-object v2, v0, v1

    sput-object v0, LX/2hA;->$VALUES:[LX/2hA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449856
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449857
    iput-object p3, p0, LX/2hA;->value:Ljava/lang/String;

    .line 449858
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2hA;
    .locals 1

    .prologue
    .line 449859
    const-class v0, LX/2hA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2hA;

    return-object v0
.end method

.method public static values()[LX/2hA;
    .locals 1

    .prologue
    .line 449860
    sget-object v0, LX/2hA;->$VALUES:[LX/2hA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2hA;

    return-object v0
.end method
