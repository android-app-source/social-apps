.class public final LX/3AT;
.super LX/3AU;
.source ""


# instance fields
.field private final a:Ljava/io/File;

.field private final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/3AS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/io/File;[LX/3AS;)V
    .locals 1

    .prologue
    .line 525289
    invoke-direct {p0}, LX/3AU;-><init>()V

    .line 525290
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/3AT;->a:Ljava/io/File;

    .line 525291
    invoke-static {p2}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3AT;->b:LX/0Rf;

    .line 525292
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 525294
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, LX/3AT;->a:Ljava/io/File;

    iget-object v2, p0, LX/3AT;->b:LX/0Rf;

    sget-object v3, LX/3AS;->APPEND:LX/3AS;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 525293
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Files.asByteSink("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3AT;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3AT;->b:LX/0Rf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
