.class public LX/2QQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile v:LX/2QQ;


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public d:J

.field private e:Ljava/lang/String;

.field public f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$RefreshCallback;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Landroid/content/Context;

.field public final m:LX/0aG;

.field public final n:Ljava/util/concurrent/Executor;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/2QR;

.field private final q:LX/0SF;

.field public final r:LX/03V;

.field public final s:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final t:LX/0Sh;

.field public final u:LX/0W9;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0aG;Ljava/util/concurrent/Executor;LX/2QR;LX/0SF;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Sh;LX/0W9;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0aG;",
            "Ljava/util/concurrent/Executor;",
            "LX/2QR;",
            "LX/0SF;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408021
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/2QQ;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 408022
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/2QQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 408023
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/2QQ;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 408024
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2QQ;->d:J

    .line 408025
    iput-object p1, p0, LX/2QQ;->l:Landroid/content/Context;

    .line 408026
    iput-object p2, p0, LX/2QQ;->m:LX/0aG;

    .line 408027
    iput-object p3, p0, LX/2QQ;->n:Ljava/util/concurrent/Executor;

    .line 408028
    iput-object p4, p0, LX/2QQ;->p:LX/2QR;

    .line 408029
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->f:Ljava/util/HashMap;

    .line 408030
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->g:Ljava/util/HashMap;

    .line 408031
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    .line 408032
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->i:Ljava/util/List;

    .line 408033
    iput-object p5, p0, LX/2QQ;->q:LX/0SF;

    .line 408034
    iput-object p6, p0, LX/2QQ;->r:LX/03V;

    .line 408035
    iput-object p7, p0, LX/2QQ;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 408036
    iput-object p8, p0, LX/2QQ;->o:LX/0Or;

    .line 408037
    iput-object p9, p0, LX/2QQ;->t:LX/0Sh;

    .line 408038
    iput-object p10, p0, LX/2QQ;->u:LX/0W9;

    .line 408039
    return-void
.end method

.method public static a(LX/0QB;)LX/2QQ;
    .locals 14

    .prologue
    .line 408045
    sget-object v0, LX/2QQ;->v:LX/2QQ;

    if-nez v0, :cond_1

    .line 408046
    const-class v1, LX/2QQ;

    monitor-enter v1

    .line 408047
    :try_start_0
    sget-object v0, LX/2QQ;->v:LX/2QQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408048
    if-eqz v2, :cond_0

    .line 408049
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408050
    new-instance v3, LX/2QQ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/2QR;->a(LX/0QB;)LX/2QR;

    move-result-object v7

    check-cast v7, LX/2QR;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SF;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v11, 0x15e7

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v13

    check-cast v13, LX/0W9;

    invoke-direct/range {v3 .. v13}, LX/2QQ;-><init>(Landroid/content/Context;LX/0aG;Ljava/util/concurrent/Executor;LX/2QR;LX/0SF;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Sh;LX/0W9;)V

    .line 408051
    move-object v0, v3

    .line 408052
    sput-object v0, LX/2QQ;->v:LX/2QQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408053
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408054
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408055
    :cond_1
    sget-object v0, LX/2QQ;->v:LX/2QQ;

    return-object v0

    .line 408056
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408057
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 408058
    iget-object v0, p0, LX/2QQ;->m:LX/0aG;

    const v1, -0x19b85fa7

    invoke-static {v0, p2, p1, v1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 408059
    return-object v0
.end method

.method private a(Ljava/util/ArrayList;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 408060
    const/4 v1, 0x0

    .line 408061
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 408062
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 408063
    const-string v1, "platform_urls_to_delete"

    invoke-virtual {v2, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    move v1, v0

    .line 408064
    :cond_0
    if-eqz p2, :cond_2

    .line 408065
    const-string v1, "delete_orphaned_files_flag"

    invoke-virtual {v2, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 408066
    :goto_0
    if-nez v0, :cond_1

    .line 408067
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 408068
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, LX/2QQ;->m:LX/0aG;

    const-string v1, "platform_cleanup_cached_webdialogs"

    const v3, 0x1ab0eca1

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 408069
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v3

    .line 408070
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 408071
    :goto_0
    const-string v4, "%1$s%2$s%3$s=%4$s"

    if-eqz v0, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    const-string v0, "&"

    :goto_2
    const-string v5, "platformurlversion"

    invoke-static {v4, v3, v0, v5, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 408072
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 408073
    const-string v4, "locale"

    invoke-virtual {p0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 408074
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 408075
    :goto_3
    const-string v4, "%1$s%2$s%3$s=%4$s"

    if-eqz v1, :cond_5

    move-object v2, v0

    :goto_4
    if-eqz v1, :cond_6

    const-string v0, "&"

    :goto_5
    const-string v1, "locale"

    invoke-static {v4, v2, v0, v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 408076
    :cond_0
    const-string v1, "?#"

    invoke-static {v1}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v1

    invoke-static {v1}, LX/2Cb;->on(LX/1IA;)LX/2Cb;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    .line 408077
    const-string v2, "%1$s?%2$s#%3$s"

    const-string v4, ""

    invoke-static {v1, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 408078
    goto :goto_0

    .line 408079
    :cond_2
    const-string v3, ""

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_2

    :cond_4
    move v1, v2

    .line 408080
    goto :goto_3

    .line 408081
    :cond_5
    const-string v0, ""

    move-object v2, v0

    goto :goto_4

    :cond_6
    const-string v0, ""

    goto :goto_5
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 408082
    iget-object v0, p0, LX/2QQ;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;-><init>(LX/2QQ;Z)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 408083
    return-void
.end method

.method public static a(LX/2QQ;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 408084
    iget-object v0, p0, LX/2QQ;->t:LX/0Sh;

    const-string v3, "Cannot kick off refreshes on non-UI threads."

    invoke-virtual {v0, v3}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 408085
    iget-object v0, p0, LX/2QQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408086
    invoke-direct {p0, v1}, LX/2QQ;->a(Z)V

    move v0, v1

    .line 408087
    :goto_0
    return v0

    .line 408088
    :cond_0
    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    .line 408089
    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 408090
    goto :goto_0

    .line 408091
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408092
    :cond_2
    if-nez p2, :cond_3

    iget-object v0, p0, LX/2QQ;->p:LX/2QR;

    .line 408093
    iget-boolean v3, v0, LX/2QR;->n:Z

    move v0, v3

    .line 408094
    if-nez v0, :cond_3

    move v0, v2

    .line 408095
    goto :goto_0

    .line 408096
    :cond_3
    iget-object v0, p0, LX/2QQ;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 408097
    goto :goto_0

    .line 408098
    :cond_4
    iget-object v0, p0, LX/2QQ;->q:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/2QQ;->d:J

    .line 408099
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 408100
    const-string v2, "platform_webdialogs_manifest_fetch_URL"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 408101
    const-string v2, "platform_webdialogs_manifest_fetch"

    invoke-direct {p0, v0, v2}, LX/2QQ;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408102
    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/HY1;

    invoke-direct {v2, p0, p2}, LX/HY1;-><init>(LX/2QQ;Ljava/lang/String;)V

    iget-object v3, p0, LX/2QQ;->n:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move v0, v1

    .line 408103
    goto :goto_0
.end method

.method public static a$redex0(LX/2QQ;Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 408104
    iget-object v0, p0, LX/2QQ;->t:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 408105
    iget-object v0, p0, LX/2QQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 408106
    iget-object v0, p0, LX/2QQ;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 408107
    if-eqz p1, :cond_1

    .line 408108
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->b()J

    move-result-wide v0

    iput-wide v0, p0, LX/2QQ;->d:J

    .line 408109
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->e:Ljava/lang/String;

    .line 408110
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->a()Ljava/lang/Iterable;

    move-result-object v0

    .line 408111
    if-eqz v0, :cond_0

    .line 408112
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 408113
    iget-object v2, p0, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 408114
    :cond_0
    invoke-static {p0}, LX/2QQ;->g$redex0(LX/2QQ;)V

    .line 408115
    :cond_1
    iget-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 408116
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 408117
    iget-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 408118
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HXy;

    .line 408119
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408120
    if-eqz v0, :cond_6

    .line 408121
    invoke-virtual {p0, v0}, LX/2QQ;->a(Ljava/lang/String;)Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    move-result-object v4

    .line 408122
    :goto_2
    if-nez v4, :cond_2

    .line 408123
    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 408124
    goto :goto_1

    .line 408125
    :cond_2
    invoke-virtual {v1, v4, v7}, LX/HXy;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Z)V

    .line 408126
    invoke-virtual {v1}, LX/HXy;->a()V

    goto :goto_1

    .line 408127
    :cond_3
    iput-object v5, p0, LX/2QQ;->h:Ljava/util/HashMap;

    .line 408128
    if-eqz v2, :cond_4

    .line 408129
    invoke-static {p0}, LX/2QQ;->h(LX/2QQ;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, LX/2QQ;->a(LX/2QQ;Ljava/lang/String;Ljava/lang/String;)Z

    .line 408130
    :cond_4
    iget-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 408131
    invoke-virtual {p0}, LX/2QQ;->a()V

    .line 408132
    invoke-direct {p0, v3, v8}, LX/2QQ;->a(Ljava/util/ArrayList;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408133
    :cond_5
    return-void

    :cond_6
    move-object v4, v3

    goto :goto_2
.end method

.method public static e(LX/2QQ;)V
    .locals 2

    .prologue
    .line 408040
    iget-object v0, p0, LX/2QQ;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 408041
    iget-object v0, p0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 408042
    iget-object v0, p0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 408043
    const/4 v0, 0x0

    iput-object v0, p0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408044
    :cond_0
    return-void
.end method

.method public static f(LX/2QQ;)V
    .locals 3

    .prologue
    .line 408008
    iget-object v0, p0, LX/2QQ;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408009
    iget-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HXy;

    .line 408010
    invoke-virtual {v0}, LX/HXy;->a()V

    goto :goto_0

    .line 408011
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->h:Ljava/util/HashMap;

    .line 408012
    invoke-direct {p0}, LX/2QQ;->j()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408013
    :goto_1
    return-void

    .line 408014
    :cond_1
    iget-object v0, p0, LX/2QQ;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 408015
    sget-object v1, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->FETCHING:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;)V

    .line 408016
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 408017
    const-string v2, "platform_webview_actionmanifest"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 408018
    const-string v2, "platform_webdialog_fetch"

    invoke-direct {p0, v1, v2}, LX/2QQ;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408019
    iget-object v1, p0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/HY2;

    invoke-direct {v2, p0, v0}, LX/HY2;-><init>(LX/2QQ;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V

    iget-object v0, p0, LX/2QQ;->n:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method public static g$redex0(LX/2QQ;)V
    .locals 5

    .prologue
    .line 407997
    iget-object v1, p0, LX/2QQ;->g:Ljava/util/HashMap;

    .line 407998
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->g:Ljava/util/HashMap;

    .line 407999
    iget-object v0, p0, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 408000
    invoke-virtual {v0}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c()Ljava/lang/String;

    move-result-object v3

    .line 408001
    iget-object v4, p0, LX/2QQ;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408002
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 408003
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 408004
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408005
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    sget-object v4, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->NOT_FETCHED:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {v0, v4}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;)V

    goto :goto_1

    .line 408006
    :cond_1
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/2QQ;->a(Ljava/util/ArrayList;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 408007
    return-void
.end method

.method public static h(LX/2QQ;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 407991
    iget-object v0, p0, LX/2QQ;->l:Landroid/content/Context;

    const-string v1, "https://m.%s/js_dialog_resources/dialog_descriptions_android.json"

    invoke-static {v0, v1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407992
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#locale=%s"

    iget-object v2, p0, LX/2QQ;->u:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 407993
    iget-object v1, p0, LX/2QQ;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2QQ;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 407994
    invoke-virtual {p0}, LX/2QQ;->b()V

    .line 407995
    :cond_0
    iput-object v0, p0, LX/2QQ;->e:Ljava/lang/String;

    .line 407996
    iget-object v0, p0, LX/2QQ;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407984
    new-instance v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;

    invoke-direct {v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;-><init>()V

    .line 407985
    iget-wide v2, p0, LX/2QQ;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->a(J)V

    .line 407986
    iget-object v1, p0, LX/2QQ;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->a(Ljava/lang/String;)V

    .line 407987
    iget-object v1, p0, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->a(Ljava/util/ArrayList;)V

    .line 407988
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 407989
    const-string v2, "platform_webdialogs_save_parcel"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 407990
    iget-object v0, p0, LX/2QQ;->m:LX/0aG;

    const-string v2, "platform_webdialogs_save_manifest"

    const v3, -0x601a3c92

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;
    .locals 3

    .prologue
    .line 407978
    iget-object v0, p0, LX/2QQ;->t:LX/0Sh;

    const-string v1, "Must check the manifest on the UI thread. This is avoid race conditions"

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 407979
    invoke-static {p0}, LX/2QQ;->h(LX/2QQ;)Ljava/lang/String;

    .line 407980
    iget-object v0, p0, LX/2QQ;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 407981
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->d()Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    move-result-object v1

    sget-object v2, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->FETCHED:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    if-eq v1, v2, :cond_0

    .line 407982
    const/4 v0, 0x0

    .line 407983
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 407966
    iget-object v0, p0, LX/2QQ;->t:LX/0Sh;

    const-string v1, "Cannot refresh the manifest off the UI thread."

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 407967
    iget-object v0, p0, LX/2QQ;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 407968
    :cond_0
    :goto_0
    return-void

    .line 407969
    :cond_1
    iget-object v0, p0, LX/2QQ;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2QS;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 407970
    if-nez v0, :cond_0

    .line 407971
    iget-object v0, p0, LX/2QQ;->p:LX/2QR;

    .line 407972
    iget-boolean v1, v0, LX/2QR;->o:Z

    move v0, v1

    .line 407973
    if-nez v0, :cond_0

    .line 407974
    invoke-static {p0}, LX/2QQ;->h(LX/2QQ;)Ljava/lang/String;

    move-result-object v0

    .line 407975
    iget-object v1, p0, LX/2QQ;->q:LX/0SF;

    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/2QQ;->d:J

    sub-long/2addr v2, v4

    .line 407976
    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 407977
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/2QQ;->a(LX/2QQ;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 407957
    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 407958
    iget-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 407959
    const/4 v0, 0x0

    iput-object v0, p0, LX/2QQ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407960
    :cond_0
    invoke-static {p0}, LX/2QQ;->e(LX/2QQ;)V

    .line 407961
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2QQ;->d:J

    .line 407962
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2QQ;->f:Ljava/util/HashMap;

    .line 407963
    invoke-static {p0}, LX/2QQ;->g$redex0(LX/2QQ;)V

    .line 407964
    invoke-direct {p0}, LX/2QQ;->j()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407965
    return-void
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 407955
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2QQ;->a(Z)V

    .line 407956
    return-void
.end method
