.class public final LX/37r;
.super LX/37s;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/37l;)V
    .locals 0

    .prologue
    .line 502370
    invoke-direct {p0, p1, p2}, LX/37s;-><init>(Landroid/content/Context;LX/37l;)V

    .line 502371
    return-void
.end method


# virtual methods
.method public final a(LX/38G;LX/38H;)V
    .locals 1

    .prologue
    .line 502360
    invoke-super {p0, p1, p2}, LX/37s;->a(LX/38G;LX/38H;)V

    .line 502361
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502362
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getDescription()Ljava/lang/CharSequence;

    move-result-object p0

    move-object v0, p0

    .line 502363
    if-eqz v0, :cond_0

    .line 502364
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 502365
    iget-object p0, p2, LX/38H;->a:Landroid/os/Bundle;

    const-string p1, "status"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 502366
    :cond_0
    return-void
.end method

.method public final a(LX/67Q;)V
    .locals 2

    .prologue
    .line 502355
    invoke-super {p0, p1}, LX/37s;->a(LX/67Q;)V

    .line 502356
    iget-object v0, p1, LX/67Q;->b:Ljava/lang/Object;

    iget-object v1, p1, LX/67Q;->a:LX/384;

    .line 502357
    iget-object p0, v1, LX/384;->e:Ljava/lang/String;

    move-object v1, p0

    .line 502358
    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setDescription(Ljava/lang/CharSequence;)V

    .line 502359
    return-void
.end method

.method public final a(LX/38G;)Z
    .locals 1

    .prologue
    .line 502367
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502368
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isConnecting()Z

    move-result p0

    move v0, p0

    .line 502369
    return v0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 502348
    iget-boolean v1, p0, LX/37t;->g:Z

    if-eqz v1, :cond_0

    .line 502349
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    iget-object v2, p0, LX/37t;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/3Fl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502350
    :cond_0
    iput-boolean v0, p0, LX/37r;->g:Z

    .line 502351
    iget-object v1, p0, LX/37t;->a:Ljava/lang/Object;

    iget v2, p0, LX/37t;->e:I

    iget-object v3, p0, LX/37t;->b:Ljava/lang/Object;

    iget-boolean v4, p0, LX/37t;->f:Z

    if-eqz v4, :cond_1

    :goto_0
    or-int/lit8 v0, v0, 0x2

    .line 502352
    check-cast v1, Landroid/media/MediaRouter;

    check-cast v3, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;I)V

    .line 502353
    return-void

    .line 502354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 502345
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    const v1, 0x800003

    .line 502346
    check-cast v0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    .line 502347
    return-void
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 502342
    iget-object v0, p0, LX/37t;->a:Ljava/lang/Object;

    .line 502343
    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object p0

    move-object v0, p0

    .line 502344
    return-object v0
.end method
