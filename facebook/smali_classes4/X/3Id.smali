.class public final LX/3Id;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;)V
    .locals 0

    .prologue
    .line 546906
    iput-object p1, p0, LX/3Id;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546917
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 546907
    check-cast p1, LX/2ou;

    .line 546908
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 546909
    iget-object v0, p0, LX/3Id;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a()V

    .line 546910
    :cond_0
    :goto_0
    return-void

    .line 546911
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 546912
    iget-object v0, p0, LX/3Id;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->j()V

    goto :goto_0

    .line 546913
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 546914
    iget-object v0, p0, LX/3Id;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a()V

    goto :goto_0

    .line 546915
    :cond_3
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 546916
    iget-object v0, p0, LX/3Id;->a:Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->i()V

    goto :goto_0
.end method
