.class public LX/2Vt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Vt;


# instance fields
.field private final a:LX/2Os;

.field private final b:LX/2Vu;

.field private final c:LX/2UW;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/2Os;LX/2Vu;LX/2UW;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 418188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418189
    iput-object p1, p0, LX/2Vt;->a:LX/2Os;

    .line 418190
    iput-object p2, p0, LX/2Vt;->b:LX/2Vu;

    .line 418191
    iput-object p3, p0, LX/2Vt;->c:LX/2UW;

    .line 418192
    iput-object p4, p0, LX/2Vt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 418193
    return-void
.end method

.method public static a(LX/0QB;)LX/2Vt;
    .locals 7

    .prologue
    .line 418175
    sget-object v0, LX/2Vt;->e:LX/2Vt;

    if-nez v0, :cond_1

    .line 418176
    const-class v1, LX/2Vt;

    monitor-enter v1

    .line 418177
    :try_start_0
    sget-object v0, LX/2Vt;->e:LX/2Vt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 418178
    if-eqz v2, :cond_0

    .line 418179
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 418180
    new-instance p0, LX/2Vt;

    invoke-static {v0}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v3

    check-cast v3, LX/2Os;

    invoke-static {v0}, LX/2Vu;->a(LX/0QB;)LX/2Vu;

    move-result-object v4

    check-cast v4, LX/2Vu;

    invoke-static {v0}, LX/2UW;->a(LX/0QB;)LX/2UW;

    move-result-object v5

    check-cast v5, LX/2UW;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Vt;-><init>(LX/2Os;LX/2Vu;LX/2UW;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 418181
    move-object v0, p0

    .line 418182
    sput-object v0, LX/2Vt;->e:LX/2Vt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418183
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 418184
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 418185
    :cond_1
    sget-object v0, LX/2Vt;->e:LX/2Vt;

    return-object v0

    .line 418186
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 418187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 418152
    iget-object v0, p0, LX/2Vt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Vv;->d:LX/0Tn;

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 418153
    if-nez v0, :cond_0

    .line 418154
    const/4 v0, 0x0

    .line 418155
    :goto_0
    return-object v0

    .line 418156
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "mswitch_accounts_state"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 418157
    const-string v1, "mswitch_accounts"

    .line 418158
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 418159
    const-string v1, "accounts_count"

    iget-object v2, p0, LX/2Vt;->a:LX/2Os;

    invoke-virtual {v2}, LX/2Os;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418160
    const-string v1, "accounts_with_dbl"

    iget-object v2, p0, LX/2Vt;->b:LX/2Vu;

    invoke-virtual {v2}, LX/2Vu;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418161
    const-string v1, "current_total_unseen_count"

    iget-object v2, p0, LX/2Vt;->c:LX/2UW;

    .line 418162
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 418163
    iget-object v3, v2, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/2Vv;->g:LX/0Tn;

    invoke-interface {v3, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Tn;

    .line 418164
    sget-object p1, LX/2Vv;->g:LX/0Tn;

    invoke-virtual {v3, p1}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object p1

    .line 418165
    iget-object p2, v2, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 p3, 0x0

    invoke-interface {p2, v3, p3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 418166
    invoke-virtual {v5, p1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 418167
    :cond_1
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v5, v3

    .line 418168
    const/4 v3, 0x0

    .line 418169
    invoke-virtual {v5}, LX/0P1;->values()LX/0Py;

    move-result-object v5

    invoke-virtual {v5}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v3

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 418170
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v5

    move v5, v3

    .line 418171
    goto :goto_2

    .line 418172
    :cond_2
    move v2, v5

    .line 418173
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 418174
    const-string v1, "current_tab_badge_count"

    iget-object v2, p0, LX/2Vt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2Vv;->f:LX/0Tn;

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_0
.end method
