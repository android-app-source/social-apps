.class public LX/2d5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0Tn;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Tn;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            "Ljava/lang/Class",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 442910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442911
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2d5;->a:LX/0Tn;

    .line 442912
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LX/2d5;->b:Ljava/lang/Class;

    .line 442913
    return-void
.end method

.method public static a(LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)LX/2d5;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Tn;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/2d5",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 442914
    new-instance v1, LX/2d5;

    invoke-virtual {p0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-direct {v1, v0, p2}, LX/2d5;-><init>(LX/0Tn;Ljava/lang/Class;)V

    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 442915
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/2d5;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/2d5;

    iget-object v0, v0, LX/2d5;->a:LX/0Tn;

    iget-object v1, p0, LX/2d5;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, LX/2d5;

    iget-object v0, p1, LX/2d5;->b:Ljava/lang/Class;

    iget-object v1, p0, LX/2d5;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 442916
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/2d5;->a:LX/0Tn;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/2d5;->b:Ljava/lang/Class;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 442917
    const-string v0, "ObjectPrefKey: %s, type: %s"

    iget-object v1, p0, LX/2d5;->a:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2d5;->b:Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
