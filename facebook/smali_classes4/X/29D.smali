.class public LX/29D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/29D;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/0kb;

.field public final d:LX/0Uo;

.field public final e:LX/29E;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 375929
    const-class v0, LX/29D;

    sput-object v0, LX/29D;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0kb;LX/0Uo;LX/29E;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375931
    iput-object p1, p0, LX/29D;->b:LX/0Zb;

    .line 375932
    iput-object p2, p0, LX/29D;->c:LX/0kb;

    .line 375933
    iput-object p3, p0, LX/29D;->d:LX/0Uo;

    .line 375934
    iput-object p4, p0, LX/29D;->e:LX/29E;

    .line 375935
    return-void
.end method

.method public static a(LX/0QB;)LX/29D;
    .locals 7

    .prologue
    .line 375936
    sget-object v0, LX/29D;->f:LX/29D;

    if-nez v0, :cond_1

    .line 375937
    const-class v1, LX/29D;

    monitor-enter v1

    .line 375938
    :try_start_0
    sget-object v0, LX/29D;->f:LX/29D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375939
    if-eqz v2, :cond_0

    .line 375940
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 375941
    new-instance p0, LX/29D;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v5

    check-cast v5, LX/0Uo;

    invoke-static {v0}, LX/29E;->b(LX/0QB;)LX/29E;

    move-result-object v6

    check-cast v6, LX/29E;

    invoke-direct {p0, v3, v4, v5, v6}, LX/29D;-><init>(LX/0Zb;LX/0kb;LX/0Uo;LX/29E;)V

    .line 375942
    move-object v0, p0

    .line 375943
    sput-object v0, LX/29D;->f:LX/29D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375944
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375945
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375946
    :cond_1
    sget-object v0, LX/29D;->f:LX/29D;

    return-object v0

    .line 375947
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ZLjava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375949
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "reason"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 375950
    iget-object v1, p0, LX/29D;->c:LX/0kb;

    .line 375951
    iget-wide v4, v1, LX/0kb;->x:J

    move-wide v2, v4

    .line 375952
    const-string v1, "network_session_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375953
    const-string v1, "mqtt_service_state"

    .line 375954
    if-eqz v0, :cond_0

    .line 375955
    iget-object v4, p0, LX/29D;->e:LX/29E;

    invoke-virtual {v4, v0}, LX/29E;->a(Ljava/util/Map;)V

    .line 375956
    :cond_0
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "mqtt_client"

    .line 375957
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 375958
    move-object v5, v5

    .line 375959
    invoke-virtual {v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "inet_session_id"

    iget-object v7, p0, LX/29D;->c:LX/0kb;

    .line 375960
    iget-wide v9, v7, LX/0kb;->w:J

    move-wide v7, v9

    .line 375961
    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "app_session_id"

    iget-object v7, p0, LX/29D;->d:LX/0Uo;

    .line 375962
    iget-wide v9, v7, LX/0Uo;->J:J

    move-wide v7, v9

    .line 375963
    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "app_bg"

    iget-object v5, p0, LX/29D;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->j()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "1"

    :goto_0
    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 375964
    const-string v6, "service_name"

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 375965
    const-string v6, "service_name"

    const-string v7, "MQTT"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375966
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 375967
    iget-object v6, p0, LX/29D;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 375968
    return-void

    .line 375969
    :cond_2
    const-string v5, "0"

    goto :goto_0
.end method
