.class public LX/2Pd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Pd;


# instance fields
.field public final a:LX/2Pe;

.field public final b:LX/0Sh;


# direct methods
.method public constructor <init>(LX/2Pe;LX/0Sh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 406862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406863
    iput-object p1, p0, LX/2Pd;->a:LX/2Pe;

    .line 406864
    iput-object p2, p0, LX/2Pd;->b:LX/0Sh;

    .line 406865
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pd;
    .locals 5

    .prologue
    .line 406866
    sget-object v0, LX/2Pd;->c:LX/2Pd;

    if-nez v0, :cond_1

    .line 406867
    const-class v1, LX/2Pd;

    monitor-enter v1

    .line 406868
    :try_start_0
    sget-object v0, LX/2Pd;->c:LX/2Pd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406869
    if-eqz v2, :cond_0

    .line 406870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406871
    new-instance p0, LX/2Pd;

    invoke-static {v0}, LX/2Pe;->a(LX/0QB;)LX/2Pe;

    move-result-object v3

    check-cast v3, LX/2Pe;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/2Pd;-><init>(LX/2Pe;LX/0Sh;)V

    .line 406872
    move-object v0, p0

    .line 406873
    sput-object v0, LX/2Pd;->c:LX/2Pd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406876
    :cond_1
    sget-object v0, LX/2Pd;->c:LX/2Pd;

    return-object v0

    .line 406877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/util/Set;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/CSH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 406879
    iget-object v0, p0, LX/2Pd;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 406880
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, LX/2Ph;->b:LX/0U1;

    .line 406881
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 406882
    aput-object v0, v2, v9

    sget-object v0, LX/2Ph;->a:LX/0U1;

    .line 406883
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 406884
    aput-object v0, v2, v8

    const/4 v0, 0x2

    sget-object v1, LX/2Ph;->c:LX/0U1;

    .line 406885
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 406886
    aput-object v1, v2, v0

    const/4 v0, 0x3

    sget-object v1, LX/2Ph;->d:LX/0U1;

    .line 406887
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 406888
    aput-object v1, v2, v0

    .line 406889
    iget-object v0, p0, LX/2Pd;->a:LX/2Pe;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "offline_lwi_table"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 406890
    sget-object v0, LX/2Ph;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 406891
    sget-object v0, LX/2Ph;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 406892
    sget-object v0, LX/2Ph;->c:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 406893
    sget-object v0, LX/2Ph;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 406894
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 406895
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406896
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 406897
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 406898
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v8

    .line 406899
    :goto_1
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 406900
    new-instance v12, LX/CSH;

    invoke-direct {v12, v7, v10, v0, v11}, LX/CSH;-><init>(Ljava/lang/String;Ljava/lang/String;Z[B)V

    invoke-interface {v6, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 406901
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v9

    .line 406902
    goto :goto_1

    .line 406903
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 406904
    return-object v6
.end method
