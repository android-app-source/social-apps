.class public LX/2iR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/23P;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2do;

.field public final d:LX/2hb;

.field public final e:LX/2dp;

.field public final f:LX/2hc;

.field private final g:LX/1mR;

.field public final h:Landroid/content/res/Resources;

.field public final i:LX/2iQ;

.field public j:J

.field public k:Z


# direct methods
.method public constructor <init>(LX/23P;LX/0Ot;LX/2do;LX/2hb;LX/2dp;LX/2hc;LX/1mR;LX/0ad;Landroid/content/res/Resources;LX/2iQ;)V
    .locals 4
    .param p10    # LX/2iQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/23P;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/2do;",
            "LX/2hb;",
            "LX/2dp;",
            "LX/2hc;",
            "LX/1mR;",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            "LX/2iQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 451573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451574
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2iR;->j:J

    .line 451575
    iput-object p1, p0, LX/2iR;->a:LX/23P;

    .line 451576
    iput-object p2, p0, LX/2iR;->b:LX/0Ot;

    .line 451577
    iput-object p3, p0, LX/2iR;->c:LX/2do;

    .line 451578
    iput-object p4, p0, LX/2iR;->d:LX/2hb;

    .line 451579
    iput-object p5, p0, LX/2iR;->e:LX/2dp;

    .line 451580
    iput-object p6, p0, LX/2iR;->f:LX/2hc;

    .line 451581
    iput-object p7, p0, LX/2iR;->g:LX/1mR;

    .line 451582
    iput-object p9, p0, LX/2iR;->h:Landroid/content/res/Resources;

    .line 451583
    iput-object p10, p0, LX/2iR;->i:LX/2iQ;

    .line 451584
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->Off:LX/0c1;

    sget-short v2, LX/2ez;->j:S

    const/4 v3, 0x0

    invoke-interface {p8, v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2iR;->k:Z

    .line 451585
    return-void
.end method

.method public static a(LX/2iR;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # LX/2iR;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 451586
    iget-object v0, p0, LX/2iR;->a:LX/23P;

    iget-object v1, p0, LX/2iR;->h:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2iR;)V
    .locals 2

    .prologue
    .line 451587
    iget-object v0, p0, LX/2iR;->g:LX/1mR;

    const-string v1, "REQUESTS_TAB_PYMI_QUERY_TAG"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 451588
    return-void
.end method

.method public static a(Lcom/facebook/fbui/widget/contentview/ContentView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 451589
    const v0, 0x7f02032d

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 451590
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 451591
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 451592
    const-string v0, "%s %s"

    invoke-static {v0, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 451593
    return-void
.end method
