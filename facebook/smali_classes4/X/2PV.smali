.class public final LX/2PV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cM;


# instance fields
.field public final synthetic a:LX/2PR;


# direct methods
.method public constructor <init>(LX/2PR;)V
    .locals 0

    .prologue
    .line 406597
    iput-object p1, p0, LX/2PV;->a:LX/2PR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0cK;Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 406598
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    iget-object v0, v0, LX/2PR;->i:LX/0Uh;

    const/16 v1, 0x77

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 406599
    :cond_0
    :goto_0
    return-void

    .line 406600
    :cond_1
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "key_message_action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406601
    const-string v1, "action_messenger_user_log_out"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 406602
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    invoke-static {v0}, LX/2PR;->h(LX/2PR;)V

    goto :goto_0

    .line 406603
    :cond_2
    const-string v1, "action_badge_count_update"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406604
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "key_user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406605
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "key_messenger_badge_count"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 406606
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, LX/2PV;->a:LX/2PR;

    iget-object v2, v2, LX/2PR;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-gez v1, :cond_4

    .line 406607
    :cond_3
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    invoke-static {v0}, LX/2PR;->h(LX/2PR;)V

    goto :goto_0

    .line 406608
    :cond_4
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    iget-object v0, v0, LX/2PR;->e:Landroid/os/Handler;

    iget-object v2, p0, LX/2PV;->a:LX/2PR;

    iget-object v2, v2, LX/2PR;->n:Ljava/lang/Runnable;

    invoke-static {v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 406609
    iget-object v2, p0, LX/2PV;->a:LX/2PR;

    monitor-enter v2

    .line 406610
    :try_start_0
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    const/4 v3, 0x1

    .line 406611
    iput-boolean v3, v0, LX/2PR;->p:Z

    .line 406612
    iget-object v0, p0, LX/2PV;->a:LX/2PR;

    iget-object v0, v0, LX/2PR;->f:LX/0xB;

    sget-object v3, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v0, v3, v1}, LX/0xB;->a(LX/12j;I)V

    .line 406613
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
