.class public LX/2nD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2nD;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/Bag;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463558
    iput-object p1, p0, LX/2nD;->b:LX/0Or;

    .line 463559
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2nD;->a:Ljava/util/Map;

    .line 463560
    return-void
.end method

.method public static a(LX/0QB;)LX/2nD;
    .locals 4

    .prologue
    .line 463561
    sget-object v0, LX/2nD;->c:LX/2nD;

    if-nez v0, :cond_1

    .line 463562
    const-class v1, LX/2nD;

    monitor-enter v1

    .line 463563
    :try_start_0
    sget-object v0, LX/2nD;->c:LX/2nD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463564
    if-eqz v2, :cond_0

    .line 463565
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463566
    new-instance v3, LX/2nD;

    const/16 p0, 0x122d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2nD;-><init>(LX/0Or;)V

    .line 463567
    move-object v0, v3

    .line 463568
    sput-object v0, LX/2nD;->c:LX/2nD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463569
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463570
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463571
    :cond_1
    sget-object v0, LX/2nD;->c:LX/2nD;

    return-object v0

    .line 463572
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Set;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 463574
    if-eqz p0, :cond_0

    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/2nD;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 463575
    iget-object v0, p0, LX/2nD;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 463576
    iput-object p1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 463577
    move-object v0, v0

    .line 463578
    invoke-virtual {v0}, LX/0gt;->b()V

    .line 463579
    return-void
.end method
