.class public final LX/3Bs;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/PersonYouMayKnow;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 0

    .prologue
    .line 529174
    iput-object p1, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-boolean p2, p0, LX/3Bs;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 529175
    iget-object v0, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 529176
    invoke-static {v0, p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Throwable;)V

    .line 529177
    iget-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 529178
    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->E(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 529179
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 529180
    check-cast p1, Ljava/util/List;

    .line 529181
    iget-object v0, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v1, p0, LX/3Bs;->a:Z

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    .line 529182
    iget-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aE:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aO:Z

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 529183
    iget-object v4, v3, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_0
    move v3, v4

    .line 529184
    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 529185
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, LX/8DG;

    if-eqz v3, :cond_0

    .line 529186
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, LX/8DG;

    const-string v4, "people_you_may_know"

    invoke-interface {v3, v4}, LX/8DG;->b(Ljava/lang/String;)V

    .line 529187
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aO:Z

    .line 529188
    if-eqz v1, :cond_1

    .line 529189
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v3}, LX/2iK;->e()V

    .line 529190
    :cond_1
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v3}, LX/2dj;->d()Z

    move-result v3

    if-nez v3, :cond_2

    .line 529191
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 529192
    :cond_2
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 529193
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 529194
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 529195
    if-eqz v0, :cond_3

    .line 529196
    iget-boolean v3, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    move v3, v3

    .line 529197
    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_3

    .line 529198
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 529199
    :cond_4
    iget-object v0, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 529200
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 529201
    if-eqz v5, :cond_5

    iget-object v7, v0, LX/2iK;->l:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 529202
    iget-object v7, v0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 529203
    iget-object v7, v0, LX/2iK;->l:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 529204
    :cond_6
    invoke-virtual {v0}, LX/2iK;->m()V

    .line 529205
    const v5, -0x8d5315d

    invoke-static {v0, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 529206
    iget-object v0, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, p0, LX/3Bs;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v1}, LX/2iK;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    .line 529207
    invoke-static {v0, v1, v2}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 529208
    return-void

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0
.end method
