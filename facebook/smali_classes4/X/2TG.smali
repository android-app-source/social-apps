.class public LX/2TG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2TG;


# instance fields
.field public a:LX/1ES;


# direct methods
.method public constructor <init>(LX/1ES;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413877
    iput-object p1, p0, LX/2TG;->a:LX/1ES;

    .line 413878
    return-void
.end method

.method public static a(LX/0QB;)LX/2TG;
    .locals 4

    .prologue
    .line 413879
    sget-object v0, LX/2TG;->b:LX/2TG;

    if-nez v0, :cond_1

    .line 413880
    const-class v1, LX/2TG;

    monitor-enter v1

    .line 413881
    :try_start_0
    sget-object v0, LX/2TG;->b:LX/2TG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413882
    if-eqz v2, :cond_0

    .line 413883
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413884
    new-instance p0, LX/2TG;

    invoke-static {v0}, LX/1ES;->a(LX/0QB;)LX/1ES;

    move-result-object v3

    check-cast v3, LX/1ES;

    invoke-direct {p0, v3}, LX/2TG;-><init>(LX/1ES;)V

    .line 413885
    move-object v0, p0

    .line 413886
    sput-object v0, LX/2TG;->b:LX/2TG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413887
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413888
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413889
    :cond_1
    sget-object v0, LX/2TG;->b:LX/2TG;

    return-object v0

    .line 413890
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 413892
    iget-object v0, p0, LX/2TG;->a:LX/1ES;

    const-class v1, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v1, v2, v3}, LX/1ES;->a(Ljava/lang/Class;J)V

    .line 413893
    return-void
.end method
