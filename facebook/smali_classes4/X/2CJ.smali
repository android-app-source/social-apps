.class public final LX/2CJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 0

    .prologue
    .line 382636
    iput-object p1, p0, LX/2CJ;->a:LX/2CH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x187ad01c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 382637
    const-string v1, "event"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object v1

    .line 382638
    sget-object v2, LX/2Z5;->a:[I

    invoke-virtual {v1}, LX/2EU;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 382639
    :goto_0
    const v1, -0x19d6bff1

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 382640
    :pswitch_0
    iget-object v1, p0, LX/2CJ;->a:LX/2CH;

    .line 382641
    iget-object v3, v1, LX/2CH;->n:LX/13Q;

    const-string v4, "presence_map_reset_on_mqtt_disconnect"

    invoke-virtual {v3, v4}, LX/13Q;->a(Ljava/lang/String;)V

    .line 382642
    iget-object v3, v1, LX/2CH;->o:LX/1BA;

    const v4, 0x59000f

    invoke-virtual {v3, v4}, LX/1BA;->a(I)V

    .line 382643
    iget-object v4, v1, LX/2CH;->G:LX/2AK;

    iget-object v3, v1, LX/2CH;->k:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v5

    iput-wide v5, v4, LX/2AK;->d:J

    .line 382644
    invoke-static {v1}, LX/2CH;->w(LX/2CH;)V

    .line 382645
    sget-object v3, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    iput-object v3, v1, LX/2CH;->J:LX/2AL;

    .line 382646
    const/4 v3, 0x1

    invoke-static {v1, v3}, LX/2CH;->b(LX/2CH;Z)V

    .line 382647
    goto :goto_0

    .line 382648
    :pswitch_1
    iget-object v1, p0, LX/2CJ;->a:LX/2CH;

    .line 382649
    invoke-virtual {v1}, LX/2CH;->b()V

    .line 382650
    sget-object v2, LX/2AL;->MQTT_CONNECTED_WAITING_FOR_PRESENCE:LX/2AL;

    iput-object v2, v1, LX/2CH;->J:LX/2AL;

    .line 382651
    iget-object v2, v1, LX/2CH;->x:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Mg;

    .line 382652
    invoke-virtual {v2}, LX/3Mg;->b()V

    goto :goto_1

    .line 382653
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
