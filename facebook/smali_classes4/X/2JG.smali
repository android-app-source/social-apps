.class public LX/2JG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0VT;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jz;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0UW;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0VT;LX/0Or;LX/0UW;)V
    .locals 0
    .param p4    # LX/0UW;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "LX/0Or",
            "<",
            "LX/2Jz;",
            ">;",
            "LX/0UW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392745
    iput-object p1, p0, LX/2JG;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 392746
    iput-object p2, p0, LX/2JG;->c:LX/0VT;

    .line 392747
    iput-object p3, p0, LX/2JG;->d:LX/0Or;

    .line 392748
    iput-object p4, p0, LX/2JG;->e:LX/0UW;

    .line 392749
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 392743
    iget-object v0, p0, LX/2JG;->e:LX/0UW;

    invoke-interface {v0}, LX/0UW;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/2JG;->c:LX/0VT;

    invoke-virtual {v0}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 392742
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392736
    iget-object v0, p0, LX/2JG;->d:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 5

    .prologue
    .line 392738
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    .line 392739
    const/4 v1, 0x1

    iget-object v2, p0, LX/2JG;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2Jx;->e:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 392740
    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    .line 392741
    :cond_0
    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 392737
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method
