.class public final enum LX/3DP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3DP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3DP;

.field public static final enum FIRST:LX/3DP;

.field public static final enum LAST:LX/3DP;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 534701
    new-instance v0, LX/3DP;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, LX/3DP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DP;->FIRST:LX/3DP;

    .line 534702
    new-instance v0, LX/3DP;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v3}, LX/3DP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DP;->LAST:LX/3DP;

    .line 534703
    const/4 v0, 0x2

    new-array v0, v0, [LX/3DP;

    sget-object v1, LX/3DP;->FIRST:LX/3DP;

    aput-object v1, v0, v2

    sget-object v1, LX/3DP;->LAST:LX/3DP;

    aput-object v1, v0, v3

    sput-object v0, LX/3DP;->$VALUES:[LX/3DP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 534704
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3DP;
    .locals 1

    .prologue
    .line 534705
    const-class v0, LX/3DP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3DP;

    return-object v0
.end method

.method public static values()[LX/3DP;
    .locals 1

    .prologue
    .line 534706
    sget-object v0, LX/3DP;->$VALUES:[LX/3DP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3DP;

    return-object v0
.end method
