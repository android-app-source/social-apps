.class public LX/2Ht;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;
.implements LX/2AX;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Ht;


# instance fields
.field public volatile a:Ljava/util/concurrent/atomic/AtomicInteger;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0Xl;

.field public final c:LX/0lB;

.field private final d:LX/2Hu;

.field public final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/6XE;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xl;LX/0lB;LX/2Hu;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391174
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/2Ht;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 391175
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LX/2Ht;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 391176
    iput-object p1, p0, LX/2Ht;->b:LX/0Xl;

    .line 391177
    iput-object p2, p0, LX/2Ht;->c:LX/0lB;

    .line 391178
    iput-object p3, p0, LX/2Ht;->d:LX/2Hu;

    .line 391179
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ht;
    .locals 6

    .prologue
    .line 391259
    sget-object v0, LX/2Ht;->g:LX/2Ht;

    if-nez v0, :cond_1

    .line 391260
    const-class v1, LX/2Ht;

    monitor-enter v1

    .line 391261
    :try_start_0
    sget-object v0, LX/2Ht;->g:LX/2Ht;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391262
    if-eqz v2, :cond_0

    .line 391263
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391264
    new-instance p0, LX/2Ht;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v5

    check-cast v5, LX/2Hu;

    invoke-direct {p0, v3, v4, v5}, LX/2Ht;-><init>(LX/0Xl;LX/0lB;LX/2Hu;)V

    .line 391265
    move-object v0, p0

    .line 391266
    sput-object v0, LX/2Ht;->g:LX/2Ht;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391267
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391268
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391269
    :cond_1
    sget-object v0, LX/2Ht;->g:LX/2Ht;

    return-object v0

    .line 391270
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(ILX/0gW;)[B
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 391247
    new-instance v8, LX/1so;

    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    invoke-direct {v8, v0}, LX/1so;-><init>(LX/1sq;)V

    .line 391248
    iget-object v0, p1, LX/0gW;->h:Ljava/lang/String;

    move-object v0, v0

    .line 391249
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 391250
    new-instance v4, Lorg/json/JSONObject;

    .line 391251
    iget-object v0, p1, LX/0gW;->e:LX/0w7;

    move-object v0, v0

    .line 391252
    invoke-virtual {v0}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 391253
    new-instance v0, LX/6mO;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 391254
    iget-boolean v4, p1, LX/0gW;->k:Z

    move v4, v4

    .line 391255
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 391256
    iget-boolean v5, p1, LX/0gW;->l:Z

    move v5, v5

    .line 391257
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, LX/6mO;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;)V

    .line 391258
    invoke-virtual {v8, v0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized a$redex0(LX/2Ht;)V
    .locals 3

    .prologue
    .line 391242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Ht;->f:LX/0Yb;

    if-nez v0, :cond_0

    .line 391243
    iget-object v0, p0, LX/2Ht;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/6XD;

    invoke-direct {v2, p0}, LX/6XD;-><init>(LX/2Ht;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Ht;->f:LX/0Yb;

    .line 391244
    iget-object v0, p0, LX/2Ht;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391245
    :cond_0
    monitor-exit p0

    return-void

    .line 391246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0gW",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 391228
    invoke-static {p0}, LX/2Ht;->a$redex0(LX/2Ht;)V

    .line 391229
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 391230
    iget-object v0, p0, LX/2Ht;->d:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v2

    .line 391231
    iget-object v0, p0, LX/2Ht;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 391232
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, p1}, LX/2Ht;->a(ILX/0gW;)[B

    move-result-object v3

    .line 391233
    const-string v4, "/t_graphql_req"

    sget-object v5, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v3, v5, v6}, LX/2gV;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    move-result v3

    .line 391234
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 391235
    new-instance v0, Ljava/lang/Throwable;

    const-string v3, "MQTT publish failed immediately."

    invoke-direct {v0, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391236
    :goto_0
    invoke-virtual {v2}, LX/2gV;->f()V

    .line 391237
    :goto_1
    return-object v1

    .line 391238
    :cond_0
    :try_start_1
    iget-object v3, p0, LX/2Ht;->e:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, LX/6XE;

    invoke-direct {v4, p0, p1, v1}, LX/6XE;-><init>(LX/2Ht;LX/0gW;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v3, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 391239
    :catch_0
    move-exception v0

    .line 391240
    :try_start_2
    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391241
    invoke-virtual {v2}, LX/2gV;->f()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/2gV;->f()V

    throw v0
.end method

.method public final get()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391225
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 391226
    new-instance v1, LX/1se;

    const-string v2, "/t_graphql_resp"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->ALWAYS:LX/2C3;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 391227
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 5

    .prologue
    .line 391180
    const-string v0, "/t_graphql_resp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 391181
    const/4 v4, 0x0

    .line 391182
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 391183
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    array-length v3, p2

    invoke-direct {v2, p2, v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 391184
    const/4 v1, 0x0

    .line 391185
    :try_start_0
    const/4 v2, 0x0

    .line 391186
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    move-object v3, v2

    move-object p1, v2

    .line 391187
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object p3

    .line 391188
    iget-byte p4, p3, LX/1sw;->b:B

    if-eqz p4, :cond_3

    .line 391189
    iget-short p4, p3, LX/1sw;->c:S

    packed-switch p4, :pswitch_data_0

    .line 391190
    iget-byte p3, p3, LX/1sw;->b:B

    invoke-static {v0, p3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 391191
    :pswitch_0
    iget-byte p4, p3, LX/1sw;->b:B

    const/16 p2, 0x8

    if-ne p4, p2, :cond_0

    .line 391192
    invoke-virtual {v0}, LX/1su;->m()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    .line 391193
    :cond_0
    iget-byte p3, p3, LX/1sw;->b:B

    invoke-static {v0, p3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 391194
    :pswitch_1
    iget-byte p4, p3, LX/1sw;->b:B

    const/16 p2, 0xb

    if-ne p4, p2, :cond_1

    .line 391195
    invoke-virtual {v0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 391196
    :cond_1
    iget-byte p3, p3, LX/1sw;->b:B

    invoke-static {v0, p3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 391197
    :pswitch_2
    iget-byte p4, p3, LX/1sw;->b:B

    const/4 p2, 0x2

    if-ne p4, p2, :cond_2

    .line 391198
    invoke-virtual {v0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 391199
    :cond_2
    iget-byte p3, p3, LX/1sw;->b:B

    invoke-static {v0, p3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 391200
    :cond_3
    invoke-virtual {v0}, LX/1su;->e()V

    .line 391201
    new-instance p3, LX/6mP;

    invoke-direct {p3, p1, v3, v2}, LX/6mP;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 391202
    move-object v2, p3

    .line 391203
    iget-object v0, p0, LX/2Ht;->e:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, v2, LX/6mP;->requestId:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6XE;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 391204
    if-nez v0, :cond_5

    .line 391205
    :try_start_1
    const-string v1, "GraphQLMQTTSimpleExecutor"

    const-string v2, "Got a response for non-existing request."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 391206
    :cond_4
    :goto_1
    return-void

    .line 391207
    :cond_5
    :try_start_2
    iget-object v1, v2, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    iget-object v1, v2, LX/6mP;->isSuccess:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_6

    .line 391208
    iget-object v1, v0, LX/6XE;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "GraphQL-over-mqtt query failed"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 391209
    :catch_0
    move-exception v1

    move-object p1, v1

    move-object v1, v0

    move-object v0, p1

    .line 391210
    :goto_2
    const-string v2, "GraphQLMQTTSimpleExecutor"

    const-string v3, "Failed to handle response payload."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391211
    if-eqz v1, :cond_4

    .line 391212
    iget-object v1, v1, LX/6XE;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_1

    .line 391213
    :cond_6
    :try_start_3
    iget-object v1, v0, LX/6XE;->a:LX/0gW;

    .line 391214
    iget-boolean v3, v1, LX/0gW;->m:Z

    move v1, v3

    .line 391215
    if-eqz v1, :cond_7

    .line 391216
    iget-object v1, v0, LX/6XE;->a:LX/0gW;

    .line 391217
    iget-object v3, v1, LX/0gW;->a:LX/0w5;

    move-object v1, v3

    .line 391218
    iget-object v3, p0, LX/2Ht;->c:LX/0lB;

    iget-object v2, v2, LX/6mP;->queryResponse:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, LX/0w5;->a(LX/0lC;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    .line 391219
    iget-object v2, v0, LX/6XE;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    const v3, -0x3bee4c4

    invoke-static {v2, v1, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_1

    .line 391220
    :cond_7
    iget-object v1, v0, LX/6XE;->a:LX/0gW;

    .line 391221
    iget-object v3, v1, LX/0gW;->a:LX/0w5;

    move-object v1, v3

    .line 391222
    iget-object v3, p0, LX/2Ht;->c:LX/0lB;

    iget-object v2, v2, LX/6mP;->queryResponse:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, LX/0w5;->b(LX/0lC;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 391223
    iget-object v2, v0, LX/6XE;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v3, 0x65c28e4b

    invoke-static {v2, v1, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 391224
    :catch_1
    move-exception v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
