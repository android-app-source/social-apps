.class public LX/28p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/28p;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field public final c:Landroid/content/Context;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374810
    const-string v0, "uid_"

    iput-object v0, p0, LX/28p;->a:Ljava/lang/String;

    .line 374811
    const-string v0, "_temp"

    iput-object v0, p0, LX/28p;->b:Ljava/lang/String;

    .line 374812
    const/4 v0, 0x0

    iput-object v0, p0, LX/28p;->d:Ljava/lang/String;

    .line 374813
    iput-object p1, p0, LX/28p;->c:Landroid/content/Context;

    .line 374814
    return-void
.end method

.method public static a(LX/0QB;)LX/28p;
    .locals 4

    .prologue
    .line 374815
    sget-object v0, LX/28p;->e:LX/28p;

    if-nez v0, :cond_1

    .line 374816
    const-class v1, LX/28p;

    monitor-enter v1

    .line 374817
    :try_start_0
    sget-object v0, LX/28p;->e:LX/28p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374818
    if-eqz v2, :cond_0

    .line 374819
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374820
    new-instance p0, LX/28p;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/28p;-><init>(Landroid/content/Context;)V

    .line 374821
    move-object v0, p0

    .line 374822
    sput-object v0, LX/28p;->e:LX/28p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374823
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374824
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374825
    :cond_1
    sget-object v0, LX/28p;->e:LX/28p;

    return-object v0

    .line 374826
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374827
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 374828
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/28p;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 374829
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 374830
    :cond_1
    const/4 v1, 0x0

    .line 374831
    :try_start_1
    iget-object v0, p0, LX/28p;->c:Landroid/content/Context;

    invoke-static {v0}, LX/08u;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 374832
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "uid_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "_temp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 374833
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 374834
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 374835
    :cond_3
    const-string v0, "uid_"

    const-string v2, "_temp"

    iget-object v3, p0, LX/28p;->c:Landroid/content/Context;

    invoke-static {v3}, LX/08u;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-static {v0, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 374836
    iget-object v0, p0, LX/28p;->c:Landroid/content/Context;

    invoke-static {v0}, LX/08u;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    .line 374837
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374838
    :try_start_2
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 374839
    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 374840
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Renaming temporary file failed"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 374841
    :catch_0
    :goto_2
    const/4 v1, 0x1

    :try_start_3
    invoke-static {v0, v1}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 374842
    :catch_1
    goto :goto_0

    .line 374843
    :cond_4
    :try_start_4
    iput-object p1, p0, LX/28p;->d:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 374844
    const/4 v1, 0x1

    :try_start_5
    invoke-static {v0, v1}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 374845
    :catch_2
    goto :goto_0

    .line 374846
    :catchall_0
    move-exception v0

    .line 374847
    :goto_3
    const/4 v2, 0x1

    :try_start_6
    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 374848
    :goto_4
    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 374849
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_3
    goto :goto_4

    .line 374850
    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catch_4
    move-object v0, v1

    goto :goto_2
.end method
