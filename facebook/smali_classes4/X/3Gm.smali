.class public LX/3Gm;
.super LX/3Gn;
.source ""


# static fields
.field public static final w:LX/0Tn;


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;

.field public D:Landroid/os/Handler;

.field public E:LX/0hs;

.field public F:J

.field public o:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/3Go;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/3Gp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final x:Ljava/lang/Runnable;

.field private y:Lcom/facebook/user/tiles/UserTileView;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 541787
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "video_broadcast_is_live_scribe_tool_tip_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Gm;->w:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 541785
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Gm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 541786
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 541783
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Gm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541784
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 541777
    invoke-direct {p0, p1, p2, p3}, LX/3Gn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541778
    new-instance v0, Lcom/facebook/feed/video/inline/VideoLiveScribeButtonPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/VideoLiveScribeButtonPlugin$1;-><init>(LX/3Gm;)V

    iput-object v0, p0, LX/3Gm;->x:Ljava/lang/Runnable;

    .line 541779
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/3Gm;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v0}, LX/3Go;->b(LX/0QB;)LX/3Go;

    move-result-object p1

    check-cast p1, LX/3Go;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object p2

    check-cast p2, LX/0xX;

    invoke-static {v0}, LX/3Gp;->a(LX/0QB;)LX/3Gp;

    move-result-object p3

    check-cast p3, LX/3Gp;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    iput-object v3, v2, LX/3Gm;->o:LX/0wM;

    iput-object v4, v2, LX/3Gm;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v2, LX/3Gm;->q:LX/0SG;

    iput-object v6, v2, LX/3Gm;->r:Ljava/lang/String;

    iput-object p1, v2, LX/3Gm;->s:LX/3Go;

    iput-object p2, v2, LX/3Gm;->t:LX/0xX;

    iput-object p3, v2, LX/3Gm;->u:LX/3Gp;

    iput-object v0, v2, LX/3Gm;->v:LX/01T;

    .line 541780
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/3Gm;->D:Landroid/os/Handler;

    .line 541781
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gq;

    invoke-direct {v1, p0, p0}, LX/3Gq;-><init>(LX/3Gm;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541782
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 541749
    invoke-static {p1}, LX/393;->n(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 541750
    iget-object v3, p0, LX/3Gm;->v:LX/01T;

    sget-object v4, LX/01T;->FB4A:LX/01T;

    if-ne v3, v4, :cond_0

    if-eqz v2, :cond_0

    iget-object v3, p0, LX/3Gm;->r:Ljava/lang/String;

    invoke-static {v2}, LX/17E;->y(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 541751
    :cond_0
    iput-boolean v1, p0, LX/3Gm;->f:Z

    .line 541752
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 541753
    :cond_1
    :goto_0
    return-void

    .line 541754
    :cond_2
    iput-boolean v0, p0, LX/3Gm;->f:Z

    .line 541755
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 541756
    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 541757
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 541758
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->ALL:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-ne v4, v5, :cond_3

    .line 541759
    :goto_1
    if-eqz p2, :cond_1

    .line 541760
    iput-boolean v0, p0, LX/3Gm;->z:Z

    .line 541761
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Gm;->A:Ljava/lang/String;

    .line 541762
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Gm;->B:Ljava/lang/String;

    .line 541763
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Gm;->C:Ljava/lang/String;

    .line 541764
    iget-boolean v0, p0, LX/3Gm;->z:Z

    invoke-virtual {p0, v0}, LX/3Gn;->a(Z)V

    .line 541765
    iget-object v0, p0, LX/3Gm;->y:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-static {v3}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 541766
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 541767
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 541768
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    new-instance v1, LX/Bwu;

    invoke-direct {v1, p0}, LX/Bwu;-><init>(LX/3Gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541769
    iget-object v0, p0, LX/3Gm;->s:LX/3Go;

    iget-object v1, p0, LX/3Gm;->A:Ljava/lang/String;

    iget-object v3, p0, LX/3Gm;->B:Ljava/lang/String;

    const-string v4, "video_overlay"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v2

    .line 541770
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "live_scribe_impression"

    invoke-direct {v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "Facecast"

    .line 541771
    iput-object p0, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 541772
    move-object v5, v5

    .line 541773
    const-string p0, "live_video_id"

    invoke-virtual {v5, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "broadcaster_id"

    invoke-virtual {v5, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "surface"

    invoke-virtual {v5, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "is_live_streaming"

    invoke-virtual {v5, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 541774
    iget-object p0, v0, LX/3Go;->d:LX/0Zb;

    invoke-interface {p0, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 541775
    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 541776
    goto :goto_1
.end method

.method public final b(Z)I
    .locals 2

    .prologue
    .line 541742
    iget-object v0, p0, LX/3Gm;->t:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    .line 541743
    if-eqz v0, :cond_0

    const v1, 0x7f081a2b

    .line 541744
    :goto_0
    if-eqz v0, :cond_1

    const v0, 0x7f081a2c

    .line 541745
    :goto_1
    if-eqz p1, :cond_2

    :goto_2
    return v0

    .line 541746
    :cond_0
    const v1, 0x7f080c00

    goto :goto_0

    .line 541747
    :cond_1
    const v0, 0x7f080c00

    goto :goto_1

    :cond_2
    move v0, v1

    .line 541748
    goto :goto_2
.end method

.method public final c(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 541737
    iget-object v3, p0, LX/3Gm;->y:Lcom/facebook/user/tiles/UserTileView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 541738
    iget-object v0, p0, LX/3Gn;->d:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 541739
    return-void

    :cond_0
    move v0, v2

    .line 541740
    goto :goto_0

    :cond_1
    move v2, v1

    .line 541741
    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 541726
    invoke-super {p0}, LX/3Gn;->d()V

    .line 541727
    iget-object v0, p0, LX/3Gm;->D:Landroid/os/Handler;

    iget-object v1, p0, LX/3Gm;->x:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 541728
    iget-object v0, p0, LX/3Gm;->E:LX/0hs;

    if-eqz v0, :cond_0

    .line 541729
    iget-object v0, p0, LX/3Gm;->E:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 541730
    :cond_0
    return-void
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 4

    .prologue
    .line 541734
    iget-object v0, p0, LX/3Gn;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/3Gm;->o:LX/0wM;

    const v2, 0x7f0207d6

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 541735
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080bfc

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 541736
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 541731
    invoke-super {p0, p1}, LX/3Gn;->setupViews(Landroid/view/View;)V

    .line 541732
    const v0, 0x7f0d176d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/3Gm;->y:Lcom/facebook/user/tiles/UserTileView;

    .line 541733
    return-void
.end method
