.class public final LX/30f;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:LX/30e;


# direct methods
.method public constructor <init>(LX/30e;)V
    .locals 0

    .prologue
    .line 485088
    iput-object p1, p0, LX/30f;->a:LX/30e;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 485089
    check-cast p1, LX/2f2;

    .line 485090
    if-nez p1, :cond_1

    .line 485091
    :cond_0
    :goto_0
    return-void

    .line 485092
    :cond_1
    iget-object v0, p0, LX/30f;->a:LX/30e;

    iget-object v0, v0, LX/30e;->f:Ljava/util/Map;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjK;

    .line 485093
    if-eqz v0, :cond_0

    .line 485094
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    .line 485095
    :goto_1
    iput-boolean v1, v0, LX/EjK;->e:Z

    .line 485096
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
