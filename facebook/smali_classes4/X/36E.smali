.class public LX/36E;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498432
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 498433
    return-void
.end method


# virtual methods
.method public final a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;
    .locals 7

    .prologue
    .line 498434
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    invoke-static {p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v1

    check-cast v1, LX/14w;

    invoke-static {p0}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    const-class v4, LX/36G;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/36G;

    const-class v5, LX/36H;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/36H;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;-><init>(LX/14w;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;LX/36G;LX/36H;LX/1Wi;)V

    .line 498435
    return-object v0
.end method
