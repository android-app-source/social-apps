.class public LX/2gr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 449094
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 449095
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 449096
    :goto_0
    return v1

    .line 449097
    :cond_0
    const-string v6, "reaction_count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 449098
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 449099
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 449100
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 449101
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 449102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 449103
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 449104
    invoke-static {p0, p1}, LX/2gs;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 449105
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 449106
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 449107
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 449108
    if-eqz v0, :cond_4

    .line 449109
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 449110
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 449111
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 449112
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 449113
    if-eqz v0, :cond_0

    .line 449114
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449115
    invoke-static {p0, v0, p2, p3}, LX/2gs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449116
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 449117
    if-eqz v0, :cond_1

    .line 449118
    const-string v1, "reaction_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449119
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 449120
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 449121
    return-void
.end method
