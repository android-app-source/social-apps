.class public final LX/2rI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/093;

.field public final c:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final d:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final e:LX/04D;

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/093;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/04D;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 471806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471807
    iput-object p1, p0, LX/2rI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 471808
    iput-object p2, p0, LX/2rI;->b:LX/093;

    .line 471809
    iput-object p3, p0, LX/2rI;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 471810
    iput-object p4, p0, LX/2rI;->d:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 471811
    iput-object p5, p0, LX/2rI;->e:LX/04D;

    .line 471812
    iput-object p6, p0, LX/2rI;->f:Ljava/lang/String;

    .line 471813
    return-void
.end method
