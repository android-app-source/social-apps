.class public LX/3Ar;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 526403
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_9

    .line 526404
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 526405
    :goto_0
    return v0

    .line 526406
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 526407
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 526408
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 526409
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 526410
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 526411
    const-string v8, "icon_background_color"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 526412
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 526413
    :cond_2
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 526414
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 526415
    :cond_3
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 526416
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 526417
    :cond_4
    const-string v8, "imageLarge"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 526418
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 526419
    :cond_5
    const-string v8, "profileImageLarge"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 526420
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 526421
    :cond_6
    const-string v8, "profileImageSmall"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 526422
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 526423
    :cond_7
    const-string v8, "url"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 526424
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 526425
    :cond_8
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 526426
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 526427
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 526428
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 526429
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 526430
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 526431
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 526432
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 526433
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 526397
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 526398
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 526399
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/3Ar;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 526400
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 526401
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 526402
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 526391
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 526392
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 526393
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 526394
    invoke-static {p0, p1}, LX/3Ar;->a(LX/15w;LX/186;)I

    move-result v1

    .line 526395
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 526396
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 526360
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 526361
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526362
    if-eqz v0, :cond_0

    .line 526363
    const-string v1, "icon_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526364
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526365
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526366
    if-eqz v0, :cond_1

    .line 526367
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526368
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526369
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526370
    if-eqz v0, :cond_2

    .line 526371
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526372
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526373
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526374
    if-eqz v0, :cond_3

    .line 526375
    const-string v1, "imageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526376
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526377
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526378
    if-eqz v0, :cond_4

    .line 526379
    const-string v1, "profileImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526380
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526381
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526382
    if-eqz v0, :cond_5

    .line 526383
    const-string v1, "profileImageSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526384
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526385
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526386
    if-eqz v0, :cond_6

    .line 526387
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526388
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526389
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 526390
    return-void
.end method
