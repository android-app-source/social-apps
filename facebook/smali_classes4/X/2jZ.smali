.class public LX/2jZ;
.super LX/2ja;
.source ""

# interfaces
.implements LX/2jc;


# instance fields
.field private final a:LX/0gh;

.field public final b:LX/2Yg;

.field public final c:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

.field private final d:LX/0TD;


# direct methods
.method public constructor <init>(LX/2jY;LX/1P1;LX/0TD;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/03V;LX/0gh;LX/2Yg;LX/2j3;LX/1vC;)V
    .locals 6
    .param p1    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1P1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 453922
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move-object v4, p8

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, LX/2ja;-><init>(LX/2jY;LX/1P1;LX/03V;LX/2j3;LX/1vC;)V

    .line 453923
    iput-object p4, p0, LX/2jZ;->c:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 453924
    iput-object p3, p0, LX/2jZ;->d:LX/0TD;

    .line 453925
    iput-object p6, p0, LX/2jZ;->a:LX/0gh;

    .line 453926
    iput-object p7, p0, LX/2jZ;->b:LX/2Yg;

    .line 453927
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 453911
    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    .line 453912
    iput-object v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 453913
    move-object v1, v1

    .line 453914
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 453915
    move-object v1, v1

    .line 453916
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 453917
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 453918
    move-object v0, v1

    .line 453919
    iput p1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 453920
    move-object v0, v0

    .line 453921
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453905
    iget-object v0, p0, LX/2jZ;->d:LX/0TD;

    new-instance v1, LX/H4o;

    invoke-direct {v1, p0, p1}, LX/H4o;-><init>(LX/2jZ;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 453906
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453907
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2jZ;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/H4n;

    invoke-direct {v1, p0, p1, p2}, LX/H4n;-><init>(LX/2jZ;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 453908
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 453909
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 453910
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453900
    iget-object v0, p0, LX/2jZ;->a:LX/0gh;

    const-string v1, "tap_notification_jewel"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 453901
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2jZ;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/H4m;

    invoke-direct {v1, p0, p1, p2, p3}, LX/H4m;-><init>(LX/2jZ;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    .line 453902
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 453903
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 453904
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V
    .locals 3

    .prologue
    .line 453890
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 453891
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 453892
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 453893
    invoke-interface {v2}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    invoke-super {p0, v0, v1, v2, p2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 453894
    iget-object v0, p2, LX/Cfc;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/2jZ;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 453895
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453896
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 453897
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_0

    .line 453898
    :goto_0
    return-void

    .line 453899
    :cond_0
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, LX/2jZ;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
