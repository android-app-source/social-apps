.class public final LX/2md;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 461025
    new-instance v0, LX/0U1;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->a:LX/0U1;

    .line 461026
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_fbid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->b:LX/0U1;

    .line 461027
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->c:LX/0U1;

    .line 461028
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->d:LX/0U1;

    .line 461029
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_pic"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->e:LX/0U1;

    .line 461030
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->f:LX/0U1;

    .line 461031
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_unread_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->g:LX/0U1;

    .line 461032
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_count_string"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->h:LX/0U1;

    .line 461033
    new-instance v0, LX/0U1;

    const-string v1, "bookmark_client_token"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->i:LX/0U1;

    .line 461034
    new-instance v0, LX/0U1;

    const-string v1, "group_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->j:LX/0U1;

    .line 461035
    new-instance v0, LX/0U1;

    const-string v1, "layout"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->k:LX/0U1;

    .line 461036
    new-instance v0, LX/0U1;

    const-string v1, "sub_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->l:LX/0U1;

    .line 461037
    new-instance v0, LX/0U1;

    const-string v1, "mini_app_icon"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->m:LX/0U1;

    .line 461038
    new-instance v0, LX/0U1;

    const-string v1, "group_index"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->n:LX/0U1;

    .line 461039
    new-instance v0, LX/0U1;

    const-string v1, "visible_in_group"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2md;->o:LX/0U1;

    return-void
.end method

.method public static a()LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 461040
    sget-object v0, LX/2md;->a:LX/0U1;

    sget-object v1, LX/2md;->b:LX/0U1;

    sget-object v2, LX/2md;->c:LX/0U1;

    sget-object v3, LX/2md;->d:LX/0U1;

    sget-object v4, LX/2md;->e:LX/0U1;

    sget-object v5, LX/2md;->f:LX/0U1;

    sget-object v6, LX/2md;->g:LX/0U1;

    sget-object v7, LX/2md;->h:LX/0U1;

    sget-object v8, LX/2md;->i:LX/0U1;

    sget-object v9, LX/2md;->k:LX/0U1;

    sget-object v10, LX/2md;->l:LX/0U1;

    sget-object v11, LX/2md;->m:LX/0U1;

    const/4 v12, 0x3

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/2md;->j:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/2md;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/2md;->o:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
