.class public LX/23l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field public final b:LX/0SG;

.field public c:J

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365122
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/23l;->e:J

    .line 365123
    iput-object p1, p0, LX/23l;->a:Landroid/content/ContentResolver;

    .line 365124
    iput-object p2, p0, LX/23l;->b:LX/0SG;

    .line 365125
    return-void
.end method

.method public static a(LX/23l;Ljava/lang/String;I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 365116
    iget-object v0, p0, LX/23l;->a:Landroid/content/ContentResolver;

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 365117
    if-ne v0, v1, :cond_0

    .line 365118
    iget-object v0, p0, LX/23l;->a:Landroid/content/ContentResolver;

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 365119
    :cond_0
    if-ne v0, v1, :cond_1

    .line 365120
    :goto_0
    return p2

    :cond_1
    move p2, v0

    goto :goto_0
.end method
