.class public LX/3Le;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static volatile m:LX/3Le;


# instance fields
.field public final c:LX/3Lf;

.field public final d:LX/0Sh;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:LX/0Wd;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0ad;

.field public final j:LX/0Xl;

.field public k:I

.field private l:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551700
    const-class v0, LX/3Le;

    sput-object v0, LX/3Le;->a:Ljava/lang/Class;

    .line 551701
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    sput-object v0, LX/3Le;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(LX/3Lf;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/0Xl;Ljava/util/concurrent/Executor;LX/0Uh;LX/0Wd;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 551702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551703
    const/4 v0, -0x1

    iput v0, p0, LX/3Le;->k:I

    .line 551704
    iput-object p1, p0, LX/3Le;->c:LX/3Lf;

    .line 551705
    iput-object p2, p0, LX/3Le;->d:LX/0Sh;

    .line 551706
    iput-object p3, p0, LX/3Le;->e:Ljava/util/concurrent/ExecutorService;

    .line 551707
    iput-object p6, p0, LX/3Le;->f:Ljava/util/concurrent/Executor;

    .line 551708
    iput-object p4, p0, LX/3Le;->i:LX/0ad;

    .line 551709
    iput-object p5, p0, LX/3Le;->j:LX/0Xl;

    .line 551710
    iput-object p7, p0, LX/3Le;->l:LX/0Uh;

    .line 551711
    iput-object p8, p0, LX/3Le;->g:LX/0Wd;

    .line 551712
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551713
    if-nez p0, :cond_0

    .line 551714
    const/4 v0, 0x0

    .line 551715
    :goto_0
    return-object v0

    .line 551716
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 551717
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/models/RtcCallLogInfo;

    .line 551718
    iget-object v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 551719
    iget-object v0, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551720
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 551721
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/0Pz;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551722
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 551723
    invoke-static {p0, v0}, LX/3Le;->b(LX/3Le;LX/0Px;)V

    .line 551724
    return-object v0
.end method

.method public static a(LX/3Le;I[IZ)LX/0Px;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[IZ)",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551725
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3Le;->d:LX/0Sh;

    const-string v5, "Recent Calls DB accessed from UI Thread"

    invoke-virtual {v4, v5}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 551726
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v14

    .line 551727
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 551728
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v5

    if-nez v5, :cond_1

    .line 551729
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/3Le;->a(LX/0Pz;)LX/0Px;

    move-result-object v4

    .line 551730
    :goto_0
    return-object v4

    .line 551731
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 551732
    const-string v5, "call_type"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551733
    const-string v5, " IN ("

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551734
    move-object/from16 v0, p2

    array-length v5, v0

    invoke-static {v5}, LX/3Le;->d(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551735
    const-string v5, ")"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551736
    const-string v5, " AND "

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551737
    const-string v5, "on_going"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551738
    const-string v5, " = ?"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551739
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 551740
    move-object/from16 v0, p2

    array-length v6, v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_2

    aget v7, p2, v5

    .line 551741
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551742
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 551743
    :cond_2
    if-eqz p3, :cond_3

    const-string v5, "1"

    :goto_2
    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551744
    const-string v6, "person_summary"

    const/4 v7, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v9, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    if-eqz p3, :cond_4

    const-string v10, "thread_id"

    :goto_3
    const/4 v11, 0x0

    const-string v12, "last_call_time desc"

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    move/from16 v5, p3

    invoke-virtual/range {v4 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 551745
    if-nez v5, :cond_5

    .line 551746
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/3Le;->a(LX/0Pz;)LX/0Px;

    move-result-object v4

    goto :goto_0

    .line 551747
    :cond_3
    const-string v5, "0"

    goto :goto_2

    .line 551748
    :cond_4
    const/4 v10, 0x0

    goto :goto_3

    .line 551749
    :cond_5
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_6

    .line 551750
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/3Le;->a(LX/0Pz;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 551751
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 551752
    :cond_6
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    .line 551753
    const-string v4, "log_id"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 551754
    const-string v4, "user_id"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 551755
    const-string v4, "answered"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 551756
    const-string v4, "acknowledged"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 551757
    const-string v4, "call_type"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 551758
    const-string v4, "direction"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 551759
    const-string v4, "duration"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 551760
    const-string v4, "last_call_time"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 551761
    const-string v4, "seen"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 551762
    const-string v4, "thread_id"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 551763
    const-string v4, "on_going"

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 551764
    :goto_4
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_c

    .line 551765
    new-instance v18, Lcom/facebook/rtc/models/RtcCallLogInfo;

    invoke-direct/range {v18 .. v18}, Lcom/facebook/rtc/models/RtcCallLogInfo;-><init>()V

    .line 551766
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Lcom/facebook/rtc/models/RtcCallLogInfo;->a:J

    .line 551767
    invoke-interface {v5, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_8

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, v18

    iput-boolean v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->e:Z

    .line 551768
    invoke-interface {v5, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_9

    const/4 v4, 0x1

    :goto_6
    move-object/from16 v0, v18

    iput-boolean v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->h:Z

    .line 551769
    invoke-interface {v5, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v18

    iput v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->g:I

    .line 551770
    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v18

    iput v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->f:I

    .line 551771
    invoke-interface {v5, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Lcom/facebook/rtc/models/RtcCallLogInfo;->d:J

    .line 551772
    invoke-interface {v5, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Lcom/facebook/rtc/models/RtcCallLogInfo;->c:J

    .line 551773
    invoke-interface {v5, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_a

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, v18

    iput-boolean v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->i:Z

    .line 551774
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/rtc/models/RtcCallLogInfo;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 551775
    move/from16 v0, v16

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->j:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 551776
    :cond_7
    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v4

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 551777
    move/from16 v0, v17

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_b

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, v18

    iput-boolean v4, v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->k:Z

    .line 551778
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551779
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 551780
    :catchall_0
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    .line 551781
    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    .line 551782
    :cond_9
    const/4 v4, 0x0

    goto :goto_6

    .line 551783
    :cond_a
    const/4 v4, 0x0

    goto :goto_7

    .line 551784
    :cond_b
    const/4 v4, 0x0

    goto :goto_8

    .line 551785
    :cond_c
    :try_start_2
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/3Le;->a(LX/0Pz;)LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 551786
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/3Le;
    .locals 12

    .prologue
    .line 551787
    sget-object v0, LX/3Le;->m:LX/3Le;

    if-nez v0, :cond_1

    .line 551788
    const-class v1, LX/3Le;

    monitor-enter v1

    .line 551789
    :try_start_0
    sget-object v0, LX/3Le;->m:LX/3Le;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551790
    if-eqz v2, :cond_0

    .line 551791
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 551792
    new-instance v3, LX/3Le;

    invoke-static {v0}, LX/3Lf;->a(LX/0QB;)LX/3Lf;

    move-result-object v4

    check-cast v4, LX/3Lf;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v11

    check-cast v11, LX/0Wd;

    invoke-direct/range {v3 .. v11}, LX/3Le;-><init>(LX/3Lf;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0ad;LX/0Xl;Ljava/util/concurrent/Executor;LX/0Uh;LX/0Wd;)V

    .line 551793
    move-object v0, v3

    .line 551794
    sput-object v0, LX/3Le;->m:LX/3Le;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551795
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 551796
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551797
    :cond_1
    sget-object v0, LX/3Le;->m:LX/3Le;

    return-object v0

    .line 551798
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 551799
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3Le;LX/0Px;)V
    .locals 3
    .param p0    # LX/3Le;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 551697
    iget-object v0, p0, LX/3Le;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/models/RecentCallsDb$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/models/RecentCallsDb$7;-><init>(LX/3Le;LX/0Px;)V

    const v2, -0x4af04153

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 551698
    return-void
.end method

.method public static c(LX/3Le;I)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551699
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/3Le;->a(LX/3Le;I[IZ)LX/0Px;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public static c$redex0(LX/3Le;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    .line 551691
    iget-object v0, p0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 551692
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    .line 551693
    :cond_0
    :goto_0
    return-void

    .line 551694
    :cond_1
    const-string v1, "person_summary"

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    .line 551695
    cmp-long v1, v2, v6

    if-lez v1, :cond_0

    .line 551696
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "delete from person_summary where ROWID IN (SELECT ROWID FROM person_summary ORDER BY last_call_time ASC LIMIT "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, -0x64824623

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4f350024

    invoke-static {v0}, LX/03h;->a(I)V

    goto :goto_0
.end method

.method private static d(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 551683
    if-gtz p0, :cond_0

    .line 551684
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No placeholders"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551685
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 551686
    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551687
    const/4 v0, 0x1

    :goto_0
    if-ge v0, p0, :cond_1

    .line 551688
    const-string v2, ",?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551689
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551690
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/3Le;)V
    .locals 2

    .prologue
    .line 551679
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 551680
    const-string v1, "com.facebook.rtc.fbwebrtc.CALL_LOG_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 551681
    iget-object v1, p0, LX/3Le;->j:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 551682
    return-void
.end method

.method public static f(LX/3Le;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 551649
    iget-object v0, p0, LX/3Le;->d:LX/0Sh;

    const-string v2, "Recent Calls DB accessed from UI Thread"

    invoke-virtual {v0, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 551650
    iget-object v0, p0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 551651
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 551652
    :cond_0
    :goto_0
    return v1

    .line 551653
    :cond_1
    iget-object v0, p0, LX/3Le;->l:LX/0Uh;

    const/16 v3, 0x5ef

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 551654
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 551655
    const-string v4, "select count("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551656
    const-string v4, "seen"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551657
    const-string v4, ") from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551658
    const-string v4, "person_summary"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551659
    const-string v4, " where "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551660
    const-string v4, "seen"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551661
    const-string v4, " = 0 and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551662
    const-string v4, "answered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551663
    const-string v4, " = 0 and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551664
    const-string v4, "direction"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551665
    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551666
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551667
    if-eqz v0, :cond_2

    .line 551668
    const-string v0, " and "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551669
    const-string v0, "call_type"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551670
    const-string v0, " != "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551671
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551672
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 551673
    if-eqz v2, :cond_0

    .line 551674
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 551675
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 551676
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 551677
    goto :goto_1

    .line 551678
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(I)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 551643
    iget-object v0, p0, LX/3Le;->l:LX/0Uh;

    const/16 v2, 0x5ef

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 551644
    :goto_0
    if-eqz v0, :cond_1

    .line 551645
    invoke-static {p0, p1}, LX/3Le;->c(LX/3Le;I)LX/0Px;

    move-result-object v0

    .line 551646
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 551647
    goto :goto_0

    .line 551648
    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, p1, v0, v1}, LX/3Le;->a(LX/3Le;I[IZ)LX/0Px;

    move-result-object v0

    goto :goto_1

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public final a(Ljava/lang/String;JZZZ)V
    .locals 16

    .prologue
    .line 551639
    invoke-static/range {p1 .. p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551640
    sget-object v0, LX/3Le;->a:Ljava/lang/Class;

    const-string v1, "Invalid thread id while trying to insert call log into db!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 551641
    :goto_0
    return-void

    .line 551642
    :cond_0
    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move/from16 v9, p5

    move/from16 v10, p4

    move/from16 v14, p6

    invoke-virtual/range {v1 .. v14}, LX/3Le;->a(Ljava/lang/String;Ljava/lang/String;JJZZZZZZZ)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JJZZZZZZZ)V
    .locals 19
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 551637
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3Le;->e:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v17, v0

    new-instance v2, Lcom/facebook/rtc/models/RecentCallsDb$3;

    move-object/from16 v3, p0

    move/from16 v4, p10

    move/from16 v5, p9

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move/from16 v12, p7

    move/from16 v13, p8

    move/from16 v14, p11

    move/from16 v15, p12

    move/from16 v16, p13

    invoke-direct/range {v2 .. v16}, Lcom/facebook/rtc/models/RecentCallsDb$3;-><init>(LX/3Le;ZZLjava/lang/String;Ljava/lang/String;JJZZZZZ)V

    const v3, -0x7183568d

    move-object/from16 v0, v17

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 551638
    return-void
.end method
