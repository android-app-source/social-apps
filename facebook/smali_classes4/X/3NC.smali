.class public LX/3NC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3MV;

.field public final b:LX/3Mw;

.field private final c:LX/0tX;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3MV;LX/3Mw;LX/0tX;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3MV;",
            "LX/3Mw;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558540
    iput-object p1, p0, LX/3NC;->a:LX/3MV;

    .line 558541
    iput-object p2, p0, LX/3NC;->b:LX/3Mw;

    .line 558542
    iput-object p3, p0, LX/3NC;->c:LX/0tX;

    .line 558543
    iput-object p4, p0, LX/3NC;->d:LX/0Or;

    .line 558544
    return-void
.end method

.method public static b(LX/0QB;)LX/3NC;
    .locals 5

    .prologue
    .line 558545
    new-instance v3, LX/3NC;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v0

    check-cast v0, LX/3MV;

    invoke-static {p0}, LX/3Mw;->b(LX/0QB;)LX/3Mw;

    move-result-object v1

    check-cast v1, LX/3Mw;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/3NC;-><init>(LX/3MV;LX/3Mw;LX/0tX;LX/0Or;)V

    .line 558546
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 558547
    iget-object v0, p0, LX/3NC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558548
    if-nez v0, :cond_0

    .line 558549
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    .line 558550
    :goto_0
    return-object v0

    .line 558551
    :cond_0
    new-instance v1, LX/5Z8;

    invoke-direct {v1}, LX/5Z8;-><init>()V

    move-object v1, v1

    .line 558552
    const-string v3, "max_count"

    .line 558553
    iget v4, p1, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->a:I

    move v4, v4

    .line 558554
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "search_query"

    .line 558555
    iget-object v5, p1, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 558556
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "include_message_info"

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "include_full_user_info"

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 558557
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v4, 0x78

    invoke-virtual {v1, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    .line 558558
    iget-object v3, p0, LX/3NC;->c:LX/0tX;

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    const v3, -0x5806b3fe

    invoke-static {v1, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 558559
    iget-object v3, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v3

    .line 558560
    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel;

    .line 558561
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsThreadNameModel;

    move-result-object v4

    .line 558562
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsParticipantsModel;

    move-result-object v5

    .line 558563
    if-nez v4, :cond_7

    move-object v3, v2

    :goto_1
    if-nez v5, :cond_8

    move-object v1, v2

    :goto_2
    const/4 p1, 0x0

    const/4 v7, 0x0

    .line 558564
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 558565
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 558566
    if-eqz v3, :cond_2

    .line 558567
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v10

    move v6, v7

    :goto_3
    if-ge v6, v10, :cond_2

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 558568
    iget-object v11, p0, LX/3NC;->b:LX/3Mw;

    invoke-virtual {v11, v2, v0}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v11

    .line 558569
    if-eqz v11, :cond_1

    invoke-virtual {v9, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 558570
    iget-object v12, p0, LX/3NC;->b:LX/3Mw;

    invoke-virtual {v12, v11, v2, p1, v0}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 558571
    invoke-virtual {v9, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 558572
    :cond_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    .line 558573
    :cond_2
    if-eqz v1, :cond_4

    .line 558574
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v6, v7

    :goto_4
    if-ge v6, v10, :cond_4

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 558575
    iget-object v11, p0, LX/3NC;->b:LX/3Mw;

    invoke-virtual {v11, v2, v0}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v11

    .line 558576
    if-eqz v11, :cond_3

    invoke-virtual {v9, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 558577
    iget-object v12, p0, LX/3NC;->b:LX/3Mw;

    invoke-virtual {v12, v11, v2, p1, v0}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 558578
    invoke-virtual {v9, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 558579
    :cond_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_4

    .line 558580
    :cond_4
    new-instance v2, LX/FOA;

    invoke-direct {v2, p0}, LX/FOA;-><init>(LX/3NC;)V

    invoke-static {v8, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 558581
    new-instance v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    invoke-direct {v2, v6, v7}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    move-object v0, v2

    .line 558582
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 558583
    if-eqz v4, :cond_5

    .line 558584
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsThreadNameModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 558585
    :cond_5
    if-eqz v5, :cond_6

    .line 558586
    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsParticipantsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 558587
    :cond_6
    iget-object v2, p0, LX/3NC;->a:LX/3MV;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/3MV;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    .line 558588
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->newBuilder()LX/6iu;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 558589
    iput-object v3, v2, LX/6iu;->a:LX/0ta;

    .line 558590
    move-object v2, v2

    .line 558591
    iput-object v0, v2, LX/6iu;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 558592
    move-object v0, v2

    .line 558593
    iput-object v1, v0, LX/6iu;->c:Ljava/util/List;

    .line 558594
    move-object v0, v0

    .line 558595
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 558596
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 558597
    iput-wide v2, v0, LX/6iu;->d:J

    .line 558598
    move-object v0, v0

    .line 558599
    invoke-virtual {v0}, LX/6iu;->f()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    goto/16 :goto_0

    .line 558600
    :cond_7
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsThreadNameModel;->a()LX/0Px;

    move-result-object v1

    move-object v3, v1

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchThreadNameAndParticipantsQueryModel$SearchResultsParticipantsModel;->a()LX/0Px;

    move-result-object v1

    goto/16 :goto_2
.end method
