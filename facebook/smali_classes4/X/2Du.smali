.class public LX/2Du;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/27p;

.field public final d:LX/1Ml;

.field private final e:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384773
    const-class v0, LX/2Du;

    sput-object v0, LX/2Du;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/27p;LX/1Ml;LX/0TD;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384775
    iput-object p1, p0, LX/2Du;->b:Landroid/content/Context;

    .line 384776
    iput-object p2, p0, LX/2Du;->c:LX/27p;

    .line 384777
    iput-object p3, p0, LX/2Du;->d:LX/1Ml;

    .line 384778
    iput-object p4, p0, LX/2Du;->e:LX/0TD;

    .line 384779
    return-void
.end method

.method public static b(LX/0QB;)LX/2Du;
    .locals 5

    .prologue
    .line 384780
    new-instance v4, LX/2Du;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 384781
    new-instance v2, LX/27p;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {v2, v1}, LX/27p;-><init>(LX/0Zb;)V

    .line 384782
    move-object v1, v2

    .line 384783
    check-cast v1, LX/27p;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v2

    check-cast v2, LX/1Ml;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2Du;-><init>(Landroid/content/Context;LX/27p;LX/1Ml;LX/0TD;)V

    .line 384784
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;LX/4gy;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 384785
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "audience:server:client_id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, LX/4gy;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384786
    const/4 v1, 0x0

    .line 384787
    :try_start_0
    iget-object v2, p0, LX/2Du;->b:Landroid/content/Context;

    invoke-static {v2, p1, v0}, LX/G7J;->b(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 384788
    :try_start_1
    iget-object v2, p0, LX/2Du;->b:Landroid/content/Context;

    const-string v3, "Calling this from your main thread can lead to deadlock"

    invoke-static {v3}, LX/1ol;->c(Ljava/lang/String;)V

    invoke-static {v2}, LX/G7J;->a(Landroid/content/Context;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v5, "clientPackageName"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, LX/G7J;->d:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, LX/G7J;->d:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v4, LX/G8T;

    invoke-direct {v4, v1, v3}, LX/G8T;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    sget-object v3, LX/G7J;->a:Landroid/content/ComponentName;

    invoke-static {v2, v3, v4}, LX/G7J;->a(Landroid/content/Context;Landroid/content/ComponentName;LX/G8R;)Ljava/lang/Object;

    .line 384789
    iget-object v2, p0, LX/2Du;->b:Landroid/content/Context;

    invoke-static {v2, p1, v0}, LX/G7J;->b(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 384790
    :goto_0
    return-object v0

    .line 384791
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 384792
    :goto_1
    iget-object v2, p0, LX/2Du;->c:LX/27p;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Get ID token method exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/27p;->a(Ljava/lang/String;)V

    .line 384793
    iget-object v2, p0, LX/2Du;->c:LX/27p;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Get ID token method exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/27p;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 384794
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_1
.end method

.method public final a()Ljava/util/List;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384795
    iget-object v0, p0, LX/2Du;->d:LX/1Ml;

    const-string v1, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 384796
    iget-object v0, p0, LX/2Du;->c:LX/27p;

    const-string v1, "GET_ACCOUNTS_PERMISSION_NOT_AVAILABLE"

    invoke-virtual {v0, v1}, LX/27p;->a(Ljava/lang/String;)V

    .line 384797
    iget-object v0, p0, LX/2Du;->c:LX/27p;

    const-string v1, "GET_ACCOUNTS_PERMISSION_NOT_AVAILABLE"

    invoke-virtual {v0, v1}, LX/27p;->b(Ljava/lang/String;)V

    .line 384798
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 384799
    :goto_0
    return-object v0

    .line 384800
    :cond_0
    iget-object v0, p0, LX/2Du;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 384801
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 384802
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/4gy;
    .locals 5

    .prologue
    .line 384803
    invoke-static {}, LX/4gy;->values()[LX/4gy;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 384804
    iget-object v4, v0, LX/4gy;->type:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 384805
    :goto_1
    return-object v0

    .line 384806
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 384807
    :cond_1
    iget-object v0, p0, LX/2Du;->c:LX/27p;

    const-string v1, "NO_OPENID_CONNECT_PROVIDER"

    invoke-virtual {v0, v1}, LX/27p;->a(Ljava/lang/String;)V

    .line 384808
    iget-object v0, p0, LX/2Du;->c:LX/27p;

    const-string v1, "NO_OPENID_CONNECT_PROVIDER"

    invoke-virtual {v0, v1}, LX/27p;->b(Ljava/lang/String;)V

    .line 384809
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/accounts/Account;LX/4gy;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "LX/4gy;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384810
    iget-object v0, p0, LX/2Du;->e:LX/0TD;

    new-instance v1, LX/FQS;

    invoke-direct {v1, p0, p1, p2}, LX/FQS;-><init>(LX/2Du;Landroid/accounts/Account;LX/4gy;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
