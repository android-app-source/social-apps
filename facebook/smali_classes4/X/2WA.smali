.class public LX/2WA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Ljava/lang/String;

.field private final c:LX/0Wt;

.field public final d:LX/0SG;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;LX/0Wt;LX/0SG;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/annotations/DeviceStatusReporterInterval;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/lang/String;",
            "LX/0Wt;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 418592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418593
    iput-object p1, p0, LX/2WA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 418594
    iput-object p3, p0, LX/2WA;->c:LX/0Wt;

    .line 418595
    iput-object p2, p0, LX/2WA;->b:Ljava/lang/String;

    .line 418596
    iput-object p4, p0, LX/2WA;->d:LX/0SG;

    .line 418597
    iput-object p5, p0, LX/2WA;->e:LX/0Or;

    .line 418598
    return-void
.end method

.method public static b(LX/2WA;Ljava/lang/String;)LX/0Tn;
    .locals 3

    .prologue
    .line 418586
    iget-object v1, p0, LX/2WA;->b:Ljava/lang/String;

    .line 418587
    invoke-static {v1, p1}, LX/0Wt;->c(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    sget-object p0, LX/0Wt;->c:Ljava/lang/String;

    invoke-virtual {v2, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    move-object v0, v2

    .line 418588
    return-object v0
.end method

.method public static c(LX/2WA;Ljava/lang/String;)LX/0Tn;
    .locals 3

    .prologue
    .line 418589
    iget-object v1, p0, LX/2WA;->b:Ljava/lang/String;

    .line 418590
    invoke-static {v1, p1}, LX/0Wt;->c(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    sget-object p0, LX/0Wt;->b:Ljava/lang/String;

    invoke-virtual {v2, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    move-object v0, v2

    .line 418591
    return-object v0
.end method
