.class public final LX/2of;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 0

    .prologue
    .line 467231
    iput-object p1, p0, LX/2of;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;B)V
    .locals 0

    .prologue
    .line 467232
    invoke-direct {p0, p1}, LX/2of;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    return-void
.end method

.method private a(LX/2ou;)V
    .locals 2

    .prologue
    .line 467226
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 467227
    iget-object v0, p0, LX/2of;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->w:LX/2on;

    invoke-virtual {v0}, LX/2on;->a()V

    .line 467228
    :cond_0
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 467229
    iget-object v0, p0, LX/2of;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v0}, LX/7QU;->b(Landroid/view/View;)V

    .line 467230
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467225
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 467224
    check-cast p1, LX/2ou;

    invoke-direct {p0, p1}, LX/2of;->a(LX/2ou;)V

    return-void
.end method
