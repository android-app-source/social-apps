.class public LX/3DA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:LX/0hy;

.field private final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final c:LX/3Cm;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/03V;

.field private final f:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final g:LX/17W;

.field public final h:LX/0ad;

.field private final i:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final j:LX/17Y;

.field private final k:LX/1rU;

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/BDy;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0hy;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/3Cm;Lcom/facebook/content/SecureContextHelper;LX/03V;Lcom/facebook/performancelogger/PerformanceLogger;LX/17W;LX/0ad;Lcom/facebook/auth/viewercontext/ViewerContext;LX/17Y;LX/1rU;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 531836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531837
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3DA;->l:Ljava/util/List;

    .line 531838
    iput-object p1, p0, LX/3DA;->a:LX/0hy;

    .line 531839
    iput-object p2, p0, LX/3DA;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 531840
    iput-object p3, p0, LX/3DA;->c:LX/3Cm;

    .line 531841
    iput-object p4, p0, LX/3DA;->d:Lcom/facebook/content/SecureContextHelper;

    .line 531842
    iput-object p5, p0, LX/3DA;->e:LX/03V;

    .line 531843
    iput-object p6, p0, LX/3DA;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 531844
    iput-object p7, p0, LX/3DA;->g:LX/17W;

    .line 531845
    iput-object p8, p0, LX/3DA;->h:LX/0ad;

    .line 531846
    iput-object p9, p0, LX/3DA;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 531847
    iput-object p10, p0, LX/3DA;->j:LX/17Y;

    .line 531848
    iput-object p11, p0, LX/3DA;->k:LX/1rU;

    .line 531849
    return-void
.end method

.method public static a(LX/0QB;)LX/3DA;
    .locals 15

    .prologue
    .line 531825
    const-class v1, LX/3DA;

    monitor-enter v1

    .line 531826
    :try_start_0
    sget-object v0, LX/3DA;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 531827
    sput-object v2, LX/3DA;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 531828
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531829
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 531830
    new-instance v3, LX/3DA;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v4

    check-cast v4, LX/0hy;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v6

    check-cast v6, LX/3Cm;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v9

    check-cast v9, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v10

    check-cast v10, LX/17W;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v12

    check-cast v12, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v13

    check-cast v13, LX/17Y;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v14

    check-cast v14, LX/1rU;

    invoke-direct/range {v3 .. v14}, LX/3DA;-><init>(LX/0hy;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/3Cm;Lcom/facebook/content/SecureContextHelper;LX/03V;Lcom/facebook/performancelogger/PerformanceLogger;LX/17W;LX/0ad;Lcom/facebook/auth/viewercontext/ViewerContext;LX/17Y;LX/1rU;)V

    .line 531831
    move-object v0, v3

    .line 531832
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 531833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3DA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 531835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)LX/BDx;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 531651
    iget-object v0, p0, LX/3DA;->c:LX/3Cm;

    invoke-virtual {v0, p1}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 531652
    if-nez v0, :cond_0

    .line 531653
    iget-object v0, p0, LX/3DA;->c:LX/3Cm;

    invoke-virtual {v0, p1}, LX/3Cm;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 531654
    :cond_0
    if-eqz v0, :cond_3

    .line 531655
    iget-object v2, p0, LX/3DA;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0xa0030

    const-string v4, "NNF_PermalinkNotificationLoad"

    invoke-interface {v2, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 531656
    invoke-static {p1}, LX/3Cm;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 531657
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 531658
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 531659
    :goto_0
    move-object v2, v2

    .line 531660
    if-eqz v2, :cond_6

    .line 531661
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 531662
    invoke-static {p0, p1}, LX/3DA;->b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 531663
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 531664
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 531665
    const-string v3, "tracking_notification_type"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 531666
    :goto_1
    invoke-direct {p0, p2}, LX/3DA;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531667
    if-nez v0, :cond_1

    .line 531668
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 531669
    :cond_1
    const-string v2, "show_next_notification"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 531670
    const-string v2, "notification_position_in_jewel"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 531671
    const-string v2, "parent_control_title_bar"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 531672
    :cond_2
    new-instance v2, LX/BDx;

    invoke-direct {v2, v0, v1}, LX/BDx;-><init>(Landroid/os/Bundle;Ljava/lang/String;)V

    move-object v1, v2

    .line 531673
    :cond_3
    return-object v1

    .line 531674
    :sswitch_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 531675
    invoke-static {p1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 531676
    if-nez v3, :cond_8

    .line 531677
    :goto_2
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 531678
    goto :goto_1

    .line 531679
    :sswitch_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 531680
    sget-object v2, Lcom/facebook/events/common/ActionSource;->NOTIFICATION:Lcom/facebook/events/common/ActionSource;

    invoke-static {v1, v2}, Lcom/facebook/events/common/ActionSource;->putActionRef(Landroid/os/Bundle;Lcom/facebook/events/common/ActionSource;)V

    .line 531681
    const-string v2, "story_cache_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531682
    const-string v2, "extra_ref_module"

    const-string v3, "push_notifications_tray"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531683
    invoke-static {p0, p1}, LX/3DA;->b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 531684
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 531685
    :try_start_0
    const-string v4, "notif_type"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531686
    :goto_3
    const-string v2, "tracking_codes"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531687
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 531688
    goto :goto_1

    .line 531689
    :sswitch_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 531690
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 531691
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    .line 531692
    invoke-static {v7}, LX/3Cm;->a(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 531693
    :goto_4
    move-object v1, v4

    .line 531694
    if-nez v1, :cond_4

    :goto_5
    move-object v1, v0

    move-object v0, v2

    .line 531695
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 531696
    goto :goto_5

    .line 531697
    :sswitch_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 531698
    invoke-static {p1, v2}, LX/3DA;->d(Lcom/facebook/graphql/model/GraphQLStory;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 531699
    if-nez v1, :cond_5

    :goto_6
    move-object v1, v0

    move-object v0, v2

    .line 531700
    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    .line 531701
    goto :goto_6

    .line 531702
    :sswitch_4
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 531703
    const-string v2, "source_jewel"

    invoke-static {p1, v2, v1}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 531704
    goto/16 :goto_1

    .line 531705
    :sswitch_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 531706
    const-string v2, "notification_launch_source"

    const-string v3, "source_jewel"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531707
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 531708
    goto/16 :goto_1

    :cond_6
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 531709
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_9

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v4, v2, v3}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 531710
    :catch_0
    move-exception v2

    .line 531711
    iget-object v4, p0, LX/3DA;->e:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Unable to put notifType into tracking"

    invoke-virtual {v4, v6, v7, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 531712
    :cond_a
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 531713
    const-string v3, "Recent Check-ins"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531714
    const-string v3, "Recent Reviews"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531715
    const-string v3, "Recent Shares"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531716
    const-string v3, "Recent Mentions"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531717
    move-object v8, v1

    .line 531718
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 531719
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v9

    .line 531720
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v10

    .line 531721
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 531722
    sget-object v3, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v3, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 531723
    const-string v4, "extra_is_admin"

    invoke-virtual {v2, v4, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 531724
    const-string v4, "extra_page_tab"

    sget-object v8, LX/BEQ;->ACTIVITY:LX/BEQ;

    invoke-virtual {v2, v4, v8}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 531725
    :goto_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_d

    .line 531726
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v8, 0x2954648e

    if-ne v4, v8, :cond_e

    .line 531727
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 531728
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 531729
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v6

    :goto_9
    if-ge v3, v8, :cond_c

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 531730
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    if-eqz v6, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 531731
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531732
    :cond_b
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    .line 531733
    :cond_c
    sget-object v1, LX/0ax;->aT:Ljava/lang/String;

    invoke-static {v1, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 531734
    const-string v1, "profile_name"

    invoke-virtual {v2, v1, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531735
    const-string v1, "event_id"

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 531736
    const-string v1, "extra_ref_module"

    const-string v4, "notifications"

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    :goto_a
    move-object v4, v3

    .line 531737
    goto/16 :goto_4

    .line 531738
    :cond_e
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v4, 0x329a13c8

    if-ne v1, v4, :cond_d

    iget-object v1, p0, LX/3DA;->h:LX/0ad;

    sget-short v4, LX/15r;->C:S

    invoke-interface {v1, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 531739
    const-string v1, "force_external_activity"

    invoke-virtual {v2, v1, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 531740
    sget-object v1, LX/0ax;->bf:Ljava/lang/String;

    invoke-static {v1, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_a

    :cond_f
    move-object v3, v4

    goto/16 :goto_8

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_2
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_1
        0x41e065f -> :sswitch_4
        0x4984e12 -> :sswitch_3
        0x4ed245b -> :sswitch_5
    .end sparse-switch
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 531824
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/3DA;->k:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 531785
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 531786
    iput-object v1, v0, LX/89k;->b:Ljava/lang/String;

    .line 531787
    move-object v0, v0

    .line 531788
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 531789
    iput-object v1, v0, LX/89k;->c:Ljava/lang/String;

    .line 531790
    move-object v0, v0

    .line 531791
    invoke-virtual {v0, p1}, LX/89k;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/89k;->d(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/89k;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/89k;->c(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    sget-object v1, LX/21C;->JEWEL:LX/21C;

    .line 531792
    iput-object v1, v0, LX/89k;->k:LX/21C;

    .line 531793
    move-object v0, v0

    .line 531794
    invoke-static {p1}, LX/16z;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/89k;->a(Z)LX/89k;

    move-result-object v0

    .line 531795
    invoke-static {p1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 531796
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 531797
    if-eqz v2, :cond_4

    .line 531798
    invoke-static {v2}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v2

    .line 531799
    iput-object v2, v0, LX/89k;->i:LX/21y;

    .line 531800
    if-eqz v1, :cond_0

    .line 531801
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 531802
    iput-object v1, v0, LX/89k;->f:Ljava/lang/String;

    .line 531803
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 531804
    :cond_1
    const/4 v1, -0x1

    .line 531805
    :goto_1
    move v1, v1

    .line 531806
    iput v1, v0, LX/89k;->o:I

    .line 531807
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 531808
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    .line 531809
    iput-object v1, v0, LX/89k;->h:Ljava/lang/String;

    .line 531810
    :cond_2
    iget-object v1, p0, LX/3DA;->a:LX/0hy;

    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 531811
    if-eqz v0, :cond_5

    .line 531812
    const-string v1, "notification_launch_source"

    const-string v2, "source_jewel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 531813
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v2, p0, LX/3DA;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 531814
    invoke-direct {p0, p2}, LX/3DA;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 531815
    const-string v1, "show_next_notification"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 531816
    const-string v1, "notification_position_in_jewel"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 531817
    const-string v1, "parent_control_title_bar"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 531818
    :cond_3
    :goto_2
    return-object v0

    .line 531819
    :cond_4
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/3DA;->h:LX/0ad;

    sget-short v3, LX/15r;->F:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 531820
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 531821
    iput-object v1, v0, LX/89k;->f:Ljava/lang/String;

    .line 531822
    goto/16 :goto_0

    .line 531823
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;->a()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRelevantReactorsEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRelevantReactorsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v1

    goto :goto_1
.end method

.method public static b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 531777
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 531778
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 531779
    const-string v1, "notif_type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 531780
    const-string v1, "notif_type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 531781
    :goto_0
    return-object v0

    .line 531782
    :catch_0
    move-exception v0

    .line 531783
    iget-object v1, p0, LX/3DA;->e:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Unable to parse notifStory.tracking as JSON"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 531784
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLStory;Landroid/os/Bundle;)Ljava/lang/String;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 531764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    .line 531765
    invoke-static {v0}, LX/3Cm;->a(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v2

    .line 531766
    :goto_0
    return-object v0

    .line 531767
    :cond_0
    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 531768
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 531769
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x3d5832f5

    if-ne v3, v4, :cond_1

    .line 531770
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v3

    .line 531771
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 531772
    invoke-static {v3}, LX/B5O;->a(Lcom/facebook/graphql/model/GraphQLImageOverlay;)LX/5QV;

    move-result-object v2

    .line 531773
    new-instance v4, LX/B4K;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "notification"

    invoke-direct {v4, v2, v5, v6}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, LX/B4J;->a(J)LX/B4J;

    move-result-object v1

    check-cast v1, LX/B4K;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0, v8}, LX/B4J;->a(I)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v0

    .line 531774
    const-string v1, "heisman_camera_intent_data"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 531775
    sget-object v0, LX/0ax;->dh:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "notification"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 531776
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 531741
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 531742
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 531743
    iget-object v2, p0, LX/3DA;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 531744
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BDy;

    .line 531745
    if-eqz v2, :cond_0

    .line 531746
    invoke-interface {v2}, LX/BDy;->a()Z

    move-result v2

    .line 531747
    if-eqz v2, :cond_0

    .line 531748
    const/4 v2, 0x1

    .line 531749
    :goto_0
    move v2, v2

    .line 531750
    if-eqz v2, :cond_1

    .line 531751
    iget-object v0, p0, LX/3DA;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    move v0, v1

    .line 531752
    :goto_1
    return v0

    .line 531753
    :cond_1
    invoke-static {p0, v0, p3}, LX/3DA;->a(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)LX/BDx;

    move-result-object v2

    .line 531754
    if-eqz v2, :cond_2

    .line 531755
    iget-object v0, p0, LX/3DA;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 531756
    iget-object v1, v2, LX/BDx;->b:Ljava/lang/String;

    move-object v1, v1

    .line 531757
    iget-object v3, v2, LX/BDx;->a:Landroid/os/Bundle;

    move-object v2, v3

    .line 531758
    const/4 v3, 0x0

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    move-result v0

    goto :goto_1

    .line 531759
    :cond_2
    invoke-static {p0, v0, p3}, LX/3DA;->b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/content/Intent;

    move-result-object v0

    .line 531760
    if-eqz v0, :cond_3

    .line 531761
    iget-object v2, p0, LX/3DA;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    move v0, v1

    .line 531762
    goto :goto_1

    .line 531763
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method
