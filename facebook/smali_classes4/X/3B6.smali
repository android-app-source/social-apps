.class public LX/3B6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3B9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 527394
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 527395
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 527396
    new-instance v0, LX/3sK;

    invoke-direct {v0}, LX/3sK;-><init>()V

    sput-object v0, LX/3B6;->a:LX/3B9;

    .line 527397
    :goto_0
    return-void

    .line 527398
    :cond_0
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 527399
    new-instance v0, LX/3ls;

    invoke-direct {v0}, LX/3ls;-><init>()V

    sput-object v0, LX/3B6;->a:LX/3B9;

    goto :goto_0

    .line 527400
    :cond_1
    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 527401
    new-instance v0, LX/3B7;

    invoke-direct {v0}, LX/3B7;-><init>()V

    sput-object v0, LX/3B6;->a:LX/3B9;

    goto :goto_0

    .line 527402
    :cond_2
    new-instance v0, LX/3B8;

    invoke-direct {v0}, LX/3B8;-><init>()V

    sput-object v0, LX/3B6;->a:LX/3B9;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 527403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527404
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
    .locals 7

    .prologue
    .line 527405
    sget-object v0, LX/3B6;->a:LX/3B9;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    .line 527406
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
    .locals 6

    .prologue
    .line 527407
    sget-object v0, LX/3B6;->a:LX/3B9;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, LX/3B9;->a(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    .line 527408
    return-void
.end method
