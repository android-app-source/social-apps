.class public LX/2a0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2a0;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Xl;

.field public c:LX/0Yb;

.field public volatile d:LX/2Zy;

.field public volatile e:LX/2Im;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423179
    iput-object p1, p0, LX/2a0;->a:Landroid/content/Context;

    .line 423180
    iput-object p2, p0, LX/2a0;->b:LX/0Xl;

    .line 423181
    return-void
.end method

.method public static a(LX/0QB;)LX/2a0;
    .locals 5

    .prologue
    .line 423182
    sget-object v0, LX/2a0;->f:LX/2a0;

    if-nez v0, :cond_1

    .line 423183
    const-class v1, LX/2a0;

    monitor-enter v1

    .line 423184
    :try_start_0
    sget-object v0, LX/2a0;->f:LX/2a0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423185
    if-eqz v2, :cond_0

    .line 423186
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423187
    new-instance p0, LX/2a0;

    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-direct {p0, v3, v4}, LX/2a0;-><init>(Landroid/content/Context;LX/0Xl;)V

    .line 423188
    move-object v0, p0

    .line 423189
    sput-object v0, LX/2a0;->f:LX/2a0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423190
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423191
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423192
    :cond_1
    sget-object v0, LX/2a0;->f:LX/2a0;

    return-object v0

    .line 423193
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/2a0;)LX/2Im;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 423195
    iget-object v0, p0, LX/2a0;->a:Landroid/content/Context;

    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 423196
    if-eqz v0, :cond_0

    move-object v0, v1

    .line 423197
    :goto_0
    return-object v0

    .line 423198
    :cond_0
    iget-object v0, p0, LX/2a0;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 423199
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 423200
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 423201
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 423202
    sget-object v0, LX/2Im;->CONNECTED:LX/2Im;

    goto :goto_0

    .line 423203
    :pswitch_0
    sget-object v0, LX/2Im;->CONNECTED_THROUGH_MOBILE:LX/2Im;

    goto :goto_0

    .line 423204
    :pswitch_1
    sget-object v0, LX/2Im;->CONNECTED_THROUGH_WIFI:LX/2Im;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 423205
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
