.class public LX/33a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 493877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493878
    iput-object p1, p0, LX/33a;->a:LX/0Zb;

    .line 493879
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 493873
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 493874
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 493875
    move-object v0, v0

    .line 493876
    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/8D0;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 493880
    invoke-static {p0}, LX/33a;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {p1}, LX/8D0;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/33a;
    .locals 2

    .prologue
    .line 493871
    new-instance v1, LX/33a;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/33a;-><init>(LX/0Zb;)V

    .line 493872
    return-object v1
.end method


# virtual methods
.method public final a(LX/8D0;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 493864
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/33a;->a(LX/8D0;Ljava/lang/String;Ljava/lang/String;)V

    .line 493865
    return-void
.end method

.method public final a(LX/8D0;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 493866
    const-string v0, "native_settings_row_clicked"

    invoke-static {v0, p1}, LX/33a;->a(Ljava/lang/String;LX/8D0;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "notif_option_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 493867
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 493868
    const-string v1, "object_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 493869
    :cond_0
    iget-object v1, p0, LX/33a;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 493870
    return-void
.end method
