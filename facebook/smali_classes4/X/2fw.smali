.class public LX/2fw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field private static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 447681
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "qp/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 447682
    sput-object v0, LX/2fw;->a:LX/0Tn;

    const-string v1, "dev_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->b:LX/0Tn;

    .line 447683
    sget-object v0, LX/2fw;->a:LX/0Tn;

    const-string v1, "show_all_segues"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->c:LX/0Tn;

    .line 447684
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "qp/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 447685
    sput-object v0, LX/2fw;->d:LX/0Tn;

    const-string v1, "last_action/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->e:LX/0Tn;

    .line 447686
    sget-object v0, LX/2fw;->d:LX/0Tn;

    const-string v1, "last_impression/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->f:LX/0Tn;

    .line 447687
    sget-object v0, LX/2fw;->d:LX/0Tn;

    const-string v1, "force_mode/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->g:LX/0Tn;

    .line 447688
    sget-object v0, LX/2fw;->d:LX/0Tn;

    const-string v1, "filter_mode/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2fw;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 447679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447680
    return-void
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)LX/0Tn;
    .locals 2

    .prologue
    .line 447689
    sget-object v0, LX/2fw;->h:LX/0Tn;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 447678
    sget-object v0, LX/2fw;->e:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 447677
    sget-object v0, LX/2fw;->f:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 447676
    sget-object v0, LX/2fw;->g:LX/0Tn;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
