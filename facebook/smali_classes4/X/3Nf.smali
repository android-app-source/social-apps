.class public abstract LX/3Nf;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/3Ng;


# instance fields
.field public a:LX/3O1;

.field public b:Landroid/view/View;

.field public c:Landroid/widget/EditText;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 559724
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 559725
    invoke-virtual {p0}, LX/3Nf;->b()V

    .line 559726
    const p1, 0x7f0d2084

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, LX/3Nf;->c:Landroid/widget/EditText;

    .line 559727
    const p1, 0x7f0d1da5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/3Nf;->b:Landroid/view/View;

    .line 559728
    const p1, 0x7f0d2082

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/3Nf;->d:Landroid/widget/ImageView;

    .line 559729
    invoke-virtual {p0}, LX/3Nf;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "input_method"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    iput-object p1, p0, LX/3Nf;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 559730
    iget-object p1, p0, LX/3Nf;->c:Landroid/widget/EditText;

    new-instance p2, LX/3Nh;

    invoke-direct {p2, p0}, LX/3Nh;-><init>(LX/3Nf;)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 559731
    iget-object p1, p0, LX/3Nf;->c:Landroid/widget/EditText;

    new-instance p2, LX/3Ni;

    invoke-direct {p2, p0}, LX/3Ni;-><init>(LX/3Nf;)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 559732
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 559723
    return-void
.end method

.method public abstract b()V
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 559707
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559708
    iget-object v0, p0, LX/3Nf;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 559709
    :cond_0
    invoke-virtual {p0}, LX/3Nf;->d()V

    .line 559710
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 559720
    iget-object v0, p0, LX/3Nf;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 559721
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 559722
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 559715
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559716
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 559717
    iget-object v0, p0, LX/3Nf;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 559718
    const/4 v0, 0x1

    .line 559719
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559714
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThisView()Landroid/view/View;
    .locals 0

    .prologue
    .line 559713
    return-object p0
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 559711
    iget-object v0, p0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 559712
    return-void
.end method
