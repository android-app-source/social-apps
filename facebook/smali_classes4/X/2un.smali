.class public LX/2un;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2un;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 476970
    invoke-direct {p0}, LX/398;-><init>()V

    .line 476971
    iput-object p1, p0, LX/2un;->a:LX/0Uh;

    .line 476972
    const/16 v0, 0x21c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476973
    sget-object v0, LX/0ax;->eQ:Ljava/lang/String;

    const-string v1, "extra_section_name"

    const-string v2, "extra_referer"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->SAVED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 476974
    sget-object v0, LX/0ax;->eO:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->SAVED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 476975
    :goto_0
    return-void

    .line 476976
    :cond_0
    sget-object v0, LX/0ax;->eQ:Ljava/lang/String;

    const-string v1, "extra_section_name"

    const-string v2, "extra_referer"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->eR:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 476977
    sget-object v0, LX/0ax;->eO:Ljava/lang/String;

    sget-object v1, LX/0ax;->eR:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2un;
    .locals 4

    .prologue
    .line 476956
    sget-object v0, LX/2un;->b:LX/2un;

    if-nez v0, :cond_1

    .line 476957
    const-class v1, LX/2un;

    monitor-enter v1

    .line 476958
    :try_start_0
    sget-object v0, LX/2un;->b:LX/2un;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 476959
    if-eqz v2, :cond_0

    .line 476960
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 476961
    new-instance p0, LX/2un;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/2un;-><init>(LX/0Uh;)V

    .line 476962
    move-object v0, p0

    .line 476963
    sput-object v0, LX/2un;->b:LX/2un;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 476964
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 476965
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 476966
    :cond_1
    sget-object v0, LX/2un;->b:LX/2un;

    return-object v0

    .line 476967
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 476968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 476969
    iget-object v0, p0, LX/2un;->a:LX/0Uh;

    const/16 v1, 0x21c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
