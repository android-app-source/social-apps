.class public final LX/3Hn;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 544770
    iput-object p1, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544771
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 544772
    check-cast p1, LX/2ow;

    .line 544773
    iget-object v1, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, p1, LX/2ow;->a:LX/2oN;

    sget-object v2, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v2, :cond_2

    const/4 v0, 0x1

    .line 544774
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->E:Z

    .line 544775
    iget-object v0, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->E:Z

    if-eqz v0, :cond_0

    .line 544776
    iget-object v0, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 544777
    :cond_0
    iget-object v0, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    const/16 v3, 0x8

    .line 544778
    sget-object v2, LX/2oN;->NONE:LX/2oN;

    if-eq v1, v2, :cond_1

    .line 544779
    iget-object v2, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 544780
    iget-object v2, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v2, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 544781
    :cond_1
    iget-object v0, p0, LX/3Hn;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(LX/2oN;)V

    .line 544782
    return-void

    .line 544783
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
