.class public LX/35v;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 497764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497765
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 497692
    const/4 v1, 0x4

    new-array v1, v1, [B

    .line 497693
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    .line 497694
    const-string v2, "RIFF"

    invoke-static {v1, v2}, LX/35v;->a([BLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 497695
    if-eqz p0, :cond_0

    .line 497696
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 497697
    :cond_0
    :goto_0
    return-object v0

    .line 497698
    :catch_0
    move-exception v1

    .line 497699
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 497700
    :cond_1
    :try_start_2
    invoke-static {p0}, LX/35v;->e(Ljava/io/InputStream;)I

    .line 497701
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    .line 497702
    const-string v2, "WEBP"

    invoke-static {v1, v2}, LX/35v;->a([BLjava/lang/String;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 497703
    if-eqz p0, :cond_0

    .line 497704
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 497705
    :catch_1
    move-exception v1

    .line 497706
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 497707
    :cond_2
    :try_start_4
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    .line 497708
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 497709
    const/4 v2, 0x0

    :goto_1
    array-length v4, v1

    if-ge v2, v4, :cond_3

    .line 497710
    aget-byte v4, v1, v2

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 497711
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 497712
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 497713
    const-string v2, "VP8 "

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 497714
    const-wide/16 v3, 0x7

    invoke-virtual {p0, v3, v4}, Ljava/io/InputStream;->skip(J)J

    .line 497715
    invoke-static {p0}, LX/35v;->h(Ljava/io/InputStream;)S

    move-result v3

    .line 497716
    invoke-static {p0}, LX/35v;->h(Ljava/io/InputStream;)S

    move-result v4

    .line 497717
    invoke-static {p0}, LX/35v;->h(Ljava/io/InputStream;)S

    move-result v5

    .line 497718
    const/16 v6, 0x9d

    if-ne v3, v6, :cond_4

    const/4 v3, 0x1

    if-ne v4, v3, :cond_4

    const/16 v3, 0x2a

    if-eq v5, v3, :cond_9

    .line 497719
    :cond_4
    const/4 v3, 0x0

    .line 497720
    :goto_2
    move-object v0, v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 497721
    if-eqz p0, :cond_0

    .line 497722
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 497723
    :catch_2
    move-exception v1

    .line 497724
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 497725
    :cond_5
    :try_start_6
    const-string v2, "VP8L"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 497726
    invoke-static {p0}, LX/35v;->e(Ljava/io/InputStream;)I

    .line 497727
    invoke-static {p0}, LX/35v;->i(Ljava/io/InputStream;)B

    move-result v1

    .line 497728
    const/16 v2, 0x2f

    if-eq v1, v2, :cond_a

    .line 497729
    const/4 v1, 0x0

    .line 497730
    :goto_3
    move-object v0, v1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 497731
    if-eqz p0, :cond_0

    .line 497732
    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_0

    .line 497733
    :catch_3
    move-exception v1

    .line 497734
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 497735
    :cond_6
    :try_start_8
    const-string v2, "VP8X"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 497736
    const-wide/16 v3, 0x8

    invoke-virtual {p0, v3, v4}, Ljava/io/InputStream;->skip(J)J

    .line 497737
    new-instance v3, Landroid/util/Pair;

    invoke-static {p0}, LX/35v;->g(Ljava/io/InputStream;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p0}, LX/35v;->g(Ljava/io/InputStream;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v3
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 497738
    if-eqz p0, :cond_0

    .line 497739
    :try_start_9
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 497740
    :catch_4
    move-exception v1

    .line 497741
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 497742
    :cond_7
    if-eqz p0, :cond_0

    .line 497743
    :try_start_a
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_0

    .line 497744
    :catch_5
    move-exception v1

    .line 497745
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 497746
    :catch_6
    move-exception v1

    .line 497747
    :try_start_b
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 497748
    if-eqz p0, :cond_0

    .line 497749
    :try_start_c
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto/16 :goto_0

    .line 497750
    :catch_7
    move-exception v1

    .line 497751
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 497752
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_8

    .line 497753
    :try_start_d
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 497754
    :cond_8
    :goto_4
    throw v0

    .line 497755
    :catch_8
    move-exception v1

    .line 497756
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :cond_9
    :try_start_e
    new-instance v3, Landroid/util/Pair;

    invoke-static {p0}, LX/35v;->f(Ljava/io/InputStream;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p0}, LX/35v;->f(Ljava/io/InputStream;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 497757
    :cond_a
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    and-int/lit16 v1, v1, 0xff

    .line 497758
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    int-to-byte v2, v2

    and-int/lit16 v2, v2, 0xff

    .line 497759
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    and-int/lit16 v3, v3, 0xff

    .line 497760
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    int-to-byte v4, v4

    and-int/lit16 v4, v4, 0xff

    .line 497761
    and-int/lit8 v5, v2, 0x3f

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v1, v5

    add-int/lit8 v5, v1, 0x1

    .line 497762
    and-int/lit8 v1, v4, 0xf

    shl-int/lit8 v1, v1, 0xa

    shl-int/lit8 v3, v3, 0x2

    or-int/2addr v1, v3

    and-int/lit16 v2, v2, 0xc0

    shr-int/lit8 v2, v2, 0x6

    or-int/2addr v1, v2

    add-int/lit8 v2, v1, 0x1

    .line 497763
    new-instance v1, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_3
.end method

.method private static a([BLjava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 497686
    array-length v0, p0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 497687
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 497688
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 497689
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    aget-byte v3, p0, v0

    if-ne v2, v3, :cond_0

    .line 497690
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 497691
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static e(Ljava/io/InputStream;)I
    .locals 5

    .prologue
    .line 497681
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    .line 497682
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    .line 497683
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    int-to-byte v2, v2

    .line 497684
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    .line 497685
    shl-int/lit8 v3, v3, 0x18

    const/high16 v4, -0x1000000

    and-int/2addr v3, v4

    shl-int/lit8 v2, v2, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v2, v4

    or-int/2addr v2, v3

    shl-int/lit8 v1, v1, 0x8

    const v3, 0xff00

    and-int/2addr v1, v3

    or-int/2addr v1, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static f(Ljava/io/InputStream;)I
    .locals 3

    .prologue
    .line 497672
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    .line 497673
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    .line 497674
    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static g(Ljava/io/InputStream;)I
    .locals 4

    .prologue
    .line 497677
    invoke-static {p0}, LX/35v;->i(Ljava/io/InputStream;)B

    move-result v0

    .line 497678
    invoke-static {p0}, LX/35v;->i(Ljava/io/InputStream;)B

    move-result v1

    .line 497679
    invoke-static {p0}, LX/35v;->i(Ljava/io/InputStream;)B

    move-result v2

    .line 497680
    shl-int/lit8 v2, v2, 0x10

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    shl-int/lit8 v1, v1, 0x8

    const v3, 0xff00

    and-int/2addr v1, v3

    or-int/2addr v1, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static h(Ljava/io/InputStream;)S
    .locals 1

    .prologue
    .line 497676
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    return v0
.end method

.method public static i(Ljava/io/InputStream;)B
    .locals 1

    .prologue
    .line 497675
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    return v0
.end method
