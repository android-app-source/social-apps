.class public final enum LX/3NQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3NQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3NQ;

.field public static final enum BOTS_FILTER:LX/3NQ;

.field public static final enum FRIEND_FILTER:LX/3NQ;

.field public static final enum GROUP_FILTER:LX/3NQ;

.field public static final enum M_FILTER:LX/3NQ;

.field public static final enum NON_FRIENDS_FILTER:LX/3NQ;

.field public static final enum PAGES_FILTER:LX/3NQ;

.field public static final enum TINCAN_FILTER:LX/3NQ;

.field public static final enum VC_ENDPOINTS_FILTER:LX/3NQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 559165
    new-instance v0, LX/3NQ;

    const-string v1, "M_FILTER"

    invoke-direct {v0, v1, v3}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->M_FILTER:LX/3NQ;

    .line 559166
    new-instance v0, LX/3NQ;

    const-string v1, "FRIEND_FILTER"

    invoke-direct {v0, v1, v4}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->FRIEND_FILTER:LX/3NQ;

    .line 559167
    new-instance v0, LX/3NQ;

    const-string v1, "GROUP_FILTER"

    invoke-direct {v0, v1, v5}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->GROUP_FILTER:LX/3NQ;

    .line 559168
    new-instance v0, LX/3NQ;

    const-string v1, "TINCAN_FILTER"

    invoke-direct {v0, v1, v6}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->TINCAN_FILTER:LX/3NQ;

    .line 559169
    new-instance v0, LX/3NQ;

    const-string v1, "VC_ENDPOINTS_FILTER"

    invoke-direct {v0, v1, v7}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->VC_ENDPOINTS_FILTER:LX/3NQ;

    .line 559170
    new-instance v0, LX/3NQ;

    const-string v1, "PAGES_FILTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->PAGES_FILTER:LX/3NQ;

    .line 559171
    new-instance v0, LX/3NQ;

    const-string v1, "BOTS_FILTER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->BOTS_FILTER:LX/3NQ;

    .line 559172
    new-instance v0, LX/3NQ;

    const-string v1, "NON_FRIENDS_FILTER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3NQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3NQ;->NON_FRIENDS_FILTER:LX/3NQ;

    .line 559173
    const/16 v0, 0x8

    new-array v0, v0, [LX/3NQ;

    sget-object v1, LX/3NQ;->M_FILTER:LX/3NQ;

    aput-object v1, v0, v3

    sget-object v1, LX/3NQ;->FRIEND_FILTER:LX/3NQ;

    aput-object v1, v0, v4

    sget-object v1, LX/3NQ;->GROUP_FILTER:LX/3NQ;

    aput-object v1, v0, v5

    sget-object v1, LX/3NQ;->TINCAN_FILTER:LX/3NQ;

    aput-object v1, v0, v6

    sget-object v1, LX/3NQ;->VC_ENDPOINTS_FILTER:LX/3NQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3NQ;->PAGES_FILTER:LX/3NQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3NQ;->BOTS_FILTER:LX/3NQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3NQ;->NON_FRIENDS_FILTER:LX/3NQ;

    aput-object v2, v0, v1

    sput-object v0, LX/3NQ;->$VALUES:[LX/3NQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 559174
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3NQ;
    .locals 1

    .prologue
    .line 559175
    const-class v0, LX/3NQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3NQ;

    return-object v0
.end method

.method public static values()[LX/3NQ;
    .locals 1

    .prologue
    .line 559176
    sget-object v0, LX/3NQ;->$VALUES:[LX/3NQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3NQ;

    return-object v0
.end method
