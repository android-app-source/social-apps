.class public LX/3Qm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 565791
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "graph_search/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 565792
    sput-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "db_bootstrap_entities_last_fetch_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->b:LX/0Tn;

    .line 565793
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "db_bootstrap_keywords_last_fetch_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->c:LX/0Tn;

    .line 565794
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "db_bootstrap_last_fetch_user_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->d:LX/0Tn;

    .line 565795
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "injected_trending_topic_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->e:LX/0Tn;

    .line 565796
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "injected_pulse_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->f:LX/0Tn;

    .line 565797
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "phonetic_tokens_generated"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->g:LX/0Tn;

    .line 565798
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "num_extra_profiles_added_to_bootstrap"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->h:LX/0Tn;

    .line 565799
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "extra_profile_ids_added_to_bootstrap"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->i:LX/0Tn;

    .line 565800
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "post_search_enabled_override"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->j:LX/0Tn;

    .line 565801
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "search_native_template_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->k:LX/0Tn;

    .line 565802
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "search_nux_dev_trigger_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->l:LX/0Tn;

    .line 565803
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "no_components"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->m:LX/0Tn;

    .line 565804
    sget-object v0, LX/3Qm;->a:LX/0Tn;

    const-string v1, "search_result_debug_info_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Qm;->n:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 565790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
