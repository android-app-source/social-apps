.class public LX/2PJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2PJ;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/01T;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;LX/01T;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/DoO;",
            ">;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406247
    const/4 v0, 0x0

    iput-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    .line 406248
    iput-object p1, p0, LX/2PJ;->a:LX/0Or;

    .line 406249
    iput-object p2, p0, LX/2PJ;->b:LX/01T;

    .line 406250
    return-void
.end method

.method public static a(LX/0QB;)LX/2PJ;
    .locals 5

    .prologue
    .line 406251
    sget-object v0, LX/2PJ;->d:LX/2PJ;

    if-nez v0, :cond_1

    .line 406252
    const-class v1, LX/2PJ;

    monitor-enter v1

    .line 406253
    :try_start_0
    sget-object v0, LX/2PJ;->d:LX/2PJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406254
    if-eqz v2, :cond_0

    .line 406255
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406256
    new-instance v4, LX/2PJ;

    const/16 v3, 0x2a22

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-direct {v4, p0, v3}, LX/2PJ;-><init>(LX/0Or;LX/01T;)V

    .line 406257
    move-object v0, v4

    .line 406258
    sput-object v0, LX/2PJ;->d:LX/2PJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406259
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406260
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406261
    :cond_1
    sget-object v0, LX/2PJ;->d:LX/2PJ;

    return-object v0

    .line 406262
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 406264
    iget-object v0, p0, LX/2PJ;->b:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    .line 406265
    iget-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 406266
    iget-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    .line 406267
    :goto_0
    move-object v0, v0

    .line 406268
    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406269
    :goto_1
    return-object v0

    :cond_0
    const-string v0, "deviceidinvalid"

    goto :goto_1

    .line 406270
    :cond_1
    iget-object v0, p0, LX/2PJ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoO;

    .line 406271
    if-nez v0, :cond_2

    .line 406272
    const-string v0, "TincanDeviceIdHolder"

    const-string v1, "Could not retrieve a valid identity key store when generating Tincan device ID"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 406273
    const-string v0, "deviceidinvalid"

    .line 406274
    :goto_2
    move-object v0, v0

    .line 406275
    iput-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    .line 406276
    iget-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    goto :goto_0

    .line 406277
    :cond_2
    invoke-interface {v0}, LX/DoO;->a()LX/Eaf;

    move-result-object v0

    .line 406278
    if-nez v0, :cond_3

    .line 406279
    const-string v0, "TincanDeviceIdHolder"

    const-string v1, "Could not retrieve a valid identity key to go into Tincan device ID"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 406280
    const-string v0, "deviceidinvalid"

    goto :goto_2

    .line 406281
    :cond_3
    iget-object v1, v0, LX/Eaf;->a:LX/Eae;

    move-object v0, v1

    .line 406282
    invoke-virtual {v0}, LX/Eae;->b()[B

    move-result-object v0

    .line 406283
    sget-object v1, LX/1I6;->c:LX/1I6;

    move-object v1, v1

    .line 406284
    invoke-virtual {v1}, LX/1I6;->b()LX/1I6;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 406285
    const/4 v0, 0x0

    iput-object v0, p0, LX/2PJ;->c:Ljava/lang/String;

    .line 406286
    return-void
.end method
