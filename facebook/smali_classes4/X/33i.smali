.class public LX/33i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/33h;

.field private final c:LX/33k;

.field private final d:LX/0W9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494132
    const-class v0, LX/33i;

    sput-object v0, LX/33i;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/33h;LX/33k;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494128
    iput-object p1, p0, LX/33i;->b:LX/33h;

    .line 494129
    iput-object p2, p0, LX/33i;->c:LX/33k;

    .line 494130
    iput-object p3, p0, LX/33i;->d:LX/0W9;

    .line 494131
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 494106
    check-cast p1, Ljava/lang/String;

    .line 494107
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 494108
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "megaphone_location"

    iget-object v2, p0, LX/33i;->c:LX/33k;

    invoke-interface {v2}, LX/33k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494109
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "zero_campaign"

    invoke-direct {v0, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494110
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494111
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "locale"

    iget-object v2, p0, LX/33i;->d:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494112
    new-instance v0, LX/14N;

    const-string v1, "fetchZeroIndicator"

    const-string v2, "GET"

    const-string v3, "me/megaphone_top_stories"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 494113
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 494114
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    .line 494115
    if-nez v1, :cond_0

    .line 494116
    const/4 v1, 0x0

    .line 494117
    :goto_0
    move-object v0, v1

    .line 494118
    return-object v0

    .line 494119
    :cond_0
    const-string v2, "content"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 494120
    const-string v2, "action"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 494121
    const-string v2, "id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 494122
    const-string v3, "title"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v3, ""

    invoke-static {v1, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 494123
    const-string v1, "text"

    invoke-virtual {v4, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v4, ""

    invoke-static {v1, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 494124
    const-string v1, "title"

    invoke-virtual {v6, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v5, ""

    invoke-static {v1, v5}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 494125
    const-string v1, "url"

    invoke-virtual {v6, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v6, ""

    invoke-static {v1, v6}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 494126
    new-instance v1, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
