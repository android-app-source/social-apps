.class public abstract LX/29W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public state:LX/29X;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 376350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376351
    sget-object v0, LX/29X;->NOT_READY:LX/29X;

    iput-object v0, p0, LX/29W;->state:LX/29X;

    .line 376352
    return-void
.end method


# virtual methods
.method public abstract computeNext()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final endOfData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 376330
    sget-object v0, LX/29X;->DONE:LX/29X;

    iput-object v0, p0, LX/29W;->state:LX/29X;

    .line 376331
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 376339
    iget-object v0, p0, LX/29W;->state:LX/29X;

    sget-object v3, LX/29X;->FAILED:LX/29X;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 376340
    sget-object v0, LX/29Y;->$SwitchMap$com$google$common$base$AbstractIterator$State:[I

    iget-object v3, p0, LX/29W;->state:LX/29X;

    invoke-virtual {v3}, LX/29X;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 376341
    sget-object v0, LX/29X;->FAILED:LX/29X;

    iput-object v0, p0, LX/29W;->state:LX/29X;

    .line 376342
    invoke-virtual {p0}, LX/29W;->computeNext()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/29W;->next:Ljava/lang/Object;

    .line 376343
    iget-object v0, p0, LX/29W;->state:LX/29X;

    sget-object v1, LX/29X;->DONE:LX/29X;

    if-eq v0, v1, :cond_1

    .line 376344
    sget-object v0, LX/29X;->READY:LX/29X;

    iput-object v0, p0, LX/29W;->state:LX/29X;

    .line 376345
    const/4 v0, 0x1

    .line 376346
    :goto_1
    move v1, v0

    .line 376347
    :goto_2
    :pswitch_0
    return v1

    :cond_0
    move v0, v2

    .line 376348
    goto :goto_0

    :pswitch_1
    move v1, v2

    .line 376349
    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 376333
    invoke-virtual {p0}, LX/29W;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376334
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 376335
    :cond_0
    sget-object v0, LX/29X;->NOT_READY:LX/29X;

    iput-object v0, p0, LX/29W;->state:LX/29X;

    .line 376336
    iget-object v0, p0, LX/29W;->next:Ljava/lang/Object;

    .line 376337
    const/4 v1, 0x0

    iput-object v1, p0, LX/29W;->next:Ljava/lang/Object;

    .line 376338
    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 376332
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
