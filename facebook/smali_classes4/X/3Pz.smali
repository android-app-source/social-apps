.class public LX/3Pz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Pz;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0tX;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562606
    iput-object p1, p0, LX/3Pz;->a:Ljava/lang/String;

    .line 562607
    iput-object p2, p0, LX/3Pz;->b:LX/0tX;

    .line 562608
    return-void
.end method

.method public static a(LX/0QB;)LX/3Pz;
    .locals 5

    .prologue
    .line 562609
    sget-object v0, LX/3Pz;->c:LX/3Pz;

    if-nez v0, :cond_1

    .line 562610
    const-class v1, LX/3Pz;

    monitor-enter v1

    .line 562611
    :try_start_0
    sget-object v0, LX/3Pz;->c:LX/3Pz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562612
    if-eqz v2, :cond_0

    .line 562613
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 562614
    new-instance p0, LX/3Pz;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/3Pz;-><init>(Ljava/lang/String;LX/0tX;)V

    .line 562615
    move-object v0, p0

    .line 562616
    sput-object v0, LX/3Pz;->c:LX/3Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562617
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562618
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562619
    :cond_1
    sget-object v0, LX/3Pz;->c:LX/3Pz;

    return-object v0

    .line 562620
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
