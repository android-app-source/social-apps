.class public LX/2M0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2M0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396006
    return-void
.end method

.method public static a(LX/0QB;)LX/2M0;
    .locals 3

    .prologue
    .line 395993
    sget-object v0, LX/2M0;->a:LX/2M0;

    if-nez v0, :cond_1

    .line 395994
    const-class v1, LX/2M0;

    monitor-enter v1

    .line 395995
    :try_start_0
    sget-object v0, LX/2M0;->a:LX/2M0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395996
    if-eqz v2, :cond_0

    .line 395997
    :try_start_1
    new-instance v0, LX/2M0;

    invoke-direct {v0}, LX/2M0;-><init>()V

    .line 395998
    move-object v0, v0

    .line 395999
    sput-object v0, LX/2M0;->a:LX/2M0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396000
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396001
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396002
    :cond_1
    sget-object v0, LX/2M0;->a:LX/2M0;

    return-object v0

    .line 396003
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/messaging/model/attachment/Attachment;)Z
    .locals 3

    .prologue
    .line 396007
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    .line 396008
    iget-object v1, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 396009
    if-eqz v1, :cond_0

    const-string v2, "audio/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v1, "audioclip-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/messaging/model/attachment/Attachment;)Z
    .locals 2

    .prologue
    .line 395991
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 395992
    if-eqz v0, :cond_0

    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/messaging/model/attachment/Attachment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 395987
    iget-object v1, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-nez v1, :cond_1

    .line 395988
    :cond_0
    :goto_0
    return v0

    .line 395989
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v1, v1, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    if-eqz v1, :cond_0

    .line 395990
    const/4 v0, 0x1

    goto :goto_0
.end method
