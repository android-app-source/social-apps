.class public LX/2R4;
.super LX/2QY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QY",
        "<",
        "Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;",
        "Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Or;LX/2R5;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2R5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409428
    const-string v0, "ak_seamless_login"

    invoke-direct {p0, v0, p1, p2}, LX/2QY;-><init>(Ljava/lang/String;LX/0Or;LX/0e6;)V

    .line 409429
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 409430
    check-cast p1, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Result;

    .line 409431
    invoke-static {p1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409432
    const-string v0, "ak_seamless_login_param"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;

    return-object v0
.end method
