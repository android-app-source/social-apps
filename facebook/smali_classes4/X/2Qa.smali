.class public LX/2Qa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;",
        "Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 408560
    const-class v0, LX/2Qa;

    sput-object v0, LX/2Qa;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408562
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 408563
    check-cast p1, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;

    .line 408564
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_id"

    iget-object v2, p1, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 408565
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "queryProxiedPermissions"

    .line 408566
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 408567
    move-object v1, v1

    .line 408568
    const-string v2, "GET"

    .line 408569
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 408570
    move-object v1, v1

    .line 408571
    const-string v2, "/v2.7/me/permissions"

    .line 408572
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 408573
    move-object v1, v1

    .line 408574
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 408575
    move-object v0, v1

    .line 408576
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 408577
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 408578
    move-object v0, v0

    .line 408579
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 408580
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    .line 408581
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 408582
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 408583
    if-eqz v0, :cond_0

    .line 408584
    const-string v2, "data"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    .line 408585
    if-eqz v0, :cond_0

    .line 408586
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 408587
    const-string v3, "permission"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 408588
    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 408589
    :cond_0
    new-instance v0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;

    invoke-direct {v0, v1}, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method
