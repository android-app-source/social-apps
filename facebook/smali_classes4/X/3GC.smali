.class public LX/3GC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 540713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)F
    .locals 9

    .prologue
    .line 540709
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 540710
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 540711
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static b(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)J
    .locals 4

    .prologue
    .line 540712
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    return-wide v0
.end method
