.class public final LX/39l;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/39j;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/39k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39j",
            "<TE;>.FooterButtonComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/39j;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/39j;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 523875
    iput-object p1, p0, LX/39l;->b:LX/39j;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 523876
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "downstateType"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "contentDescription"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "viewTags"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "textColor"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/39l;->c:[Ljava/lang/String;

    .line 523877
    iput v3, p0, LX/39l;->d:I

    .line 523878
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/39l;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/39l;LX/1De;IILX/39k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/39j",
            "<TE;>.FooterButtonComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 523871
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 523872
    iput-object p4, p0, LX/39l;->a:LX/39k;

    .line 523873
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 523874
    return-void
.end method


# virtual methods
.method public final a(LX/1Pr;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523868
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->b:LX/1Pr;

    .line 523869
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523870
    return-object p0
.end method

.method public final a(LX/1Wk;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Wk;",
            ")",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523865
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->c:LX/1Wk;

    .line 523866
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523867
    return-object p0
.end method

.method public final a(LX/1dc;)LX/39l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523863
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->m:LX/1dc;

    .line 523864
    return-object p0
.end method

.method public final a(LX/1n6;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523861
    iget-object v0, p0, LX/39l;->a:LX/39k;

    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/39k;->m:LX/1dc;

    .line 523862
    return-object p0
.end method

.method public final a(Landroid/util/SparseArray;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523858
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->f:Landroid/util/SparseArray;

    .line 523859
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523860
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523855
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 523856
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523857
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523852
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->e:Ljava/lang/CharSequence;

    .line 523853
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523854
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 523879
    invoke-super {p0}, LX/1X5;->a()V

    .line 523880
    const/4 v0, 0x0

    iput-object v0, p0, LX/39l;->a:LX/39k;

    .line 523881
    iget-object v0, p0, LX/39l;->b:LX/39j;

    iget-object v0, v0, LX/39j;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 523882
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523849
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->g:Ljava/lang/CharSequence;

    .line 523850
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523851
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/39l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523846
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput-object p1, v0, LX/39k;->d:Ljava/lang/String;

    .line 523847
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523848
    return-object p0
.end method

.method public final c(F)LX/39l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523844
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput p1, v0, LX/39k;->j:F

    .line 523845
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/39j;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 523834
    iget-object v1, p0, LX/39l;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/39l;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/39l;->d:I

    if-ge v1, v2, :cond_2

    .line 523835
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 523836
    :goto_0
    iget v2, p0, LX/39l;->d:I

    if-ge v0, v2, :cond_1

    .line 523837
    iget-object v2, p0, LX/39l;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 523838
    iget-object v2, p0, LX/39l;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523839
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 523840
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523841
    :cond_2
    iget-object v0, p0, LX/39l;->a:LX/39k;

    .line 523842
    invoke-virtual {p0}, LX/39l;->a()V

    .line 523843
    return-object v0
.end method

.method public final d(F)LX/39l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523819
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput p1, v0, LX/39k;->k:F

    .line 523820
    return-object p0
.end method

.method public final e(F)LX/39l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523832
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput p1, v0, LX/39k;->l:F

    .line 523833
    return-object p0
.end method

.method public final h(I)LX/39l;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523829
    iget-object v0, p0, LX/39l;->a:LX/39k;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/39k;->e:Ljava/lang/CharSequence;

    .line 523830
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523831
    return-object p0
.end method

.method public final i(I)LX/39l;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523826
    iget-object v0, p0, LX/39l;->a:LX/39k;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/39k;->g:Ljava/lang/CharSequence;

    .line 523827
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523828
    return-object p0
.end method

.method public final j(I)LX/39l;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523823
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput p1, v0, LX/39k;->h:I

    .line 523824
    iget-object v0, p0, LX/39l;->e:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 523825
    return-object p0
.end method

.method public final k(I)LX/39l;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/39j",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 523821
    iget-object v0, p0, LX/39l;->a:LX/39k;

    iput p1, v0, LX/39k;->i:I

    .line 523822
    return-object p0
.end method
