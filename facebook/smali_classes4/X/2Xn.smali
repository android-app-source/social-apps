.class public LX/2Xn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/28d;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Xm;

.field private final b:LX/0hw;

.field private final c:LX/0dC;


# direct methods
.method public constructor <init>(LX/2Xm;LX/0hw;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420216
    iput-object p1, p0, LX/2Xn;->a:LX/2Xm;

    .line 420217
    iput-object p2, p0, LX/2Xn;->b:LX/0hw;

    .line 420218
    iput-object p3, p0, LX/2Xn;->c:LX/0dC;

    .line 420219
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xn;
    .locals 1

    .prologue
    .line 420220
    invoke-static {p0}, LX/2Xn;->b(LX/0QB;)LX/2Xn;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Xn;
    .locals 4

    .prologue
    .line 420221
    new-instance v3, LX/2Xn;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v0

    check-cast v0, LX/2Xm;

    invoke-static {p0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v1

    check-cast v1, LX/0hw;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v2

    check-cast v2, LX/0dC;

    invoke-direct {v3, v0, v1, v2}, LX/2Xn;-><init>(LX/2Xm;LX/0hw;LX/0dC;)V

    .line 420222
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 420223
    check-cast p1, LX/28d;

    .line 420224
    iget-object v0, p1, LX/28d;->a:Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 420225
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 420226
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "adid"

    iget-object v4, p0, LX/2Xn;->b:LX/0hw;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420227
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420228
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/2Xn;->c:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420229
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "email"

    .line 420230
    iget-object v4, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    move-object v4, v4

    .line 420231
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420232
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "password"

    .line 420233
    iget-object v4, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->b:Ljava/lang/String;

    move-object v4, v4

    .line 420234
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420235
    iget-object v2, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->c:LX/28K;

    move-object v2, v2

    .line 420236
    invoke-virtual {v2}, LX/28K;->getServerValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 420237
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "credentials_type"

    .line 420238
    iget-object v4, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->c:LX/28K;

    move-object v0, v4

    .line 420239
    invoke-virtual {v0}, LX/28K;->getServerValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420240
    :cond_0
    iget-boolean v0, p1, LX/28d;->d:Z

    if-eqz v0, :cond_1

    .line 420241
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_session_cookies"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420242
    :cond_1
    iget-object v0, p1, LX/28d;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 420243
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "error_detail_type"

    iget-object v3, p1, LX/28d;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420244
    :cond_2
    iget-object v0, p1, LX/28d;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 420245
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    iget-object v3, p1, LX/28d;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420246
    :goto_0
    iget-object v0, p1, LX/28d;->c:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 420247
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_latitude"

    iget-object v3, p1, LX/28d;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420248
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_longitude"

    iget-object v3, p1, LX/28d;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420249
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_accuracy_m"

    iget-object v3, p1, LX/28d;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420250
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_timestamp_ms"

    iget-object v3, p1, LX/28d;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420251
    :cond_3
    iget-object v0, p1, LX/28d;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 420252
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "code_verifier"

    iget-object v3, p1, LX/28d;->f:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420253
    :cond_4
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 420254
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 420255
    move-object v0, v0

    .line 420256
    const-string v2, "POST"

    .line 420257
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 420258
    move-object v0, v0

    .line 420259
    const-string v2, "method/auth.login"

    .line 420260
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 420261
    move-object v0, v0

    .line 420262
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 420263
    move-object v0, v0

    .line 420264
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 420265
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 420266
    move-object v0, v0

    .line 420267
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 420268
    :cond_5
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_machine_id"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 420269
    check-cast p1, LX/28d;

    .line 420270
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 420271
    iget-object v0, p1, LX/28d;->a:Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 420272
    iget-object v1, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    move-object v0, v1

    .line 420273
    iget-object v1, p0, LX/2Xn;->a:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/28d;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
