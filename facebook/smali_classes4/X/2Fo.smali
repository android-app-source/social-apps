.class public LX/2Fo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fp;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/12x;

.field public final d:LX/0So;

.field public final e:LX/0W3;

.field private final f:Lcom/facebook/content/SecureContextHelper;

.field private final g:Landroid/hardware/SensorManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387355
    const-class v0, LX/2Fo;

    sput-object v0, LX/2Fo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12x;LX/0So;LX/0W3;Lcom/facebook/content/SecureContextHelper;Landroid/hardware/SensorManager;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387348
    iput-object p1, p0, LX/2Fo;->b:Landroid/content/Context;

    .line 387349
    iput-object p2, p0, LX/2Fo;->c:LX/12x;

    .line 387350
    iput-object p3, p0, LX/2Fo;->d:LX/0So;

    .line 387351
    iput-object p4, p0, LX/2Fo;->e:LX/0W3;

    .line 387352
    iput-object p5, p0, LX/2Fo;->f:Lcom/facebook/content/SecureContextHelper;

    .line 387353
    iput-object p6, p0, LX/2Fo;->g:Landroid/hardware/SensorManager;

    .line 387354
    return-void
.end method

.method public static b(LX/0QB;)LX/2Fo;
    .locals 7

    .prologue
    .line 387345
    new-instance v0, LX/2Fo;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v2

    check-cast v2, LX/12x;

    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/2Fq;->b(LX/0QB;)Landroid/hardware/SensorManager;

    move-result-object v6

    check-cast v6, Landroid/hardware/SensorManager;

    invoke-direct/range {v0 .. v6}, LX/2Fo;-><init>(Landroid/content/Context;LX/12x;LX/0So;LX/0W3;Lcom/facebook/content/SecureContextHelper;Landroid/hardware/SensorManager;)V

    .line 387346
    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 387356
    sget-wide v0, LX/0X5;->w:J

    return-wide v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 387342
    iget-object v0, p0, LX/2Fo;->g:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 387343
    iget-object v0, p0, LX/2Fo;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2Fo;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 387344
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 387336
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Fo;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 387337
    iget-object v1, p0, LX/2Fo;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x20000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 387338
    if-eqz v0, :cond_0

    .line 387339
    iget-object v1, p0, LX/2Fo;->c:LX/12x;

    invoke-virtual {v1, v0}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 387340
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 387341
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 12

    .prologue
    .line 387323
    iget-object v0, p0, LX/2Fo;->f:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2Fo;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, LX/2Fo;->b:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 387324
    iget-object v4, p0, LX/2Fo;->e:LX/0W3;

    sget-wide v6, LX/0X5;->C:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    .line 387325
    iget-object v5, p0, LX/2Fo;->e:LX/0W3;

    sget-wide v6, LX/0X5;->B:J

    invoke-interface {v5, v6, v7}, LX/0W4;->a(J)Z

    move-result v5

    .line 387326
    if-eqz v4, :cond_0

    const/4 v4, 0x2

    .line 387327
    :goto_0
    iget-object v6, p0, LX/2Fo;->e:LX/0W3;

    sget-wide v8, LX/0X5;->y:J

    invoke-interface {v6, v8, v9}, LX/0W4;->c(J)J

    move-result-wide v6

    .line 387328
    new-instance v8, Landroid/content/Intent;

    iget-object v9, p0, LX/2Fo;->b:Landroid/content/Context;

    const-class v10, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 387329
    iget-object v9, p0, LX/2Fo;->b:Landroid/content/Context;

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    invoke-static {v9, v10, v8, v11}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 387330
    iget-object v9, p0, LX/2Fo;->d:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v10

    add-long/2addr v6, v10

    .line 387331
    if-eqz v5, :cond_1

    .line 387332
    iget-object v5, p0, LX/2Fo;->c:LX/12x;

    invoke-virtual {v5, v4, v6, v7, v8}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 387333
    :goto_1
    return-void

    .line 387334
    :cond_0
    const/4 v4, 0x3

    goto :goto_0

    .line 387335
    :cond_1
    iget-object v5, p0, LX/2Fo;->c:LX/12x;

    invoke-virtual {v5, v4, v6, v7, v8}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    goto :goto_1
.end method
