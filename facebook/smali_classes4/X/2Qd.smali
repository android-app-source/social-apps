.class public LX/2Qd;
.super LX/2QZ;
.source ""


# instance fields
.field private final b:LX/2Qe;

.field private final c:LX/2Qr;

.field private final d:LX/2Qi;


# direct methods
.method public constructor <init>(LX/2Qe;LX/2Qr;LX/2Qi;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408652
    const-string v0, "platform_add_pending_media_upload"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 408653
    iput-object p1, p0, LX/2Qd;->b:LX/2Qe;

    .line 408654
    iput-object p2, p0, LX/2Qd;->c:LX/2Qr;

    .line 408655
    iput-object p3, p0, LX/2Qd;->d:LX/2Qi;

    .line 408656
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 408657
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 408658
    const-string v1, "platform_add_pending_media_upload_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;

    .line 408659
    iget-object v1, p0, LX/2Qd;->d:LX/2Qi;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 408660
    const v2, 0x406552fd

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 408661
    :try_start_0
    iget-object v2, v0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    move-object v2, v2

    .line 408662
    iget-object v3, p0, LX/2Qd;->c:LX/2Qr;

    .line 408663
    iget-object v4, v3, LX/2Qr;->b:LX/2Qi;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 408664
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 408665
    sget-object v4, LX/2Qo;->a:LX/0U1;

    .line 408666
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408667
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v7, v7

    .line 408668
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408669
    sget-object v4, LX/2Qo;->b:LX/0U1;

    .line 408670
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408671
    iget v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    move v7, v7

    .line 408672
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408673
    sget-object v4, LX/2Qo;->c:LX/0U1;

    .line 408674
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v7

    .line 408675
    iget-boolean v4, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    move v4, v4

    .line 408676
    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408677
    sget-object v4, LX/2Qo;->d:LX/0U1;

    .line 408678
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408679
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    move-object v7, v7

    .line 408680
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408681
    sget-object v4, LX/2Qo;->e:LX/0U1;

    .line 408682
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408683
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v7, v7

    .line 408684
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408685
    sget-object v4, LX/2Qo;->f:LX/0U1;

    .line 408686
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408687
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    move-object v7, v7

    .line 408688
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408689
    sget-object v4, LX/2Qo;->g:LX/0U1;

    .line 408690
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408691
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    move-object v7, v7

    .line 408692
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408693
    sget-object v4, LX/2Qo;->h:LX/0U1;

    .line 408694
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408695
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    move-object v7, v7

    .line 408696
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408697
    sget-object v4, LX/2Qo;->i:LX/0U1;

    .line 408698
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408699
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v7, v7

    .line 408700
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408701
    sget-object v4, LX/2Qo;->j:LX/0U1;

    .line 408702
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 408703
    iget-object v7, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    move-object v7, v7

    .line 408704
    invoke-virtual {v6, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408705
    const-string v4, "pending_app_calls"

    const/4 v7, 0x0

    const p1, -0x7498dee5

    invoke-static {p1}, LX/03h;->a(I)V

    invoke-virtual {v5, v4, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, -0x39ce4f16

    invoke-static {v4}, LX/03h;->a(I)V

    .line 408706
    new-instance v3, Lcom/facebook/katana/platform/PendingMediaUpload;

    .line 408707
    iget-object v4, v0, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;->b:Ljava/lang/String;

    move-object v0, v4

    .line 408708
    iget-object v4, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v2, v4

    .line 408709
    invoke-direct {v3, v0, v2}, Lcom/facebook/katana/platform/PendingMediaUpload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 408710
    iget-object v0, p0, LX/2Qd;->b:LX/2Qe;

    .line 408711
    iget-object v2, v0, LX/2Qe;->b:LX/2Qi;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 408712
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 408713
    sget-object v5, LX/2Qg;->a:LX/0U1;

    .line 408714
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 408715
    iget-object v6, v3, Lcom/facebook/katana/platform/PendingMediaUpload;->a:Ljava/lang/String;

    move-object v6, v6

    .line 408716
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408717
    sget-object v5, LX/2Qg;->b:LX/0U1;

    .line 408718
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 408719
    iget-object v6, v3, Lcom/facebook/katana/platform/PendingMediaUpload;->b:Ljava/lang/String;

    move-object v6, v6

    .line 408720
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408721
    const-string v5, "pending_media_uploads"

    const/4 v6, 0x0

    const v7, 0x2f6174b2

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, 0x63593875

    invoke-static {v2}, LX/03h;->a(I)V

    .line 408722
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408723
    const v0, 0x144b90f9

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 408724
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 408725
    return-object v0

    .line 408726
    :catchall_0
    move-exception v0

    const v2, -0x9286cd

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 408727
    :cond_0
    const/4 v4, 0x0

    goto/16 :goto_0
.end method
