.class public LX/2vJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/39j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39j",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1vg;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/39j;LX/1vg;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477674
    iput-object p1, p0, LX/2vJ;->a:LX/39j;

    .line 477675
    iput-object p2, p0, LX/2vJ;->b:LX/1vg;

    .line 477676
    iput-object p3, p0, LX/2vJ;->c:LX/0Uh;

    .line 477677
    return-void
.end method

.method private static a(LX/2vJ;I)I
    .locals 3

    .prologue
    .line 477672
    const v0, -0x6e685d

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/2vJ;->c:LX/0Uh;

    const/16 v1, 0x3ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, -0xe2ded7

    :cond_0
    return p1
.end method

.method public static a(LX/0QB;)LX/2vJ;
    .locals 6

    .prologue
    .line 477653
    const-class v1, LX/2vJ;

    monitor-enter v1

    .line 477654
    :try_start_0
    sget-object v0, LX/2vJ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 477655
    sput-object v2, LX/2vJ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 477656
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477657
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 477658
    new-instance p0, LX/2vJ;

    invoke-static {v0}, LX/39j;->a(LX/0QB;)LX/39j;

    move-result-object v3

    check-cast v3, LX/39j;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/2vJ;-><init>(LX/39j;LX/1vg;LX/0Uh;)V

    .line 477659
    move-object v0, p0

    .line 477660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 477661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2vJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 477663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/20X;LX/1Wk;ZLX/1Pr;IIIFFF)LX/1Dg;
    .locals 6
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/20X;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Wk;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p12    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/20X;",
            "LX/1Wk;",
            "ZTE;IIIFFF)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 477664
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 477665
    invoke-static {p0, p7}, LX/2vJ;->a(LX/2vJ;I)I

    move-result v2

    .line 477666
    iget-object v3, p0, LX/2vJ;->a:LX/39j;

    invoke-virtual {v3, p1}, LX/39j;->c(LX/1De;)LX/39l;

    move-result-object v3

    invoke-static {p3, v1}, LX/39m;->b(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/39l;->h(I)LX/39l;

    move-result-object v3

    invoke-static {p3}, LX/39m;->a(LX/20X;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/39l;->a(Landroid/util/SparseArray;)LX/39l;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/39l;->a(LX/1Wk;)LX/39l;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/39l;->a(LX/1Pr;)LX/39l;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/39l;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39l;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, LX/20X;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/39l;->b(Ljava/lang/String;)LX/39l;

    move-result-object v3

    invoke-static {p3, v1}, LX/39m;->a(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/39l;->i(I)LX/39l;

    move-result-object v3

    invoke-static {p3, v1, v2}, LX/39m;->a(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;I)I

    move-result v2

    invoke-virtual {v3, v2}, LX/39l;->j(I)LX/39l;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/39l;->k(I)LX/39l;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/39l;->c(F)LX/39l;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/39l;->d(F)LX/39l;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v2, v0}, LX/39l;->e(F)LX/39l;

    move-result-object v2

    .line 477667
    if-eqz p5, :cond_0

    .line 477668
    invoke-static {p0, p8}, LX/2vJ;->a(LX/2vJ;I)I

    move-result v3

    .line 477669
    iget-object v4, p0, LX/2vJ;->b:LX/1vg;

    invoke-static {p1, v1, p3, v4, v3}, LX/39m;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLStory;LX/20X;LX/1vg;I)LX/1dc;

    move-result-object v1

    .line 477670
    invoke-virtual {v2, v1}, LX/39l;->a(LX/1dc;)LX/39l;

    .line 477671
    :cond_0
    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v1

    return-object v1
.end method
