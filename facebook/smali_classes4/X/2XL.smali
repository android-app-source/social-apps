.class public LX/2XL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;",
        "Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419776
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 419777
    check-cast p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;

    .line 419778
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 419779
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419780
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "return_structure"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419781
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 419782
    iget-object v1, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 419783
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 419784
    const-string v2, "url"

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 419785
    :cond_0
    const-string v1, "token"

    .line 419786
    iget-object v2, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 419787
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 419788
    const-string v1, "device_id"

    .line 419789
    iget-object v2, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 419790
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 419791
    const-string v1, "is_initial_reg"

    .line 419792
    iget-boolean v2, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->e:Z

    move v2, v2

    .line 419793
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 419794
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 419795
    const-string v2, "android_build"

    .line 419796
    iget v3, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->f:I

    move v3, v3

    .line 419797
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 419798
    const-string v2, "android_setting_mask"

    .line 419799
    iget v3, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->g:I

    move v3, v3

    .line 419800
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 419801
    const-string v2, "orca_muted_until_ms"

    .line 419802
    iget-wide v8, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->h:J

    move-wide v6, v8

    .line 419803
    invoke-virtual {v1, v2, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 419804
    const-string v2, "extra_data"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 419805
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "protocol_params"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419806
    new-instance v0, LX/14N;

    const-string v1, "registerPush"

    const-string v2, "POST"

    const-string v3, "method/user.registerPushCallback"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 419807
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 419808
    new-instance v1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;

    const-string v2, "success"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->g(LX/0lF;)Z

    move-result v2

    const-string v3, "previously_disabled"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;-><init>(ZZJ)V

    return-object v1
.end method
