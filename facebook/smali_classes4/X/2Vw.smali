.class public LX/2Vw;
.super LX/2Vx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Vx",
        "<",
        "LX/FCa;",
        "[B>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Vw;


# direct methods
.method public constructor <init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V
    .locals 10
    .param p5    # LX/0rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1Ha;
        .annotation build Lcom/facebook/messaging/audio/playback/AudioFileCache;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 418271
    const-string v0, "audio"

    const-string v1, "audio"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/2Vw;->a(Ljava/lang/String;Ljava/lang/String;Z)LX/2W2;

    move-result-object v5

    invoke-static {}, LX/2Vw;->a()LX/2W4;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/2Vx;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/2W2;LX/0rb;LX/1Ha;LX/2W4;LX/1GQ;)V

    .line 418272
    return-void
.end method

.method public static a(LX/0QB;)LX/2Vw;
    .locals 11

    .prologue
    .line 418255
    sget-object v0, LX/2Vw;->a:LX/2Vw;

    if-nez v0, :cond_1

    .line 418256
    const-class v1, LX/2Vw;

    monitor-enter v1

    .line 418257
    :try_start_0
    sget-object v0, LX/2Vw;->a:LX/2Vw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 418258
    if-eqz v2, :cond_0

    .line 418259
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 418260
    new-instance v3, LX/2Vw;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v8

    check-cast v8, LX/0rb;

    invoke-static {v0}, LX/2Vz;->a(LX/0QB;)LX/1Ha;

    move-result-object v9

    check-cast v9, LX/1Ha;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v10

    check-cast v10, LX/1GQ;

    invoke-direct/range {v3 .. v10}, LX/2Vw;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V

    .line 418261
    move-object v0, v3

    .line 418262
    sput-object v0, LX/2Vw;->a:LX/2Vw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418263
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 418264
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 418265
    :cond_1
    sget-object v0, LX/2Vw;->a:LX/2Vw;

    return-object v0

    .line 418266
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 418267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Z)LX/2W2;
    .locals 3

    .prologue
    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 418273
    new-instance v0, LX/2W2;

    invoke-direct {v0}, LX/2W2;-><init>()V

    .line 418274
    iput-object p0, v0, LX/2W2;->a:Ljava/lang/String;

    .line 418275
    move-object v0, v0

    .line 418276
    iput-object p1, v0, LX/2W2;->b:Ljava/lang/String;

    .line 418277
    move-object v0, v0

    .line 418278
    iput-boolean p2, v0, LX/2W2;->c:Z

    .line 418279
    move-object v0, v0

    .line 418280
    iput v2, v0, LX/2W2;->f:I

    .line 418281
    move-object v0, v0

    .line 418282
    iput v2, v0, LX/2W2;->g:I

    .line 418283
    move-object v0, v0

    .line 418284
    iput v1, v0, LX/2W2;->d:I

    .line 418285
    move-object v0, v0

    .line 418286
    iput v1, v0, LX/2W2;->e:I

    .line 418287
    move-object v0, v0

    .line 418288
    return-object v0
.end method

.method private static a()LX/2W4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2W4",
            "<",
            "LX/FCa;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 418270
    new-instance v0, LX/2W3;

    invoke-direct {v0}, LX/2W3;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 418268
    check-cast p1, [B

    .line 418269
    array-length v0, p1

    return v0
.end method
