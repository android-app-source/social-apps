.class public LX/2LL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile p:LX/2LL;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/2LY;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/00H;

.field public final e:LX/0xB;

.field public final f:LX/2LM;

.field public final g:LX/0xW;

.field public final h:LX/0WJ;

.field private final i:LX/2LT;

.field public final j:LX/23i;

.field public final k:Ljava/util/concurrent/ExecutorService;

.field public final l:LX/0ad;

.field public final m:Z

.field public final n:Z

.field public final o:LX/0fO;


# direct methods
.method public constructor <init>(LX/0xB;LX/0Or;LX/00H;LX/2LM;LX/2LT;LX/23i;Ljava/util/concurrent/ExecutorService;LX/0xW;LX/0ad;LX/0xX;LX/0WJ;Ljava/lang/Boolean;LX/0fO;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/launcherbadges/annotations/IsAppIconBadgingOnHtcEnabled;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0xB;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/00H;",
            "Lcom/facebook/launcherbadges/LauncherBadgesController;",
            "LX/2LT;",
            "LX/23i;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0xW;",
            "LX/0ad;",
            "LX/0xX;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "Ljava/lang/Boolean;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394846
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    iput-object v0, p0, LX/2LL;->e:LX/0xB;

    .line 394847
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    iput-object v0, p0, LX/2LL;->c:LX/0Or;

    .line 394848
    iput-object p3, p0, LX/2LL;->d:LX/00H;

    .line 394849
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2LM;

    iput-object v0, p0, LX/2LL;->f:LX/2LM;

    .line 394850
    iput-object p8, p0, LX/2LL;->g:LX/0xW;

    .line 394851
    iput-object p11, p0, LX/2LL;->h:LX/0WJ;

    .line 394852
    invoke-virtual {p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2LL;->n:Z

    .line 394853
    new-instance v0, LX/2LY;

    invoke-direct {v0, p0}, LX/2LY;-><init>(LX/2LL;)V

    iput-object v0, p0, LX/2LL;->b:LX/2LY;

    .line 394854
    iput-object p5, p0, LX/2LL;->i:LX/2LT;

    .line 394855
    iput-object p6, p0, LX/2LL;->j:LX/23i;

    .line 394856
    iput-object p7, p0, LX/2LL;->k:Ljava/util/concurrent/ExecutorService;

    .line 394857
    iput-object p9, p0, LX/2LL;->l:LX/0ad;

    .line 394858
    invoke-virtual {p10}, LX/0xX;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/2LL;->m:Z

    .line 394859
    iput-object p13, p0, LX/2LL;->o:LX/0fO;

    .line 394860
    return-void
.end method

.method public static a(LX/0QB;)LX/2LL;
    .locals 3

    .prologue
    .line 394826
    sget-object v0, LX/2LL;->p:LX/2LL;

    if-nez v0, :cond_1

    .line 394827
    const-class v1, LX/2LL;

    monitor-enter v1

    .line 394828
    :try_start_0
    sget-object v0, LX/2LL;->p:LX/2LL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394829
    if-eqz v2, :cond_0

    .line 394830
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2LL;->b(LX/0QB;)LX/2LL;

    move-result-object v0

    sput-object v0, LX/2LL;->p:LX/2LL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394831
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394832
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394833
    :cond_1
    sget-object v0, LX/2LL;->p:LX/2LL;

    return-object v0

    .line 394834
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2LL;
    .locals 14

    .prologue
    .line 394836
    new-instance v0, LX/2LL;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v1

    check-cast v1, LX/0xB;

    const/16 v2, 0x32d

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const-class v3, LX/00H;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    invoke-static {p0}, LX/2LM;->a(LX/0QB;)LX/2LM;

    move-result-object v4

    check-cast v4, LX/2LM;

    invoke-static {p0}, LX/2LT;->a(LX/0QB;)LX/2LT;

    move-result-object v5

    check-cast v5, LX/2LT;

    invoke-static {p0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v6

    check-cast v6, LX/23i;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v8

    check-cast v8, LX/0xW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v10

    check-cast v10, LX/0xX;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v11

    check-cast v11, LX/0WJ;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v13

    check-cast v13, LX/0fO;

    invoke-direct/range {v0 .. v13}, LX/2LL;-><init>(LX/0xB;LX/0Or;LX/00H;LX/2LM;LX/2LT;LX/23i;Ljava/util/concurrent/ExecutorService;LX/0xW;LX/0ad;LX/0xX;LX/0WJ;Ljava/lang/Boolean;LX/0fO;)V

    .line 394837
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    .line 394838
    iput-object v1, v0, LX/2LL;->a:LX/0Uh;

    .line 394839
    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 394840
    iget-object v0, p0, LX/2LL;->k:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$1;-><init>(LX/2LL;I)V

    const v2, 0x5d5dbe48

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 394841
    return-void
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 394842
    iget-object v0, p0, LX/2LL;->e:LX/0xB;

    iget-object v1, p0, LX/2LL;->b:LX/2LY;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/0xA;)V

    .line 394843
    iget-object v0, p0, LX/2LL;->b:LX/2LY;

    invoke-static {v0}, LX/2LY;->c(LX/2LY;)V

    .line 394844
    return-void
.end method
