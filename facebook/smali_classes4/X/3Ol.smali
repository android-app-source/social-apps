.class public LX/3Ol;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1se;

.field public final b:LX/2C3;

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/2C3;)V
    .locals 1

    .prologue
    .line 560722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560723
    new-instance v0, LX/1se;

    invoke-direct {v0, p1, p2}, LX/1se;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/3Ol;->a:LX/1se;

    .line 560724
    iput-object p3, p0, LX/3Ol;->b:LX/2C3;

    .line 560725
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Ol;->c:Z

    .line 560726
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 560727
    if-ne p0, p1, :cond_1

    .line 560728
    :cond_0
    :goto_0
    return v0

    .line 560729
    :cond_1
    instance-of v2, p1, LX/3Ol;

    if-nez v2, :cond_2

    move v0, v1

    .line 560730
    goto :goto_0

    .line 560731
    :cond_2
    check-cast p1, LX/3Ol;

    .line 560732
    iget-object v2, p0, LX/3Ol;->a:LX/1se;

    .line 560733
    iget-object v3, p1, LX/3Ol;->a:LX/1se;

    move-object v3, v3

    .line 560734
    invoke-virtual {v2, v3}, LX/1se;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/3Ol;->b:LX/2C3;

    .line 560735
    iget-object v3, p1, LX/3Ol;->b:LX/2C3;

    move-object v3, v3

    .line 560736
    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/3Ol;->c:Z

    .line 560737
    iget-boolean v3, p1, LX/3Ol;->c:Z

    move v3, v3

    .line 560738
    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 560739
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3Ol;->a:LX/1se;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/3Ol;->b:LX/2C3;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/3Ol;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
