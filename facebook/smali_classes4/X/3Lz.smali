.class public LX/3Lz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final ALL_PLUS_NUMBER_GROUPING_SYMBOLS:LX/3M3;

.field public static final ALPHA_MAPPINGS:LX/3M3;

.field private static final ALPHA_PHONE_MAPPINGS:LX/3M3;

.field public static final ASCII_DIGIT_MAPPINGS:LX/3M3;

.field public static final DEFAULT_METADATA_LOADER:LX/3M1;

.field private static final DIALLABLE_CHAR_MAPPINGS:LX/3M3;

.field public static LAZY_CAPTURING_DIGIT_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_CC_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_EXTN_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_FIRST_GROUP_ONLY_PREFIX_PATTERN:Ljava/util/regex/Pattern;

.field private static LAZY_FIRST_GROUP_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_NON_DIGITS_PATTERN:Ljava/util/regex/Pattern;

.field private static LAZY_PLUS_CHARS_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_SECOND_NUMBER_START_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_UNWANTED_END_CHAR_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_VALID_ALPHA_PHONE_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_VALID_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

.field public static LAZY_VALID_START_CHAR_PATTERN:Ljava/util/regex/Pattern;

.field private static instance:LX/3Lz;

.field public static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/4hL;",
            ">;"
        }
    .end annotation
.end field

.field private final currentFilePrefix:Ljava/lang/String;

.field public final data:LX/3M9;

.field private final mContext:Landroid/content/Context;

.field private final metadataLoader:LX/3M1;

.field public final regexCache:LX/3MC;

.field private final regionToMetadataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4hL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 553167
    new-instance v0, LX/3M0;

    invoke-direct {v0}, LX/3M0;-><init>()V

    sput-object v0, LX/3Lz;->DEFAULT_METADATA_LOADER:LX/3M1;

    .line 553168
    const-class v0, LX/3Lz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/3Lz;->logger:Ljava/util/logging/Logger;

    .line 553169
    new-instance v0, LX/3M2;

    invoke-direct {v0}, LX/3M2;-><init>()V

    sput-object v0, LX/3Lz;->ASCII_DIGIT_MAPPINGS:LX/3M3;

    .line 553170
    new-instance v0, LX/3M4;

    invoke-direct {v0}, LX/3M4;-><init>()V

    sput-object v0, LX/3Lz;->ALPHA_MAPPINGS:LX/3M3;

    .line 553171
    new-instance v0, LX/3M5;

    invoke-direct {v0}, LX/3M5;-><init>()V

    sput-object v0, LX/3Lz;->ALPHA_PHONE_MAPPINGS:LX/3M3;

    .line 553172
    new-instance v0, LX/3M6;

    invoke-direct {v0}, LX/3M6;-><init>()V

    sput-object v0, LX/3Lz;->DIALLABLE_CHAR_MAPPINGS:LX/3M3;

    .line 553173
    new-instance v0, LX/3M7;

    invoke-direct {v0}, LX/3M7;-><init>()V

    sput-object v0, LX/3Lz;->ALL_PLUS_NUMBER_GROUPING_SYMBOLS:LX/3M3;

    .line 553174
    const/4 v0, 0x0

    sput-object v0, LX/3Lz;->instance:LX/3Lz;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/3M1;LX/3M9;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 553175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/3Lz;->regionToMetadataMap:Ljava/util/Map;

    .line 553177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/3Lz;->countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;

    .line 553178
    new-instance v0, LX/3MC;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/3MC;-><init>(I)V

    iput-object v0, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553179
    iput-object p4, p0, LX/3Lz;->mContext:Landroid/content/Context;

    .line 553180
    iput-object p1, p0, LX/3Lz;->currentFilePrefix:Ljava/lang/String;

    .line 553181
    iput-object p2, p0, LX/3Lz;->metadataLoader:LX/3M1;

    .line 553182
    iput-object p3, p0, LX/3Lz;->data:LX/3M9;

    .line 553183
    return-void
.end method

.method private buildNationalNumberForParsing(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    .line 553184
    const-string v0, ";phone-context="

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 553185
    if-lez v1, :cond_4

    .line 553186
    add-int/lit8 v0, v1, 0xf

    .line 553187
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2b

    if-ne v2, v3, :cond_0

    .line 553188
    const/16 v2, 0x3b

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 553189
    if-lez v2, :cond_2

    .line 553190
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553191
    :cond_0
    :goto_0
    const-string v0, "tel:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 553192
    if-ltz v0, :cond_3

    add-int/lit8 v0, v0, 0x4

    .line 553193
    :goto_1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553194
    :goto_2
    const-string v0, ";isub="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 553195
    if-lez v0, :cond_1

    .line 553196
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 553197
    :cond_1
    return-void

    .line 553198
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 553199
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 553200
    :cond_4
    const/4 p0, 0x0

    .line 553201
    const-string v0, "getValidStartCharPattern()"

    sget-object v1, LX/3Lz;->LAZY_VALID_START_CHAR_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "[+\uff0b\\p{Nd}]"

    invoke-static {v0, v1, v2}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_VALID_START_CHAR_PATTERN:Ljava/util/regex/Pattern;

    move-object v0, v0

    .line 553202
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 553203
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 553204
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 553205
    const-string v1, "getUnwantedEndCharPattern()"

    sget-object v2, LX/3Lz;->LAZY_UNWANTED_END_CHAR_PATTERN:Ljava/util/regex/Pattern;

    const-string v3, "[[\\P{N}&&\\P{L}]&&[^#]]+$"

    invoke-static {v1, v2, v3}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, LX/3Lz;->LAZY_UNWANTED_END_CHAR_PATTERN:Ljava/util/regex/Pattern;

    move-object v1, v1

    .line 553206
    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553207
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 553208
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 553209
    sget-object v1, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Stripped trailing characters: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 553210
    :cond_5
    const-string v1, "getSecondNumberStartPattern()"

    sget-object v2, LX/3Lz;->LAZY_SECOND_NUMBER_START_PATTERN:Ljava/util/regex/Pattern;

    const-string v3, "[\\\\/] *x"

    invoke-static {v1, v2, v3}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, LX/3Lz;->LAZY_SECOND_NUMBER_START_PATTERN:Ljava/util/regex/Pattern;

    move-object v1, v1

    .line 553211
    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553212
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 553213
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 553214
    :cond_6
    :goto_3
    move-object v0, v0

    .line 553215
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_7
    const-string v0, ""

    goto :goto_3
.end method

.method public static createInstance(LX/3M1;Landroid/content/Context;)LX/3Lz;
    .locals 3

    .prologue
    .line 553216
    if-nez p0, :cond_0

    .line 553217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "metadataLoader could not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553218
    :cond_0
    new-instance v0, LX/3Lz;

    const-string v1, "PhoneNumberMetadataProto"

    new-instance v2, LX/3M9;

    invoke-direct {v2}, LX/3M9;-><init>()V

    invoke-direct {v0, v1, p0, v2, p1}, LX/3Lz;-><init>(Ljava/lang/String;LX/3M1;LX/3M9;Landroid/content/Context;)V

    return-object v0
.end method

.method public static formatNsn(LX/3Lz;Ljava/lang/String;LX/4hL;LX/4hG;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 553219
    const/4 v0, 0x0

    .line 553220
    iget-object v1, p2, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    move-object v1, v1

    .line 553221
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/4hG;->NATIONAL:LX/4hG;

    if-ne p3, v1, :cond_3

    .line 553222
    :cond_0
    iget-object v1, p2, LX/4hL;->numberFormat_:Ljava/util/List;

    move-object v1, v1

    .line 553223
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4hJ;

    .line 553224
    invoke-virtual {v2}, LX/4hJ;->leadingDigitsPatternSize()I

    move-result v4

    .line 553225
    if-eqz v4, :cond_2

    iget-object p2, p0, LX/3Lz;->regexCache:LX/3MC;

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, LX/4hJ;->getLeadingDigitsPattern(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 553226
    :cond_2
    iget-object v4, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553227
    iget-object p2, v2, LX/4hJ;->pattern_:Ljava/lang/String;

    move-object p2, p2

    .line 553228
    invoke-virtual {v4, p2}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 553229
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 553230
    :goto_1
    move-object v1, v2

    .line 553231
    if-nez v1, :cond_4

    :goto_2
    move-object v0, p1

    .line 553232
    return-object v0

    .line 553233
    :cond_3
    iget-object v1, p2, LX/4hL;->intlNumberFormat_:Ljava/util/List;

    move-object v1, v1

    .line 553234
    goto :goto_0

    .line 553235
    :cond_4
    iget-object v2, v1, LX/4hJ;->format_:Ljava/lang/String;

    move-object v2, v2

    .line 553236
    iget-object v3, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553237
    iget-object v4, v1, LX/4hJ;->pattern_:Ljava/lang/String;

    move-object v4, v4

    .line 553238
    invoke-virtual {v3, v4}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 553239
    sget-object v4, LX/4hG;->NATIONAL:LX/4hG;

    if-ne p3, v4, :cond_9

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_9

    .line 553240
    iget-object v4, v1, LX/4hJ;->domesticCarrierCodeFormattingRule_:Ljava/lang/String;

    move-object v4, v4

    .line 553241
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_9

    .line 553242
    iget-object v4, v1, LX/4hJ;->domesticCarrierCodeFormattingRule_:Ljava/lang/String;

    move-object v4, v4

    .line 553243
    const-string p2, "getCcPattern()"

    sget-object p0, LX/3Lz;->LAZY_CC_PATTERN:Ljava/util/regex/Pattern;

    const-string p1, "\\$CC"

    invoke-static {p2, p0, p1}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p2

    sput-object p2, LX/3Lz;->LAZY_CC_PATTERN:Ljava/util/regex/Pattern;

    move-object p2, p2

    .line 553244
    invoke-virtual {p2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 553245
    invoke-static {}, LX/3Lz;->getFirstGroupPattern()Ljava/util/regex/Pattern;

    move-result-object p2

    invoke-virtual {p2, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 553246
    :cond_5
    invoke-virtual {v3, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 553247
    :goto_3
    sget-object v3, LX/4hG;->RFC3966:LX/4hG;

    if-ne p3, v3, :cond_7

    .line 553248
    const-string v3, "getSeparatorPattern()"

    sget-object v4, LX/3Lz;->LAZY_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    const-string p2, "[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+"

    invoke-static {v3, v4, p2}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    sput-object v3, LX/3Lz;->LAZY_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    move-object v3, v3

    .line 553249
    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 553250
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 553251
    const-string v2, ""

    invoke-virtual {v3, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 553252
    :cond_6
    invoke-virtual {v3, v2}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 553253
    :cond_7
    move-object p1, v2

    .line 553254
    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 553255
    :cond_9
    iget-object v4, v1, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    move-object v4, v4

    .line 553256
    sget-object p2, LX/4hG;->NATIONAL:LX/4hG;

    if-ne p3, p2, :cond_5

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_5

    .line 553257
    invoke-static {}, LX/3Lz;->getFirstGroupPattern()Ljava/util/regex/Pattern;

    move-result-object p2

    invoke-virtual {p2, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 553258
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method public static getCountryCodeForValidRegion(LX/3Lz;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 553259
    iget-object v0, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v0, p1, v1}, LX/3M9;->getCountryCallingCodeForRegionCode(Ljava/lang/String;I)I

    move-result v0

    .line 553260
    if-eq v0, v1, :cond_0

    .line 553261
    :goto_0
    return v0

    .line 553262
    :cond_0
    invoke-virtual {p0, p1}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    .line 553263
    if-nez v0, :cond_1

    .line 553264
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid region code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553265
    :cond_1
    iget-object v1, p0, LX/3Lz;->data:LX/3M9;

    .line 553266
    iget v2, v0, LX/4hL;->countryCode_:I

    move v2, v2

    .line 553267
    invoke-virtual {v1, p1, v2}, LX/3M9;->addRegionCodeToCountryCallingCodeMapping(Ljava/lang/String;I)Z

    .line 553268
    iget v1, v0, LX/4hL;->countryCode_:I

    move v0, v1

    .line 553269
    goto :goto_0
.end method

.method public static getFirstGroupPattern()Ljava/util/regex/Pattern;
    .locals 3

    .prologue
    .line 553270
    const-string v0, "getFirstGroupPattern()"

    sget-object v1, LX/3Lz;->LAZY_FIRST_GROUP_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "(\\$\\d)"

    invoke-static {v0, v1, v2}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_FIRST_GROUP_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)LX/3Lz;
    .locals 2

    .prologue
    .line 553271
    const-class v1, LX/3Lz;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/3Lz;->instance:LX/3Lz;

    if-nez v0, :cond_0

    .line 553272
    new-instance v0, LX/3M8;

    invoke-direct {v0, p0}, LX/3M8;-><init>(Landroid/content/Context;)V

    .line 553273
    invoke-static {v0, p0}, LX/3Lz;->createInstance(LX/3M1;Landroid/content/Context;)LX/3Lz;

    move-result-object v0

    invoke-static {v0}, LX/3Lz;->setInstance(LX/3Lz;)V

    .line 553274
    :cond_0
    sget-object v0, LX/3Lz;->instance:LX/3Lz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 553275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getMetadataForRegionOrCallingCode(LX/3Lz;ILjava/lang/String;)LX/4hL;
    .locals 1

    .prologue
    .line 553276
    const-string v0, "001"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/3Lz;->getMetadataForNonGeographicalRegion(I)LX/4hL;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v0

    goto :goto_0
.end method

.method public static getNumberTypeHelper(LX/3Lz;Ljava/lang/String;LX/4hL;)LX/4hH;
    .locals 1

    .prologue
    .line 553277
    iget-object v0, p2, LX/4hL;->generalDesc_:LX/4hP;

    move-object v0, v0

    .line 553278
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 553279
    sget-object v0, LX/4hH;->UNKNOWN:LX/4hH;

    .line 553280
    :goto_0
    return-object v0

    .line 553281
    :cond_0
    iget-object v0, p2, LX/4hL;->premiumRate_:LX/4hP;

    move-object v0, v0

    .line 553282
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553283
    sget-object v0, LX/4hH;->PREMIUM_RATE:LX/4hH;

    goto :goto_0

    .line 553284
    :cond_1
    iget-object v0, p2, LX/4hL;->tollFree_:LX/4hP;

    move-object v0, v0

    .line 553285
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553286
    sget-object v0, LX/4hH;->TOLL_FREE:LX/4hH;

    goto :goto_0

    .line 553287
    :cond_2
    iget-object v0, p2, LX/4hL;->sharedCost_:LX/4hP;

    move-object v0, v0

    .line 553288
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553289
    sget-object v0, LX/4hH;->SHARED_COST:LX/4hH;

    goto :goto_0

    .line 553290
    :cond_3
    iget-object v0, p2, LX/4hL;->voip_:LX/4hP;

    move-object v0, v0

    .line 553291
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 553292
    sget-object v0, LX/4hH;->VOIP:LX/4hH;

    goto :goto_0

    .line 553293
    :cond_4
    iget-object v0, p2, LX/4hL;->personalNumber_:LX/4hP;

    move-object v0, v0

    .line 553294
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 553295
    sget-object v0, LX/4hH;->PERSONAL_NUMBER:LX/4hH;

    goto :goto_0

    .line 553296
    :cond_5
    iget-object v0, p2, LX/4hL;->pager_:LX/4hP;

    move-object v0, v0

    .line 553297
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 553298
    sget-object v0, LX/4hH;->PAGER:LX/4hH;

    goto :goto_0

    .line 553299
    :cond_6
    iget-object v0, p2, LX/4hL;->uan_:LX/4hP;

    move-object v0, v0

    .line 553300
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 553301
    sget-object v0, LX/4hH;->UAN:LX/4hH;

    goto :goto_0

    .line 553302
    :cond_7
    iget-object v0, p2, LX/4hL;->voicemail_:LX/4hP;

    move-object v0, v0

    .line 553303
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 553304
    sget-object v0, LX/4hH;->VOICEMAIL:LX/4hH;

    goto :goto_0

    .line 553305
    :cond_8
    iget-object v0, p2, LX/4hL;->fixedLine_:LX/4hP;

    move-object v0, v0

    .line 553306
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    .line 553307
    if-eqz v0, :cond_b

    .line 553308
    iget-boolean v0, p2, LX/4hL;->sameMobileAndFixedLinePattern_:Z

    move v0, v0

    .line 553309
    if-eqz v0, :cond_9

    .line 553310
    sget-object v0, LX/4hH;->FIXED_LINE_OR_MOBILE:LX/4hH;

    goto :goto_0

    .line 553311
    :cond_9
    iget-object v0, p2, LX/4hL;->mobile_:LX/4hP;

    move-object v0, v0

    .line 553312
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 553313
    sget-object v0, LX/4hH;->FIXED_LINE_OR_MOBILE:LX/4hH;

    goto :goto_0

    .line 553314
    :cond_a
    sget-object v0, LX/4hH;->FIXED_LINE:LX/4hH;

    goto :goto_0

    .line 553315
    :cond_b
    iget-boolean v0, p2, LX/4hL;->sameMobileAndFixedLinePattern_:Z

    move v0, v0

    .line 553316
    if-nez v0, :cond_c

    .line 553317
    iget-object v0, p2, LX/4hL;->mobile_:LX/4hP;

    move-object v0, v0

    .line 553318
    invoke-virtual {p0, p1, v0}, LX/3Lz;->isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 553319
    sget-object v0, LX/4hH;->MOBILE:LX/4hH;

    goto/16 :goto_0

    .line 553320
    :cond_c
    sget-object v0, LX/4hH;->UNKNOWN:LX/4hH;

    goto/16 :goto_0
.end method

.method public static getPlusCharsPattern()Ljava/util/regex/Pattern;
    .locals 3

    .prologue
    .line 553321
    const-string v0, "getPlusCharsPattern()"

    sget-object v1, LX/3Lz;->LAZY_PLUS_CHARS_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "[+\uff0b]+"

    invoke-static {v0, v1, v2}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_PLUS_CHARS_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static isViablePhoneNumber(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 553322
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 553323
    const/4 v0, 0x0

    .line 553324
    :goto_0
    return v0

    .line 553325
    :cond_0
    const-string v0, "getValidPhoneNumberPattern()"

    sget-object v1, LX/3Lz;->LAZY_VALID_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\\p{Nd}]*(?:;ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[,x\uff58#\uff03~\uff5e]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*(\\p{Nd}{1,7})#?|[- ]+(\\p{Nd}{1,5})#)?"

    const/16 v3, 0x42

    invoke-static {v0, v1, v2, v3}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_VALID_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    move-object v0, v0

    .line 553326
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 553327
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method public static maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 1
    .param p1    # Ljava/util/regex/Pattern;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 552828
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method public static maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;I)Ljava/util/regex/Pattern;
    .locals 0
    .param p1    # Ljava/util/regex/Pattern;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 553328
    if-eqz p1, :cond_0

    .line 553329
    :goto_0
    return-object p1

    :cond_0
    invoke-static {p2, p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object p1

    goto :goto_0
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 553330
    const-string v0, "getValidAlphaPhonePattern()"

    sget-object v1, LX/3Lz;->LAZY_VALID_ALPHA_PHONE_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "(?:.*?[A-Za-z]){3}.*"

    invoke-static {v0, v1, v2}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_VALID_ALPHA_PHONE_PATTERN:Ljava/util/regex/Pattern;

    move-object v0, v0

    .line 553331
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 553332
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553333
    sget-object v0, LX/3Lz;->ALPHA_PHONE_MAPPINGS:LX/3M3;

    const/4 v1, 0x1

    const v6, 0xd800

    .line 553334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 553335
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 553336
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 553337
    invoke-static {v4}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v5

    invoke-interface {v0, v5, v6}, LX/3M3;->get(CC)C

    move-result v5

    .line 553338
    if-eq v5, v6, :cond_1

    .line 553339
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 553340
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 553341
    :cond_1
    if-nez v1, :cond_0

    .line 553342
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 553343
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 553344
    :goto_2
    return-object v0

    :cond_3
    invoke-static {p0}, LX/3Lz;->normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static normalize(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 553345
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Lz;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553346
    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 553347
    return-void
.end method

.method public static normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 553348
    const/4 v0, 0x0

    .line 553349
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 553350
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-char v5, v3, v1

    .line 553351
    const/16 v6, 0xa

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    .line 553352
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 553353
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 553354
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 553355
    :cond_1
    if-eqz v0, :cond_0

    .line 553356
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 553357
    :cond_2
    move-object v0, v2

    .line 553358
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseHelper(LX/3Lz;Ljava/lang/String;Ljava/lang/String;ZZLX/4hT;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 553359
    if-nez p1, :cond_0

    .line 553360
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->NOT_A_NUMBER:LX/4hD;

    const-string v2, "The phone number supplied was null."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553361
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xfa

    if-le v0, v1, :cond_1

    .line 553362
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->TOO_LONG:LX/4hD;

    const-string v2, "The string supplied was too long to parse."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553363
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 553364
    invoke-direct {p0, p1, v6}, LX/3Lz;->buildNationalNumberForParsing(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 553365
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Lz;->isViablePhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 553366
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->NOT_A_NUMBER:LX/4hD;

    const-string v2, "The string supplied did not seem to be a phone number."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553367
    :cond_2
    if-eqz p4, :cond_4

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553368
    iget-object v1, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v1, p2}, LX/3M9;->isValidRegionCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 553369
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, LX/3Lz;->getPlusCharsPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v1

    if-nez v1, :cond_13

    .line 553370
    :cond_3
    const/4 v1, 0x0

    .line 553371
    :goto_0
    move v0, v1

    .line 553372
    if-nez v0, :cond_4

    .line 553373
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    const-string v2, "Missing or invalid default region."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553374
    :cond_4
    if-eqz p3, :cond_5

    .line 553375
    invoke-virtual {p5, p1}, LX/4hT;->setRawInput(Ljava/lang/String;)LX/4hT;

    .line 553376
    :cond_5
    const-string v0, "getExtnPattern()"

    sget-object v1, LX/3Lz;->LAZY_EXTN_PATTERN:Ljava/util/regex/Pattern;

    const-string v2, "(?:;ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[,x\uff58#\uff03~\uff5e]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*(\\p{Nd}{1,7})#?|[- ]+(\\p{Nd}{1,5})#)$"

    const/16 v3, 0x42

    invoke-static {v0, v1, v2, v3}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Lz;->LAZY_EXTN_PATTERN:Ljava/util/regex/Pattern;

    move-object v0, v0

    .line 553377
    invoke-virtual {v0, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553378
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {v6, v0, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Lz;->isViablePhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 553379
    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    :goto_1
    if-gt v0, v2, :cond_15

    .line 553380
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 553381
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 553382
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 553383
    :goto_2
    move-object v0, v0

    .line 553384
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 553385
    invoke-virtual {p5, v0}, LX/4hT;->setExtension(Ljava/lang/String;)LX/4hT;

    .line 553386
    :cond_6
    invoke-virtual {p0, p2}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v2

    .line 553387
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 553388
    :try_start_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/3Lz;->maybeExtractCountryCode(Ljava/lang/String;LX/4hL;Ljava/lang/StringBuilder;ZLX/4hT;)I
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 553389
    :cond_7
    if-eqz v0, :cond_a

    .line 553390
    invoke-virtual {p0, v0}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v1

    .line 553391
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 553392
    invoke-static {p0, v0, v1}, LX/3Lz;->getMetadataForRegionOrCallingCode(LX/3Lz;ILjava/lang/String;)LX/4hL;

    move-result-object v2

    .line 553393
    :cond_8
    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-ge v0, v7, :cond_c

    .line 553394
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->TOO_SHORT_NSN:LX/4hD;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553395
    :catch_0
    move-exception v0

    .line 553396
    invoke-static {}, LX/3Lz;->getPlusCharsPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553397
    iget-object v4, v0, LX/4hE;->errorType:LX/4hD;

    move-object v4, v4

    .line 553398
    sget-object v5, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    if-ne v4, v5, :cond_9

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 553399
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/3Lz;->maybeExtractCountryCode(Ljava/lang/String;LX/4hL;Ljava/lang/StringBuilder;ZLX/4hT;)I

    move-result v0

    .line 553400
    if-nez v0, :cond_7

    .line 553401
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    const-string v2, "Could not interpret numbers after plus-sign."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553402
    :cond_9
    new-instance v1, LX/4hE;

    .line 553403
    iget-object v2, v0, LX/4hE;->errorType:LX/4hD;

    move-object v2, v2

    .line 553404
    invoke-virtual {v0}, LX/4hE;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v1

    .line 553405
    :cond_a
    invoke-static {v6}, LX/3Lz;->normalize(Ljava/lang/StringBuilder;)V

    .line 553406
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 553407
    if-eqz p2, :cond_b

    .line 553408
    iget v0, v2, LX/4hL;->countryCode_:I

    move v0, v0

    .line 553409
    invoke-virtual {p5, v0}, LX/4hT;->setCountryCode(I)LX/4hT;

    goto :goto_3

    .line 553410
    :cond_b
    if-eqz p3, :cond_8

    .line 553411
    const/4 v0, 0x0

    iput-boolean v0, p5, LX/4hT;->hasCountryCodeSource:Z

    .line 553412
    sget-object v0, LX/4hS;->FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;

    iput-object v0, p5, LX/4hT;->countryCodeSource_:LX/4hS;

    .line 553413
    goto :goto_3

    .line 553414
    :cond_c
    if-eqz v2, :cond_e

    .line 553415
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 553416
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 553417
    invoke-virtual {p0, v0, v2, v1}, LX/3Lz;->maybeStripNationalPrefixAndCarrierCode(Ljava/lang/StringBuilder;LX/4hL;Ljava/lang/StringBuilder;)Z

    .line 553418
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 553419
    iget-object v5, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553420
    iget-object v6, v2, LX/4hL;->generalDesc_:LX/4hP;

    move-object v6, v6

    .line 553421
    iget-object v2, v6, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    move-object v6, v2

    .line 553422
    invoke-virtual {v5, v6}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 553423
    invoke-static {p0, v5, v4}, LX/3Lz;->testNumberLengthAgainstPattern(LX/3Lz;Ljava/util/regex/Pattern;Ljava/lang/String;)LX/4hI;

    move-result-object v5

    sget-object v6, LX/4hI;->TOO_SHORT:LX/4hI;

    if-ne v5, v6, :cond_16

    const/4 v5, 0x1

    :goto_4
    move v2, v5

    .line 553424
    if-nez v2, :cond_e

    .line 553425
    if-eqz p3, :cond_d

    .line 553426
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v1}, LX/4hT;->setPreferredDomesticCarrierCode(Ljava/lang/String;)LX/4hT;

    :cond_d
    move-object v3, v0

    .line 553427
    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 553428
    if-ge v0, v7, :cond_f

    .line 553429
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->TOO_SHORT_NSN:LX/4hD;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553430
    :cond_f
    const/16 v1, 0x11

    if-le v0, v1, :cond_10

    .line 553431
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->TOO_LONG:LX/4hD;

    const-string v2, "The string supplied is too long to be a phone number."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553432
    :cond_10
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v5, 0x30

    const/4 v2, 0x1

    .line 553433
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_12

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_12

    .line 553434
    invoke-virtual {p5, v2}, LX/4hT;->setItalianLeadingZero(Z)LX/4hT;

    move v1, v2

    .line 553435
    :goto_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_11

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v5, :cond_11

    .line 553436
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 553437
    :cond_11
    if-eq v1, v2, :cond_12

    .line 553438
    invoke-virtual {p5, v1}, LX/4hT;->setNumberOfLeadingZeros(I)LX/4hT;

    .line 553439
    :cond_12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p5, v0, v1}, LX/4hT;->setNationalNumber(J)LX/4hT;

    .line 553440
    return-void

    :cond_13
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 553441
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 553442
    :cond_15
    const-string v0, ""

    goto/16 :goto_2

    :cond_16
    const/4 v5, 0x0

    goto :goto_4
.end method

.method public static prefixNumberWithCountryCallingCode(LX/3Lz;ILX/4hG;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    const/16 v3, 0x2b

    const/4 v2, 0x0

    .line 553162
    sget-object v0, LX/4hF;->$SwitchMap$com$facebook$phonenumbers$PhoneNumberUtil$PhoneNumberFormat:[I

    invoke-virtual {p2}, LX/4hG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 553163
    :goto_0
    return-void

    .line 553164
    :pswitch_0
    invoke-virtual {p3, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 553165
    :pswitch_1
    const-string v0, " "

    invoke-virtual {p3, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 553166
    :pswitch_2
    const-string v0, "-"

    invoke-virtual {p3, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tel:"

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static declared-synchronized setInstance(LX/3Lz;)V
    .locals 2

    .prologue
    .line 553443
    const-class v0, LX/3Lz;

    monitor-enter v0

    :try_start_0
    sput-object p0, LX/3Lz;->instance:LX/3Lz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553444
    monitor-exit v0

    return-void

    .line 553445
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static testNumberLengthAgainstPattern(LX/3Lz;Ljava/util/regex/Pattern;Ljava/lang/String;)LX/4hI;
    .locals 2

    .prologue
    .line 552963
    invoke-virtual {p1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 552964
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552965
    sget-object v0, LX/4hI;->IS_POSSIBLE:LX/4hI;

    .line 552966
    :goto_0
    return-object v0

    .line 552967
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552968
    sget-object v0, LX/4hI;->TOO_LONG:LX/4hI;

    goto :goto_0

    .line 552969
    :cond_1
    sget-object v0, LX/4hI;->TOO_SHORT:LX/4hI;

    goto :goto_0
.end method


# virtual methods
.method public extractCountryCode(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 552829
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x30

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 552830
    :goto_0
    return v0

    .line 552831
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 552832
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    const/4 v0, 0x3

    if-gt v2, v0, :cond_3

    if-gt v2, v3, :cond_3

    .line 552833
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 552834
    iget-object v4, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v4, v0}, LX/3M9;->isValidCountryCallingCode(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 552835
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 552836
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 552837
    goto :goto_0
.end method

.method public format(LX/4hT;LX/4hG;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 552838
    iget-wide v4, p1, LX/4hT;->nationalNumber_:J

    move-wide v0, v4

    .line 552839
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 552840
    iget-boolean v0, p1, LX/4hT;->hasRawInput:Z

    move v0, v0

    .line 552841
    if-eqz v0, :cond_0

    .line 552842
    iget-object v0, p1, LX/4hT;->rawInput_:Ljava/lang/String;

    move-object v0, v0

    .line 552843
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 552844
    :goto_0
    return-object v0

    .line 552845
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 552846
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 552847
    iget v1, p1, LX/4hT;->countryCode_:I

    move v1, v1

    .line 552848
    invoke-virtual {p0, p1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v2

    .line 552849
    sget-object v3, LX/4hG;->E164:LX/4hG;

    if-ne p2, v3, :cond_1

    .line 552850
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552851
    sget-object v2, LX/4hG;->E164:LX/4hG;

    invoke-static {p0, v1, v2, v0}, LX/3Lz;->prefixNumberWithCountryCallingCode(LX/3Lz;ILX/4hG;Ljava/lang/StringBuilder;)V

    .line 552852
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 552853
    :cond_1
    iget-object v3, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v3, v1}, LX/3M9;->isValidCountryCallingCode(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 552854
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 552855
    :cond_2
    invoke-virtual {p0, v1}, LX/3Lz;->getRegionCodeForCountryCode(I)Ljava/lang/String;

    move-result-object v3

    .line 552856
    invoke-static {p0, v1, v3}, LX/3Lz;->getMetadataForRegionOrCallingCode(LX/3Lz;ILjava/lang/String;)LX/4hL;

    move-result-object v3

    .line 552857
    invoke-static {p0, v2, v3, p2}, LX/3Lz;->formatNsn(LX/3Lz;Ljava/lang/String;LX/4hL;LX/4hG;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552858
    iget-boolean v2, p1, LX/4hT;->hasExtension:Z

    move v2, v2

    .line 552859
    if-eqz v2, :cond_3

    .line 552860
    iget-object v2, p1, LX/4hT;->extension_:Ljava/lang/String;

    move-object v2, v2

    .line 552861
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 552862
    sget-object v2, LX/4hG;->RFC3966:LX/4hG;

    if-ne p2, v2, :cond_4

    .line 552863
    const-string v2, ";ext="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552864
    iget-object v4, p1, LX/4hT;->extension_:Ljava/lang/String;

    move-object v4, v4

    .line 552865
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552866
    :cond_3
    :goto_2
    invoke-static {p0, v1, p2, v0}, LX/3Lz;->prefixNumberWithCountryCallingCode(LX/3Lz;ILX/4hG;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 552867
    :cond_4
    iget-boolean v2, v3, LX/4hL;->hasPreferredExtnPrefix:Z

    move v2, v2

    .line 552868
    if-eqz v2, :cond_5

    .line 552869
    iget-object v2, v3, LX/4hL;->preferredExtnPrefix_:Ljava/lang/String;

    move-object v2, v2

    .line 552870
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552871
    iget-object v4, p1, LX/4hT;->extension_:Ljava/lang/String;

    move-object v4, v4

    .line 552872
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 552873
    :cond_5
    const-string v2, " ext. "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552874
    iget-object v4, p1, LX/4hT;->extension_:Ljava/lang/String;

    move-object v4, v4

    .line 552875
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public getCountryCodeForRegion(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 552876
    iget-object v0, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v0, p1}, LX/3M9;->isValidRegionCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 552877
    sget-object v0, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid or missing region code ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string p1, "null"

    :cond_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") provided."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 552878
    const/4 v0, 0x0

    .line 552879
    :goto_0
    return v0

    :cond_1
    invoke-static {p0, p1}, LX/3Lz;->getCountryCodeForValidRegion(LX/3Lz;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getLengthOfGeographicalAreaCode(LX/4hT;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 552880
    invoke-virtual {p0, p1}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v1

    .line 552881
    if-nez v1, :cond_1

    .line 552882
    :cond_0
    :goto_0
    return v0

    .line 552883
    :cond_1
    iget-boolean v2, v1, LX/4hL;->hasNationalPrefix:Z

    move v1, v2

    .line 552884
    if-nez v1, :cond_2

    .line 552885
    iget-boolean v1, p1, LX/4hT;->italianLeadingZero_:Z

    move v1, v1

    .line 552886
    if-eqz v1, :cond_0

    .line 552887
    :cond_2
    invoke-virtual {p0, p1}, LX/3Lz;->getNumberType(LX/4hT;)LX/4hH;

    move-result-object v1

    .line 552888
    sget-object v2, LX/4hH;->FIXED_LINE:LX/4hH;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/4hH;->FIXED_LINE_OR_MOBILE:LX/4hH;

    if-ne v1, v2, :cond_c

    :cond_3
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 552889
    if-eqz v1, :cond_0

    .line 552890
    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 552891
    iget-boolean v2, p1, LX/4hT;->hasExtension:Z

    move v2, v2

    .line 552892
    if-eqz v2, :cond_d

    .line 552893
    new-instance v2, LX/4hT;

    invoke-direct {v2}, LX/4hT;-><init>()V

    .line 552894
    iget-boolean v7, p1, LX/4hT;->hasCountryCode:Z

    move v7, v7

    .line 552895
    if-eqz v7, :cond_4

    .line 552896
    iget v7, p1, LX/4hT;->countryCode_:I

    move v7, v7

    .line 552897
    invoke-virtual {v2, v7}, LX/4hT;->setCountryCode(I)LX/4hT;

    .line 552898
    :cond_4
    iget-boolean v7, p1, LX/4hT;->hasNationalNumber:Z

    move v7, v7

    .line 552899
    if-eqz v7, :cond_5

    .line 552900
    iget-wide v9, p1, LX/4hT;->nationalNumber_:J

    move-wide v7, v9

    .line 552901
    invoke-virtual {v2, v7, v8}, LX/4hT;->setNationalNumber(J)LX/4hT;

    .line 552902
    :cond_5
    iget-boolean v7, p1, LX/4hT;->hasExtension:Z

    move v7, v7

    .line 552903
    if-eqz v7, :cond_6

    .line 552904
    iget-object v7, p1, LX/4hT;->extension_:Ljava/lang/String;

    move-object v7, v7

    .line 552905
    invoke-virtual {v2, v7}, LX/4hT;->setExtension(Ljava/lang/String;)LX/4hT;

    .line 552906
    :cond_6
    iget-boolean v7, p1, LX/4hT;->hasItalianLeadingZero:Z

    move v7, v7

    .line 552907
    if-eqz v7, :cond_7

    .line 552908
    iget-boolean v7, p1, LX/4hT;->italianLeadingZero_:Z

    move v7, v7

    .line 552909
    invoke-virtual {v2, v7}, LX/4hT;->setItalianLeadingZero(Z)LX/4hT;

    .line 552910
    :cond_7
    iget-boolean v7, p1, LX/4hT;->hasNumberOfLeadingZeros:Z

    move v7, v7

    .line 552911
    if-eqz v7, :cond_8

    .line 552912
    iget v7, p1, LX/4hT;->numberOfLeadingZeros_:I

    move v7, v7

    .line 552913
    invoke-virtual {v2, v7}, LX/4hT;->setNumberOfLeadingZeros(I)LX/4hT;

    .line 552914
    :cond_8
    iget-boolean v7, p1, LX/4hT;->hasRawInput:Z

    move v7, v7

    .line 552915
    if-eqz v7, :cond_9

    .line 552916
    iget-object v7, p1, LX/4hT;->rawInput_:Ljava/lang/String;

    move-object v7, v7

    .line 552917
    invoke-virtual {v2, v7}, LX/4hT;->setRawInput(Ljava/lang/String;)LX/4hT;

    .line 552918
    :cond_9
    iget-boolean v7, p1, LX/4hT;->hasCountryCodeSource:Z

    move v7, v7

    .line 552919
    if-eqz v7, :cond_a

    .line 552920
    iget-object v7, p1, LX/4hT;->countryCodeSource_:LX/4hS;

    move-object v7, v7

    .line 552921
    invoke-virtual {v2, v7}, LX/4hT;->setCountryCodeSource(LX/4hS;)LX/4hT;

    .line 552922
    :cond_a
    iget-boolean v7, p1, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    move v7, v7

    .line 552923
    if-eqz v7, :cond_b

    .line 552924
    iget-object v7, p1, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    move-object v7, v7

    .line 552925
    invoke-virtual {v2, v7}, LX/4hT;->setPreferredDomesticCarrierCode(Ljava/lang/String;)LX/4hT;

    .line 552926
    :cond_b
    const/4 v3, 0x0

    iput-boolean v3, v2, LX/4hT;->hasExtension:Z

    .line 552927
    const-string v3, ""

    iput-object v3, v2, LX/4hT;->extension_:Ljava/lang/String;

    .line 552928
    :goto_2
    sget-object v3, LX/4hG;->INTERNATIONAL:LX/4hG;

    invoke-virtual {p0, v2, v3}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v2

    .line 552929
    const-string v3, "getNonDigitsPattern()"

    sget-object v4, LX/3Lz;->LAZY_NON_DIGITS_PATTERN:Ljava/util/regex/Pattern;

    const-string v7, "(\\D+)"

    invoke-static {v3, v4, v7}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    sput-object v3, LX/3Lz;->LAZY_NON_DIGITS_PATTERN:Ljava/util/regex/Pattern;

    move-object v3, v3

    .line 552930
    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    .line 552931
    array-length v3, v2

    if-gt v3, v6, :cond_e

    .line 552932
    const/4 v2, 0x0

    .line 552933
    :goto_3
    move v0, v2

    .line 552934
    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_d
    move-object v2, p1

    .line 552935
    goto :goto_2

    .line 552936
    :cond_e
    invoke-virtual {p0, p1}, LX/3Lz;->getNumberType(LX/4hT;)LX/4hH;

    move-result-object v3

    sget-object v4, LX/4hH;->MOBILE:LX/4hH;

    if-ne v3, v4, :cond_f

    .line 552937
    iget v3, p1, LX/4hT;->countryCode_:I

    move v3, v3

    .line 552938
    packed-switch v3, :pswitch_data_0

    .line 552939
    :pswitch_0
    const-string v4, ""

    :goto_4
    move-object v3, v4

    .line 552940
    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 552941
    aget-object v3, v2, v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    aget-object v2, v2, v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v3

    goto :goto_3

    .line 552942
    :cond_f
    aget-object v2, v2, v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_3

    .line 552943
    :pswitch_1
    const-string v4, "1"

    goto :goto_4

    .line 552944
    :pswitch_2
    const-string v4, "9"

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getMetadataForNonGeographicalRegion(I)LX/4hL;
    .locals 4

    .prologue
    .line 552945
    iget-object v1, p0, LX/3Lz;->countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;

    monitor-enter v1

    .line 552946
    :try_start_0
    iget-object v0, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v0, p1}, LX/3M9;->isValidCountryCallingCode(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552947
    const/4 v0, 0x0

    monitor-exit v1

    .line 552948
    :goto_0
    return-object v0

    .line 552949
    :cond_0
    iget-object v0, p0, LX/3Lz;->countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 552950
    iget-object v0, p0, LX/3Lz;->currentFilePrefix:Ljava/lang/String;

    const-string v2, "001"

    iget-object v3, p0, LX/3Lz;->metadataLoader:LX/3M1;

    invoke-virtual {p0, v0, v2, p1, v3}, LX/3Lz;->loadMetadataFromFile(Ljava/lang/String;Ljava/lang/String;ILX/3M1;)V

    .line 552951
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552952
    iget-object v0, p0, LX/3Lz;->countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hL;

    goto :goto_0

    .line 552953
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getMetadataForRegion(Ljava/lang/String;)LX/4hL;
    .locals 4

    .prologue
    .line 552954
    iget-object v0, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v0, p1}, LX/3M9;->isValidRegionCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552955
    const/4 v0, 0x0

    .line 552956
    :goto_0
    return-object v0

    .line 552957
    :cond_0
    iget-object v1, p0, LX/3Lz;->regionToMetadataMap:Ljava/util/Map;

    monitor-enter v1

    .line 552958
    :try_start_0
    iget-object v0, p0, LX/3Lz;->regionToMetadataMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 552959
    iget-object v0, p0, LX/3Lz;->currentFilePrefix:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/3Lz;->metadataLoader:LX/3M1;

    invoke-virtual {p0, v0, p1, v2, v3}, LX/3Lz;->loadMetadataFromFile(Ljava/lang/String;Ljava/lang/String;ILX/3M1;)V

    .line 552960
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552961
    iget-object v0, p0, LX/3Lz;->regionToMetadataMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hL;

    goto :goto_0

    .line 552962
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 552970
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 552971
    iget-boolean v1, p1, LX/4hT;->italianLeadingZero_:Z

    move v1, v1

    .line 552972
    if-eqz v1, :cond_0

    .line 552973
    iget v1, p1, LX/4hT;->numberOfLeadingZeros_:I

    move v1, v1

    .line 552974
    new-array v1, v1, [C

    .line 552975
    const/16 v2, 0x30

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 552976
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552977
    :cond_0
    iget-wide v4, p1, LX/4hT;->nationalNumber_:J

    move-wide v2, v4

    .line 552978
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 552979
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumberType(LX/4hT;)LX/4hH;
    .locals 2

    .prologue
    .line 552980
    invoke-virtual {p0, p1}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    .line 552981
    iget v1, p1, LX/4hT;->countryCode_:I

    move v1, v1

    .line 552982
    invoke-static {p0, v1, v0}, LX/3Lz;->getMetadataForRegionOrCallingCode(LX/3Lz;ILjava/lang/String;)LX/4hL;

    move-result-object v0

    .line 552983
    if-nez v0, :cond_0

    .line 552984
    sget-object v0, LX/4hH;->UNKNOWN:LX/4hH;

    .line 552985
    :goto_0
    return-object v0

    .line 552986
    :cond_0
    invoke-virtual {p0, p1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v1

    .line 552987
    invoke-static {p0, v1, v0}, LX/3Lz;->getNumberTypeHelper(LX/3Lz;Ljava/lang/String;LX/4hL;)LX/4hH;

    move-result-object v0

    goto :goto_0
.end method

.method public getRegionCodeForCountryCode(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 552988
    iget-object v0, p0, LX/3Lz;->data:LX/3M9;

    .line 552989
    iget-object v1, v0, LX/3M9;->mCountryCallingCodeToRegionCodeTable:LX/3MA;

    invoke-virtual {v1, p1}, LX/3MA;->firstIndexOfKey(I)I

    move-result v1

    .line 552990
    if-ltz v1, :cond_0

    iget-object p0, v0, LX/3M9;->mCountryCallingCodeToRegionCodeTable:LX/3MA;

    invoke-virtual {p0, v1}, LX/3MA;->valueAt(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 552991
    return-object v0

    :cond_0
    const-string v1, "ZZ"

    goto :goto_0
.end method

.method public getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 552992
    iget v0, p1, LX/4hT;->countryCode_:I

    move v0, v0

    .line 552993
    iget-object v1, p0, LX/3Lz;->data:LX/3M9;

    invoke-virtual {v1, v0}, LX/3M9;->getRegionCodesForCountryCallingCode(I)Ljava/util/List;

    move-result-object v1

    .line 552994
    if-nez v1, :cond_0

    .line 552995
    invoke-virtual {p0, p1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v1

    .line 552996
    sget-object v2, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing/invalid country_code ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") for number "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 552997
    const/4 v0, 0x0

    .line 552998
    :goto_0
    return-object v0

    .line 552999
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 553000
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 553001
    :cond_1
    invoke-virtual {p0, p1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v2

    .line 553002
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 553003
    invoke-virtual {p0, v0}, LX/3Lz;->getMetadataForRegion(Ljava/lang/String;)LX/4hL;

    move-result-object v4

    .line 553004
    iget-boolean v5, v4, LX/4hL;->hasLeadingDigits:Z

    move v5, v5

    .line 553005
    if-eqz v5, :cond_3

    .line 553006
    iget-object v5, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553007
    iget-object p1, v4, LX/4hL;->leadingDigits_:Ljava/lang/String;

    move-object v4, p1

    .line 553008
    invoke-virtual {v5, v4}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 553009
    :goto_1
    move-object v0, v0

    .line 553010
    goto :goto_0

    .line 553011
    :cond_3
    invoke-static {p0, v2, v4}, LX/3Lz;->getNumberTypeHelper(LX/3Lz;Ljava/lang/String;LX/4hL;)LX/4hH;

    move-result-object v4

    sget-object v5, LX/4hH;->UNKNOWN:LX/4hH;

    if-eq v4, v5, :cond_2

    goto :goto_1

    .line 553012
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isNumberMatchingDesc(Ljava/lang/String;LX/4hP;)Z
    .locals 3

    .prologue
    .line 553013
    iget-object v0, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553014
    iget-object v1, p2, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    move-object v1, v1

    .line 553015
    invoke-virtual {v0, v1}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 553016
    iget-object v1, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553017
    iget-object v2, p2, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    move-object v2, v2

    .line 553018
    invoke-virtual {v1, v2}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553019
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    move v1, v1

    .line 553020
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidNumber(LX/4hT;)Z
    .locals 5

    .prologue
    .line 553021
    invoke-virtual {p0, p1}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    .line 553022
    const/4 v1, 0x0

    .line 553023
    iget v2, p1, LX/4hT;->countryCode_:I

    move v2, v2

    .line 553024
    invoke-static {p0, v2, v0}, LX/3Lz;->getMetadataForRegionOrCallingCode(LX/3Lz;ILjava/lang/String;)LX/4hL;

    move-result-object v3

    .line 553025
    if-eqz v3, :cond_0

    const-string v4, "001"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p0, v0}, LX/3Lz;->getCountryCodeForValidRegion(LX/3Lz;Ljava/lang/String;)I

    move-result v4

    if-eq v2, v4, :cond_1

    .line 553026
    :cond_0
    :goto_0
    move v0, v1

    .line 553027
    return v0

    .line 553028
    :cond_1
    invoke-virtual {p0, p1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v2

    .line 553029
    invoke-static {p0, v2, v3}, LX/3Lz;->getNumberTypeHelper(LX/3Lz;Ljava/lang/String;LX/4hL;)LX/4hH;

    move-result-object v2

    sget-object v3, LX/4hH;->UNKNOWN:LX/4hH;

    if-eq v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public loadMetadataFromFile(Ljava/lang/String;Ljava/lang/String;ILX/3M1;)V
    .locals 7

    .prologue
    .line 553030
    const-string v0, "001"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 553031
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "libphone_data/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 553032
    invoke-interface {p4, v2}, LX/3M1;->loadMetadata(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 553033
    if-nez v0, :cond_1

    .line 553034
    sget-object v0, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "missing metadata: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 553035
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "missing metadata: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p2

    .line 553036
    goto :goto_0

    .line 553037
    :cond_1
    :try_start_0
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 553038
    new-instance v0, LX/4hN;

    invoke-direct {v0}, LX/4hN;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 553039
    :try_start_1
    invoke-virtual {v0, v3}, LX/4hN;->readExternal(Ljava/io/ObjectInput;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 553040
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 553041
    :goto_1
    :try_start_3
    move-object v0, v0

    .line 553042
    iget-object v3, v0, LX/4hN;->metadata_:Ljava/util/List;

    move-object v0, v3

    .line 553043
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 553044
    sget-object v0, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "empty metadata: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 553045
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "empty metadata: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 553046
    :catch_0
    move-exception v0

    .line 553047
    sget-object v1, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cannot load/parse metadata: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 553048
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot load/parse metadata: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 553049
    :cond_2
    :try_start_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    .line 553050
    sget-object v3, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid metadata (too many entries): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 553051
    :cond_3
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hL;

    .line 553052
    if-eqz v1, :cond_4

    .line 553053
    iget-object v1, p0, LX/3Lz;->countryCodeToNonGeographicalMetadataMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553054
    :goto_2
    return-void

    .line 553055
    :cond_4
    iget-object v1, p0, LX/3Lz;->regionToMetadataMap:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 553056
    :catch_1
    :try_start_5
    move-exception v4
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 553057
    :try_start_6
    sget-object v5, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p1, "error closing input stream (ignored)"

    invoke-virtual {v5, v6, p1, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    goto/16 :goto_1

    .line 553058
    :catchall_0
    goto/16 :goto_1

    .line 553059
    :catch_2
    move-exception v4
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 553060
    :try_start_8
    sget-object v5, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p1, "error reading input (ignored)"

    invoke-virtual {v5, v6, p1, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 553061
    :try_start_9
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    goto/16 :goto_1

    .line 553062
    :catch_3
    move-exception v4
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    .line 553063
    :try_start_b
    sget-object v5, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p1, "error closing input stream (ignored)"

    invoke-virtual {v5, v6, p1, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    goto/16 :goto_1

    .line 553064
    :catchall_1
    goto/16 :goto_1
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0

    .line 553065
    :catchall_2
    :try_start_d
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    goto/16 :goto_1

    .line 553066
    :catch_4
    move-exception v4
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0

    .line 553067
    :try_start_f
    sget-object v5, LX/3Lz;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string p1, "error closing input stream (ignored)"

    invoke-virtual {v5, v6, p1, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto/16 :goto_1

    .line 553068
    :catchall_3
    goto/16 :goto_1
.end method

.method public maybeExtractCountryCode(Ljava/lang/String;LX/4hL;Ljava/lang/StringBuilder;ZLX/4hT;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 553069
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 553070
    :goto_0
    return v0

    .line 553071
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 553072
    const-string v0, "NonMatch"

    .line 553073
    if-eqz p2, :cond_1

    .line 553074
    iget-object v0, p2, LX/4hL;->internationalPrefix_:Ljava/lang/String;

    move-object v0, v0

    .line 553075
    :cond_1
    invoke-virtual {p0, v2, v0}, LX/3Lz;->maybeStripInternationalPrefixAndNormalize(Ljava/lang/StringBuilder;Ljava/lang/String;)LX/4hS;

    move-result-object v0

    .line 553076
    if-eqz p4, :cond_2

    .line 553077
    invoke-virtual {p5, v0}, LX/4hT;->setCountryCodeSource(LX/4hS;)LX/4hT;

    .line 553078
    :cond_2
    sget-object v3, LX/4hS;->FROM_DEFAULT_COUNTRY:LX/4hS;

    if-eq v0, v3, :cond_5

    .line 553079
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_3

    .line 553080
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->TOO_SHORT_AFTER_IDD:LX/4hD;

    const-string v2, "Phone number had an IDD, but after this was not long enough to be a viable phone number."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553081
    :cond_3
    invoke-virtual {p0, v2, p3}, LX/3Lz;->extractCountryCode(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I

    move-result v0

    .line 553082
    if-eqz v0, :cond_4

    .line 553083
    invoke-virtual {p5, v0}, LX/4hT;->setCountryCode(I)LX/4hT;

    goto :goto_0

    .line 553084
    :cond_4
    new-instance v0, LX/4hE;

    sget-object v1, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    const-string v2, "Country calling code supplied was not recognised."

    invoke-direct {v0, v1, v2}, LX/4hE;-><init>(LX/4hD;Ljava/lang/String;)V

    throw v0

    .line 553085
    :cond_5
    if-eqz p2, :cond_9

    .line 553086
    iget v0, p2, LX/4hL;->countryCode_:I

    move v0, v0

    .line 553087
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 553088
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 553089
    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 553090
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 553091
    iget-object v3, p2, LX/4hL;->generalDesc_:LX/4hP;

    move-object v3, v3

    .line 553092
    iget-object v4, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553093
    iget-object v6, v3, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    move-object v6, v6

    .line 553094
    invoke-virtual {v4, v6}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 553095
    const/4 v6, 0x0

    invoke-virtual {p0, v5, p2, v6}, LX/3Lz;->maybeStripNationalPrefixAndCarrierCode(Ljava/lang/StringBuilder;LX/4hL;Ljava/lang/StringBuilder;)Z

    .line 553096
    iget-object v6, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553097
    iget-object p1, v3, LX/4hP;->possibleNumberPattern_:Ljava/lang/String;

    move-object v3, p1

    .line 553098
    invoke-virtual {v6, v3}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 553099
    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v3, v2}, LX/3Lz;->testNumberLengthAgainstPattern(LX/3Lz;Ljava/util/regex/Pattern;Ljava/lang/String;)LX/4hI;

    move-result-object v2

    sget-object v3, LX/4hI;->TOO_LONG:LX/4hI;

    if-ne v2, v3, :cond_9

    .line 553100
    :cond_7
    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 553101
    if-eqz p4, :cond_8

    .line 553102
    sget-object v1, LX/4hS;->FROM_NUMBER_WITHOUT_PLUS_SIGN:LX/4hS;

    invoke-virtual {p5, v1}, LX/4hT;->setCountryCodeSource(LX/4hS;)LX/4hT;

    .line 553103
    :cond_8
    invoke-virtual {p5, v0}, LX/4hT;->setCountryCode(I)LX/4hT;

    goto/16 :goto_0

    .line 553104
    :cond_9
    invoke-virtual {p5, v1}, LX/4hT;->setCountryCode(I)LX/4hT;

    move v0, v1

    .line 553105
    goto/16 :goto_0
.end method

.method public maybeStripInternationalPrefixAndNormalize(Ljava/lang/StringBuilder;Ljava/lang/String;)LX/4hS;
    .locals 5

    .prologue
    .line 553106
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 553107
    sget-object v0, LX/4hS;->FROM_DEFAULT_COUNTRY:LX/4hS;

    .line 553108
    :goto_0
    return-object v0

    .line 553109
    :cond_0
    invoke-static {}, LX/3Lz;->getPlusCharsPattern()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 553110
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 553111
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 553112
    invoke-static {p1}, LX/3Lz;->normalize(Ljava/lang/StringBuilder;)V

    .line 553113
    sget-object v0, LX/4hS;->FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;

    goto :goto_0

    .line 553114
    :cond_1
    iget-object v0, p0, LX/3Lz;->regexCache:LX/3MC;

    invoke-virtual {v0, p2}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 553115
    invoke-static {p1}, LX/3Lz;->normalize(Ljava/lang/StringBuilder;)V

    .line 553116
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 553117
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 553118
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 553119
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    .line 553120
    const-string v4, "getCapturingDigitPattern()"

    sget-object p2, LX/3Lz;->LAZY_CAPTURING_DIGIT_PATTERN:Ljava/util/regex/Pattern;

    const-string p0, "(\\p{Nd})"

    invoke-static {v4, p2, p0}, LX/3Lz;->maybeCompilePattern(Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    sput-object v4, LX/3Lz;->LAZY_CAPTURING_DIGIT_PATTERN:Ljava/util/regex/Pattern;

    move-object v4, v4

    .line 553121
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 553122
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 553123
    invoke-virtual {v4, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/3Lz;->normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 553124
    const-string p2, "0"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 553125
    :cond_2
    :goto_1
    move v0, v1

    .line 553126
    if-eqz v0, :cond_3

    sget-object v0, LX/4hS;->FROM_NUMBER_WITH_IDD:LX/4hS;

    goto :goto_0

    :cond_3
    sget-object v0, LX/4hS;->FROM_DEFAULT_COUNTRY:LX/4hS;

    goto :goto_0

    .line 553127
    :cond_4
    invoke-virtual {p1, v1, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v1, v2

    .line 553128
    goto :goto_1
.end method

.method public maybeStripNationalPrefixAndCarrierCode(Ljava/lang/StringBuilder;LX/4hL;Ljava/lang/StringBuilder;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 553129
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 553130
    iget-object v3, p2, LX/4hL;->nationalPrefixForParsing_:Ljava/lang/String;

    move-object v3, v3

    .line 553131
    if-eqz v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 553132
    :cond_0
    :goto_0
    return v0

    .line 553133
    :cond_1
    iget-object v4, p0, LX/3Lz;->regexCache:LX/3MC;

    invoke-virtual {v4, v3}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 553134
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 553135
    iget-object v4, p0, LX/3Lz;->regexCache:LX/3MC;

    .line 553136
    iget-object v5, p2, LX/4hL;->generalDesc_:LX/4hP;

    move-object v5, v5

    .line 553137
    iget-object v6, v5, LX/4hP;->nationalNumberPattern_:Ljava/lang/String;

    move-object v5, v6

    .line 553138
    invoke-virtual {v4, v5}, LX/3MC;->getPatternForRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 553139
    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    .line 553140
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    .line 553141
    iget-object v7, p2, LX/4hL;->nationalPrefixTransformRule_:Ljava/lang/String;

    move-object v7, v7

    .line 553142
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    .line 553143
    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553144
    :cond_3
    if-eqz p3, :cond_4

    if-lez v6, :cond_4

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 553145
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553146
    :cond_4
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v0, v1

    .line 553147
    goto :goto_0

    .line 553148
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 553149
    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v0, v2, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 553150
    if-eqz v5, :cond_6

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553151
    :cond_6
    if-eqz p3, :cond_7

    if-le v6, v1, :cond_7

    .line 553152
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553153
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 553154
    goto/16 :goto_0
.end method

.method public parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;
    .locals 7

    .prologue
    .line 553155
    new-instance v0, LX/4hT;

    invoke-direct {v0}, LX/4hT;-><init>()V

    .line 553156
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/3Lz;->parseHelper(LX/3Lz;Ljava/lang/String;Ljava/lang/String;ZZLX/4hT;)V

    .line 553157
    return-object v0
.end method

.method public parseAndKeepRawInput(Ljava/lang/String;Ljava/lang/String;)LX/4hT;
    .locals 7

    .prologue
    .line 553158
    new-instance v0, LX/4hT;

    invoke-direct {v0}, LX/4hT;-><init>()V

    .line 553159
    const/4 v4, 0x1

    .line 553160
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, v4

    move-object v6, v0

    invoke-static/range {v1 .. v6}, LX/3Lz;->parseHelper(LX/3Lz;Ljava/lang/String;Ljava/lang/String;ZZLX/4hT;)V

    .line 553161
    return-object v0
.end method
