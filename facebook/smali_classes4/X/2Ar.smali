.class public final LX/2Ar;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:LX/2Ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/2Ar",
            "<TT;>;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 378272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378273
    iput-object p1, p0, LX/2Ar;->a:Ljava/lang/Object;

    .line 378274
    iput-object p2, p0, LX/2Ar;->b:LX/2Ar;

    .line 378275
    if-nez p3, :cond_0

    move-object p3, v0

    move-object v0, p0

    .line 378276
    :goto_0
    iput-object p3, v0, LX/2Ar;->c:Ljava/lang/String;

    .line 378277
    iput-boolean p4, p0, LX/2Ar;->d:Z

    .line 378278
    iput-boolean p5, p0, LX/2Ar;->e:Z

    .line 378279
    return-void

    .line 378280
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    move-object p3, v0

    move-object v0, p0

    goto :goto_0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method private a(LX/2Ar;)LX/2Ar;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378270
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-ne p1, v0, :cond_0

    .line 378271
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/2Ar;

    iget-object v1, p0, LX/2Ar;->a:Ljava/lang/Object;

    iget-object v3, p0, LX/2Ar;->c:Ljava/lang/String;

    iget-boolean v4, p0, LX/2Ar;->d:Z

    iget-boolean v5, p0, LX/2Ar;->e:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static b(LX/2Ar;LX/2Ar;)LX/2Ar;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ar",
            "<TT;>;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378267
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-nez v0, :cond_0

    .line 378268
    invoke-direct {p0, p1}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    .line 378269
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    invoke-static {v0, p1}, LX/2Ar;->b(LX/2Ar;LX/2Ar;)LX/2Ar;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/2Ar;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378281
    iget-boolean v0, p0, LX/2Ar;->e:Z

    if-eqz v0, :cond_2

    .line 378282
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-nez v0, :cond_1

    const/4 p0, 0x0

    .line 378283
    :cond_0
    :goto_0
    return-object p0

    .line 378284
    :cond_1
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    invoke-virtual {v0}, LX/2Ar;->a()LX/2Ar;

    move-result-object p0

    goto :goto_0

    .line 378285
    :cond_2
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-eqz v0, :cond_0

    .line 378286
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    invoke-virtual {v0}, LX/2Ar;->a()LX/2Ar;

    move-result-object v0

    .line 378287
    iget-object v1, p0, LX/2Ar;->b:LX/2Ar;

    if-eq v0, v1, :cond_0

    .line 378288
    invoke-direct {p0, v0}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)LX/2Ar;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378265
    iget-object v0, p0, LX/2Ar;->a:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 378266
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/2Ar;

    iget-object v2, p0, LX/2Ar;->b:LX/2Ar;

    iget-object v3, p0, LX/2Ar;->c:Ljava/lang/String;

    iget-boolean v4, p0, LX/2Ar;->d:Z

    iget-boolean v5, p0, LX/2Ar;->e:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/2Ar;-><init>(Ljava/lang/Object;LX/2Ar;Ljava/lang/String;ZZ)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final b()LX/2Ar;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 378246
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 378247
    :goto_0
    iget-boolean v1, p0, LX/2Ar;->d:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object v0

    :cond_0
    return-object v0

    .line 378248
    :cond_1
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    invoke-virtual {v0}, LX/2Ar;->b()LX/2Ar;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()LX/2Ar;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Ar",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 378253
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    if-nez v0, :cond_0

    .line 378254
    :goto_0
    return-object p0

    .line 378255
    :cond_0
    iget-object v0, p0, LX/2Ar;->b:LX/2Ar;

    invoke-virtual {v0}, LX/2Ar;->c()LX/2Ar;

    move-result-object v0

    .line 378256
    iget-object v1, p0, LX/2Ar;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 378257
    iget-object v1, v0, LX/2Ar;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 378258
    invoke-direct {p0, v3}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object p0

    goto :goto_0

    .line 378259
    :cond_1
    invoke-direct {p0, v0}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object p0

    goto :goto_0

    .line 378260
    :cond_2
    iget-object v1, v0, LX/2Ar;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    move-object p0, v0

    .line 378261
    goto :goto_0

    .line 378262
    :cond_3
    iget-boolean v1, p0, LX/2Ar;->d:Z

    iget-boolean v2, v0, LX/2Ar;->d:Z

    if-ne v1, v2, :cond_4

    .line 378263
    invoke-direct {p0, v0}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object p0

    goto :goto_0

    .line 378264
    :cond_4
    iget-boolean v1, p0, LX/2Ar;->d:Z

    if-eqz v1, :cond_5

    invoke-direct {p0, v3}, LX/2Ar;->a(LX/2Ar;)LX/2Ar;

    move-result-object p0

    goto :goto_0

    :cond_5
    move-object p0, v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 378249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/2Ar;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[visible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/2Ar;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378250
    iget-object v1, p0, LX/2Ar;->b:LX/2Ar;

    if-eqz v1, :cond_0

    .line 378251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Ar;->b:LX/2Ar;

    invoke-virtual {v1}, LX/2Ar;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378252
    :cond_0
    return-object v0
.end method
