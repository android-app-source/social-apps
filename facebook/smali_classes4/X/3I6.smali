.class public LX/3I6;
.super LX/2oy;
.source ""


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 545569
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3I6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545570
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545571
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3I6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545572
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 545573
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545574
    const v0, 0x7f031613

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 545575
    const v0, 0x7f0d31a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3I6;->a:Landroid/view/View;

    .line 545576
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 545577
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "WATCH_AND_SHOP_PLUGIN_VIDEO_PARAMS_KEY"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 545578
    const/4 v0, 0x0

    .line 545579
    :goto_0
    move v0, v0

    .line 545580
    if-eqz v0, :cond_0

    .line 545581
    iget-object v0, p0, LX/3I6;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545582
    :goto_1
    return-void

    .line 545583
    :cond_0
    iget-object v0, p0, LX/3I6;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "WATCH_AND_SHOP_PLUGIN_VIDEO_PARAMS_KEY"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
