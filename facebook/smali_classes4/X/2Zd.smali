.class public LX/2Zd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 422609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 422610
    const/16 v16, 0x0

    .line 422611
    const/4 v15, 0x0

    .line 422612
    const/4 v14, 0x0

    .line 422613
    const/4 v13, 0x0

    .line 422614
    const/4 v12, 0x0

    .line 422615
    const/4 v11, 0x0

    .line 422616
    const/4 v10, 0x0

    .line 422617
    const/4 v9, 0x0

    .line 422618
    const/4 v8, 0x0

    .line 422619
    const/4 v7, 0x0

    .line 422620
    const/4 v6, 0x0

    .line 422621
    const/4 v5, 0x0

    .line 422622
    const/4 v4, 0x0

    .line 422623
    const/4 v3, 0x0

    .line 422624
    const/4 v2, 0x0

    .line 422625
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 422626
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 422627
    const/4 v2, 0x0

    .line 422628
    :goto_0
    return v2

    .line 422629
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 422630
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_d

    .line 422631
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v17

    .line 422632
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 422633
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    if-eqz v17, :cond_1

    .line 422634
    const-string v18, "already_completed"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 422635
    const/4 v4, 0x1

    .line 422636
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 422637
    :cond_2
    const-string v18, "completed_subtitle"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 422638
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 422639
    :cond_3
    const-string v18, "completed_title"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 422640
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 422641
    :cond_4
    const-string v18, "id"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 422642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 422643
    :cond_5
    const-string v18, "negative_feedback_action_type"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 422644
    const/4 v3, 0x1

    .line 422645
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v12

    goto :goto_1

    .line 422646
    :cond_6
    const-string v18, "subtitle"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 422647
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 422648
    :cond_7
    const-string v18, "target_entity"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 422649
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 422650
    :cond_8
    const-string v18, "target_entity_type"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 422651
    const/4 v2, 0x1

    .line 422652
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v9

    goto/16 :goto_1

    .line 422653
    :cond_9
    const-string v18, "title"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 422654
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 422655
    :cond_a
    const-string v18, "titleForSummary"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 422656
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 422657
    :cond_b
    const-string v18, "url"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 422658
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 422659
    :cond_c
    const-string v18, "confirmation_button_label"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 422660
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 422661
    :cond_d
    const/16 v17, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 422662
    if-eqz v4, :cond_e

    .line 422663
    const/4 v4, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 422664
    :cond_e
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 422665
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 422666
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 422667
    if-eqz v3, :cond_f

    .line 422668
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 422669
    :cond_f
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 422670
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 422671
    if-eqz v2, :cond_10

    .line 422672
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 422673
    :cond_10
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 422674
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 422675
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 422676
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 422677
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 422678
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 422679
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 422680
    if-eqz v0, :cond_0

    .line 422681
    const-string v1, "already_completed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422682
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 422683
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422684
    if-eqz v0, :cond_1

    .line 422685
    const-string v1, "completed_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422686
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422687
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422688
    if-eqz v0, :cond_2

    .line 422689
    const-string v1, "completed_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422690
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422691
    :cond_2
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 422692
    if-eqz v0, :cond_3

    .line 422693
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422694
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 422695
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 422696
    if-eqz v0, :cond_4

    .line 422697
    const-string v0, "negative_feedback_action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422698
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 422699
    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422700
    if-eqz v0, :cond_5

    .line 422701
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422702
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422703
    :cond_5
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422704
    if-eqz v0, :cond_6

    .line 422705
    const-string v1, "target_entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422706
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422707
    :cond_6
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 422708
    if-eqz v0, :cond_7

    .line 422709
    const-string v0, "target_entity_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422710
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 422711
    :cond_7
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422712
    if-eqz v0, :cond_8

    .line 422713
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422714
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422715
    :cond_8
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422716
    if-eqz v0, :cond_9

    .line 422717
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422718
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422719
    :cond_9
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 422720
    if-eqz v0, :cond_a

    .line 422721
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422722
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 422723
    :cond_a
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 422724
    if-eqz v0, :cond_b

    .line 422725
    const-string v1, "confirmation_button_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 422726
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 422727
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 422728
    return-void
.end method
