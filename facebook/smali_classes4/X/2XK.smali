.class public final LX/2XK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2Ge;

.field public final synthetic c:LX/2H7;

.field public final synthetic d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;


# direct methods
.method public constructor <init>(Lcom/facebook/push/registration/FacebookPushServerRegistrar;Ljava/lang/String;LX/2Ge;LX/2H7;)V
    .locals 0

    .prologue
    .line 419768
    iput-object p1, p0, LX/2XK;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iput-object p2, p0, LX/2XK;->a:Ljava/lang/String;

    iput-object p3, p0, LX/2XK;->b:LX/2Ge;

    iput-object p4, p0, LX/2XK;->c:LX/2H7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 419769
    sget-object v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    const-string v1, "RegisterPushToken %s failed %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2XK;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 419770
    iget-object v0, p0, LX/2XK;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v1, p0, LX/2XK;->b:LX/2Ge;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/Throwable;)V

    .line 419771
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 419772
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 419773
    iget-object v0, p0, LX/2XK;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v1, p0, LX/2XK;->b:LX/2Ge;

    iget-object v2, p0, LX/2XK;->c:LX/2H7;

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Lcom/facebook/fbservice/service/OperationResult;LX/2H7;)V

    .line 419774
    return-void
.end method
