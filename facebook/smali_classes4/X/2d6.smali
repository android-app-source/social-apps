.class public LX/2d6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2d7;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2d6;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2d7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442942
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2d6;->a:Ljava/util/Set;

    .line 442943
    iput-object p1, p0, LX/2d6;->b:Ljava/util/concurrent/Executor;

    .line 442944
    return-void
.end method

.method public static a(LX/0QB;)LX/2d6;
    .locals 4

    .prologue
    .line 442928
    sget-object v0, LX/2d6;->c:LX/2d6;

    if-nez v0, :cond_1

    .line 442929
    const-class v1, LX/2d6;

    monitor-enter v1

    .line 442930
    :try_start_0
    sget-object v0, LX/2d6;->c:LX/2d6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 442931
    if-eqz v2, :cond_0

    .line 442932
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 442933
    new-instance p0, LX/2d6;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3}, LX/2d6;-><init>(Ljava/util/concurrent/Executor;)V

    .line 442934
    move-object v0, p0

    .line 442935
    sput-object v0, LX/2d6;->c:LX/2d6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442936
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 442937
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 442938
    :cond_1
    sget-object v0, LX/2d6;->c:LX/2d6;

    return-object v0

    .line 442939
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 442940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/2d6;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 4
    .param p0    # LX/2d6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442945
    iget-object v1, p0, LX/2d6;->a:Ljava/util/Set;

    monitor-enter v1

    .line 442946
    :try_start_0
    iget-object v0, p0, LX/2d6;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 442947
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442948
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 442949
    check-cast v0, LX/2d7;

    invoke-interface {v0, p1, p2}, LX/2d7;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    .line 442950
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 442951
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 442952
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442926
    iget-object v0, p0, LX/2d6;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/placetips/settings/PlaceTipsRuntimeSettingsManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/placetips/settings/PlaceTipsRuntimeSettingsManager$1;-><init>(LX/2d6;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    const v2, 0x2611a680

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 442927
    return-void
.end method

.method public final a(LX/2d7;)Z
    .locals 2

    .prologue
    .line 442918
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442919
    iget-object v1, p0, LX/2d6;->a:Ljava/util/Set;

    monitor-enter v1

    .line 442920
    :try_start_0
    iget-object v0, p0, LX/2d6;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 442921
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/2d7;)Z
    .locals 2

    .prologue
    .line 442922
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442923
    iget-object v1, p0, LX/2d6;->a:Ljava/util/Set;

    monitor-enter v1

    .line 442924
    :try_start_0
    iget-object v0, p0, LX/2d6;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 442925
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
