.class public LX/2kj;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/2kk;
.implements LX/2kl;
.implements LX/2km;
.implements LX/2kn;
.implements LX/2ko;
.implements LX/2kp;


# instance fields
.field private final A:LX/2ks;

.field private final n:LX/1Kf;

.field private final o:LX/1dy;

.field private final p:Landroid/content/Context;

.field private final q:LX/1PT;

.field private final r:LX/1SX;

.field private final s:Landroid/support/v4/app/Fragment;

.field private final t:LX/2kt;

.field private final u:LX/2ku;

.field private final v:LX/2jZ;

.field private final w:LX/2iv;

.field private final x:Lcom/facebook/content/SecureContextHelper;

.field private final y:LX/1ro;

.field private final z:LX/2jY;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;Landroid/support/v4/app/Fragment;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/2jZ;LX/2jY;LX/1PY;LX/2iv;LX/1Kf;LX/1dy;LX/2kq;LX/2kr;Lcom/facebook/content/SecureContextHelper;LX/1ro;LX/2ks;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/2jZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/2iv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456541
    invoke-direct {p0, p1, p5, p9}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 456542
    iput-object p11, p0, LX/2kj;->n:LX/1Kf;

    .line 456543
    iput-object p12, p0, LX/2kj;->o:LX/1dy;

    .line 456544
    iput-object p1, p0, LX/2kj;->p:Landroid/content/Context;

    .line 456545
    iput-object p2, p0, LX/2kj;->q:LX/1PT;

    .line 456546
    iput-object p4, p0, LX/2kj;->r:LX/1SX;

    .line 456547
    iput-object p3, p0, LX/2kj;->s:Landroid/support/v4/app/Fragment;

    .line 456548
    invoke-static {p6}, LX/2kq;->a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/2kt;

    move-result-object v1

    iput-object v1, p0, LX/2kj;->t:LX/2kt;

    .line 456549
    invoke-static {p1}, LX/2kr;->a(Landroid/content/Context;)LX/2ku;

    move-result-object v1

    iput-object v1, p0, LX/2kj;->u:LX/2ku;

    .line 456550
    iput-object p7, p0, LX/2kj;->v:LX/2jZ;

    .line 456551
    iput-object p10, p0, LX/2kj;->w:LX/2iv;

    .line 456552
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2kj;->x:Lcom/facebook/content/SecureContextHelper;

    .line 456553
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2kj;->y:LX/1ro;

    .line 456554
    iput-object p8, p0, LX/2kj;->z:LX/2jY;

    .line 456555
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2kj;->A:LX/2ks;

    .line 456556
    return-void
.end method

.method private a(LX/Cfl;)V
    .locals 5
    .param p1    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456535
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "notification_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456536
    iget-object v0, p0, LX/2kj;->y:LX/1ro;

    iget-object v1, p1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v2, "notification_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ro;->a(Ljava/lang/String;)V

    .line 456537
    :cond_0
    iget-object v0, p1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "composer_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456538
    iget-object v1, p0, LX/2kj;->n:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "composer_configuration"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, LX/Cfl;->a:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->ordinal()I

    move-result v3

    iget-object v4, p0, LX/2kj;->s:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 456539
    :cond_1
    iget-object v0, p0, LX/2kj;->x:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p1, LX/Cfl;->d:Landroid/content/Intent;

    iget-object v2, p0, LX/2kj;->p:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 456540
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 456534
    iget-object v0, p0, LX/2kj;->u:LX/2ku;

    invoke-virtual {v0, p1}, LX/2ku;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2nq;)V
    .locals 2

    .prologue
    .line 456531
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 456532
    iget-object v0, p0, LX/2kj;->y:LX/1ro;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ro;->a(Ljava/lang/String;)V

    .line 456533
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V
    .locals 0

    .prologue
    .line 456530
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456527
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2kj;->a(LX/2nq;)V

    .line 456528
    iget-object v0, p0, LX/2kj;->v:LX/2jZ;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/2jZ;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 456529
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456524
    iget-object v0, p0, LX/2kj;->v:LX/2jZ;

    invoke-virtual {v0, p1, p2, p3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 456525
    invoke-direct {p0, p3}, LX/2kj;->a(LX/Cfl;)V

    .line 456526
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456521
    iget-object v0, p0, LX/2kj;->v:LX/2jZ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 456522
    invoke-direct {p0, p4}, LX/2kj;->a(LX/Cfl;)V

    .line 456523
    return-void
.end method

.method public final a(LX/2nq;Z)Z
    .locals 8

    .prologue
    .line 456557
    new-instance v0, LX/3D5;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1Qj;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3D6;

    .line 456558
    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 456559
    iput-object v1, v0, LX/3D6;->a:LX/03R;

    .line 456560
    new-instance v0, LX/BDE;

    invoke-direct {v0}, LX/BDE;-><init>()V

    invoke-interface {p1}, LX/2nq;->hZ_()Ljava/lang/String;

    move-result-object v1

    .line 456561
    iput-object v1, v0, LX/BDE;->a:Ljava/lang/String;

    .line 456562
    move-object v0, v0

    .line 456563
    iput-boolean p2, v0, LX/BDE;->b:Z

    .line 456564
    move-object v0, v0

    .line 456565
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 456566
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 456567
    iget-object v3, v0, LX/BDE;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 456568
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 456569
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 456570
    iget-boolean v3, v0, LX/BDE;->b:Z

    invoke-virtual {v2, v6, v3}, LX/186;->a(IZ)V

    .line 456571
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 456572
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 456573
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 456574
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 456575
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 456576
    new-instance v3, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;-><init>(LX/15i;)V

    .line 456577
    move-object v0, v3

    .line 456578
    iget-object v1, p0, LX/2kj;->o:LX/1dy;

    invoke-virtual {v1, v0}, LX/1dy;->a(LX/0jT;)V

    .line 456579
    const/4 v0, 0x1

    return v0
.end method

.method public final b(LX/2nq;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 456520
    iget-object v0, p0, LX/2kj;->A:LX/2ks;

    invoke-virtual {v0, p1, p0}, LX/2ks;->a(LX/2nq;Ljava/lang/Object;)LX/3D9;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 456519
    iget-object v0, p0, LX/2kj;->q:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 456518
    iget-object v0, p0, LX/2kj;->r:LX/1SX;

    return-object v0
.end method

.method public final e_(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 456517
    iget-object v0, p0, LX/2kj;->w:LX/2iv;

    invoke-virtual {v0, p1}, LX/2iv;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final md_()LX/2jc;
    .locals 1

    .prologue
    .line 456516
    iget-object v0, p0, LX/2kj;->v:LX/2jZ;

    return-object v0
.end method

.method public final t()LX/2jY;
    .locals 1

    .prologue
    .line 456515
    iget-object v0, p0, LX/2kj;->z:LX/2jY;

    return-object v0
.end method

.method public final v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 456514
    iget-object v0, p0, LX/2kj;->t:LX/2kt;

    invoke-virtual {v0}, LX/2kt;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    return-object v0
.end method
