.class public LX/2q9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2qI;

.field public b:LX/7QP;

.field public final c:LX/0Zr;

.field public d:Z

.field public e:Landroid/os/HandlerThread;

.field public f:Landroid/os/Handler;

.field public g:I

.field public h:LX/2pw;

.field public i:Z

.field private j:Z


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 470628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470629
    iput-object p1, p0, LX/2q9;->c:LX/0Zr;

    .line 470630
    iput-boolean v0, p0, LX/2q9;->d:Z

    .line 470631
    iput v0, p0, LX/2q9;->g:I

    .line 470632
    iput-boolean v0, p0, LX/2q9;->i:Z

    .line 470633
    iput-boolean v0, p0, LX/2q9;->j:Z

    .line 470634
    return-void
.end method

.method public static b(LX/2q9;I)V
    .locals 4

    .prologue
    .line 470568
    iget v0, p0, LX/2q9;->g:I

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    if-eqz v0, :cond_0

    iget v0, p0, LX/2q9;->g:I

    iget-object v1, p0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v1}, LX/7QP;->a()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 470569
    :cond_0
    :goto_0
    return-void

    .line 470570
    :cond_1
    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    iget v1, p0, LX/2q9;->g:I

    invoke-virtual {v0, v1}, LX/7QP;->a(I)LX/7QR;

    move-result-object v0

    .line 470571
    iget v1, v0, LX/7QR;->a:I

    move v0, v1

    .line 470572
    sub-int/2addr v0, p1

    int-to-long v0, v0

    .line 470573
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 470574
    iget-object v2, p0, LX/2q9;->f:Landroid/os/Handler;

    const v3, 0x1337c0de

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 470575
    iget-object v3, p0, LX/2q9;->f:Landroid/os/Handler;

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public static d(LX/2q9;I)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 470614
    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    if-nez v0, :cond_1

    move v0, v1

    .line 470615
    :cond_0
    :goto_0
    return v0

    .line 470616
    :cond_1
    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    .line 470617
    iget-object v2, v0, LX/7QP;->a:[LX/7QR;

    new-instance v3, LX/7QR;

    const-string v4, ""

    invoke-direct {v3, p1, p1, v4}, LX/7QR;-><init>(IILjava/lang/String;)V

    invoke-static {v2, v3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    move v0, v2

    .line 470618
    if-gez v0, :cond_2

    .line 470619
    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    .line 470620
    :cond_2
    iget-object v2, p0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v2}, LX/7QP;->a()I

    move-result v2

    if-lt v0, v2, :cond_3

    move v0, v1

    .line 470621
    goto :goto_0

    .line 470622
    :cond_3
    iget-object v2, p0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v2, v0}, LX/7QP;->a(I)LX/7QR;

    move-result-object v2

    .line 470623
    iget v3, v2, LX/7QR;->a:I

    move v2, v3

    .line 470624
    if-ge v2, p1, :cond_4

    .line 470625
    add-int/lit8 v0, v0, 0x1

    .line 470626
    :cond_4
    iget-object v2, p0, LX/2q9;->b:LX/7QP;

    invoke-virtual {v2}, LX/7QP;->a()I

    move-result v2

    if-lt v0, v2, :cond_0

    move v0, v1

    .line 470627
    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 470600
    iget-boolean v0, p0, LX/2q9;->d:Z

    if-eqz v0, :cond_0

    .line 470601
    invoke-virtual {p0}, LX/2q9;->d()V

    .line 470602
    :cond_0
    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    if-nez v0, :cond_1

    .line 470603
    :goto_0
    return-void

    .line 470604
    :cond_1
    iget-boolean v0, p0, LX/2q9;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2q9;->h:LX/2pw;

    if-eqz v0, :cond_2

    .line 470605
    iput v1, p0, LX/2q9;->g:I

    .line 470606
    :cond_2
    iput-boolean v1, p0, LX/2q9;->i:Z

    .line 470607
    iget-boolean v0, p0, LX/2q9;->d:Z

    if-nez v0, :cond_3

    .line 470608
    iget-object v0, p0, LX/2q9;->c:LX/0Zr;

    const-string v1, "background_video_subtitle_thread"

    sget-object v2, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-virtual {v0, v1, v2}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, LX/2q9;->e:Landroid/os/HandlerThread;

    .line 470609
    iget-object v0, p0, LX/2q9;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 470610
    new-instance v0, LX/7QS;

    iget-object v1, p0, LX/2q9;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/7QS;-><init>(Landroid/os/Looper;LX/2q9;)V

    iput-object v0, p0, LX/2q9;->f:Landroid/os/Handler;

    .line 470611
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2q9;->d:Z

    .line 470612
    :cond_3
    iget-object v0, p0, LX/2q9;->f:Landroid/os/Handler;

    const v1, 0x1337c0de

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 470613
    iget-object v1, p0, LX/2q9;->f:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 470598
    invoke-static {p0, p1}, LX/2q9;->d(LX/2q9;I)I

    move-result v0

    iput v0, p0, LX/2q9;->g:I

    .line 470599
    return-void
.end method

.method public final a(LX/2pw;)V
    .locals 0

    .prologue
    .line 470596
    iput-object p1, p0, LX/2q9;->h:LX/2pw;

    .line 470597
    return-void
.end method

.method public final a(LX/2qI;)V
    .locals 0

    .prologue
    .line 470594
    iput-object p1, p0, LX/2q9;->a:LX/2qI;

    .line 470595
    return-void
.end method

.method public final a(LX/7QP;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 470588
    iput-object p1, p0, LX/2q9;->b:LX/7QP;

    .line 470589
    if-nez p1, :cond_1

    .line 470590
    :cond_0
    :goto_0
    return v1

    .line 470591
    :cond_1
    iget-boolean v0, p0, LX/2q9;->j:Z

    if-eqz v0, :cond_0

    .line 470592
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2q9;->j:Z

    .line 470593
    invoke-direct {p0}, LX/2q9;->f()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 470584
    iget-object v0, p0, LX/2q9;->b:LX/7QP;

    if-nez v0, :cond_0

    .line 470585
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2q9;->j:Z

    .line 470586
    :goto_0
    return-void

    .line 470587
    :cond_0
    invoke-direct {p0}, LX/2q9;->f()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 470582
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2q9;->i:Z

    .line 470583
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 470576
    iget-boolean v1, p0, LX/2q9;->d:Z

    if-eqz v1, :cond_0

    .line 470577
    iget-object v1, p0, LX/2q9;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 470578
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2q9;->d:Z

    .line 470579
    :cond_0
    iput v0, p0, LX/2q9;->g:I

    .line 470580
    iput-boolean v0, p0, LX/2q9;->j:Z

    .line 470581
    return-void
.end method
