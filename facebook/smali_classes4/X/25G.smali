.class public LX/25G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25H;


# static fields
.field private static final a:LX/25I;


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 368960
    new-instance v0, LX/25I;

    invoke-direct {v0}, LX/25I;-><init>()V

    sput-object v0, LX/25G;->a:LX/25I;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0SI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 368961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368962
    iput-object p1, p0, LX/25G;->b:LX/0tX;

    .line 368963
    iput-object p2, p0, LX/25G;->c:LX/0SI;

    .line 368964
    return-void
.end method

.method public static a(Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)LX/0jT;
    .locals 14

    .prologue
    .line 368965
    const/4 v0, 0x0

    .line 368966
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 368967
    new-instance v0, LX/5Bq;

    invoke-direct {v0}, LX/5Bq;-><init>()V

    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v1

    .line 368968
    iput v1, v0, LX/5Bq;->a:I

    .line 368969
    move-object v0, v0

    .line 368970
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 368971
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 368972
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 368973
    iget v4, v0, LX/5Bq;->a:I

    invoke-virtual {v3, v6, v4, v6}, LX/186;->a(III)V

    .line 368974
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 368975
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 368976
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 368977
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 368978
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 368979
    new-instance v4, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    invoke-direct {v4, v3}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;-><init>(LX/15i;)V

    .line 368980
    move-object v0, v4

    .line 368981
    :cond_0
    new-instance v1, LX/5Bp;

    invoke-direct {v1}, LX/5Bp;-><init>()V

    iget-object v2, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->a:Ljava/lang/String;

    .line 368982
    iput-object v2, v1, LX/5Bp;->c:Ljava/lang/String;

    .line 368983
    move-object v1, v1

    .line 368984
    iget-boolean v2, p0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    .line 368985
    iput-boolean v2, v1, LX/5Bp;->a:Z

    .line 368986
    move-object v1, v1

    .line 368987
    iput-object v0, v1, LX/5Bp;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    .line 368988
    move-object v0, v1

    .line 368989
    const/4 v7, 0x1

    const/4 v13, 0x0

    const/4 v5, 0x0

    .line 368990
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 368991
    iget-object v4, v0, LX/5Bp;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 368992
    iget-object v6, v0, LX/5Bp;->c:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 368993
    iget-object v8, v0, LX/5Bp;->d:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-static {v3, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 368994
    iget-object v9, v0, LX/5Bp;->e:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel$LikersModel;

    invoke-static {v3, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 368995
    iget-object v10, v0, LX/5Bp;->f:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-static {v3, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 368996
    iget-object v11, v0, LX/5Bp;->g:Lcom/facebook/api/graphql/likes/LikeMutationsModels$FeedbackTextWithEntitiesWithRangesFieldsModel;

    invoke-static {v3, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 368997
    const/4 v12, 0x7

    invoke-virtual {v3, v12}, LX/186;->c(I)V

    .line 368998
    iget-boolean v12, v0, LX/5Bp;->a:Z

    invoke-virtual {v3, v13, v12}, LX/186;->a(IZ)V

    .line 368999
    invoke-virtual {v3, v7, v4}, LX/186;->b(II)V

    .line 369000
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 369001
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 369002
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 369003
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 369004
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 369005
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 369006
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 369007
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 369008
    invoke-virtual {v4, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 369009
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 369010
    new-instance v4, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;

    invoke-direct {v4, v3}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$FBFeedbackLikeMutationFragmentModel;-><init>(LX/15i;)V

    .line 369011
    move-object v0, v4

    .line 369012
    return-object v0
.end method

.method private a(LX/0zP;LX/2vO;LX/0jT;LX/0Rf;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zP;",
            "LX/2vO;",
            "LX/0jT;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369013
    const-string v0, "input"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 369014
    new-instance v0, LX/399;

    invoke-direct {v0, p1, p4}, LX/399;-><init>(LX/0zP;LX/0Rf;)V

    .line 369015
    invoke-virtual {v0, p3}, LX/399;->a(LX/0jT;)LX/399;

    .line 369016
    if-eqz p5, :cond_0

    .line 369017
    iget-object v1, p0, LX/25G;->c:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 369018
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 369019
    :cond_0
    iget-object v1, p0, LX/25G;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->b(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 369020
    sget-object v1, LX/25G;->a:LX/25I;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/25G;
    .locals 3

    .prologue
    .line 369021
    new-instance v2, LX/25G;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-direct {v2, v0, v1}, LX/25G;-><init>(LX/0tX;LX/0SI;)V

    .line 369022
    return-object v2
.end method

.method public static b(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V
    .locals 3

    .prologue
    .line 369023
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 369024
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 369025
    if-nez v0, :cond_1

    .line 369026
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "feedback == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369027
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 369028
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 369029
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 369030
    const-string v2, "feedback.id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369031
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369032
    const-string v2, ", feedback.legacyApiPostId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369033
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369034
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369035
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369036
    const-string v0, "params"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369037
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    const-string v1, "likeableId"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369038
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    const-string v1, "likerProfile"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369039
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    if-eqz v0, :cond_2

    .line 369040
    new-instance v0, LX/5Bm;

    invoke-direct {v0}, LX/5Bm;-><init>()V

    move-object v1, v0

    .line 369041
    new-instance v0, LX/4Ho;

    invoke-direct {v0}, LX/4Ho;-><init>()V

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    .line 369042
    const-string v3, "page_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369043
    move-object v0, v0

    .line 369044
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    .line 369045
    const-string v3, "actor_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369046
    move-object v2, v0

    .line 369047
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->e:Ljava/lang/String;

    .line 369048
    if-nez v0, :cond_0

    .line 369049
    const/4 v0, 0x0

    .line 369050
    :cond_0
    move-object v0, v0

    .line 369051
    if-eqz v0, :cond_1

    .line 369052
    const-string v3, "source"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369053
    :cond_1
    :goto_0
    new-instance v0, LX/5Bt;

    invoke-direct {v0}, LX/5Bt;-><init>()V

    iget-boolean v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    .line 369054
    iput-boolean v3, v0, LX/5Bt;->a:Z

    .line 369055
    move-object v0, v0

    .line 369056
    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    .line 369057
    iput-object v3, v0, LX/5Bt;->b:Ljava/lang/String;

    .line 369058
    move-object v0, v0

    .line 369059
    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v8, 0x0

    .line 369060
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 369061
    iget-object v7, v0, LX/5Bt;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 369062
    const/4 v9, 0x2

    invoke-virtual {v6, v9}, LX/186;->c(I)V

    .line 369063
    iget-boolean v9, v0, LX/5Bt;->a:Z

    invoke-virtual {v6, v11, v9}, LX/186;->a(IZ)V

    .line 369064
    invoke-virtual {v6, v10, v7}, LX/186;->b(II)V

    .line 369065
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 369066
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 369067
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 369068
    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 369069
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 369070
    new-instance v7, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    invoke-direct {v7, v6}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;-><init>(LX/15i;)V

    .line 369071
    move-object v0, v7

    .line 369072
    new-instance v3, LX/5Bs;

    invoke-direct {v3}, LX/5Bs;-><init>()V

    .line 369073
    iput-object v0, v3, LX/5Bs;->a:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    .line 369074
    move-object v0, v3

    .line 369075
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 369076
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 369077
    iget-object v7, v0, LX/5Bs;->a:Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageToggleLikeFragmentModel;

    invoke-static {v6, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 369078
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 369079
    invoke-virtual {v6, v9, v7}, LX/186;->b(II)V

    .line 369080
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 369081
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 369082
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 369083
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 369084
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 369085
    new-instance v7, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;

    invoke-direct {v7, v6}, Lcom/facebook/api/graphql/likes/LikeMutationsModels$PageLikeModel;-><init>(LX/15i;)V

    .line 369086
    move-object v3, v7

    .line 369087
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v4, v0

    .line 369088
    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/25G;->a(LX/0zP;LX/2vO;LX/0jT;LX/0Rf;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 369089
    :cond_2
    new-instance v0, LX/5Bn;

    invoke-direct {v0}, LX/5Bn;-><init>()V

    move-object v1, v0

    .line 369090
    new-instance v0, LX/4I2;

    invoke-direct {v0}, LX/4I2;-><init>()V

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    .line 369091
    const-string v3, "page_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369092
    move-object v0, v0

    .line 369093
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    .line 369094
    const-string v3, "actor_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369095
    move-object v2, v0

    .line 369096
    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 369097
    invoke-static {p1}, LX/25G;->b(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V

    .line 369098
    invoke-virtual {p1}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b()Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    move-result-object v6

    .line 369099
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    const-string v3, "null actor"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369100
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    iget-boolean v3, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Feedback object passed in should reflect the optimistic mutation"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 369101
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    .line 369102
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Null/empty actor ID"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 369103
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_3

    .line 369104
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 369105
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 369106
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 369107
    iget-object v2, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 369108
    iget-object v0, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f()LX/0Px;

    move-result-object v0

    .line 369109
    :goto_2
    iget-boolean v3, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->b:Z

    if-eqz v3, :cond_2

    .line 369110
    invoke-static {}, LX/5Bo;->a()LX/5Bk;

    move-result-object v3

    .line 369111
    new-instance v7, LX/4En;

    invoke-direct {v7}, LX/4En;-><init>()V

    iget-object v8, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4En;->b(Ljava/lang/String;)LX/4En;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/4En;->a(Ljava/lang/String;)LX/4En;

    move-result-object v7

    .line 369112
    const-string v8, "nectar_module"

    invoke-virtual {v7, v8, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369113
    move-object v2, v7

    .line 369114
    const-string v7, "feedback_source"

    invoke-virtual {v2, v7, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369115
    move-object v1, v2

    .line 369116
    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 369117
    move-object v2, v1

    .line 369118
    move-object v1, v3

    .line 369119
    :goto_3
    invoke-static {v4}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v4

    .line 369120
    invoke-static {v6}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)LX/0jT;

    move-result-object v3

    move-object v0, p0

    .line 369121
    invoke-direct/range {v0 .. v5}, LX/25G;->a(LX/0zP;LX/2vO;LX/0jT;LX/0Rf;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v5

    .line 369122
    goto :goto_0

    :cond_1
    move v1, v5

    .line 369123
    goto :goto_1

    .line 369124
    :cond_2
    invoke-static {}, LX/5Bo;->b()LX/5Bl;

    move-result-object v3

    .line 369125
    new-instance v7, LX/4Es;

    invoke-direct {v7}, LX/4Es;-><init>()V

    iget-object v8, v6, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4Es;->b(Ljava/lang/String;)LX/4Es;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/4Es;->a(Ljava/lang/String;)LX/4Es;

    move-result-object v7

    .line 369126
    const-string v8, "nectar_module"

    invoke-virtual {v7, v8, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369127
    move-object v2, v7

    .line 369128
    const-string v7, "feedback_source"

    invoke-virtual {v2, v7, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369129
    move-object v1, v2

    .line 369130
    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 369131
    move-object v2, v1

    .line 369132
    move-object v1, v3

    goto :goto_3

    :cond_3
    move-object v0, v2

    move-object v1, v2

    goto :goto_2
.end method
