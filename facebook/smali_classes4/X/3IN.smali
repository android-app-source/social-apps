.class public final LX/3IN;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7M4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 546059
    iput-object p1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7M4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546060
    const-class v0, LX/7M4;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 546061
    const/16 v8, 0x3e8

    .line 546062
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_2

    .line 546063
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v2, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->w:F

    .line 546064
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v3, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->x:F

    .line 546065
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v4, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->y:F

    .line 546066
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getDefaultFov(Lcom/facebook/video/player/plugins/Video360Plugin;)F

    move-result v7

    .line 546067
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    .line 546068
    iget-boolean v1, v0, LX/7E5;->b:Z

    move v0, v1

    .line 546069
    if-eqz v0, :cond_3

    .line 546070
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->G(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 546071
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget-object v1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v0, v1}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    neg-int v0, v0

    int-to-float v1, v0

    .line 546072
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->D:LX/7E5;

    iget-object v5, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget v5, v5, Lcom/facebook/video/player/plugins/Video360Plugin;->F:I

    invoke-virtual {v0, v5}, LX/7E5;->a(I)Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    int-to-float v0, v0

    move v6, v0

    move v5, v1

    .line 546073
    :goto_0
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546074
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    iget-object v1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v7, v8, v1}, LX/3IP;->a(FILX/3II;)V

    .line 546075
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    iget-object v1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v5, v6, v8, v1}, LX/3IP;->a(FFILX/3II;)V

    .line 546076
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v8, v8, LX/2oy;->j:LX/2pb;

    invoke-virtual {v8}, LX/2pb;->s()LX/04D;

    move-result-object v8

    .line 546077
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v10, LX/0JK;->HEADING_RESET:LX/0JK;

    iget-object v10, v10, LX/0JK;->value:Ljava/lang/String;

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v10, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v10, v10, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v9, v10, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object v10, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v10, v10, LX/04F;->value:Ljava/lang/String;

    iget-object p1, v8, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v9, v10, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object v10, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object v10, v10, LX/04F;->value:Ljava/lang/String;

    iget-object p1, v8, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {v9, v10, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 546078
    invoke-static {v0, v9}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    .line 546079
    :cond_0
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->b:Z

    if-nez v0, :cond_1

    .line 546080
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M0;

    invoke-direct {v1}, LX/7M0;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 546081
    :cond_1
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_2

    .line 546082
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    float-to-int v2, v2

    float-to-int v3, v3

    float-to-int v4, v4

    float-to-int v5, v5

    float-to-int v6, v6

    float-to-int v7, v7

    iget-object v8, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {v8}, Lcom/facebook/video/player/plugins/Video360Plugin;->getCurrentPlaybackPositionMs(Lcom/facebook/video/player/plugins/Video360Plugin;)I

    move-result v8

    div-int/lit16 v8, v8, 0x3e8

    iget-object v9, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v9, v9, LX/2oy;->j:LX/2pb;

    invoke-virtual {v9}, LX/2pb;->s()LX/04D;

    move-result-object v9

    .line 546083
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/0JI;->SPHERICAL_VIDEO_HEADING_INDICATOR_CLICK:LX/0JI;

    iget-object p0, p0, LX/0JI;->value:Ljava/lang/String;

    invoke-direct {v10, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p0, LX/04F;->VIDEO_ID:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->PITCH_ANGLE:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->YAW_ANGLE:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->TARGET_PITCH_ANGLE:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->TARGET_YAW_ANGLE:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->FIELD_OF_VIEW_VERTICAL:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/0JH;->TARGET_FIELD_OF_VIEW_VERTICAL:LX/0JH;

    iget-object p0, p0, LX/0JH;->value:Ljava/lang/String;

    invoke-virtual {v10, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    iget-object p1, v9, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v10, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p0, LX/04F;->PLAYER_SUBORIGIN:LX/04F;

    iget-object p0, p0, LX/04F;->value:Ljava/lang/String;

    iget-object p1, v9, LX/04D;->subOrigin:Ljava/lang/String;

    invoke-virtual {v10, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    .line 546084
    invoke-static {v0, v10}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    .line 546085
    :cond_2
    return-void

    .line 546086
    :cond_3
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->e()F

    move-result v1

    .line 546087
    iget-object v0, p0, LX/3IN;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->P:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->c()F

    move-result v0

    move v6, v0

    move v5, v1

    goto/16 :goto_0
.end method
