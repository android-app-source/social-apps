.class public LX/3Kw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/presence/PresenceManager;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2Oi;


# direct methods
.method private constructor <init>(LX/0Or;LX/2Oi;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Oi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 549521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549522
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549523
    iput-object v0, p0, LX/3Kw;->a:LX/0Ot;

    .line 549524
    iput-object p1, p0, LX/3Kw;->b:LX/0Or;

    .line 549525
    iput-object p2, p0, LX/3Kw;->c:LX/2Oi;

    .line 549526
    return-void
.end method

.method public static b(LX/0QB;)LX/3Kw;
    .locals 3

    .prologue
    .line 549527
    new-instance v1, LX/3Kw;

    const/16 v0, 0x12cb

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v0

    check-cast v0, LX/2Oi;

    invoke-direct {v1, v2, v0}, LX/3Kw;-><init>(LX/0Or;LX/2Oi;)V

    .line 549528
    const/16 v0, 0xfa5

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    .line 549529
    iput-object v0, v1, LX/3Kw;->a:LX/0Ot;

    .line 549530
    return-object v1
.end method
