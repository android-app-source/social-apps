.class public final LX/2Cs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(Landroid/content/Context;I)Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "com.google.android.gms"

    invoke-static {p0, p1, v1}, LX/2Cs;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    const-string v2, "com.google.android.gms"

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {p0}, LX/1oY;->a(Landroid/content/Context;)LX/1oY;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    move v0, v2

    goto :goto_0

    :catch_0
    goto :goto_0

    :cond_2
    invoke-static {v0, v2}, LX/1oY;->a(Landroid/content/pm/PackageInfo;Z)Z

    move-result p0

    if-eqz p0, :cond_3

    move v2, v3

    goto :goto_1

    :cond_3
    invoke-static {v0, v3}, LX/1oY;->a(Landroid/content/pm/PackageInfo;Z)Z

    move-result p0

    if-eqz p0, :cond_1

    iget-object p0, v1, LX/1oY;->b:Landroid/content/Context;

    invoke-static {p0}, LX/1oW;->c(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_4

    move v2, v3

    goto :goto_1

    :cond_4
    const-string v3, "GoogleSignatureVerifier"

    const-string p0, "Test-keys aren\'t accepted on this build."

    invoke-static {v3, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    invoke-static {p0}, LX/1oi;->a(Landroid/content/Context;)LX/1oj;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x13

    invoke-static {v1}, LX/1oe;->a(I)Z

    move-result v1

    move v1, v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, v0, LX/1oj;->a:Landroid/content/Context;

    const-string v4, "appops"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AppOpsManager;

    invoke-virtual {v1, p1, p2}, Landroid/app/AppOpsManager;->checkPackage(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    :cond_0
    :goto_0
    move v0, v2

    return v0

    :cond_1
    iget-object v1, v0, LX/1oj;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_0

    if-eqz v4, :cond_0

    move v1, v2

    :goto_1
    array-length p0, v4

    if-ge v1, p0, :cond_0

    aget-object p0, v4, v1

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    goto :goto_0
.end method
