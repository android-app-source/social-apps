.class public LX/3Og;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/3Oh;

.field public final b:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DAQ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I


# direct methods
.method public constructor <init>(LX/3Oh;Ljava/lang/CharSequence;LX/0Px;)V
    .locals 4
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Oh;",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "LX/DAQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 560675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560676
    iput-object p1, p0, LX/3Og;->a:LX/3Oh;

    .line 560677
    iput-object p2, p0, LX/3Og;->b:Ljava/lang/CharSequence;

    .line 560678
    iput-object p3, p0, LX/3Og;->c:LX/0Px;

    .line 560679
    if-eqz p3, :cond_1

    .line 560680
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAQ;

    .line 560681
    iget-object v0, v0, LX/DAQ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v1, v0

    .line 560682
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 560683
    :cond_1
    iput v0, p0, LX/3Og;->d:I

    .line 560684
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;)LX/3Og;
    .locals 3

    .prologue
    .line 560704
    new-instance v0, LX/3Og;

    sget-object v1, LX/3Oh;->EMPTY_CONSTRAINT:LX/3Oh;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, LX/3Og;-><init>(LX/3Oh;Ljava/lang/CharSequence;LX/0Px;)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)",
            "LX/3Og;"
        }
    .end annotation

    .prologue
    .line 560705
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 560706
    new-instance v0, LX/DAQ;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/DAQ;-><init>(LX/0Px;Ljava/lang/String;)V

    .line 560707
    new-instance v1, LX/3Og;

    sget-object v2, LX/3Oh;->OK:LX/3Oh;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v2, p0, v0}, LX/3Og;-><init>(LX/3Oh;Ljava/lang/CharSequence;LX/0Px;)V

    return-object v1
.end method

.method public static b(Ljava/lang/CharSequence;)LX/3Og;
    .locals 3

    .prologue
    .line 560702
    new-instance v0, LX/3Og;

    sget-object v1, LX/3Oh;->EXCEPTION:LX/3Oh;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, LX/3Og;-><init>(LX/3Oh;Ljava/lang/CharSequence;LX/0Px;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/3Oh;
    .locals 1

    .prologue
    .line 560703
    iget-object v0, p0, LX/3Og;->a:LX/3Oh;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 560701
    iget-object v0, p0, LX/3Og;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 560691
    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 560692
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 560693
    :goto_0
    return-object v0

    .line 560694
    :cond_1
    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 560695
    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAQ;

    iget-object v0, v0, LX/DAQ;->a:LX/0Px;

    goto :goto_0

    .line 560696
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 560697
    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    iget-object v0, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAQ;

    .line 560698
    iget-object v0, v0, LX/DAQ;->a:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 560699
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 560700
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 560685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 560686
    const-string v1, "State: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/3Og;->a:LX/3Oh;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560687
    const-string v1, "Constraints: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/3Og;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 560688
    iget-object v1, p0, LX/3Og;->c:LX/0Px;

    if-eqz v1, :cond_0

    .line 560689
    const-string v1, ", Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/3Og;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 560690
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
