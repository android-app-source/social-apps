.class public LX/2eq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field private final b:Landroid/content/Context;

.field private final c:LX/17W;

.field private final d:LX/17Q;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/content/Context;LX/17W;LX/17Q;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445792
    iput-object p1, p0, LX/2eq;->a:LX/0Zb;

    .line 445793
    iput-object p2, p0, LX/2eq;->b:Landroid/content/Context;

    .line 445794
    iput-object p3, p0, LX/2eq;->c:LX/17W;

    .line 445795
    iput-object p4, p0, LX/2eq;->d:LX/17Q;

    .line 445796
    const v0, 0x7f080f85

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2eq;->e:Ljava/lang/String;

    .line 445797
    const v0, 0x7f080f8a

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2eq;->f:Ljava/lang/String;

    .line 445798
    const v0, 0x7f080f86

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2eq;->g:Ljava/lang/String;

    .line 445799
    return-void
.end method

.method public static a(LX/0QB;)LX/2eq;
    .locals 9

    .prologue
    .line 445800
    const-class v1, LX/2eq;

    monitor-enter v1

    .line 445801
    :try_start_0
    sget-object v0, LX/2eq;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445802
    sput-object v2, LX/2eq;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445803
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445804
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445805
    new-instance v3, LX/2eq;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v7

    check-cast v7, LX/17Q;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, LX/2eq;-><init>(LX/0Zb;Landroid/content/Context;LX/17W;LX/17Q;Landroid/content/res/Resources;)V

    .line 445806
    move-object v0, v3

    .line 445807
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445808
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2eq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445809
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/2ep;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 445811
    iget-object v0, p2, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 445812
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_1

    .line 445813
    iget-object p1, p0, LX/2eq;->e:Ljava/lang/String;

    .line 445814
    :cond_0
    :goto_0
    return-object p1

    .line 445815
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_2

    .line 445816
    iget-object p1, p0, LX/2eq;->f:Ljava/lang/String;

    goto :goto_0

    .line 445817
    :cond_2
    iget-boolean v0, p2, LX/2ep;->b:Z

    if-eqz v0, :cond_0

    .line 445818
    iget-object p1, p0, LX/2eq;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)V
    .locals 4
    .param p4    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 445819
    if-eqz p4, :cond_0

    .line 445820
    iget-object v0, p0, LX/2eq;->a:LX/0Zb;

    .line 445821
    invoke-static {p4}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 445822
    const/4 v1, 0x0

    .line 445823
    :goto_0
    move-object v1, v1

    .line 445824
    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 445825
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 445826
    invoke-static {v0, p1, p2, p3}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445827
    iget-object v1, p0, LX/2eq;->c:LX/17W;

    iget-object v2, p0, LX/2eq;->b:Landroid/content/Context;

    sget-object v3, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 445828
    return-void

    .line 445829
    :cond_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "pymk_profile"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 445830
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 445831
    move-object v1, v1

    .line 445832
    goto :goto_0
.end method
