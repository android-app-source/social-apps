.class public LX/2OO;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400804
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 400805
    return-void
.end method

.method public static a(LX/2ON;)LX/2OQ;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .prologue
    .line 400806
    sget-object v0, LX/2OP;->FACEBOOK:LX/2OP;

    invoke-virtual {p0, v0}, LX/2ON;->a(LX/2OP;)LX/2OQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/01T;)Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
    .end annotation

    .prologue
    .line 400807
    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2ON;)LX/2OQ;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .prologue
    .line 400808
    sget-object v0, LX/2OP;->SMS:LX/2OP;

    invoke-virtual {p0, v0}, LX/2ON;->a(LX/2OP;)LX/2OQ;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2ON;)LX/2OQ;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
    .end annotation

    .prologue
    .line 400809
    sget-object v0, LX/2OP;->TINCAN:LX/2OP;

    invoke-virtual {p0, v0}, LX/2ON;->a(LX/2OP;)LX/2OQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 400810
    return-void
.end method
