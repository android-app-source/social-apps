.class public final enum LX/2MK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2MK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2MK;

.field public static final enum AUDIO:LX/2MK;

.field public static final enum ENCRYPTED_PHOTO:LX/2MK;

.field public static final enum ENT_PHOTO:LX/2MK;

.field public static final enum OTHER:LX/2MK;

.field public static final enum PHOTO:LX/2MK;

.field private static final VALUE_MAP:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VIDEO:LX/2MK;


# instance fields
.field public final DBSerialValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 396411
    new-instance v1, LX/2MK;

    const-string v2, "PHOTO"

    const-string v3, "PHOTO"

    invoke-direct {v1, v2, v0, v3}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 396412
    new-instance v1, LX/2MK;

    const-string v2, "VIDEO"

    const-string v3, "VIDEO"

    invoke-direct {v1, v2, v5, v3}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->VIDEO:LX/2MK;

    .line 396413
    new-instance v1, LX/2MK;

    const-string v2, "AUDIO"

    const-string v3, "AUDIO"

    invoke-direct {v1, v2, v6, v3}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->AUDIO:LX/2MK;

    .line 396414
    new-instance v1, LX/2MK;

    const-string v2, "OTHER"

    const-string v3, "OTHER"

    invoke-direct {v1, v2, v7, v3}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->OTHER:LX/2MK;

    .line 396415
    new-instance v1, LX/2MK;

    const-string v2, "ENCRYPTED_PHOTO"

    const-string v3, "ENCRYPTED_PHOTO"

    invoke-direct {v1, v2, v8, v3}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    .line 396416
    new-instance v1, LX/2MK;

    const-string v2, "ENT_PHOTO"

    const/4 v3, 0x5

    const-string v4, "ENT_PHOTO"

    invoke-direct {v1, v2, v3, v4}, LX/2MK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/2MK;->ENT_PHOTO:LX/2MK;

    .line 396417
    const/4 v1, 0x6

    new-array v1, v1, [LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    aput-object v2, v1, v0

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    aput-object v2, v1, v5

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    aput-object v2, v1, v6

    sget-object v2, LX/2MK;->OTHER:LX/2MK;

    aput-object v2, v1, v7

    sget-object v2, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/2MK;->ENT_PHOTO:LX/2MK;

    aput-object v3, v1, v2

    sput-object v1, LX/2MK;->$VALUES:[LX/2MK;

    .line 396418
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 396419
    invoke-static {}, LX/2MK;->values()[LX/2MK;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 396420
    iget-object v5, v4, LX/2MK;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 396421
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 396422
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/2MK;->VALUE_MAP:LX/0P1;

    .line 396423
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 396424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 396425
    iput-object p3, p0, LX/2MK;->DBSerialValue:Ljava/lang/String;

    .line 396426
    return-void
.end method

.method public static fromDBSerialValue(Ljava/lang/String;)LX/2MK;
    .locals 3

    .prologue
    .line 396427
    sget-object v0, LX/2MK;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396428
    sget-object v0, LX/2MK;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MK;

    return-object v0

    .line 396429
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2MK;
    .locals 1

    .prologue
    .line 396430
    const-class v0, LX/2MK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2MK;

    return-object v0
.end method

.method public static values()[LX/2MK;
    .locals 1

    .prologue
    .line 396431
    sget-object v0, LX/2MK;->$VALUES:[LX/2MK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2MK;

    return-object v0
.end method
