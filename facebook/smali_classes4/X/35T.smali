.class public LX/35T;
.super Lcom/facebook/location/BaseFbLocationManager;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/concurrent/ScheduledExecutorService;

.field public c:LX/0SG;

.field public d:J

.field public final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497171
    const-class v0, LX/35T;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/35T;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0y3;",
            "LX/0SG;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Zb;",
            "LX/0y2;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497166
    invoke-direct/range {p0 .. p8}, Lcom/facebook/location/BaseFbLocationManager;-><init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V

    .line 497167
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/35T;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 497168
    iput-object p3, p0, LX/35T;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 497169
    iput-object p2, p0, LX/35T;->c:LX/0SG;

    .line 497170
    return-void
.end method

.method public static a$redex0(LX/35T;J)V
    .locals 3

    .prologue
    .line 497163
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 497164
    :goto_0
    return-void

    .line 497165
    :cond_0
    iget-object v0, p0, LX/35T;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/location/MockStaticMpkFbLocationManager$1;-><init>(LX/35T;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/35T;->f:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1wZ;
    .locals 1

    .prologue
    .line 497152
    sget-object v0, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    return-object v0
.end method

.method public final a(LX/2vk;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 497158
    iget-object v1, p0, LX/35T;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "operation already running"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 497159
    iget-wide v0, p1, LX/2vk;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/35T;->d:J

    .line 497160
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/35T;->a$redex0(LX/35T;J)V

    .line 497161
    return-void

    .line 497162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 497153
    iget-object v0, p0, LX/35T;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 497154
    :cond_0
    :goto_0
    return-void

    .line 497155
    :cond_1
    iget-object v0, p0, LX/35T;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 497156
    iget-object v0, p0, LX/35T;->f:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 497157
    const/4 v0, 0x0

    iput-object v0, p0, LX/35T;->f:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method
