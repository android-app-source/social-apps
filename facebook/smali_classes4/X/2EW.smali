.class public LX/2EW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2EW;


# instance fields
.field public final a:LX/0kb;

.field public final b:LX/0Zb;

.field private final c:LX/0TR;


# direct methods
.method public constructor <init>(LX/0kb;LX/0Zb;LX/0TR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385454
    iput-object p1, p0, LX/2EW;->a:LX/0kb;

    .line 385455
    iput-object p2, p0, LX/2EW;->b:LX/0Zb;

    .line 385456
    iput-object p3, p0, LX/2EW;->c:LX/0TR;

    .line 385457
    return-void
.end method

.method public static a(LX/0QB;)LX/2EW;
    .locals 6

    .prologue
    .line 385458
    sget-object v0, LX/2EW;->d:LX/2EW;

    if-nez v0, :cond_1

    .line 385459
    const-class v1, LX/2EW;

    monitor-enter v1

    .line 385460
    :try_start_0
    sget-object v0, LX/2EW;->d:LX/2EW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 385461
    if-eqz v2, :cond_0

    .line 385462
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 385463
    new-instance p0, LX/2EW;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v5

    check-cast v5, LX/0TR;

    invoke-direct {p0, v3, v4, v5}, LX/2EW;-><init>(LX/0kb;LX/0Zb;LX/0TR;)V

    .line 385464
    move-object v0, p0

    .line 385465
    sput-object v0, LX/2EW;->d:LX/2EW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 385467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385468
    :cond_1
    sget-object v0, LX/2EW;->d:LX/2EW;

    return-object v0

    .line 385469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 385470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 385471
    iget-object v0, p0, LX/2EW;->c:LX/0TR;

    .line 385472
    new-instance v1, LX/2EX;

    invoke-direct {v1, p0}, LX/2EX;-><init>(LX/2EW;)V

    move-object v1, v1

    .line 385473
    iget-object p0, v0, LX/0TR;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 385474
    return-void
.end method
