.class public LX/2WI;
.super LX/2WJ;
.source ""


# instance fields
.field public c:I

.field private final d:Z


# direct methods
.method public constructor <init>(LX/15w;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 418705
    invoke-direct {p0, p1}, LX/2WJ;-><init>(LX/15w;)V

    .line 418706
    iput-boolean p2, p0, LX/2WI;->d:Z

    .line 418707
    iget-boolean v0, p0, LX/2WI;->d:Z

    if-eqz v0, :cond_1

    .line 418708
    instance-of v0, p1, LX/2WI;

    if-eqz v0, :cond_0

    .line 418709
    check-cast p1, LX/2WI;

    iget v0, p1, LX/2WI;->c:I

    iput v0, p0, LX/2WI;->c:I

    .line 418710
    :goto_0
    return-void

    .line 418711
    :cond_0
    iput v1, p0, LX/2WI;->c:I

    goto :goto_0

    .line 418712
    :cond_1
    iput v1, p0, LX/2WI;->c:I

    goto :goto_0
.end method

.method private K()V
    .locals 2

    .prologue
    .line 418713
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 418714
    :cond_0
    iget v0, p0, LX/2WI;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2WI;->c:I

    .line 418715
    :cond_1
    return-void
.end method


# virtual methods
.method public final J()LX/0lG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0lG;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 418716
    invoke-direct {p0}, LX/2WI;->K()V

    .line 418717
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/266;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/266",
            "<*>;)TT;"
        }
    .end annotation

    .prologue
    .line 418718
    invoke-direct {p0}, LX/2WI;->K()V

    .line 418719
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    .line 418720
    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 418721
    invoke-direct {p0}, LX/2WI;->K()V

    .line 418722
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 418723
    return-object v0
.end method

.method public final b(Ljava/lang/Class;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 418724
    invoke-direct {p0}, LX/2WI;->K()V

    .line 418725
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0, p1}, LX/15w;->b(Ljava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public c()LX/15z;
    .locals 2

    .prologue
    .line 418726
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 418727
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 418728
    :cond_0
    iget v1, p0, LX/2WI;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/2WI;->c:I

    .line 418729
    :cond_1
    sget-object v1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-ne v0, v1, :cond_3

    .line 418730
    :cond_2
    iget v1, p0, LX/2WI;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/2WI;->c:I

    .line 418731
    :cond_3
    return-object v0
.end method

.method public f()LX/15w;
    .locals 8

    .prologue
    .line 418732
    iget-boolean v0, p0, LX/2WI;->d:Z

    if-nez v0, :cond_1

    .line 418733
    invoke-super {p0}, LX/2WJ;->f()LX/15w;

    move-result-object v0

    .line 418734
    :cond_0
    :goto_0
    return-object v0

    .line 418735
    :cond_1
    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v0

    .line 418736
    iget-wide v6, v0, LX/28G;->_totalChars:J

    move-wide v2, v6

    .line 418737
    invoke-super {p0}, LX/2WJ;->f()LX/15w;

    move-result-object v0

    .line 418738
    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v1

    .line 418739
    iget-wide v6, v1, LX/28G;->_totalChars:J

    move-wide v4, v6

    .line 418740
    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 418741
    iget v1, p0, LX/2WI;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/2WI;->c:I

    goto :goto_0
.end method
