.class public LX/37s;
.super LX/37t;
.source ""

# interfaces
.implements LX/37y;


# instance fields
.field private j:Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;

.field private k:LX/3Fk;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/37l;)V
    .locals 0

    .prologue
    .line 502427
    invoke-direct {p0, p1, p2}, LX/37t;-><init>(Landroid/content/Context;LX/37l;)V

    .line 502428
    return-void
.end method


# virtual methods
.method public a(LX/38G;LX/38H;)V
    .locals 3

    .prologue
    .line 502414
    invoke-super {p0, p1, p2}, LX/37t;->a(LX/38G;LX/38H;)V

    .line 502415
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    .line 502416
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v1

    move v0, v1

    .line 502417
    if-nez v0, :cond_0

    .line 502418
    const/4 v0, 0x0

    .line 502419
    iget-object v1, p2, LX/38H;->a:Landroid/os/Bundle;

    const-string v2, "enabled"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 502420
    :cond_0
    invoke-virtual {p0, p1}, LX/37s;->a(LX/38G;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502421
    const/4 v0, 0x1

    .line 502422
    iget-object v1, p2, LX/38H;->a:Landroid/os/Bundle;

    const-string v2, "connecting"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 502423
    :cond_1
    iget-object v0, p1, LX/38G;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/4tq;->b(Ljava/lang/Object;)Landroid/view/Display;

    move-result-object v0

    .line 502424
    if-eqz v0, :cond_2

    .line 502425
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, LX/38H;->f(I)LX/38H;

    .line 502426
    :cond_2
    return-void
.end method

.method public a(LX/38G;)Z
    .locals 3

    .prologue
    .line 502402
    iget-object v0, p0, LX/37s;->k:LX/3Fk;

    if-nez v0, :cond_0

    .line 502403
    new-instance v0, LX/3Fk;

    invoke-direct {v0}, LX/3Fk;-><init>()V

    iput-object v0, p0, LX/37s;->k:LX/3Fk;

    .line 502404
    :cond_0
    iget-object v0, p0, LX/37s;->k:LX/3Fk;

    iget-object v1, p1, LX/38G;->a:Ljava/lang/Object;

    const/4 p0, 0x0

    .line 502405
    check-cast v1, Landroid/media/MediaRouter$RouteInfo;

    .line 502406
    iget-object v2, v0, LX/3Fk;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_2

    .line 502407
    :try_start_0
    iget-object v2, v0, LX/3Fk;->a:Ljava/lang/reflect/Method;

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {v2, v1, p1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 502408
    iget p1, v0, LX/3Fk;->b:I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    .line 502409
    :goto_0
    move v0, v2

    .line 502410
    return v0

    :cond_1
    move v2, p0

    .line 502411
    goto :goto_0

    :catch_0
    :cond_2
    :goto_1
    move v2, p0

    .line 502412
    goto :goto_0

    .line 502413
    :catch_1
    goto :goto_1
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 502392
    invoke-virtual {p0, p1}, LX/37t;->g(Ljava/lang/Object;)I

    move-result v0

    .line 502393
    if-ltz v0, :cond_0

    .line 502394
    iget-object v1, p0, LX/37t;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38G;

    .line 502395
    invoke-static {p1}, LX/4tq;->b(Ljava/lang/Object;)Landroid/view/Display;

    move-result-object v1

    .line 502396
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 502397
    :goto_0
    iget-object v2, v0, LX/38G;->c:LX/383;

    invoke-virtual {v2}, LX/383;->l()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 502398
    new-instance v2, LX/38H;

    iget-object v3, v0, LX/38G;->c:LX/383;

    invoke-direct {v2, v3}, LX/38H;-><init>(LX/383;)V

    invoke-virtual {v2, v1}, LX/38H;->f(I)LX/38H;

    move-result-object v1

    invoke-virtual {v1}, LX/38H;->a()LX/383;

    move-result-object v1

    iput-object v1, v0, LX/38G;->c:LX/383;

    .line 502399
    invoke-virtual {p0}, LX/37t;->f()V

    .line 502400
    :cond_0
    return-void

    .line 502401
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 502372
    invoke-super {p0}, LX/37t;->g()V

    .line 502373
    iget-object v0, p0, LX/37s;->j:Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;

    if-nez v0, :cond_0

    .line 502374
    new-instance v0, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;

    .line 502375
    iget-object v1, p0, LX/37v;->a:Landroid/content/Context;

    move-object v1, v1

    .line 502376
    iget-object v2, p0, LX/37v;->c:LX/38C;

    move-object v2, v2

    .line 502377
    invoke-direct {v0, v1, v2}, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, LX/37s;->j:Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;

    .line 502378
    :cond_0
    iget-object v1, p0, LX/37s;->j:Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;

    iget-boolean v0, p0, LX/37t;->f:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/37t;->e:I

    .line 502379
    :goto_0
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_4

    .line 502380
    iget-boolean v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->d:Z

    if-nez v2, :cond_1

    .line 502381
    iget-object v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->c:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_3

    .line 502382
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->d:Z

    .line 502383
    iget-object v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->b:Landroid/os/Handler;

    const p0, 0x4706d87c

    invoke-static {v2, v1, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 502384
    :cond_1
    :goto_1
    return-void

    .line 502385
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 502386
    :cond_3
    const-string v2, "MediaRouterJellybeanMr1"

    const-string p0, "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device."

    invoke-static {v2, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 502387
    :cond_4
    iget-boolean v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->d:Z

    if-eqz v2, :cond_1

    .line 502388
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->d:Z

    .line 502389
    iget-object v2, v1, Landroid/support/v7/media/MediaRouterJellybeanMr1$ActiveScanWorkaround;->b:Landroid/os/Handler;

    invoke-static {v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 502390
    new-instance v0, LX/3Fj;

    invoke-direct {v0, p0}, LX/3Fj;-><init>(LX/37y;)V

    move-object v0, v0

    .line 502391
    return-object v0
.end method
