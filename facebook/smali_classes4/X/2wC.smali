.class public final LX/2wC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:I

.field public c:I

.field public d:Z

.field public final synthetic e:LX/118;


# direct methods
.method public constructor <init>(LX/118;I)V
    .locals 1

    .prologue
    .line 478706
    iput-object p1, p0, LX/2wC;->e:LX/118;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478707
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wC;->d:Z

    .line 478708
    iput p2, p0, LX/2wC;->a:I

    .line 478709
    invoke-virtual {p1}, LX/118;->a()I

    move-result v0

    iput v0, p0, LX/2wC;->b:I

    .line 478710
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 478705
    iget v0, p0, LX/2wC;->c:I

    iget v1, p0, LX/2wC;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 478694
    iget-object v0, p0, LX/2wC;->e:LX/118;

    iget v1, p0, LX/2wC;->c:I

    iget v2, p0, LX/2wC;->a:I

    invoke-virtual {v0, v1, v2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 478695
    iget v1, p0, LX/2wC;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/2wC;->c:I

    .line 478696
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2wC;->d:Z

    .line 478697
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 478698
    iget-boolean v0, p0, LX/2wC;->d:Z

    if-nez v0, :cond_0

    .line 478699
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 478700
    :cond_0
    iget v0, p0, LX/2wC;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2wC;->c:I

    .line 478701
    iget v0, p0, LX/2wC;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2wC;->b:I

    .line 478702
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wC;->d:Z

    .line 478703
    iget-object v0, p0, LX/2wC;->e:LX/118;

    iget v1, p0, LX/2wC;->c:I

    invoke-virtual {v0, v1}, LX/118;->a(I)V

    .line 478704
    return-void
.end method
