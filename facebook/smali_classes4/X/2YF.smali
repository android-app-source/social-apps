.class public LX/2YF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/analytics2/logger/Uploader;

.field private final b:LX/0pD;

.field private final c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "LX/2Wd;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2WY;

.field private final e:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/analytics2/logger/Uploader;LX/0pD;Ljava/util/Iterator;LX/2WY;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V
    .locals 2
    .param p5    # Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics2/logger/Uploader;",
            "LX/0pD;",
            "Ljava/util/Iterator",
            "<",
            "LX/2Wd;",
            ">;",
            "LX/2WY;",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 420818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420819
    iput-object p1, p0, LX/2YF;->a:Lcom/facebook/analytics2/logger/Uploader;

    .line 420820
    iput-object p2, p0, LX/2YF;->b:LX/0pD;

    .line 420821
    iput-object p3, p0, LX/2YF;->c:Ljava/util/Iterator;

    .line 420822
    iput-object p4, p0, LX/2YF;->d:LX/2WY;

    .line 420823
    iput-object p5, p0, LX/2YF;->e:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    .line 420824
    iget-object v0, p0, LX/2YF;->c:Ljava/util/Iterator;

    if-nez v0, :cond_0

    .line 420825
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mBatchPayloadIterator is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420826
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 420827
    iget-object v0, p0, LX/2YF;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 420828
    invoke-virtual {p0}, LX/2YF;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420829
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No more batches to upload"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420830
    :cond_0
    iget-object v0, p0, LX/2YF;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Wd;

    .line 420831
    new-instance v1, LX/2Xd;

    iget-object v2, p0, LX/2YF;->b:LX/0pD;

    sget-object v3, LX/2YU;->DEFAULT:LX/2YU;

    invoke-direct {v1, v2, v3, v0}, LX/2Xd;-><init>(LX/0pD;LX/2YU;LX/2We;)V

    .line 420832
    iget-object v2, p0, LX/2YF;->a:Lcom/facebook/analytics2/logger/Uploader;

    new-instance v3, LX/2YV;

    iget-object v4, p0, LX/2YF;->e:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    iget-object v5, p0, LX/2YF;->d:LX/2WY;

    invoke-direct {v3, v0, v4, v5}, LX/2YV;-><init>(LX/2Wd;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;LX/2WY;)V

    invoke-interface {v2, v1, v3}, Lcom/facebook/analytics2/logger/Uploader;->a(LX/2Xd;LX/2YV;)V

    .line 420833
    return-void
.end method
