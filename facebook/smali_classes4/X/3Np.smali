.class public final enum LX/3Np;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Np;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Np;

.field public static final enum ANIMATE_IN:LX/3Np;

.field public static final enum ANIMATE_OUT:LX/3Np;

.field public static final enum HIDE:LX/3Np;

.field public static final enum SHOW:LX/3Np;

.field public static final enum WHATEVER:LX/3Np;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 559966
    new-instance v0, LX/3Np;

    const-string v1, "WHATEVER"

    invoke-direct {v0, v1, v2}, LX/3Np;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Np;->WHATEVER:LX/3Np;

    new-instance v0, LX/3Np;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v3}, LX/3Np;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Np;->SHOW:LX/3Np;

    new-instance v0, LX/3Np;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4}, LX/3Np;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Np;->HIDE:LX/3Np;

    new-instance v0, LX/3Np;

    const-string v1, "ANIMATE_IN"

    invoke-direct {v0, v1, v5}, LX/3Np;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Np;->ANIMATE_IN:LX/3Np;

    new-instance v0, LX/3Np;

    const-string v1, "ANIMATE_OUT"

    invoke-direct {v0, v1, v6}, LX/3Np;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Np;->ANIMATE_OUT:LX/3Np;

    .line 559967
    const/4 v0, 0x5

    new-array v0, v0, [LX/3Np;

    sget-object v1, LX/3Np;->WHATEVER:LX/3Np;

    aput-object v1, v0, v2

    sget-object v1, LX/3Np;->SHOW:LX/3Np;

    aput-object v1, v0, v3

    sget-object v1, LX/3Np;->HIDE:LX/3Np;

    aput-object v1, v0, v4

    sget-object v1, LX/3Np;->ANIMATE_IN:LX/3Np;

    aput-object v1, v0, v5

    sget-object v1, LX/3Np;->ANIMATE_OUT:LX/3Np;

    aput-object v1, v0, v6

    sput-object v0, LX/3Np;->$VALUES:[LX/3Np;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 559968
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Np;
    .locals 1

    .prologue
    .line 559969
    const-class v0, LX/3Np;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Np;

    return-object v0
.end method

.method public static values()[LX/3Np;
    .locals 1

    .prologue
    .line 559970
    sget-object v0, LX/3Np;->$VALUES:[LX/3Np;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Np;

    return-object v0
.end method
