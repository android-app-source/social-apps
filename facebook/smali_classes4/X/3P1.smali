.class public final enum LX/3P1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3P1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3P1;

.field public static final enum NORMAL:LX/3P1;

.field public static final enum UPPER_CASE:LX/3P1;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 561032
    new-instance v0, LX/3P1;

    const-string v1, "UPPER_CASE"

    invoke-direct {v0, v1, v2}, LX/3P1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3P1;->UPPER_CASE:LX/3P1;

    .line 561033
    new-instance v0, LX/3P1;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, LX/3P1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3P1;->NORMAL:LX/3P1;

    .line 561034
    const/4 v0, 0x2

    new-array v0, v0, [LX/3P1;

    sget-object v1, LX/3P1;->UPPER_CASE:LX/3P1;

    aput-object v1, v0, v2

    sget-object v1, LX/3P1;->NORMAL:LX/3P1;

    aput-object v1, v0, v3

    sput-object v0, LX/3P1;->$VALUES:[LX/3P1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 561031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3P1;
    .locals 1

    .prologue
    .line 561030
    const-class v0, LX/3P1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3P1;

    return-object v0
.end method

.method public static values()[LX/3P1;
    .locals 1

    .prologue
    .line 561029
    sget-object v0, LX/3P1;->$VALUES:[LX/3P1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3P1;

    return-object v0
.end method
