.class public final LX/3FN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/3Iv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final f:LX/04D;

.field public final g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final h:LX/2pa;

.field public final i:LX/2oO;

.field public final j:LX/2oL;

.field public final k:LX/093;

.field public l:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public final m:LX/3It;

.field public final n:LX/1bf;

.field public final o:LX/395;

.field public final p:LX/3FO;

.field public q:Z

.field public r:Z

.field public s:LX/0hE;

.field public t:LX/3Iw;

.field public u:LX/0iX;

.field public v:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ZLX/04D;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/2pa;LX/2oO;LX/2oL;LX/3It;LX/1bf;LX/395;LX/3FO;LX/3Iv;LX/0iX;Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Z",
            "LX/04D;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/2pa;",
            "LX/2oO;",
            "LX/2oL;",
            "LX/3It;",
            "LX/1bf;",
            "LX/395;",
            "LX/3FO;",
            "LX/3Iv;",
            "LX/0iX;",
            "Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;",
            ")V"
        }
    .end annotation

    .prologue
    .line 539481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539482
    new-instance v1, LX/093;

    invoke-direct {v1}, LX/093;-><init>()V

    iput-object v1, p0, LX/3FN;->k:LX/093;

    .line 539483
    iput-object p1, p0, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 539484
    iput-object p2, p0, LX/3FN;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 539485
    iput-object p3, p0, LX/3FN;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 539486
    iput-boolean p4, p0, LX/3FN;->d:Z

    .line 539487
    iput-object p5, p0, LX/3FN;->f:LX/04D;

    .line 539488
    iput-object p6, p0, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 539489
    iput-object p7, p0, LX/3FN;->h:LX/2pa;

    .line 539490
    iput-object p8, p0, LX/3FN;->i:LX/2oO;

    .line 539491
    iput-object p9, p0, LX/3FN;->j:LX/2oL;

    .line 539492
    iput-object p10, p0, LX/3FN;->m:LX/3It;

    .line 539493
    iput-object p11, p0, LX/3FN;->n:LX/1bf;

    .line 539494
    iput-object p12, p0, LX/3FN;->o:LX/395;

    .line 539495
    iput-object p13, p0, LX/3FN;->p:LX/3FO;

    .line 539496
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3FN;->a:LX/3Iv;

    .line 539497
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3FN;->u:LX/0iX;

    .line 539498
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3FN;->v:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    .line 539499
    return-void
.end method
