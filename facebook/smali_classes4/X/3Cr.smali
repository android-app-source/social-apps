.class public final LX/3Cr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAJ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAK;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAM;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAN;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAO;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAP;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BAQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 530864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530865
    iput-object v0, p0, LX/3Cr;->a:LX/0Ot;

    .line 530866
    iput-object v0, p0, LX/3Cr;->b:LX/0Ot;

    .line 530867
    iput-object v0, p0, LX/3Cr;->c:LX/0Ot;

    .line 530868
    iput-object v0, p0, LX/3Cr;->d:LX/0Ot;

    .line 530869
    iput-object v0, p0, LX/3Cr;->e:LX/0Ot;

    .line 530870
    iput-object v0, p0, LX/3Cr;->f:LX/0Ot;

    .line 530871
    iput-object v0, p0, LX/3Cr;->g:LX/0Ot;

    .line 530872
    iput-object v0, p0, LX/3Cr;->h:LX/0Ot;

    .line 530873
    iput-object v0, p0, LX/3Cr;->i:LX/0Ot;

    .line 530874
    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BAI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BAQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530876
    iput-object p1, p0, LX/3Cr;->a:LX/0Ot;

    .line 530877
    iput-object p2, p0, LX/3Cr;->b:LX/0Ot;

    .line 530878
    iput-object p3, p0, LX/3Cr;->c:LX/0Ot;

    .line 530879
    iput-object p4, p0, LX/3Cr;->d:LX/0Ot;

    .line 530880
    iput-object p5, p0, LX/3Cr;->e:LX/0Ot;

    .line 530881
    iput-object p6, p0, LX/3Cr;->f:LX/0Ot;

    .line 530882
    iput-object p7, p0, LX/3Cr;->g:LX/0Ot;

    .line 530883
    iput-object p8, p0, LX/3Cr;->h:LX/0Ot;

    .line 530884
    iput-object p9, p0, LX/3Cr;->i:LX/0Ot;

    .line 530885
    return-void
.end method

.method public static b(LX/0QB;)LX/3Cr;
    .locals 10

    .prologue
    .line 530886
    new-instance v0, LX/3Cr;

    const/16 v1, 0x2aba

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2abb

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2abc

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2abd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2abe

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2abf

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ac0

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ac1

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ac2

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/3Cr;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 530887
    return-object v0
.end method
