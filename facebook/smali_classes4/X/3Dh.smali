.class public final LX/3Dh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Di;


# instance fields
.field public final a:LX/2WF;

.field public final synthetic b:LX/1Lv;

.field private final c:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/video/server/FileResource$Reader;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/3Dg;

.field private final f:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/net/Uri;

.field private final h:I

.field private final i:I

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/7Q5;


# direct methods
.method public constructor <init>(LX/1Lv;LX/3Dg;LX/3Da;Landroid/net/Uri;IILjava/util/List;LX/2WF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Dg;",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;",
            "Landroid/net/Uri;",
            "II",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;",
            "LX/2WF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 535412
    iput-object p1, p0, LX/3Dh;->b:LX/1Lv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535413
    iput-object p2, p0, LX/3Dh;->e:LX/3Dg;

    .line 535414
    iput-object p3, p0, LX/3Dh;->f:LX/3Da;

    .line 535415
    iput-object p4, p0, LX/3Dh;->g:Landroid/net/Uri;

    .line 535416
    iput p5, p0, LX/3Dh;->h:I

    .line 535417
    iput p6, p0, LX/3Dh;->i:I

    .line 535418
    iput-object p7, p0, LX/3Dh;->j:Ljava/util/List;

    .line 535419
    iput-object p8, p0, LX/3Dh;->a:LX/2WF;

    .line 535420
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    .line 535421
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/3Dh;->d:Lcom/google/common/util/concurrent/SettableFuture;

    .line 535422
    return-void
.end method


# virtual methods
.method public final a()LX/3Da;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535423
    :try_start_0
    iget-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x6a20462c

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Da;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 535424
    :catch_0
    move-exception v0

    .line 535425
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-class v2, Ljava/io/IOException;

    invoke-static {v1, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 535426
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error waiting for result"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 535427
    :catch_1
    move-exception v0

    .line 535428
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 535429
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error waiting for result"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Z)LX/7Ou;
    .locals 4

    .prologue
    .line 535430
    :try_start_0
    iget-object v0, p0, LX/3Dh;->d:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x4582264d

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Os;

    .line 535431
    iget-object v1, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/7Q6;

    invoke-direct {v2}, LX/7Q6;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 535432
    if-eqz p1, :cond_0

    .line 535433
    iget-object v1, p0, LX/3Dh;->k:LX/7Q5;

    .line 535434
    iget-object v2, v1, LX/7Q5;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 535435
    iget-object v1, v0, LX/7Os;->b:Ljava/io/InputStream;

    move-object v0, v1

    .line 535436
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 535437
    const/4 v0, 0x0

    .line 535438
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/7Ou;

    iget-object v2, p0, LX/3Dh;->a:LX/2WF;

    invoke-direct {v1, v2, v0}, LX/7Ou;-><init>(LX/2WF;LX/7Os;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 535439
    :catch_0
    move-exception v0

    .line 535440
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-class v2, Ljava/io/IOException;

    invoke-static {v1, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 535441
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error waiting for result"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 535442
    :catch_1
    move-exception v0

    .line 535443
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 535444
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error waiting for result"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 6

    .prologue
    .line 535445
    :try_start_0
    iget-object v0, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 535446
    :try_start_1
    iget-object v0, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, p0, LX/3Dh;->g:Landroid/net/Uri;

    .line 535447
    iget-object v2, v0, LX/1Lv;->f:LX/1Lg;

    invoke-virtual {v2, v1}, LX/1Lg;->b(Landroid/net/Uri;)Z

    move-result v2

    move v0, v2

    .line 535448
    if-eqz v0, :cond_0

    .line 535449
    iget-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/io/InterruptedIOException;

    const-string v2, "cancelled"

    invoke-direct {v1, v2}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 535450
    :cond_0
    iget-object v0, p0, LX/3Dh;->e:LX/3Dg;

    invoke-virtual {v0, p1}, LX/3Dg;->a(LX/3Dd;)Ljava/io/OutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 535451
    :try_start_2
    iget-object v1, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 535452
    iget-object v1, p0, LX/3Dh;->f:LX/3Da;

    if-nez v1, :cond_1

    .line 535453
    iget-object v1, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->u:LX/16V;

    new-instance v2, LX/373;

    iget v3, p0, LX/3Dh;->h:I

    iget v4, p0, LX/3Dh;->i:I

    iget-object v5, p0, LX/3Dh;->j:Ljava/util/List;

    invoke-direct {v2, v3, v4, v5}, LX/373;-><init>(IILjava/util/List;)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 535454
    iget-object v1, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->u:LX/16V;

    new-instance v2, LX/370;

    iget v3, p0, LX/3Dh;->h:I

    iget-object v4, p0, LX/3Dh;->a:LX/2WF;

    invoke-direct {v2, v3, v4}, LX/370;-><init>(ILX/2WF;)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 535455
    :cond_1
    new-instance v1, LX/7Q5;

    invoke-direct {v1, v0}, LX/7Q5;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, LX/3Dh;->k:LX/7Q5;

    .line 535456
    iget-object v0, p0, LX/3Dh;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/3Dh;->e:LX/3Dg;

    .line 535457
    iget-object v2, v1, LX/3Dg;->m:LX/7Os;

    move-object v1, v2

    .line 535458
    const v2, -0x5180ca54

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 535459
    iget-object v0, p0, LX/3Dh;->k:LX/7Q5;

    .line 535460
    :goto_0
    return-object v0

    .line 535461
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 535462
    :catch_0
    move-exception v0

    .line 535463
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 535464
    iget-object v1, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 535465
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 535466
    iget-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 535467
    return-void
.end method

.method public final a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    .locals 6

    .prologue
    .line 535468
    move-object v0, p1

    check-cast v0, LX/7Q5;

    .line 535469
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 535470
    :goto_0
    iget-object v1, p0, LX/3Dh;->b:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->u:LX/16V;

    new-instance v2, LX/371;

    iget v3, p0, LX/3Dh;->h:I

    .line 535471
    iget v4, v0, LX/7Q5;->b:I

    move v4, v4

    .line 535472
    int-to-long v4, v4

    .line 535473
    iget-object p1, v0, LX/7Q5;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    move v0, p1

    .line 535474
    invoke-direct {v2, v3, v4, v5, v0}, LX/371;-><init>(IJZ)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 535475
    if-eqz p2, :cond_0

    .line 535476
    iget-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 535477
    :goto_1
    return-void

    .line 535478
    :catch_0
    move-exception v1

    .line 535479
    sget-object v2, LX/1Lv;->a:Ljava/lang/String;

    const-string v3, "Error closing prefetch writing stream"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 535480
    :cond_0
    iget-object v0, p0, LX/3Dh;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/3Dh;->e:LX/3Dg;

    .line 535481
    iget-object v2, v1, LX/3Dg;->f:LX/3Da;

    move-object v1, v2

    .line 535482
    const v2, 0xef94e3a

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_1
.end method
