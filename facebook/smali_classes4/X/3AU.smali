.class public abstract LX/3AU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 525295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;)J
    .locals 4

    .prologue
    .line 525296
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 525297
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 525298
    :try_start_0
    invoke-virtual {p0}, LX/3AU;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 525299
    invoke-static {p1, v0}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v2

    .line 525300
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525301
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-wide v2

    .line 525302
    :catch_0
    move-exception v0

    .line 525303
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525304
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method

.method public abstract a()Ljava/io/OutputStream;
.end method

.method public final a([B)V
    .locals 2

    .prologue
    .line 525305
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 525306
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 525307
    :try_start_0
    invoke-virtual {p0}, LX/3AU;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 525308
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 525309
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525310
    invoke-virtual {v1}, LX/1vJ;->close()V

    .line 525311
    return-void

    .line 525312
    :catch_0
    move-exception v0

    .line 525313
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525314
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method
