.class public final LX/2cy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0bV;


# instance fields
.field private final a:LX/2cv;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2d7;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0SG;


# direct methods
.method public constructor <init>(LX/2cv;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/objects/FbSharedObjectPreferences;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2d7;",
            ">;>;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442584
    iput-object p1, p0, LX/2cy;->a:LX/2cv;

    .line 442585
    iput-object p2, p0, LX/2cy;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 442586
    iput-object p3, p0, LX/2cy;->c:LX/0Or;

    .line 442587
    iput-object p4, p0, LX/2cy;->d:LX/0SG;

    .line 442588
    return-void
.end method

.method public static b(LX/0QB;)LX/2cy;
    .locals 5

    .prologue
    .line 442563
    new-instance v3, LX/2cy;

    invoke-static {p0}, LX/2cv;->b(LX/0QB;)LX/2cv;

    move-result-object v0

    check-cast v0, LX/2cv;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 442564
    new-instance v2, LX/2cz;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v4

    invoke-direct {v2, v4}, LX/2cz;-><init>(LX/0QB;)V

    move-object v4, v2

    .line 442565
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-direct {v3, v0, v1, v4, v2}, LX/2cy;-><init>(LX/2cv;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;)V

    .line 442566
    return-object v3
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 442554
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "------------GRAVITY SETTINGS-------------\n\tSETTINGS OBJECT (from graphql)"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 442555
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v1

    .line 442556
    if-nez v1, :cond_0

    .line 442557
    const-string v1, "\nSettings object is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442558
    :goto_0
    const-string v1, "\n\tSETTINGS LOGIC\nisGravityFeatureEnabled(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/2cy;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nisGravityNotificationsEnabled(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 442559
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v2

    invoke-static {v2}, LX/2dT;->b(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v2

    move v2, v2

    .line 442560
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nisGravityBackgroundScanningEnabled(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/2cy;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 442561
    return-object v0

    .line 442562
    :cond_0
    const-string v2, "\nis_feature_enabled: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nlocation_tracking_enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nnotifications_enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nlearn_more_link: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V
    .locals 5
    .param p1    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442567
    if-nez p1, :cond_1

    .line 442568
    :cond_0
    return-void

    .line 442569
    :cond_1
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v1

    .line 442570
    instance-of v0, p1, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 442571
    :goto_0
    iget-object v2, p0, LX/2cy;->a:LX/2cv;

    invoke-virtual {v2}, LX/2cv;->a()LX/3C9;

    move-result-object v2

    sget-object v3, LX/2d4;->b:LX/2d5;

    invoke-interface {v2, v3, v0}, LX/3C9;->a(LX/2d5;Ljava/lang/Object;)LX/3C9;

    move-result-object v0

    invoke-interface {v0}, LX/3C9;->a()V

    .line 442572
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 442573
    if-ne v1, p1, :cond_4

    .line 442574
    :cond_2
    :goto_1
    move v0, v0

    .line 442575
    if-nez v0, :cond_0

    .line 442576
    iget-object v0, p0, LX/2cy;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 442577
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d7;

    .line 442578
    invoke-interface {v0, v1, p1}, LX/2d7;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    goto :goto_2

    .line 442579
    :cond_3
    invoke-static {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    goto :goto_0

    .line 442580
    :cond_4
    if-eqz v1, :cond_5

    if-nez p1, :cond_6

    :cond_5
    move v0, v2

    .line 442581
    goto :goto_1

    .line 442582
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->c()Z

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->c()Z

    move-result v4

    if-ne v3, v4, :cond_7

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a()Z

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a()Z

    move-result v4

    if-ne v3, v4, :cond_7

    invoke-virtual {v1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->d()Z

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->d()Z

    move-result v4

    if-eq v3, v4, :cond_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public final b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 442553
    iget-object v0, p0, LX/2cy;->a:LX/2cv;

    sget-object v1, LX/2d4;->b:LX/2d5;

    invoke-virtual {v0, v1}, LX/2cv;->a(LX/2d5;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 442550
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/2dT;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 442552
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/2dT;->b(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 442551
    invoke-virtual {p0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/2dT;->c(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    return v0
.end method
