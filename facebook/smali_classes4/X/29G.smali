.class public final LX/29G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/29A;


# direct methods
.method public constructor <init>(LX/29A;)V
    .locals 0

    .prologue
    .line 376068
    iput-object p1, p0, LX/29G;->a:LX/29A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 376069
    iget-object v0, p0, LX/29G;->a:LX/29A;

    iget-object v0, v0, LX/29A;->p:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 376070
    iget-object v2, p0, LX/29G;->a:LX/29A;

    iget-object v2, v2, LX/29A;->o:LX/0ZR;

    new-instance v3, LX/2Yu;

    const-string v4, "ServiceConnected (MqttPushServiceManager)"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {v3, v0, v1, v4, v5}, LX/2Yu;-><init>(JLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, LX/0ZK;->a(LX/0ki;)V

    .line 376071
    iget-object v0, p0, LX/29G;->a:LX/29A;

    invoke-static {v0, p2}, LX/29A;->a$redex0(LX/29A;Landroid/os/IBinder;)V

    .line 376072
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6

    .prologue
    .line 376073
    iget-object v0, p0, LX/29G;->a:LX/29A;

    iget-object v0, v0, LX/29A;->p:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 376074
    iget-object v2, p0, LX/29G;->a:LX/29A;

    iget-object v2, v2, LX/29A;->o:LX/0ZR;

    new-instance v3, LX/2Yu;

    const-string v4, "ServiceDisconnected (MqttPushServiceManager)"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {v3, v0, v1, v4, v5}, LX/2Yu;-><init>(JLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, LX/0ZK;->a(LX/0ki;)V

    .line 376075
    iget-object v0, p0, LX/29G;->a:LX/29A;

    invoke-static {v0}, LX/29A;->v(LX/29A;)V

    .line 376076
    return-void
.end method
