.class public LX/3QI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3QJ;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/3QI;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Mk;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0aG;

.field public final e:LX/0Uq;

.field private final f:LX/3QK;

.field public final g:LX/13O;

.field public final h:LX/3QL;

.field public final i:LX/3QM;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0aG;LX/0Uq;LX/3QK;LX/13O;LX/3QL;LX/3QM;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Mk;",
            ">;",
            "LX/0aG;",
            "LX/0Uq;",
            "LX/3QK;",
            "LX/13O;",
            "LX/3QL;",
            "LX/3QM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564042
    iput-object p2, p0, LX/3QI;->b:LX/0Or;

    .line 564043
    iput-object p3, p0, LX/3QI;->c:LX/0Or;

    .line 564044
    iput-object p4, p0, LX/3QI;->d:LX/0aG;

    .line 564045
    iput-object p5, p0, LX/3QI;->e:LX/0Uq;

    .line 564046
    iput-object p6, p0, LX/3QI;->f:LX/3QK;

    .line 564047
    iput-object p7, p0, LX/3QI;->g:LX/13O;

    .line 564048
    iput-object p8, p0, LX/3QI;->h:LX/3QL;

    .line 564049
    iput-object p9, p0, LX/3QI;->i:LX/3QM;

    .line 564050
    iput-object p1, p0, LX/3QI;->a:LX/0Or;

    .line 564051
    return-void
.end method

.method public static a(LX/0QB;)LX/3QI;
    .locals 13

    .prologue
    .line 564004
    sget-object v0, LX/3QI;->j:LX/3QI;

    if-nez v0, :cond_1

    .line 564005
    const-class v1, LX/3QI;

    monitor-enter v1

    .line 564006
    :try_start_0
    sget-object v0, LX/3QI;->j:LX/3QI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 564007
    if-eqz v2, :cond_0

    .line 564008
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 564009
    new-instance v3, LX/3QI;

    const/16 v4, 0x15e8

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x2847

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xd66

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v8

    check-cast v8, LX/0Uq;

    invoke-static {v0}, LX/3QK;->a(LX/0QB;)LX/3QK;

    move-result-object v9

    check-cast v9, LX/3QK;

    invoke-static {v0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v10

    check-cast v10, LX/13O;

    invoke-static {v0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v11

    check-cast v11, LX/3QL;

    invoke-static {v0}, LX/3QM;->b(LX/0QB;)LX/3QM;

    move-result-object v12

    check-cast v12, LX/3QM;

    invoke-direct/range {v3 .. v12}, LX/3QI;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0aG;LX/0Uq;LX/3QK;LX/13O;LX/3QL;LX/3QM;)V

    .line 564010
    move-object v0, v3

    .line 564011
    sput-object v0, LX/3QI;->j:LX/3QI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564012
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 564013
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 564014
    :cond_1
    sget-object v0, LX/3QI;->j:LX/3QI;

    return-object v0

    .line 564015
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 564016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3QI;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 564052
    iget-object v0, p0, LX/3QI;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 12

    .prologue
    .line 564017
    iget-object v0, p0, LX/3QI;->e:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 564018
    iget-object v0, p0, LX/3QI;->h:LX/3QL;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v4, v4, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v4}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    .line 564019
    iget-object v5, v0, LX/3QL;->g:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 564020
    :goto_0
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 564021
    iget-object v5, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {p0, v5}, LX/3QI;->b(LX/3QI;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 564022
    iget-object v5, p0, LX/3QI;->h:LX/3QL;

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v7, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v8, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v8, v8, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v8}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v9, v9, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    const-string v10, "from_viewer"

    invoke-virtual/range {v5 .. v10}, LX/3QL;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564023
    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 564024
    invoke-static {p0, v0}, LX/3QI;->b(LX/3QI;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 564025
    sget-object v5, LX/77X;->MESSAGE_RECEIVED:LX/77X;

    if-eqz v5, :cond_1

    .line 564026
    iget-object v5, p0, LX/3QI;->g:LX/13O;

    sget-object v6, LX/77X;->MESSAGE_RECEIVED:LX/77X;

    invoke-virtual {v6}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/13O;->b(Ljava/lang/String;)V

    .line 564027
    if-eqz v0, :cond_1

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v5, v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v6, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v5, v6, :cond_1

    .line 564028
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v5, v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 564029
    iget-object v7, p0, LX/3QI;->i:LX/3QM;

    invoke-virtual {v7, v5, v6}, LX/3QM;->a(J)V

    .line 564030
    :cond_1
    iget-object v0, p0, LX/3QI;->f:LX/3QK;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v1}, LX/3QK;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 564031
    :cond_2
    return-void

    .line 564032
    :cond_3
    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "source"

    aput-object v8, v5, v6

    const/4 v6, 0x1

    aput-object v4, v5, v6

    const/4 v6, 0x2

    const-string v8, "message_id"

    aput-object v8, v5, v6

    const/4 v6, 0x3

    aput-object v1, v5, v6

    invoke-static {v5}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v8

    .line 564033
    invoke-static {v8, v2}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564034
    if-eqz v3, :cond_4

    .line 564035
    const-string v5, "offline_threading_id"

    invoke-interface {v8, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564036
    :cond_4
    iget-object v5, v0, LX/3QL;->f:LX/2bC;

    const-string v6, "messaging_received"

    move-object v9, v7

    move-object v10, v7

    move-object v11, v7

    invoke-virtual/range {v5 .. v11}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 564037
    :cond_5
    iget-object v5, p0, LX/3QI;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v5, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 564038
    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->OUTGOING_CALL:LX/2uW;

    if-eq v6, v7, :cond_6

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->INCOMING_CALL:LX/2uW;

    if-eq v6, v7, :cond_6

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->MISSED_CALL:LX/2uW;

    if-eq v6, v7, :cond_6

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->VIDEO_CALL:LX/2uW;

    if-eq v6, v7, :cond_6

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    if-eq v6, v7, :cond_6

    iget-object v6, v5, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v7, LX/2uW;->CALL_LOG:LX/2uW;

    if-ne v6, v7, :cond_8

    :cond_6
    const/4 v6, 0x1

    :goto_2
    move v5, v6

    .line 564039
    if-eqz v5, :cond_7

    iget-object v5, p0, LX/3QI;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Mk;

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v5, v6}, LX/2Mk;->l(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 564040
    :cond_7
    iget-object v5, p0, LX/3QI;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Di5;

    invoke-virtual {v5, p1}, LX/Di5;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto/16 :goto_1

    :cond_8
    const/4 v6, 0x0

    goto :goto_2
.end method
