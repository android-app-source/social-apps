.class public final LX/2tX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:LX/1S9;

.field public final c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

.field public d:LX/328;


# direct methods
.method public constructor <init>(LX/1S9;Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;LX/328;)V
    .locals 2

    .prologue
    .line 475092
    iput-object p1, p0, LX/2tX;->b:LX/1S9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475093
    iput-object p2, p0, LX/2tX;->c:Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    .line 475094
    iput-object p3, p0, LX/2tX;->d:LX/328;

    .line 475095
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2tX;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 475096
    return-void
.end method


# virtual methods
.method public final a(LX/328;)V
    .locals 3

    .prologue
    .line 475097
    iget-object v1, p0, LX/2tX;->d:LX/328;

    .line 475098
    iput-object p1, p0, LX/2tX;->d:LX/328;

    .line 475099
    iget-object v0, p0, LX/2tX;->b:LX/1S9;

    iget-object v0, v0, LX/1S9;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SA;

    .line 475100
    invoke-virtual {v0}, LX/1SA;->a()V

    goto :goto_0

    .line 475101
    :cond_0
    sget-object v0, LX/328;->UNANSWERED:LX/328;

    invoke-virtual {v0, v1}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/328;->ANSWERED_YES:LX/328;

    invoke-virtual {v0, p1}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/328;->ANSWERED_NO:LX/328;

    invoke-virtual {v0, p1}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/328;->DISMISSED:LX/328;

    invoke-virtual {v0, p1}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 475102
    :cond_1
    iget-object v0, p0, LX/2tX;->b:LX/1S9;

    iget-object v0, v0, LX/1S9;->c:LX/1S5;

    .line 475103
    iget-object v1, v0, LX/1S5;->b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/DCe;

    invoke-direct {v2, v0, p0}, LX/DCe;-><init>(LX/1S5;LX/2tX;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 475104
    :cond_2
    return-void
.end method
