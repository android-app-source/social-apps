.class public LX/30E;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/TaskQueueFactoryHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/TaskQueueFactoryHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 484115
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;
    .locals 3

    .prologue
    .line 484116
    sget-object v0, LX/30E;->a:Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    if-nez v0, :cond_1

    .line 484117
    const-class v1, LX/30E;

    monitor-enter v1

    .line 484118
    :try_start_0
    sget-object v0, LX/30E;->a:Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 484119
    if-eqz v2, :cond_0

    .line 484120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 484121
    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/2FB;->a(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object p0

    move-object v0, p0

    .line 484122
    sput-object v0, LX/30E;->a:Lcom/facebook/compactdisk/TaskQueueFactoryHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 484123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 484124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 484125
    :cond_1
    sget-object v0, LX/30E;->a:Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    return-object v0

    .line 484126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 484127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 484128
    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/2FB;->a(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object v0

    return-object v0
.end method
