.class public final LX/32e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final c:LX/32e;


# instance fields
.field public final a:LX/24P;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 490792
    new-instance v0, LX/32e;

    sget-object v1, LX/24P;->MAXIMIZED:LX/24P;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/32e;-><init>(LX/24P;Z)V

    sput-object v0, LX/32e;->c:LX/32e;

    return-void
.end method

.method public constructor <init>(LX/24P;Z)V
    .locals 0

    .prologue
    .line 490793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490794
    iput-object p1, p0, LX/32e;->a:LX/24P;

    .line 490795
    iput-boolean p2, p0, LX/32e;->b:Z

    .line 490796
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 490786
    if-ne p0, p1, :cond_1

    .line 490787
    :cond_0
    :goto_0
    return v0

    .line 490788
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 490789
    goto :goto_0

    .line 490790
    :cond_3
    check-cast p1, LX/32e;

    .line 490791
    iget-object v2, p0, LX/32e;->a:LX/24P;

    iget-object v3, p1, LX/32e;->a:LX/24P;

    invoke-virtual {v2, v3}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/32e;->b:Z

    iget-boolean v3, p1, LX/32e;->b:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 490782
    iget-object v0, p0, LX/32e;->a:LX/24P;

    invoke-virtual {v0}, LX/24P;->hashCode()I

    move-result v0

    .line 490783
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, LX/32e;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 490784
    return v0

    .line 490785
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
