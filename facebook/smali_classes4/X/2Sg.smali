.class public LX/2Sg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/2Sh;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/2Sh;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final c:LX/11o;

.field private static volatile j:LX/2Sg;


# instance fields
.field public d:Z

.field public final e:LX/11i;

.field public final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final g:LX/0Uh;

.field public h:LX/2Si;

.field public i:LX/7BE;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const v2, 0x70015

    .line 413104
    new-instance v0, LX/2Sh;

    const-string v1, "NullStatePerfLogger"

    invoke-direct {v0, v2, v1}, LX/2Sh;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/2Sg;->a:LX/2Sh;

    .line 413105
    new-instance v0, LX/2Sh;

    const-string v1, "GraphSearchNullStatePerfLogger"

    invoke-direct {v0, v2, v1}, LX/2Sh;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/2Sg;->b:LX/2Sh;

    .line 413106
    new-instance v0, LX/11n;

    invoke-direct {v0}, LX/11n;-><init>()V

    sput-object v0, LX/2Sg;->c:LX/11o;

    return-void
.end method

.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413108
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Sg;->d:Z

    .line 413109
    sget-object v0, LX/2Si;->NONE:LX/2Si;

    iput-object v0, p0, LX/2Sg;->h:LX/2Si;

    .line 413110
    iput-object p1, p0, LX/2Sg;->e:LX/11i;

    .line 413111
    iput-object p2, p0, LX/2Sg;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 413112
    iput-object p3, p0, LX/2Sg;->g:LX/0Uh;

    .line 413113
    return-void
.end method

.method public static a(LX/2Sg;LX/2Si;)LX/11o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Si;",
            ")",
            "LX/11o",
            "<",
            "LX/2Sh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413127
    iget-object v0, p0, LX/2Sg;->e:LX/11i;

    invoke-static {p0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 413128
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/2Sg;->h:LX/2Si;

    sget-object v2, LX/2Si;->COLD_START:LX/2Si;

    if-ne v1, v2, :cond_0

    .line 413129
    :goto_0
    return-object v0

    .line 413130
    :cond_0
    invoke-static {p0}, LX/2Sg;->k(LX/2Sg;)V

    .line 413131
    invoke-static {p0, p1}, LX/2Sg;->b(LX/2Sg;LX/2Si;)LX/11o;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Sg;
    .locals 6

    .prologue
    .line 413114
    sget-object v0, LX/2Sg;->j:LX/2Sg;

    if-nez v0, :cond_1

    .line 413115
    const-class v1, LX/2Sg;

    monitor-enter v1

    .line 413116
    :try_start_0
    sget-object v0, LX/2Sg;->j:LX/2Sg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413117
    if-eqz v2, :cond_0

    .line 413118
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413119
    new-instance p0, LX/2Sg;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/2Sg;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;)V

    .line 413120
    move-object v0, p0

    .line 413121
    sput-object v0, LX/2Sg;->j:LX/2Sg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413122
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413123
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413124
    :cond_1
    sget-object v0, LX/2Sg;->j:LX/2Sg;

    return-object v0

    .line 413125
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413126
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/11o;LX/Cvr;)V
    .locals 4

    .prologue
    .line 413098
    const-string v0, "end_to_end"

    invoke-interface {p0, v0}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 413099
    const-string v0, "end_to_end"

    const/4 v1, 0x0

    const-string v2, "entry_action"

    invoke-virtual {p1}, LX/Cvr;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    const v3, -0x6a394c12

    invoke-static {p0, v0, v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 413100
    :cond_0
    return-void
.end method

.method public static b(LX/2Sg;LX/2Si;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Si;",
            ")",
            "LX/11o",
            "<",
            "LX/2Sh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413101
    iput-object p1, p0, LX/2Sg;->h:LX/2Si;

    .line 413102
    iget-object v0, p0, LX/2Sg;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x70015

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 413103
    iget-object v0, p0, LX/2Sg;->e:LX/11i;

    invoke-static {p0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/2Sg;Ljava/lang/String;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<",
            "LX/2Sh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413086
    iget-object v0, p0, LX/2Sg;->e:LX/11i;

    invoke-static {p0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 413087
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2Sg;->c:LX/11o;

    goto :goto_0
.end method

.method public static i(LX/2Sg;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/2Sh;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 413088
    iget-object v0, p0, LX/2Sg;->h:LX/2Si;

    sget-object v1, LX/2Si;->COLD_START:LX/2Si;

    if-ne v0, v1, :cond_0

    .line 413089
    iget-object v0, p0, LX/2Sg;->e:LX/11i;

    invoke-static {p0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 413090
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(LX/2Sg;)V
    .locals 3

    .prologue
    .line 413091
    iget-object v0, p0, LX/2Sg;->e:LX/11i;

    invoke-static {p0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->d(LX/0Pq;)V

    .line 413092
    iget-object v0, p0, LX/2Sg;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x70015

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 413093
    sget-object v0, LX/2Si;->NONE:LX/2Si;

    iput-object v0, p0, LX/2Sg;->h:LX/2Si;

    .line 413094
    return-void
.end method

.method public static l(LX/2Sg;)LX/2Sh;
    .locals 2

    .prologue
    .line 413095
    iget-object v0, p0, LX/2Sg;->g:LX/0Uh;

    sget v1, LX/2SU;->e:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413096
    sget-object v0, LX/2Sg;->b:LX/2Sh;

    .line 413097
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2Sg;->a:LX/2Sh;

    goto :goto_0
.end method
