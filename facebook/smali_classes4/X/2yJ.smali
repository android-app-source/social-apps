.class public LX/2yJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static q:LX/0Xm;


# instance fields
.field public final b:LX/17V;

.field public final c:LX/2mt;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xP;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Zb;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1Ay;

.field private final h:LX/0bH;

.field public final i:LX/17Q;

.field private final j:LX/03V;

.field private final k:LX/1CK;

.field private final l:LX/16I;

.field private final m:LX/0SG;

.field private final n:LX/1EQ;

.field private final o:LX/2yK;

.field private final p:LX/1Um;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481513
    const-class v0, LX/2yJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2yJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/17V;LX/17Q;LX/2mt;LX/0Ot;LX/0Zb;LX/0Ot;LX/1Ay;LX/0bH;LX/03V;LX/16I;LX/0SG;LX/1CK;LX/1EQ;LX/2yK;LX/1Um;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17V;",
            "LX/17Q;",
            "LX/2mt;",
            "LX/0Ot",
            "<",
            "LX/1xP;",
            ">;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/1Ay;",
            "LX/0bH;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/16I;",
            "LX/0SG;",
            "LX/1CK;",
            "LX/1EQ;",
            "LX/2yK;",
            "LX/1Um;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481600
    iput-object p3, p0, LX/2yJ;->c:LX/2mt;

    .line 481601
    iput-object p4, p0, LX/2yJ;->d:LX/0Ot;

    .line 481602
    iput-object p5, p0, LX/2yJ;->e:LX/0Zb;

    .line 481603
    iput-object p6, p0, LX/2yJ;->f:LX/0Ot;

    .line 481604
    iput-object p7, p0, LX/2yJ;->g:LX/1Ay;

    .line 481605
    iput-object p8, p0, LX/2yJ;->h:LX/0bH;

    .line 481606
    iput-object p9, p0, LX/2yJ;->j:LX/03V;

    .line 481607
    iput-object p11, p0, LX/2yJ;->m:LX/0SG;

    .line 481608
    iput-object p10, p0, LX/2yJ;->l:LX/16I;

    .line 481609
    iput-object p12, p0, LX/2yJ;->k:LX/1CK;

    .line 481610
    iput-object p1, p0, LX/2yJ;->b:LX/17V;

    .line 481611
    iput-object p2, p0, LX/2yJ;->i:LX/17Q;

    .line 481612
    iput-object p13, p0, LX/2yJ;->n:LX/1EQ;

    .line 481613
    iput-object p14, p0, LX/2yJ;->o:LX/2yK;

    .line 481614
    iput-object p15, p0, LX/2yJ;->p:LX/1Um;

    .line 481615
    return-void
.end method

.method public static a(LX/0QB;)LX/2yJ;
    .locals 3

    .prologue
    .line 481591
    const-class v1, LX/2yJ;

    monitor-enter v1

    .line 481592
    :try_start_0
    sget-object v0, LX/2yJ;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481593
    sput-object v2, LX/2yJ;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481594
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481595
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2yJ;->b(LX/0QB;)LX/2yJ;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481596
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2yJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481597
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/Object;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 481581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 481582
    if-eqz p2, :cond_0

    .line 481583
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481584
    :cond_0
    instance-of v1, p0, LX/1Po;

    if-eqz v1, :cond_1

    .line 481585
    check-cast p0, LX/1Po;

    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 481586
    if-eqz v1, :cond_1

    .line 481587
    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481588
    :cond_1
    invoke-static {p1}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 481589
    const-string v1, "_ads"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481590
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 481576
    invoke-static {p0}, LX/2mt;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 481577
    :goto_0
    return-object v0

    .line 481578
    :cond_0
    invoke-static {p0}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 481579
    if-eqz v1, :cond_1

    invoke-static {v1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 481580
    :cond_1
    const-string v1, "native"

    invoke-static {p0}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {p1, v1, v2, v0}, LX/17Q;->b(Ljava/lang/String;Ljava/lang/String;ZLX/0lF;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 481575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1ZS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1ZS;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 481570
    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 481571
    if-nez p0, :cond_0

    .line 481572
    :goto_0
    return-object v0

    .line 481573
    :cond_0
    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eq v1, v2, :cond_1

    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 481574
    :cond_1
    new-instance v1, LX/1ZS;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/1ZS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2yJ;
    .locals 17

    .prologue
    .line 481568
    new-instance v1, LX/2yJ;

    invoke-static/range {p0 .. p0}, LX/17V;->a(LX/0QB;)LX/17V;

    move-result-object v2

    check-cast v2, LX/17V;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v3

    check-cast v3, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v4

    check-cast v4, LX/2mt;

    const/16 v5, 0xc7b

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v7, 0x97

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v8

    check-cast v8, LX/1Ay;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static/range {p0 .. p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v11

    check-cast v11, LX/16I;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v13

    check-cast v13, LX/1CK;

    invoke-static/range {p0 .. p0}, LX/1EQ;->a(LX/0QB;)LX/1EQ;

    move-result-object v14

    check-cast v14, LX/1EQ;

    invoke-static/range {p0 .. p0}, LX/2yK;->a(LX/0QB;)LX/2yK;

    move-result-object v15

    check-cast v15, LX/2yK;

    invoke-static/range {p0 .. p0}, LX/1Um;->a(LX/0QB;)LX/1Um;

    move-result-object v16

    check-cast v16, LX/1Um;

    invoke-direct/range {v1 .. v16}, LX/2yJ;-><init>(LX/17V;LX/17Q;LX/2mt;LX/0Ot;LX/0Zb;LX/0Ot;LX/1Ay;LX/0bH;LX/03V;LX/16I;LX/0SG;LX/1CK;LX/1EQ;LX/2yK;LX/1Um;)V

    .line 481569
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 481557
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481558
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481559
    invoke-static {p1}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 481560
    if-nez v1, :cond_0

    .line 481561
    const-string v2, "%s parent:%s, url:%s mediaUrl:%s title:%s subtitle:%s dedupkey:%s"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, LX/2yJ;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    aput-object v1, v3, v6

    const/4 v1, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    invoke-static {v0}, LX/1VO;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x5

    invoke-static {v0}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NPE of attachment story"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 481562
    iput-boolean v6, v0, LX/0VK;->f:Z

    .line 481563
    move-object v0, v0

    .line 481564
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 481565
    iget-object v1, p0, LX/2yJ;->j:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 481566
    const/4 v0, 0x0

    .line 481567
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/2yN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 481556
    new-instance v0, LX/2yM;

    invoke-direct {v0, p0}, LX/2yM;-><init>(LX/2yJ;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;Ljava/lang/String;LX/2yV;LX/2yi;ZLX/1Pq;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ":",
            "LX/1Pr;",
            ">(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;",
            "Ljava/lang/String;",
            "LX/2yV;",
            "LX/2yi;",
            "ZTE;)V"
        }
    .end annotation

    .prologue
    .line 481514
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481515
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, LX/2yJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 481516
    invoke-static {v10}, LX/2mt;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "open_graph"

    move-object v13, v4

    .line 481517
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gh;

    const-string v6, "tap_story_attachment"

    invoke-virtual {v4, v6}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 481518
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-interface {v0, v1, v2}, LX/2yN;->a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v14

    .line 481519
    invoke-static/range {p2 .. p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v15

    .line 481520
    invoke-virtual {v15}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lcom/facebook/graphql/model/GraphQLStory;

    .line 481521
    invoke-static {v15}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->p:LX/1Um;

    invoke-virtual {v4}, LX/1Um;->a()Ljava/lang/String;

    move-result-object v9

    .line 481522
    :goto_1
    if-eqz v14, :cond_0

    if-eqz v9, :cond_0

    .line 481523
    const-string v4, "browser_metrics_join_key"

    invoke-virtual {v14, v4, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 481524
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->k:LX/1CK;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-virtual {v4, v11, v0, v6}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 481525
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, LX/1xP;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1xP;

    const/4 v6, 0x0

    move-object/from16 v0, p8

    invoke-static {v0, v15, v6}, LX/2yJ;->a(Ljava/lang/Object;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    invoke-virtual/range {v4 .. v9}, LX/1xP;->a(LX/162;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    move-object/from16 v1, p2

    invoke-static {v1, v13}, LX/2yJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v12, v0, v1, v4, v6}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 481526
    if-eqz v11, :cond_1

    if-eqz v14, :cond_1

    .line 481527
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->n:LX/1EQ;

    invoke-virtual {v4, v14, v11}, LX/1EQ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 481528
    :cond_1
    invoke-static {v14}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 481529
    move-object/from16 v0, p1

    invoke-static {v14, v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 481530
    :cond_2
    if-eqz v14, :cond_3

    invoke-static {v10}, LX/2yJ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 481531
    const-string v4, "article_ID"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 481532
    :cond_3
    if-eqz v14, :cond_4

    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 481533
    invoke-static/range {p2 .. p2}, LX/2sb;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v4

    .line 481534
    const/4 v6, -0x1

    if-eq v4, v6, :cond_4

    .line 481535
    const-string v6, "item_index"

    invoke-virtual {v14, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 481536
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->e:LX/0Zb;

    invoke-interface {v4, v14}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 481537
    if-eqz p7, :cond_5

    .line 481538
    const/4 v4, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, LX/2yV;->a(Z)V

    .line 481539
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->m:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    invoke-static {v11, v6, v7}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 481540
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 481541
    :cond_5
    invoke-virtual {v15}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v15}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v4, :cond_6

    invoke-virtual {v15}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v4}, LX/39w;->e(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 481542
    invoke-virtual {v15}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 481543
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2yJ;->o:LX/2yK;

    sget-object v7, LX/0ig;->z:LX/0ih;

    const/16 v8, 0x8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "position:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v4, v8, v9}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 481544
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->l:LX/16I;

    invoke-virtual {v4}, LX/16I;->a()Z

    move-result v4

    if-nez v4, :cond_7

    .line 481545
    const/4 v4, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, LX/2yi;->a(Z)V

    .line 481546
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 481547
    :cond_7
    if-eqz v5, :cond_8

    invoke-virtual {v5}, LX/0lF;->e()I

    move-result v4

    if-nez v4, :cond_b

    .line 481548
    :cond_8
    :goto_2
    return-void

    .line 481549
    :cond_9
    const-string v4, "other"

    move-object v13, v4

    goto/16 :goto_0

    .line 481550
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 481551
    :cond_b
    if-eqz v11, :cond_c

    .line 481552
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2yJ;->g:LX/1Ay;

    invoke-static {v15}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v4, v5, v0}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    .line 481553
    :cond_c
    invoke-static/range {p2 .. p2}, LX/2yJ;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1ZS;

    move-result-object v4

    .line 481554
    if-eqz v4, :cond_8

    .line 481555
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2yJ;->h:LX/0bH;

    invoke-virtual {v5, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_2
.end method
