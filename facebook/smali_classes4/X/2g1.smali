.class public LX/2g1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13H;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2g1;


# instance fields
.field public final a:LX/03V;

.field public final b:LX/2g2;

.field public final c:LX/2g3;


# direct methods
.method public constructor <init>(LX/2g2;LX/03V;LX/2g3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447720
    iput-object p2, p0, LX/2g1;->a:LX/03V;

    .line 447721
    iput-object p1, p0, LX/2g1;->b:LX/2g2;

    .line 447722
    iput-object p3, p0, LX/2g1;->c:LX/2g3;

    .line 447723
    return-void
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)LX/1Y7;
    .locals 2

    .prologue
    .line 447724
    new-instance v0, LX/1Y8;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1Y8;-><init>(Z)V

    .line 447725
    iput-object p0, v0, LX/1Y8;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 447726
    move-object v0, v0

    .line 447727
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/2g1;
    .locals 6

    .prologue
    .line 447728
    sget-object v0, LX/2g1;->d:LX/2g1;

    if-nez v0, :cond_1

    .line 447729
    const-class v1, LX/2g1;

    monitor-enter v1

    .line 447730
    :try_start_0
    sget-object v0, LX/2g1;->d:LX/2g1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 447731
    if-eqz v2, :cond_0

    .line 447732
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 447733
    new-instance p0, LX/2g1;

    invoke-static {v0}, LX/2g2;->b(LX/0QB;)LX/2g2;

    move-result-object v3

    check-cast v3, LX/2g2;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/2g3;->a(LX/0QB;)LX/2g3;

    move-result-object v5

    check-cast v5, LX/2g3;

    invoke-direct {p0, v3, v4, v5}, LX/2g1;-><init>(LX/2g2;LX/03V;LX/2g3;)V

    .line 447734
    move-object v0, p0

    .line 447735
    sput-object v0, LX/2g1;->d:LX/2g1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447736
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 447737
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 447738
    :cond_1
    sget-object v0, LX/2g1;->d:LX/2g1;

    return-object v0

    .line 447739
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 447740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;
    .locals 5
    .param p2    # Lcom/facebook/interstitial/manager/InterstitialTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 447741
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    .line 447742
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->type:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->type:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    sget-object v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;->UNKNOWN:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 447743
    if-eqz v0, :cond_0

    .line 447744
    iget-object v0, p0, LX/2g1;->b:LX/2g2;

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 447745
    :try_start_0
    invoke-static {v0, v1, p1, p2}, LX/2g2;->a(LX/2g2;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    :try_end_0
    .catch LX/77W; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    .line 447746
    :goto_1
    move v0, v2

    .line 447747
    if-eqz v0, :cond_3

    .line 447748
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 447749
    :goto_2
    move-object v0, v0

    .line 447750
    :goto_3
    return-object v0

    .line 447751
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 447752
    :try_start_1
    iget-object v2, p0, LX/2g1;->c:LX/2g3;

    invoke-virtual {v2, v0, p1, p2}, LX/2g3;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v2

    .line 447753
    if-nez v2, :cond_1

    .line 447754
    invoke-static {v0}, LX/2g1;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)LX/1Y7;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    .line 447755
    :goto_4
    move-object v0, v0

    .line 447756
    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    new-instance v0, LX/1Y8;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1Y8;-><init>(Z)V

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 447757
    iput-object v1, v0, LX/1Y8;->d:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    .line 447758
    move-object v0, v0

    .line 447759
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    goto :goto_2

    .line 447760
    :catch_0
    iget-object v2, v0, LX/2g2;->b:LX/03V;

    const-string v3, "QuickPromotion_boolean_filter"

    const-string p0, "Invalid clause"

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 447761
    :goto_5
    const/4 v2, 0x0

    goto :goto_1

    .line 447762
    :catch_1
    iget-object v2, v0, LX/2g2;->b:LX/03V;

    const-string v3, "QuickPromotion_boolean_filter"

    const-string p0, "Invalid filter"

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 447763
    :catch_2
    iget-object v2, v0, LX/2g2;->b:LX/03V;

    const-string v3, "QuickPromotion_boolean_filter"

    const-string p0, "Invalid filter"

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 447764
    :catch_3
    move-exception v1

    .line 447765
    iget-object v2, p0, LX/2g1;->a:LX/03V;

    const-string v3, "QuickPromotion_filter"

    const-string v4, "Invalid filter"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 447766
    invoke-static {v0}, LX/2g1;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)LX/1Y7;

    move-result-object v0

    goto :goto_4

    .line 447767
    :catch_4
    move-exception v1

    .line 447768
    iget-object v2, p0, LX/2g1;->a:LX/03V;

    const-string v3, "QuickPromotion_filter"

    const-string v4, "Invalid filter"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 447769
    invoke-static {v0}, LX/2g1;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)LX/1Y7;

    move-result-object v0

    goto :goto_4

    .line 447770
    :cond_4
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    goto :goto_4
.end method
