.class public LX/2ye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yF;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482467
    return-void
.end method

.method public static a(LX/0QB;)LX/2ye;
    .locals 3

    .prologue
    .line 482468
    const-class v1, LX/2ye;

    monitor-enter v1

    .line 482469
    :try_start_0
    sget-object v0, LX/2ye;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482470
    sput-object v2, LX/2ye;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482471
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482472
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 482473
    new-instance v0, LX/2ye;

    invoke-direct {v0}, LX/2ye;-><init>()V

    .line 482474
    move-object v0, v0

    .line 482475
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482476
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2ye;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482477
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 482479
    return-object p1
.end method
