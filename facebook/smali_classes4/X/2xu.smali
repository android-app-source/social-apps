.class public final LX/2xu;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public final synthetic c:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 1

    .prologue
    .line 479588
    iput-object p1, p0, LX/2xu;->c:LX/1vg;

    .line 479589
    move-object v0, p1

    .line 479590
    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 479591
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479592
    const-string v0, "GlyphColorizerDrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 479593
    if-ne p0, p1, :cond_1

    .line 479594
    :cond_0
    :goto_0
    return v0

    .line 479595
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 479596
    goto :goto_0

    .line 479597
    :cond_3
    check-cast p1, LX/2xu;

    .line 479598
    iget v2, p0, LX/2xu;->a:I

    iget v3, p1, LX/2xu;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 479599
    goto :goto_0

    .line 479600
    :cond_4
    iget v2, p0, LX/2xu;->b:I

    iget v3, p1, LX/2xu;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 479601
    goto :goto_0
.end method
