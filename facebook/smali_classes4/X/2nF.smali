.class public LX/2nF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2nF;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463635
    iput-object p1, p0, LX/2nF;->a:LX/0ad;

    .line 463636
    return-void
.end method

.method public static a(LX/0QB;)LX/2nF;
    .locals 4

    .prologue
    .line 463621
    sget-object v0, LX/2nF;->b:LX/2nF;

    if-nez v0, :cond_1

    .line 463622
    const-class v1, LX/2nF;

    monitor-enter v1

    .line 463623
    :try_start_0
    sget-object v0, LX/2nF;->b:LX/2nF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 463624
    if-eqz v2, :cond_0

    .line 463625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 463626
    new-instance p0, LX/2nF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/2nF;-><init>(LX/0ad;)V

    .line 463627
    move-object v0, p0

    .line 463628
    sput-object v0, LX/2nF;->b:LX/2nF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 463630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 463631
    :cond_1
    sget-object v0, LX/2nF;->b:LX/2nF;

    return-object v0

    .line 463632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 463633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2nF;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 4
    .param p0    # LX/2nF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 463617
    if-nez p1, :cond_1

    .line 463618
    :cond_0
    :goto_0
    return v0

    .line 463619
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp()Ljava/lang/String;

    move-result-object v2

    .line 463620
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v3, "unsupported_video_ratio"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/2nF;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 3
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryActionLink;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 463613
    if-nez p0, :cond_1

    .line 463614
    :cond_0
    :goto_0
    return v0

    .line 463615
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    .line 463616
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->PLAY_GAME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 463637
    invoke-static {p0}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463638
    const/4 v0, 0x0

    .line 463639
    :goto_0
    return v0

    .line 463640
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463641
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 463642
    const/4 v1, 0x0

    .line 463643
    if-nez v0, :cond_2

    .line 463644
    :cond_1
    :goto_1
    move v0, v1

    .line 463645
    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object p0

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 463612
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, LX/2nF;->a:LX/0ad;

    sget-short v2, LX/D7l;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 463610
    iget-object v2, p0, LX/2nF;->a:LX/0ad;

    sget v3, LX/D7l;->b:I

    invoke-interface {v2, v3, v1}, LX/0ad;->a(II)I

    move-result v2

    .line 463611
    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 463601
    invoke-virtual {p0}, LX/2nF;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, LX/2nF;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 463602
    iget-object v2, p0, LX/2nF;->a:LX/0ad;

    sget v3, LX/D7l;->b:I

    invoke-interface {v2, v3, v0}, LX/0ad;->a(II)I

    move-result v2

    .line 463603
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 463604
    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/2nF;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 463605
    if-eqz v0, :cond_2

    invoke-static {p1}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 463606
    :goto_1
    return v0

    .line 463607
    :cond_3
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463608
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v2, -0x1e53800c

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 463609
    invoke-static {v0}, LX/2nF;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0, v0}, LX/2nF;->a(LX/2nF;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 463600
    invoke-virtual {p0}, LX/2nF;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/2nF;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 463599
    iget-object v0, p0, LX/2nF;->a:LX/0ad;

    sget-short v1, LX/D7l;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 463598
    iget-object v0, p0, LX/2nF;->a:LX/0ad;

    sget-short v1, LX/D7l;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
