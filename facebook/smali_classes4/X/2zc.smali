.class public LX/2zc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1wl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483683
    iput-object p1, p0, LX/2zc;->a:LX/0Ot;

    .line 483684
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 483675
    iget-object v0, p0, LX/2zc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 483676
    invoke-static {}, LX/1wl;->b()Z

    move-result v0

    invoke-static {v0}, LX/1wm;->a(Z)V

    .line 483677
    iget-object v0, p0, LX/2zc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wl;

    .line 483678
    invoke-virtual {v0}, LX/1wl;->d()LX/2zd;

    move-result-object v1

    .line 483679
    invoke-virtual {v0}, LX/1wl;->k()Landroid/os/Handler;

    move-result-object v0

    .line 483680
    new-instance v2, Lcom/facebook/appupdate/integration/common/AppUpdateLibInitializer$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/appupdate/integration/common/AppUpdateLibInitializer$1;-><init>(LX/2zc;LX/2zd;)V

    const v1, -0x73e82c40

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 483681
    return-void
.end method
