.class public LX/2hW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450037
    iput-object p1, p0, LX/2hW;->a:LX/0Zb;

    .line 450038
    iput-object p2, p0, LX/2hW;->b:LX/0Uh;

    .line 450039
    return-void
.end method

.method public static a(LX/0QB;)LX/2hW;
    .locals 5

    .prologue
    .line 450040
    const-class v1, LX/2hW;

    monitor-enter v1

    .line 450041
    :try_start_0
    sget-object v0, LX/2hW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 450042
    sput-object v2, LX/2hW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 450043
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450044
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 450045
    new-instance p0, LX/2hW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/2hW;-><init>(LX/0Zb;LX/0Uh;)V

    .line 450046
    move-object v0, p0

    .line 450047
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 450048
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2hW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450049
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 450050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLX/2hA;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 450051
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p4

    .line 450052
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "request_seen"

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "request_id"

    invoke-virtual {v0, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p0, "request_surface"

    iget-object p1, v4, LX/2hA;->value:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string p0, "friend_request_waterfall"

    .line 450053
    iput-object p0, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 450054
    move-object v0, v0

    .line 450055
    iput-object v6, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 450056
    move-object v0, v0

    .line 450057
    goto :goto_0

    .line 450058
    :goto_0
    iget-object p0, v1, LX/2hW;->a:LX/0Zb;

    invoke-interface {p0, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 450059
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 450060
    iget-object v0, p0, LX/2hW;->b:LX/0Uh;

    const/16 v1, 0x3bc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
