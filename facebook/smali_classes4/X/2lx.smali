.class public LX/2lx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2lx;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459208
    return-void
.end method

.method public static a(LX/0QB;)LX/2lx;
    .locals 3

    .prologue
    .line 459209
    sget-object v0, LX/2lx;->a:LX/2lx;

    if-nez v0, :cond_1

    .line 459210
    const-class v1, LX/2lx;

    monitor-enter v1

    .line 459211
    :try_start_0
    sget-object v0, LX/2lx;->a:LX/2lx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 459212
    if-eqz v2, :cond_0

    .line 459213
    :try_start_1
    new-instance v0, LX/2lx;

    invoke-direct {v0}, LX/2lx;-><init>()V

    .line 459214
    move-object v0, v0

    .line 459215
    sput-object v0, LX/2lx;->a:LX/2lx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459216
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 459217
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 459218
    :cond_1
    sget-object v0, LX/2lx;->a:LX/2lx;

    return-object v0

    .line 459219
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 459220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 459221
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 459222
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459223
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "one_column"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459224
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "icon_style"

    const-string v2, "caspian"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459225
    new-instance v0, LX/14N;

    sget-object v1, LX/11I;->BOOKMARK_SYNC:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    const-string v2, "GET"

    const-string v3, "method/bookmarks.get"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 459226
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
