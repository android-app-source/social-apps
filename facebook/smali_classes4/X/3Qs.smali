.class public final LX/3Qs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 566444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)Lcom/facebook/auth/credentials/SessionCookie;
    .locals 6

    .prologue
    .line 566445
    new-instance v0, Lcom/facebook/auth/credentials/SessionCookie;

    invoke-direct {v0}, Lcom/facebook/auth/credentials/SessionCookie;-><init>()V

    .line 566446
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 566447
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 566448
    const/4 v0, 0x0

    .line 566449
    :cond_0
    return-object v0

    .line 566450
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 566451
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 566452
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 566453
    const/4 v2, 0x0

    .line 566454
    const-string v4, "name"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 566455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_2

    :goto_1
    iput-object v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    .line 566456
    :goto_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 566457
    :cond_2
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 566458
    :cond_3
    const-string v4, "value"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 566459
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_4

    :goto_3
    iput-object v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    .line 566460
    goto :goto_2

    .line 566461
    :cond_4
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 566462
    :cond_5
    const-string v4, "expires"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 566463
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_6

    :goto_4
    iput-object v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    .line 566464
    goto :goto_2

    .line 566465
    :cond_6
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 566466
    :cond_7
    const-string v4, "domain"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 566467
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_8

    :goto_5
    iput-object v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    .line 566468
    goto :goto_2

    .line 566469
    :cond_8
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 566470
    :cond_9
    const-string v4, "secure"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 566471
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    iput-boolean v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    .line 566472
    goto :goto_2

    .line 566473
    :cond_a
    const-string v4, "path"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 566474
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_b

    :goto_6
    iput-object v2, v0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    .line 566475
    goto :goto_2

    .line 566476
    :cond_b
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 566477
    :cond_c
    goto :goto_2
.end method
