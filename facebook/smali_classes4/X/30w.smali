.class public final LX/30w;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/1AN;


# direct methods
.method public constructor <init>(LX/1AN;)V
    .locals 0

    .prologue
    .line 486485
    iput-object p1, p0, LX/30w;->a:LX/1AN;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 11

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 486486
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v10, v0

    .line 486487
    float-to-double v0, v10

    const-wide/16 v2, 0x0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-wide v8, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 486488
    iget-object v1, p0, LX/30w;->a:LX/1AN;

    iget-object v1, v1, LX/1AN;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 486489
    iget-object v1, p0, LX/30w;->a:LX/1AN;

    iget-object v1, v1, LX/1AN;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 486490
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    invoke-static {v0}, LX/1AN;->j(LX/1AN;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/30w;->a:LX/1AN;

    invoke-static {v0}, LX/1AN;->m(LX/1AN;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486491
    :cond_0
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    iget-object v0, v0, LX/1AN;->k:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setAlpha(F)V

    .line 486492
    :cond_1
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 486493
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    invoke-static {v0}, LX/1AN;->m(LX/1AN;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486494
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    sget-object v1, LX/1AO;->HIDDEN:LX/1AO;

    .line 486495
    iput-object v1, v0, LX/1AN;->n:LX/1AO;

    .line 486496
    :cond_0
    :goto_0
    return-void

    .line 486497
    :cond_1
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    invoke-static {v0}, LX/1AN;->j(LX/1AN;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486498
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    sget-object v1, LX/1AO;->SHOWN:LX/1AO;

    .line 486499
    iput-object v1, v0, LX/1AN;->n:LX/1AO;

    .line 486500
    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 486501
    iget-object v0, p0, LX/30w;->a:LX/1AN;

    iget-object v0, v0, LX/1AN;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 486502
    return-void
.end method
