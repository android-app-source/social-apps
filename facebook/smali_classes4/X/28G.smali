.class public LX/28G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/28G;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _columnNr:I

.field public final _lineNr:I

.field public final _totalBytes:J

.field public final _totalChars:J

.field public final transient b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, -0x1

    .line 373837
    new-instance v0, LX/28G;

    const-string v1, "N/A"

    move-wide v4, v2

    move v7, v6

    invoke-direct/range {v0 .. v7}, LX/28G;-><init>(Ljava/lang/Object;JJII)V

    sput-object v0, LX/28G;->a:LX/28G;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;JII)V
    .locals 8

    .prologue
    .line 373835
    const-wide/16 v2, -0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, LX/28G;-><init>(Ljava/lang/Object;JJII)V

    .line 373836
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;JJII)V
    .locals 0

    .prologue
    .line 373838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373839
    iput-object p1, p0, LX/28G;->b:Ljava/lang/Object;

    .line 373840
    iput-wide p2, p0, LX/28G;->_totalBytes:J

    .line 373841
    iput-wide p4, p0, LX/28G;->_totalChars:J

    .line 373842
    iput p6, p0, LX/28G;->_lineNr:I

    .line 373843
    iput p7, p0, LX/28G;->_columnNr:I

    .line 373844
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 373823
    if-ne p1, p0, :cond_1

    .line 373824
    :cond_0
    :goto_0
    return v0

    .line 373825
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 373826
    :cond_2
    instance-of v2, p1, LX/28G;

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    .line 373827
    :cond_3
    check-cast p1, LX/28G;

    .line 373828
    iget-object v2, p0, LX/28G;->b:Ljava/lang/Object;

    if-nez v2, :cond_4

    .line 373829
    iget-object v2, p1, LX/28G;->b:Ljava/lang/Object;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 373830
    :cond_4
    iget-object v2, p0, LX/28G;->b:Ljava/lang/Object;

    iget-object v3, p1, LX/28G;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 373831
    :cond_5
    iget v2, p0, LX/28G;->_lineNr:I

    iget v3, p1, LX/28G;->_lineNr:I

    if-ne v2, v3, :cond_6

    iget v2, p0, LX/28G;->_columnNr:I

    iget v3, p1, LX/28G;->_columnNr:I

    if-ne v2, v3, :cond_6

    iget-wide v2, p0, LX/28G;->_totalChars:J

    iget-wide v4, p1, LX/28G;->_totalChars:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    .line 373832
    iget-wide v6, p0, LX/28G;->_totalBytes:J

    move-wide v2, v6

    .line 373833
    iget-wide v6, p1, LX/28G;->_totalBytes:J

    move-wide v4, v6

    .line 373834
    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 373816
    iget-object v0, p0, LX/28G;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 373817
    :goto_0
    iget v1, p0, LX/28G;->_lineNr:I

    xor-int/2addr v0, v1

    .line 373818
    iget v1, p0, LX/28G;->_columnNr:I

    add-int/2addr v0, v1

    .line 373819
    iget-wide v2, p0, LX/28G;->_totalChars:J

    long-to-int v1, v2

    xor-int/2addr v0, v1

    .line 373820
    iget-wide v2, p0, LX/28G;->_totalBytes:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 373821
    return v0

    .line 373822
    :cond_0
    iget-object v0, p0, LX/28G;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373805
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 373806
    const-string v1, "[Source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373807
    iget-object v1, p0, LX/28G;->b:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 373808
    const-string v1, "UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373809
    :goto_0
    const-string v1, "; line: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373810
    iget v1, p0, LX/28G;->_lineNr:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373811
    const-string v1, ", column: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373812
    iget v1, p0, LX/28G;->_columnNr:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373813
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 373814
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 373815
    :cond_0
    iget-object v1, p0, LX/28G;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
