.class public LX/3LP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/3LP;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2RC;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6N7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2RE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550460
    const-class v0, LX/3LP;

    sput-object v0, LX/3LP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/2RE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2RC;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6N7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Nf;",
            ">;",
            "LX/2RE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550492
    iput-object p1, p0, LX/3LP;->b:LX/0Ot;

    .line 550493
    iput-object p2, p0, LX/3LP;->c:LX/0Or;

    .line 550494
    iput-object p3, p0, LX/3LP;->d:LX/0Ot;

    .line 550495
    iput-object p5, p0, LX/3LP;->e:LX/2RE;

    .line 550496
    return-void
.end method

.method public static a(LX/0QB;)LX/3LP;
    .locals 9

    .prologue
    .line 550478
    sget-object v0, LX/3LP;->f:LX/3LP;

    if-nez v0, :cond_1

    .line 550479
    const-class v1, LX/3LP;

    monitor-enter v1

    .line 550480
    :try_start_0
    sget-object v0, LX/3LP;->f:LX/3LP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 550481
    if-eqz v2, :cond_0

    .line 550482
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 550483
    new-instance v3, LX/3LP;

    const/16 v4, 0x41b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x438

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1a12

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1a19

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v8

    check-cast v8, LX/2RE;

    invoke-direct/range {v3 .. v8}, LX/3LP;-><init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/2RE;)V

    .line 550484
    move-object v0, v3

    .line 550485
    sput-object v0, LX/3LP;->f:LX/3LP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 550486
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 550487
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 550488
    :cond_1
    sget-object v0, LX/3LP;->f:LX/3LP;

    return-object v0

    .line 550489
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 550490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/2RR;Ljava/util/Set;)LX/3On;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2RR;",
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;)",
            "LX/3On;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 550465
    sget-object v0, LX/2RU;->FACEBOOK_FRIENDS_TYPES:LX/0Px;

    .line 550466
    iput-object v0, p1, LX/2RR;->b:Ljava/util/Collection;

    .line 550467
    iget-object v0, p0, LX/3LP;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jp;

    .line 550468
    sget-object v1, LX/3Oj;->a:[I

    invoke-virtual {v0}, LX/2Jp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 550469
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected contact storage mode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 550470
    :pswitch_0
    iget-object v0, p0, LX/3LP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2RC;

    sget-object v1, LX/2RV;->USER:LX/2RV;

    invoke-virtual {v0, p1, v1, p2}, LX/2RC;->a(LX/2RR;LX/2RV;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v1

    .line 550471
    if-eqz v1, :cond_0

    new-instance v0, LX/3Om;

    invoke-direct {v0, v1}, LX/3Om;-><init>(Landroid/database/Cursor;)V

    .line 550472
    :goto_0
    return-object v0

    .line 550473
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 550474
    :pswitch_1
    iget-object v0, p0, LX/3LP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6N7;

    .line 550475
    iget-object v1, v0, LX/6N7;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6NX;

    sget-object v2, LX/6Nb;->USER_QUERY:LX/6Nb;

    invoke-virtual {v1, v2}, LX/6NX;->a(LX/6Nb;)Lcom/facebook/omnistore/Collection;

    move-result-object v1

    .line 550476
    new-instance v2, LX/6NC;

    invoke-static {v0, p1, p2, v1}, LX/6N7;->a(LX/6N7;LX/2RR;Ljava/util/Set;Lcom/facebook/omnistore/Collection;)Lcom/facebook/omnistore/Cursor;

    move-result-object v1

    iget-object v3, v0, LX/6N7;->f:LX/6Nf;

    invoke-direct {v2, v1, v3}, LX/6NC;-><init>(Lcom/facebook/omnistore/Cursor;LX/6Nf;)V

    move-object v0, v2

    .line 550477
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2RR;)LX/3On;
    .locals 1

    .prologue
    .line 550464
    iget-object v0, p0, LX/3LP;->e:LX/2RE;

    invoke-virtual {v0}, LX/2RE;->a()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/3LP;->a(LX/2RR;Ljava/util/Set;)LX/3On;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2RR;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2RR;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550461
    invoke-virtual {p0, p1}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v0

    .line 550462
    :try_start_0
    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 550463
    invoke-interface {v0}, LX/3On;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, LX/3On;->close()V

    throw v1
.end method
