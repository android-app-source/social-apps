.class public LX/2WB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2WB;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 418599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418600
    iput-object p1, p0, LX/2WB;->a:LX/0Ot;

    .line 418601
    return-void
.end method

.method public static a(LX/0QB;)LX/2WB;
    .locals 4

    .prologue
    .line 418602
    sget-object v0, LX/2WB;->b:LX/2WB;

    if-nez v0, :cond_1

    .line 418603
    const-class v1, LX/2WB;

    monitor-enter v1

    .line 418604
    :try_start_0
    sget-object v0, LX/2WB;->b:LX/2WB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 418605
    if-eqz v2, :cond_0

    .line 418606
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 418607
    new-instance v3, LX/2WB;

    const/16 p0, 0x12f4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2WB;-><init>(LX/0Ot;)V

    .line 418608
    move-object v0, v3

    .line 418609
    sput-object v0, LX/2WB;->b:LX/2WB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 418611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 418612
    :cond_1
    sget-object v0, LX/2WB;->b:LX/2WB;

    return-object v0

    .line 418613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 418614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 4

    .prologue
    .line 418615
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "video_daily_data_usage"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 418616
    iput-wide p1, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 418617
    iput-object p3, v1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->f:Ljava/lang/String;

    .line 418618
    iget-object v0, p0, LX/2WB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0A1;

    invoke-virtual {v0, v1}, LX/0A1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 418619
    return-object v1
.end method
