.class public LX/2Lc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/2Ld;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Ld;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395226
    iput-object p1, p0, LX/2Lc;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 395227
    iput-object p2, p0, LX/2Lc;->b:LX/2Ld;

    .line 395228
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 7

    .prologue
    .line 395229
    iget-object v0, p0, LX/2Lc;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->i:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 395230
    if-nez v0, :cond_0

    .line 395231
    :try_start_0
    iget-object v0, p0, LX/2Lc;->b:LX/2Ld;

    .line 395232
    sget-object v1, LX/2Ld;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 395233
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-eqz v2, :cond_1

    .line 395234
    invoke-static {v0, v1}, LX/2Ld;->b(LX/2Ld;Ljava/io/File;)V

    .line 395235
    :goto_0
    iget-object v0, p0, LX/2Lc;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0hM;->i:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395236
    :cond_0
    :goto_1
    return-void

    :catch_0
    goto :goto_1

    .line 395237
    :cond_1
    iget-object v2, v0, LX/2Ld;->c:Landroid/content/Context;

    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 395238
    invoke-static {v0}, LX/2Ld;->b(LX/2Ld;)V

    .line 395239
    :goto_2
    goto :goto_0

    .line 395240
    :cond_2
    iget-object v2, v0, LX/2Ld;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "facebook_ringtone_pop.m4a"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 395241
    iget-object v3, v0, LX/2Ld;->e:LX/0en;

    invoke-virtual {v3, v2, v1}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 395242
    invoke-static {v0, v1}, LX/2Ld;->b(LX/2Ld;Ljava/io/File;)V

    goto :goto_2
.end method
