.class public LX/2d4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/2d5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2d5",
            "<",
            "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 442845
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "pulsar/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 442846
    sput-object v0, LX/2d4;->a:LX/0Tn;

    const-string v1, "gravity_settings"

    const-class v2, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    invoke-static {v0, v1, v2}, LX/2d5;->a(LX/0Tn;Ljava/lang/String;Ljava/lang/Class;)LX/2d5;

    move-result-object v0

    sput-object v0, LX/2d4;->b:LX/2d5;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442841
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442842
    sget-object v0, LX/2d4;->b:LX/2d5;

    .line 442843
    iget-object p0, v0, LX/2d5;->a:LX/0Tn;

    move-object v0, p0

    .line 442844
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
