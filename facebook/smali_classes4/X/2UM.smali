.class public LX/2UM;
.super LX/1Eg;
.source ""


# instance fields
.field private final a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;


# direct methods
.method public constructor <init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415814
    const-string v0, "ADDRESSBOOK_SYNC"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415815
    iput-object p1, p0, LX/2UM;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 415816
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 415817
    const-class v0, Lcom/facebook/contacts/background/ContactsTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415818
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 415819
    iget-object v0, p0, LX/2UM;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0, v1}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(Z)V

    .line 415820
    return v1
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415821
    const/4 v0, 0x0

    return-object v0
.end method
