.class public LX/3HF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HG",
        "<",
        "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3HH;

.field private final b:LX/3HI;

.field private final c:LX/0Uh;

.field private final d:LX/3HJ;


# direct methods
.method public constructor <init>(LX/3HH;LX/3HI;LX/0Uh;LX/3HJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543207
    iput-object p1, p0, LX/3HF;->a:LX/3HH;

    .line 543208
    iput-object p2, p0, LX/3HF;->b:LX/3HI;

    .line 543209
    iput-object p3, p0, LX/3HF;->c:LX/0Uh;

    .line 543210
    iput-object p4, p0, LX/3HF;->d:LX/3HJ;

    .line 543211
    return-void
.end method

.method public static b(LX/0QB;)LX/3HF;
    .locals 5

    .prologue
    .line 543212
    new-instance v4, LX/3HF;

    const-class v0, LX/3HH;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3HH;

    const-class v1, LX/3HI;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3HI;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/3HJ;->b(LX/0QB;)LX/3HJ;

    move-result-object v3

    check-cast v3, LX/3HJ;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3HF;-><init>(LX/3HH;LX/3HI;LX/0Uh;LX/3HJ;)V

    .line 543213
    return-object v4
.end method


# virtual methods
.method public final bridge synthetic a(LX/0jT;)LX/3Bq;
    .locals 1

    .prologue
    .line 543214
    check-cast p1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    invoke-virtual {p0, p1}, LX/3HF;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/3Bq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;)LX/3Bq;
    .locals 6

    .prologue
    .line 543215
    iget-object v0, p0, LX/3HF;->d:LX/3HJ;

    invoke-virtual {v0}, LX/3HJ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 543216
    invoke-virtual {p1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    .line 543217
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 543218
    :cond_0
    const/4 v0, 0x0

    .line 543219
    :goto_0
    return-object v0

    .line 543220
    :cond_1
    new-instance v0, LX/4VL;

    iget-object v2, p0, LX/3HF;->c:LX/0Uh;

    const/4 v3, 0x1

    new-array v3, v3, [LX/4VT;

    const/4 v4, 0x0

    iget-object v5, p0, LX/3HF;->b:LX/3HI;

    .line 543221
    new-instance p1, LX/6A3;

    invoke-static {v5}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object p0

    check-cast p0, LX/20i;

    invoke-direct {p1, p0, v1}, LX/6A3;-><init>(LX/20i;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)V

    .line 543222
    move-object v1, p1

    .line 543223
    aput-object v1, v3, v4

    invoke-direct {v0, v2, v3}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    goto :goto_0

    .line 543224
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 543225
    iget-object v1, p0, LX/3HF;->a:LX/3HH;

    .line 543226
    new-instance v4, LX/3dF;

    invoke-static {v1}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v2

    check-cast v2, LX/20j;

    invoke-static {v1}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v3

    check-cast v3, LX/20i;

    invoke-direct {v4, v2, v3, p1, v0}, LX/3dF;-><init>(LX/20j;LX/20i;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;Ljava/lang/String;)V

    .line 543227
    move-object v0, v4

    .line 543228
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 543229
    const-class v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543230
    const/4 v0, 0x0

    return-object v0
.end method
