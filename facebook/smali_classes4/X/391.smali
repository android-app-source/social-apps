.class public final enum LX/391;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/391;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/391;

.field public static final enum AUTOMATIC:LX/391;

.field public static final enum MANUAL:LX/391;

.field public static final enum UNKNOWN:LX/391;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 522162
    new-instance v0, LX/391;

    const-string v1, "MANUAL"

    invoke-direct {v0, v1, v2}, LX/391;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/391;->MANUAL:LX/391;

    .line 522163
    new-instance v0, LX/391;

    const-string v1, "AUTOMATIC"

    invoke-direct {v0, v1, v3}, LX/391;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/391;->AUTOMATIC:LX/391;

    .line 522164
    new-instance v0, LX/391;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/391;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/391;->UNKNOWN:LX/391;

    .line 522165
    const/4 v0, 0x3

    new-array v0, v0, [LX/391;

    sget-object v1, LX/391;->MANUAL:LX/391;

    aput-object v1, v0, v2

    sget-object v1, LX/391;->AUTOMATIC:LX/391;

    aput-object v1, v0, v3

    sget-object v1, LX/391;->UNKNOWN:LX/391;

    aput-object v1, v0, v4

    sput-object v0, LX/391;->$VALUES:[LX/391;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 522166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/391;
    .locals 1

    .prologue
    .line 522167
    const-class v0, LX/391;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/391;

    return-object v0
.end method

.method public static values()[LX/391;
    .locals 1

    .prologue
    .line 522168
    sget-object v0, LX/391;->$VALUES:[LX/391;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/391;

    return-object v0
.end method
