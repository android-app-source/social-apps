.class public LX/3NV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mi;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/concurrent/Future;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public final c:LX/3Mi;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/3NP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559378
    const-class v0, LX/3NV;

    sput-object v0, LX/3NV;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;LX/3NP;)V
    .locals 0

    .prologue
    .line 559379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559380
    iput-object p1, p0, LX/3NV;->c:LX/3Mi;

    .line 559381
    iput-object p2, p0, LX/3NV;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 559382
    iput-object p3, p0, LX/3NV;->e:LX/3NP;

    .line 559383
    return-void
.end method

.method private a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 559345
    invoke-direct {p0}, LX/3NV;->c()V

    .line 559346
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0}, LX/3Mi;->a()V

    .line 559347
    iget-object v0, p0, LX/3NV;->d:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p1, p2, p3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    .line 559348
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 559374
    iget-object v0, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 559375
    iget-object v0, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 559376
    const/4 v0, 0x0

    iput-object v0, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    .line 559377
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 559372
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0}, LX/3Mi;->a()V

    .line 559373
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559384
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/0Px;)V

    .line 559385
    return-void
.end method

.method public final a(LX/3LI;)V
    .locals 1

    .prologue
    .line 559370
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/3LI;)V

    .line 559371
    return-void
.end method

.method public final a(LX/3Md;)V
    .locals 1

    .prologue
    .line 559368
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/3Md;)V

    .line 559369
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation

    .prologue
    .line 559361
    iget-object v0, p0, LX/3NV;->e:LX/3NP;

    invoke-interface {v0, p1}, LX/3NP;->a(Ljava/lang/CharSequence;)J

    move-result-wide v0

    .line 559362
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 559363
    invoke-direct {p0}, LX/3NV;->c()V

    .line 559364
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0, p1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 559365
    :goto_0
    return-void

    .line 559366
    :cond_0
    iget-object v2, p0, LX/3NV;->c:LX/3Mi;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 559367
    new-instance v2, Lcom/facebook/contacts/picker/ContactPickerDelayingListFilter$1;

    invoke-direct {v2, p0, p1}, Lcom/facebook/contacts/picker/ContactPickerDelayingListFilter$1;-><init>(LX/3NV;Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2, v0, v1}, LX/3NV;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;LX/3NY;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation

    .prologue
    .line 559352
    iget-object v0, p0, LX/3NV;->e:LX/3NP;

    invoke-interface {v0, p1}, LX/3NP;->a(Ljava/lang/CharSequence;)J

    move-result-wide v0

    .line 559353
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 559354
    invoke-direct {p0}, LX/3NV;->c()V

    .line 559355
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0, p1, p2}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 559356
    :goto_0
    return-void

    .line 559357
    :cond_0
    iget-object v2, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    if-nez v2, :cond_1

    iget-object v2, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v2}, LX/333;->b()LX/3Mr;

    move-result-object v2

    sget-object v3, LX/3Mr;->FILTERING:LX/3Mr;

    if-eq v2, v3, :cond_1

    .line 559358
    sget-object v2, LX/3Mr;->FILTERING:LX/3Mr;

    invoke-interface {p2, v2}, LX/3NY;->a(LX/3Mr;)V

    .line 559359
    :cond_1
    iget-object v2, p0, LX/3NV;->c:LX/3Mi;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 559360
    new-instance v2, Lcom/facebook/contacts/picker/ContactPickerDelayingListFilter$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/facebook/contacts/picker/ContactPickerDelayingListFilter$2;-><init>(LX/3NV;Ljava/lang/CharSequence;LX/3NY;)V

    invoke-direct {p0, v2, v0, v1}, LX/3NV;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final b()LX/3Mr;
    .locals 1

    .prologue
    .line 559349
    iget-object v0, p0, LX/3NV;->a:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 559350
    sget-object v0, LX/3Mr;->FILTERING:LX/3Mr;

    .line 559351
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3NV;->c:LX/3Mi;

    invoke-interface {v0}, LX/333;->b()LX/3Mr;

    move-result-object v0

    goto :goto_0
.end method
