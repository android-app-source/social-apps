.class public final enum LX/3Kt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Kt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Kt;

.field public static final enum FULL:LX/3Kt;

.field public static final enum NONE:LX/3Kt;

.field public static final enum READONLY:LX/3Kt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 549381
    new-instance v0, LX/3Kt;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/3Kt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Kt;->NONE:LX/3Kt;

    .line 549382
    new-instance v0, LX/3Kt;

    const-string v1, "READONLY"

    invoke-direct {v0, v1, v3}, LX/3Kt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Kt;->READONLY:LX/3Kt;

    .line 549383
    new-instance v0, LX/3Kt;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v4}, LX/3Kt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Kt;->FULL:LX/3Kt;

    .line 549384
    const/4 v0, 0x3

    new-array v0, v0, [LX/3Kt;

    sget-object v1, LX/3Kt;->NONE:LX/3Kt;

    aput-object v1, v0, v2

    sget-object v1, LX/3Kt;->READONLY:LX/3Kt;

    aput-object v1, v0, v3

    sget-object v1, LX/3Kt;->FULL:LX/3Kt;

    aput-object v1, v0, v4

    sput-object v0, LX/3Kt;->$VALUES:[LX/3Kt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 549385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Kt;
    .locals 1

    .prologue
    .line 549386
    const-class v0, LX/3Kt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Kt;

    return-object v0
.end method

.method public static values()[LX/3Kt;
    .locals 1

    .prologue
    .line 549387
    sget-object v0, LX/3Kt;->$VALUES:[LX/3Kt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Kt;

    return-object v0
.end method
