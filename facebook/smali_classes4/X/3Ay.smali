.class public LX/3Ay;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Ay;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 526613
    const-string v0, "push_notifications"

    const/4 v1, 0x2

    new-instance v2, LX/3Az;

    invoke-direct {v2}, LX/3Az;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 526614
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ay;
    .locals 3

    .prologue
    .line 526615
    sget-object v0, LX/3Ay;->a:LX/3Ay;

    if-nez v0, :cond_1

    .line 526616
    const-class v1, LX/3Ay;

    monitor-enter v1

    .line 526617
    :try_start_0
    sget-object v0, LX/3Ay;->a:LX/3Ay;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 526618
    if-eqz v2, :cond_0

    .line 526619
    :try_start_1
    new-instance v0, LX/3Ay;

    invoke-direct {v0}, LX/3Ay;-><init>()V

    .line 526620
    move-object v0, v0

    .line 526621
    sput-object v0, LX/3Ay;->a:LX/3Ay;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 526622
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 526623
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 526624
    :cond_1
    sget-object v0, LX/3Ay;->a:LX/3Ay;

    return-object v0

    .line 526625
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 526626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
