.class public LX/2lw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2lw;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Xp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459189
    iput-object p1, p0, LX/2lw;->a:Landroid/content/Context;

    .line 459190
    iget-object v0, p0, LX/2lw;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    iput-object v0, p0, LX/2lw;->b:LX/0Xp;

    .line 459191
    return-void
.end method

.method public static a(LX/0QB;)LX/2lw;
    .locals 4

    .prologue
    .line 459192
    sget-object v0, LX/2lw;->c:LX/2lw;

    if-nez v0, :cond_1

    .line 459193
    const-class v1, LX/2lw;

    monitor-enter v1

    .line 459194
    :try_start_0
    sget-object v0, LX/2lw;->c:LX/2lw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 459195
    if-eqz v2, :cond_0

    .line 459196
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 459197
    new-instance p0, LX/2lw;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2lw;-><init>(Landroid/content/Context;)V

    .line 459198
    move-object v0, p0

    .line 459199
    sput-object v0, LX/2lw;->c:LX/2lw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459200
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 459201
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 459202
    :cond_1
    sget-object v0, LX/2lw;->c:LX/2lw;

    return-object v0

    .line 459203
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 459204
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 459205
    iget-object v0, p0, LX/2lw;->b:LX/0Xp;

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 459206
    return-void
.end method
