.class public LX/2BU;
.super LX/0Tw;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/2BU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 379162
    const-class v0, LX/2BU;

    sput-object v0, LX/2BU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 379148
    const-string v0, "legacy_user_values"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0Tw;-><init>(Ljava/lang/String;I)V

    .line 379149
    return-void
.end method

.method public static a(LX/0QB;)LX/2BU;
    .locals 3

    .prologue
    .line 379150
    sget-object v0, LX/2BU;->b:LX/2BU;

    if-nez v0, :cond_1

    .line 379151
    const-class v1, LX/2BU;

    monitor-enter v1

    .line 379152
    :try_start_0
    sget-object v0, LX/2BU;->b:LX/2BU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 379153
    if-eqz v2, :cond_0

    .line 379154
    :try_start_1
    new-instance v0, LX/2BU;

    invoke-direct {v0}, LX/2BU;-><init>()V

    .line 379155
    move-object v0, v0

    .line 379156
    sput-object v0, LX/2BU;->b:LX/2BU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379157
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 379158
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379159
    :cond_1
    sget-object v0, LX/2BU;->b:LX/2BU;

    return-object v0

    .line 379160
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 379161
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 379146
    const-string v0, "CREATE TABLE user_values (_id INTEGER PRIMARY KEY,name TEXT,value TEXT);"

    const v1, -0x10e1281

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x8b47d3d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 379147
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 379143
    const-string v0, "DROP TABLE IF EXISTS user_values"

    const v1, 0x4a1dab10    # 2583236.0f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3bae85f1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 379144
    invoke-virtual {p0, p1}, LX/2BU;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 379145
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 379141
    const-string v0, "user_values"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 379142
    return-void
.end method
