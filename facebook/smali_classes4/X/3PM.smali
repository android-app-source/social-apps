.class public LX/3PM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PN;


# direct methods
.method public constructor <init>(LX/3PN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562003
    iput-object p1, p0, LX/3PM;->a:LX/3PN;

    .line 562004
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562005
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;

    .line 562006
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    .line 562007
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 562008
    :cond_0
    const/4 v0, 0x0

    .line 562009
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 562010
    new-instance v1, LX/6A2;

    invoke-direct {v1, v0}, LX/6A2;-><init>(Ljava/lang/String;)V

    .line 562011
    move-object v0, v1

    .line 562012
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562013
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$UnconvertPlaceListStoryMutationCallModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562014
    const/4 v0, 0x0

    return-object v0
.end method
