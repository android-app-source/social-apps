.class public LX/2Jy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/conditionalworker/ConditionalWorkerExecutionInfo;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2LI;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/30V;

.field private final c:Z

.field private final d:Ljava/lang/String;

.field private final e:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final f:LX/30T;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 393302
    const-class v0, LX/2Jy;

    sput-object v0, LX/2Jy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/30V;ZLjava/lang/String;Lcom/facebook/performancelogger/PerformanceLogger;LX/30T;)V
    .locals 0

    .prologue
    .line 393303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393304
    iput-object p1, p0, LX/2Jy;->b:LX/30V;

    .line 393305
    iput-boolean p2, p0, LX/2Jy;->c:Z

    .line 393306
    iput-object p3, p0, LX/2Jy;->d:Ljava/lang/String;

    .line 393307
    iput-object p4, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 393308
    iput-object p5, p0, LX/2Jy;->f:LX/30T;

    .line 393309
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 393310
    iget-object v0, p0, LX/2Jy;->f:LX/30T;

    invoke-virtual {v0}, LX/30T;->a()LX/2Jj;

    move-result-object v0

    .line 393311
    iget-object v1, p0, LX/2Jy;->b:LX/30V;

    invoke-interface {v1}, LX/30V;->d()LX/2Jl;

    move-result-object v1

    .line 393312
    invoke-virtual {v0, v1}, LX/2Jj;->a(LX/2Jl;)Z

    move-result v0

    .line 393313
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 393314
    return v0
.end method

.method public final b()LX/2Jh;
    .locals 1

    .prologue
    .line 393315
    iget-object v0, p0, LX/2Jy;->f:LX/30T;

    invoke-virtual {v0}, LX/30T;->b()LX/2Jh;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 393316
    const v5, 0x2c0001

    .line 393317
    const/4 v2, 0x0

    .line 393318
    const/4 v1, 0x0

    .line 393319
    :try_start_0
    iget-object v0, p0, LX/2Jy;->b:LX/30V;

    invoke-interface {v0}, LX/30V;->c()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jw;

    .line 393320
    iget-boolean v3, p0, LX/2Jy;->c:Z

    if-eqz v3, :cond_0

    .line 393321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, LX/2Jy;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 393322
    iget-object v3, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x2c0001

    invoke-interface {v3, v4, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 393323
    :cond_0
    if-eqz v0, :cond_1

    .line 393324
    invoke-interface {v0, p0}, LX/2Jw;->a(LX/2Jy;)Z

    move-result v1

    .line 393325
    :cond_1
    new-instance v0, LX/2LI;

    iget-object v3, p0, LX/2Jy;->b:LX/30V;

    invoke-direct {v0, v3, v1}, LX/2LI;-><init>(LX/30V;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393326
    iget-boolean v3, p0, LX/2Jy;->c:Z

    if-eqz v3, :cond_2

    .line 393327
    if-eqz v1, :cond_3

    .line 393328
    iget-object v1, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v5, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 393329
    :cond_2
    :goto_0
    return-object v0

    :cond_3
    iget-object v1, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v5, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    goto :goto_0

    .line 393330
    :catchall_0
    move-exception v0

    iget-boolean v3, p0, LX/2Jy;->c:Z

    if-eqz v3, :cond_4

    .line 393331
    if-eqz v1, :cond_5

    .line 393332
    iget-object v1, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v5, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 393333
    :cond_4
    :goto_1
    throw v0

    :cond_5
    iget-object v1, p0, LX/2Jy;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v5, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    goto :goto_1
.end method
