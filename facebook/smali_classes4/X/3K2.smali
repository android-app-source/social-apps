.class public LX/3K2;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3Jt;",
        "LX/9Ua;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/util/List;[[[F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jt;",
            ">;[[[F)V"
        }
    .end annotation

    .prologue
    .line 548254
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 548255
    return-void
.end method

.method public static a(LX/3K1;)LX/3K2;
    .locals 3

    .prologue
    .line 548256
    new-instance v0, LX/3K2;

    .line 548257
    iget-object v1, p0, LX/3K1;->j:Ljava/util/List;

    move-object v1, v1

    .line 548258
    iget-object v2, p0, LX/3K1;->k:[[[F

    move-object v2, v2

    .line 548259
    invoke-direct {v0, v1, v2}, LX/3K2;-><init>(Ljava/util/List;[[[F)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 6

    .prologue
    .line 548260
    check-cast p1, LX/3Jt;

    check-cast p2, LX/3Jt;

    check-cast p4, LX/9Ua;

    .line 548261
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_2

    .line 548262
    :cond_0
    iget-object v0, p1, LX/3Jt;->b:LX/3Ju;

    move-object v0, v0

    .line 548263
    const/4 v1, 0x0

    iget-object v2, v0, LX/3Ju;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 548264
    iget-object v1, v0, LX/3Ju;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Jv;

    invoke-virtual {v1, p4}, LX/3Jv;->a(LX/9Ua;)V

    .line 548265
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 548266
    :cond_1
    return-void

    .line 548267
    :cond_2
    iget-object v0, p1, LX/3Jt;->b:LX/3Ju;

    move-object v3, v0

    .line 548268
    iget-object v0, p2, LX/3Jt;->b:LX/3Ju;

    move-object v4, v0

    .line 548269
    const/4 v0, 0x0

    .line 548270
    iget-object v1, v3, LX/3Ju;->a:Ljava/util/List;

    move-object v1, v1

    .line 548271
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_1

    .line 548272
    iget-object v0, v3, LX/3Ju;->a:Ljava/util/List;

    move-object v0, v0

    .line 548273
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jv;

    .line 548274
    iget-object v1, v4, LX/3Ju;->a:Ljava/util/List;

    move-object v1, v1

    .line 548275
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Jv;

    invoke-virtual {v0, v1, p3, p4}, LX/3Jv;->a(LX/3Jv;FLX/9Ua;)V

    .line 548276
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method
