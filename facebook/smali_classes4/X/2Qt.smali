.class public LX/2Qt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Qt;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0en;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0en;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409027
    iput-object p1, p0, LX/2Qt;->a:Landroid/content/Context;

    .line 409028
    iput-object p2, p0, LX/2Qt;->b:LX/0en;

    .line 409029
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qt;
    .locals 5

    .prologue
    .line 409030
    sget-object v0, LX/2Qt;->c:LX/2Qt;

    if-nez v0, :cond_1

    .line 409031
    const-class v1, LX/2Qt;

    monitor-enter v1

    .line 409032
    :try_start_0
    sget-object v0, LX/2Qt;->c:LX/2Qt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409033
    if-eqz v2, :cond_0

    .line 409034
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409035
    new-instance p0, LX/2Qt;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v4

    check-cast v4, LX/0en;

    invoke-direct {p0, v3, v4}, LX/2Qt;-><init>(Landroid/content/Context;LX/0en;)V

    .line 409036
    move-object v0, p0

    .line 409037
    sput-object v0, LX/2Qt;->c:LX/2Qt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409038
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409039
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409040
    :cond_1
    sget-object v0, LX/2Qt;->c:LX/2Qt;

    return-object v0

    .line 409041
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 409043
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409044
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409045
    :goto_0
    return-void

    .line 409046
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 409047
    :cond_1
    invoke-static {p0}, LX/04M;->a(Ljava/io/File;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 409048
    iget-object v0, p0, LX/2Qt;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 409049
    const-string v1, "platform"

    invoke-static {v0, v1}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 409050
    invoke-static {v0}, LX/2Qt;->a(Ljava/io/File;)V

    .line 409051
    invoke-static {v0, p1}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 409052
    invoke-static {v0}, LX/2Qt;->a(Ljava/io/File;)V

    .line 409053
    move-object v0, v0

    .line 409054
    invoke-static {v0, p2}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
