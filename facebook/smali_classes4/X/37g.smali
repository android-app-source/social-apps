.class public LX/37g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field private static volatile n:LX/37g;


# instance fields
.field public c:LX/0AU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/37f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/37a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:LX/38X;

.field private final h:LX/38Z;

.field public final i:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "LX/38f;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/android/gms/cast/CastDevice;

.field public k:LX/37h;

.field public l:Landroid/content/Context;

.field public m:LX/2wX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 501881
    const-class v0, LX/37g;

    sput-object v0, LX/37g;->a:Ljava/lang/Class;

    .line 501882
    sget-object v0, LX/37a;->a:LX/0Tn;

    const-string v1, "route-id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/37g;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/37h;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501874
    new-instance v0, LX/38X;

    invoke-direct {v0, p0}, LX/38X;-><init>(LX/37g;)V

    iput-object v0, p0, LX/37g;->g:LX/38X;

    .line 501875
    new-instance v0, LX/38Z;

    invoke-direct {v0, p0}, LX/38Z;-><init>(LX/37g;)V

    iput-object v0, p0, LX/37g;->h:LX/38Z;

    .line 501876
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/37g;->i:Ljava/util/Vector;

    .line 501877
    iput-object p2, p0, LX/37g;->k:LX/37h;

    .line 501878
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/37g;->l:Landroid/content/Context;

    .line 501879
    iget-object v0, p0, LX/37g;->k:LX/37h;

    new-instance v1, LX/38b;

    invoke-direct {v1, p0}, LX/38b;-><init>(LX/37g;)V

    invoke-virtual {v0, v1}, LX/37h;->a(LX/38c;)V

    .line 501880
    return-void
.end method

.method public static a(LX/0QB;)LX/37g;
    .locals 7

    .prologue
    .line 501858
    sget-object v0, LX/37g;->n:LX/37g;

    if-nez v0, :cond_1

    .line 501859
    const-class v1, LX/37g;

    monitor-enter v1

    .line 501860
    :try_start_0
    sget-object v0, LX/37g;->n:LX/37g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501861
    if-eqz v2, :cond_0

    .line 501862
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501863
    new-instance p0, LX/37g;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/37h;->a(LX/0QB;)LX/37h;

    move-result-object v4

    check-cast v4, LX/37h;

    invoke-direct {p0, v3, v4}, LX/37g;-><init>(Landroid/content/Context;LX/37h;)V

    .line 501864
    invoke-static {v0}, LX/0AU;->a(LX/0QB;)LX/0AU;

    move-result-object v3

    check-cast v3, LX/0AU;

    invoke-static {v0}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v4

    check-cast v4, LX/37f;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/37a;->a(LX/0QB;)LX/37a;

    move-result-object v6

    check-cast v6, LX/37a;

    .line 501865
    iput-object v3, p0, LX/37g;->c:LX/0AU;

    iput-object v4, p0, LX/37g;->d:LX/37f;

    iput-object v5, p0, LX/37g;->e:LX/0So;

    iput-object v6, p0, LX/37g;->f:LX/37a;

    .line 501866
    move-object v0, p0

    .line 501867
    sput-object v0, LX/37g;->n:LX/37g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501868
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501869
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501870
    :cond_1
    sget-object v0, LX/37g;->n:LX/37g;

    return-object v0

    .line 501871
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501872
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/37g;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 501854
    iget-object v0, p0, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38f;

    .line 501855
    invoke-interface {v0, p1, p2}, LX/38f;->a(ILjava/lang/String;)V

    .line 501856
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 501857
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/37g;LX/38a;)V
    .locals 3

    .prologue
    .line 501883
    iget-object v0, p0, LX/37g;->h:LX/38Z;

    .line 501884
    iget-object v1, v0, LX/38Z;->b:LX/38a;

    if-ne v1, p1, :cond_1

    .line 501885
    :cond_0
    return-void

    .line 501886
    :cond_1
    iput-object p1, v0, LX/38Z;->b:LX/38a;

    .line 501887
    iget-object v1, v0, LX/38Z;->a:LX/37g;

    iget-object v1, v1, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    iget-object v1, v0, LX/38Z;->a:LX/37g;

    iget-object v1, v1, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/38f;

    .line 501888
    invoke-interface {v1}, LX/38f;->a()V

    .line 501889
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public static i(LX/37g;)V
    .locals 5

    .prologue
    .line 501844
    iget-object v0, p0, LX/37g;->m:LX/2wX;

    if-nez v0, :cond_0

    .line 501845
    iget-object v0, p0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    iget-object v1, p0, LX/37g;->g:LX/38X;

    new-instance v2, LX/7Yn;

    invoke-direct {v2, v0, v1}, LX/7Yn;-><init>(Lcom/google/android/gms/cast/CastDevice;LX/38Y;)V

    move-object v0, v2

    .line 501846
    new-instance v1, LX/2vz;

    iget-object v2, p0, LX/37g;->l:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v2, LX/7Yq;->b:LX/2vs;

    new-instance v3, LX/7Yo;

    invoke-direct {v3, v0}, LX/7Yo;-><init>(LX/7Yn;)V

    move-object v0, v3

    .line 501847
    invoke-virtual {v1, v2, v0}, LX/2vz;->a(LX/2vs;LX/2w6;)LX/2vz;

    move-result-object v0

    iget-object v1, p0, LX/37g;->g:LX/38X;

    invoke-virtual {v0, v1}, LX/2vz;->a(LX/1qf;)LX/2vz;

    move-result-object v0

    iget-object v1, p0, LX/37g;->g:LX/38X;

    invoke-virtual {v0, v1}, LX/2vz;->a(LX/1qg;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v0

    iput-object v0, p0, LX/37g;->m:LX/2wX;

    .line 501848
    :cond_0
    invoke-virtual {p0}, LX/37g;->d()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/37g;->d()LX/38p;

    move-result-object v0

    .line 501849
    iget-object v1, v0, LX/38p;->a:LX/38a;

    sget-object v2, LX/38a;->CONNECTING:LX/38a;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 501850
    if-nez v0, :cond_1

    .line 501851
    sget-object v0, LX/38a;->CONNECTING:LX/38a;

    invoke-static {p0, v0}, LX/37g;->a$redex0(LX/37g;LX/38a;)V

    .line 501852
    iget-object v0, p0, LX/37g;->m:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->e()V

    .line 501853
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501843
    iget-object v0, p0, LX/37g;->f:LX/37a;

    sget-object v1, LX/37g;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/37a;->b(LX/0Tn;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/384;)V
    .locals 2

    .prologue
    .line 501822
    iget-object v0, p1, LX/384;->p:Landroid/os/Bundle;

    move-object v0, v0

    .line 501823
    invoke-static {v0}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    .line 501824
    iget-object v1, p0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 501825
    if-eqz v0, :cond_0

    .line 501826
    invoke-virtual {p0}, LX/37g;->d()LX/38p;

    move-result-object v0

    .line 501827
    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/38p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 501828
    invoke-virtual {p0}, LX/37g;->d()LX/38p;

    .line 501829
    :goto_1
    return-void

    .line 501830
    :cond_0
    sget-object v0, LX/38a;->SELECTING:LX/38a;

    invoke-static {p0, v0}, LX/37g;->a$redex0(LX/37g;LX/38a;)V

    .line 501831
    iget-object v0, p0, LX/37g;->k:LX/37h;

    .line 501832
    iget-object v1, p1, LX/384;->p:Landroid/os/Bundle;

    move-object v1, v1

    .line 501833
    invoke-static {v1}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    .line 501834
    if-nez v1, :cond_3

    .line 501835
    const/4 v1, 0x0

    iput-object v1, v0, LX/37h;->h:LX/384;

    .line 501836
    :cond_1
    :goto_2
    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 501837
    :cond_3
    invoke-static {p1}, LX/37i;->a(LX/384;)V

    .line 501838
    invoke-static {}, LX/37i;->a()LX/384;

    move-result-object v1

    .line 501839
    iget-object p0, v1, LX/384;->c:Ljava/lang/String;

    move-object v1, p0

    .line 501840
    iget-object p0, p1, LX/384;->c:Ljava/lang/String;

    move-object p0, p0

    .line 501841
    if-ne v1, p0, :cond_1

    .line 501842
    invoke-static {}, LX/37i;->a()LX/384;

    move-result-object v1

    invoke-static {v0, v1}, LX/37h;->d(LX/37h;LX/384;)V

    goto :goto_2
.end method

.method public final a(LX/38f;)V
    .locals 1

    .prologue
    .line 501819
    iget-object v0, p0, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 501820
    iget-object v0, p0, LX/37g;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 501821
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 501811
    invoke-virtual {p0}, LX/37g;->d()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501812
    :cond_0
    :goto_0
    return-void

    .line 501813
    :cond_1
    iget-object v0, p0, LX/37g;->f:LX/37a;

    sget-object v1, LX/37g;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/37a;->a(LX/0Tn;)V

    .line 501814
    sget-object v0, LX/38a;->DISCONNECTED:LX/38a;

    invoke-static {p0, v0}, LX/37g;->a$redex0(LX/37g;LX/38a;)V

    .line 501815
    iget-object v0, p0, LX/37g;->m:LX/2wX;

    if-eqz v0, :cond_0

    .line 501816
    iget-object v0, p0, LX/37g;->m:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/37g;->m:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 501817
    :cond_2
    iget-object v0, p0, LX/37g;->m:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    .line 501818
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/37g;->m:LX/2wX;

    goto :goto_0
.end method

.method public final d()LX/38p;
    .locals 2

    .prologue
    .line 501808
    iget-object v0, p0, LX/37g;->h:LX/38Z;

    .line 501809
    new-instance v1, LX/38p;

    iget-object p0, v0, LX/38Z;->b:LX/38a;

    invoke-direct {v1, p0}, LX/38p;-><init>(LX/38a;)V

    move-object v0, v1

    .line 501810
    return-object v0
.end method
