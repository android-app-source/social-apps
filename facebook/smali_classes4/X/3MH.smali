.class public LX/3MH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3MH;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6jO;",
            ">;"
        }
    .end annotation
.end field

.field public b:J

.field public final c:LX/0SG;

.field public final d:LX/3MI;


# direct methods
.method public constructor <init>(LX/0SG;LX/3MI;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554026
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3MH;->a:Ljava/util/Map;

    .line 554027
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3MH;->b:J

    .line 554028
    iput-object p1, p0, LX/3MH;->c:LX/0SG;

    .line 554029
    iput-object p2, p0, LX/3MH;->d:LX/3MI;

    .line 554030
    return-void
.end method

.method public static a(LX/0QB;)LX/3MH;
    .locals 5

    .prologue
    .line 554031
    sget-object v0, LX/3MH;->e:LX/3MH;

    if-nez v0, :cond_1

    .line 554032
    const-class v1, LX/3MH;

    monitor-enter v1

    .line 554033
    :try_start_0
    sget-object v0, LX/3MH;->e:LX/3MH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 554034
    if-eqz v2, :cond_0

    .line 554035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 554036
    new-instance p0, LX/3MH;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/3MI;->a(LX/0QB;)LX/3MI;

    move-result-object v4

    check-cast v4, LX/3MI;

    invoke-direct {p0, v3, v4}, LX/3MH;-><init>(LX/0SG;LX/3MI;)V

    .line 554037
    move-object v0, p0

    .line 554038
    sput-object v0, LX/3MH;->e:LX/3MH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 554040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554041
    :cond_1
    sget-object v0, LX/3MH;->e:LX/3MH;

    return-object v0

    .line 554042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 554043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3MH;LX/6jO;)D
    .locals 8

    .prologue
    .line 554044
    iget-wide v2, p1, LX/6jO;->c:D

    iget-wide v4, p1, LX/6jO;->d:J

    iget-object v6, p0, LX/3MH;->c:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static/range {v2 .. v7}, LX/6jP;->a(DJJ)D

    move-result-wide v2

    move-wide v0, v2

    .line 554045
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double v2, v0, v2

    invoke-static {v2, v3}, LX/6jP;->a(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    move-wide v0, v2

    .line 554046
    return-wide v0
.end method

.method public static d(LX/3MH;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554047
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 554048
    iget-object v0, p0, LX/3MH;->d:LX/3MI;

    invoke-virtual {v0, p1}, LX/3MI;->d(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    .line 554049
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 554050
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jO;

    .line 554051
    if-eqz v1, :cond_0

    .line 554052
    iget-object v5, p0, LX/3MH;->a:Ljava/util/Map;

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554053
    invoke-static {p0, v1}, LX/3MH;->b(LX/3MH;LX/6jO;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 554054
    :cond_1
    return-object v2
.end method

.method private static e(LX/3MH;Ljava/lang/String;)D
    .locals 8

    .prologue
    .line 554055
    iget-object v2, p0, LX/3MH;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/3MH;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0x55d4a80

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 554056
    if-eqz v0, :cond_0

    .line 554057
    iget-object v2, p0, LX/3MH;->d:LX/3MI;

    invoke-virtual {v2, p1}, LX/3MI;->a(Ljava/lang/String;)LX/6jO;

    move-result-object v2

    .line 554058
    iget-wide v4, v2, LX/6jO;->d:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_3

    .line 554059
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 554060
    :goto_1
    move-wide v0, v2

    .line 554061
    :goto_2
    return-wide v0

    .line 554062
    :cond_0
    iget-object v0, p0, LX/3MH;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jO;

    .line 554063
    if-eqz v0, :cond_1

    invoke-static {p0, v0}, LX/3MH;->b(LX/3MH;LX/6jO;)D

    move-result-wide v0

    goto :goto_2

    :cond_1
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 554064
    :cond_3
    iget-object v3, p0, LX/3MH;->a:Ljava/util/Map;

    invoke-interface {v3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554065
    invoke-static {p0, v2}, LX/3MH;->b(LX/3MH;LX/6jO;)D

    move-result-wide v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)D
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 554066
    invoke-static {p0, p1}, LX/3MH;->e(LX/3MH;Ljava/lang/String;)D

    move-result-wide v0

    .line 554067
    cmpl-double v2, v0, v4

    if-nez v2, :cond_0

    .line 554068
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/3MH;->e(LX/3MH;Ljava/lang/String;)D

    move-result-wide v0

    .line 554069
    cmpl-double v2, v0, v4

    if-nez v2, :cond_0

    if-eqz p2, :cond_0

    .line 554070
    invoke-static {p0, p2}, LX/3MH;->e(LX/3MH;Ljava/lang/String;)D

    move-result-wide v0

    .line 554071
    :cond_0
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)D
    .locals 4

    .prologue
    .line 554072
    invoke-static {p0, p1}, LX/3MH;->e(LX/3MH;Ljava/lang/String;)D

    move-result-wide v0

    .line 554073
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
