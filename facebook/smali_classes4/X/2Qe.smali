.class public LX/2Qe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile c:LX/2Qe;


# instance fields
.field public final b:LX/2Qi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 408728
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pending_media_uploads."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/2Qg;->a:LX/0U1;

    .line 408729
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 408730
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2Qg;->b:LX/0U1;

    .line 408731
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408732
    aput-object v2, v0, v1

    sput-object v0, LX/2Qe;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Qi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408734
    iput-object p1, p0, LX/2Qe;->b:LX/2Qi;

    .line 408735
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qe;
    .locals 4

    .prologue
    .line 408736
    sget-object v0, LX/2Qe;->c:LX/2Qe;

    if-nez v0, :cond_1

    .line 408737
    const-class v1, LX/2Qe;

    monitor-enter v1

    .line 408738
    :try_start_0
    sget-object v0, LX/2Qe;->c:LX/2Qe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408739
    if-eqz v2, :cond_0

    .line 408740
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408741
    new-instance p0, LX/2Qe;

    invoke-static {v0}, LX/2Qi;->a(LX/0QB;)LX/2Qi;

    move-result-object v3

    check-cast v3, LX/2Qi;

    invoke-direct {p0, v3}, LX/2Qe;-><init>(LX/2Qi;)V

    .line 408742
    move-object v0, p0

    .line 408743
    sput-object v0, LX/2Qe;->c:LX/2Qe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408744
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408745
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408746
    :cond_1
    sget-object v0, LX/2Qe;->c:LX/2Qe;

    return-object v0

    .line 408747
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/2Qe;Ljava/lang/String;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/katana/platform/PendingMediaUpload;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 408749
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 408750
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 408751
    const-string v1, "pending_media_uploads"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 408752
    if-eqz p1, :cond_1

    .line 408753
    sget-object v1, LX/2Qg;->a:LX/0U1;

    .line 408754
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 408755
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 408756
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    .line 408757
    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    .line 408758
    :goto_0
    iget-object v1, p0, LX/2Qe;->b:LX/2Qi;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/2Qe;->a:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 408759
    :try_start_0
    sget-object v0, LX/2Qg;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 408760
    sget-object v2, LX/2Qg;->b:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 408761
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 408762
    new-instance v3, Lcom/facebook/katana/platform/PendingMediaUpload;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/katana/platform/PendingMediaUpload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 408763
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 408764
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v4, v5

    move-object v3, v5

    goto :goto_0
.end method
