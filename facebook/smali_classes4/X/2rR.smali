.class public LX/2rR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471933
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/2rR;->a:LX/0Px;

    .line 471934
    iget-object v0, p0, LX/2rR;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 471935
    return-void

    .line 471936
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471930
    iget-object v0, p0, LX/2rR;->a:LX/0Px;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 471937
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/4Yr;

    .line 471938
    iget-object v0, p0, LX/2rR;->a:LX/0Px;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 471939
    :cond_0
    :goto_0
    return-void

    .line 471940
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    .line 471941
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471942
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 471943
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "seen_state"

    invoke-virtual {v1, p0, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 471944
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471931
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471929
    const-string v0, "MarkNotificationsSeenMutatingVisitor"

    return-object v0
.end method
