.class public abstract LX/3Jj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/3Jd;",
        "M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/animation/Interpolator;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 547950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547951
    iput-object v1, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    .line 547952
    iput-object v1, p0, LX/3Jj;->b:Ljava/util/List;

    .line 547953
    iput v0, p0, LX/3Jj;->c:I

    .line 547954
    iput v0, p0, LX/3Jj;->d:I

    .line 547955
    return-void
.end method

.method public constructor <init>(Ljava/util/List;[[[F)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;[[[F)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 547929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547930
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 547931
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    move v1, v2

    .line 547932
    :goto_0
    if-ge v1, v3, :cond_0

    .line 547933
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jd;

    .line 547934
    iget-object v4, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    invoke-interface {v0}, LX/3Jd;->a()I

    move-result v5

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 547935
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 547936
    :cond_0
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iput v0, p0, LX/3Jj;->c:I

    .line 547937
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    add-int/lit8 v1, v3, -0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iput v0, p0, LX/3Jj;->d:I

    .line 547938
    const/4 p1, 0x1

    const/4 v1, 0x0

    .line 547939
    if-nez p2, :cond_1

    .line 547940
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 547941
    :goto_1
    move-object v0, v0

    .line 547942
    iput-object v0, p0, LX/3Jj;->b:Ljava/util/List;

    .line 547943
    return-void

    .line 547944
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 547945
    array-length v3, p2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_2

    .line 547946
    aget-object v4, p2, v0

    .line 547947
    new-instance v5, LX/3Jk;

    aget-object v6, v4, v1

    aget v6, v6, v1

    aget-object v7, v4, v1

    aget v7, v7, p1

    aget-object v8, v4, p1

    aget v8, v8, v1

    aget-object v4, v4, p1

    aget v4, v4, p1

    invoke-direct {v5, v6, v7, v8, v4}, LX/3Jk;-><init>(FFFF)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 547948
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 547949
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 547915
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method


# virtual methods
.method public final a(FLjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FTM;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 547916
    iget-object v0, p0, LX/3Jj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LX/3Jj;->c:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 547917
    :cond_0
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    iget v2, p0, LX/3Jj;->c:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jd;

    invoke-virtual {p0, v0, v1, v3, p2}, LX/3Jj;->a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V

    .line 547918
    :goto_0
    return-void

    .line 547919
    :cond_1
    iget v0, p0, LX/3Jj;->d:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    .line 547920
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    iget v2, p0, LX/3Jj;->d:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jd;

    invoke-virtual {p0, v0, v1, v3, p2}, LX/3Jj;->a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V

    goto :goto_0

    .line 547921
    :cond_2
    iget-object v0, p0, LX/3Jj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 547922
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v2, :cond_5

    .line 547923
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gez v0, :cond_4

    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_4

    .line 547924
    :cond_3
    iget-object v0, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jd;

    .line 547925
    iget-object v1, p0, LX/3Jj;->a:Landroid/util/SparseArray;

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Jd;

    move-object v2, v0

    .line 547926
    :goto_2
    invoke-interface {v2}, LX/3Jd;->a()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-interface {v1}, LX/3Jd;->a()I

    move-result v4

    invoke-interface {v2}, LX/3Jd;->a()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float v4, v0, v4

    .line 547927
    iget-object v0, p0, LX/3Jj;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Interpolator;

    invoke-interface {v0, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    invoke-virtual {p0, v2, v1, v0, p2}, LX/3Jj;->a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V

    goto :goto_0

    .line 547928
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move-object v2, v1

    goto :goto_2
.end method

.method public abstract a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;FTM;)V"
        }
    .end annotation
.end method
