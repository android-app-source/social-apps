.class public LX/280;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/27v;

.field private final c:LX/27x;

.field public final d:LX/281;

.field public final e:LX/27y;

.field private final f:LX/27z;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/27v;LX/27x;LX/27y;LX/27z;)V
    .locals 3

    .prologue
    .line 373542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373543
    iput-object p1, p0, LX/280;->a:Landroid/content/Context;

    .line 373544
    iput-object p2, p0, LX/280;->b:LX/27v;

    .line 373545
    iput-object p3, p0, LX/280;->c:LX/27x;

    .line 373546
    iput-object p4, p0, LX/280;->e:LX/27y;

    .line 373547
    iput-object p5, p0, LX/280;->f:LX/27z;

    .line 373548
    new-instance v0, LX/281;

    iget-object v1, p0, LX/280;->b:LX/27v;

    iget-object v2, p0, LX/280;->c:LX/27x;

    invoke-direct {v0, v1, v2}, LX/281;-><init>(LX/27v;LX/27x;)V

    iput-object v0, p0, LX/280;->d:LX/281;

    .line 373549
    return-void
.end method

.method private a(Landroid/database/Cursor;LX/4hB;)V
    .locals 12

    .prologue
    .line 373505
    const-string v0, "COL_SFDID"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 373506
    const-string v1, "COL_SFDID_CREATION_TS"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 373507
    const-string v1, "COL_SFDID_GP"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 373508
    const-string v1, "COL_SFDID_GA"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 373509
    if-ltz v0, :cond_1

    if-ltz v2, :cond_1

    if-ltz v3, :cond_1

    if-ltz v5, :cond_1

    .line 373510
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 373511
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 373512
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 373513
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 373514
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 373515
    new-instance v0, LX/4hA;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, LX/4hA;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 373516
    iput-object v0, p2, LX/4hB;->b:LX/4hA;

    .line 373517
    iget-object v0, p0, LX/280;->d:LX/281;

    .line 373518
    iget-object v6, p2, LX/4hB;->c:LX/4hA;

    move-object v6, v6

    .line 373519
    iget-object v7, p2, LX/4hB;->b:LX/4hA;

    move-object v7, v7

    .line 373520
    if-nez v6, :cond_2

    .line 373521
    sget-object v6, LX/4h9;->NEW:LX/4h9;

    .line 373522
    iput-object v6, p2, LX/31y;->a:LX/4h9;

    .line 373523
    iget-object v6, v0, LX/281;->a:LX/27v;

    invoke-virtual {v6, v7}, LX/27v;->a(LX/4hA;)V

    .line 373524
    :goto_0
    return-void

    .line 373525
    :cond_0
    sget-object v0, LX/4h9;->NULL:LX/4h9;

    .line 373526
    iput-object v0, p2, LX/31y;->a:LX/4h9;

    .line 373527
    goto :goto_0

    .line 373528
    :cond_1
    sget-object v0, LX/4h9;->FAILED:LX/4h9;

    .line 373529
    iput-object v0, p2, LX/31y;->a:LX/4h9;

    .line 373530
    goto :goto_0

    .line 373531
    :cond_2
    iget-object v8, v7, LX/4hA;->a:Ljava/lang/String;

    iget-object v9, v6, LX/4hA;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 373532
    iget-wide v8, v6, LX/4hA;->b:J

    iget-wide v10, v7, LX/4hA;->b:J

    cmp-long v6, v8, v10

    if-lez v6, :cond_3

    .line 373533
    sget-object v6, LX/4h9;->MISMATCH_UPDATED:LX/4h9;

    .line 373534
    iput-object v6, p2, LX/31y;->a:LX/4h9;

    .line 373535
    iget-object v6, v0, LX/281;->a:LX/27v;

    invoke-virtual {v6, v7}, LX/27v;->a(LX/4hA;)V

    goto :goto_0

    .line 373536
    :cond_3
    sget-object v6, LX/4h9;->MISMATCH_NOT_UPDATED:LX/4h9;

    .line 373537
    iput-object v6, p2, LX/31y;->a:LX/4h9;

    .line 373538
    goto :goto_0

    .line 373539
    :cond_4
    sget-object v6, LX/4h9;->SAME:LX/4h9;

    .line 373540
    iput-object v6, p2, LX/31y;->a:LX/4h9;

    .line 373541
    goto :goto_0
.end method

.method public static b(LX/280;Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 373450
    new-instance v7, LX/4h7;

    iget-object v0, p0, LX/280;->b:LX/27v;

    invoke-virtual {v0}, LX/27v;->b()LX/282;

    move-result-object v0

    const-string v1, "contentproviders"

    invoke-direct {v7, p1, v0, v1}, LX/4h7;-><init>(Ljava/lang/String;LX/282;Ljava/lang/String;)V

    .line 373451
    new-instance v8, LX/4hB;

    iget-object v0, p0, LX/280;->b:LX/27v;

    invoke-virtual {v0}, LX/27v;->d()LX/4hA;

    move-result-object v0

    invoke-direct {v8, p1, v0}, LX/4hB;-><init>(Ljava/lang/String;LX/4hA;)V

    .line 373452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".provider.phoneid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373453
    iget-object v0, p0, LX/280;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 373454
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 373455
    if-nez v0, :cond_1

    move v0, v6

    .line 373456
    :cond_0
    :goto_0
    return v0

    .line 373457
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/280;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 373458
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 373459
    if-nez v3, :cond_3

    .line 373460
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "content provider package name conflict. Expected:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Found: No provider info."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373461
    :catch_0
    move-exception v1

    move-object v2, v1

    move v1, v6

    .line 373462
    :goto_1
    :try_start_1
    iget-object v3, p0, LX/280;->f:LX/27z;

    if-eqz v3, :cond_2

    .line 373463
    iget-object v3, p0, LX/280;->f:LX/27z;

    const-string v4, "PhoneIdRequester"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/27z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373464
    :cond_2
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    move v0, v1

    .line 373465
    :goto_2
    iget-object v1, p0, LX/280;->e:LX/27y;

    if-eqz v1, :cond_0

    .line 373466
    iget-object v1, p0, LX/280;->e:LX/27y;

    invoke-virtual {v1, v7}, LX/27y;->a(LX/31y;)V

    .line 373467
    iget-object v1, p0, LX/280;->e:LX/27y;

    invoke-virtual {v1, v8}, LX/27y;->a(LX/31y;)V

    goto :goto_0

    .line 373468
    :cond_3
    :try_start_2
    iget-object v4, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 373469
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "content provider package name conflict. Expected:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Found:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 373470
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1

    .line 373471
    :cond_4
    :try_start_3
    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 373472
    invoke-static {v2}, LX/283;->a(Landroid/content/pm/PackageInfo;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 373473
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "app signature mismatch"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 373474
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "content://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 373475
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 373476
    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_7

    .line 373477
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "empty Cursor object"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 373478
    :cond_7
    invoke-virtual {v7}, LX/31y;->d()V

    .line 373479
    invoke-virtual {v8}, LX/31y;->d()V

    .line 373480
    const/4 v9, 0x0

    .line 373481
    const-string v10, "COL_PHONE_ID"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 373482
    const-string v11, "COL_TIMESTAMP"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 373483
    if-ltz v10, :cond_a

    if-ltz v11, :cond_a

    .line 373484
    invoke-interface {v2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 373485
    invoke-interface {v2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 373486
    if-eqz v10, :cond_9

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_9

    if-eqz v11, :cond_9

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_9

    .line 373487
    new-instance v9, LX/282;

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-direct {v9, v10, v11, v12}, LX/282;-><init>(Ljava/lang/String;J)V

    .line 373488
    iput-object v9, v7, LX/4h7;->b:LX/282;

    .line 373489
    iget-object v9, p0, LX/280;->d:LX/281;

    invoke-virtual {v9, v7}, LX/281;->a(LX/4h7;)V

    .line 373490
    const/4 v9, 0x1

    .line 373491
    :goto_3
    move v1, v9
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 373492
    :try_start_4
    invoke-direct {p0, v2, v8}, LX/280;->a(Landroid/database/Cursor;LX/4hB;)V

    .line 373493
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, LX/280;->f:LX/27z;

    if-eqz v3, :cond_8

    .line 373494
    iget-object v3, p0, LX/280;->f:LX/27z;

    const-string v4, "PhoneIdRequester"

    const-string v5, "Multiple records in cursor"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, LX/27z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 373495
    :cond_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 373496
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    move v0, v1

    .line 373497
    goto/16 :goto_2

    .line 373498
    :catch_1
    move-exception v2

    goto/16 :goto_1

    .line 373499
    :cond_9
    sget-object v10, LX/4h9;->NULL:LX/4h9;

    .line 373500
    iput-object v10, v7, LX/31y;->a:LX/4h9;

    .line 373501
    goto :goto_3

    .line 373502
    :cond_a
    sget-object v10, LX/4h9;->FAILED:LX/4h9;

    .line 373503
    iput-object v10, v7, LX/31y;->a:LX/4h9;

    .line 373504
    goto :goto_3
.end method

.method private c()Z
    .locals 6

    .prologue
    .line 373414
    iget-object v1, p0, LX/280;->b:LX/27v;

    monitor-enter v1

    .line 373415
    :try_start_0
    iget-object v0, p0, LX/280;->b:LX/27v;

    invoke-virtual {v0}, LX/27v;->b()LX/282;

    move-result-object v0

    if-nez v0, :cond_0

    .line 373416
    iget-object v0, p0, LX/280;->b:LX/27v;

    new-instance v2, LX/282;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, LX/282;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, LX/27v;->a(LX/282;)V

    .line 373417
    const/4 v0, 0x1

    monitor-exit v1

    .line 373418
    :goto_0
    return v0

    .line 373419
    :cond_0
    monitor-exit v1

    .line 373420
    const/4 v0, 0x0

    goto :goto_0

    .line 373421
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 373422
    iget-object v0, p0, LX/280;->b:LX/27v;

    invoke-virtual {v0}, LX/27v;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/280;->b:LX/27v;

    invoke-virtual {v0}, LX/27v;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373423
    :goto_0
    return-void

    .line 373424
    :cond_0
    invoke-direct {p0}, LX/280;->c()Z

    .line 373425
    iget-object v1, p0, LX/280;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    .line 373426
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 373427
    iget-object v3, p0, LX/280;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 373428
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 373429
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 373430
    :try_start_0
    iget-object v5, p0, LX/280;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v6, 0x40

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 373431
    invoke-static {v1}, LX/283;->a(Landroid/content/pm/PackageInfo;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 373432
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 373433
    :cond_2
    move-object v1, v2

    .line 373434
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 373435
    invoke-static {p0, v1}, LX/280;->b(LX/280;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 373436
    const/4 v6, 0x0

    .line 373437
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 373438
    const-string v5, "com.facebook.GET_PHONE_ID"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 373439
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 373440
    move-object v5, v4

    .line 373441
    iget-object v4, p0, LX/280;->a:Landroid/content/Context;

    const/4 v7, 0x0

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const/high16 v9, 0x8000000

    invoke-static {v4, v7, v8, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 373442
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 373443
    const-string v8, "auth"

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 373444
    move-object v11, v7

    .line 373445
    new-instance v8, LX/4h7;

    iget-object v4, p0, LX/280;->b:LX/27v;

    invoke-virtual {v4}, LX/27v;->b()LX/282;

    move-result-object v4

    const-string v7, "broadcasts"

    invoke-direct {v8, v1, v4, v7}, LX/4h7;-><init>(Ljava/lang/String;LX/282;Ljava/lang/String;)V

    .line 373446
    iget-object v4, p0, LX/280;->a:Landroid/content/Context;

    new-instance v7, LX/4h8;

    iget-object v9, p0, LX/280;->d:LX/281;

    iget-object v10, p0, LX/280;->e:LX/27y;

    invoke-direct {v7, v9, v10, v8}, LX/4h8;-><init>(LX/281;LX/27y;LX/4h7;)V

    const/4 v9, 0x1

    move-object v8, v6

    move-object v10, v6

    invoke-virtual/range {v4 .. v11}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 373447
    goto :goto_2

    .line 373448
    :cond_4
    goto/16 :goto_0

    .line 373449
    :catch_0
    goto/16 :goto_1
.end method
