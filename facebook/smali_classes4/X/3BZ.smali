.class public final LX/3BZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPlace;

.field public final synthetic c:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 0

    .prologue
    .line 528712
    iput-object p1, p0, LX/3BZ;->c:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    iput-object p2, p0, LX/3BZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3b43894a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 528713
    iget-object v0, p0, LX/3BZ;->c:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_story_attachment"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 528714
    iget-object v0, p0, LX/3BZ;->c:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "checkin_story_map_opened_on_savable_story"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "native_newsfeed"

    .line 528715
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 528716
    move-object v1, v1

    .line 528717
    const-string v2, "tracking"

    iget-object v3, p0, LX/3BZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "place_id"

    iget-object v3, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 528718
    iget-object v0, p0, LX/3BZ;->c:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Zi;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "native_story"

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v4

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v6

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3BZ;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->j()Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual/range {v1 .. v9}, LX/6Zi;->a(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 528719
    const v0, 0x1cb9fa29

    invoke-static {v0, v10}, LX/02F;->a(II)V

    return-void

    .line 528720
    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method
