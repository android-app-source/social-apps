.class public LX/2NG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2N8;

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(LX/2N8;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399031
    iput-object p1, p0, LX/2NG;->a:LX/2N8;

    .line 399032
    iput-object p2, p0, LX/2NG;->b:LX/03V;

    .line 399033
    return-void
.end method

.method public static a(LX/0QB;)LX/2NG;
    .locals 1

    .prologue
    .line 399029
    invoke-static {p0}, LX/2NG;->b(LX/0QB;)LX/2NG;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399026
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->NULL:LX/0nH;

    if-eq v0, v1, :cond_0

    .line 399027
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 399028
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 398948
    if-nez p0, :cond_0

    .line 398949
    const-string v0, "{}"

    .line 398950
    :goto_0
    return-object v0

    .line 398951
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    .line 398952
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 398953
    const-string v4, "request_id"

    iget-object v6, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "service_name"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    const-string v7, "booking_status"

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-nez v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v6, v7, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "localized_booking_status"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "start_time_sec"

    iget-wide v8, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-virtual {v4, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v4

    const-string v6, "page_name"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->g:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "page_id"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->h:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "localized_price"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "profile_pic_url"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "user_id"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->k:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "user_name"

    iget-object v7, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->l:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398954
    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 398955
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 398956
    const-string v2, "booking_request_detail"

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v2, "pending_booking_request_count"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v0

    const-string v2, "requested_booking_request_count"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v0

    const-string v2, "confirmed_booking_request_count"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v0

    const-string v2, "page_id"

    iget-object v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v2, "page_name"

    iget-object v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398957
    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 398958
    :cond_1
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1
.end method

.method public static b(LX/0QB;)LX/2NG;
    .locals 3

    .prologue
    .line 399024
    new-instance v2, LX/2NG;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/2NG;-><init>(LX/2N8;LX/03V;)V

    .line 399025
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadBookingRequests;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 398959
    if-eqz p1, :cond_0

    const-string v0, "{}"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398960
    :cond_0
    const/4 v0, 0x0

    .line 398961
    :goto_0
    return-object v0

    .line 398962
    :cond_1
    iget-object v0, p0, LX/2NG;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398963
    const-string v1, "booking_request_detail"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 398964
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 398965
    iget-object v3, p0, LX/2NG;->b:LX/03V;

    const-string v4, "booking"

    const-string v5, "BookingRequestDetail deserializer received empty json String."

    invoke-virtual {v3, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 398966
    new-instance v3, LX/6fc;

    invoke-direct {v3}, LX/6fc;-><init>()V

    invoke-virtual {v3}, LX/6fc;->a()Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    move-result-object v3

    .line 398967
    :goto_1
    move-object v1, v3

    .line 398968
    new-instance v2, LX/6fo;

    invoke-direct {v2}, LX/6fo;-><init>()V

    .line 398969
    invoke-virtual {v2, v1}, LX/6fo;->a(Lcom/facebook/messaging/model/threads/BookingRequestDetail;)LX/6fo;

    move-result-object v1

    const-string v2, "pending_booking_request_count"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 398970
    iput v2, v1, LX/6fo;->b:I

    .line 398971
    move-object v1, v1

    .line 398972
    const-string v2, "requested_booking_request_count"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 398973
    iput v2, v1, LX/6fo;->c:I

    .line 398974
    move-object v1, v1

    .line 398975
    const-string v2, "confirmed_booking_request_count"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 398976
    iput v2, v1, LX/6fo;->d:I

    .line 398977
    move-object v1, v1

    .line 398978
    const-string v2, "page_id"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    .line 398979
    iput-object v2, v1, LX/6fo;->f:Ljava/lang/String;

    .line 398980
    move-object v1, v1

    .line 398981
    const-string v2, "page_name"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 398982
    iput-object v0, v1, LX/6fo;->e:Ljava/lang/String;

    .line 398983
    move-object v0, v1

    .line 398984
    invoke-virtual {v0}, LX/6fo;->a()Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    move-result-object v0

    goto :goto_0

    .line 398985
    :cond_2
    iget-object v3, p0, LX/2NG;->a:LX/2N8;

    invoke-virtual {v3, v1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 398986
    new-instance v4, LX/6fc;

    invoke-direct {v4}, LX/6fc;-><init>()V

    const-string v5, "request_id"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 398987
    iput-object v5, v4, LX/6fc;->a:Ljava/lang/String;

    .line 398988
    move-object v4, v4

    .line 398989
    const-string v5, "service_name"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 398990
    iput-object v5, v4, LX/6fc;->b:Ljava/lang/String;

    .line 398991
    move-object v4, v4

    .line 398992
    const-string v5, "booking_status"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v5

    .line 398993
    iput-object v5, v4, LX/6fc;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 398994
    move-object v4, v4

    .line 398995
    const-string v5, "localized_booking_status"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    .line 398996
    iput-object v5, v4, LX/6fc;->d:Ljava/lang/String;

    .line 398997
    move-object v4, v4

    .line 398998
    const-string v5, "start_time_sec"

    .line 398999
    if-eqz v3, :cond_3

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v3}, LX/0lF;->k()LX/0nH;

    move-result-object v7

    sget-object v8, LX/0nH;->NULL:LX/0nH;

    if-eq v7, v8, :cond_3

    .line 399000
    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-virtual {v7}, LX/0lF;->D()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 399001
    :goto_2
    move-object v5, v7

    .line 399002
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 399003
    iput-wide v5, v4, LX/6fc;->e:J

    .line 399004
    move-object v4, v4

    .line 399005
    const-string v5, "page_name"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 399006
    iput-object v5, v4, LX/6fc;->f:Ljava/lang/String;

    .line 399007
    move-object v4, v4

    .line 399008
    const-string v5, "page_id"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 399009
    iput-object v5, v4, LX/6fc;->g:Ljava/lang/String;

    .line 399010
    move-object v4, v4

    .line 399011
    const-string v5, "localized_price"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 399012
    iput-object v5, v4, LX/6fc;->h:Ljava/lang/String;

    .line 399013
    move-object v4, v4

    .line 399014
    const-string v5, "profile_pic_url"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 399015
    iput-object v5, v4, LX/6fc;->i:Ljava/lang/String;

    .line 399016
    move-object v4, v4

    .line 399017
    const-string v5, "user_id"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 399018
    iput-object v5, v4, LX/6fc;->j:Ljava/lang/String;

    .line 399019
    move-object v4, v4

    .line 399020
    const-string v5, "user_name"

    invoke-static {v3, v5}, LX/2NG;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 399021
    iput-object v3, v4, LX/6fc;->k:Ljava/lang/String;

    .line 399022
    move-object v3, v4

    .line 399023
    invoke-virtual {v3}, LX/6fc;->a()Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    move-result-object v3

    goto/16 :goto_1

    :cond_3
    const/4 v7, 0x0

    goto :goto_2
.end method
