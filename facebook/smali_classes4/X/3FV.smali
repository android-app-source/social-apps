.class public final LX/3FV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3FQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/D4s;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/D6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 539642
    invoke-direct {p0, p1, p2, v0, v0}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;)V

    .line 539643
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/04D;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 539644
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V

    .line 539645
    return-void
.end method

.method private constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/3FQ;",
            "LX/D4s;",
            ")V"
        }
    .end annotation

    .prologue
    .line 539646
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;)V

    .line 539647
    return-void
.end method

.method private constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/3FQ;",
            "LX/D4s;",
            "LX/D6L;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 539648
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v0 .. v8}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V

    .line 539649
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/3FQ;",
            "LX/D4s;",
            "LX/D6L;",
            "LX/0JG;",
            "Ljava/lang/String;",
            "LX/04D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 539650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539651
    iput-object p1, p0, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 539652
    iput-object p2, p0, LX/3FV;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 539653
    iput-object p3, p0, LX/3FV;->c:LX/3FQ;

    .line 539654
    iput-object p4, p0, LX/3FV;->d:LX/D4s;

    .line 539655
    iput-object p5, p0, LX/3FV;->e:LX/D6L;

    .line 539656
    iput-object p6, p0, LX/3FV;->f:LX/0JG;

    .line 539657
    iput-object p7, p0, LX/3FV;->g:Ljava/lang/String;

    .line 539658
    iput-object p8, p0, LX/3FV;->h:LX/04D;

    .line 539659
    return-void
.end method
