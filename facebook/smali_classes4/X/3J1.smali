.class public final LX/3J1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/2oL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final c:LX/093;

.field public final d:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final f:LX/04D;

.field public final g:LX/3FX;


# direct methods
.method public constructor <init>(LX/1KL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/2oL;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/093;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/04D;",
            "LX/3FX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 547396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547397
    iput-object p1, p0, LX/3J1;->a:LX/1KL;

    .line 547398
    iput-object p2, p0, LX/3J1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 547399
    iput-object p3, p0, LX/3J1;->c:LX/093;

    .line 547400
    iput-object p4, p0, LX/3J1;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 547401
    iput-object p5, p0, LX/3J1;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 547402
    iput-object p6, p0, LX/3J1;->f:LX/04D;

    .line 547403
    iput-object p7, p0, LX/3J1;->g:LX/3FX;

    .line 547404
    return-void
.end method
