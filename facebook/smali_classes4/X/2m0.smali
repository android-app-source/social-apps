.class public LX/2m0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/bookmark/model/BookmarksGroup;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2m0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459244
    return-void
.end method

.method public static a(LX/0QB;)LX/2m0;
    .locals 3

    .prologue
    .line 459245
    sget-object v0, LX/2m0;->a:LX/2m0;

    if-nez v0, :cond_1

    .line 459246
    const-class v1, LX/2m0;

    monitor-enter v1

    .line 459247
    :try_start_0
    sget-object v0, LX/2m0;->a:LX/2m0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 459248
    if-eqz v2, :cond_0

    .line 459249
    :try_start_1
    new-instance v0, LX/2m0;

    invoke-direct {v0}, LX/2m0;-><init>()V

    .line 459250
    move-object v0, v0

    .line 459251
    sput-object v0, LX/2m0;->a:LX/2m0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459252
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 459253
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 459254
    :cond_1
    sget-object v0, LX/2m0;->a:LX/2m0;

    return-object v0

    .line 459255
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 459256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 459257
    check-cast p1, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 459258
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 459259
    invoke-virtual {p1}, Lcom/facebook/bookmark/model/BookmarksGroup;->e()Ljava/util/List;

    move-result-object v0

    new-instance v1, LX/EhM;

    invoke-direct {v1, p0}, LX/EhM;-><init>(LX/2m0;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    .line 459260
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ids"

    const/16 v3, 0x2c

    invoke-static {v3}, LX/0PO;->on(C)LX/0PO;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459261
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459262
    new-instance v0, LX/14N;

    const-string v1, "bookmarkSetFavorites"

    const-string v2, "GET"

    const-string v3, "method/bookmarks.set"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459263
    const/4 v0, 0x0

    return-object v0
.end method
