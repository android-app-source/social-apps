.class public LX/2Qy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409327
    const-class v0, LX/2Qy;

    sput-object v0, LX/2Qy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409329
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qy;
    .locals 1

    .prologue
    .line 409324
    new-instance v0, LX/2Qy;

    invoke-direct {v0}, LX/2Qy;-><init>()V

    .line 409325
    move-object v0, v0

    .line 409326
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 409314
    check-cast p1, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;

    .line 409315
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409316
    iget-object v0, p1, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409317
    iget-object v0, p1, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;->l:Ljava/util/HashMap;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409318
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409319
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409320
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 409321
    invoke-virtual {p1, v4}, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a(Ljava/util/List;)V

    .line 409322
    const-string v0, "me/%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 409323
    new-instance v0, LX/14N;

    const-string v1, "graphOpenGraphActionPublish"

    const-string v2, "POST"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 409313
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
