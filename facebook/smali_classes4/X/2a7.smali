.class public LX/2a7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2a7;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/presence/PresenceManager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Uh;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/presence/PresenceManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423375
    new-instance v0, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;

    invoke-direct {v0, p0}, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;-><init>(LX/2a7;)V

    iput-object v0, p0, LX/2a7;->e:Ljava/lang/Runnable;

    .line 423376
    iput-object p1, p0, LX/2a7;->a:LX/0Ot;

    .line 423377
    iput-object p2, p0, LX/2a7;->b:LX/0Ot;

    .line 423378
    iput-object p3, p0, LX/2a7;->c:Ljava/util/concurrent/Executor;

    .line 423379
    iput-object p4, p0, LX/2a7;->d:LX/0Uh;

    .line 423380
    return-void
.end method

.method public static a(LX/0QB;)LX/2a7;
    .locals 7

    .prologue
    .line 423381
    sget-object v0, LX/2a7;->f:LX/2a7;

    if-nez v0, :cond_1

    .line 423382
    const-class v1, LX/2a7;

    monitor-enter v1

    .line 423383
    :try_start_0
    sget-object v0, LX/2a7;->f:LX/2a7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423384
    if-eqz v2, :cond_0

    .line 423385
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423386
    new-instance v5, LX/2a7;

    const/16 v3, 0xfa5

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x1021

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v6, p0, v3, v4}, LX/2a7;-><init>(LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V

    .line 423387
    move-object v0, v5

    .line 423388
    sput-object v0, LX/2a7;->f:LX/2a7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423389
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423390
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423391
    :cond_1
    sget-object v0, LX/2a7;->f:LX/2a7;

    return-object v0

    .line 423392
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423393
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 3

    .prologue
    .line 423394
    const-string v0, "/p_a_req"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423395
    iget-object v0, p0, LX/2a7;->c:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/2a7;->e:Ljava/lang/Runnable;

    const v2, -0x2cacaf4d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 423396
    :cond_0
    return-void
.end method
