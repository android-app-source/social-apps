.class public LX/259;
.super LX/25A;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25A",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private f:LX/0TD;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FpY;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0TD;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0TD;",
            "Ljava/util/List",
            "<",
            "LX/FpY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 368261
    invoke-direct {p0, p1}, LX/25A;-><init>(Landroid/content/Context;)V

    .line 368262
    iput-object p2, p0, LX/259;->f:LX/0TD;

    .line 368263
    iput-object p3, p0, LX/259;->g:Ljava/util/List;

    .line 368264
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, LX/259;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/259;->h:Ljava/util/concurrent/CountDownLatch;

    .line 368265
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 368266
    :try_start_0
    iget-object v0, p0, LX/259;->h:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368267
    return-void

    .line 368268
    :catch_0
    move-exception v0

    .line 368269
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final d()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 368270
    iget-object v0, p0, LX/259;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpY;

    .line 368271
    new-instance v2, LX/Fpa;

    invoke-direct {v2, p0, v0}, LX/Fpa;-><init>(LX/259;LX/FpY;)V

    .line 368272
    iget-object v3, p0, LX/259;->f:LX/0TD;

    new-instance v4, Lcom/facebook/tablet/sideshow/loader/SideshowLoader$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/facebook/tablet/sideshow/loader/SideshowLoader$2;-><init>(LX/259;LX/FpY;LX/Fpa;)V

    const v0, 0x1af0e18f

    invoke-static {v3, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 368273
    :cond_0
    invoke-direct {p0}, LX/259;->x()V

    .line 368274
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 368275
    invoke-super {p0}, LX/25A;->g()V

    .line 368276
    iget-object v0, p0, LX/259;->h:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 368277
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 368278
    :cond_0
    return-void
.end method
