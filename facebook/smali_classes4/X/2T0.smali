.class public final LX/2T0;
.super LX/0aB;
.source ""

# interfaces
.implements LX/0dN;


# instance fields
.field public final synthetic a:LX/2St;


# direct methods
.method public constructor <init>(LX/2St;)V
    .locals 0

    .prologue
    .line 413675
    iput-object p1, p0, LX/2T0;->a:LX/2St;

    invoke-direct {p0}, LX/0aB;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/2T0;Ljava/util/Collection;LX/2T6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/2Sw;",
            ">;",
            "LX/2T6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 413676
    iget-object v0, p0, LX/2T0;->a:LX/2St;

    iget-object v0, v0, LX/2St;->e:LX/2AY;

    invoke-virtual {v0}, LX/2AY;->a()V

    .line 413677
    iget-object v0, p0, LX/2T0;->a:LX/2St;

    invoke-static {v0, p1, p2}, LX/2St;->a$redex0(LX/2St;Ljava/util/Collection;LX/2T6;)V

    .line 413678
    return-void
.end method


# virtual methods
.method public final a(LX/0Uh;I)V
    .locals 2

    .prologue
    .line 413679
    iget-object v0, p0, LX/2T0;->a:LX/2St;

    iget-object v0, v0, LX/2St;->i:LX/0Xu;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    sget-object v1, LX/2T6;->GATEKEEPER_CHANGED:LX/2T6;

    invoke-static {p0, v0, v1}, LX/2T0;->a$redex0(LX/2T0;Ljava/util/Collection;LX/2T6;)V

    .line 413680
    return-void
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 2

    .prologue
    .line 413681
    iget-object v0, p0, LX/2T0;->a:LX/2St;

    iget-object v0, v0, LX/2St;->h:LX/0Xu;

    invoke-interface {v0, p2}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    sget-object v1, LX/2T6;->PREFKEY_CHANGED:LX/2T6;

    invoke-static {p0, v0, v1}, LX/2T0;->a$redex0(LX/2T0;Ljava/util/Collection;LX/2T6;)V

    .line 413682
    return-void
.end method
