.class public LX/2Nt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FHz;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Nt;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400112
    return-void
.end method

.method public static a(LX/0QB;)LX/2Nt;
    .locals 3

    .prologue
    .line 400113
    sget-object v0, LX/2Nt;->a:LX/2Nt;

    if-nez v0, :cond_1

    .line 400114
    const-class v1, LX/2Nt;

    monitor-enter v1

    .line 400115
    :try_start_0
    sget-object v0, LX/2Nt;->a:LX/2Nt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400116
    if-eqz v2, :cond_0

    .line 400117
    :try_start_1
    new-instance v0, LX/2Nt;

    invoke-direct {v0}, LX/2Nt;-><init>()V

    .line 400118
    move-object v0, v0

    .line 400119
    sput-object v0, LX/2Nt;->a:LX/2Nt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400122
    :cond_1
    sget-object v0, LX/2Nt;->a:LX/2Nt;

    return-object v0

    .line 400123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 400125
    check-cast p1, LX/FHz;

    .line 400126
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 400127
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fbid"

    iget-wide v4, p1, LX/FHz;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 400128
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message_id"

    iget-object v3, p1, LX/FHz;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 400129
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "updateMessageForHiRes"

    .line 400130
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 400131
    move-object v1, v1

    .line 400132
    const-string v2, "POST"

    .line 400133
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 400134
    move-object v1, v1

    .line 400135
    const-string v2, "me/message_forced_fetch"

    .line 400136
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 400137
    move-object v1, v1

    .line 400138
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 400139
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 400140
    move-object v0, v1

    .line 400141
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 400142
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 400143
    move-object v0, v0

    .line 400144
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 400145
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 400146
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 400147
    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
