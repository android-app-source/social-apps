.class public LX/39V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/17Q;

.field private final b:LX/2mt;

.field private final c:LX/1xP;

.field private final d:LX/1CK;


# direct methods
.method public constructor <init>(LX/17Q;LX/2mt;LX/1xP;LX/1CK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523091
    iput-object p1, p0, LX/39V;->a:LX/17Q;

    .line 523092
    iput-object p2, p0, LX/39V;->b:LX/2mt;

    .line 523093
    iput-object p3, p0, LX/39V;->c:LX/1xP;

    .line 523094
    iput-object p4, p0, LX/39V;->d:LX/1CK;

    .line 523095
    return-void
.end method

.method public static a(LX/0QB;)LX/39V;
    .locals 7

    .prologue
    .line 523096
    const-class v1, LX/39V;

    monitor-enter v1

    .line 523097
    :try_start_0
    sget-object v0, LX/39V;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523098
    sput-object v2, LX/39V;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523099
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523100
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523101
    new-instance p0, LX/39V;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v3

    check-cast v3, LX/17Q;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v4

    check-cast v4, LX/2mt;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v5

    check-cast v5, LX/1xP;

    invoke-static {v0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v6

    check-cast v6, LX/1CK;

    invoke-direct {p0, v3, v4, v5, v6}, LX/39V;-><init>(LX/17Q;LX/2mt;LX/1xP;LX/1CK;)V

    .line 523102
    move-object v0, p0

    .line 523103
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523104
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523105
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/util/Map;LX/1Pq;)V
    .locals 8
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ">(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 523107
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 523108
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 523109
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 523110
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 523111
    :goto_0
    invoke-static {p2}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v2, v1}, LX/17Q;->c(ZLX/0lF;)Ljava/util/Map;

    move-result-object v6

    .line 523112
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 523113
    invoke-interface {v6, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 523114
    :cond_0
    invoke-static {p1}, LX/1vZ;->a(Landroid/view/View;)LX/1vY;

    move-result-object v2

    .line 523115
    if-eqz v2, :cond_1

    .line 523116
    invoke-static {v2}, LX/1vZ;->b(LX/1vY;)LX/162;

    move-result-object v2

    .line 523117
    if-eqz v2, :cond_1

    .line 523118
    const-string v3, "tn"

    invoke-interface {v6, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523119
    :cond_1
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 523120
    if-nez v3, :cond_3

    move-object v2, v5

    .line 523121
    :goto_1
    iget-object v4, p0, LX/39V;->d:LX/1CK;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v4, v2, p5, v7}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 523122
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v4

    .line 523123
    iput-object p3, v4, LX/47H;->a:Ljava/lang/String;

    .line 523124
    move-object v4, v4

    .line 523125
    invoke-static {v0, v2, p3}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v0

    .line 523126
    iput-object v0, v4, LX/47H;->d:LX/47G;

    .line 523127
    move-object v7, v4

    .line 523128
    iget-object v0, p0, LX/39V;->c:LX/1xP;

    const-string v2, "applink_"

    invoke-static {p5, v3, v2}, LX/2yJ;->a(Ljava/lang/Object;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/1xP;->a(LX/162;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 523129
    iput-object v0, v7, LX/47H;->b:Landroid/os/Bundle;

    .line 523130
    move-object v0, v7

    .line 523131
    invoke-virtual {v0, v6}, LX/47H;->a(Ljava/util/Map;)LX/47H;

    move-result-object v0

    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 523132
    iget-object v1, p0, LX/39V;->c:LX/1xP;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 523133
    :try_start_0
    iget-object v3, v1, LX/1xP;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v3, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;LX/47I;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_4

    .line 523134
    :goto_2
    return-void

    :cond_2
    move-object v1, v5

    .line 523135
    goto :goto_0

    .line 523136
    :cond_3
    iget-object v2, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 523137
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_1

    .line 523138
    :catch_0
    move-exception v3

    .line 523139
    iget-object v4, v1, LX/1xP;->f:LX/03V;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/1xP;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_openUrl"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error calling IFeedIntentBuilder.handleNativeUrl"

    invoke-static {v5, v6}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v5

    .line 523140
    iput-object v3, v5, LX/0VK;->c:Ljava/lang/Throwable;

    .line 523141
    move-object v3, v5

    .line 523142
    invoke-virtual {v3}, LX/0VK;->g()LX/0VG;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/03V;->a(LX/0VG;)V

    .line 523143
    :cond_4
    iget-object v3, v0, LX/47I;->a:Ljava/lang/String;

    move-object v3, v3

    .line 523144
    iget-object v4, v0, LX/47I;->b:Landroid/os/Bundle;

    move-object v4, v4

    .line 523145
    iget-object v5, v0, LX/47I;->c:LX/0P1;

    move-object v5, v5

    .line 523146
    invoke-virtual {v1, v2, v3, v4, v5}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    goto :goto_2
.end method
