.class public LX/378;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/378;


# instance fields
.field private final a:LX/0Zb;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Z1",
            "<",
            "LX/7Ru;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 500820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500821
    iput-object p1, p0, LX/378;->a:LX/0Zb;

    .line 500822
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/378;->b:Ljava/util/List;

    .line 500823
    return-void
.end method

.method public static a(LX/378;LX/7Rp;)LX/0oG;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 500809
    iget-object v0, p0, LX/378;->a:LX/0Zb;

    iget-object v1, p1, LX/7Rp;->value:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 500810
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 500811
    const/4 v0, 0x0

    .line 500812
    :goto_0
    return-object v0

    .line 500813
    :cond_0
    iget-object v1, p0, LX/378;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 500814
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 500815
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Z1;

    .line 500816
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    .line 500817
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 500818
    :cond_2
    invoke-virtual {v1}, LX/6Z1;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Ru;

    invoke-interface {v1, v0}, LX/7Ru;->a(LX/0oG;)V

    goto :goto_1

    .line 500819
    :cond_3
    const-string v1, "watch_and_go"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/378;
    .locals 4

    .prologue
    .line 500796
    sget-object v0, LX/378;->c:LX/378;

    if-nez v0, :cond_1

    .line 500797
    const-class v1, LX/378;

    monitor-enter v1

    .line 500798
    :try_start_0
    sget-object v0, LX/378;->c:LX/378;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 500799
    if-eqz v2, :cond_0

    .line 500800
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 500801
    new-instance p0, LX/378;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/378;-><init>(LX/0Zb;)V

    .line 500802
    move-object v0, p0

    .line 500803
    sput-object v0, LX/378;->c:LX/378;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500804
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 500805
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 500806
    :cond_1
    sget-object v0, LX/378;->c:LX/378;

    return-object v0

    .line 500807
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 500808
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0oG;)V
    .locals 0
    .param p0    # LX/0oG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 500793
    if-nez p0, :cond_0

    .line 500794
    :goto_0
    return-void

    .line 500795
    :cond_0
    invoke-virtual {p0}, LX/0oG;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7Ru;)V
    .locals 2

    .prologue
    .line 500784
    new-instance v0, LX/6Z1;

    invoke-direct {v0, p1}, LX/6Z1;-><init>(Ljava/lang/Object;)V

    .line 500785
    iget-object v1, p0, LX/378;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500786
    :goto_0
    return-void

    .line 500787
    :cond_0
    iget-object v1, p0, LX/378;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(LX/7Ru;)V
    .locals 2

    .prologue
    .line 500790
    new-instance v0, LX/6Z1;

    invoke-direct {v0, p1}, LX/6Z1;-><init>(Ljava/lang/Object;)V

    .line 500791
    iget-object v1, p0, LX/378;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 500792
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 500788
    sget-object v0, LX/7Rp;->SESSION_FINISHED:LX/7Rp;

    invoke-static {p0, v0}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v0

    invoke-static {v0}, LX/378;->a(LX/0oG;)V

    .line 500789
    return-void
.end method
