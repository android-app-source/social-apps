.class public final enum LX/2Rb;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Rb;",
        ">;",
        "LX/0QK",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Rb;

.field public static final enum INSTANCE:LX/2Rb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 409942
    new-instance v0, LX/2Rb;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/2Rb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Rb;->INSTANCE:LX/2Rb;

    .line 409943
    const/4 v0, 0x1

    new-array v0, v0, [LX/2Rb;

    sget-object v1, LX/2Rb;->INSTANCE:LX/2Rb;

    aput-object v1, v0, v2

    sput-object v0, LX/2Rb;->$VALUES:[LX/2Rb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 409935
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Rb;
    .locals 1

    .prologue
    .line 409941
    const-class v0, LX/2Rb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Rb;

    return-object v0
.end method

.method public static values()[LX/2Rb;
    .locals 1

    .prologue
    .line 409940
    sget-object v0, LX/2Rb;->$VALUES:[LX/2Rb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Rb;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409939
    invoke-virtual {p0, p1}, LX/2Rb;->apply(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final apply(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 409937
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409938
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409936
    const-string v0, "Functions.toStringFunction()"

    return-object v0
.end method
