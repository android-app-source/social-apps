.class public final LX/2A5;
.super Landroid/database/ContentObserver;
.source ""


# instance fields
.field private a:LX/2A6;


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/2A6;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 376921
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 376922
    iput-object p2, p0, LX/2A5;->a:LX/2A6;

    .line 376923
    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 1

    .prologue
    .line 376924
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2A5;->onChange(ZLandroid/net/Uri;)V

    .line 376925
    return-void
.end method

.method public final onChange(ZLandroid/net/Uri;)V
    .locals 10

    .prologue
    .line 376926
    iget-object v0, p0, LX/2A5;->a:LX/2A6;

    .line 376927
    iget-object v1, v0, LX/2A6;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v3, LX/0X5;->eA:J

    invoke-interface {v1, v3, v4}, LX/0W4;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 376928
    :goto_0
    return-void

    .line 376929
    :cond_0
    new-instance v1, LX/BAR;

    sget-object v2, LX/BAT;->PHOTO_NOTIFICATION:LX/BAT;

    iget-object v3, v0, LX/2A6;->a:Landroid/content/Context;

    const v4, 0x7f083bfd

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/BAR;-><init>(LX/BAT;Ljava/lang/String;)V

    .line 376930
    new-instance v2, LX/BAS;

    invoke-direct {v2, v1}, LX/BAS;-><init>(LX/BAR;)V

    move-object v1, v2

    .line 376931
    iget-object v2, v0, LX/2A6;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const/4 v9, 0x0

    .line 376932
    iget-object v5, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->p:LX/2AJ;

    .line 376933
    iget-object v6, v1, LX/BAS;->a:LX/BAT;

    move-object v6, v6

    .line 376934
    invoke-virtual {v6}, LX/BAT;->isUniqueNotification()Z

    move-result v6

    invoke-static {v5, v1, v6}, LX/2AJ;->a(LX/2AJ;LX/BAS;Z)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    move-object v6, v6

    .line 376935
    iget-object v5, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->q:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 376936
    iget-object v5, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->c()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 376937
    iget-object v5, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->l:LX/0TD;

    new-instance v9, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper$3;

    invoke-direct {v9, v2, v6, v7, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper$3;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/graphql/model/GraphQLStory;J)V

    const v6, 0x71932789

    invoke-static {v5, v9, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 376938
    :goto_1
    goto :goto_0

    .line 376939
    :cond_1
    invoke-virtual {v2, v7, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, LX/2AJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/2nq;

    move-result-object v5

    .line 376940
    const/4 v6, 0x1

    new-array v6, v6, [LX/2nq;

    aput-object v5, v6, v9

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-static {v2, v5, v7, v8, v9}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a$redex0(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;JZ)V

    goto :goto_1
.end method
