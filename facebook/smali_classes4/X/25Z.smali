.class public final LX/25Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field public d:I

.field private e:Landroid/view/animation/Interpolator;

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 369783
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/25Z;-><init>(IIILandroid/view/animation/Interpolator;)V

    .line 369784
    return-void
.end method

.method private constructor <init>(IIILandroid/view/animation/Interpolator;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369775
    const/4 v0, -0x1

    iput v0, p0, LX/25Z;->d:I

    .line 369776
    iput-boolean v1, p0, LX/25Z;->f:Z

    .line 369777
    iput v1, p0, LX/25Z;->g:I

    .line 369778
    iput p1, p0, LX/25Z;->a:I

    .line 369779
    iput p2, p0, LX/25Z;->b:I

    .line 369780
    iput p3, p0, LX/25Z;->c:I

    .line 369781
    iput-object p4, p0, LX/25Z;->e:Landroid/view/animation/Interpolator;

    .line 369782
    return-void
.end method

.method public static a$redex0(LX/25Z;Landroid/support/v7/widget/RecyclerView;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 369744
    iget v0, p0, LX/25Z;->d:I

    if-ltz v0, :cond_0

    .line 369745
    iget v0, p0, LX/25Z;->d:I

    .line 369746
    const/4 v1, -0x1

    iput v1, p0, LX/25Z;->d:I

    .line 369747
    invoke-static {p1, v0}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/support/v7/widget/RecyclerView;I)V

    .line 369748
    iput-boolean v5, p0, LX/25Z;->f:Z

    .line 369749
    :goto_0
    return-void

    .line 369750
    :cond_0
    iget-boolean v0, p0, LX/25Z;->f:Z

    if-eqz v0, :cond_4

    .line 369751
    invoke-direct {p0}, LX/25Z;->b()V

    .line 369752
    iget-object v0, p0, LX/25Z;->e:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_3

    .line 369753
    iget v0, p0, LX/25Z;->c:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2

    .line 369754
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    iget v1, p0, LX/25Z;->a:I

    iget v2, p0, LX/25Z;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->b(II)V

    .line 369755
    :goto_1
    iget v0, p0, LX/25Z;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/25Z;->g:I

    .line 369756
    iget v0, p0, LX/25Z;->g:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    .line 369757
    const-string v0, "RecyclerView"

    const-string v1, "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369758
    :cond_1
    iput-boolean v5, p0, LX/25Z;->f:Z

    goto :goto_0

    .line 369759
    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    iget v1, p0, LX/25Z;->a:I

    iget v2, p0, LX/25Z;->b:I

    iget v3, p0, LX/25Z;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(III)V

    goto :goto_1

    .line 369760
    :cond_3
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    iget v1, p0, LX/25Z;->a:I

    iget v2, p0, LX/25Z;->b:I

    iget v3, p0, LX/25Z;->c:I

    iget-object v4, p0, LX/25Z;->e:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a(IIILandroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 369761
    :cond_4
    iput v5, p0, LX/25Z;->g:I

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 369769
    iget-object v0, p0, LX/25Z;->e:Landroid/view/animation/Interpolator;

    if-eqz v0, :cond_0

    iget v0, p0, LX/25Z;->c:I

    if-gtz v0, :cond_0

    .line 369770
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If you provide an interpolator, you must set a positive duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369771
    :cond_0
    iget v0, p0, LX/25Z;->c:I

    if-gtz v0, :cond_1

    .line 369772
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Scroll duration must be a positive number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369773
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IIILandroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 369763
    iput p1, p0, LX/25Z;->a:I

    .line 369764
    iput p2, p0, LX/25Z;->b:I

    .line 369765
    iput p3, p0, LX/25Z;->c:I

    .line 369766
    iput-object p4, p0, LX/25Z;->e:Landroid/view/animation/Interpolator;

    .line 369767
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/25Z;->f:Z

    .line 369768
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 369762
    iget v0, p0, LX/25Z;->d:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
