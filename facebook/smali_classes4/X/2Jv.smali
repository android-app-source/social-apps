.class public LX/2Jv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile e:LX/2Jv;


# instance fields
.field public final b:LX/0Zb;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 393295
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "malware_scan_finished"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Jv;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393291
    iput-object p1, p0, LX/2Jv;->b:LX/0Zb;

    .line 393292
    iput-object p2, p0, LX/2Jv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 393293
    iput-object p3, p0, LX/2Jv;->d:Landroid/content/Context;

    .line 393294
    return-void
.end method

.method public static a(LX/0QB;)LX/2Jv;
    .locals 6

    .prologue
    .line 393277
    sget-object v0, LX/2Jv;->e:LX/2Jv;

    if-nez v0, :cond_1

    .line 393278
    const-class v1, LX/2Jv;

    monitor-enter v1

    .line 393279
    :try_start_0
    sget-object v0, LX/2Jv;->e:LX/2Jv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393280
    if-eqz v2, :cond_0

    .line 393281
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393282
    new-instance p0, LX/2Jv;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/2Jv;-><init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V

    .line 393283
    move-object v0, p0

    .line 393284
    sput-object v0, LX/2Jv;->e:LX/2Jv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393285
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393286
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393287
    :cond_1
    sget-object v0, LX/2Jv;->e:LX/2Jv;

    return-object v0

    .line 393288
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Jv;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 393230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Logging error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393231
    iget-object v0, p0, LX/2Jv;->b:LX/0Zb;

    const-string v1, "malware_transaction_exception_event"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 393232
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393233
    const-string v1, "malware_detector"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 393234
    const-string v1, "MalwareDetector"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 393235
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 393236
    :cond_0
    return-void
.end method

.method public static b(LX/2Jv;Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393261
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 393262
    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 393263
    invoke-static {p1, v0}, LX/2BO;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 393264
    :goto_0
    return-object v0

    .line 393265
    :catch_0
    move-exception v0

    .line 393266
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception during normal: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Jv;->a(LX/2Jv;Ljava/lang/String;)V

    .line 393267
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 393268
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 393269
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 393270
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 393271
    const/4 v1, 0x0

    .line 393272
    :try_start_1
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const/16 p0, 0x1000

    invoke-virtual {v2, v0, p0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 393273
    :goto_2
    if-eqz v0, :cond_0

    .line 393274
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_1
    move-object v0, v1

    goto :goto_2

    .line 393275
    :cond_1
    invoke-static {p1, v3}, LX/2BO;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 393276
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 8

    .prologue
    .line 393237
    iget-object v0, p0, LX/2Jv;->d:Landroid/content/Context;

    .line 393238
    iget-object v1, p0, LX/2Jv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2Jv;->a:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 393239
    :try_start_0
    invoke-static {p0, v0}, LX/2Jv;->b(LX/2Jv;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 393240
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 393241
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 393242
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 393243
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 393244
    const-string v6, "name"

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 393245
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 393246
    iget-object v7, v2, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    array-length p1, v7

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    aget-object v0, v7, v2

    .line 393247
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 393248
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 393249
    :cond_0
    const-string v2, "permissions"

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 393250
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    goto :goto_0

    .line 393251
    :catch_0
    move-exception v2

    .line 393252
    const-string v3, "MalwareDetector"

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 393253
    :cond_1
    :goto_2
    const/4 v0, 0x1

    return v0

    .line 393254
    :catch_1
    move-exception v1

    .line 393255
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception during complex: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/2Jv;->a(LX/2Jv;Ljava/lang/String;)V

    goto :goto_2

    .line 393256
    :cond_2
    :try_start_3
    iget-object v2, p0, LX/2Jv;->b:LX/0Zb;

    const-string v4, "android_malware_detected_event"

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 393257
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 393258
    const-string v4, "malware_detector"

    invoke-virtual {v2, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 393259
    const-string v4, "description"

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 393260
    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method
