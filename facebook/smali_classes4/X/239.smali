.class public LX/239;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 361934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;
    .locals 4

    .prologue
    .line 361935
    new-instance v0, Lcom/facebook/feed/model/GapFeedEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/239;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v2

    .line 361936
    move-object v2, v2

    .line 361937
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/239;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/facebook/feed/model/GapFeedEdge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361938
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 361939
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 361940
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;
    .locals 4

    .prologue
    .line 361941
    new-instance v0, Lcom/facebook/feed/model/GapFeedEdge;

    invoke-static {p1}, LX/239;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 361942
    move-object v2, p0

    .line 361943
    invoke-static {p2}, LX/239;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p5}, Lcom/facebook/feed/model/GapFeedEdge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361944
    move-object v1, p3

    .line 361945
    invoke-static {v0, v1}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 361946
    invoke-static {p4}, LX/239;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 361947
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 361948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":gap"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 361949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(gap)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
