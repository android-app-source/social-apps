.class public final LX/2ff;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2fd;

.field public final synthetic b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/2fd;)V
    .locals 0

    .prologue
    .line 447042
    iput-object p1, p0, LX/2ff;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iput-object p2, p0, LX/2ff;->a:LX/2fd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5dd8975b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 447043
    iget-object v0, p0, LX/2ff;->a:LX/2fd;

    iget-object v0, v0, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447044
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 447045
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 447046
    iget-object v2, p0, LX/2ff;->a:LX/2fd;

    iget-object v2, v2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-static {v2, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "saved_collection_ego_item_action_link_clicked"

    invoke-static {v2, v0, v3}, LX/17Q;->b(LX/0lF;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 447047
    iget-object v2, p0, LX/2ff;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 447048
    const v0, -0x101ba02f

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
