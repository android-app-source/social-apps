.class public LX/2dH;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "Lcom/facebook/prefs/shared/FbSharedPreferences;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2dH;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 443002
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 443003
    new-instance v0, LX/2dI;

    invoke-direct {v0, p0, p1}, LX/2dI;-><init>(LX/2dH;LX/0Ot;)V

    iput-object v0, p0, LX/2dH;->a:LX/0Ot;

    .line 443004
    return-void
.end method

.method public static a(LX/0QB;)LX/2dH;
    .locals 4

    .prologue
    .line 442988
    sget-object v0, LX/2dH;->b:LX/2dH;

    if-nez v0, :cond_1

    .line 442989
    const-class v1, LX/2dH;

    monitor-enter v1

    .line 442990
    :try_start_0
    sget-object v0, LX/2dH;->b:LX/2dH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 442991
    if-eqz v2, :cond_0

    .line 442992
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 442993
    new-instance v3, LX/2dH;

    const/16 p0, 0xf9a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2dH;-><init>(LX/0Ot;)V

    .line 442994
    move-object v0, v3

    .line 442995
    sput-object v0, LX/2dH;->b:LX/2dH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442996
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 442997
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 442998
    :cond_1
    sget-object v0, LX/2dH;->b:LX/2dH;

    return-object v0

    .line 442999
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 443000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 443001
    invoke-virtual {p0}, LX/2dH;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442987
    iget-object v0, p0, LX/2dH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 442986
    invoke-virtual {p0}, LX/2dH;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
