.class public LX/2Q8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 407732
    invoke-static {}, LX/2Q9;->a()LX/2QB;

    move-result-object v0

    invoke-virtual {v0}, LX/2QB;->b()LX/3SI;

    move-result-object v0

    invoke-virtual {v0}, LX/3SI;->b()LX/0Xv;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Q8;-><init>(LX/0Xu;)V

    .line 407733
    return-void
.end method

.method public constructor <init>(LX/0Xu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 407734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407735
    iput-object p1, p0, LX/2Q8;->a:LX/0Xu;

    .line 407736
    return-void
.end method


# virtual methods
.method public b()LX/18f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/18f",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407737
    iget-object v0, p0, LX/2Q8;->c:Ljava/util/Comparator;

    if-eqz v0, :cond_0

    .line 407738
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 407739
    check-cast v0, Ljava/util/List;

    .line 407740
    iget-object v2, p0, LX/2Q8;->c:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 407741
    :cond_0
    iget-object v0, p0, LX/2Q8;->b:Ljava/util/Comparator;

    if-eqz v0, :cond_2

    .line 407742
    invoke-static {}, LX/2Q9;->a()LX/2QB;

    move-result-object v0

    invoke-virtual {v0}, LX/2QB;->b()LX/3SI;

    move-result-object v0

    invoke-virtual {v0}, LX/3SI;->b()LX/0Xv;

    move-result-object v1

    .line 407743
    iget-object v0, p0, LX/2Q8;->b:Ljava/util/Comparator;

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->e()LX/1sm;

    move-result-object v0

    iget-object v2, p0, LX/2Q8;->a:LX/0Xu;

    invoke-interface {v2}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1sm;->d(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    .line 407744
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407745
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v1, v3, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 407746
    :cond_1
    iput-object v1, p0, LX/2Q8;->a:LX/0Xu;

    .line 407747
    :cond_2
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-static {v0}, LX/18f;->c(LX/0Xu;)LX/18f;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/0Xu;)LX/2Q8;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<+TK;+TV;>;)",
            "LX/2Q8",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407748
    invoke-interface {p1}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407749
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0, v2, v0}, LX/2Q8;->b(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q8;

    goto :goto_0

    .line 407750
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q8;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Iterable",
            "<+TV;>;)",
            "LX/2Q8",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407751
    if-nez p1, :cond_0

    .line 407752
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "null key in entry: null="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, LX/0Ph;->c(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407753
    :cond_0
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 407754
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 407755
    invoke-static {p1, v2}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 407756
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407757
    :cond_1
    return-object p0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q8;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "LX/2Q8",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407758
    invoke-static {p1, p2}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 407759
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 407760
    return-object p0
.end method
