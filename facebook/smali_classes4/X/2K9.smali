.class public LX/2K9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2K9;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393502
    iput-object p1, p0, LX/2K9;->a:Landroid/content/Context;

    .line 393503
    iput-object p2, p0, LX/2K9;->b:LX/0Uh;

    .line 393504
    return-void
.end method

.method public static a(LX/0QB;)LX/2K9;
    .locals 5

    .prologue
    .line 393488
    sget-object v0, LX/2K9;->c:LX/2K9;

    if-nez v0, :cond_1

    .line 393489
    const-class v1, LX/2K9;

    monitor-enter v1

    .line 393490
    :try_start_0
    sget-object v0, LX/2K9;->c:LX/2K9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393491
    if-eqz v2, :cond_0

    .line 393492
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393493
    new-instance p0, LX/2K9;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/2K9;-><init>(Landroid/content/Context;LX/0Uh;)V

    .line 393494
    move-object v0, p0

    .line 393495
    sput-object v0, LX/2K9;->c:LX/2K9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393496
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393497
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393498
    :cond_1
    sget-object v0, LX/2K9;->c:LX/2K9;

    return-object v0

    .line 393499
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 393505
    if-eqz p1, :cond_0

    move v0, v1

    .line 393506
    :goto_0
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, LX/2K9;->a:Landroid/content/Context;

    const-class v4, Lcom/facebook/deeplinking/activity/PagesDeepLinkingAliasActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393507
    iget-object v3, p0, LX/2K9;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 393508
    return-void

    .line 393509
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static a$redex0(LX/2K9;)V
    .locals 3

    .prologue
    .line 393482
    iget-object v0, p0, LX/2K9;->b:LX/0Uh;

    const/16 v1, 0x230

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-direct {p0, v0}, LX/2K9;->a(Z)V

    .line 393483
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 393486
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2K9;->a(Z)V

    .line 393487
    return-void
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 393484
    invoke-static {p0}, LX/2K9;->a$redex0(LX/2K9;)V

    .line 393485
    return-void
.end method
