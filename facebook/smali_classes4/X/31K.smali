.class public LX/31K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wo;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wr;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2V0;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wi;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wt;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1wo;",
            ">;",
            "Landroid/os/Handler;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/1wr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2V0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wt;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486844
    iput-object p1, p0, LX/31K;->c:LX/0Ot;

    .line 486845
    iput-object p2, p0, LX/31K;->a:Landroid/os/Handler;

    .line 486846
    iput-object p3, p0, LX/31K;->b:Ljava/util/concurrent/ExecutorService;

    .line 486847
    iput-object p4, p0, LX/31K;->d:LX/0Ot;

    .line 486848
    iput-object p5, p0, LX/31K;->e:LX/0Ot;

    .line 486849
    iput-object p6, p0, LX/31K;->f:LX/0Ot;

    .line 486850
    iput-object p7, p0, LX/31K;->g:LX/0Ot;

    .line 486851
    iput-object p8, p0, LX/31K;->h:Lcom/facebook/content/SecureContextHelper;

    .line 486852
    return-void
.end method

.method public static a(LX/0QB;)LX/31K;
    .locals 10

    .prologue
    .line 486853
    new-instance v1, LX/31K;

    const/16 v2, 0x141

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x11db

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11dd

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x11df

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x11e2

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v1 .. v9}, LX/31K;-><init>(LX/0Ot;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;)V

    .line 486854
    move-object v0, v1

    .line 486855
    return-object v0
.end method
