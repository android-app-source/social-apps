.class public LX/2Cz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1gy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/1gy;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383572
    iput-object p1, p0, LX/2Cz;->a:Lcom/facebook/content/SecureContextHelper;

    .line 383573
    iput-object p2, p0, LX/2Cz;->b:LX/0Ot;

    .line 383574
    iput-object p3, p0, LX/2Cz;->c:LX/0SG;

    .line 383575
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cz;
    .locals 1

    .prologue
    .line 383576
    invoke-static {p0}, LX/2Cz;->b(LX/0QB;)LX/2Cz;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Cz;
    .locals 4

    .prologue
    .line 383577
    new-instance v2, LX/2Cz;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0x2c2

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v3, v1}, LX/2Cz;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;)V

    .line 383578
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 383579
    if-nez p1, :cond_0

    move v0, v1

    .line 383580
    :goto_0
    return v0

    .line 383581
    :cond_0
    if-nez p2, :cond_1

    .line 383582
    const-string p2, ""

    .line 383583
    :cond_1
    iget-object v0, p0, LX/2Cz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1gy;

    iget-wide v2, v0, LX/1gy;->c:J

    .line 383584
    iget-object v0, p0, LX/2Cz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 383585
    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x421d5ec4b0000000L    # 3.1536E10

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 383586
    const-string v0, "Could not validate certificate: current time:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DATE_ERROR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v2, v4

    if-lez v0, :cond_4

    .line 383587
    :cond_2
    if-nez p2, :cond_3

    const-string p2, ""

    :cond_3
    const/4 v4, 0x0

    .line 383588
    const v0, 0x7f080068

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 383589
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 383590
    if-eqz v1, :cond_5

    .line 383591
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 383592
    :goto_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0313a9

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 383593
    const v2, 0x7f0d2d5f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 383594
    const v0, 0x7f0d02a7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 383595
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383596
    new-instance v0, LX/0ju;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v3, 0x1030073

    invoke-direct {v1, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->b(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080015

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    const-string v1, "Change date settings"

    new-instance v2, LX/EQK;

    invoke-direct {v2, p0, p1}, LX/EQK;-><init>(LX/2Cz;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    invoke-virtual {v0, v1, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 383597
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 383598
    goto/16 :goto_0

    :cond_5
    move-object v1, v0

    .line 383599
    goto :goto_1
.end method
