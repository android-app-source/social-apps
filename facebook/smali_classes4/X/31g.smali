.class public LX/31g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/31g;


# instance fields
.field private a:J

.field private b:J

.field private final c:LX/0SG;

.field private final d:Ljava/util/Random;


# direct methods
.method public constructor <init>(LX/0SG;Ljava/util/Random;)V
    .locals 0
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 487727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487728
    iput-object p1, p0, LX/31g;->c:LX/0SG;

    .line 487729
    iput-object p2, p0, LX/31g;->d:Ljava/util/Random;

    .line 487730
    return-void
.end method

.method public static a(LX/0QB;)LX/31g;
    .locals 5

    .prologue
    .line 487731
    sget-object v0, LX/31g;->e:LX/31g;

    if-nez v0, :cond_1

    .line 487732
    const-class v1, LX/31g;

    monitor-enter v1

    .line 487733
    :try_start_0
    sget-object v0, LX/31g;->e:LX/31g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 487734
    if-eqz v2, :cond_0

    .line 487735
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 487736
    new-instance p0, LX/31g;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v4

    check-cast v4, Ljava/util/Random;

    invoke-direct {p0, v3, v4}, LX/31g;-><init>(LX/0SG;Ljava/util/Random;)V

    .line 487737
    move-object v0, p0

    .line 487738
    sput-object v0, LX/31g;->e:LX/31g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487739
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 487740
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 487741
    :cond_1
    sget-object v0, LX/31g;->e:LX/31g;

    return-object v0

    .line 487742
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 487743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 487744
    iget-wide v0, p0, LX/31g;->b:J

    .line 487745
    iget-object v2, p0, LX/31g;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/31g;->b:J

    .line 487746
    iget-wide v2, p0, LX/31g;->b:J

    sub-long v0, v2, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/31g;->a:J

    :goto_0
    iput-wide v0, p0, LX/31g;->a:J

    .line 487747
    iget-wide v0, p0, LX/31g;->a:J

    return-wide v0

    .line 487748
    :cond_0
    iget-object v0, p0, LX/31g;->d:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    goto :goto_0
.end method
