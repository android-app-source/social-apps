.class public final LX/2vz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/accounts/Account;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "LX/3Kc;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/content/Context;

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "LX/2w7;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/4uq;

.field public l:I

.field public m:LX/1qg;

.field private n:Landroid/os/Looper;

.field private o:LX/1vX;

.field private p:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1qf;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1qg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2vz;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2vz;->c:Ljava/util/Set;

    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/2vz;->h:Ljava/util/Map;

    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    const/4 v0, -0x1

    iput v0, p0, LX/2vz;->l:I

    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    iput-object v0, p0, LX/2vz;->o:LX/1vX;

    sget-object v0, LX/2w0;->c:LX/2vq;

    iput-object v0, p0, LX/2vz;->p:LX/2vq;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2vz;->q:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2vz;->r:Ljava/util/ArrayList;

    iput-object p1, p0, LX/2vz;->i:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LX/2vz;->n:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2vz;->f:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2vz;->g:Ljava/lang/String;

    return-void
.end method

.method private static a(LX/2vq;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;)LX/2wJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LX/2wJ;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vq",
            "<TC;TO;>;",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "LX/2wA;",
            "LX/1qf;",
            "LX/1qg;",
            ")TC;"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/2vq;->a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Ljava/lang/Object;LX/1qf;LX/1qg;)LX/2wJ;

    move-result-object v0

    return-object v0
.end method

.method private c()LX/2wX;
    .locals 15

    invoke-virtual {p0}, LX/2vz;->a()LX/2wA;

    move-result-object v4

    const/4 v9, 0x0

    const/4 v0, 0x0

    iget-object v1, v4, LX/2wA;->d:Ljava/util/Map;

    move-object v12, v1

    new-instance v7, LX/026;

    invoke-direct {v7}, LX/026;-><init>()V

    new-instance v10, LX/026;

    invoke-direct {v10}, LX/026;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, LX/2vz;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-object v2, v0

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2vs;

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    invoke-interface {v12, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v12, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kc;

    iget-boolean v0, v0, LX/3Kc;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v7, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, LX/2wE;

    invoke-direct {v5, v8, v0}, LX/2wE;-><init>(LX/2vs;I)V

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :goto_2
    invoke-virtual {v8}, LX/2vs;->d()LX/2vo;

    move-result-object v2

    invoke-interface {v10, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1}, LX/2wJ;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v9, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, v8, LX/2vs;->e:Ljava/lang/String;

    move-object v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v9, LX/2vs;->e:Ljava/lang/String;

    move-object v2, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " cannot be used with "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    :goto_3
    invoke-virtual {v8}, LX/2vs;->b()LX/2vq;

    move-result-object v0

    invoke-virtual {v0}, LX/2vr;->a()I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_8

    move-object v11, v8

    :goto_4
    iget-object v2, p0, LX/2vz;->i:Landroid/content/Context;

    iget-object v3, p0, LX/2vz;->n:Landroid/os/Looper;

    move-object v6, v5

    invoke-static/range {v0 .. v6}, LX/2vz;->a(LX/2vq;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;)LX/2wJ;

    move-result-object v0

    move-object v1, v0

    move-object v0, v11

    goto :goto_2

    :cond_2
    move-object v8, v9

    :cond_3
    move-object v2, v0

    move-object v9, v8

    goto/16 :goto_0

    :cond_4
    if-eqz v9, :cond_6

    if-eqz v2, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, v9, LX/2vs;->e:Ljava/lang/String;

    move-object v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v2, LX/2vs;->e:Ljava/lang/String;

    move-object v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " cannot be used with "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p0, LX/2vz;->a:Landroid/accounts/Account;

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_5
    const-string v1, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, v9, LX/2vs;->e:Ljava/lang/String;

    move-object v5, v5

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, LX/1ol;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, LX/2vz;->b:Ljava/util/Set;

    iget-object v1, p0, LX/2vz;->c:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, v9, LX/2vs;->e:Ljava/lang/String;

    move-object v5, v5

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, LX/1ol;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/2wW;->a(Ljava/lang/Iterable;Z)I

    move-result v12

    new-instance v0, LX/2wW;

    iget-object v1, p0, LX/2vz;->i:Landroid/content/Context;

    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iget-object v3, p0, LX/2vz;->n:Landroid/os/Looper;

    iget-object v5, p0, LX/2vz;->o:LX/1vX;

    iget-object v6, p0, LX/2vz;->p:LX/2vq;

    iget-object v8, p0, LX/2vz;->q:Ljava/util/ArrayList;

    iget-object v9, p0, LX/2vz;->r:Ljava/util/ArrayList;

    iget v11, p0, LX/2vz;->l:I

    invoke-direct/range {v0 .. v13}, LX/2wW;-><init>(Landroid/content/Context;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/2wA;LX/1vX;LX/2vq;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;IILjava/util/ArrayList;)V

    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_5

    :cond_8
    move-object v11, v2

    goto/16 :goto_4
.end method


# virtual methods
.method public final a(LX/1qf;)LX/2vz;
    .locals 1
    .param p1    # LX/1qf;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "Listener must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2vz;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(LX/1qg;)LX/2vz;
    .locals 1
    .param p1    # LX/1qg;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "Listener must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2vz;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(LX/2vs;)LX/2vz;
    .locals 2
    .param p1    # LX/2vs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vs",
            "<+",
            "LX/2w8;",
            ">;)",
            "LX/2vz;"
        }
    .end annotation

    const/4 v1, 0x0

    const-string v0, "Api must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, LX/2vs;->a()LX/2vr;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2vr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LX/2vz;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, LX/2vz;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final a(LX/2vs;LX/2w6;)LX/2vz;
    .locals 2
    .param p1    # LX/2vs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # LX/2w6;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O::",
            "LX/2w6;",
            ">(",
            "LX/2vs",
            "<TO;>;TO;)",
            "LX/2vz;"
        }
    .end annotation

    const-string v0, "Api must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, LX/2vs;->a()LX/2vr;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2vr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LX/2vz;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, LX/2vz;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final a(Landroid/os/Handler;)LX/2vz;
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "Handler must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LX/2vz;->n:Landroid/os/Looper;

    return-object p0
.end method

.method public final a()LX/2wA;
    .locals 9

    sget-object v8, LX/2w4;->a:LX/2w4;

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    sget-object v1, LX/2w0;->g:LX/2vs;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    sget-object v1, LX/2w0;->g:LX/2vs;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2w4;

    move-object v8, v0

    :cond_0
    new-instance v0, LX/2wA;

    iget-object v1, p0, LX/2vz;->a:Landroid/accounts/Account;

    iget-object v2, p0, LX/2vz;->b:Ljava/util/Set;

    iget-object v3, p0, LX/2vz;->h:Ljava/util/Map;

    iget v4, p0, LX/2vz;->d:I

    iget-object v5, p0, LX/2vz;->e:Landroid/view/View;

    iget-object v6, p0, LX/2vz;->f:Ljava/lang/String;

    iget-object v7, p0, LX/2vz;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, LX/2wA;-><init>(Landroid/accounts/Account;Ljava/util/Set;Ljava/util/Map;ILandroid/view/View;Ljava/lang/String;Ljava/lang/String;LX/2w4;)V

    return-object v0
.end method

.method public final b()LX/2wX;
    .locals 7

    iget-object v0, p0, LX/2vz;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    invoke-direct {p0}, LX/2vz;->c()LX/2wX;

    move-result-object v0

    sget-object v1, LX/2wX;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    sget-object v2, LX/2wX;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v1, p0, LX/2vz;->l:I

    if-ltz v1, :cond_0

    iget-object v1, p0, LX/2vz;->k:LX/4uq;

    iget-object v2, v1, LX/4uq;->a:Ljava/lang/Object;

    instance-of v2, v2, Landroid/support/v4/app/FragmentActivity;

    move v2, v2

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/4uq;->a:Ljava/lang/Object;

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    move-object v2, v2

    invoke-static {v2}, Lcom/google/android/gms/internal/zzqv;->a(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/gms/internal/zzqv;

    move-result-object v2

    :goto_1
    move-object v3, v2

    const-string v2, "AutoManageHelper"

    const-class v4, LX/4uP;

    invoke-interface {v3, v2, v4}, LX/4ur;->a(Ljava/lang/String;Ljava/lang/Class;)LX/4uN;

    move-result-object v2

    check-cast v2, LX/4uP;

    if-eqz v2, :cond_2

    :goto_2
    move-object v1, v2

    iget v2, p0, LX/2vz;->l:I

    iget-object v3, p0, LX/2vz;->m:LX/1qg;

    const/16 p0, 0x36

    const-string v4, "GoogleApiClient instance cannot be null"

    invoke-static {v0, v4}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, LX/4uP;->e:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Already managing a GoogleApiClient with id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-boolean v4, v1, LX/4uO;->b:Z

    iget-boolean v5, v1, LX/4uO;->c:Z

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p0, "starting AutoManage for client "

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string p0, " "

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    new-instance v4, LX/4uM;

    invoke-direct {v4, v1, v2, v0, v3}, LX/4uM;-><init>(LX/4uP;ILX/2wX;LX/1qg;)V

    iget-object v5, v1, LX/4uP;->e:Landroid/util/SparseArray;

    invoke-virtual {v5, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-boolean v4, v1, LX/4uO;->b:Z

    if-eqz v4, :cond_0

    iget-boolean v4, v1, LX/4uO;->c:Z

    if-nez v4, :cond_0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "connecting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, LX/2wX;->e()V

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    new-instance v2, LX/4uP;

    invoke-direct {v2, v3}, LX/4uP;-><init>(LX/4ur;)V

    goto/16 :goto_2

    :cond_3
    iget-object v2, v1, LX/4uq;->a:Ljava/lang/Object;

    check-cast v2, Landroid/app/Activity;

    move-object v2, v2

    invoke-static {v2}, LX/4us;->a(Landroid/app/Activity;)LX/4us;

    move-result-object v2

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_3
.end method
