.class public abstract LX/2l9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 457359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457360
    iput-object p1, p0, LX/2l9;->a:Lcom/facebook/content/SecureContextHelper;

    .line 457361
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 457352
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 457353
    :goto_0
    invoke-static {v0}, LX/32x;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 457354
    :goto_1
    return v0

    .line 457355
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 457356
    :cond_1
    invoke-static {v0}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457357
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 457358
    goto :goto_1
.end method


# virtual methods
.method public a(LX/EhY;Landroid/content/Intent;)V
    .locals 0
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 457351
    return-void
.end method

.method public final a(LX/EhY;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 457317
    invoke-virtual {p0, p1}, LX/2l9;->c(LX/EhY;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 457318
    :goto_0
    return v0

    .line 457319
    :cond_0
    const/4 v0, 0x0

    .line 457320
    iget-object v2, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 457321
    if-eqz v2, :cond_1

    .line 457322
    const-class v0, LX/2l9;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 457323
    const-string v0, "bookmark_identifier"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 457324
    :cond_1
    invoke-virtual {p1}, LX/EhY;->c()Landroid/os/Bundle;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 457325
    if-nez v2, :cond_6

    if-nez v0, :cond_6

    move v3, v4

    .line 457326
    :goto_1
    move v0, v3

    .line 457327
    if-eqz v0, :cond_2

    move v0, v1

    .line 457328
    goto :goto_0

    .line 457329
    :cond_2
    invoke-virtual {p0, p1}, LX/2l9;->b(LX/EhY;)Landroid/content/Intent;

    move-result-object v0

    .line 457330
    invoke-virtual {p0, p1, v0}, LX/2l9;->a(LX/EhY;Landroid/content/Intent;)V

    .line 457331
    if-eqz v0, :cond_5

    .line 457332
    const-string v2, "bookmark_identifier"

    invoke-virtual {p1}, LX/EhY;->c()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 457333
    invoke-virtual {p1}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2l9;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 457334
    invoke-virtual {p1}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 457335
    const-string v2, "extra_launch_uri"

    invoke-virtual {p1}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457336
    :cond_3
    iget-object v2, p0, LX/2l9;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    :goto_2
    move v0, v1

    .line 457337
    goto :goto_0

    .line 457338
    :cond_4
    iget-object v2, p0, LX/2l9;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    .line 457339
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 457340
    :cond_6
    if-eqz v2, :cond_7

    if-nez v0, :cond_8

    :cond_7
    move v3, v5

    .line 457341
    goto :goto_1

    .line 457342
    :cond_8
    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    move-result v3

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v6

    if-eq v3, v6, :cond_9

    move v3, v5

    .line 457343
    goto :goto_1

    .line 457344
    :cond_9
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 457345
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 457346
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 457347
    if-nez v7, :cond_b

    if-eqz v3, :cond_a

    .line 457348
    :cond_b
    if-eqz v7, :cond_c

    invoke-virtual {v7, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    :cond_c
    move v3, v5

    .line 457349
    goto :goto_1

    :cond_d
    move v3, v4

    .line 457350
    goto :goto_1
.end method

.method public abstract b(LX/EhY;)Landroid/content/Intent;
.end method

.method public c(LX/EhY;)Z
    .locals 1

    .prologue
    .line 457316
    const/4 v0, 0x0

    return v0
.end method
