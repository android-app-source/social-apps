.class public LX/2YQ;
.super Ljava/io/Reader;
.source ""


# instance fields
.field private a:Ljava/io/InputStream;

.field private b:Z

.field private c:Ljava/nio/charset/CharsetDecoder;

.field private final d:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 420913
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, LX/2YQ;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/nio/ByteBuffer;)V

    .line 420914
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 420965
    invoke-direct {p0, p1}, Ljava/io/Reader;-><init>(Ljava/lang/Object;)V

    .line 420966
    iput-boolean v2, p0, LX/2YQ;->b:Z

    .line 420967
    iput-object p3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    .line 420968
    iput-object p1, p0, LX/2YQ;->a:Ljava/io/InputStream;

    .line 420969
    invoke-virtual {p2}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    iput-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    .line 420970
    iget-object v0, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 420971
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 420964
    iget-object v0, p0, LX/2YQ;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 420972
    iget-object v1, p0, Ljava/io/Reader;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 420973
    :try_start_0
    iget-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    if-eqz v0, :cond_0

    .line 420974
    iget-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 420975
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    .line 420976
    iget-object v0, p0, LX/2YQ;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 420977
    iget-object v0, p0, LX/2YQ;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 420978
    const/4 v0, 0x0

    iput-object v0, p0, LX/2YQ;->a:Ljava/io/InputStream;

    .line 420979
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final read()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 420958
    iget-object v1, p0, Ljava/io/Reader;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 420959
    :try_start_0
    invoke-direct {p0}, LX/2YQ;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 420960
    new-instance v0, Ljava/io/IOException;

    const-string v2, "InputStreamReader is closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420961
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 420962
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    new-array v2, v2, [C

    .line 420963
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, LX/2YQ;->read([CII)I

    move-result v3

    if-eq v3, v0, :cond_1

    const/4 v0, 0x0

    aget-char v0, v2, v0

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final read([CII)I
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 420921
    iget-object v4, p0, Ljava/io/Reader;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 420922
    :try_start_0
    invoke-direct {p0}, LX/2YQ;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 420923
    new-instance v0, Ljava/io/IOException;

    const-string v1, "InputStreamReader is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420924
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 420925
    :cond_0
    :try_start_1
    array-length v3, p1

    invoke-static {v3, p2, p3}, LX/2Df;->a(III)V

    .line 420926
    if-nez p3, :cond_1

    .line 420927
    monitor-exit v4

    .line 420928
    :goto_0
    return v0

    .line 420929
    :cond_1
    invoke-static {p1, p2, p3}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v5

    .line 420930
    sget-object v3, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    .line 420931
    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v6

    if-nez v6, :cond_2

    move v0, v1

    :cond_2
    move v9, v0

    move-object v0, v3

    move v3, v9

    .line 420932
    :goto_1
    invoke-virtual {v5}, Ljava/nio/CharBuffer;->hasRemaining()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    .line 420933
    if-eqz v3, :cond_a

    .line 420934
    :try_start_2
    iget-object v3, p0, LX/2YQ;->a:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v5}, Ljava/nio/CharBuffer;->position()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-le v3, p2, :cond_8

    .line 420935
    :cond_3
    :goto_2
    :try_start_3
    sget-object v1, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v0, v1, :cond_5

    iget-boolean v1, p0, LX/2YQ;->b:Z

    if-eqz v1, :cond_5

    .line 420936
    iget-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    iget-object v1, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v5, v3}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v0

    .line 420937
    sget-object v1, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v0, v1, :cond_4

    .line 420938
    iget-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v0, v5}, Ljava/nio/charset/CharsetDecoder;->flush(Ljava/nio/CharBuffer;)Ljava/nio/charset/CoderResult;

    move-result-object v0

    .line 420939
    :cond_4
    iget-object v1, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v1}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 420940
    :cond_5
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isMalformed()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isUnmappable()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 420941
    :cond_6
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->throwException()V

    .line 420942
    :cond_7
    invoke-virtual {v5}, Ljava/nio/CharBuffer;->position()I

    move-result v0

    sub-int/2addr v0, p2

    if-nez v0, :cond_c

    move v0, v2

    :goto_3
    monitor-exit v4

    goto :goto_0

    .line 420943
    :catch_0
    :cond_8
    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    sub-int/2addr v3, v6

    .line 420944
    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v6

    iget-object v7, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->limit()I

    move-result v7

    add-int/2addr v6, v7

    .line 420945
    iget-object v7, p0, LX/2YQ;->a:Ljava/io/InputStream;

    iget-object v8, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    invoke-virtual {v7, v8, v6, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 420946
    if-ne v3, v2, :cond_9

    .line 420947
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2YQ;->b:Z

    goto :goto_2

    .line 420948
    :cond_9
    if-eqz v3, :cond_3

    .line 420949
    iget-object v0, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    add-int/2addr v3, v6

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 420950
    :cond_a
    iget-object v0, p0, LX/2YQ;->c:Ljava/nio/charset/CharsetDecoder;

    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v5, v6}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v0

    .line 420951
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isUnderflow()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 420952
    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v6

    if-ne v3, v6, :cond_b

    .line 420953
    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 420954
    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    iget-object v6, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 420955
    iget-object v3, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_b
    move v3, v1

    .line 420956
    goto/16 :goto_1

    .line 420957
    :cond_c
    invoke-virtual {v5}, Ljava/nio/CharBuffer;->position()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    sub-int/2addr v0, p2

    goto :goto_3
.end method

.method public final ready()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 420915
    iget-object v1, p0, Ljava/io/Reader;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 420916
    :try_start_0
    iget-object v2, p0, LX/2YQ;->a:Ljava/io/InputStream;

    if-nez v2, :cond_0

    .line 420917
    new-instance v0, Ljava/io/IOException;

    const-string v2, "InputStreamReader is closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420918
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 420919
    :cond_0
    :try_start_1
    iget-object v2, p0, LX/2YQ;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LX/2YQ;->a:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-lez v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    :try_start_2
    monitor-exit v1

    .line 420920
    :goto_0
    return v0

    :catch_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
