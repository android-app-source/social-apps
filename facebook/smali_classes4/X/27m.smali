.class public final LX/27m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 373118
    iput-object p1, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;B)V
    .locals 0

    .prologue
    .line 373117
    invoke-direct {p0, p1}, LX/27m;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 373119
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 373120
    iput-boolean v1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aI:Z

    .line 373121
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    iget-object v1, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 373122
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ah(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 373123
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 373111
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    const-string v1, "FacebookLoginActivity"

    const-string v2, "LoginCompleteFutureCallback failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 373112
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v1, 0x1

    .line 373113
    iput-boolean v1, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    .line 373114
    iget-object v0, p0, LX/27m;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sget-object v1, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    .line 373115
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 373116
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 373110
    invoke-direct {p0}, LX/27m;->a()V

    return-void
.end method
