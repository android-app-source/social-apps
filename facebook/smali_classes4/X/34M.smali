.class public final LX/34M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1NI;


# direct methods
.method public constructor <init>(LX/1NI;)V
    .locals 0

    .prologue
    .line 494987
    iput-object p1, p0, LX/34M;->a:LX/1NI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 494988
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 494989
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 494990
    iget-object v0, p0, LX/34M;->a:LX/1NI;

    iget-object v0, v0, LX/1NI;->a:LX/0TF;

    .line 494991
    iget-object v1, p0, LX/34M;->a:LX/1NI;

    monitor-enter v1

    .line 494992
    :try_start_0
    iget-object v2, p0, LX/34M;->a:LX/1NI;

    iget-boolean v2, v2, LX/1NI;->k:Z

    if-eqz v2, :cond_1

    .line 494993
    iget-object v0, p0, LX/34M;->a:LX/1NI;

    const/4 v2, 0x1

    .line 494994
    iput-boolean v2, v0, LX/1NI;->l:Z

    .line 494995
    monitor-exit v1

    .line 494996
    :cond_0
    :goto_0
    return-void

    .line 494997
    :cond_1
    iget-object v2, p0, LX/34M;->a:LX/1NI;

    const/4 v3, 0x0

    .line 494998
    iput-boolean v3, v2, LX/1NI;->l:Z

    .line 494999
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495000
    if-nez p1, :cond_2

    .line 495001
    const-class v1, LX/1NI;

    const-string v2, "Failed to retrieve new push result %s"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 495002
    new-instance v1, LX/4V6;

    invoke-direct {v1}, LX/4V6;-><init>()V

    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 495003
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 495004
    :cond_2
    iget-object v1, p0, LX/34M;->a:LX/1NI;

    iget-object v1, v1, LX/1NI;->e:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_3

    .line 495005
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 495006
    iget-object v2, p0, LX/34M;->a:LX/1NI;

    iget-object v2, v2, LX/1NI;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 495007
    :cond_3
    iget-object v1, p0, LX/34M;->a:LX/1NI;

    new-instance v2, Ljava/lang/ref/WeakReference;

    .line 495008
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 495009
    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, LX/1NI;->e:Ljava/lang/ref/WeakReference;

    .line 495010
    :try_start_2
    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 495011
    :catch_0
    move-exception v1

    .line 495012
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 495013
    iget-object v2, p0, LX/34M;->a:LX/1NI;

    iget-object v2, v2, LX/1NI;->f:LX/03V;

    const-string v3, "GraphQLSubscription_OptimisticCallbackFailed"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to run success callback for new update. Failing class: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
