.class public abstract LX/2eF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Ua;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 445101
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 445102
    iput v1, v0, LX/1UY;->d:F

    .line 445103
    move-object v0, v0

    .line 445104
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, LX/2eF;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 445088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445089
    return-void
.end method

.method public static a(FI)LX/2eF;
    .locals 2

    .prologue
    .line 445100
    new-instance v0, LX/2eH;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LX/2eH;-><init>(FIZ)V

    return-object v0
.end method

.method public static a(FIZ)LX/2eF;
    .locals 1

    .prologue
    .line 445099
    new-instance v0, LX/2eH;

    invoke-direct {v0, p0, p1, p2}, LX/2eH;-><init>(FIZ)V

    return-object v0
.end method

.method public static a(IZZ)LX/2eF;
    .locals 2

    .prologue
    .line 445098
    new-instance v0, LX/99k;

    int-to-float v1, p0

    invoke-direct {v0, v1, p1, p2}, LX/99k;-><init>(FZZ)V

    return-object v0
.end method

.method public static a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 445090
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 445091
    if-nez v0, :cond_1

    .line 445092
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, p1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 445093
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445094
    :cond_0
    :goto_0
    return-void

    .line 445095
    :cond_1
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v1, p1, :cond_0

    .line 445096
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 445097
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/support/v7/widget/RecyclerView;)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract b(Landroid/view/View;)V
.end method
