.class public LX/3Kn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile j:LX/3Kn;


# instance fields
.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Ko;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3Kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/3Kq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2Oq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 549207
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    sput-object v0, LX/3Kn;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549209
    return-void
.end method

.method public static a(LX/0QB;)LX/3Kn;
    .locals 11

    .prologue
    .line 549210
    sget-object v0, LX/3Kn;->j:LX/3Kn;

    if-nez v0, :cond_1

    .line 549211
    const-class v1, LX/3Kn;

    monitor-enter v1

    .line 549212
    :try_start_0
    sget-object v0, LX/3Kn;->j:LX/3Kn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 549213
    if-eqz v2, :cond_0

    .line 549214
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 549215
    new-instance v3, LX/3Kn;

    invoke-direct {v3}, LX/3Kn;-><init>()V

    .line 549216
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/3Ko;->a(LX/0QB;)LX/3Ko;

    move-result-object v5

    check-cast v5, LX/3Ko;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v6

    check-cast v6, LX/1Ml;

    const-class v7, LX/0i4;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0i4;

    const-class v8, LX/3Kp;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/3Kp;

    invoke-static {v0}, LX/3Kq;->a(LX/0QB;)LX/3Kq;

    move-result-object v9

    check-cast v9, LX/3Kq;

    invoke-static {v0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v10

    check-cast v10, LX/2Oq;

    invoke-static {v0}, LX/2uq;->a(LX/0QB;)LX/2uq;

    move-result-object p0

    check-cast p0, LX/2uq;

    .line 549217
    iput-object v4, v3, LX/3Kn;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v3, LX/3Kn;->c:LX/3Ko;

    iput-object v6, v3, LX/3Kn;->d:LX/1Ml;

    iput-object v7, v3, LX/3Kn;->e:LX/0i4;

    iput-object v8, v3, LX/3Kn;->f:LX/3Kp;

    iput-object v9, v3, LX/3Kn;->g:LX/3Kq;

    iput-object v10, v3, LX/3Kn;->h:LX/2Oq;

    iput-object p0, v3, LX/3Kn;->i:LX/2uq;

    .line 549218
    move-object v0, v3

    .line 549219
    sput-object v0, LX/3Kn;->j:LX/3Kn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 549221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 549222
    :cond_1
    sget-object v0, LX/3Kn;->j:LX/3Kn;

    return-object v0

    .line 549223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 549224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
