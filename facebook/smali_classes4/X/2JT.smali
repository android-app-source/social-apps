.class public LX/2JT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392964
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    sput-object v0, LX/2JT;->a:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392966
    new-instance v0, LX/2JU;

    invoke-direct {v0, p0}, LX/2JU;-><init>(LX/2JT;)V

    iput-object v0, p0, LX/2JT;->c:Ljava/util/Comparator;

    .line 392967
    iput-object p1, p0, LX/2JT;->b:LX/0Or;

    .line 392968
    return-void
.end method

.method public static a(LX/0QB;)LX/2JT;
    .locals 4

    .prologue
    .line 392969
    const-class v1, LX/2JT;

    monitor-enter v1

    .line 392970
    :try_start_0
    sget-object v0, LX/2JT;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 392971
    sput-object v2, LX/2JT;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 392972
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392973
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 392974
    new-instance v3, LX/2JT;

    const/16 p0, 0x12cc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2JT;-><init>(LX/0Or;)V

    .line 392975
    move-object v0, v3

    .line 392976
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 392977
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2JT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392978
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 392979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392980
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 392981
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 392982
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 392983
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->kh_()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    if-ne v5, v6, :cond_0

    .line 392984
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392985
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 392986
    :cond_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 392987
    iget-object v0, p0, LX/2JT;->c:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 392988
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 392989
    invoke-virtual {p0, p1}, LX/2JT;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 392990
    :goto_0
    return v0

    .line 392991
    :cond_0
    sget-object v1, LX/Dug;->a:[I

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->kh_()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 392992
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 392993
    iget-object v1, p0, LX/2JT;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 392994
    :cond_0
    :goto_0
    return v0

    .line 392995
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->k()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 392996
    iget-object v0, p0, LX/2JT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 392997
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 392998
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->k()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 392999
    iget-object v1, p0, LX/2JT;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 393000
    :cond_0
    :goto_0
    return v0

    .line 393001
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 393002
    iget-object v0, p0, LX/2JT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 393003
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 393004
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
