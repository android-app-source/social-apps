.class public Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(LX/329;LX/0lS;LX/32t;Ljava/util/Map;Ljava/util/HashSet;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/329;",
            "LX/0lS;",
            "LX/32t;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 488519
    invoke-direct/range {p0 .. p7}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(LX/329;LX/0lS;LX/32t;Ljava/util/Map;Ljava/util/HashSet;ZZ)V

    .line 488520
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;)V
    .locals 1

    .prologue
    .line 488521
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Z)V

    .line 488522
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4qD;)V
    .locals 0

    .prologue
    .line 488523
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4qD;)V

    .line 488524
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4ro;)V
    .locals 0

    .prologue
    .line 488525
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4ro;)V

    .line 488526
    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 488527
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Ljava/util/HashSet;)V

    .line 488528
    return-void
.end method

.method private A(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 488530
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_0

    .line 488531
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->B(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488532
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private B(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 488533
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_externalTypeIdHandler:LX/4q8;

    invoke-virtual {v0}, LX/4q8;->a()LX/4q8;

    move-result-object v2

    .line 488534
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488535
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    invoke-virtual {v3, p1, p2, v0}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v4

    .line 488536
    new-instance v5, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    invoke-direct {v5, v0}, LX/0nW;-><init>(LX/0lD;)V

    .line 488537
    invoke-virtual {v5}, LX/0nX;->f()V

    .line 488538
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488539
    :goto_0
    sget-object v6, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v6, :cond_6

    .line 488540
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 488541
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488542
    invoke-virtual {v3, v6}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 488543
    if-eqz v0, :cond_3

    .line 488544
    invoke-virtual {v2, p1, p2, v6, v4}, LX/4q8;->b(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 488545
    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v7

    .line 488546
    invoke-virtual {v0}, LX/32s;->c()I

    move-result v0

    invoke-virtual {v4, v0, v7}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488547
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 488548
    :try_start_0
    invoke-virtual {v3, p2, v4}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 488549
    :goto_1
    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v3, :cond_1

    .line 488550
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488551
    invoke-virtual {v5, p1}, LX/0nW;->b(LX/15w;)V

    .line 488552
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_1

    .line 488553
    :catch_0
    move-exception v0

    .line 488554
    iget-object v7, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488555
    iget-object v8, v7, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v7, v8

    .line 488556
    invoke-static {v0, v7, v6, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    .line 488557
    :cond_0
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 488558
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488559
    iget-object v4, v3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v3, v4

    .line 488560
    if-eq v0, v3, :cond_2

    .line 488561
    const-string v0, "Can not create polymorphic instances with unwrapped values"

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 488562
    :cond_2
    invoke-virtual {v2, p1, p2, v1}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488563
    :goto_3
    return-object v0

    .line 488564
    :cond_3
    invoke-virtual {v4, v6}, LX/4qL;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488565
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0, v6}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 488566
    if-eqz v0, :cond_4

    .line 488567
    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, LX/4qL;->a(LX/32s;Ljava/lang/Object;)V

    goto :goto_2

    .line 488568
    :cond_4
    invoke-virtual {v2, p1, p2, v6, v1}, LX/4q8;->b(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488569
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 488570
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488571
    :cond_5
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v0, :cond_0

    .line 488572
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iget-object v7, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v7, p1, p2}, LX/4q5;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v4, v0, v6, v7}, LX/4qL;->a(LX/4q5;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 488573
    :cond_6
    :try_start_1
    invoke-virtual {v2, p1, p2, v4, v3}, LX/4q8;->a(LX/15w;LX/0n3;LX/4qL;LX/4qF;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_3

    .line 488574
    :catch_1
    move-exception v0

    .line 488575
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    move-object v0, v1

    .line 488576
    goto :goto_3
.end method

.method private final a(LX/15w;LX/0n3;LX/15z;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488577
    if-nez p3, :cond_0

    .line 488578
    invoke-direct {p0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488579
    :goto_0
    return-object v0

    .line 488580
    :cond_0
    sget-object v0, LX/4q0;->a:[I

    invoke-virtual {p3}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 488581
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 488582
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->f(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488583
    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488584
    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->g(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488585
    :pswitch_3
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488586
    :pswitch_4
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->h(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488587
    :pswitch_5
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->i(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488588
    :pswitch_6
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    if-eqz v0, :cond_1

    .line 488589
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488590
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_2

    .line 488591
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->c(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488592
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private a(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 488593
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488594
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 488595
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 488596
    :cond_0
    new-instance v2, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v1

    invoke-direct {v2, v1}, LX/0nW;-><init>(LX/0lD;)V

    .line 488597
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 488598
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v1, :cond_2

    .line 488599
    iget-object v1, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v1, v1

    .line 488600
    :goto_0
    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v3, :cond_6

    .line 488601
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 488602
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v3, v0}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v3

    .line 488603
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488604
    if-eqz v3, :cond_4

    .line 488605
    if-eqz v1, :cond_3

    invoke-virtual {v3, v1}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 488606
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488607
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 488608
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 488609
    :cond_3
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 488610
    :catch_0
    move-exception v3

    .line 488611
    invoke-static {v3, p3, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_1

    .line 488612
    :cond_4
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 488613
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 488614
    :cond_5
    invoke-virtual {v2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488615
    invoke-virtual {v2, p1}, LX/0nW;->b(LX/15w;)V

    .line 488616
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v3, :cond_1

    .line 488617
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v3, p1, p2, p3, v0}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 488618
    :cond_6
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 488619
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    invoke-virtual {v0, p2, p3, v2}, LX/4qO;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    .line 488620
    return-object p3
.end method

.method private a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 488621
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488622
    :goto_0
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_4

    .line 488623
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 488624
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488625
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v1, v0}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v1

    .line 488626
    if-eqz v1, :cond_1

    .line 488627
    invoke-virtual {v1, p4}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 488628
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488629
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 488630
    :cond_0
    :try_start_0
    invoke-virtual {v1, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 488631
    :catch_0
    move-exception v1

    .line 488632
    invoke-static {v1, p3, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_1

    .line 488633
    :cond_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 488634
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 488635
    :cond_2
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v1, :cond_3

    .line 488636
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v1, p1, p2, p3, v0}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 488637
    :cond_3
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 488638
    :cond_4
    return-object p3
.end method

.method private b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;
    .locals 1

    .prologue
    .line 488639
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4qD;)V

    return-object v0
.end method

.method private b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;"
        }
    .end annotation

    .prologue
    .line 488518
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Ljava/util/HashSet;)V

    return-object v0
.end method

.method private b(LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 488529
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0n3;->c(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method private b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 488277
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v0, :cond_2

    .line 488278
    iget-object v0, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v0, v0

    .line 488279
    :goto_0
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_externalTypeIdHandler:LX/4q8;

    invoke-virtual {v1}, LX/4q8;->a()LX/4q8;

    move-result-object v2

    .line 488280
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 488281
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 488282
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488283
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v1, v3}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v1

    .line 488284
    if-eqz v1, :cond_4

    .line 488285
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    invoke-virtual {v4}, LX/15z;->isScalarValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 488286
    invoke-virtual {v2, p1, p2, v3, p3}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 488287
    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 488288
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488289
    :cond_1
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 488290
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 488291
    :cond_3
    :try_start_0
    invoke-virtual {v1, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 488292
    :catch_0
    move-exception v1

    .line 488293
    invoke-static {v1, p3, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488294
    :cond_4
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 488295
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488296
    :cond_5
    invoke-virtual {v2, p1, p2, v3, p3}, LX/4q8;->b(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 488297
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v1, :cond_6

    .line 488298
    :try_start_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v1, p1, p2, p3, v3}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 488299
    :catch_1
    move-exception v1

    .line 488300
    invoke-static {v1, p3, v3, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488301
    :cond_6
    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 488302
    :cond_7
    invoke-virtual {v2, p1, p2, p3}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private final x(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488333
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v0

    .line 488334
    :goto_0
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 488335
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 488336
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488337
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v2, v1}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v2

    .line 488338
    if-eqz v2, :cond_0

    .line 488339
    :try_start_0
    invoke-virtual {v2, p1, p2, v0}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 488340
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 488341
    :catch_0
    move-exception v2

    .line 488342
    invoke-static {v2, v0, v1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_1

    .line 488343
    :cond_0
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 488344
    :cond_1
    return-object v0
.end method

.method private y(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 488345
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    .line 488346
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488347
    :goto_0
    return-object v0

    .line 488348
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_1

    .line 488349
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->z(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488350
    :cond_1
    new-instance v3, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    invoke-direct {v3, v0}, LX/0nW;-><init>(LX/0lD;)V

    .line 488351
    invoke-virtual {v3}, LX/0nX;->f()V

    .line 488352
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v1

    .line 488353
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_2

    .line 488354
    invoke-virtual {p0, p2, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 488355
    :cond_2
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v0, :cond_4

    .line 488356
    iget-object v0, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v0, v0

    .line 488357
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 488358
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 488359
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488360
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v2, v4}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v2

    .line 488361
    if-eqz v2, :cond_6

    .line 488362
    if-eqz v0, :cond_5

    invoke-virtual {v2, v0}, LX/32s;->a(Ljava/lang/Class;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 488363
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488364
    :cond_3
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 488365
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 488366
    :cond_5
    :try_start_0
    invoke-virtual {v2, p1, p2, v1}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 488367
    :catch_0
    move-exception v2

    .line 488368
    invoke-static {v2, v1, v4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488369
    :cond_6
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 488370
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488371
    :cond_7
    invoke-virtual {v3, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488372
    invoke-virtual {v3, p1}, LX/0nW;->b(LX/15w;)V

    .line 488373
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v2, :cond_3

    .line 488374
    :try_start_1
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v2, p1, p2, v1, v4}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 488375
    :catch_1
    move-exception v2

    .line 488376
    invoke-static {v2, v1, v4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488377
    :cond_8
    invoke-virtual {v3}, LX/0nX;->g()V

    .line 488378
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    invoke-virtual {v0, p2, v1, v3}, LX/4qO;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-object v0, v1

    .line 488379
    goto/16 :goto_0
.end method

.method private z(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 488380
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488381
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    invoke-virtual {v1, p1, p2, v0}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v2

    .line 488382
    new-instance v3, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    invoke-direct {v3, v0}, LX/0nW;-><init>(LX/0lD;)V

    .line 488383
    invoke-virtual {v3}, LX/0nX;->f()V

    .line 488384
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488385
    :goto_0
    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v4, :cond_6

    .line 488386
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 488387
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488388
    invoke-virtual {v1, v4}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 488389
    if-eqz v0, :cond_3

    .line 488390
    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v5

    .line 488391
    invoke-virtual {v0}, LX/32s;->c()I

    move-result v0

    invoke-virtual {v2, v0, v5}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488392
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 488393
    :try_start_0
    invoke-virtual {v1, p2, v2}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 488394
    :goto_1
    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_1

    .line 488395
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488396
    invoke-virtual {v3, p1}, LX/0nW;->b(LX/15w;)V

    .line 488397
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_1

    .line 488398
    :catch_0
    move-exception v0

    .line 488399
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488400
    iget-object v6, v5, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v5, v6

    .line 488401
    invoke-static {v0, v5, v4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    .line 488402
    :cond_0
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 488403
    :cond_1
    invoke-virtual {v3}, LX/0nX;->g()V

    .line 488404
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488405
    iget-object v4, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v4

    .line 488406
    if-eq v0, v2, :cond_2

    .line 488407
    invoke-virtual {v3}, LX/0nX;->close()V

    .line 488408
    const-string v0, "Can not create polymorphic instances with unwrapped values"

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 488409
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    invoke-virtual {v0, p2, v1, v3}, LX/4qO;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    .line 488410
    :goto_3
    return-object v0

    .line 488411
    :cond_3
    invoke-virtual {v2, v4}, LX/4qL;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488412
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0, v4}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 488413
    if-eqz v0, :cond_4

    .line 488414
    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/4qL;->a(LX/32s;Ljava/lang/Object;)V

    goto :goto_2

    .line 488415
    :cond_4
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 488416
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488417
    :cond_5
    invoke-virtual {v3, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488418
    invoke-virtual {v3, p1}, LX/0nW;->b(LX/15w;)V

    .line 488419
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v0, :cond_0

    .line 488420
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v5, p1, p2}, LX/4q5;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v0, v4, v5}, LX/4qL;->a(LX/4q5;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 488421
    :cond_6
    :try_start_1
    invoke-virtual {v1, p2, v2}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 488422
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    invoke-virtual {v1, p2, v0, v3}, LX/4qO;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 488423
    :catch_1
    move-exception v0

    .line 488424
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    .line 488425
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a()Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 2

    .prologue
    .line 488426
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0}, LX/32t;->b()[LX/32s;

    move-result-object v0

    .line 488427
    new-instance v1, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;

    invoke-direct {v1, p0, v0}, Lcom/fasterxml/jackson/databind/deser/impl/BeanAsArrayDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;[LX/32s;)V

    return-object v1
.end method

.method public final synthetic a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 488428
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .locals 1

    .prologue
    .line 488429
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488303
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    if-eqz v0, :cond_3

    .line 488304
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    if-eqz v0, :cond_1

    .line 488305
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->y(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488306
    :cond_0
    :goto_0
    return-object v0

    .line 488307
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_externalTypeIdHandler:LX/4q8;

    if-eqz v0, :cond_2

    .line 488308
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->A(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488309
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->d(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488310
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->l()Ljava/lang/Object;

    move-result-object v0

    .line 488311
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_4

    .line 488312
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 488313
    :cond_4
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v1, :cond_5

    .line 488314
    iget-object v1, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v1, v1

    .line 488315
    if-eqz v1, :cond_5

    .line 488316
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488317
    :cond_5
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 488318
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 488319
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488320
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v1, v2}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v1

    .line 488321
    if-eqz v1, :cond_6

    .line 488322
    :try_start_0
    invoke-virtual {v1, p1, p2, v0}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 488323
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 488324
    :catch_0
    move-exception v1

    .line 488325
    invoke-static {v1, v0, v2, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488326
    :cond_6
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 488327
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488328
    :cond_7
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v1, :cond_8

    .line 488329
    :try_start_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v1, p1, p2, v0, v2}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 488330
    :catch_1
    move-exception v1

    .line 488331
    invoke-static {v1, v0, v2, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488332
    :cond_8
    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final b(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 488430
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488431
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    invoke-virtual {v3, p1, p2, v0}, LX/4qF;->a(LX/15w;LX/0n3;LX/4qD;)LX/4qL;

    move-result-object v4

    .line 488432
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    move-object v1, v0

    move-object v0, v2

    .line 488433
    :goto_0
    sget-object v5, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v5, :cond_8

    .line 488434
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 488435
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488436
    invoke-virtual {v3, v1}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v5

    .line 488437
    if-eqz v5, :cond_2

    .line 488438
    invoke-virtual {v5, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v6

    .line 488439
    invoke-virtual {v5}, LX/32s;->c()I

    move-result v5

    invoke-virtual {v4, v5, v6}, LX/4qL;->a(ILjava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 488440
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488441
    :try_start_0
    invoke-virtual {v3, p2, v4}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 488442
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488443
    iget-object v4, v3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v3, v4

    .line 488444
    if-eq v1, v3, :cond_0

    .line 488445
    invoke-virtual {p0, p1, p2, v2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    .line 488446
    :goto_2
    return-object v0

    .line 488447
    :catch_0
    move-exception v3

    .line 488448
    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488449
    iget-object v5, v4, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v4, v5

    .line 488450
    invoke-static {v3, v4, v1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_1

    .line 488451
    :cond_0
    if-eqz v0, :cond_1

    .line 488452
    invoke-virtual {p0, p2, v2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v2

    .line 488453
    :cond_1
    invoke-virtual {p0, p1, p2, v2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 488454
    :cond_2
    invoke-virtual {v4, v1}, LX/4qL;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 488455
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v5, v1}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v5

    .line 488456
    if-eqz v5, :cond_4

    .line 488457
    invoke-virtual {v5, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/4qL;->a(LX/32s;Ljava/lang/Object;)V

    .line 488458
    :cond_3
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    goto :goto_0

    .line 488459
    :cond_4
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 488460
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 488461
    :cond_5
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v5, :cond_6

    .line 488462
    iget-object v5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iget-object v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v6, p1, p2}, LX/4q5;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v1, v6}, LX/4qL;->a(LX/4q5;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_3

    .line 488463
    :cond_6
    if-nez v0, :cond_7

    .line 488464
    new-instance v0, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v5

    invoke-direct {v0, v5}, LX/0nW;-><init>(LX/0lD;)V

    .line 488465
    :cond_7
    invoke-virtual {v0, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488466
    invoke-virtual {v0, p1}, LX/0nW;->b(LX/15w;)V

    goto :goto_3

    .line 488467
    :cond_8
    :try_start_1
    invoke-virtual {v3, p2, v4}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 488468
    :goto_4
    if-eqz v0, :cond_a

    .line 488469
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488470
    iget-object v5, v4, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v4, v5

    .line 488471
    if-eq v3, v4, :cond_9

    .line 488472
    invoke-virtual {p0, v2, p2, v1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 488473
    :catch_1
    move-exception v1

    .line 488474
    invoke-virtual {p0, v1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    move-object v1, v2

    .line 488475
    goto :goto_4

    .line 488476
    :cond_9
    invoke-virtual {p0, p2, v1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_2

    :cond_a
    move-object v0, v1

    .line 488477
    goto/16 :goto_2
.end method

.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488478
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488479
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_2

    .line 488480
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    if-eqz v0, :cond_0

    .line 488481
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488482
    :goto_0
    return-object v0

    .line 488483
    :cond_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488484
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_1

    .line 488485
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->c(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488486
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488487
    :cond_2
    invoke-direct {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->a(LX/15w;LX/0n3;LX/15z;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488488
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v0, :cond_0

    .line 488489
    invoke-virtual {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 488490
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    if-eqz v0, :cond_2

    .line 488491
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->a(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 488492
    :cond_1
    :goto_0
    return-object p3

    .line 488493
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_externalTypeIdHandler:LX/4q8;

    if-eqz v0, :cond_3

    .line 488494
    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    goto :goto_0

    .line 488495
    :cond_3
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488496
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_4

    .line 488497
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 488498
    :cond_4
    iget-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-eqz v1, :cond_5

    .line 488499
    iget-object v1, p2, LX/0n3;->_view:Ljava/lang/Class;

    move-object v1, v1

    .line 488500
    if-eqz v1, :cond_5

    .line 488501
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p3

    goto :goto_0

    .line 488502
    :cond_5
    :goto_1
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_1

    .line 488503
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 488504
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488505
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v1, v0}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v1

    .line 488506
    if-eqz v1, :cond_6

    .line 488507
    :try_start_0
    invoke-virtual {v1, p1, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 488508
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_1

    .line 488509
    :catch_0
    move-exception v1

    .line 488510
    invoke-static {v1, p3, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_2

    .line 488511
    :cond_6
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 488512
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 488513
    :cond_7
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v1, :cond_8

    .line 488514
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v1, p1, p2, p3, v0}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 488515
    :cond_8
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488516
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    if-eq v0, v1, :cond_0

    .line 488517
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4ro;)V

    move-object p0, v0

    goto :goto_0
.end method
