.class public abstract Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""

# interfaces
.implements LX/1Xy;
.implements LX/1Xx;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/1Xy;",
        "LX/1Xx;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x1c4b3fef168370e0L


# instance fields
.field public _anySetter:LX/4q5;

.field public final _backRefs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field

.field public final _beanProperties:LX/32t;

.field public final _beanType:LX/0lJ;

.field public _delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public _externalTypeIdHandler:LX/4q8;

.field public final _ignorableProps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final _ignoreAllUnknown:Z

.field public final _injectables:[LX/4qP;

.field public final _needViewProcesing:Z

.field public _nonStandardCreation:Z

.field public final _objectIdReader:LX/4qD;

.field public _propertyBasedCreator:LX/4qF;

.field public final _serializationShape:LX/2BA;

.field public _unwrappedPropertyHandler:LX/4qO;

.field public final _valueInstantiator:LX/320;

.field public _vanillaProcessing:Z

.field public transient a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/1Xc;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transient b:LX/0lQ;


# direct methods
.method public constructor <init>(LX/329;LX/0lS;LX/32t;Ljava/util/Map;Ljava/util/HashSet;ZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/329;",
            "LX/0lS;",
            "LX/32t;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 489024
    iget-object v0, p2, LX/0lS;->a:LX/0lJ;

    move-object v0, v0

    .line 489025
    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 489026
    invoke-virtual {p2}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    .line 489027
    invoke-virtual {v0}, LX/0lN;->g()LX/0lQ;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    .line 489028
    iget-object v0, p2, LX/0lS;->a:LX/0lJ;

    move-object v0, v0

    .line 489029
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 489030
    iget-object v0, p1, LX/329;->g:LX/320;

    move-object v0, v0

    .line 489031
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 489032
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 489033
    iput-object p4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    .line 489034
    iput-object p5, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    .line 489035
    iput-boolean p6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    .line 489036
    iget-object v0, p1, LX/329;->i:LX/4q5;

    move-object v0, v0

    .line 489037
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 489038
    iget-object v0, p1, LX/329;->d:Ljava/util/List;

    move-object v0, v0

    .line 489039
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    .line 489040
    iget-object v0, p1, LX/329;->h:LX/4qD;

    move-object v0, v0

    .line 489041
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 489042
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->j()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->h()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 489043
    invoke-virtual {p2, v1}, LX/0lS;->a(LX/4pN;)LX/4pN;

    move-result-object v0

    .line 489044
    if-nez v0, :cond_4

    :goto_2
    iput-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 489045
    iput-boolean p7, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    .line 489046
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_5

    :goto_3
    iput-boolean v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 489047
    return-void

    .line 489048
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [LX/4qP;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4qP;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 489049
    goto :goto_1

    .line 489050
    :cond_4
    iget-object v1, v0, LX/4pN;->b:LX/2BA;

    move-object v1, v1

    .line 489051
    goto :goto_2

    :cond_5
    move v3, v2

    .line 489052
    goto :goto_3
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;)V
    .locals 1

    .prologue
    .line 488860
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;-><init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Z)V

    .line 488861
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4qD;)V
    .locals 2

    .prologue
    .line 488862
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 488863
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    .line 488864
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488865
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 488866
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 488867
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488868
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    .line 488869
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    .line 488870
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    .line 488871
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488872
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    .line 488873
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488874
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488875
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    .line 488876
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 488877
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 488878
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 488879
    if-nez p2, :cond_0

    .line 488880
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 488881
    :goto_0
    return-void

    .line 488882
    :cond_0
    new-instance v0, LX/4qE;

    const/4 v1, 0x1

    invoke-direct {v0, p2, v1}, LX/4qE;-><init>(LX/4qD;Z)V

    .line 488883
    iget-object v1, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v1, v0}, LX/32t;->a(LX/32s;)LX/32t;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;LX/4ro;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 488884
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 488885
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    .line 488886
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488887
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 488888
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 488889
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488890
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    .line 488891
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    .line 488892
    if-nez p2, :cond_0

    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    .line 488893
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488894
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    .line 488895
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 488896
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488897
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488898
    if-eqz p2, :cond_3

    .line 488899
    if-eqz v0, :cond_1

    .line 488900
    invoke-virtual {v0, p2}, LX/4qO;->a(LX/4ro;)LX/4qO;

    move-result-object v0

    .line 488901
    :cond_1
    iget-object v2, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v2, p2}, LX/32t;->a(LX/4ro;)LX/32t;

    move-result-object v2

    iput-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 488902
    :goto_1
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488903
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    .line 488904
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 488905
    iput-boolean v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 488906
    return-void

    :cond_2
    move v0, v1

    .line 488907
    goto :goto_0

    .line 488908
    :cond_3
    iget-object v2, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    iput-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 488909
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 488910
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    .line 488911
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488912
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 488913
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 488914
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488915
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    .line 488916
    iput-object p2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    .line 488917
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    .line 488918
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488919
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    .line 488920
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488921
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488922
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    .line 488923
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 488924
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 488925
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 488926
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 488927
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;Z)V
    .locals 1

    .prologue
    .line 488928
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(LX/0lJ;)V

    .line 488929
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    .line 488930
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488931
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 488932
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 488933
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488934
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    .line 488935
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    .line 488936
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    .line 488937
    iput-boolean p2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    .line 488938
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488939
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    .line 488940
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 488941
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488942
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488943
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_needViewProcesing:Z

    .line 488944
    iget-object v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 488945
    iget-boolean v0, p1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    iput-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 488946
    return-void
.end method

.method private a(LX/32s;)LX/32s;
    .locals 7

    .prologue
    .line 488947
    iget-object v0, p1, LX/32s;->_managedReferenceName:Ljava/lang/String;

    move-object v2, v0

    .line 488948
    if-nez v2, :cond_0

    .line 488949
    :goto_0
    return-object p1

    .line 488950
    :cond_0
    invoke-virtual {p1}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 488951
    const/4 v5, 0x0

    .line 488952
    instance-of v1, v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    if-eqz v1, :cond_1

    .line 488953
    check-cast v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-direct {v0, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(Ljava/lang/String;)LX/32s;

    move-result-object v3

    .line 488954
    :goto_1
    if-nez v3, :cond_6

    .line 488955
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Can not handle managed/back reference \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\': no back reference property found from type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/32s;->a()LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488956
    :cond_1
    instance-of v1, v0, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;

    if-eqz v1, :cond_4

    .line 488957
    check-cast v0, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;

    invoke-virtual {v0}, Lcom/fasterxml/jackson/databind/deser/std/ContainerDeserializerBase;->a()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 488958
    instance-of v1, v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    if-nez v1, :cond_3

    .line 488959
    if-nez v0, :cond_2

    const-string v0, "NULL"

    .line 488960
    :goto_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not handle managed/back reference \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': value deserializer is of type ContainerDeserializerBase, but content type is not handled by a BeanDeserializer  (instead it\'s of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 488961
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 488962
    :cond_3
    check-cast v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    invoke-direct {v0, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(Ljava/lang/String;)LX/32s;

    move-result-object v3

    .line 488963
    const/4 v5, 0x1

    .line 488964
    goto :goto_1

    :cond_4
    instance-of v1, v0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;

    if-eqz v1, :cond_5

    .line 488965
    check-cast v0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;

    invoke-virtual {v0, v2}, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;->a(Ljava/lang/String;)LX/32s;

    move-result-object v3

    goto :goto_1

    .line 488966
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not handle managed/back reference \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\': type for value deserializer is not BeanDeserializer or ContainerDeserializerBase, but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 488967
    :cond_6
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488968
    invoke-virtual {v3}, LX/32s;->a()LX/0lJ;

    move-result-object v1

    .line 488969
    iget-object v4, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v4, v4

    .line 488970
    iget-object v6, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v6, v6

    .line 488971
    invoke-virtual {v4, v6}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 488972
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Can not handle managed/back reference \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\': back reference type ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 488973
    iget-object v4, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v4

    .line 488974
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") not compatible with managed type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 488975
    iget-object v2, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v2

    .line 488976
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 488977
    :cond_7
    new-instance v0, LX/4qA;

    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/4qA;-><init>(LX/32s;Ljava/lang/String;LX/32s;LX/0lQ;Z)V

    move-object p1, v0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)LX/32s;
    .locals 2

    .prologue
    .line 488978
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 488979
    :goto_0
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v1, :cond_0

    .line 488980
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    invoke-virtual {v0, p1}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 488981
    :cond_0
    return-object v0

    .line 488982
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0, p1}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0n3;LX/32s;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/32s;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488983
    invoke-virtual {p0}, LX/0n3;->f()LX/0lU;

    move-result-object v0

    .line 488984
    if-eqz v0, :cond_0

    .line 488985
    invoke-virtual {p1}, LX/32s;->b()LX/2An;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->u(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 488986
    if-eqz v0, :cond_0

    .line 488987
    invoke-virtual {p1}, LX/32s;->b()LX/2An;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/0mz;->a(LX/0lO;Ljava/lang/Object;)LX/1Xr;

    move-result-object v1

    .line 488988
    invoke-interface {v1}, LX/1Xr;->b()LX/0lJ;

    move-result-object v2

    .line 488989
    invoke-virtual {p0, v2, p1}, LX/0n3;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v3

    .line 488990
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;

    invoke-direct {v0, v1, v2, v3}, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 488991
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V
    .locals 1

    .prologue
    .line 488992
    invoke-static {p0, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(Ljava/lang/Throwable;LX/0n3;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method private static b(LX/0n3;LX/32s;)LX/32s;
    .locals 2

    .prologue
    .line 489119
    invoke-virtual {p1}, LX/32s;->b()LX/2An;

    move-result-object v0

    .line 489120
    if-eqz v0, :cond_0

    .line 489121
    invoke-virtual {p0}, LX/0n3;->f()LX/0lU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lU;->b(LX/2An;)LX/4ro;

    move-result-object v0

    .line 489122
    if-eqz v0, :cond_0

    .line 489123
    invoke-virtual {p1}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 489124
    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 489125
    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    .line 489126
    invoke-virtual {p1, v0}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v0

    .line 489127
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 488993
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 488994
    const/4 v0, 0x0

    .line 488995
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_backRefs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    goto :goto_0
.end method

.method private b(LX/0n3;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488996
    monitor-enter p0

    .line 488997
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a:Ljava/util/HashMap;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 488998
    :goto_0
    monitor-exit p0

    .line 488999
    if-eqz v0, :cond_2

    .line 489000
    :cond_0
    :goto_1
    return-object v0

    .line 489001
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a:Ljava/util/HashMap;

    new-instance v1, LX/1Xc;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Xc;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    goto :goto_0

    .line 489002
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 489003
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0n3;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 489004
    invoke-virtual {p1, v0}, LX/0n3;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 489005
    if-eqz v0, :cond_0

    .line 489006
    monitor-enter p0

    .line 489007
    :try_start_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a:Ljava/util/HashMap;

    if-nez v1, :cond_3

    .line 489008
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a:Ljava/util/HashMap;

    .line 489009
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a:Ljava/util/HashMap;

    new-instance v2, LX/1Xc;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Xc;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489010
    monitor-exit p0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method private static b(Ljava/lang/Throwable;LX/0n3;)Ljava/lang/Throwable;
    .locals 3

    .prologue
    .line 489011
    move-object v0, p0

    :goto_0
    instance-of v1, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 489012
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 489013
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 489014
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 489015
    :cond_1
    if-eqz p1, :cond_2

    sget-object v1, LX/0mv;->WRAP_EXCEPTIONS:LX/0mv;

    invoke-virtual {p1, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    .line 489016
    :goto_1
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_5

    .line 489017
    if-eqz v1, :cond_3

    instance-of v1, v0, LX/28F;

    if-nez v1, :cond_6

    .line 489018
    :cond_3
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 489019
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 489020
    :cond_5
    if-nez v1, :cond_6

    .line 489021
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_6

    .line 489022
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 489023
    :cond_6
    return-object v0
.end method

.method private c(LX/0n3;LX/32s;)LX/32s;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 489053
    invoke-virtual {p2}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 489054
    instance-of v2, v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    if-eqz v2, :cond_1

    .line 489055
    check-cast v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    .line 489056
    iget-object v2, v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    move-object v0, v2

    .line 489057
    invoke-virtual {v0}, LX/320;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 489058
    invoke-virtual {p2}, LX/32s;->a()LX/0lJ;

    move-result-object v0

    .line 489059
    iget-object v2, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v2

    .line 489060
    invoke-static {v0}, LX/1Xw;->b(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 489061
    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 489062
    iget-object v4, v3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v3, v4

    .line 489063
    if-ne v2, v3, :cond_1

    .line 489064
    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 489065
    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    .line 489066
    array-length v7, v6

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    aget-object v6, v6, v1

    if-ne v6, v2, :cond_2

    .line 489067
    iget-object v0, p1, LX/0n3;->_config:LX/0mu;

    move-object v0, v0

    .line 489068
    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489069
    invoke-static {v5}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 489070
    :cond_0
    new-instance v0, LX/4q9;

    invoke-direct {v0, p2, v5}, LX/4q9;-><init>(LX/32s;Ljava/lang/reflect/Constructor;)V

    move-object p2, v0

    .line 489071
    :cond_1
    return-object p2

    .line 489072
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private x(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 489073
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iget-object v0, v0, LX/4qD;->deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 489074
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iget-object v1, v1, LX/4qD;->generator:LX/4pS;

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Object;LX/4pS;)LX/4qM;

    move-result-object v1

    .line 489075
    iget-object v1, v1, LX/4qM;->b:Ljava/lang/Object;

    .line 489076
    if-nez v1, :cond_0

    .line 489077
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not resolve Object Id ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] (for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") -- unresolved forward-reference?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 489078
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 489079
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    .line 489080
    invoke-virtual {p1}, LX/0n3;->f()LX/0lU;

    move-result-object v6

    .line 489081
    if-eqz p2, :cond_0

    if-nez v6, :cond_1

    :cond_0
    move-object v5, v3

    .line 489082
    :goto_0
    if-eqz p2, :cond_a

    if-eqz v6, :cond_a

    .line 489083
    invoke-virtual {v6, v5}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v4

    .line 489084
    invoke-virtual {v6, v5}, LX/0lU;->a(LX/0lO;)LX/4qt;

    move-result-object v1

    .line 489085
    if-eqz v1, :cond_9

    .line 489086
    invoke-virtual {v6, v5, v1}, LX/0lU;->a(LX/0lO;LX/4qt;)LX/4qt;

    move-result-object v7

    .line 489087
    iget-object v0, v7, LX/4qt;->b:Ljava/lang/Class;

    move-object v0, v0

    .line 489088
    const-class v1, LX/4pV;

    if-ne v0, v1, :cond_5

    .line 489089
    iget-object v0, v7, LX/4qt;->a:Ljava/lang/String;

    move-object v0, v0

    .line 489090
    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/String;)LX/32s;

    move-result-object v2

    .line 489091
    if-nez v2, :cond_2

    .line 489092
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Object Id definition for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": can not find property with name \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 489093
    :cond_1
    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    move-object v5, v1

    goto :goto_0

    .line 489094
    :cond_2
    invoke-virtual {v2}, LX/32s;->a()LX/0lJ;

    move-result-object v1

    .line 489095
    new-instance v0, LX/4qG;

    .line 489096
    iget-object v8, v7, LX/4qt;->c:Ljava/lang/Class;

    move-object v8, v8

    .line 489097
    invoke-direct {v0, v8}, LX/4qG;-><init>(Ljava/lang/Class;)V

    .line 489098
    :goto_1
    invoke-virtual {p1, v1}, LX/0n3;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v8

    .line 489099
    iget-object p1, v7, LX/4qt;->a:Ljava/lang/String;

    move-object v7, p1

    .line 489100
    invoke-static {v1, v7, v0, v8, v2}, LX/4qD;->a(LX/0lJ;Ljava/lang/String;LX/4pS;Lcom/fasterxml/jackson/databind/JsonDeserializer;LX/32s;)LX/4qD;

    move-result-object v0

    move-object v1, v0

    move-object v0, v4

    .line 489101
    :goto_2
    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eq v1, v2, :cond_8

    .line 489102
    invoke-virtual {p0, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    .line 489103
    :goto_3
    if-eqz v0, :cond_3

    array-length v2, v0

    if-eqz v2, :cond_3

    .line 489104
    iget-object v2, v1, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-static {v2, v0}, LX/0nj;->a(Ljava/util/Set;[Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 489105
    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v1

    .line 489106
    :cond_3
    if-eqz v5, :cond_7

    .line 489107
    invoke-virtual {v6, v5}, LX/0lU;->e(LX/0lO;)LX/4pN;

    move-result-object v0

    .line 489108
    if-eqz v0, :cond_7

    .line 489109
    iget-object v2, v0, LX/4pN;->b:LX/2BA;

    move-object v3, v2

    .line 489110
    move-object v0, v3

    .line 489111
    :goto_4
    if-nez v0, :cond_4

    .line 489112
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_serializationShape:LX/2BA;

    .line 489113
    :cond_4
    sget-object v2, LX/2BA;->ARRAY:LX/2BA;

    if-ne v0, v2, :cond_6

    .line 489114
    invoke-virtual {v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a()Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;

    move-result-object v0

    .line 489115
    :goto_5
    return-object v0

    .line 489116
    :cond_5
    invoke-virtual {p1, v0}, LX/0n3;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 489117
    invoke-virtual {p1}, LX/0mz;->c()LX/0li;

    move-result-object v1

    const-class v2, LX/4pS;

    invoke-virtual {v1, v0, v2}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 489118
    invoke-virtual {p1, v5, v7}, LX/0mz;->a(LX/0lO;LX/4qt;)LX/4pS;

    move-result-object v0

    move-object v2, v3

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move-object v0, v3

    goto :goto_4

    :cond_8
    move-object v1, p0

    goto :goto_3

    :cond_9
    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    :cond_a
    move-object v1, v0

    move-object v0, v3

    goto :goto_2
.end method

.method public abstract a()Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.end method

.method public abstract a(LX/4qD;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
.end method

.method public abstract a(Ljava/util/HashSet;)Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;"
        }
    .end annotation
.end method

.method public final a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488853
    invoke-virtual {p3}, LX/0nX;->g()V

    .line 488854
    invoke-virtual {p3}, LX/0nW;->i()LX/15w;

    move-result-object v0

    .line 488855
    :goto_0
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 488856
    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 488857
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 488858
    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 488859
    :cond_0
    return-object p2
.end method

.method public abstract a(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488640
    invoke-direct {p0, p2, p3}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(LX/0n3;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 488641
    if-eqz v1, :cond_1

    .line 488642
    if-eqz p4, :cond_3

    .line 488643
    invoke-virtual {p4}, LX/0nX;->g()V

    .line 488644
    invoke-virtual {p4}, LX/0nW;->i()LX/15w;

    move-result-object v0

    .line 488645
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 488646
    invoke-virtual {v1, v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488647
    :goto_0
    if-eqz p1, :cond_0

    .line 488648
    invoke-virtual {v1, p1, p2, v0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488649
    :cond_0
    :goto_1
    return-object v0

    .line 488650
    :cond_1
    if-eqz p4, :cond_2

    .line 488651
    invoke-virtual {p0, p2, p3, p4}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    .line 488652
    :goto_2
    if-eqz p1, :cond_0

    .line 488653
    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, p3

    goto :goto_2

    :cond_3
    move-object v0, p3

    goto :goto_0
.end method

.method public final a(LX/0n3;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 488654
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488655
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    .line 488656
    iget-object v2, p1, LX/0n3;->_config:LX/0mu;

    move-object v2, v2

    .line 488657
    invoke-virtual {v0, v2}, LX/320;->a(LX/0mu;)[LX/32s;

    move-result-object v0

    .line 488658
    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-static {p1, v2, v0}, LX/4qF;->a(LX/0n3;LX/320;[LX/32s;)LX/4qF;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    .line 488659
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    invoke-virtual {v0}, LX/4qF;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 488660
    invoke-virtual {v0}, LX/32s;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 488661
    iget-object v4, v0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    move-object v4, v4

    .line 488662
    invoke-virtual {v4}, LX/4qw;->a()LX/4pO;

    move-result-object v5

    sget-object v7, LX/4pO;->EXTERNAL_PROPERTY:LX/4pO;

    if-ne v5, v7, :cond_0

    .line 488663
    if-nez v2, :cond_1

    .line 488664
    new-instance v2, LX/4q6;

    invoke-direct {v2}, LX/4q6;-><init>()V

    .line 488665
    :cond_1
    invoke-virtual {v2, v0, v4}, LX/4q6;->a(LX/32s;LX/4qw;)V

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 488666
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0}, LX/32t;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v7, v1

    move-object v8, v2

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 488667
    invoke-virtual {v0}, LX/32s;->j()Z

    move-result v2

    if-nez v2, :cond_6

    .line 488668
    invoke-static {p1, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;LX/32s;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 488669
    if-nez v2, :cond_5

    .line 488670
    invoke-virtual {v0}, LX/32s;->a()LX/0lJ;

    move-result-object v2

    invoke-static {p1, v2, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 488671
    :cond_5
    invoke-virtual {v0, v2}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v2

    .line 488672
    :goto_2
    invoke-direct {p0, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/32s;)LX/32s;

    move-result-object v2

    .line 488673
    invoke-static {p1, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(LX/0n3;LX/32s;)LX/32s;

    move-result-object v3

    .line 488674
    if-eqz v3, :cond_7

    .line 488675
    if-nez v7, :cond_11

    .line 488676
    new-instance v0, LX/4qO;

    invoke-direct {v0}, LX/4qO;-><init>()V

    .line 488677
    :goto_3
    invoke-virtual {v0, v3}, LX/4qO;->a(LX/32s;)V

    move-object v7, v0

    .line 488678
    goto :goto_1

    .line 488679
    :cond_6
    invoke-virtual {v0}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v3

    .line 488680
    instance-of v2, v3, LX/1Xy;

    if-eqz v2, :cond_12

    move-object v2, v3

    .line 488681
    check-cast v2, LX/1Xy;

    invoke-interface {v2, p1, v0}, LX/1Xy;->a(LX/0n3;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 488682
    if-eq v2, v3, :cond_12

    .line 488683
    invoke-virtual {v0, v2}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v2

    goto :goto_2

    .line 488684
    :cond_7
    invoke-direct {p0, p1, v2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->c(LX/0n3;LX/32s;)LX/32s;

    move-result-object v3

    .line 488685
    if-eq v3, v0, :cond_8

    .line 488686
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0, v3}, LX/32t;->b(LX/32s;)V

    .line 488687
    :cond_8
    invoke-virtual {v3}, LX/32s;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 488688
    iget-object v0, v3, LX/32s;->_valueTypeDeserializer:LX/4qw;

    move-object v0, v0

    .line 488689
    invoke-virtual {v0}, LX/4qw;->a()LX/4pO;

    move-result-object v2

    sget-object v5, LX/4pO;->EXTERNAL_PROPERTY:LX/4pO;

    if-ne v2, v5, :cond_4

    .line 488690
    if-nez v8, :cond_10

    .line 488691
    new-instance v2, LX/4q6;

    invoke-direct {v2}, LX/4q6;-><init>()V

    .line 488692
    :goto_4
    invoke-virtual {v2, v3, v0}, LX/4q6;->a(LX/32s;LX/4qw;)V

    .line 488693
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0, v3}, LX/32t;->c(LX/32s;)V

    move-object v8, v2

    .line 488694
    goto :goto_1

    .line 488695
    :cond_9
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v0}, LX/4q5;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 488696
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488697
    iget-object v3, v2, LX/4q5;->c:LX/0lJ;

    move-object v2, v3

    .line 488698
    iget-object v3, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488699
    iget-object v4, v3, LX/4q5;->a:LX/2Ay;

    move-object v3, v4

    .line 488700
    invoke-static {p1, v2, v3}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4q5;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q5;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    .line 488701
    :cond_a
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 488702
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->k()LX/0lJ;

    move-result-object v2

    .line 488703
    if-nez v2, :cond_b

    .line 488704
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid delegate-creator definition for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": value instantiator ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") returned true for \'canCreateUsingDelegate()\', but null for \'getDelegateType()\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488705
    :cond_b
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->n()LX/2Au;

    move-result-object v5

    .line 488706
    new-instance v0, LX/2B3;

    iget-object v4, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b:LX/0lQ;

    move-object v3, v1

    invoke-direct/range {v0 .. v6}, LX/2B3;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/0lQ;LX/2An;Z)V

    .line 488707
    invoke-static {p1, v2, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/0n3;LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 488708
    :cond_c
    if-eqz v8, :cond_d

    .line 488709
    invoke-virtual {v8}, LX/4q6;->a()LX/4q8;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_externalTypeIdHandler:LX/4q8;

    .line 488710
    iput-boolean v9, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488711
    :cond_d
    iput-object v7, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_unwrappedPropertyHandler:LX/4qO;

    .line 488712
    if-eqz v7, :cond_e

    .line 488713
    iput-boolean v9, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    .line 488714
    :cond_e
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_nonStandardCreation:Z

    if-nez v0, :cond_f

    move v6, v9

    :cond_f
    iput-boolean v6, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_vanillaProcessing:Z

    .line 488715
    return-void

    :cond_10
    move-object v2, v8

    goto/16 :goto_4

    :cond_11
    move-object v0, v7

    goto/16 :goto_3

    :cond_12
    move-object v2, v0

    goto/16 :goto_2
.end method

.method public final a(LX/0n3;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 488716
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 488717
    invoke-virtual {v3, p1, p2}, LX/4qP;->a(LX/0n3;Ljava/lang/Object;)V

    .line 488718
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488719
    :cond_0
    return-void
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 488720
    iget-boolean v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignoreAllUnknown:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v0, p4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488721
    :cond_0
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488722
    :goto_0
    return-void

    .line 488723
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;LX/0n3;)V
    .locals 3

    .prologue
    .line 488724
    move-object v0, p1

    :goto_0
    instance-of v1, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 488725
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 488726
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 488727
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 488728
    :cond_1
    if-eqz p2, :cond_2

    sget-object v1, LX/0mv;->WRAP_EXCEPTIONS:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 488729
    :goto_1
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_4

    .line 488730
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 488731
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 488732
    :cond_4
    if-nez v1, :cond_5

    .line 488733
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_5

    .line 488734
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 488735
    :cond_5
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488736
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 488737
    invoke-virtual {p2, v1, v0}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/Throwable;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 488738
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    .line 488739
    iget-object p0, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p0

    .line 488740
    return-object v0
.end method

.method public abstract b(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 488741
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_ignorableProps:Ljava/util/HashSet;

    invoke-virtual {v0, p4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488742
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 488743
    :goto_0
    return-void

    .line 488744
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    if-eqz v0, :cond_1

    .line 488745
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_anySetter:LX/4q5;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/4q5;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 488746
    :catch_0
    move-exception v0

    .line 488747
    invoke-static {v0, p3, p4, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;LX/0n3;)V

    goto :goto_0

    .line 488748
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 488749
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    iget-object v3, v0, LX/4qD;->propertyName:Ljava/lang/String;

    .line 488750
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488751
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488752
    :goto_0
    return-object v0

    .line 488753
    :cond_0
    new-instance v1, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0nW;-><init>(LX/0lD;)V

    move-object v0, v2

    .line 488754
    :goto_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_3

    .line 488755
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 488756
    if-nez v0, :cond_2

    .line 488757
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 488758
    new-instance v0, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v5

    invoke-direct {v0, v5}, LX/0nW;-><init>(LX/0lD;)V

    .line 488759
    invoke-virtual {v0, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488760
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488761
    invoke-virtual {v0, p1}, LX/0nW;->b(LX/15w;)V

    .line 488762
    invoke-virtual {v0, v1}, LX/0nW;->a(LX/0nW;)LX/0nW;

    move-object v1, v2

    .line 488763
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_1

    .line 488764
    :cond_1
    invoke-virtual {v1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488765
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488766
    invoke-virtual {v1, p1}, LX/0nW;->b(LX/15w;)V

    goto :goto_2

    .line 488767
    :cond_2
    invoke-virtual {v0, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 488768
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 488769
    invoke-virtual {v0, p1}, LX/0nW;->b(LX/15w;)V

    goto :goto_2

    .line 488770
    :cond_3
    if-nez v0, :cond_4

    .line 488771
    :goto_3
    invoke-virtual {v1}, LX/0nX;->g()V

    .line 488772
    invoke-virtual {v1}, LX/0nW;->i()LX/15w;

    move-result-object v0

    .line 488773
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 488774
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v1, v0

    .line 488775
    goto :goto_3
.end method

.method public final d(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488776
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    .line 488777
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488778
    :goto_0
    return-object v0

    .line 488779
    :cond_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_propertyBasedCreator:LX/4qF;

    if-eqz v0, :cond_1

    .line 488780
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488781
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488782
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not instantiate abstract type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 488783
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No suitable constructor found for type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": can not instantiate from JSON object (need to add/enable type information?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/28E;->a(LX/15w;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 488784
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_0

    .line 488785
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 488786
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/15z;->isScalarValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488787
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488788
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p3, p1, p2}, LX/4qw;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 488789
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_1

    .line 488790
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488791
    :cond_0
    :goto_0
    return-object v0

    .line 488792
    :cond_1
    sget-object v0, LX/4q1;->a:[I

    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v1

    invoke-virtual {v1}, LX/16L;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 488793
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_4

    .line 488794
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488795
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488796
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    goto :goto_0

    .line 488797
    :pswitch_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_2

    .line 488798
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 488799
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488800
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488801
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    goto :goto_0

    .line 488802
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->x()I

    move-result v1

    invoke-virtual {v0, v1}, LX/320;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488803
    :pswitch_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_3

    .line 488804
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 488805
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488806
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488807
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    goto :goto_0

    .line 488808
    :cond_3
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->y()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/320;->a(J)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488809
    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "no suitable creator method found to deserialize from JSON integer number"

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488810
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    if-eqz v0, :cond_1

    .line 488811
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->x(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 488812
    :cond_0
    :goto_0
    return-object v0

    .line 488813
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_2

    .line 488814
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 488815
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488816
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488817
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    goto :goto_0

    .line 488818
    :cond_2
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 488819
    sget-object v0, LX/4q1;->a:[I

    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v1

    invoke-virtual {v1}, LX/16L;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 488820
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_2

    .line 488821
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 488822
    :pswitch_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_1

    .line 488823
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 488824
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488825
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488826
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    goto :goto_0

    .line 488827
    :cond_1
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/320;->a(D)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488828
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "no suitable creator method found to deserialize from JSON floating-point number"

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getKnownPropertyNames()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488829
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 488830
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_beanProperties:LX/32t;

    invoke-virtual {v0}, LX/32t;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 488831
    iget-object p0, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v0, p0

    .line 488832
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 488833
    :cond_0
    return-object v1
.end method

.method public final getObjectIdReader()LX/4qD;
    .locals 1

    .prologue
    .line 488834
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_objectIdReader:LX/4qD;

    return-object v0
.end method

.method public final h(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488835
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_1

    .line 488836
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v0}, LX/320;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 488837
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488838
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488839
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V

    .line 488840
    :cond_0
    :goto_0
    return-object v0

    .line 488841
    :cond_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_TRUE:LX/15z;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 488842
    :goto_1
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    invoke-virtual {v1, v0}, LX/320;->a(Z)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 488843
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final i(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 488844
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_1

    .line 488845
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_valueInstantiator:LX/320;

    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_delegateDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/320;->a(LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 488846
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->_injectables:[LX/4qP;

    if-eqz v1, :cond_0

    .line 488847
    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(LX/0n3;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 488848
    :cond_0
    return-object v0

    .line 488849
    :catch_0
    move-exception v0

    .line 488850
    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->a(Ljava/lang/Throwable;LX/0n3;)V

    .line 488851
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializerBase;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final isCachable()Z
    .locals 1

    .prologue
    .line 488852
    const/4 v0, 0x1

    return v0
.end method

.method public abstract unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4ro;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method
