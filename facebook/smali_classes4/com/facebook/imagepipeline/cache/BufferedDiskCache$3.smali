.class public final Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1bh;

.field public final synthetic b:LX/1FL;

.field public final synthetic c:LX/1HY;


# direct methods
.method public constructor <init>(LX/1HY;LX/1bh;LX/1FL;)V
    .locals 0

    .prologue
    .line 490337
    iput-object p1, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->c:LX/1HY;

    iput-object p2, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->a:LX/1bh;

    iput-object p3, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 490338
    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->c:LX/1HY;

    iget-object v1, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->a:LX/1bh;

    iget-object v2, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    .line 490339
    invoke-interface {v1}, LX/1bh;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490340
    :try_start_1
    iget-object v3, v0, LX/1HY;->b:LX/1Ha;

    new-instance v4, LX/2ut;

    invoke-direct {v4, v0, v2}, LX/2ut;-><init>(LX/1HY;LX/1FL;)V

    invoke-interface {v3, v1, v4}, LX/1Ha;->a(LX/1bh;LX/2uu;)LX/1gI;

    .line 490341
    invoke-interface {v1}, LX/1bh;->a()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490342
    :goto_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->c:LX/1HY;

    iget-object v0, v0, LX/1HY;->g:LX/1IY;

    iget-object v1, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->a:LX/1bh;

    iget-object v2, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    invoke-virtual {v0, v1, v2}, LX/1IY;->b(LX/1bh;LX/1FL;)Z

    .line 490343
    iget-object v0, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 490344
    return-void

    .line 490345
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->c:LX/1HY;

    iget-object v1, v1, LX/1HY;->g:LX/1IY;

    iget-object v2, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->a:LX/1bh;

    iget-object v3, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    invoke-virtual {v1, v2, v3}, LX/1IY;->b(LX/1bh;LX/1FL;)Z

    .line 490346
    iget-object v1, p0, Lcom/facebook/imagepipeline/cache/BufferedDiskCache$3;->b:LX/1FL;

    invoke-static {v1}, LX/1FL;->d(LX/1FL;)V

    throw v0

    .line 490347
    :catch_0
    move-exception v3

    .line 490348
    sget-object v4, LX/1HY;->a:Ljava/lang/Class;

    const-string v5, "Failed to write to disk-cache for key %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v1}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v3, v5, v6}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
