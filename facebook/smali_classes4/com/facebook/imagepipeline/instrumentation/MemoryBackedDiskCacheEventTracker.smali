.class public Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2xx;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zb;

.field private final c:LX/0SG;

.field private d:J


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448191
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    .line 448192
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->b:LX/0Zb;

    .line 448193
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    .line 448194
    return-void
.end method

.method private a(Ljava/lang/String;)LX/2xx;
    .locals 8

    .prologue
    .line 448184
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xx;

    .line 448185
    if-nez v0, :cond_0

    .line 448186
    const-wide/16 v4, 0x0

    .line 448187
    new-instance v2, LX/2xx;

    const/4 v3, 0x0

    move-wide v6, v4

    invoke-direct/range {v2 .. v7}, LX/2xx;-><init>(Lcom/facebook/common/callercontext/CallerContext;JJ)V

    move-object v0, v2

    .line 448188
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448189
    :cond_0
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;
    .locals 5

    .prologue
    .line 448090
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->e:Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;

    if-nez v0, :cond_1

    .line 448091
    const-class v1, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;

    monitor-enter v1

    .line 448092
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->e:Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448093
    if-eqz v2, :cond_0

    .line 448094
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448095
    new-instance p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;-><init>(LX/0Zb;LX/0SG;)V

    .line 448096
    move-object v0, p0

    .line 448097
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->e:Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448098
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448099
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448100
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->e:Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;

    return-object v0

    .line 448101
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448102
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0oG;Ljava/lang/String;LX/2xx;)V
    .locals 8

    .prologue
    .line 448159
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 448160
    const-string v2, "event"

    invoke-virtual {p1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "hit_count"

    .line 448161
    iget v4, p3, LX/2xx;->e:I

    move v4, v4

    .line 448162
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v2

    const-string v3, "cache_size"

    iget-wide v4, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->d:J

    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 448163
    iget-object v2, p3, LX/2xx;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 448164
    if-eqz v2, :cond_0

    .line 448165
    iget-object v2, p3, LX/2xx;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 448166
    const-string v3, "original_analytics_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "original_calling_class"

    .line 448167
    iget-object v5, v2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v5, v5

    .line 448168
    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "original_feature_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "bytes"

    .line 448169
    iget-wide v6, p3, LX/2xx;->b:J

    move-wide v4, v6

    .line 448170
    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v2

    const-string v3, "age_in_cache"

    .line 448171
    iget-wide v6, p3, LX/2xx;->c:J

    move-wide v4, v6

    .line 448172
    sub-long v4, v0, v4

    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 448173
    :cond_0
    iget-object v2, p3, LX/2xx;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 448174
    if-eqz v2, :cond_1

    .line 448175
    const-string v3, "latest_analytics_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "latest_calling_class"

    .line 448176
    iget-object v5, v2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v5, v5

    .line 448177
    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "latest_feature_tag"

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 448178
    :cond_1
    iget-wide v6, p3, LX/2xx;->f:J

    move-wide v2, v6

    .line 448179
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 448180
    const-string v2, "age_since_last_hit"

    .line 448181
    iget-wide v6, p3, LX/2xx;->f:J

    move-wide v4, v6

    .line 448182
    sub-long/2addr v0, v4

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 448183
    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;LX/2xx;)V
    .locals 2

    .prologue
    .line 448154
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v0

    .line 448155
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 448156
    :goto_0
    return-void

    .line 448157
    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/2xx;)V

    .line 448158
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;LX/2xx;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 448148
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v0

    .line 448149
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 448150
    :goto_0
    return-void

    .line 448151
    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/2xx;)V

    .line 448152
    const-string v1, "exception"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 448153
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method private b()LX/0oG;
    .locals 3

    .prologue
    .line 448147
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->b:LX/0Zb;

    const-string v1, "fresco_disk_cache_event"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method

.method private static h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2

    .prologue
    .line 448140
    invoke-interface {p0}, LX/1gC;->a()LX/1bh;

    move-result-object v0

    .line 448141
    instance-of v1, v0, LX/1ec;

    if-eqz v1, :cond_0

    .line 448142
    check-cast v0, LX/1ec;

    .line 448143
    iget-object v1, v0, LX/1ec;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 448144
    instance-of v1, v0, Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v1, :cond_0

    .line 448145
    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 448146
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 448138
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 448139
    return-void
.end method

.method public final a(LX/1gC;)V
    .locals 4

    .prologue
    .line 448132
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;)LX/2xx;

    move-result-object v0

    .line 448133
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 448134
    iput-object v1, v0, LX/2xx;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 448135
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2xx;->a(J)V

    .line 448136
    const-string v1, "hit"

    invoke-direct {p0, v1, v0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/2xx;)V

    .line 448137
    return-void
.end method

.method public final b(LX/1gC;)V
    .locals 0

    .prologue
    .line 448131
    return-void
.end method

.method public final c(LX/1gC;)V
    .locals 0

    .prologue
    .line 448130
    return-void
.end method

.method public final d(LX/1gC;)V
    .locals 6

    .prologue
    .line 448124
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 448125
    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, LX/2xx;->a(Lcom/facebook/common/callercontext/CallerContext;JJ)LX/2xx;

    move-result-object v0

    .line 448126
    invoke-interface {p1}, LX/1gC;->d()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->d:J

    .line 448127
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448128
    const-string v1, "write"

    invoke-direct {p0, v1, v0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/2xx;)V

    .line 448129
    return-void
.end method

.method public final e(LX/1gC;)V
    .locals 4

    .prologue
    .line 448118
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;)LX/2xx;

    move-result-object v0

    .line 448119
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 448120
    iput-object v1, v0, LX/2xx;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 448121
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2xx;->a(J)V

    .line 448122
    const-string v1, "read_exception"

    invoke-interface {p1}, LX/1gC;->f()Ljava/io/IOException;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/2xx;Ljava/io/IOException;)V

    .line 448123
    return-void
.end method

.method public final f(LX/1gC;)V
    .locals 6

    .prologue
    .line 448114
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 448115
    const-wide/16 v2, 0x0

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, LX/2xx;->a(Lcom/facebook/common/callercontext/CallerContext;JJ)LX/2xx;

    move-result-object v0

    .line 448116
    const-string v1, "write_exception"

    invoke-interface {p1}, LX/1gC;->f()Ljava/io/IOException;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/2xx;Ljava/io/IOException;)V

    .line 448117
    return-void
.end method

.method public final g(LX/1gC;)V
    .locals 6

    .prologue
    .line 448103
    invoke-interface {p1}, LX/1gC;->g()LX/37E;

    move-result-object v0

    .line 448104
    invoke-interface {p1}, LX/1gC;->d()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->d:J

    .line 448105
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v1

    .line 448106
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v2

    .line 448107
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 448108
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448109
    :goto_0
    return-void

    .line 448110
    :cond_0
    invoke-direct {p0, v2}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(Ljava/lang/String;)LX/2xx;

    move-result-object v3

    .line 448111
    const-string v4, "eviction"

    invoke-direct {p0, v1, v4, v3}, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/2xx;)V

    .line 448112
    const-string v3, "cache_limit"

    invoke-interface {p1}, LX/1gC;->e()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v3, "eviction_reason"

    invoke-virtual {v1, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 448113
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/MemoryBackedDiskCacheEventTracker;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
