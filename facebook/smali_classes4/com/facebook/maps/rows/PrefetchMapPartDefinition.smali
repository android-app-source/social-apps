.class public Lcom/facebook/maps/rows/PrefetchMapPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3Ba;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/maps/FbStaticMapView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 528020
    const-class v0, Lcom/facebook/maps/rows/MapPartDefinition;

    const-string v1, "newsfeed_map_view"

    const-string v2, "map"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528021
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 528022
    iput-object p1, p0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->b:LX/0Ot;

    .line 528023
    iput-object p2, p0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->c:LX/0Ot;

    .line 528024
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/maps/rows/PrefetchMapPartDefinition;
    .locals 5

    .prologue
    .line 528025
    const-class v1, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    monitor-enter v1

    .line 528026
    :try_start_0
    sget-object v0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528027
    sput-object v2, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528028
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528029
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528030
    new-instance v3, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    const/16 v4, 0x2be

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 528031
    move-object v0, v3

    .line 528032
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528033
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528034
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528035
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 528036
    check-cast p2, LX/3Ba;

    check-cast p3, LX/1Pt;

    .line 528037
    iget-object v0, p2, LX/3Ba;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 528038
    iget-object v0, p2, LX/3Ba;->d:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v1, p2, LX/3Ba;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p2, LX/3Ba;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 528039
    iget-object v3, p0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    .line 528040
    const/4 v5, 0x2

    iget-object v3, p0, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    move v3, v1

    move v4, v2

    move-object v8, v0

    invoke-static/range {v3 .. v8}, LX/3BP;->a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;

    move-result-object v3

    .line 528041
    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    sget-object v4, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v3, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 528042
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
