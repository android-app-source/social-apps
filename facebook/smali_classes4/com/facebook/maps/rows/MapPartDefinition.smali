.class public Lcom/facebook/maps/rows/MapPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3BY;",
        "Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;",
        "LX/1PW;",
        "Lcom/facebook/maps/FbStaticMapView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527989
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 527990
    iput-object p1, p0, Lcom/facebook/maps/rows/MapPartDefinition;->a:LX/0Ot;

    .line 527991
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/maps/rows/MapPartDefinition;
    .locals 4

    .prologue
    .line 527992
    const-class v1, Lcom/facebook/maps/rows/MapPartDefinition;

    monitor-enter v1

    .line 527993
    :try_start_0
    sget-object v0, Lcom/facebook/maps/rows/MapPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527994
    sput-object v2, Lcom/facebook/maps/rows/MapPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527995
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527996
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527997
    new-instance v3, Lcom/facebook/maps/rows/MapPartDefinition;

    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/maps/rows/MapPartDefinition;-><init>(LX/0Ot;)V

    .line 527998
    move-object v0, v3

    .line 527999
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528000
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/maps/rows/MapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528001
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 528003
    check-cast p2, LX/3BY;

    .line 528004
    iget-object v0, p2, LX/3BY;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7e360df1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 528005
    check-cast p1, LX/3BY;

    check-cast p2, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    check-cast p4, Lcom/facebook/maps/FbStaticMapView;

    .line 528006
    invoke-virtual {p4}, Lcom/facebook/maps/FbStaticMapView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 528007
    if-nez v1, :cond_1

    .line 528008
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p1, LX/3BY;->c:I

    iget p3, p1, LX/3BY;->d:I

    invoke-direct {v1, v2, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 528009
    invoke-virtual {p4, v1}, Lcom/facebook/maps/FbStaticMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 528010
    :cond_0
    :goto_0
    invoke-virtual {p4, p2}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 528011
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/3BP;->setReportButtonVisibility(I)V

    .line 528012
    iget-boolean v1, p1, LX/3BY;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/maps/rows/MapPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v2, 0x7f020e90

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_1
    invoke-virtual {p4, v1}, LX/3BP;->setCenteredMapPinDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 528013
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 528014
    const/16 v1, 0x1f

    const v2, -0x1971663a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 528015
    :cond_1
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget p3, p1, LX/3BY;->c:I

    if-ne v2, p3, :cond_2

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget p3, p1, LX/3BY;->d:I

    if-eq v2, p3, :cond_0

    .line 528016
    :cond_2
    iget v2, p1, LX/3BY;->c:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 528017
    iget v2, p1, LX/3BY;->d:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 528018
    invoke-virtual {p4, v1}, Lcom/facebook/maps/FbStaticMapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 528019
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
