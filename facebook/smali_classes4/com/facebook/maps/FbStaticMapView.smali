.class public Lcom/facebook/maps/FbStaticMapView;
.super LX/3BP;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/0yY;

.field public g:LX/121;

.field public h:LX/6Zs;

.field public i:LX/0yc;

.field private j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public k:LX/0gc;

.field private l:LX/3BS;

.field private m:LX/1Ad;

.field private n:LX/1FZ;

.field public o:J

.field public p:I

.field public q:I

.field public r:I

.field public s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 487351
    invoke-direct {p0, p1}, LX/3BP;-><init>(Landroid/content/Context;)V

    .line 487352
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/maps/FbStaticMapView;->o:J

    .line 487353
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/maps/FbStaticMapView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 487354
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 487355
    invoke-direct {p0, p1, p2}, LX/3BP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 487356
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/maps/FbStaticMapView;->o:J

    .line 487357
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/maps/FbStaticMapView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 487358
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 487359
    invoke-direct {p0, p1, p2, p3}, LX/3BP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 487360
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/maps/FbStaticMapView;->o:J

    .line 487361
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/maps/FbStaticMapView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 487362
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 487363
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getPaddingTop()I

    move-result v2

    .line 487364
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getPaddingRight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getPaddingBottom()I

    move-result v4

    sub-int v4, v0, v4

    .line 487365
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->n:LX/1FZ;

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getHeight()I

    move-result v6

    invoke-virtual {v0, v5, v6}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 487366
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 487367
    new-instance v6, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 487368
    invoke-virtual {v6, v9}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 487369
    const/16 v7, 0x8

    new-array v7, v7, [F

    .line 487370
    iget v8, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    move v8, v8

    .line 487371
    int-to-float v8, v8

    aput v8, v7, v9

    const/4 v8, 0x1

    .line 487372
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    move v9, v9

    .line 487373
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x2

    .line 487374
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    move v9, v9

    .line 487375
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x3

    .line 487376
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    move v9, v9

    .line 487377
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x4

    .line 487378
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    move v9, v9

    .line 487379
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x5

    .line 487380
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    move v9, v9

    .line 487381
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x6

    .line 487382
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    move v9, v9

    .line 487383
    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x7

    .line 487384
    iget v9, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    move v9, v9

    .line 487385
    int-to-float v9, v9

    aput v9, v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 487386
    const v7, -0xbdbdbe

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 487387
    invoke-virtual {v6, v1, v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 487388
    invoke-virtual {v6, v5}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 487389
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 487390
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 487391
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 487392
    invoke-virtual {v5, p1, v7, v7, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 487393
    return-object v0
.end method

.method private a(LX/0W9;LX/3BR;LX/3BS;LX/31d;LX/121;Lcom/facebook/auth/viewercontext/ViewerContext;LX/1Ad;LX/0yc;LX/1FZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 487394
    invoke-virtual {p1}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->d:Ljava/lang/String;

    .line 487395
    iput-object p5, p0, Lcom/facebook/maps/FbStaticMapView;->g:LX/121;

    .line 487396
    iput-object p3, p0, Lcom/facebook/maps/FbStaticMapView;->l:LX/3BS;

    .line 487397
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 487398
    iget-object v1, p6, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v1, v1

    .line 487399
    invoke-static {v0, v1, p4}, LX/3BU;->a(Landroid/content/Context;Ljava/lang/String;LX/31d;)V

    .line 487400
    invoke-static {p2}, LX/31U;->a(LX/3BR;)V

    .line 487401
    const/4 v0, 0x2

    .line 487402
    iput v0, p0, LX/3BP;->k:I

    .line 487403
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3BP;->setReportButtonVisibility(I)V

    .line 487404
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->l:LX/3BS;

    invoke-virtual {p0, v0}, LX/3BP;->setMapReporterLauncher(LX/3BT;)V

    .line 487405
    iput-object p7, p0, Lcom/facebook/maps/FbStaticMapView;->m:LX/1Ad;

    .line 487406
    iput-object p8, p0, Lcom/facebook/maps/FbStaticMapView;->i:LX/0yc;

    .line 487407
    iput-object p9, p0, Lcom/facebook/maps/FbStaticMapView;->n:LX/1FZ;

    .line 487408
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/maps/FbStaticMapView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 487409
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 487410
    const-class v0, Lcom/facebook/maps/FbStaticMapView;

    invoke-static {v0, p0}, Lcom/facebook/maps/FbStaticMapView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 487411
    sget-object v0, LX/03r;->FbStaticMapView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 487412
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    .line 487413
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    .line 487414
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    .line 487415
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    .line 487416
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 487417
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/maps/FbStaticMapView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    invoke-static {v9}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {v9}, LX/3BR;->a(LX/0QB;)LX/3BR;

    move-result-object v2

    check-cast v2, LX/3BR;

    invoke-static {v9}, LX/3BS;->a(LX/0QB;)LX/3BS;

    move-result-object v3

    check-cast v3, LX/3BS;

    invoke-static {v9}, LX/31d;->a(LX/0QB;)LX/31d;

    move-result-object v4

    check-cast v4, LX/31d;

    invoke-static {v9}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v5

    check-cast v5, LX/121;

    invoke-static {v9}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v9}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {v9}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    invoke-static {v9}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v9

    check-cast v9, LX/1FZ;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/maps/FbStaticMapView;->a(LX/0W9;LX/3BR;LX/3BS;LX/31d;LX/121;Lcom/facebook/auth/viewercontext/ViewerContext;LX/1Ad;LX/0yc;LX/1FZ;)V

    return-void
.end method

.method private b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 2

    .prologue
    .line 487418
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 487419
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 487420
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    move v0, v0

    .line 487421
    if-gtz v0, :cond_0

    .line 487422
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    move v0, v0

    .line 487423
    if-gtz v0, :cond_0

    .line 487424
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    move v0, v0

    .line 487425
    if-gtz v0, :cond_0

    .line 487426
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    move v0, v0

    .line 487427
    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lcom/facebook/resources/ui/FbTextView;
    .locals 4

    .prologue
    .line 487428
    new-instance v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 487429
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 487430
    const v2, 0x7f0801c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 487431
    const v2, -0x958e80

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 487432
    const/4 v2, 0x0

    const v3, 0x7f0b0056

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 487433
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 487434
    const v1, 0x7f02182d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 487435
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 487436
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Landroid/view/View;
    .locals 1

    .prologue
    .line 487350
    invoke-direct {p0}, Lcom/facebook/maps/FbStaticMapView;->b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0yY;LX/0gc;LX/6Zs;)V
    .locals 1
    .param p3    # LX/6Zs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 487342
    iput-object p1, p0, Lcom/facebook/maps/FbStaticMapView;->f:LX/0yY;

    .line 487343
    iput-object p2, p0, Lcom/facebook/maps/FbStaticMapView;->k:LX/0gc;

    .line 487344
    iput-object p3, p0, Lcom/facebook/maps/FbStaticMapView;->h:LX/6Zs;

    .line 487345
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->g:LX/121;

    invoke-virtual {v0, p1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/maps/FbStaticMapView;->setZeroRatingEnabled(Z)V

    .line 487346
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->l:LX/3BS;

    .line 487347
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/3BS;->g:Z

    .line 487348
    iput-object p2, v0, LX/3BS;->h:LX/0gc;

    .line 487349
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 487290
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    const/4 v1, 0x1

    .line 487291
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 487292
    move-object v0, v0

    .line 487293
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 487294
    const/4 v0, 0x0

    .line 487295
    sget-object v1, LX/31U;->C:LX/31U;

    invoke-virtual {v1}, LX/31U;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 487296
    invoke-static {}, LX/31U;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/maps/FbStaticMapView;->o:J

    .line 487297
    new-instance v0, LX/6Zr;

    invoke-direct {v0, p0, p3}, LX/6Zr;-><init>(Lcom/facebook/maps/FbStaticMapView;Ljava/lang/String;)V

    move-object v1, v0

    .line 487298
    :goto_0
    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 487299
    invoke-direct {p0}, Lcom/facebook/maps/FbStaticMapView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487300
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    move v0, v0

    .line 487301
    int-to-float v0, v0

    .line 487302
    iget v3, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    move v3, v3

    .line 487303
    int-to-float v3, v3

    .line 487304
    iget v4, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    move v4, v4

    .line 487305
    int-to-float v4, v4

    .line 487306
    iget v5, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    move v5, v5

    .line 487307
    int-to-float v5, v5

    invoke-static {v0, v3, v4, v5}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v0

    .line 487308
    new-instance v3, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 487309
    iput-object v0, v3, LX/1Uo;->u:LX/4Ab;

    .line 487310
    move-object v0, v3

    .line 487311
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 487312
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 487313
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->m:LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const-class v2, Lcom/facebook/maps/FbStaticMapView;

    const-string v3, "map_view"

    invoke-static {v2, v3, p3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 487314
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 487315
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 487316
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getRadiusBottomLeft()I
    .locals 1

    .prologue
    .line 487317
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->s:I

    return v0
.end method

.method public getRadiusBottomRight()I
    .locals 1

    .prologue
    .line 487318
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->r:I

    return v0
.end method

.method public getRadiusTopLeft()I
    .locals 1

    .prologue
    .line 487319
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->p:I

    return v0
.end method

.method public getRadiusTopRight()I
    .locals 1

    .prologue
    .line 487320
    iget v0, p0, Lcom/facebook/maps/FbStaticMapView;->q:I

    return v0
.end method

.method public getReportButtonText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 487321
    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 487322
    invoke-direct {p0}, Lcom/facebook/maps/FbStaticMapView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 487323
    invoke-super {p0, p1}, LX/3BP;->onDraw(Landroid/graphics/Canvas;)V

    .line 487324
    :goto_0
    return-void

    .line 487325
    :cond_0
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->n:LX/1FZ;

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/maps/FbStaticMapView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 487326
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-super {p0, v1}, LX/3BP;->onDraw(Landroid/graphics/Canvas;)V

    .line 487327
    invoke-direct {p0, v0}, Lcom/facebook/maps/FbStaticMapView;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 487328
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setMapReporterLauncher(LX/3BT;)V
    .locals 0

    .prologue
    .line 487329
    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/facebook/maps/FbStaticMapView;->l:LX/3BS;

    :cond_0
    invoke-super {p0, p1}, LX/3BP;->setMapReporterLauncher(LX/3BT;)V

    .line 487330
    return-void
.end method

.method public setZeroRatingEnabled(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 487331
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->f:LX/0yY;

    if-nez v0, :cond_2

    .line 487332
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/maps/FbStaticMapView;->setEnabled(Z)V

    .line 487333
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 487334
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 487335
    :cond_1
    :goto_0
    return-void

    .line 487336
    :cond_2
    invoke-virtual {p0, v3}, Lcom/facebook/maps/FbStaticMapView;->setEnabled(Z)V

    .line 487337
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_3

    .line 487338
    invoke-direct {p0}, Lcom/facebook/maps/FbStaticMapView;->d()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 487339
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/6Zp;

    invoke-direct {v1, p0}, LX/6Zp;-><init>(Lcom/facebook/maps/FbStaticMapView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487340
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0, v2, v2}, Lcom/facebook/maps/FbStaticMapView;->addView(Landroid/view/View;II)V

    .line 487341
    :cond_3
    iget-object v0, p0, Lcom/facebook/maps/FbStaticMapView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
