.class public Lcom/facebook/tablet/sideshow/SideshowHost;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/1jv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/1jv",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/FpX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/257;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public e:Landroid/widget/ScrollView;

.field public f:Landroid/widget/LinearLayout;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FpY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367951
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 367952
    return-void
.end method

.method public static a(Lcom/facebook/tablet/sideshow/SideshowHost;LX/FpY;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 367956
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 367957
    const v1, 0x7f031326

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 367958
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v2, 0x1

    .line 367959
    iget p0, p1, LX/FpY;->d:I

    if-eqz p0, :cond_1

    .line 367960
    iget-object p0, p1, LX/FpY;->c:Landroid/view/View;

    if-eqz p0, :cond_0

    :goto_0
    const-string p0, "View should have already been created."

    invoke-static {v2, p0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 367961
    iget-object v2, p1, LX/FpY;->c:Landroid/view/View;

    .line 367962
    :goto_1
    move-object v0, v2

    .line 367963
    return-object v0

    .line 367964
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 367965
    :cond_1
    iput v2, p1, LX/FpY;->d:I

    .line 367966
    iget-object v2, p1, LX/FpY;->a:LX/FpW;

    invoke-interface {v2}, LX/FpW;->a()Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, LX/FpY;->c:Landroid/view/View;

    .line 367967
    invoke-virtual {p1}, LX/FpY;->a()V

    .line 367968
    iget-object v2, p1, LX/FpY;->c:Landroid/view/View;

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/0k9;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0k9",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367953
    packed-switch p1, :pswitch_data_0

    .line 367954
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 367955
    :pswitch_0
    new-instance v0, LX/259;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->a:LX/0TD;

    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, LX/259;-><init>(Landroid/content/Context;LX/0TD;Ljava/util/List;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 367950
    return-void
.end method

.method public final a(LX/0k9;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 367969
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 367970
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpY;

    .line 367971
    iget-object v3, v0, LX/FpY;->b:LX/FpZ;

    move-object v3, v3

    .line 367972
    sget-object v4, LX/FpZ;->ERROR:LX/FpZ;

    if-eq v3, v4, :cond_0

    .line 367973
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367974
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->c:LX/257;

    invoke-virtual {v0}, LX/FpY;->e()Ljava/lang/String;

    move-result-object v4

    .line 367975
    iget-wide v8, v0, LX/FpY;->f:J

    move-wide v6, v8

    .line 367976
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "sideshow_load_success"

    invoke-direct {v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "sideshow"

    .line 367977
    iput-object v5, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 367978
    move-object v0, v0

    .line 367979
    const-string v5, "sideshow_name"

    invoke-virtual {v0, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v5, "loading_time"

    invoke-virtual {v0, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 367980
    iget-object v5, v3, LX/257;->a:LX/0Zb;

    invoke-interface {v5, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 367981
    goto :goto_0

    .line 367982
    :cond_0
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->c:LX/257;

    invoke-virtual {v0}, LX/FpY;->e()Ljava/lang/String;

    move-result-object v0

    .line 367983
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "sideshow_load_fail"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "sideshow"

    .line 367984
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 367985
    move-object v4, v4

    .line 367986
    const-string v5, "sideshow_name"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 367987
    iget-object v5, v3, LX/257;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 367988
    goto :goto_0

    .line 367989
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 367990
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 367991
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 367992
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpY;

    .line 367993
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->f:Landroid/widget/LinearLayout;

    invoke-static {p0, v0, v3}, Lcom/facebook/tablet/sideshow/SideshowHost;->a(Lcom/facebook/tablet/sideshow/SideshowHost;LX/FpY;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 367994
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 367995
    :cond_2
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 367947
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 367948
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/tablet/sideshow/SideshowHost;

    invoke-static {v3}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    new-instance p1, LX/0U8;

    invoke-interface {v3}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    new-instance v1, LX/256;

    invoke-direct {v1, v3}, LX/256;-><init>(LX/0QB;)V

    invoke-direct {p1, v0, v1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object p1, p1

    invoke-static {v3}, LX/257;->a(LX/0QB;)LX/257;

    move-result-object v3

    check-cast v3, LX/257;

    iput-object v2, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->a:LX/0TD;

    iput-object p1, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->b:Ljava/util/Set;

    iput-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->c:LX/257;

    .line 367949
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v2, -0x33233555

    invoke-static {v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 367905
    const v0, 0x7f031324

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 367906
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v3, 0x7f0d2c6d

    invoke-virtual {v0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    .line 367907
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    const v3, 0x7f0d2c6e

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->f:Landroid/widget/LinearLayout;

    .line 367908
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, LX/0fE;

    if-eqz v0, :cond_2

    .line 367909
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/0fE;

    invoke-interface {v0}, LX/0fE;->w()LX/0cQ;

    move-result-object v0

    .line 367910
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 367911
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->b:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 367912
    new-instance v3, LX/258;

    invoke-direct {v3, p0, v0}, LX/258;-><init>(Lcom/facebook/tablet/sideshow/SideshowHost;LX/0cQ;)V

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 367913
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 367914
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FpX;

    .line 367915
    invoke-interface {v1}, LX/FpX;->b()I

    move-result p2

    if-eqz p2, :cond_0

    .line 367916
    new-instance p2, LX/FpV;

    invoke-direct {p2, p0}, LX/FpV;-><init>(Lcom/facebook/tablet/sideshow/SideshowHost;)V

    .line 367917
    invoke-interface {v1}, LX/FpX;->a()LX/FpW;

    move-result-object v1

    .line 367918
    new-instance p3, LX/FpY;

    invoke-direct {p3, v1}, LX/FpY;-><init>(LX/FpW;)V

    .line 367919
    iput-object p3, p2, LX/FpV;->a:LX/FpY;

    .line 367920
    invoke-interface {v3, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 367921
    :cond_1
    move-object v0, v3

    .line 367922
    iput-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    .line 367923
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    .line 367924
    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, p0}, LX/0k5;->a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    .line 367925
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 367926
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x2b

    const v3, 0x4049a044

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4eeddfcc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 367941
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 367942
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpY;

    .line 367943
    iget v3, v0, LX/FpY;->d:I

    const/4 p0, 0x2

    if-ne v3, p0, :cond_0

    .line 367944
    const/4 v3, 0x3

    iput v3, v0, LX/FpY;->d:I

    .line 367945
    :cond_0
    goto :goto_0

    .line 367946
    :cond_1
    const v0, -0x183f43c1

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x297e110

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 367927
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 367928
    const/4 v4, 0x0

    .line 367929
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, LX/25B;

    if-eqz v0, :cond_3

    .line 367930
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/25B;

    .line 367931
    invoke-interface {v0}, LX/25B;->a()I

    move-result v2

    .line 367932
    invoke-interface {v0}, LX/25B;->b()I

    move-result v0

    .line 367933
    if-gtz v2, :cond_0

    if-lez v0, :cond_1

    .line 367934
    :cond_0
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->setClipToPadding(Z)V

    .line 367935
    iget-object v3, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    iget-object v4, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingLeft()I

    move-result v4

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getPaddingRight()I

    move-result v2

    iget-object v5, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v5}, Landroid/widget/ScrollView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v3, v4, v0, v2, v5}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 367936
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpY;

    .line 367937
    invoke-virtual {v0}, LX/FpY;->a()V

    goto :goto_1

    .line 367938
    :cond_2
    const v0, 0x7de5f29d

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 367939
    :cond_3
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setClipToPadding(Z)V

    .line 367940
    iget-object v0, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1dbf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/tablet/sideshow/SideshowHost;->e:Landroid/widget/ScrollView;

    invoke-virtual {v5}, Landroid/widget/ScrollView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/ScrollView;->setPadding(IIII)V

    goto :goto_0
.end method
