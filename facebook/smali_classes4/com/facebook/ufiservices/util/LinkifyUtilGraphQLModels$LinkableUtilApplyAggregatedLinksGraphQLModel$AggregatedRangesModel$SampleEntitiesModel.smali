.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/36L;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12368a4d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 490148
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 490149
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 490150
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 490151
    return-void
.end method

.method private o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490152
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 490153
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490154
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->j:Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->j:Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 490155
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->j:Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    return-object v0
.end method

.method private q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490156
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 490157
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 490158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 490159
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 490160
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 490161
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 490162
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 490163
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 490164
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 490165
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 490166
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 490167
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 490168
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 490169
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 490170
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 490171
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 490172
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 490173
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 490174
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->k:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 490175
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 490176
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 490177
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 490178
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 490179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 490180
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 490181
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 490182
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 490183
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;

    .line 490184
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 490185
    :cond_0
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 490186
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 490187
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 490188
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;

    .line 490189
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->j:Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 490190
    :cond_1
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 490191
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 490192
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 490193
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;

    .line 490194
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 490195
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 490196
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 490197
    new-instance v0, LX/8sQ;

    invoke-direct {v0, p1}, LX/8sQ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490198
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 490199
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 490200
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->k:I

    .line 490201
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 490202
    invoke-virtual {p2}, LX/18L;->a()V

    .line 490203
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 490144
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 490145
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;-><init>()V

    .line 490146
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 490147
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490128
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->o()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490125
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 490126
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 490127
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic cp_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490124
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490129
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 490130
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 490131
    const v0, -0x74aebb6b

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490132
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    .line 490133
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 490134
    const v0, 0x7c02d003

    return v0
.end method

.method public final synthetic j()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490135
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 490136
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 490137
    iget v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->k:I

    return v0
.end method

.method public final synthetic l()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490138
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->q()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490139
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->m:Ljava/lang/String;

    .line 490140
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic n()LX/8qg;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490141
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->p()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490142
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->i:Ljava/lang/String;

    .line 490143
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->i:Ljava/lang/String;

    return-object v0
.end method
