.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;
.super LX/2fD;
.source ""


# instance fields
.field public a:LX/0aU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 529388
    invoke-direct {p0}, LX/2fD;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JJZ)V
    .locals 7

    .prologue
    .line 529389
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v1

    new-instance v0, LX/2E9;

    invoke-direct {v0}, LX/2E9;-><init>()V

    const-class v2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;

    invoke-virtual {v0, v2}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2E9;->a(JJ)LX/2E9;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, LX/2EA;->e:Z

    move-object v0, v0

    .line 529390
    const/4 v2, 0x0

    iput v2, v0, LX/2EA;->a:I

    move-object v2, v0

    .line 529391
    if-eqz p5, :cond_0

    const-string v0, "BGLR-batch"

    :goto_0
    iput-object v0, v2, LX/2EA;->c:Ljava/lang/String;

    move-object v0, v2

    .line 529392
    invoke-virtual {v0}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 529393
    return-void

    .line 529394
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "BGLR-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;LX/0aU;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 529387
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->a:LX/0aU;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;

    invoke-static {v1}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v0

    check-cast v0, LX/0aU;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;LX/0aU;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 4

    .prologue
    .line 529383
    invoke-virtual {p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 529384
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->a:LX/0aU;

    iget-object v3, p1, LX/2fH;->a:Ljava/lang/String;

    move-object v3, v3

    .line 529385
    invoke-static {v0, v2, v3}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Landroid/content/Context;LX/0aU;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 529386
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x274c76db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 529380
    invoke-super {p0}, LX/2fD;->onCreate()V

    .line 529381
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 529382
    const/16 v1, 0x25

    const v2, 0x7cf11d1b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x200b195b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 529373
    if-nez p1, :cond_0

    .line 529374
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, -0x73b07734

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529375
    :catch_0
    move-exception v1

    .line 529376
    const-string v3, "background_location_reporting_gcm_upload"

    const-string v4, "Unexpected service start parameters"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529377
    invoke-virtual {p0, p3}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->stopSelf(I)V

    .line 529378
    const v1, -0x607d005e

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 529379
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    const v1, -0x527b07d0

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
