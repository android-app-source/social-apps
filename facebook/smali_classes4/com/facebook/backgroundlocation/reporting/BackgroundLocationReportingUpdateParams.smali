.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529842
    new-instance v0, LX/HlR;

    invoke-direct {v0}, LX/HlR;-><init>()V

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 529843
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 529844
    invoke-direct {p0, p1, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;-><init>(LX/0Px;LX/0Px;)V

    .line 529845
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 529846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529847
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    .line 529848
    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    .line 529849
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 529850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529851
    const-class v0, Lcom/facebook/location/LocationSignalDataPackage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    .line 529852
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    .line 529853
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 529854
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 529855
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 529856
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 529857
    return-void
.end method
