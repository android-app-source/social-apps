.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Cx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Ct;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2GC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hlk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/3C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Hc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/3C6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0aU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3C7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:J

.field private r:LX/1ql;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529395
    const-class v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 529574
    const-string v0, "bglr-gcm-scheduler"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 529575
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->q:J

    .line 529576
    return-void
.end method

.method private a()J
    .locals 6

    .prologue
    .line 529569
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v2, LX/0X5;->au:J

    invoke-interface {v0, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v0

    .line 529570
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 529571
    iget-wide v4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->q:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 529572
    sub-long/2addr v0, v2

    .line 529573
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;LX/0aU;Lcom/facebook/location/ImmutableLocation;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 529568
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE_FROM_LOCATION_PROVIDER"

    invoke-virtual {p1, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fb_location"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/0aU;Lcom/facebook/location/LocationSignalDataPackage;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 529567
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_SCHEDULE_LOCATION_UPLOAD"

    invoke-virtual {p1, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location_signal_data_package"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "deferred"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/0aU;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 529566
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_UPLOAD_LOCATION"

    invoke-virtual {p1, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "task_tag"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;LX/0Uo;LX/2Cx;LX/2Ct;LX/2GC;LX/1r1;LX/0Ot;LX/3C2;LX/0Ot;LX/3C6;LX/0aU;LX/11H;LX/03V;LX/0SG;LX/3C7;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;",
            "LX/0Uo;",
            "LX/2Cx;",
            "LX/2Ct;",
            "LX/2GC;",
            "LX/1r1;",
            "LX/0Ot",
            "<",
            "LX/Hlk;",
            ">;",
            "LX/3C2;",
            "LX/0Ot",
            "<",
            "LX/2Hc;",
            ">;",
            "LX/3C6;",
            "LX/0aU;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/3C7;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 529565
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a:LX/0Uo;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->c:LX/2Ct;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->d:LX/2GC;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->e:LX/1r1;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->g:LX/3C2;

    iput-object p8, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->i:LX/3C6;

    iput-object p10, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->j:LX/0aU;

    iput-object p11, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->k:LX/11H;

    iput-object p12, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->l:LX/03V;

    iput-object p13, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->m:LX/0SG;

    iput-object p14, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->n:LX/3C7;

    iput-object p15, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    return-void
.end method

.method private a(Lcom/facebook/location/LocationSignalDataPackage;Z)V
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 529521
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 529522
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 529523
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 529524
    sget-object v0, LX/3C4;->b:LX/0U1;

    .line 529525
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 529526
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529527
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 529528
    sget-object v0, LX/3C4;->c:LX/0U1;

    .line 529529
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v1

    .line 529530
    iget-object v0, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 529531
    sget-object v0, LX/3C4;->d:LX/0U1;

    .line 529532
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 529533
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 529534
    :try_start_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->g:LX/3C2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 529535
    sget-object v1, LX/3C4;->a:Ljava/lang/String;

    const/4 v3, 0x0

    const v4, 0x55406788

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0xc57bbe0

    invoke-static {v1}, LX/03h;->a(I)V

    .line 529536
    if-eqz p2, :cond_0

    .line 529537
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aa:J

    const/4 v4, -0x1

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 529538
    if-eq v1, v5, :cond_0

    .line 529539
    :try_start_2
    sget-object v2, LX/3C4;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/3C4;->d:LX/0U1;

    .line 529540
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 529541
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 529542
    int-to-long v4, v1

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 529543
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/3C4;->c:LX/0U1;

    .line 529544
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 529545
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LX/3C4;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LX/3C4;->d:LX/0U1;

    .line 529546
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 529547
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=1 ORDER BY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LX/3C4;->c:LX/0U1;

    .line 529548
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 529549
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC LIMIT ?,1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 529550
    sget-object v1, LX/3C4;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/3C4;->c:LX/0U1;

    .line 529551
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 529552
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " <= ? AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LX/3C4;->d:LX/0U1;

    .line 529553
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 529554
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v0, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1

    .line 529555
    :cond_0
    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v4, LX/0X5;->al:J

    invoke-interface {v0, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v6, LX/0X5;->as:J

    invoke-interface {v0, v6, v7}, LX/0W4;->a(J)Z

    move-result v6

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadService;->a(Landroid/content/Context;JJZ)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1

    .line 529556
    :goto_2
    return-void

    .line 529557
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 529558
    :cond_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    goto/16 :goto_0

    .line 529559
    :catch_0
    move-exception v0

    .line 529560
    :try_start_4
    const-string v1, "bglr-gcm-scheduler"

    const-string v2, "Couldn\'t clean up excess deferred location updates"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 529561
    :catch_1
    move-exception v0

    .line 529562
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    .line 529563
    iget-object v2, v1, LX/2Cx;->b:LX/0Zb;

    const-string v3, "background_location_saving_location_to_storage_fail"

    invoke-static {v3}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v1, v3}, LX/2Cx;->a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "exception"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 529564
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Hc;

    invoke-virtual {v0, p1}, LX/2Hc;->a(Lcom/facebook/location/LocationSignalDataPackage;)V

    goto :goto_2
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;

    invoke-static {v15}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v1

    check-cast v1, LX/0Uo;

    invoke-static {v15}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v2

    check-cast v2, LX/2Cx;

    invoke-static {v15}, LX/2Ct;->a(LX/0QB;)LX/2Ct;

    move-result-object v3

    check-cast v3, LX/2Ct;

    invoke-static {v15}, LX/2GC;->a(LX/0QB;)LX/2GC;

    move-result-object v4

    check-cast v4, LX/2GC;

    invoke-static {v15}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v5

    check-cast v5, LX/1r1;

    const/16 v6, 0x17e4

    invoke-static {v15, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v15}, LX/3C2;->a(LX/0QB;)LX/3C2;

    move-result-object v7

    check-cast v7, LX/3C2;

    const/16 v8, 0x1a3

    invoke-static {v15, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v15}, LX/3C6;->a(LX/0QB;)LX/3C6;

    move-result-object v9

    check-cast v9, LX/3C6;

    invoke-static {v15}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v10

    check-cast v10, LX/0aU;

    invoke-static {v15}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v11

    check-cast v11, LX/11H;

    invoke-static {v15}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v15}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-static {v15}, LX/3C7;->a(LX/0QB;)LX/3C7;

    move-result-object v14

    check-cast v14, LX/3C7;

    invoke-static {v15}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v15

    check-cast v15, LX/0W3;

    invoke-static/range {v0 .. v15}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;LX/0Uo;LX/2Cx;LX/2Ct;LX/2GC;LX/1r1;LX/0Ot;LX/3C2;LX/0Ot;LX/3C6;LX/0aU;LX/11H;LX/03V;LX/0SG;LX/3C7;LX/0W3;)V

    return-void
.end method

.method private a(Ljava/lang/String;LX/0Px;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 529505
    new-instance v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, p2, v2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;-><init>(LX/0Px;LX/0Px;)V

    .line 529506
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->size()I

    .line 529507
    :cond_0
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    invoke-virtual {v2, v0}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)V

    .line 529508
    :try_start_0
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->k:LX/11H;

    iget-object v3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->i:LX/3C6;

    sget-object v4, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v0, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 529509
    if-nez v0, :cond_1

    .line 529510
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    const-string v2, "No response from location update is available"

    const-string v3, "bglr-gcm-scheduler"

    invoke-virtual {v0, v5, v2, p1, v3}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 529511
    :goto_0
    return v0

    .line 529512
    :catch_0
    move-exception v0

    .line 529513
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "bglr-gcm-scheduler"

    invoke-virtual {v2, v5, v0, p1, v3}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 529514
    goto :goto_0

    .line 529515
    :cond_1
    iget-boolean v1, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    if-eqz v1, :cond_3

    .line 529516
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    const-string v2, "bglr-gcm-scheduler"

    invoke-virtual {v1, p1, v2}, LX/2Cx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529517
    :goto_1
    iget-boolean v0, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    if-nez v0, :cond_2

    .line 529518
    invoke-direct {p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b()V

    .line 529519
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 529520
    :cond_3
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    const-string v2, "The update did not succeed"

    const-string v3, "bglr-gcm-scheduler"

    invoke-virtual {v1, v0, v2, p1, v3}, LX/2Cx;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 529501
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->j:LX/0aU;

    const-string v2, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529502
    const-string v1, "expected_location_history_setting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 529503
    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 529504
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 529491
    if-nez p1, :cond_1

    .line 529492
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->l:LX/03V;

    const-string v1, "bglr-gcm-scheduler"

    const-string v2, "intent is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529493
    :cond_0
    :goto_0
    return-void

    .line 529494
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 529495
    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE_FROM_LOCATION_PROVIDER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 529496
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 529497
    :cond_2
    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_UPLOAD_LOCATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 529498
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->e(Landroid/content/Intent;)V

    goto :goto_0

    .line 529499
    :cond_3
    const-string v1, "BACKGROUND_LOCATION_REPORTING_ACTION_SCHEDULE_LOCATION_UPLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529500
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->c(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 529485
    const-string v0, "location_signal_data_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/LocationSignalDataPackage;

    .line 529486
    if-nez v0, :cond_0

    .line 529487
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->l:LX/03V;

    const-string v1, "bglr-gcm-scheduler"

    const-string v2, "intent does not contain location signal data package"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529488
    :goto_0
    return-void

    .line 529489
    :cond_0
    const-string v1, "deferred"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 529490
    invoke-direct {p0, v0, v1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Lcom/facebook/location/LocationSignalDataPackage;Z)V

    goto :goto_0
.end method

.method private d(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 529460
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->c:LX/2Ct;

    invoke-virtual {v0}, LX/2Ct;->b()V

    .line 529461
    const-string v0, "fb_location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 529462
    if-nez v0, :cond_0

    .line 529463
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->l:LX/03V;

    const-string v1, "bglr-gcm-scheduler"

    const-string v2, "location should not be null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529464
    :goto_0
    return-void

    .line 529465
    :cond_0
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aC:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    .line 529466
    new-instance v1, LX/2TT;

    invoke-direct {v1}, LX/2TT;-><init>()V

    .line 529467
    iput-object v0, v1, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 529468
    move-object v0, v1

    .line 529469
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->l()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 529470
    iput-object v1, v0, LX/2TT;->b:Ljava/lang/Boolean;

    .line 529471
    move-object v0, v0

    .line 529472
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->d:LX/2GC;

    invoke-virtual {v1}, LX/2GC;->a()Lcom/facebook/wifiscan/WifiScanResult;

    move-result-object v1

    .line 529473
    iput-object v1, v0, LX/2TT;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 529474
    move-object v0, v0

    .line 529475
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->d:LX/2GC;

    invoke-virtual {v1}, LX/2GC;->b()Ljava/util/List;

    move-result-object v1

    .line 529476
    iput-object v1, v0, LX/2TT;->d:Ljava/util/List;

    .line 529477
    move-object v0, v0

    .line 529478
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->n:LX/3C7;

    iget-object v4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->m:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, LX/3C7;->a(J)Ljava/util/List;

    move-result-object v1

    .line 529479
    iput-object v1, v0, LX/2TT;->g:Ljava/util/List;

    .line 529480
    move-object v0, v0

    .line 529481
    invoke-virtual {v0}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v1

    .line 529482
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v2, LX/0X5;->am:J

    invoke-interface {v0, v2, v3, v6}, LX/0W4;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529483
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hlk;

    iget-object v2, v1, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0, v2}, LX/Hlk;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 529484
    :cond_1
    invoke-direct {p0, v1, v6}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Lcom/facebook/location/LocationSignalDataPackage;Z)V

    goto :goto_0
.end method

.method private e(Landroid/content/Intent;)V
    .locals 18

    .prologue
    .line 529409
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->q:J

    .line 529410
    const-string v2, "task_tag"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 529411
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 529412
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 529413
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->g:LX/3C2;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 529414
    const-wide/16 v4, -0x1

    .line 529415
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v6, LX/0X5;->ad:J

    invoke-interface {v3, v6, v7}, LX/0W4;->a(J)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 529416
    sget-object v3, LX/3C4;->a:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, LX/3C4;->d:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, LX/3C4;->c:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/3C4;->d:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = 0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/3C4;->c:LX/0U1;

    invoke-virtual {v10}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " DESC"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 529417
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 529418
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 529419
    :cond_0
    :goto_0
    return-void

    .line 529420
    :catch_0
    move-exception v2

    .line 529421
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    invoke-virtual {v3, v2}, LX/2Cx;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 529422
    :cond_1
    :try_start_2
    sget-object v4, LX/3C4;->c:LX/0U1;

    invoke-virtual {v4, v3}, LX/0U1;->c(Landroid/database/Cursor;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    .line 529423
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move-wide v6, v4

    .line 529424
    :goto_1
    sget-object v3, LX/3C4;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/3C4;->c:LX/0U1;

    invoke-virtual {v8}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " <= ?"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_2
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_3

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v9

    move-object v6, v8

    :goto_3
    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/3C4;->c:LX/0U1;

    invoke-virtual {v10}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ASC"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 529425
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-nez v3, :cond_4

    .line 529426
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 529427
    :catchall_0
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    .line 529428
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    .line 529429
    :cond_4
    :goto_4
    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 529430
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    .line 529431
    :try_start_5
    sget-object v3, LX/3C4;->b:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v3

    .line 529432
    sget-object v6, LX/3C4;->c:LX/0U1;

    invoke-virtual {v6, v4}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 529433
    const/4 v8, 0x0

    array-length v9, v3

    invoke-virtual {v5, v3, v8, v9}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 529434
    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 529435
    const-class v3, Lcom/facebook/location/LocationSignalDataPackage;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/location/LocationSignalDataPackage;

    .line 529436
    invoke-virtual {v12, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 529437
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v13, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 529438
    :try_start_6
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    .line 529439
    :catchall_1
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2

    .line 529440
    :catch_1
    move-exception v3

    .line 529441
    :try_start_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b:LX/2Cx;

    invoke-virtual {v6, v3}, LX/2Cx;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 529442
    :try_start_8
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    goto :goto_4

    :catchall_2
    move-exception v2

    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 529443
    :cond_5
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 529444
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 529445
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 529446
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 529447
    sget-object v3, LX/3C4;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 529448
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v4, LX/0X5;->av:J

    const/16 v8, 0x14

    invoke-interface {v3, v4, v5, v8}, LX/0W4;->a(JI)I

    move-result v8

    .line 529449
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->o:LX/0W3;

    sget-wide v4, LX/0X5;->as:J

    invoke-interface {v3, v4, v5}, LX/0W4;->a(J)Z

    move-result v9

    .line 529450
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    .line 529451
    const/4 v4, 0x0

    move v5, v4

    :goto_5
    if-ge v5, v10, :cond_0

    .line 529452
    add-int v3, v5, v8

    invoke-static {v10, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v3

    .line 529453
    :goto_6
    if-ge v4, v10, :cond_7

    add-int/lit8 v3, v4, -0x1

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 529454
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 529455
    :cond_7
    if-eqz v9, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_7
    invoke-virtual {v6, v5, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Ljava/lang/String;LX/0Px;)Z

    move-result v3

    .line 529456
    if-eqz v3, :cond_0

    .line 529457
    sget-object v5, LX/3C4;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, LX/3C4;->c:LX/0U1;

    invoke-virtual {v12}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, " <= ?"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v3, 0x1

    new-array v13, v3, [Ljava/lang/String;

    const/4 v14, 0x0

    add-int/lit8 v3, v4, -0x1

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v13, v14

    invoke-virtual {v2, v5, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move v5, v4

    .line 529458
    goto/16 :goto_5

    .line 529459
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, "_"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    :cond_9
    move-wide v6, v4

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x4e94c4dd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 529404
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->c()V

    .line 529405
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->b(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529406
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 529407
    const v1, -0x45d229fe

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 529408
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    const v2, 0x3f001e4a

    invoke-static {v2, v0}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x3e35c3cb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 529400
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 529401
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 529402
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->e:LX/1r1;

    const/4 v2, 0x1

    const-string v3, "bglr-gcm-scheduler"

    invoke-virtual {v1, v2, v3}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    .line 529403
    const/16 v1, 0x25

    const v2, 0x3862fb57

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x364aad93

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 529396
    invoke-super {p0}, LX/1ZN;->onDestroy()V

    .line 529397
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->e:LX/1r1;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    invoke-virtual {v1, v2}, LX/1r1;->a(LX/1ql;)V

    .line 529398
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->r:LX/1ql;

    .line 529399
    const/16 v1, 0x25

    const v2, -0x61ce713a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
