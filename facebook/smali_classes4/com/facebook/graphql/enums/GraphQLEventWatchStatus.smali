.class public final enum Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public static final enum DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public static final enum GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public static final enum UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public static final enum WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 368954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const-string v1, "WATCHED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const-string v1, "UNWATCHED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368959
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368940
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 1

    .prologue
    .line 368941
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 368942
    :goto_0
    return-object v0

    .line 368943
    :cond_1
    const-string v0, "WATCHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 368945
    :cond_2
    const-string v0, "UNWATCHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368946
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 368947
    :cond_3
    const-string v0, "GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368948
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 368949
    :cond_4
    const-string v0, "DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 368950
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 368951
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 1

    .prologue
    .line 368952
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 1

    .prologue
    .line 368953
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method
