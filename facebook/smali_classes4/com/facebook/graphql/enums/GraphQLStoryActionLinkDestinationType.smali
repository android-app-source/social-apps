.class public final enum Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum APP_WITH_SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum INTERNAL_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum LINK:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum MESSENGER:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum MESSENGER_EXTENSIONS:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum NOT_CLASSIFIED:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365598
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365599
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "APP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365600
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "APP_WITH_PRODUCT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365601
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "APP_WITH_SURVEY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365602
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "LINK"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->LINK:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365603
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "NOT_CLASSIFIED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->NOT_CLASSIFIED:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365604
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "INTERNAL_FLOW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->INTERNAL_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365605
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "MESSENGER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365606
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const-string v1, "MESSENGER_EXTENSIONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER_EXTENSIONS:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365607
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->LINK:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->NOT_CLASSIFIED:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->INTERNAL_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER_EXTENSIONS:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365608
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 1

    .prologue
    .line 365609
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 365610
    :goto_0
    return-object v0

    .line 365611
    :cond_1
    const-string v0, "APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365612
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365613
    :cond_2
    const-string v0, "LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365614
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->LINK:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365615
    :cond_3
    const-string v0, "APP_WITH_PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365616
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365617
    :cond_4
    const-string v0, "APP_WITH_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365618
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_SURVEY:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365619
    :cond_5
    const-string v0, "NOT_CLASSIFIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365620
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->NOT_CLASSIFIED:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365621
    :cond_6
    const-string v0, "INTERNAL_FLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 365622
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->INTERNAL_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365623
    :cond_7
    const-string v0, "MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 365624
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365625
    :cond_8
    const-string v0, "MESSENGER_EXTENSIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 365626
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER_EXTENSIONS:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0

    .line 365627
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 1

    .prologue
    .line 365628
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 1

    .prologue
    .line 365629
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-object v0
.end method
