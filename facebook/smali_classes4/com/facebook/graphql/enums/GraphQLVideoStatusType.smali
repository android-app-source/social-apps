.class public final enum Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLVideoStatusType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum ENCODE_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum OK:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum SCRUB_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum SOFT_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum THUMBNAIL_INVALID:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum TOO_LONG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UNDISCOVERABLE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UNPUBLISHED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public static final enum UPLOAD_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 466154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "ENCODING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "DELETED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "ENCODED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "SCRUB_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SCRUB_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UPLOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "OK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->OK:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "TOO_LONG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->TOO_LONG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "ENCODE_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODE_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UNPUBLISHED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNPUBLISHED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "SOFT_DELETED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SOFT_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UNDISCOVERABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNDISCOVERABLE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "THUMBNAIL_INVALID"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->THUMBNAIL_INVALID:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UPLOAD_FAILED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOAD_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const-string v1, "UPLOADED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466169
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SCRUB_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->OK:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->TOO_LONG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODE_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNPUBLISHED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SOFT_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNDISCOVERABLE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->THUMBNAIL_INVALID:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOAD_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 466153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 1

    .prologue
    .line 466122
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 466123
    :goto_0
    return-object v0

    .line 466124
    :cond_1
    const-string v0, "ENCODING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466126
    :cond_2
    const-string v0, "DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 466127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466128
    :cond_3
    const-string v0, "ENCODED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 466129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466130
    :cond_4
    const-string v0, "SCRUB_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 466131
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SCRUB_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466132
    :cond_5
    const-string v0, "UPLOADING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 466133
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466134
    :cond_6
    const-string v0, "OK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 466135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->OK:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466136
    :cond_7
    const-string v0, "TOO_LONG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 466137
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->TOO_LONG:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466138
    :cond_8
    const-string v0, "ENCODE_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 466139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODE_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466140
    :cond_9
    const-string v0, "UNPUBLISHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 466141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNPUBLISHED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466142
    :cond_a
    const-string v0, "SOFT_DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 466143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->SOFT_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466144
    :cond_b
    const-string v0, "UNDISCOVERABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 466145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNDISCOVERABLE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto :goto_0

    .line 466146
    :cond_c
    const-string v0, "THUMBNAIL_INVALID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 466147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->THUMBNAIL_INVALID:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto/16 :goto_0

    .line 466148
    :cond_d
    const-string v0, "UPLOAD_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 466149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOAD_FAILED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto/16 :goto_0

    .line 466150
    :cond_e
    const-string v0, "UPLOADED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 466151
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto/16 :goto_0

    .line 466152
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 1

    .prologue
    .line 466120
    const-class v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 1

    .prologue
    .line 466121
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method
