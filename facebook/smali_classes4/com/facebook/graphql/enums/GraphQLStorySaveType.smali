.class public final enum Lcom/facebook/graphql/enums/GraphQLStorySaveType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStorySaveType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum BOOK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum GENERIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum JOB:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum LINK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum LIST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum MOVIE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum MUSIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PLACE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum POST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PRODUCT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum PROFILE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum TV_SHOW:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum UNKONWN:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 498973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "UNKONWN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNKONWN:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "LINK"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LINK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PLACE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "MUSIC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "BOOK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->BOOK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "MOVIE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MOVIE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "TV_SHOW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "EVENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->EVENT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "POST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->POST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PHOTO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PHOTOS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PRODUCT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "PROFILE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "MESSAGE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "LIST"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LIST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "FUNDRAISER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "GENERIC"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    const-string v1, "JOB"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySaveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->JOB:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498994
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNKONWN:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LINK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PLACE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->BOOK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MOVIE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->EVENT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->POST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LIST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->JOB:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 498997
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySaveType;
    .locals 1

    .prologue
    .line 498998
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 498999
    :goto_0
    return-object v0

    .line 499000
    :cond_1
    const-string v0, "UNKONWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 499001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNKONWN:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499002
    :cond_2
    const-string v0, "LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 499003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LINK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499004
    :cond_3
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 499005
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499006
    :cond_4
    const-string v0, "PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 499007
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PLACE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499008
    :cond_5
    const-string v0, "PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 499009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499010
    :cond_6
    const-string v0, "MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 499011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499012
    :cond_7
    const-string v0, "BOOK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 499013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->BOOK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499014
    :cond_8
    const-string v0, "MOVIE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 499015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MOVIE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499016
    :cond_9
    const-string v0, "TV_SHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 499017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->TV_SHOW:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499018
    :cond_a
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 499019
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->EVENT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499020
    :cond_b
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 499021
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->POST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto :goto_0

    .line 499022
    :cond_c
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 499023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499024
    :cond_d
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 499025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499026
    :cond_e
    const-string v0, "PRODUCT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 499027
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499028
    :cond_f
    const-string v0, "PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 499029
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499030
    :cond_10
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 499031
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499032
    :cond_11
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 499033
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LIST:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499034
    :cond_12
    const-string v0, "FUNDRAISER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 499035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499036
    :cond_13
    const-string v0, "JOB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 499037
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->JOB:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499038
    :cond_14
    const-string v0, "GENERIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 499039
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0

    .line 499040
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySaveType;
    .locals 1

    .prologue
    .line 498996
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStorySaveType;
    .locals 1

    .prologue
    .line 498995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    return-object v0
.end method
