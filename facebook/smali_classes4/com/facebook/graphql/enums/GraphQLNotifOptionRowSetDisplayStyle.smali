.class public final enum Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum ACTION_SHEET_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum CHEVRON_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum FLYOUT_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum H_SCROLL:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum MENU_SECTION_WITH_INDEPENDENT_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum MENU_SECTION_WITH_REMOVABLE_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum MULTI_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum RICH_NOTIF_ACTIONS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum SETTING_PAGE_SECTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum SWIPE_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum TOGGLE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 540237
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540238
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "SWIPE_MENU"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SWIPE_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540239
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "LONGPRESS_MENU"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540240
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "FLYOUT_MENU"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->FLYOUT_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540241
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "CHEVRON_MENU"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->CHEVRON_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540242
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "RICH_NOTIF_ACTIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->RICH_NOTIF_ACTIONS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540243
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "ACTION_SHEET_MENU"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->ACTION_SHEET_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540244
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "SETTING_PAGE_SECTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SETTING_PAGE_SECTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540245
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "MENU_SECTION_WITH_INDEPENDENT_ROWS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_INDEPENDENT_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540246
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "SINGLE_SELECTOR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540247
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "MULTI_SELECTOR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MULTI_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540248
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "H_SCROLL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540249
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "TOGGLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->TOGGLE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540250
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const-string v1, "MENU_SECTION_WITH_REMOVABLE_ROWS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_REMOVABLE_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540251
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SWIPE_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->FLYOUT_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->CHEVRON_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->RICH_NOTIF_ACTIONS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->ACTION_SHEET_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SETTING_PAGE_SECTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_INDEPENDENT_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MULTI_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->TOGGLE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_REMOVABLE_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 540252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
    .locals 1

    .prologue
    .line 540253
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 540254
    :goto_0
    return-object v0

    .line 540255
    :cond_1
    const-string v0, "SWIPE_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 540256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SWIPE_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540257
    :cond_2
    const-string v0, "LONGPRESS_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 540258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540259
    :cond_3
    const-string v0, "FLYOUT_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 540260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->FLYOUT_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540261
    :cond_4
    const-string v0, "CHEVRON_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 540262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->CHEVRON_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540263
    :cond_5
    const-string v0, "RICH_NOTIF_ACTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 540264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->RICH_NOTIF_ACTIONS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540265
    :cond_6
    const-string v0, "ACTION_SHEET_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 540266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->ACTION_SHEET_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540267
    :cond_7
    const-string v0, "SETTING_PAGE_SECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 540268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SETTING_PAGE_SECTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540269
    :cond_8
    const-string v0, "MENU_SECTION_WITH_INDEPENDENT_ROWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 540270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_INDEPENDENT_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540271
    :cond_9
    const-string v0, "SINGLE_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 540272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540273
    :cond_a
    const-string v0, "MULTI_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 540274
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MULTI_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540275
    :cond_b
    const-string v0, "H_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 540276
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->H_SCROLL:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto :goto_0

    .line 540277
    :cond_c
    const-string v0, "TOGGLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 540278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->TOGGLE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto/16 :goto_0

    .line 540279
    :cond_d
    const-string v0, "MENU_SECTION_WITH_REMOVABLE_ROWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 540280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_REMOVABLE_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto/16 :goto_0

    .line 540281
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
    .locals 1

    .prologue
    .line 540282
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
    .locals 1

    .prologue
    .line 540283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    return-object v0
.end method
