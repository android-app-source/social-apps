.class public final enum Lcom/facebook/graphql/enums/ChainingSectionViewState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/ChainingSectionViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/ChainingSectionViewState;

.field public static final enum EMPTY:Lcom/facebook/graphql/enums/ChainingSectionViewState;

.field public static final enum FULL:Lcom/facebook/graphql/enums/ChainingSectionViewState;

.field public static final enum START_ANIMATE:Lcom/facebook/graphql/enums/ChainingSectionViewState;

.field public static final enum START_HIDING:Lcom/facebook/graphql/enums/ChainingSectionViewState;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 497032
    new-instance v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    const-string v1, "START_ANIMATE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/ChainingSectionViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_ANIMATE:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 497033
    new-instance v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/ChainingSectionViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->FULL:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 497034
    new-instance v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    const-string v1, "START_HIDING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/ChainingSectionViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_HIDING:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 497035
    new-instance v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/ChainingSectionViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->EMPTY:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 497036
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/ChainingSectionViewState;

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_ANIMATE:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->FULL:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_HIDING:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->EMPTY:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->$VALUES:[Lcom/facebook/graphql/enums/ChainingSectionViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 497031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/ChainingSectionViewState;
    .locals 1

    .prologue
    .line 497029
    const-class v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/ChainingSectionViewState;
    .locals 1

    .prologue
    .line 497030
    sget-object v0, Lcom/facebook/graphql/enums/ChainingSectionViewState;->$VALUES:[Lcom/facebook/graphql/enums/ChainingSectionViewState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/ChainingSectionViewState;

    return-object v0
.end method
