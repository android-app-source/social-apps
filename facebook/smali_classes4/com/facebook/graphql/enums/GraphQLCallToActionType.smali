.class public final enum Lcom/facebook/graphql/enums/GraphQLCallToActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum APPLY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum BET_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum BOOK_TRAVEL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum BUY:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum BUY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum CALL_ME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum CIVIC_ACTION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum CONTACT_US:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum DONATE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GET_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GET_OFFER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GIVE_FREE_RIDES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum GO_LIVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum INSTALL_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum INSTALL_FREE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum INSTALL_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum LISTEN_MUSIC:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum MESSAGE_USER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum MISSED_CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum MOBILE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum MOMENTS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum OPEN_LINK:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum OPEN_MESSENGER_EXT:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum OPEN_MOVIES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum PLAY_GAME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum RECORD_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum REGISTER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum REQUEST_TIME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SAY_THANKS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SEE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SEE_MENU:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SELL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SEND_INVITES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SIGN_UP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum UNLIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum UPDATE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum USE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum USE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum VIDEO_ANNOTATION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum VISIT_PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum VOTE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum WATCH_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public static final enum WATCH_VIDEO:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365659
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365660
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "OPEN_LINK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_LINK:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365661
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "LIKE_PAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365662
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SHOP_NOW"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365663
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "PLAY_GAME"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->PLAY_GAME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365664
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "INSTALL_APP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365665
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "USE_APP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365666
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "CALL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365667
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "CALL_ME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_ME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365668
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "INSTALL_MOBILE_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365669
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "INSTALL_FREE_MOBILE_APP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_FREE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365670
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "USE_MOBILE_APP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365671
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "MOBILE_DOWNLOAD"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOBILE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365672
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "BOOK_TRAVEL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BOOK_TRAVEL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365673
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "LISTEN_MUSIC"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LISTEN_MUSIC:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365674
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "WATCH_VIDEO"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_VIDEO:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365675
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "LEARN_MORE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365676
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SIGN_UP"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365677
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "DOWNLOAD"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365678
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "WATCH_MORE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365679
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "NO_BUTTON"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365680
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "VISIT_PAGES_FEED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VISIT_PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365681
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "MISSED_CALL"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MISSED_CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365682
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "CALL_NOW"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365683
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "APPLY_NOW"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->APPLY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365684
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "BUY_NOW"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365685
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GET_OFFER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365686
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GET_OFFER_VIEW"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365687
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "BUY_TICKETS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365688
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "UPDATE_APP"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UPDATE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365689
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GET_DIRECTIONS"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365690
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "BUY"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365691
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SEE_DETAILS"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365692
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "MESSAGE_PAGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365693
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "DONATE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365694
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SUBSCRIBE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365695
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SAY_THANKS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAY_THANKS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365696
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SELL_NOW"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SELL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365697
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SHARE"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365698
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "DONATE_NOW"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365699
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GET_QUOTE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365700
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "CONTACT_US"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365701
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "ORDER_NOW"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365702
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "ADD_TO_CART"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365703
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "VIDEO_ANNOTATION"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VIDEO_ANNOTATION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365704
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "MOMENTS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOMENTS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365705
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "RECORD_NOW"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->RECORD_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365706
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "VOTE_NOW"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VOTE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365707
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GIVE_FREE_RIDES"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GIVE_FREE_RIDES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365708
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "OPEN_MESSENGER_EXT"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MESSENGER_EXT:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365709
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "CIVIC_ACTION"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CIVIC_ACTION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365710
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SEND_INVITES"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEND_INVITES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365711
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "UNLIKE_PAGE"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNLIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365712
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "BET_NOW"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BET_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365713
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "OPEN_MOVIES"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MOVIES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365714
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "EVENT_RSVP"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365715
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SAVE"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "REGISTER_NOW"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REGISTER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GO_LIVE"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GO_LIVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "REQUEST_TIME"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REQUEST_TIME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365719
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "GET_MOBILE_APP"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "SEE_MENU"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_MENU:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const-string v1, "MESSAGE_USER"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_USER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365722
    const/16 v0, 0x3f

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_LINK:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->PLAY_GAME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_ME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_FREE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOBILE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BOOK_TRAVEL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LISTEN_MUSIC:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_VIDEO:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VISIT_PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MISSED_CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->APPLY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UPDATE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAY_THANKS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SELL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VIDEO_ANNOTATION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOMENTS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->RECORD_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VOTE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GIVE_FREE_RIDES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MESSENGER_EXT:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CIVIC_ACTION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEND_INVITES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNLIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BET_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MOVIES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REGISTER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GO_LIVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REQUEST_TIME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_MENU:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_USER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365723
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 2

    .prologue
    .line 365724
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 365725
    :goto_0
    return-object v0

    .line 365726
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 365727
    packed-switch v0, :pswitch_data_0

    .line 365728
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365729
    :pswitch_1
    const-string v0, "BOOK_TRAVEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365730
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BOOK_TRAVEL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365731
    :cond_2
    const-string v0, "ORDER_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365732
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365733
    :cond_3
    const-string v0, "MOMENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365734
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOMENTS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365735
    :cond_4
    const-string v0, "CIVIC_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CIVIC_ACTION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365737
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365738
    :pswitch_2
    const-string v0, "VISIT_PAGES_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VISIT_PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365740
    :cond_6
    const-string v0, "GET_DIRECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 365741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365742
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365743
    :pswitch_3
    const-string v0, "GIVE_FREE_RIDES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 365744
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GIVE_FREE_RIDES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365745
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_0

    .line 365746
    :pswitch_4
    const-string v0, "SHOP_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 365747
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365748
    :cond_9
    const-string v0, "INSTALL_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 365749
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365750
    :cond_a
    const-string v0, "SELL_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 365751
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SELL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365752
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365753
    :pswitch_5
    const-string v0, "CALL_ME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 365754
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_ME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365755
    :cond_c
    const-string v0, "GET_MOBILE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 365756
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365757
    :cond_d
    const-string v0, "DONATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 365758
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365759
    :cond_e
    const-string v0, "RECORD_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 365760
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->RECORD_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365761
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365762
    :pswitch_6
    const-string v0, "SEE_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 365763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_MENU:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365764
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365765
    :pswitch_7
    const-string v0, "MESSAGE_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 365766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_USER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365767
    :cond_11
    const-string v0, "VOTE_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 365768
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VOTE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365769
    :cond_12
    const-string v0, "REGISTER_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 365770
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REGISTER_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365771
    :cond_13
    const-string v0, "OPEN_MOVIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 365772
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MOVIES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365773
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365774
    :pswitch_8
    const-string v0, "DOWNLOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 365775
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365776
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365777
    :pswitch_9
    const-string v0, "NO_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 365778
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365779
    :cond_16
    const-string v0, "GO_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 365780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GO_LIVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365781
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365782
    :pswitch_a
    const-string v0, "SIGN_UP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 365783
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365784
    :cond_18
    const-string v0, "SAY_THANKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 365785
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAY_THANKS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365786
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365787
    :pswitch_b
    const-string v0, "INSTALL_MOBILE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 365788
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365789
    :cond_1a
    const-string v0, "SEE_DETAILS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 365790
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365791
    :cond_1b
    const-string v0, "GET_QUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 365792
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365793
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365794
    :pswitch_c
    const-string v0, "USE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 365795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365796
    :cond_1d
    const-string v0, "MISSED_CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 365797
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MISSED_CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365798
    :cond_1e
    const-string v0, "BUY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 365799
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365800
    :cond_1f
    const-string v0, "SEND_INVITES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 365801
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SEND_INVITES:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365802
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365803
    :pswitch_d
    const-string v0, "OPEN_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 365804
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_LINK:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365805
    :cond_21
    const-string v0, "OPEN_MESSENGER_EXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 365806
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->OPEN_MESSENGER_EXT:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365807
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365808
    :pswitch_e
    const-string v0, "UPDATE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 365809
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UPDATE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365810
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365811
    :pswitch_f
    const-string v0, "LIKE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 365812
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365813
    :cond_24
    const-string v0, "INSTALL_FREE_MOBILE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 365814
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->INSTALL_FREE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365815
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365816
    :pswitch_10
    const-string v0, "LEARN_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 365817
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365818
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365819
    :pswitch_11
    const-string v0, "BUY_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 365820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365821
    :cond_27
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 365822
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365823
    :cond_28
    const-string v0, "BET_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 365824
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BET_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365825
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365826
    :pswitch_12
    const-string v0, "USE_MOBILE_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 365827
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->USE_MOBILE_APP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365828
    :cond_2a
    const-string v0, "WATCH_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 365829
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_VIDEO:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365830
    :cond_2b
    const-string v0, "APPLY_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 365831
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->APPLY_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365832
    :cond_2c
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 365833
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365834
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365835
    :pswitch_13
    const-string v0, "PLAY_GAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 365836
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->PLAY_GAME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365837
    :cond_2e
    const-string v0, "CALL_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 365838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365839
    :cond_2f
    const-string v0, "MESSAGE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 365840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365841
    :cond_30
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365842
    :pswitch_14
    const-string v0, "LISTEN_MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 365843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LISTEN_MUSIC:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365844
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365845
    :pswitch_15
    const-string v0, "SUBSCRIBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 365846
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365847
    :cond_32
    const-string v0, "DONATE_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 365848
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365849
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365850
    :pswitch_16
    const-string v0, "MOBILE_DOWNLOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 365851
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MOBILE_DOWNLOAD:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365852
    :cond_34
    const-string v0, "ADD_TO_CART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 365853
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365854
    :cond_35
    const-string v0, "VIDEO_ANNOTATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 365855
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->VIDEO_ANNOTATION:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365856
    :cond_36
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365857
    :pswitch_17
    const-string v0, "REQUEST_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 365858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->REQUEST_TIME:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365859
    :cond_37
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365860
    :pswitch_18
    const-string v0, "BUY_TICKETS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 365861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365862
    :cond_38
    const-string v0, "CONTACT_US"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 365863
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365864
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365865
    :pswitch_19
    const-string v0, "CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 365866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365867
    :cond_3a
    const-string v0, "UNLIKE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 365868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNLIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365869
    :cond_3b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365870
    :pswitch_1a
    const-string v0, "WATCH_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 365871
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->WATCH_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365872
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365873
    :pswitch_1b
    const-string v0, "GET_OFFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 365874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365875
    :cond_3d
    const-string v0, "GET_OFFER_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 365876
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365877
    :cond_3e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365878
    :pswitch_1c
    const-string v0, "EVENT_RSVP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 365879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    .line 365880
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 1

    .prologue
    .line 365881
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 1

    .prologue
    .line 365882
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method
