.class public final enum Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum ALBUM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum APP_INVITE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum APP_REQUEST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum APP_UPGRADE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum BACKSTAGE_MULTI_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum BACKSTAGE_REACTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum BACKSTAGE_SINGLE_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum BADGE_COUNT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum BIRTHDAY_REMINDER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum CHECKIN:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum COLLECTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum DEPRECATED_40:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum EVENT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum GIFT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum LIVE_VIDEO_PAGE_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum LOCAL_NEWS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum MINGLES:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum NEARBY_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum NOTE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum OG_SUGGESTION_LIST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PAGE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PAGE_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PLACE_FEED:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum POKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PRESENCE_LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PULSE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum PUSH_ONLY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum QUESTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum STREAM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum SUPPORT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum USER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum USER_ABOUT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

.field public static final enum VIDEO_LIVE_LOBBY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 566228
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566229
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566230
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "APP_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_REQUEST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566231
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "APP_INVITE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_INVITE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566232
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "BIRTHDAY_REMINDER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BIRTHDAY_REMINDER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566233
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "BADGE_COUNT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BADGE_COUNT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566234
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "CHECKIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566235
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "COLLECTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->COLLECTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566236
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "EVENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566237
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "EVENT_DASHBOARD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566238
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "FRIEND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566239
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "GIFT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GIFT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566240
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "GROUP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566241
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "LOCAL_NEWS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LOCAL_NEWS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566242
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "NEARBY_FRIEND"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NEARBY_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566243
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "NOTE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566244
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "OG_SUGGESTION_LIST"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->OG_SUGGESTION_LIST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566245
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PAGE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566246
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PHOTO"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566247
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "POKE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->POKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566248
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "QUESTION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566249
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PRESENCE_LIKE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PRESENCE_LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566250
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "STREAM"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->STREAM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566251
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "SUPPORT_DASHBOARD"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->SUPPORT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566252
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "USER"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566253
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "USER_ABOUT"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER_ABOUT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566254
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "VIDEO"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566255
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "VIDEO_LIVE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566256
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PLACE_FEED"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PLACE_FEED:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566257
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PAGE_ACTIVITY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566258
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PULSE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PULSE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566259
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "APP_UPGRADE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_UPGRADE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566260
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "CONTACT_IMPORTER"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566261
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "NOTIFICATION"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566262
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "BACKSTAGE_SINGLE_POST"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_SINGLE_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566263
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "BACKSTAGE_MULTI_POST"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_MULTI_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566264
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "BACKSTAGE_REACTION"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_REACTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566265
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "PUSH_ONLY"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PUSH_ONLY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566266
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "VIDEO_LIVE_LOBBY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE_LOBBY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566267
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "MINGLES"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->MINGLES:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566268
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "DEPRECATED_40"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->DEPRECATED_40:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566269
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "LIVE_VIDEO_PAGE_LEVEL_INSIGHTS"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_PAGE_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566270
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    const-string v1, "LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566271
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_REQUEST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_INVITE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BIRTHDAY_REMINDER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BADGE_COUNT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->COLLECTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GIFT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LOCAL_NEWS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NEARBY_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->OG_SUGGESTION_LIST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->POKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PRESENCE_LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->STREAM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->SUPPORT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER_ABOUT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PLACE_FEED:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PULSE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_UPGRADE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_SINGLE_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_MULTI_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_REACTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PUSH_ONLY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE_LOBBY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->MINGLES:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->DEPRECATED_40:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_PAGE_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 566272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;
    .locals 2

    .prologue
    .line 566273
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 566274
    :goto_0
    return-object v0

    .line 566275
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 566276
    packed-switch v0, :pswitch_data_0

    .line 566277
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566278
    :pswitch_1
    const-string v0, "SUPPORT_DASHBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 566279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->SUPPORT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566280
    :cond_2
    const-string v0, "PUSH_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 566281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PUSH_ONLY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566282
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566283
    :pswitch_2
    const-string v0, "BIRTHDAY_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 566284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BIRTHDAY_REMINDER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566285
    :cond_4
    const-string v0, "CONTACT_IMPORTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 566286
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566287
    :cond_5
    const-string v0, "MINGLES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 566288
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->MINGLES:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566289
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566290
    :pswitch_3
    const-string v0, "BACKSTAGE_MULTI_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 566291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_MULTI_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566292
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566293
    :pswitch_4
    const-string v0, "LOCAL_NEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 566294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LOCAL_NEWS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto :goto_0

    .line 566295
    :cond_8
    const-string v0, "BACKSTAGE_SINGLE_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 566296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_SINGLE_POST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566297
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566298
    :pswitch_5
    const-string v0, "PAGE_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 566299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566300
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566301
    :pswitch_6
    const-string v0, "APP_INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 566302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_INVITE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566303
    :cond_b
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 566304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566305
    :cond_c
    const-string v0, "BACKSTAGE_REACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 566306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BACKSTAGE_REACTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566307
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566308
    :pswitch_7
    const-string v0, "USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 566309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566310
    :cond_e
    const-string v0, "APP_UPGRADE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 566311
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_UPGRADE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566312
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566313
    :pswitch_8
    const-string v0, "FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 566314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566315
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566316
    :pswitch_9
    const-string v0, "QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 566317
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566318
    :cond_11
    const-string v0, "USER_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 566319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER_ABOUT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566320
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566321
    :pswitch_a
    const-string v0, "STREAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 566322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->STREAM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566323
    :cond_13
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 566324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566325
    :cond_14
    const-string v0, "NOTIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 566326
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566327
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566328
    :pswitch_b
    const-string v0, "NOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 566329
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566330
    :cond_16
    const-string v0, "OG_SUGGESTION_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 566331
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->OG_SUGGESTION_LIST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566332
    :cond_17
    const-string v0, "VIDEO_LIVE_LOBBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 566333
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE_LOBBY:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566334
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566335
    :pswitch_c
    const-string v0, "PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 566336
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566337
    :cond_19
    const-string v0, "POKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 566338
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->POKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566339
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566340
    :pswitch_d
    const-string v0, "EVENT_DASHBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 566341
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566342
    :cond_1b
    const-string v0, "PULSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 566343
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PULSE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566344
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566345
    :pswitch_e
    const-string v0, "EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 566346
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566347
    :cond_1d
    const-string v0, "PLACE_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 566348
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PLACE_FEED:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566349
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566350
    :pswitch_f
    const-string v0, "GIFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 566351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GIFT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566352
    :cond_1f
    const-string v0, "NEARBY_FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 566353
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NEARBY_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566354
    :cond_20
    const-string v0, "LIVE_VIDEO_PAGE_LEVEL_INSIGHTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 566355
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_PAGE_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566356
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566357
    :pswitch_10
    const-string v0, "APP_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 566358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_REQUEST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566359
    :cond_22
    const-string v0, "PRESENCE_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 566360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PRESENCE_LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566361
    :cond_23
    const-string v0, "LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 566362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->LIVE_VIDEO_VIDEO_LEVEL_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566363
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566364
    :pswitch_11
    const-string v0, "ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 566365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566366
    :cond_25
    const-string v0, "BADGE_COUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 566367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->BADGE_COUNT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566368
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566369
    :pswitch_12
    const-string v0, "VIDEO_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 566370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566371
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566372
    :pswitch_13
    const-string v0, "CHECKIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 566373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566374
    :cond_28
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 566375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566376
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566377
    :pswitch_14
    const-string v0, "COLLECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 566378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->COLLECTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    .line 566379
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_14
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;
    .locals 1

    .prologue
    .line 566380
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;
    .locals 1

    .prologue
    .line 566381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    return-object v0
.end method
