.class public final enum Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum SINGLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum SUGGESTED_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum TEMPORARY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum UNIFIED_MEDIA_GALLERY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365575
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365576
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "SINGLE_OVERLAY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SINGLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365577
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "SUGGESTED_OVERLAYS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SUGGESTED_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365578
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "UNIFIED_MEDIA_GALLERY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNIFIED_MEDIA_GALLERY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365579
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "TEMPORARY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->TEMPORARY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365580
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const-string v1, "BIRTHDAY_WISHES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365581
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SINGLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SUGGESTED_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNIFIED_MEDIA_GALLERY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->TEMPORARY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365582
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .locals 1

    .prologue
    .line 365583
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 365584
    :goto_0
    return-object v0

    .line 365585
    :cond_1
    const-string v0, "UNIFIED_MEDIA_GALLERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365586
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNIFIED_MEDIA_GALLERY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0

    .line 365587
    :cond_2
    const-string v0, "TEMPORARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365588
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->TEMPORARY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0

    .line 365589
    :cond_3
    const-string v0, "SINGLE_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365590
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SINGLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0

    .line 365591
    :cond_4
    const-string v0, "SUGGESTED_OVERLAYS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SUGGESTED_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0

    .line 365593
    :cond_5
    const-string v0, "BIRTHDAY_WISHES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365594
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0

    .line 365595
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .locals 1

    .prologue
    .line 365596
    const-class v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .locals 1

    .prologue
    .line 365597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    return-object v0
.end method
