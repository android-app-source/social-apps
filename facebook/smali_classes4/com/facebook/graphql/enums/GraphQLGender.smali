.class public final enum Lcom/facebook/graphql/enums/GraphQLGender;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGender;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGender;

.field public static final enum FEMALE:Lcom/facebook/graphql/enums/GraphQLGender;

.field public static final enum MALE:Lcom/facebook/graphql/enums/GraphQLGender;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGender;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 492979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGender;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGender;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGender;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGender;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->MALE:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492983
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGender;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLGender;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->MALE:Lcom/facebook/graphql/enums/GraphQLGender;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 492984
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 1

    .prologue
    .line 492970
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492971
    :goto_0
    return-object v0

    .line 492972
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492973
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGender;

    goto :goto_0

    .line 492974
    :cond_2
    const-string v0, "FEMALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 492975
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLGender;

    goto :goto_0

    .line 492976
    :cond_3
    const-string v0, "MALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 492977
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->MALE:Lcom/facebook/graphql/enums/GraphQLGender;

    goto :goto_0

    .line 492978
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 1

    .prologue
    .line 492969
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGender;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 1

    .prologue
    .line 492968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGender;

    return-object v0
.end method
