.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupCategory;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum DECLINE_TO_STATE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum FORSALE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum GOAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum IDENTITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum ORGANIZATION:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum PROFESSIONAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 368817
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368818
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368819
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "COMPANY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368820
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "COLLEGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368821
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368822
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "DECLINE_TO_STATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->DECLINE_TO_STATE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368823
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "FAMILY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368824
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "ORGANIZATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->ORGANIZATION:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368825
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "IDENTITY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->IDENTITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368826
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "PROFESSIONAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->PROFESSIONAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368827
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "INTEREST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368828
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "GOAL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->GOAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368829
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "FORSALE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FORSALE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368830
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "OTHER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->OTHER:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368831
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const-string v1, "INFORMAL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368832
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->DECLINE_TO_STATE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->ORGANIZATION:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->IDENTITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->PROFESSIONAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->GOAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FORSALE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->OTHER:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368833
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 1

    .prologue
    .line 368786
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 368787
    :goto_0
    return-object v0

    .line 368788
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368789
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368790
    :cond_2
    const-string v0, "COMPANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368791
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368792
    :cond_3
    const-string v0, "COLLEGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368793
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368794
    :cond_4
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 368795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368796
    :cond_5
    const-string v0, "INFORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 368797
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368798
    :cond_6
    const-string v0, "DECLINE_TO_STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 368799
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->DECLINE_TO_STATE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368800
    :cond_7
    const-string v0, "FAMILY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 368801
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368802
    :cond_8
    const-string v0, "ORGANIZATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 368803
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->ORGANIZATION:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368804
    :cond_9
    const-string v0, "IDENTITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 368805
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->IDENTITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368806
    :cond_a
    const-string v0, "PROFESSIONAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 368807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->PROFESSIONAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368808
    :cond_b
    const-string v0, "INTEREST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 368809
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto :goto_0

    .line 368810
    :cond_c
    const-string v0, "GOAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 368811
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->GOAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto/16 :goto_0

    .line 368812
    :cond_d
    const-string v0, "FORSALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 368813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->FORSALE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto/16 :goto_0

    .line 368814
    :cond_e
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 368815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->OTHER:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto/16 :goto_0

    .line 368816
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 1

    .prologue
    .line 368785
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 1

    .prologue
    .line 368784
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method
