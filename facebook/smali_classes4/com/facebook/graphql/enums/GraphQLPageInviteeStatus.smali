.class public final enum Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public static final enum INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public static final enum INVITED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public static final enum LIKED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public static final enum NOT_INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 492985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const-string v1, "LIKED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const-string v1, "INVITABLE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const-string v1, "NOT_INVITABLE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->NOT_INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492990
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->NOT_INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 492991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 1

    .prologue
    .line 492992
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492993
    :goto_0
    return-object v0

    .line 492994
    :cond_1
    const-string v0, "INVITABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    goto :goto_0

    .line 492996
    :cond_2
    const-string v0, "INVITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 492997
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    goto :goto_0

    .line 492998
    :cond_3
    const-string v0, "LIKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 492999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    goto :goto_0

    .line 493000
    :cond_4
    const-string v0, "NOT_INVITABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 493001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->NOT_INVITABLE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    goto :goto_0

    .line 493002
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 1

    .prologue
    .line 493003
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 1

    .prologue
    .line 493004
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-object v0
.end method
