.class public final enum Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

.field public static final enum CONTAINED_LINK:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

.field public static final enum CONTAINED_MEDIA:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

.field public static final enum FEED_TOPICS:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

.field public static final enum STICKER:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 366111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366112
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    const-string v1, "FEED_TOPICS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->FEED_TOPICS:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->STICKER:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    const-string v1, "CONTAINED_MEDIA"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_MEDIA:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    const-string v1, "CONTAINED_LINK"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_LINK:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366116
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->FEED_TOPICS:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->STICKER:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_MEDIA:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_LINK:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 366110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;
    .locals 1

    .prologue
    .line 366117
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 366118
    :goto_0
    return-object v0

    .line 366119
    :cond_1
    const-string v0, "FEED_TOPICS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366120
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->FEED_TOPICS:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    goto :goto_0

    .line 366121
    :cond_2
    const-string v0, "STICKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 366122
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->STICKER:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    goto :goto_0

    .line 366123
    :cond_3
    const-string v0, "CONTAINED_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 366124
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_MEDIA:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    goto :goto_0

    .line 366125
    :cond_4
    const-string v0, "CONTAINED_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 366126
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->CONTAINED_LINK:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    goto :goto_0

    .line 366127
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;
    .locals 1

    .prologue
    .line 366108
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;
    .locals 1

    .prologue
    .line 366109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    return-object v0
.end method
