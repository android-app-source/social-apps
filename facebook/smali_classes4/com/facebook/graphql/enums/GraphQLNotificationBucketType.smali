.class public final enum Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum MIXED_STATE_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum SEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum SEE_FIRST_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum UNSEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 532053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "FRIEND_REQUESTS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532056
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "UNSEEN_NOTIFICATIONS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "SEEN_NOTIFICATIONS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "MIXED_STATE_NOTIFICATIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->MIXED_STATE_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    const-string v1, "SEE_FIRST_NOTIFICATIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEE_FIRST_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532060
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->MIXED_STATE_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEE_FIRST_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 532035
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;
    .locals 1

    .prologue
    .line 532038
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 532039
    :goto_0
    return-object v0

    .line 532040
    :cond_1
    const-string v0, "FALLBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 532041
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532042
    :cond_2
    const-string v0, "FRIEND_REQUESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 532043
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532044
    :cond_3
    const-string v0, "UNSEEN_NOTIFICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 532045
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532046
    :cond_4
    const-string v0, "SEEN_NOTIFICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 532047
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEEN_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532048
    :cond_5
    const-string v0, "MIXED_STATE_NOTIFICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 532049
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->MIXED_STATE_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532050
    :cond_6
    const-string v0, "SEE_FIRST_NOTIFICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 532051
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEE_FIRST_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0

    .line 532052
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;
    .locals 1

    .prologue
    .line 532037
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;
    .locals 1

    .prologue
    .line 532036
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    return-object v0
.end method
