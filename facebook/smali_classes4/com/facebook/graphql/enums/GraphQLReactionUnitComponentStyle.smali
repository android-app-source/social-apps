.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_MOVIE_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_NEARBY_FRIEND_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ACORN_UNIT_SETTINGS_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ADINTERFACES_CARD_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ADINTERFACES_OBJECTIVE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ADMIN_FEED_STORY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ARTICLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum A_PLACE_FOR_FOOTER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum A_PLACE_FOR_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum A_PLACE_FOR_STORY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BANNER_HIGHLIGHTABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BOOSTED_COMPONENT_PROMOTION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BOTTOM_BORDER_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BROADCAST_REMINDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BROWSE_BACKED_MEDIA_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum BUTTON_ROUNDED_CORNERS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_SMALL_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CENTERED_TITLE_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CIRCULAR_IMAGE_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CITY_GUIDE_FRIENDS_AT_CITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CITY_GUIDE_INFO_AND_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum COMMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CONNECTED_EVENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_IMAGE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_IMAGE_TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CORE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum COUNTS_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum COVER_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CRISIS_RESPONSE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum DEFERRED_LOAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum DEPRECATED_336:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EPCOT_PASSPORT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITHOUT_DATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITH_DAY_AND_TIME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITH_ETA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITH_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_DETAILS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_GUEST_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EVENT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EXPANDABLE_DRAWER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EXPANDABLE_PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum EXPLORE_FEED_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FIG_ACTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FIG_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FIG_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FOOTER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FORMATTED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FRIEND_REQUEST_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FRIEND_REQUEST_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FRIEND_REQUEST_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FRIEND_REQUEST_STATEFUL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FULL_WIDTH_ACTION_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum FUNDRAISER_AMOUNT_RAISED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_BASEBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_FAN_FAVORITE_FRIENDS_VOTES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_FAN_FAVORITE_WITH_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_FOOTBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_RECENT_PLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_SPORTS_PLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_SPORTS_PLAY_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_TEAM_PAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GAMETIME_TYPED_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GROUP_DESCRIPTION_WITH_JOIN_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum GROUP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HEADER_WITH_TEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HEADER_WITH_VERIFIED_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HEAD_TO_HEAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum HORIZONTAL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_COMPONENTS_LIST_WIDE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_PAGER_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_XOUT_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_MESSAGE_AUTO_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_MESSAGE_SMALL_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_MESSAGE_WITH_CARET:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_OVER_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum IMAGE_PROFILE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum IMAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum IMAGE_WITH_OVERLAY_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum IMAGE_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum INFO_ROW_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum INFO_ROW_WITH_FRIENDSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum INFO_ROW_WITH_RIGHT_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum JOBS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum JOBS_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum JOBS_ITEM_ON_TAB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LABELED_BAR_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LABELED_ICON_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LARGE_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LARGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LARGE_PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_ALIGN_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_ALIGN_FOOTER_LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_PARAGRAPH_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LEFT_RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LOCAL_CONTENT_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum LOCAL_CONTENT_REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum MAP_WITH_BREADCRUMBS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum MARGIN_TOP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum MESSAGE_IMAGE_BLOCK_WITH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NATIVE_TEMPLATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NOTIFICATIONS_PARITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum NOTIFICATION_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum OFFER_ON_PAGES_OFFER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum OPEN_HOURS_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum OPEN_HOURS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum OPEN_HOURS_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_FEATURED_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_INSIGHTS_AYMT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_INSIGHTS_METRIC_WITH_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_INSIGHTS_OVERVIEW_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_INSIGHTS_OVERVIEW_CARD_METRIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_SERVICE_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGES_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ABOUT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ABOUT_INFO_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ABOUT_INFO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ADDRESS_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ADMIN_TIP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CITY_FRIENDS_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTACT_INFO_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXT_ROW_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CONTEXT_ROW_WITH_CHEVRON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_HIGHLIGHTS_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_HIGHLIGHTS_PHONE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_HIGHLIGHTS_WEBSITE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_HOME_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INFO_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INFO_RATINGS_AND_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INFO_ROW_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INFO_WRITE_FIRST_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INLINE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_LOCAL_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_MAP_WITH_DISTANCE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_MAP_WITH_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_NOTE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_NOTIFICATIONS_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_NOTIFICATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_OPEN_HOURS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_ORDER_AHEAD_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_PHOTOS_WITH_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_SEE_ALL_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_SEE_ALL_PHOTO_ALBUMS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_SERVICE_NUX_BODY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_SOCIAL_CONTEXT_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_V_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTOS_HEADER_WITH_SEE_ALL_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_H_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_H_SCROLL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_TILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PHOTO_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PLACE_INFO_BLURB_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PLACE_INFO_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PLACE_WITH_METADATA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PLACE_WITH_METADATA_AND_DISCLOSURE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum POPULAR_HOURS_HISTOGRAM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum POST_COMMENTS_MODERATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum POST_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PROFILE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PROMPT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum PYMK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SEGMENTED_PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_COLOR_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_PARAGRAPH_WITH_INLINE_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SHORT_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SIMPLE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SINGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SINGLE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SINGLE_IMAGE_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SINGLE_IMAGE_SHORT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SINGLE_WIDE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SPACING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum STAGGERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum STATIC_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum STATIC_PYMK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum STORY_BLOCK_WITH_UFI:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SUBSECTION_DARK_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SUBSECTION_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum SUBTITLE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TAB_SWITCHER_LABELS_AS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TAB_SWITCHER_LABELS_ON_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TAB_SWITCHER_LABELS_ON_TOP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TAGS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TEXT_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TEXT_HEADER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TEXT_WITH_INLINE_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TOGGLE_STATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TOP_LEVEL_COMMENT_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TRENDING_TOPIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TWO_PLAYER_DETAILED_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TWO_PLAYER_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum TWO_PLAYER_MATCH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum UNLABELED_LINE_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum UPSELL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_ACTION_WITH_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_COMPONENT_LIST_INNER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VERTICAL_COMPONENT_LIST_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_CIRCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_FEATURED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_RECOMMENDATION_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_CHANNEL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_FEATURED_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_HAIRLINE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum VIDEO_HOME_STICKY_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum WEATHER_CONDITION_ALERT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum WEATHER_CONDITION_NARROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum WEATHER_CONDITION_WIDE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public static final enum WEATHER_FORECAST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 533832
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533833
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_COMPONENTS_LIST"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533834
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_COMPONENTS_LIST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533835
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_GENERIC_COMPONENTS_LIST"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533836
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TAB_SWITCHER_LABELS_ON_BOTTOM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533837
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533838
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533839
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EXPANDABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533840
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_COMPONENT_LIST_INNER_CARD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533841
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533842
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533843
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533844
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_COMPONENTS_LIST"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_V_SCROLL_COMPONENTS_LIST"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_COMPONENTS_LIST"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_COMPONENTS_LIST_WIDE_CARD"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_WIDE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533853
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533854
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_COMPONENT_LIST_W_AUX_ACTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533855
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533856
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533857
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533858
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_TEXT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533859
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_IMAGE_TEXT"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533860
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_IMAGE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533861
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_VIDEO"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533862
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_BUTTON"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533863
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_LIST"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533864
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_MESSAGE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533865
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_OVER_MESSAGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_OVER_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533866
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_MESSAGE_AUTO_ACTION"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_AUTO_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533867
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FOOTER"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_FOOTER"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "UPSELL_FOOTER"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_ACTION_LIST_FOOTER"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_PARAGRAPH"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533872
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_PARAGRAPH_WITH_INLINE_TITLE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH_WITH_INLINE_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533873
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ARTICLE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ARTICLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533874
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HEADER"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533875
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SIMPLE_TEXT_HEADER"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533876
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SIMPLE_TEXT"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533877
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "UPSELL"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533878
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "IMAGE_BLOCK"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533879
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "IMAGE_STORY_BLOCK"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533880
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "COVER_IMAGE"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COVER_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533881
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NOTIFICATIONS_LIST"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533882
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_GRID"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533883
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533884
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_PHOTO"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533885
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_WITH_TEXT_OVERLAY"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533886
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FACEPILE_HORIZONTAL_SCROLL"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533887
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TEXT_WITH_INLINE_FACEPILE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_WITH_INLINE_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533888
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_IMAGES_ROW"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533889
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SINGLE_IMAGE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533890
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TWO_PLAYER_MATCH_STATUS"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533891
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TWO_PLAYER_DETAILED_MATCH"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_DETAILED_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533892
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533893
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SINGLE_BUTTON"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533894
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_HIDE_CONFIRMATION"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533895
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "WEATHER_CONDITION_NARROW"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_NARROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533896
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "WEATHER_CONDITION_WIDE"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_WIDE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533897
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533898
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_MAP"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533899
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "INFO_ROW"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INFO_ROW"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PROMPT_IMAGE_BLOCK"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROMPT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_MOVIE_DESCRIPTION"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_MOVIE_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533903
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_MESSAGE_SMALL_TITLE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_SMALL_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533904
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_HEADER"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533905
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ADDRESS_NAVIGATION"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADDRESS_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533907
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTACT_INFO_STACK"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTACT_INFO_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INFO_DESCRIPTION"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INFO_RATINGS_AND_REVIEWS"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_RATINGS_AND_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BROADCAST_REMINDER"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROADCAST_REMINDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_NUX"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_SERVICE_NUX_BODY"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SERVICE_NUX_BODY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TEXT_DIVIDER"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_SPORTS_PLAY"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533915
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_ROW"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533916
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FIG_HEADER"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533917
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FIG_FOOTER"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_H_SCROLL"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_TITLE_WITH_BREADCRUMBS"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TITLE_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533920
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_UNIT_SETTINGS_ICON_MESSAGE"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_UNIT_SETTINGS_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533921
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_ACTION_LIST"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533922
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PLACE_INFO_BLURB"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "COUNTS_HORIZONTAL"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COUNTS_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533925
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_PARAGRAPH"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533926
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INFO_WRITE_FIRST_REVIEW"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_WRITE_FIRST_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533927
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LARGE_MAP"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533928
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_IMAGE_BLOCK"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PLACE_INFO_IMAGE_BLOCK"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SUBSECTION_TITLE"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NOTIFICATION_IMAGE_BLOCK"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATION_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITH_ETA"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_ETA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "STORY_BLOCK"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_NEARBY_FRIEND_IMAGE_BLOCK"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_NEARBY_FRIEND_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LOCAL_CONTENT_REVIEW"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_GUEST_FACEPILE"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FORMATTED_PARAGRAPH"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FORMATTED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_COLOR_BAR"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_COLOR_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533939
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "STAGGERED_IMAGES_ROW"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STAGGERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533940
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SUBTITLE_MESSAGE"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBTITLE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533941
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "UNLABELED_LINE_CHART"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNLABELED_LINE_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533942
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_INSIGHTS_OVERVIEW_CARD_HEADER"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533943
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_INSIGHTS_OVERVIEW_CARD_METRIC"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_METRIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533944
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BROWSE_BACKED_MEDIA_GRID"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROWSE_BACKED_MEDIA_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533945
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "IMAGE_WITH_OVERLAY_GRID"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_OVERLAY_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533946
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LABELED_ICON_GRID"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_ICON_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PROFILE_IMAGE_BLOCK"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_ALIGN_FOOTER_LIGHT"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER_LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_TABLE"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HEAD_TO_HEAD"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEAD_TO_HEAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TAB_SWITCHER_LABELS_AS_HEADER"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_AS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_OPEN_HOURS"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXT_ROWS_PLACEHOLDER"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_RECENT_PLAYS"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_RECENT_PLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PLACE_WITH_METADATA"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITH_DAY_AND_TIME"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_DAY_AND_TIME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "WEATHER_CONDITION_ALERT"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_ALERT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CONNECTED_EVENTS_LIST"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CONNECTED_EVENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_FULL_WIDTH_COUNTER"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LABELED_BAR_CHART"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_BAR_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TEXT_HEADER"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_RELATED_PAGES_PLACEHOLDER"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "MAP_WITH_BREADCRUMBS_HEADER"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MAP_WITH_BREADCRUMBS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533965
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533966
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533967
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_FAN_FAVORITE"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533968
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_FOOTBALL_LIVE_HEADER"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FOOTBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533969
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "IMAGE_WITH_TEXT_OVERLAY"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SINGLE_WIDE_IMAGE"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_WIDE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SUBSECTION_DARK_TITLE"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_DARK_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_PHOTOS"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_PHOTO_ALBUMS"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_H_SCROLL_SQUARE"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PLACE_WITH_METADATA_AND_DISCLOSURE"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA_AND_DISCLOSURE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_PARAGRAPH"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "COMPOSER"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTO_TILE"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_TILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FRIEND_REQUEST_LIST"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PYMK_SUGGESTION"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PYMK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "STATIC_PYMK"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_PYMK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_MATCH"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_SQUARE"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TRENDING_TOPIC"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TRENDING_TOPIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PROGRESS_BAR"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BANNER_HIGHLIGHTABLE"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BANNER_HIGHLIGHTABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_TEXT"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_CIRCLE"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CIRCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_LIVE_HEADER"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_APPOINTMENT_STATUS"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_HAIRLINE"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_HAIRLINE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_RECOMMENDATION"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_RECOMMENDATION_SQUARE"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LARGE_PROFILE_IMAGE_BLOCK"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DETAILS"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DETAILS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_FAN_FAVORITE_FRIENDS_VOTES"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_FRIENDS_VOTES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 533999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_DARK_PARAGRAPH_LONG_TRUNCATION"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "POST_COMMENTS_MODERATION_BLOCK"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_COMMENTS_MODERATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_PHOTO_ALBUM"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_MAP_WITH_NAVIGATION"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_SECTION_HEADER"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_SEE_MORE"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_MESSAGE_BLOCK"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_SHORTCUT"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "INFO_ROW_WITH_RIGHT_ICON"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_RIGHT_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534008
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_SMALL_FACEPILE"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_SMALL_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534009
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SPACING"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SPACING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534010
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CREATE_PHOTO_ALBUM"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534011
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_SEE_ALL_PHOTO_ALBUMS"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534012
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "A_PLACE_FOR_HEADER_CARD"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534013
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_SEE_ALL_PHOTO_ALBUMS_GRID"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534014
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FULL_WIDTH_ACTION_BUTTON"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FULL_WIDTH_ACTION_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534015
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NATIVE_TEMPLATE"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NATIVE_TEMPLATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534016
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SEGMENTED_PROGRESS_BAR"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SEGMENTED_PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534017
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EXPLORE_FEED"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534018
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EXPLORE_FEED_RECOMMENDATION"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LOCAL_CONTENT_REVIEW_COMPOSER"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_TYPED_TABLE"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TYPED_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_SQUARE"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_FAN_FAVORITE_WITH_TITLE"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_WITH_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "IMAGE_PROFILE_BLOCK"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_PROFILE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "POST_PIVOT"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "INFO_ROW_EVENT_ROLE"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SINGLE_IMAGE_EVENT_ROLE"

    const/16 v2, 0xc2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CIRCULAR_IMAGE_WITH_BADGE"

    const/16 v2, 0xc3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CIRCULAR_IMAGE_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SINGLE_IMAGE_SHORT"

    const/16 v2, 0xc4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_SHORT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITHOUT_DATE"

    const/16 v2, 0xc5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITH_FACEPILE"

    const/16 v2, 0xc6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BOTTOM_BORDER_IMAGE_BLOCK"

    const/16 v2, 0xc7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOTTOM_BORDER_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_FEATURED_SERVICE_ITEMS"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_FEATURED_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FIG_ACTION_FOOTER"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_ACTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GROUP_DESCRIPTION_WITH_JOIN_BUTTON"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_DESCRIPTION_WITH_JOIN_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TAB_SWITCHER_LABELS_ON_TOP"

    const/16 v2, 0xcd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_TOP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HEADER_WITH_TEXT_BUTTON"

    const/16 v2, 0xce

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_TEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_STICKY_HEADER"

    const/16 v2, 0xcf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_STICKY_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TEXT_HEADER_WITH_BADGE"

    const/16 v2, 0xd0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "COMMENT"

    const/16 v2, 0xd1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_ICON_MESSAGE"

    const/16 v2, 0xd2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HEADER_WITH_VERIFIED_BADGE"

    const/16 v2, 0xd3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_VERIFIED_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_NOTIFICATION_BLOCK"

    const/16 v2, 0xd4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TOP_LEVEL_COMMENT_COMPOSER"

    const/16 v2, 0xd5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOP_LEVEL_COMMENT_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534046
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FOOTER_WITH_BADGE"

    const/16 v2, 0xd6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS"

    const/16 v2, 0xd7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ADMIN_FEED_STORY_IMAGE_BLOCK"

    const/16 v2, 0xd8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADMIN_FEED_STORY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534049
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INLINE_COMPOSER"

    const/16 v2, 0xd9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534050
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_INSIGHTS_AYMT"

    const/16 v2, 0xda

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_AYMT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534051
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BOOSTED_COMPONENT_PROMOTION_BLOCK"

    const/16 v2, 0xdb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534052
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EXPANDABLE_PLACE_INFO_BLURB"

    const/16 v2, 0xdc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_NOTIFICATIONS_ENTRY_POINT"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATIONS_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "A_PLACE_FOR_STORY_CARD"

    const/16 v2, 0xde

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_STORY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_ACTION_WITH_COMPONENTS_LIST"

    const/16 v2, 0xdf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534056
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "MESSAGE_BLOCK"

    const/16 v2, 0xe0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "STATIC_MAP"

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EXPANDABLE_DRAWER"

    const/16 v2, 0xe2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_DRAWER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PLACE_INFO_BLURB_WITH_BREADCRUMBS"

    const/16 v2, 0xe3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534060
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "A_PLACE_FOR_FOOTER_CARD"

    const/16 v2, 0xe4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_FOOTER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534061
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "DEFERRED_LOAD"

    const/16 v2, 0xe5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->DEFERRED_LOAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534062
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE"

    const/16 v2, 0xe6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL"

    const/16 v2, 0xe7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534064
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_SPORTS_PLAY_FEED_STORY"

    const/16 v2, 0xe8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534065
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND"

    const/16 v2, 0xe9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXT_ROW_WITH_BADGE"

    const/16 v2, 0xea

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_MESSAGE_WITH_CARET"

    const/16 v2, 0xeb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_WITH_CARET:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_PARAGRAPH_EXPANDABLE"

    const/16 v2, 0xec

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TOGGLE_STATE"

    const/16 v2, 0xed

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOGGLE_STATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FRIEND_REQUEST_ACTION_LIST"

    const/16 v2, 0xee

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FRIEND_REQUEST_ATTACHMENT"

    const/16 v2, 0xef

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "MARGIN_TOP_IMAGE_BLOCK"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MARGIN_TOP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_SERVICE_ITEM"

    const/16 v2, 0xf1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST"

    const/16 v2, 0xf2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534075
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_CARD"

    const/16 v2, 0xf3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ADMIN_TIP"

    const/16 v2, 0xf4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADMIN_TIP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GROUP_IMAGE_BLOCK"

    const/16 v2, 0xf5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PROFILE_FRAME"

    const/16 v2, 0xf6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT"

    const/16 v2, 0xf7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "OFFER_ON_PAGES_OFFER_CARD"

    const/16 v2, 0xf8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OFFER_ON_PAGES_OFFER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534081
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CRISIS_RESPONSE"

    const/16 v2, 0xf9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CRISIS_RESPONSE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534082
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY"

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534083
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_BLOCK"

    const/16 v2, 0xfb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534084
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ADINTERFACES_OBJECTIVE_BLOCK"

    const/16 v2, 0xfc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_OBJECTIVE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534085
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ADINTERFACES_CARD_TITLE"

    const/16 v2, 0xfd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_CARD_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534086
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FRIEND_REQUEST_STATEFUL_ACTION_LIST"

    const/16 v2, 0xfe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_STATEFUL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534087
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_PHOTOS_WITH_OVERLAYS"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS_WITH_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534088
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_OPEN_HOURS_GRID"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534089
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_NOTIF"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534090
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_NOTIFICATIONS_HEADER"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534091
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ABOUT_DESCRIPTION"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534092
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_VIDEO"

    const/16 v2, 0x104

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534093
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "JOBS_ITEM"

    const/16 v2, 0x105

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534094
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_MAP_WITH_DISTANCE"

    const/16 v2, 0x106

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_DISTANCE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW"

    const/16 v2, 0x107

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CORE_IMAGE_TEXT_WITH_BUTTON"

    const/16 v2, 0x108

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "JOBS_ITEM_ON_TAB"

    const/16 v2, 0x109

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM_ON_TAB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_ALIGN_FOOTER"

    const/16 v2, 0x10a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CITY_GUIDE_INFO_AND_MAP"

    const/16 v2, 0x10b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_INFO_AND_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LARGE_ICON_MESSAGE"

    const/16 v2, 0x10c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "ICON_WITH_AUX_ACTION"

    const/16 v2, 0x10d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534102
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NOTIFICATIONS_PARITY"

    const/16 v2, 0x10e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_PARITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534103
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION"

    const/16 v2, 0x10f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534104
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION"

    const/16 v2, 0x110

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534105
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TWO_PLAYER_MATCH"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534106
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CURRENT_WEATHER"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534107
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "SHORT_CURRENT_WEATHER"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534108
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "WEATHER_FORECAST"

    const/16 v2, 0x114

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_FORECAST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534109
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CITY_GUIDE_FRIENDS_AT_CITY"

    const/16 v2, 0x115

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_FRIENDS_AT_CITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "OPEN_HOURS_AVAILABLE"

    const/16 v2, 0x116

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "OPEN_HOURS_UNAVAILABLE"

    const/16 v2, 0x117

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534112
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "EPCOT_PASSPORT_ROW"

    const/16 v2, 0x118

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EPCOT_PASSPORT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "OPEN_HOURS_UNDETERMINED"

    const/16 v2, 0x119

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "STORY_BLOCK_WITH_UFI"

    const/16 v2, 0x11a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK_WITH_UFI:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "CENTERED_BUTTON"

    const/16 v2, 0x11b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "FUNDRAISER_AMOUNT_RAISED"

    const/16 v2, 0x11c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FUNDRAISER_AMOUNT_RAISED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "LEFT_RIGHT"

    const/16 v2, 0x11d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "BUTTON_ROUNDED_CORNERS"

    const/16 v2, 0x11e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BUTTON_ROUNDED_CORNERS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "MESSAGE_IMAGE_BLOCK_WITH_STATUS"

    const/16 v2, 0x11f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_IMAGE_BLOCK_WITH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_TEAM_PAGES"

    const/16 v2, 0x120

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TEAM_PAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534121
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_SERVICE_ITEMS"

    const/16 v2, 0x121

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXT_ROW_WITH_CHEVRON"

    const/16 v2, 0x122

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_CHEVRON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534123
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "GAMETIME_BASEBALL_LIVE_HEADER"

    const/16 v2, 0x123

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_BASEBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "POPULAR_HOURS_HISTOGRAM"

    const/16 v2, 0x124

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POPULAR_HOURS_HISTOGRAM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_METABOX"

    const/16 v2, 0x125

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD"

    const/16 v2, 0x126

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INFO_ROW_WITH_BUTTON"

    const/16 v2, 0x127

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534128
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_POST_STORY"

    const/16 v2, 0x128

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "JOBS_CREATION"

    const/16 v2, 0x129

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD"

    const/16 v2, 0x12a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD"

    const/16 v2, 0x12b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD"

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD"

    const/16 v2, 0x12d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_HIGHLIGHTS_PHONE_ROW"

    const/16 v2, 0x12e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_PHONE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_HIGHLIGHTS_WEBSITE_ROW"

    const/16 v2, 0x12f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_WEBSITE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_PAGER_COMPONENTS_LIST"

    const/16 v2, 0x130

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_PAGER_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD"

    const/16 v2, 0x131

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CITY_FRIENDS_ACTIVITY"

    const/16 v2, 0x132

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CITY_FRIENDS_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "JOB_UPSELL"

    const/16 v2, 0x133

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_STORY_BLOCK"

    const/16 v2, 0x134

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_LIVE_VIDEO"

    const/16 v2, 0x135

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_HIGHLIGHTS_INFO_ROW"

    const/16 v2, 0x136

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER"

    const/16 v2, 0x137

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "TAGS"

    const/16 v2, 0x138

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAGS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_TEXT_HEADER"

    const/16 v2, 0x139

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534146
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_NOTE_BLOCK"

    const/16 v2, 0x13a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534147
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW"

    const/16 v2, 0x13b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534148
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PHOTOS_HEADER_WITH_SEE_ALL_BUTTON"

    const/16 v2, 0x13c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_HEADER_WITH_SEE_ALL_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534149
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ABOUT_PAYMENT_OPTIONS"

    const/16 v2, 0x13d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534150
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_LOCAL_RECOMMENDATIONS"

    const/16 v2, 0x13e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LOCAL_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534151
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ABOUT_INFO_CARD_HEADER"

    const/16 v2, 0x13f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534152
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ABOUT_INFO_GRID"

    const/16 v2, 0x141

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGES_INSIGHTS_METRIC_WITH_CHART"

    const/16 v2, 0x142

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_METRIC_WITH_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_ORDER_AHEAD_STATUS"

    const/16 v2, 0x143

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ORDER_AHEAD_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CATEGORY_BASED_RECOMMENDATIONS"

    const/16 v2, 0x144

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_SOCIAL_CONTEXT_INFO_ROW"

    const/16 v2, 0x145

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER"

    const/16 v2, 0x146

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_CREATOR"

    const/16 v2, 0x147

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER"

    const/16 v2, 0x148

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_XOUT_COMPONENTS_LIST"

    const/16 v2, 0x149

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "H_SCROLL_XOUT_HEADER"

    const/16 v2, 0x14a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "INFO_ROW_WITH_FRIENDSHIP_STATUS"

    const/16 v2, 0x14b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_FRIENDSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_INLINE_UPSELL"

    const/16 v2, 0x14c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY"

    const/16 v2, 0x14d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_UPCOMING_EVENTS_CARD"

    const/16 v2, 0x14e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_BRICK"

    const/16 v2, 0x14f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "DEPRECATED_336"

    const/16 v2, 0x150

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->DEPRECATED_336:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534169
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_PYML_CITY_RECOMMENDATIONS"

    const/16 v2, 0x151

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534170
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGE_HOME_LOCATION_CARD"

    const/16 v2, 0x152

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HOME_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534171
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_HYBRID"

    const/16 v2, 0x153

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_SEE_ALL_FOOTER"

    const/16 v2, 0x154

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_EM"

    const/16 v2, 0x155

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS"

    const/16 v2, 0x156

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534175
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_HOME_FEATURED_HEADER"

    const/16 v2, 0x157

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_FEATURED_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534176
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const-string v1, "VIDEO_CHANNEL_FEED_UNIT_FEATURED"

    const/16 v2, 0x158

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_FEATURED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 534177
    const/16 v0, 0x159

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_WIDE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_OVER_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_AUTO_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH_WITH_INLINE_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ARTICLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COVER_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_WITH_INLINE_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_DETAILED_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_NARROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_WIDE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROMPT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_MOVIE_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_SMALL_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADDRESS_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTACT_INFO_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_RATINGS_AND_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROADCAST_REMINDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SERVICE_NUX_BODY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TITLE_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_UNIT_SETTINGS_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COUNTS_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_WRITE_FIRST_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATION_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_ETA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_NEARBY_FRIEND_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FORMATTED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_COLOR_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STAGGERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBTITLE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNLABELED_LINE_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_METRIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROWSE_BACKED_MEDIA_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_OVERLAY_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_ICON_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER_LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEAD_TO_HEAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_AS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_RECENT_PLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_DAY_AND_TIME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_ALERT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CONNECTED_EVENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_BAR_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MAP_WITH_BREADCRUMBS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FOOTBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_WIDE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_DARK_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA_AND_DISCLOSURE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_TILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PYMK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_PYMK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TRENDING_TOPIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BANNER_HIGHLIGHTABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CIRCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_HAIRLINE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DETAILS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_FRIENDS_VOTES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_COMMENTS_MODERATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_RIGHT_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_SMALL_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SPACING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FULL_WIDTH_ACTION_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NATIVE_TEMPLATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SEGMENTED_PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TYPED_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_WITH_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_PROFILE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CIRCULAR_IMAGE_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_SHORT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOTTOM_BORDER_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_FEATURED_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_ACTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_DESCRIPTION_WITH_JOIN_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_TOP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_TEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_STICKY_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_VERIFIED_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOP_LEVEL_COMMENT_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADMIN_FEED_STORY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_AYMT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATIONS_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_STORY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_DRAWER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_FOOTER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->DEFERRED_LOAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_WITH_CARET:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xed

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOGGLE_STATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xee

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xef

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MARGIN_TOP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADMIN_TIP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OFFER_ON_PAGES_OFFER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CRISIS_RESPONSE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_OBJECTIVE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_CARD_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_STATEFUL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xff

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS_WITH_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x100

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x101

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x102

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x103

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x104

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x105

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x106

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_DISTANCE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x107

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x108

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x109

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM_ON_TAB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_INFO_AND_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_PARITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x110

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x111

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x112

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x113

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x114

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_FORECAST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x115

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_FRIENDS_AT_CITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x116

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x117

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x118

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EPCOT_PASSPORT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x119

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK_WITH_UFI:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FUNDRAISER_AMOUNT_RAISED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BUTTON_ROUNDED_CORNERS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_IMAGE_BLOCK_WITH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x120

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TEAM_PAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x121

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x122

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_CHEVRON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x123

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_BASEBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x124

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POPULAR_HOURS_HISTOGRAM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x125

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x126

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x127

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x128

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x129

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_PHONE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_WEBSITE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x130

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_PAGER_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x131

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x132

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CITY_FRIENDS_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x133

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x134

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x135

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x136

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x137

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x138

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAGS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x139

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_HEADER_WITH_SEE_ALL_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LOCAL_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x140

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x141

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x142

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_METRIC_WITH_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x143

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ORDER_AHEAD_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x144

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x145

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x146

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x147

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x148

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x149

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_FRIENDSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x150

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->DEPRECATED_336:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x151

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x152

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HOME_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x153

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x154

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x155

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x156

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x157

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_FEATURED_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x158

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_FEATURED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 533831
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 2

    .prologue
    .line 532960
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 532961
    :goto_0
    return-object v0

    .line 532962
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 532963
    packed-switch v0, :pswitch_data_0

    .line 532964
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532965
    :pswitch_1
    const-string v0, "CORE_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 532966
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532967
    :cond_2
    const-string v0, "PAGE_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 532968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532969
    :cond_3
    const-string v0, "OPEN_HOURS_UNAVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 532970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532971
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532972
    :pswitch_2
    const-string v0, "GAMETIME_FAN_FAVORITE_WITH_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 532973
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_WITH_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532974
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532975
    :pswitch_3
    const-string v0, "SUBSECTION_DARK_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 532976
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_DARK_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532977
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532978
    :pswitch_4
    const-string v0, "EXPANDABLE_DRAWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 532979
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_DRAWER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532980
    :cond_7
    const-string v0, "CENTERED_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 532981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto :goto_0

    .line 532982
    :cond_8
    const-string v0, "PAGE_METABOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 532983
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_METABOX:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532984
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532985
    :pswitch_5
    const-string v0, "LARGE_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 532986
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532987
    :cond_a
    const-string v0, "FRIEND_REQUEST_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 532988
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532989
    :cond_b
    const-string v0, "SINGLE_IMAGE_EVENT_ROLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 532990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532991
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532992
    :pswitch_6
    const-string v0, "H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 532993
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_MEDIUM_PADDING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532994
    :cond_d
    const-string v0, "WEATHER_CONDITION_WIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 532995
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_WIDE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532996
    :cond_e
    const-string v0, "PAGE_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 532997
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 532998
    :cond_f
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 532999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533000
    :cond_10
    const-string v0, "JOB_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 533001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOB_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533002
    :cond_11
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_BRICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 533003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533004
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533005
    :pswitch_7
    const-string v0, "FRIEND_REQUEST_ATTACHMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 533006
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533007
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533008
    :pswitch_8
    const-string v0, "NOTIFICATIONS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 533009
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533010
    :cond_14
    const-string v0, "FRIEND_REQUEST_ACTION_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 533011
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533012
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533013
    :pswitch_9
    const-string v0, "PAGE_ADMIN_TIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 533014
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADMIN_TIP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533015
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533016
    :pswitch_a
    const-string v0, "SHORT_PARAGRAPH_WITH_INLINE_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 533017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH_WITH_INLINE_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533018
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533019
    :pswitch_b
    const-string v0, "UPSELL_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 533020
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533021
    :cond_18
    const-string v0, "CORE_IMAGE_TEXT_WITH_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 533022
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533023
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533024
    :pswitch_c
    const-string v0, "SINGLE_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 533025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533026
    :cond_1a
    const-string v0, "LEFT_PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 533027
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533028
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533029
    :pswitch_d
    const-string v0, "FACEPILE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 533030
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533031
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533032
    :pswitch_e
    const-string v0, "VIDEO_HOME_SHORTCUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 533033
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533034
    :cond_1d
    const-string v0, "FRIEND_REQUEST_STATEFUL_ACTION_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 533035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FRIEND_REQUEST_STATEFUL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533036
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533037
    :pswitch_f
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 533038
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533039
    :cond_1f
    const-string v0, "PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 533040
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533041
    :cond_20
    const-string v0, "BROWSE_BACKED_MEDIA_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 533042
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROWSE_BACKED_MEDIA_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533043
    :cond_21
    const-string v0, "MAP_WITH_BREADCRUMBS_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 533044
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MAP_WITH_BREADCRUMBS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533045
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533046
    :pswitch_10
    const-string v0, "VIDEO_CHANNEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 533047
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533048
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533049
    :pswitch_11
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 533050
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533051
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533052
    :pswitch_12
    const-string v0, "VERTICAL_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 533053
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533054
    :cond_25
    const-string v0, "FUNDRAISER_AMOUNT_RAISED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 533055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FUNDRAISER_AMOUNT_RAISED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533056
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533057
    :pswitch_13
    const-string v0, "GROUP_DESCRIPTION_WITH_JOIN_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 533058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_DESCRIPTION_WITH_JOIN_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533059
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533060
    :pswitch_14
    const-string v0, "TWO_PLAYER_MATCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 533061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533062
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533063
    :pswitch_15
    const-string v0, "CORE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 533064
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533065
    :cond_29
    const-string v0, "TAB_SWITCHER_LABELS_ON_TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 533066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_TOP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533067
    :cond_2a
    const-string v0, "EXPANDABLE_PLACE_INFO_BLURB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 533068
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE_PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533069
    :cond_2b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533070
    :pswitch_16
    const-string v0, "CENTERED_IMAGES_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 533071
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533072
    :cond_2c
    const-string v0, "EVENT_DETAILS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 533073
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DETAILS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533074
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533075
    :pswitch_17
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 533076
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533077
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533078
    :pswitch_18
    const-string v0, "TWO_PLAYER_DETAILED_MATCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 533079
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_DETAILED_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533080
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533081
    :pswitch_19
    const-string v0, "VERTICAL_ACTION_WITH_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 533082
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533083
    :cond_30
    const-string v0, "JOBS_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 533084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533085
    :cond_31
    const-string v0, "PAGE_POST_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 533086
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533087
    :cond_32
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533088
    :pswitch_1a
    const-string v0, "IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 533089
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533090
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533091
    :pswitch_1b
    const-string v0, "EVENT_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 533092
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533093
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533094
    :pswitch_1c
    const-string v0, "PAGE_SERVICE_NUX_BODY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 533095
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SERVICE_NUX_BODY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533096
    :cond_35
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533097
    :pswitch_1d
    const-string v0, "PHOTO_WITH_TEXT_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 533098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533099
    :cond_36
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 533100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533101
    :cond_37
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533102
    :pswitch_1e
    const-string v0, "IMAGE_STORY_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 533103
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533104
    :cond_38
    const-string v0, "SHORT_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 533105
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533106
    :cond_39
    const-string v0, "MESSAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 533107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533108
    :cond_3a
    const-string v0, "VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 533109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_ACTION_WITH_COMPONENTS_STATEFUL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533110
    :cond_3b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533111
    :pswitch_1f
    const-string v0, "STAGGERED_IMAGES_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 533112
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STAGGERED_IMAGES_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533113
    :cond_3c
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_HYBRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 533114
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533115
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533116
    :pswitch_20
    const-string v0, "IMAGE_PROFILE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 533117
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_PROFILE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533118
    :cond_3e
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 533119
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_LIVE_SEAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533120
    :cond_3f
    const-string v0, "PAGE_CITY_FRIENDS_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 533121
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CITY_FRIENDS_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533122
    :cond_40
    const-string v0, "BANNER_HIGHLIGHTABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 533123
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BANNER_HIGHLIGHTABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533124
    :cond_41
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533125
    :pswitch_21
    const-string v0, "FOOTER_WITH_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 533126
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533127
    :cond_42
    const-string v0, "ADMIN_FEED_STORY_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 533128
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADMIN_FEED_STORY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533129
    :cond_43
    const-string v0, "ADINTERFACES_OBJECTIVE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 533130
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_OBJECTIVE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533131
    :cond_44
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_FEATURED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 533132
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_FEATURED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533133
    :cond_45
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533134
    :pswitch_22
    const-string v0, "SPACING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 533135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SPACING:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533136
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533137
    :pswitch_23
    const-string v0, "VERTICAL_COMPONENT_LIST_INNER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 533138
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533139
    :cond_47
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533140
    :pswitch_24
    const-string v0, "ACORN_NEARBY_FRIEND_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 533141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_NEARBY_FRIEND_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533142
    :cond_48
    const-string v0, "INFO_ROW_WITH_FRIENDSHIP_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 533143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_FRIENDSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533144
    :cond_49
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533145
    :pswitch_25
    const-string v0, "WEATHER_CONDITION_NARROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 533146
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_NARROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533147
    :cond_4a
    const-string v0, "NATIVE_TEMPLATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 533148
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NATIVE_TEMPLATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533149
    :cond_4b
    const-string v0, "MARGIN_TOP_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 533150
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MARGIN_TOP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533151
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533152
    :pswitch_26
    const-string v0, "PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 533153
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_WITH_TITLE_SUBTITLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533154
    :cond_4d
    const-string v0, "MESSAGE_IMAGE_BLOCK_WITH_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 533155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->MESSAGE_IMAGE_BLOCK_WITH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533156
    :cond_4e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533157
    :pswitch_27
    const-string v0, "HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 533158
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533159
    :cond_4f
    const-string v0, "ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 533160
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533161
    :cond_50
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533162
    :pswitch_28
    const-string v0, "ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 533163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_RECENT_CONVO_SUMMARY_V2_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533164
    :cond_51
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533165
    :pswitch_29
    const-string v0, "PLACE_WITH_METADATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 533166
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533167
    :cond_52
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533168
    :pswitch_2a
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_EM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 533169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533170
    :cond_53
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533171
    :pswitch_2b
    const-string v0, "VIDEO_HOME_HAIRLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 533172
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_HAIRLINE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533173
    :cond_54
    const-string v0, "VIDEO_HOME_SEE_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 533174
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_MORE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533175
    :cond_55
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533176
    :pswitch_2c
    const-string v0, "VIDEO_CHANNEL_SQUARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 533177
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533178
    :cond_56
    const-string v0, "VIDEO_CHANNEL_CIRCLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 533179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CIRCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533180
    :cond_57
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533181
    :pswitch_2d
    const-string v0, "JOBS_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 533182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_CREATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533183
    :cond_58
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533184
    :pswitch_2e
    const-string v0, "PROGRESS_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 533185
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533186
    :cond_59
    const-string v0, "H_SCROLL_XOUT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 533187
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533188
    :cond_5a
    const-string v0, "CITY_GUIDE_INFO_AND_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 533189
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_INFO_AND_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533190
    :cond_5b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533191
    :pswitch_2f
    const-string v0, "LEFT_ALIGN_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 533192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533193
    :cond_5c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533194
    :pswitch_30
    const-string v0, "ICON_MESSAGE_WITH_CARET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 533195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_WITH_CARET:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533196
    :cond_5d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533197
    :pswitch_31
    const-string v0, "CENTERED_PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 533198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533199
    :cond_5e
    const-string v0, "TEXT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 533200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533201
    :cond_5f
    const-string v0, "GAMETIME_MATCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 533202
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_MATCH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533203
    :cond_60
    const-string v0, "EXPLORE_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 533204
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533205
    :cond_61
    const-string v0, "STATIC_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 533206
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_MAP:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533207
    :cond_62
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533208
    :pswitch_32
    const-string v0, "TEXT_DIVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 533209
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_DIVIDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533210
    :cond_63
    const-string v0, "PAGE_TEXT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 533211
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533212
    :cond_64
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533213
    :pswitch_33
    const-string v0, "UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 533214
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533215
    :cond_65
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_SQUARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 533216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533217
    :cond_66
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533218
    :pswitch_34
    const-string v0, "FULL_WIDTH_ACTION_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 533219
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FULL_WIDTH_ACTION_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533220
    :cond_67
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533221
    :pswitch_35
    const-string v0, "PAGE_INLINE_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 533222
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533223
    :cond_68
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533224
    :pswitch_36
    const-string v0, "HORIZONTAL_ACTION_LIST_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 533225
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533226
    :cond_69
    const-string v0, "A_PLACE_FOR_STORY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 533227
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_STORY_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533228
    :cond_6a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533229
    :pswitch_37
    const-string v0, "VIDEO_CHANNEL_RECOMMENDATION_SQUARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 533230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533231
    :cond_6b
    const-string v0, "A_PLACE_FOR_HEADER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 533232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533233
    :cond_6c
    const-string v0, "A_PLACE_FOR_FOOTER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 533234
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->A_PLACE_FOR_FOOTER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533235
    :cond_6d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533236
    :pswitch_38
    const-string v0, "UNLABELED_LINE_CHART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 533237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNLABELED_LINE_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533238
    :cond_6e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533239
    :pswitch_39
    const-string v0, "PHOTO_FULL_WIDTH_COUNTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 533240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533241
    :cond_6f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533242
    :pswitch_3a
    const-string v0, "LOCAL_CONTENT_REVIEW_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 533243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533244
    :cond_70
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 533245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_PRUNE_NON_LIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533246
    :cond_71
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533247
    :pswitch_3b
    const-string v0, "PAGE_ABOUT_INFO_CARD_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 533248
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533249
    :cond_72
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533250
    :pswitch_3c
    const-string v0, "SHORT_PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 533251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533252
    :cond_73
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533253
    :pswitch_3d
    const-string v0, "PAGE_CONTEXT_ROWS_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 533254
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROWS_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533255
    :cond_74
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533256
    :pswitch_3e
    const-string v0, "IMAGE_WITH_OVERLAY_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 533257
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_OVERLAY_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533258
    :cond_75
    const-string v0, "PAGE_RELATED_PAGES_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 533259
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_RELATED_PAGES_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533260
    :cond_76
    const-string v0, "TOP_LEVEL_COMMENT_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 533261
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOP_LEVEL_COMMENT_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533262
    :cond_77
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533263
    :pswitch_3f
    const-string v0, "PLACE_INFO_BLURB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 533264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533265
    :cond_78
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533266
    :pswitch_40
    const-string v0, "EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 533267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533268
    :cond_79
    const-string v0, "TAB_SWITCHER_LABELS_AS_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 533269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_AS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533270
    :cond_7a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533271
    :pswitch_41
    const-string v0, "PAGES_INSIGHTS_OVERVIEW_CARD_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 533272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533273
    :cond_7b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533274
    :pswitch_42
    const-string v0, "GAMETIME_SPORTS_PLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 533275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533276
    :cond_7c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533277
    :pswitch_43
    const-string v0, "ARTICLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 533278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ARTICLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533279
    :cond_7d
    const-string v0, "EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 533280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE_AND_DETAIL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533281
    :cond_7e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533282
    :pswitch_44
    const-string v0, "VIDEO_CHANNEL_RECOMMENDATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 533283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533284
    :cond_7f
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 533285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533286
    :cond_80
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 533287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533288
    :cond_81
    const-string v0, "CITY_GUIDE_FRIENDS_AT_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 533289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CITY_GUIDE_FRIENDS_AT_CITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533290
    :cond_82
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533291
    :pswitch_45
    const-string v0, "TAGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 533292
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAGS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533293
    :cond_83
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533294
    :pswitch_46
    const-string v0, "PAGE_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 533295
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533296
    :cond_84
    const-string v0, "NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 533297
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_TIME_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533298
    :cond_85
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533299
    :pswitch_47
    const-string v0, "NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 533300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_LIST_INLINE_COUNT_EXPANSION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533301
    :cond_86
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533302
    :pswitch_48
    const-string v0, "EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_87

    .line 533303
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533304
    :cond_87
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 533305
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_EVENT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533306
    :cond_88
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533307
    :pswitch_49
    const-string v0, "VERTICAL_COMPONENT_LIST_W_AUX_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 533308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533309
    :cond_89
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 533310
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533311
    :cond_8a
    const-string v0, "PAGE_OPEN_HOURS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 533312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533313
    :cond_8b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533314
    :pswitch_4a
    const-string v0, "GAMETIME_SPORTS_PLAY_FEED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 533315
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_SPORTS_PLAY_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533316
    :cond_8c
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 533317
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533318
    :cond_8d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533319
    :pswitch_4b
    const-string v0, "PAGE_PHOTO_ALBUMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8e

    .line 533320
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533321
    :cond_8e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533322
    :pswitch_4c
    const-string v0, "ICON_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 533323
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533324
    :cond_8f
    const-string v0, "PAGES_SERVICE_ITEMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 533325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533326
    :cond_90
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533327
    :pswitch_4d
    const-string v0, "ADINTERFACES_CARD_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 533328
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ADINTERFACES_CARD_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533329
    :cond_91
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533330
    :pswitch_4e
    const-string v0, "EVENT_GUEST_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 533331
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_GUEST_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533332
    :cond_92
    const-string v0, "PAGE_APPOINTMENT_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 533333
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_APPOINTMENT_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533334
    :cond_93
    const-string v0, "PAGE_NOTE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 533335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533336
    :cond_94
    const-string v0, "PAGE_ORDER_AHEAD_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 533337
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ORDER_AHEAD_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533338
    :cond_95
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533339
    :pswitch_4f
    const-string v0, "ICON_OVER_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 533340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_OVER_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533341
    :cond_96
    const-string v0, "PAGE_STORY_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 533342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533343
    :cond_97
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533344
    :pswitch_50
    const-string v0, "PAGE_SEE_ALL_PHOTO_ALBUMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 533345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533346
    :cond_98
    const-string v0, "PAGE_PHOTOS_WITH_OVERLAYS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 533347
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTOS_WITH_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533348
    :cond_99
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533349
    :pswitch_51
    const-string v0, "VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 533350
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENT_LIST_INNER_CARD_W_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533351
    :cond_9a
    const-string v0, "PROMPT_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 533352
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROMPT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533353
    :cond_9b
    const-string v0, "PAGE_MESSAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 533354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MESSAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533355
    :cond_9c
    const-string v0, "INFO_ROW_EVENT_ROLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 533356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_EVENT_ROLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533357
    :cond_9d
    const-string v0, "BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 533358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_MANAGER_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533359
    :cond_9e
    const-string v0, "PAGE_ABOUT_PAYMENT_OPTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 533360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_PAYMENT_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533361
    :cond_9f
    const-string v0, "PAGE_LOCAL_RECOMMENDATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 533362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LOCAL_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533363
    :cond_a0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533364
    :pswitch_52
    const-string v0, "TWO_PLAYER_MATCH_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 533365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TWO_PLAYER_MATCH_STATUS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533366
    :cond_a1
    const-string v0, "PROFILE_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a2

    .line 533367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533368
    :cond_a2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533369
    :pswitch_53
    const-string v0, "COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 533370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533371
    :cond_a3
    const-string v0, "PAGES_FEATURED_SERVICE_ITEMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 533372
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_FEATURED_SERVICE_ITEMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533373
    :cond_a4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533374
    :pswitch_54
    const-string v0, "PAGE_INFO_RATINGS_AND_REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 533375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_RATINGS_AND_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533376
    :cond_a5
    const-string v0, "LARGE_PROFILE_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 533377
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_PROFILE_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533378
    :cond_a6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533379
    :pswitch_55
    const-string v0, "PLACE_INFO_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 533380
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533381
    :cond_a7
    const-string v0, "HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 533382
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_FOOTER_NO_ICONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533383
    :cond_a8
    const-string v0, "PAGE_PYML_CITY_RECOMMENDATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 533384
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PYML_CITY_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533385
    :cond_a9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533386
    :pswitch_56
    const-string v0, "ICON_MESSAGE_SMALL_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 533387
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_SMALL_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533388
    :cond_aa
    const-string v0, "PAGE_CONTACT_INFO_STACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 533389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTACT_INFO_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533390
    :cond_ab
    const-string v0, "ACORN_UNIT_SETTINGS_ICON_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 533391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_UNIT_SETTINGS_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533392
    :cond_ac
    const-string v0, "PAGE_NOTIFICATION_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 533393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533394
    :cond_ad
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533395
    :pswitch_57
    const-string v0, "EVENT_DESCRIPTION_WITHOUT_DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 533396
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533397
    :cond_ae
    const-string v0, "PLACE_INFO_BLURB_WITH_BREADCRUMBS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_af

    .line 533398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_INFO_BLURB_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533399
    :cond_af
    const-string v0, "STORY_BLOCK_WITH_UFI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 533400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK_WITH_UFI:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533401
    :cond_b0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533402
    :pswitch_58
    const-string v0, "TRENDING_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 533403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TRENDING_TOPIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533404
    :cond_b1
    const-string v0, "EVENT_DESCRIPTION_WITH_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 533405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533406
    :cond_b2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533407
    :pswitch_59
    const-string v0, "PAGE_CATEGORY_BASED_RECOMMENDATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 533408
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CATEGORY_BASED_RECOMMENDATIONS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533409
    :cond_b3
    const-string v0, "CURRENT_WEATHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 533410
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533411
    :cond_b4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533412
    :pswitch_5a
    const-string v0, "LEFT_RIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b5

    .line 533413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533414
    :cond_b5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533415
    :pswitch_5b
    const-string v0, "HORIZONTAL_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 533416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533417
    :cond_b6
    const-string v0, "EVENT_DESCRIPTION_WITH_DAY_AND_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 533418
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_DAY_AND_TIME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533419
    :cond_b7
    const-string v0, "POST_COMMENTS_MODERATION_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 533420
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_COMMENTS_MODERATION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533421
    :cond_b8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533422
    :pswitch_5c
    const-string v0, "PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 533423
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENT_AND_IMAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533424
    :cond_b9
    const-string v0, "PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ba

    .line 533425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533426
    :cond_ba
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533427
    :pswitch_5d
    const-string v0, "POST_PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 533428
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POST_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533429
    :cond_bb
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533430
    :pswitch_5e
    const-string v0, "H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bc

    .line 533431
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST_PAGE_PHOTO_ALBUMS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533432
    :cond_bc
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533433
    :pswitch_5f
    const-string v0, "EVENT_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 533434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533435
    :cond_bd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533436
    :pswitch_60
    const-string v0, "LABELED_BAR_CHART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_be

    .line 533437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_BAR_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533438
    :cond_be
    const-string v0, "GAMETIME_LIVE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bf

    .line 533439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533440
    :cond_bf
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533441
    :pswitch_61
    const-string v0, "ACORN_HIDE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 533442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533443
    :cond_c0
    const-string v0, "ACORN_MOVIE_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c1

    .line 533444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_MOVIE_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533445
    :cond_c1
    const-string v0, "HORIZONTAL_ACTION_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 533446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533447
    :cond_c2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533448
    :pswitch_62
    const-string v0, "H_SCROLL_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 533449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533450
    :cond_c3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533451
    :pswitch_63
    const-string v0, "SHORT_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 533452
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533453
    :cond_c4
    const-string v0, "DEFERRED_LOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c5

    .line 533454
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->DEFERRED_LOAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533455
    :cond_c5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533456
    :pswitch_64
    const-string v0, "HORIZONTAL_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 533457
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533458
    :cond_c6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533459
    :pswitch_65
    const-string v0, "LEFT_ALIGN_FOOTER_LIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 533460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_ALIGN_FOOTER_LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533461
    :cond_c7
    const-string v0, "PAGES_INSIGHTS_AYMT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 533462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_AYMT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533463
    :cond_c8
    const-string v0, "ICON_WITH_AUX_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 533464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533465
    :cond_c9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533466
    :pswitch_66
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 533467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_CREATORS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533468
    :cond_ca
    const-string v0, "SHORT_COLOR_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 533469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_COLOR_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533470
    :cond_cb
    const-string v0, "PAGES_INSIGHTS_OVERVIEW_CARD_METRIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 533471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_OVERVIEW_CARD_METRIC:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533472
    :cond_cc
    const-string v0, "HEAD_TO_HEAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 533473
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEAD_TO_HEAD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533474
    :cond_cd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533475
    :pswitch_67
    const-string v0, "H_SCROLL_XOUT_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 533476
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_XOUT_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533477
    :cond_ce
    const-string v0, "FORMATTED_PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 533478
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FORMATTED_PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533479
    :cond_cf
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533480
    :pswitch_68
    const-string v0, "H_SCROLL_PAGER_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 533481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_PAGER_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533482
    :cond_d0
    const-string v0, "PHOTO_H_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 533483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533484
    :cond_d1
    const-string v0, "GAMETIME_FOOTBALL_LIVE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d2

    .line 533485
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FOOTBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533486
    :cond_d2
    const-string v0, "EXPLORE_FEED_RECOMMENDATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 533487
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EXPLORE_FEED_RECOMMENDATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533488
    :cond_d3
    const-string v0, "GAMETIME_BASEBALL_LIVE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d4

    .line 533489
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_BASEBALL_LIVE_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533490
    :cond_d4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533491
    :pswitch_69
    const-string v0, "ICON_MESSAGE_AUTO_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d5

    .line 533492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ICON_MESSAGE_AUTO_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533493
    :cond_d5
    const-string v0, "SIMPLE_TEXT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 533494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533495
    :cond_d6
    const-string v0, "INFO_ROW_WITH_RIGHT_ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 533496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW_WITH_RIGHT_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533497
    :cond_d7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533498
    :pswitch_6a
    const-string v0, "H_SCROLL_GENERIC_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 533499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533500
    :cond_d8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533501
    :pswitch_6b
    const-string v0, "PHOTO_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 533502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533503
    :cond_d9
    const-string v0, "PAGE_INLINE_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_da

    .line 533504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INLINE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533505
    :cond_da
    const-string v0, "SHORT_CURRENT_WEATHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 533506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SHORT_CURRENT_WEATHER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533507
    :cond_db
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533508
    :pswitch_6c
    const-string v0, "EVENT_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 533509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533510
    :cond_dc
    const-string v0, "SEGMENTED_PROGRESS_BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 533511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SEGMENTED_PROGRESS_BAR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533512
    :cond_dd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533513
    :pswitch_6d
    const-string v0, "LABELED_ICON_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_de

    .line 533514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LABELED_ICON_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533515
    :cond_de
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533516
    :pswitch_6e
    const-string v0, "INFO_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 533517
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533518
    :cond_df
    const-string v0, "PAGE_NOTIFICATIONS_ENTRY_POINT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 533519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_NOTIFICATIONS_ENTRY_POINT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533520
    :cond_e0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533521
    :pswitch_6f
    const-string v0, "PAGES_INSIGHTS_METRIC_WITH_CHART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 533522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_INSIGHTS_METRIC_WITH_CHART:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533523
    :cond_e1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533524
    :pswitch_70
    const-string v0, "PAGINATED_H_SCROLL_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 533525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533526
    :cond_e2
    const-string v0, "PAGINATED_V_SCROLL_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e3

    .line 533527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533528
    :cond_e3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533529
    :pswitch_71
    const-string v0, "PAGE_OPEN_HOURS_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e4

    .line 533530
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_OPEN_HOURS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533531
    :cond_e4
    const-string v0, "PAGE_ABOUT_INFO_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e5

    .line 533532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_INFO_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533533
    :cond_e5
    const-string v0, "EPCOT_PASSPORT_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 533534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EPCOT_PASSPORT_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533535
    :cond_e6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533536
    :pswitch_72
    const-string v0, "HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e7

    .line 533537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HORIZONTAL_ACTION_LIST_VERTICAL_ICON_AND_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533538
    :cond_e7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533539
    :pswitch_73
    const-string v0, "PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 533540
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_CARD_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533541
    :cond_e8
    const-string v0, "PAGE_HOME_LOCATION_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 533542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HOME_LOCATION_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533543
    :cond_e9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533544
    :pswitch_74
    const-string v0, "PAGE_UPCOMING_EVENTS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 533545
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_UPCOMING_EVENTS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533546
    :cond_ea
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533547
    :pswitch_75
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_eb

    .line 533548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533549
    :cond_eb
    const-string v0, "PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ec

    .line 533550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533551
    :cond_ec
    const-string v0, "H_SCROLL_COMPONENTS_LIST_WIDE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 533552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->H_SCROLL_COMPONENTS_LIST_WIDE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533553
    :cond_ed
    const-string v0, "PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 533554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533555
    :cond_ee
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533556
    :pswitch_76
    const-string v0, "GAMETIME_TEAM_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ef

    .line 533557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TEAM_PAGES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533558
    :cond_ef
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533559
    :pswitch_77
    const-string v0, "GAMETIME_RECENT_PLAYS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 533560
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_RECENT_PLAYS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533561
    :cond_f0
    const-string v0, "NOTIFICATIONS_PARITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 533562
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATIONS_PARITY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533563
    :cond_f1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533564
    :pswitch_78
    const-string v0, "PAGE_SEE_ALL_PHOTO_ALBUMS_GRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f2

    .line 533565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SEE_ALL_PHOTO_ALBUMS_GRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533566
    :cond_f2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533567
    :pswitch_79
    const-string v0, "GROUP_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f3

    .line 533568
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GROUP_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533569
    :cond_f3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533570
    :pswitch_7a
    const-string v0, "PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 533571
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_COMPACT_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533572
    :cond_f4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533573
    :pswitch_7b
    const-string v0, "CENTERED_TITLE_WITH_BREADCRUMBS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f5

    .line 533574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TITLE_WITH_BREADCRUMBS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533575
    :cond_f5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533576
    :pswitch_7c
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f6

    .line 533577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533578
    :cond_f6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533579
    :pswitch_7d
    const-string v0, "STORY_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f7

    .line 533580
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STORY_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533581
    :cond_f7
    const-string v0, "STATIC_PYMK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f8

    .line 533582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->STATIC_PYMK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533583
    :cond_f8
    const-string v0, "PAGE_PHOTO_ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f9

    .line 533584
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533585
    :cond_f9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533586
    :pswitch_7e
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fa

    .line 533587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_MAP_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533588
    :cond_fa
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533589
    :pswitch_7f
    const-string v0, "PHOTO_TILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fb

    .line 533590
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_TILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533591
    :cond_fb
    const-string v0, "PAGES_SERVICE_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 533592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGES_SERVICE_ITEM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533593
    :cond_fc
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fd

    .line 533594
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_TEXT_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533595
    :cond_fd
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533596
    :pswitch_80
    const-string v0, "COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fe

    .line 533597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COMMENT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533598
    :cond_fe
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 533599
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_IMAGE_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533600
    :cond_ff
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_100

    .line 533601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533602
    :cond_100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533603
    :pswitch_81
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_101

    .line 533604
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533605
    :cond_101
    const-string v0, "GAMETIME_FAN_FAVORITE_FRIENDS_VOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_102

    .line 533606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE_FRIENDS_VOTES:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533607
    :cond_102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533608
    :pswitch_82
    const-string v0, "CORE_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_103

    .line 533609
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533610
    :cond_103
    const-string v0, "PROFILE_FRAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_104

    .line 533611
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PROFILE_FRAME:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533612
    :cond_104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533613
    :pswitch_83
    const-string v0, "LARGE_ICON_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_105

    .line 533614
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LARGE_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533615
    :cond_105
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533616
    :pswitch_84
    const-string v0, "FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_106

    .line 533617
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533618
    :cond_106
    const-string v0, "PAGE_CREATE_PHOTO_ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_107

    .line 533619
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533620
    :cond_107
    const-string v0, "POPULAR_HOURS_HISTOGRAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_108

    .line 533621
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->POPULAR_HOURS_HISTOGRAM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533622
    :cond_108
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533623
    :pswitch_85
    const-string v0, "TOGGLE_STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_109

    .line 533624
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOGGLE_STATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533625
    :cond_109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533626
    :pswitch_86
    const-string v0, "CENTERED_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10a

    .line 533627
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533628
    :cond_10a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533629
    :pswitch_87
    const-string v0, "HEADER_WITH_VERIFIED_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10b

    .line 533630
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_VERIFIED_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533631
    :cond_10b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533632
    :pswitch_88
    const-string v0, "CORE_IMAGE_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10c

    .line 533633
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533634
    :cond_10c
    const-string v0, "FIG_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10d

    .line 533635
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533636
    :cond_10d
    const-string v0, "FIG_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10e

    .line 533637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533638
    :cond_10e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533639
    :pswitch_89
    const-string v0, "PHOTO_H_SCROLL_SQUARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10f

    .line 533640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO_H_SCROLL_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533641
    :cond_10f
    const-string v0, "LEFT_PARAGRAPH_EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_110

    .line 533642
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_PARAGRAPH_EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533643
    :cond_110
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533644
    :pswitch_8a
    const-string v0, "PAGE_MAP_WITH_DISTANCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_111

    .line 533645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_DISTANCE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533646
    :cond_111
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533647
    :pswitch_8b
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_112

    .line 533648
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_NARROW_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533649
    :cond_112
    const-string v0, "BROADCAST_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_113

    .line 533650
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BROADCAST_REMINDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533651
    :cond_113
    const-string v0, "EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_114

    .line 533652
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITHOUT_DATE_OR_COVERPHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533653
    :cond_114
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533654
    :pswitch_8c
    const-string v0, "TAB_SWITCHER_LABELS_ON_BOTTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_115

    .line 533655
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TAB_SWITCHER_LABELS_ON_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533656
    :cond_115
    const-string v0, "CONNECTED_EVENTS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_116

    .line 533657
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CONNECTED_EVENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533658
    :cond_116
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533659
    :pswitch_8d
    const-string v0, "FIG_ACTION_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_117

    .line 533660
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->FIG_ACTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533661
    :cond_117
    const-string v0, "TEXT_HEADER_WITH_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_118

    .line 533662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_HEADER_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533663
    :cond_118
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533664
    :pswitch_8e
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_119

    .line 533665
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_LIVE_VIDEO_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533666
    :cond_119
    const-string v0, "PAGE_CONTEXT_ROW_WITH_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11a

    .line 533667
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533668
    :cond_11a
    const-string v0, "VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11b

    .line 533669
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533670
    :cond_11b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533671
    :pswitch_8f
    const-string v0, "TEXT_WITH_INLINE_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11c

    .line 533672
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TEXT_WITH_INLINE_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533673
    :cond_11c
    const-string v0, "COUNTS_HORIZONTAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11d

    .line 533674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COUNTS_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533675
    :cond_11d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533676
    :pswitch_90
    const-string v0, "SIMPLE_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11e

    .line 533677
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533678
    :cond_11e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533679
    :pswitch_91
    const-string v0, "PLACE_WITH_METADATA_AND_DISCLOSURE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11f

    .line 533680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PLACE_WITH_METADATA_AND_DISCLOSURE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533681
    :cond_11f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533682
    :pswitch_92
    const-string v0, "PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_120

    .line 533683
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VERY_RESPONSIVE_TO_MESSAGES_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533684
    :cond_120
    const-string v0, "PYMK_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_121

    .line 533685
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PYMK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533686
    :cond_121
    const-string v0, "SINGLE_IMAGE_SHORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_122

    .line 533687
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE_SHORT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533688
    :cond_122
    const-string v0, "HEADER_WITH_TEXT_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_123

    .line 533689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->HEADER_WITH_TEXT_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533690
    :cond_123
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533691
    :pswitch_93
    const-string v0, "WEATHER_FORECAST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_124

    .line 533692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_FORECAST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533693
    :cond_124
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533694
    :pswitch_94
    const-string v0, "PAGE_INFO_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_125

    .line 533695
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533696
    :cond_125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533697
    :pswitch_95
    const-string v0, "PAGE_ABOUT_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_126

    .line 533698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ABOUT_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533699
    :cond_126
    const-string v0, "JOBS_ITEM_ON_TAB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_127

    .line 533700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->JOBS_ITEM_ON_TAB:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533701
    :cond_127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533702
    :pswitch_96
    const-string v0, "PAGE_ADDRESS_NAVIGATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_128

    .line 533703
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_ADDRESS_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533704
    :cond_128
    const-string v0, "VIDEO_CHANNEL_CREATOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_129

    .line 533705
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533706
    :cond_129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533707
    :pswitch_97
    const-string v0, "WEATHER_CONDITION_ALERT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12a

    .line 533708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->WEATHER_CONDITION_ALERT:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533709
    :cond_12a
    const-string v0, "PAGE_MAP_WITH_NAVIGATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12b

    .line 533710
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_MAP_WITH_NAVIGATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533711
    :cond_12b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533712
    :pswitch_98
    const-string v0, "PAGE_INFO_ROW_WITH_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12c

    .line 533713
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533714
    :cond_12c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533715
    :pswitch_99
    const-string v0, "VIDEO_HOME_STICKY_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12d

    .line 533716
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_STICKY_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533717
    :cond_12d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533718
    :pswitch_9a
    const-string v0, "VIDEO_HOME_SECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12e

    .line 533719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533720
    :cond_12e
    const-string v0, "VIDEO_HOME_SEE_ALL_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12f

    .line 533721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533722
    :cond_12f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533723
    :pswitch_9b
    const-string v0, "VIDEO_HOME_FEATURED_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_130

    .line 533724
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_FEATURED_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533725
    :cond_130
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533726
    :pswitch_9c
    const-string v0, "PAGE_CONTEXT_ROW_WITH_CHEVRON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_131

    .line 533727
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXT_ROW_WITH_CHEVRON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533728
    :cond_131
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533729
    :pswitch_9d
    const-string v0, "LEFT_DARK_PARAGRAPH_LONG_TRUNCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_132

    .line 533730
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533731
    :cond_132
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533732
    :pswitch_9e
    const-string v0, "OPEN_HOURS_UNDETERMINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_133

    .line 533733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533734
    :cond_133
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533735
    :pswitch_9f
    const-string v0, "VIDEO_HOME_NOTIFICATIONS_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_134

    .line 533736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533737
    :cond_134
    const-string v0, "PHOTOS_HEADER_WITH_SEE_ALL_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_135

    .line 533738
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTOS_HEADER_WITH_SEE_ALL_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533739
    :cond_135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533740
    :pswitch_a0
    const-string v0, "OFFER_ON_PAGES_OFFER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_136

    .line 533741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OFFER_ON_PAGES_OFFER_CARD:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533742
    :cond_136
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533743
    :pswitch_a1
    const-string v0, "BUTTON_ROUNDED_CORNERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_137

    .line 533744
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BUTTON_ROUNDED_CORNERS:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533745
    :cond_137
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533746
    :pswitch_a2
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_138

    .line 533747
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533748
    :cond_138
    const-string v0, "PAGE_INFO_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_139

    .line 533749
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533750
    :cond_139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533751
    :pswitch_a3
    const-string v0, "IMAGE_WITH_TEXT_OVERLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13a

    .line 533752
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->IMAGE_WITH_TEXT_OVERLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533753
    :cond_13a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533754
    :pswitch_a4
    const-string v0, "CORE_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13b

    .line 533755
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533756
    :cond_13b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533757
    :pswitch_a5
    const-string v0, "COVER_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13c

    .line 533758
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->COVER_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533759
    :cond_13c
    const-string v0, "LOCAL_CONTENT_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13d

    .line 533760
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LOCAL_CONTENT_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533761
    :cond_13d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533762
    :pswitch_a6
    const-string v0, "PAGE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13e

    .line 533763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533764
    :cond_13e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533765
    :pswitch_a7
    const-string v0, "CRISIS_RESPONSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13f

    .line 533766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CRISIS_RESPONSE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533767
    :cond_13f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533768
    :pswitch_a8
    const-string v0, "GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_140

    .line 533769
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE_WITH_CUSTOM_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533770
    :cond_140
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533771
    :pswitch_a9
    const-string v0, "GAMETIME_TABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_141

    .line 533772
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533773
    :cond_141
    const-string v0, "BOTTOM_BORDER_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_142

    .line 533774
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOTTOM_BORDER_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533775
    :cond_142
    const-string v0, "PAGE_LIVE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_143

    .line 533776
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_LIVE_VIDEO:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533777
    :cond_143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533778
    :pswitch_aa
    const-string v0, "PAGE_HIGHLIGHTS_INFO_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_144

    .line 533779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533780
    :cond_144
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533781
    :pswitch_ab
    const-string v0, "PAGE_HIGHLIGHTS_PHONE_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_145

    .line 533782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_PHONE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533783
    :cond_145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533784
    :pswitch_ac
    const-string v0, "PAGE_HIGHLIGHTS_WEBSITE_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_146

    .line 533785
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_HIGHLIGHTS_WEBSITE_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533786
    :cond_146
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533787
    :pswitch_ad
    const-string v0, "PAGE_INFO_WRITE_FIRST_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_147

    .line 533788
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_INFO_WRITE_FIRST_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533789
    :cond_147
    const-string v0, "CENTERED_SMALL_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_148

    .line 533790
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CENTERED_SMALL_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533791
    :cond_148
    const-string v0, "PAGE_SOCIAL_CONTEXT_INFO_ROW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_149

    .line 533792
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_SOCIAL_CONTEXT_INFO_ROW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533793
    :cond_149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533794
    :pswitch_ae
    const-string v0, "GAMETIME_TYPED_TABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14a

    .line 533795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_TYPED_TABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533796
    :cond_14a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533797
    :pswitch_af
    const-string v0, "PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14b

    .line 533798
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST_FREE_ALIGN:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533799
    :cond_14b
    const-string v0, "GAMETIME_FAN_FAVORITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14c

    .line 533800
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533801
    :cond_14c
    const-string v0, "CIRCULAR_IMAGE_WITH_BADGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14d

    .line 533802
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CIRCULAR_IMAGE_WITH_BADGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533803
    :cond_14d
    const-string v0, "GAMETIME_ICON_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14e

    .line 533804
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_ICON_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533805
    :cond_14e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533806
    :pswitch_b0
    const-string v0, "BOOSTED_COMPONENT_PROMOTION_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14f

    .line 533807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->BOOSTED_COMPONENT_PROMOTION_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533808
    :cond_14f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533809
    :pswitch_b1
    const-string v0, "SINGLE_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_150

    .line 533810
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533811
    :cond_150
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533812
    :pswitch_b2
    const-string v0, "ACORN_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_151

    .line 533813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ACORN_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533814
    :cond_151
    const-string v0, "NOTIFICATION_IMAGE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_152

    .line 533815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->NOTIFICATION_IMAGE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533816
    :cond_152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533817
    :pswitch_b3
    const-string v0, "SUBSECTION_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_153

    .line 533818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBSECTION_TITLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533819
    :cond_153
    const-string v0, "EVENT_DESCRIPTION_WITH_ETA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_154

    .line 533820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->EVENT_DESCRIPTION_WITH_ETA:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533821
    :cond_154
    const-string v0, "SUBTITLE_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_155

    .line 533822
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SUBTITLE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533823
    :cond_155
    const-string v0, "OPEN_HOURS_AVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_156

    .line 533824
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->OPEN_HOURS_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533825
    :cond_156
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533826
    :pswitch_b4
    const-string v0, "SINGLE_WIDE_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_157

    .line 533827
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_WIDE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533828
    :cond_157
    const-string v0, "PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_158

    .line 533829
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CONTEXTUAL_RECOMMENDATIONS_REVIEW:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    .line 533830
    :cond_158
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_0
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_28
        :pswitch_0
        :pswitch_0
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_0
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_0
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_0
        :pswitch_3f
        :pswitch_40
        :pswitch_0
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_0
        :pswitch_0
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_0
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_0
        :pswitch_4c
        :pswitch_0
        :pswitch_0
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_0
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_0
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_0
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_0
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_0
        :pswitch_6b
        :pswitch_6c
        :pswitch_0
        :pswitch_6d
        :pswitch_6e
        :pswitch_0
        :pswitch_6f
        :pswitch_0
        :pswitch_70
        :pswitch_0
        :pswitch_71
        :pswitch_72
        :pswitch_0
        :pswitch_73
        :pswitch_0
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_0
        :pswitch_77
        :pswitch_78
        :pswitch_0
        :pswitch_0
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_0
        :pswitch_0
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
        :pswitch_83
        :pswitch_84
        :pswitch_85
        :pswitch_86
        :pswitch_87
        :pswitch_88
        :pswitch_0
        :pswitch_89
        :pswitch_8a
        :pswitch_8b
        :pswitch_0
        :pswitch_8c
        :pswitch_8d
        :pswitch_8e
        :pswitch_0
        :pswitch_8f
        :pswitch_0
        :pswitch_90
        :pswitch_0
        :pswitch_0
        :pswitch_91
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_92
        :pswitch_0
        :pswitch_93
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_94
        :pswitch_95
        :pswitch_96
        :pswitch_97
        :pswitch_98
        :pswitch_99
        :pswitch_9a
        :pswitch_9b
        :pswitch_9c
        :pswitch_0
        :pswitch_9d
        :pswitch_9e
        :pswitch_9f
        :pswitch_a0
        :pswitch_a1
        :pswitch_a2
        :pswitch_a3
        :pswitch_a4
        :pswitch_a5
        :pswitch_0
        :pswitch_a6
        :pswitch_0
        :pswitch_a7
        :pswitch_0
        :pswitch_a8
        :pswitch_a9
        :pswitch_aa
        :pswitch_ab
        :pswitch_0
        :pswitch_ac
        :pswitch_ad
        :pswitch_ae
        :pswitch_af
        :pswitch_b0
        :pswitch_0
        :pswitch_b1
        :pswitch_b2
        :pswitch_0
        :pswitch_0
        :pswitch_b3
        :pswitch_b4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 1

    .prologue
    .line 532958
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .locals 1

    .prologue
    .line 532959
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    return-object v0
.end method
