.class public final enum Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public static final enum LOCAL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 368868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 368869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 368870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->ALL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 368871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->LOCAL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 368872
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->ALL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->LOCAL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368884
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 1

    .prologue
    .line 368875
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 368876
    :goto_0
    return-object v0

    .line 368877
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368878
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    goto :goto_0

    .line 368879
    :cond_2
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->ALL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    goto :goto_0

    .line 368881
    :cond_3
    const-string v0, "LOCAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368882
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->LOCAL:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    goto :goto_0

    .line 368883
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 1

    .prologue
    .line 368874
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 1

    .prologue
    .line 368873
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-object v0
.end method
