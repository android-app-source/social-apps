.class public final enum Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum ATTACHMENT_AND_ENDSCREEN:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum BUTTON_UNDER_DESC:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum BUTTON_WITH_ICON_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum HIDE_FROM_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum SHOW_SPONSORSHIP:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public static final enum VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365632
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365633
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "BUTTON_WITH_TEXT_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365634
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "BUTTON_WITH_ICON_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_ICON_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365635
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "HIDE_FROM_FEED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->HIDE_FROM_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365636
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "ATTACHMENT_AND_ENDSCREEN"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->ATTACHMENT_AND_ENDSCREEN:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365637
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "SHOW_SPONSORSHIP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->SHOW_SPONSORSHIP:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365638
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "VIDEO_DR_STYLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365639
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const-string v1, "BUTTON_UNDER_DESC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_UNDER_DESC:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365640
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_ICON_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->HIDE_FROM_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->ATTACHMENT_AND_ENDSCREEN:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->SHOW_SPONSORSHIP:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_UNDER_DESC:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365641
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 1

    .prologue
    .line 365642
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 365643
    :goto_0
    return-object v0

    .line 365644
    :cond_1
    const-string v0, "BUTTON_WITH_TEXT_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365646
    :cond_2
    const-string v0, "BUTTON_WITH_ICON_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_ICON_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365648
    :cond_3
    const-string v0, "HIDE_FROM_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365649
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->HIDE_FROM_FEED:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365650
    :cond_4
    const-string v0, "ATTACHMENT_AND_ENDSCREEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365651
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->ATTACHMENT_AND_ENDSCREEN:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365652
    :cond_5
    const-string v0, "SHOW_SPONSORSHIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->SHOW_SPONSORSHIP:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365654
    :cond_6
    const-string v0, "VIDEO_DR_STYLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 365655
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365656
    :cond_7
    const-string v0, "BUTTON_UNDER_DESC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 365657
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_UNDER_DESC:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0

    .line 365658
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 1

    .prologue
    .line 365630
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 1

    .prologue
    .line 365631
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method
