.class public final enum Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum TRANSFER_FAILED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum TRANSFER_INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 537358
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537359
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "INITED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537360
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "TRANSFER_INITED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "TRANSFER_COMPLETED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "TRANSFER_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_FAILED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "CANCELED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    const-string v1, "EXPIRED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537366
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_FAILED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 537367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;
    .locals 1

    .prologue
    .line 537368
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 537369
    :goto_0
    return-object v0

    .line 537370
    :cond_1
    const-string v0, "INITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 537371
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537372
    :cond_2
    const-string v0, "DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 537373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537374
    :cond_3
    const-string v0, "TRANSFER_INITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 537375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_INITED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537376
    :cond_4
    const-string v0, "TRANSFER_COMPLETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 537377
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_COMPLETED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537378
    :cond_5
    const-string v0, "TRANSFER_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 537379
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->TRANSFER_FAILED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537380
    :cond_6
    const-string v0, "CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 537381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->CANCELED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537382
    :cond_7
    const-string v0, "EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 537383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->EXPIRED:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0

    .line 537384
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;
    .locals 1

    .prologue
    .line 537385
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;
    .locals 1

    .prologue
    .line 537386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    return-object v0
.end method
