.class public final enum Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum COMMENT:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum DEPRECATED_3:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum FRIEND_REQUEST_CONFIRM:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum FRIEND_REQUEST_DELETE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum LA_AUTHENTICATE_APPROVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum LA_AUTHENTICATE_DENY:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum PYMK_ADD_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum PYMK_REMOVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public static final enum VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 563333
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563334
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "LA_AUTHENTICATE_APPROVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_APPROVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563335
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "LA_AUTHENTICATE_DENY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_DENY:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563336
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "DEPRECATED_3"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->DEPRECATED_3:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563337
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "FRIEND_REQUEST_DELETE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_DELETE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563338
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "VIEW_PROFILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563339
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "PYMK_ADD_FRIEND"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_ADD_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563340
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "PYMK_REMOVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_REMOVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563341
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "FRIEND_REQUEST_CONFIRM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_CONFIRM:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563342
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "LIKE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    const-string v1, "COMMENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563344
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_APPROVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_DENY:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->DEPRECATED_3:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_DELETE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_ADD_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_REMOVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_CONFIRM:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 563332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
    .locals 1

    .prologue
    .line 563345
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 563346
    :goto_0
    return-object v0

    .line 563347
    :cond_1
    const-string v0, "LA_AUTHENTICATE_APPROVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 563348
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_APPROVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563349
    :cond_2
    const-string v0, "LA_AUTHENTICATE_DENY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 563350
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LA_AUTHENTICATE_DENY:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563351
    :cond_3
    const-string v0, "FRIEND_REQUEST_CONFIRM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 563352
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_CONFIRM:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563353
    :cond_4
    const-string v0, "FRIEND_REQUEST_DELETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 563354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->FRIEND_REQUEST_DELETE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563355
    :cond_5
    const-string v0, "VIEW_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 563356
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->VIEW_PROFILE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563357
    :cond_6
    const-string v0, "PYMK_ADD_FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 563358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_ADD_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563359
    :cond_7
    const-string v0, "PYMK_REMOVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 563360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->PYMK_REMOVE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563361
    :cond_8
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 563362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563363
    :cond_9
    const-string v0, "COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 563364
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0

    .line 563365
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
    .locals 1

    .prologue
    .line 563331
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
    .locals 1

    .prologue
    .line 563330
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    return-object v0
.end method
