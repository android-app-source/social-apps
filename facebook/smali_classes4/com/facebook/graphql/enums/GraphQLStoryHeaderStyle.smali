.class public final enum Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum CONNECTION_QUESTIONS:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum DAILY_DIALOGUE_LIGHTWEIGHT:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum EGO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum FUNDRAISER_UPSELL:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum GROUPS_MEMBER_BIO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum HAPPY_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum MY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum THROWBACK_SHARED_STORY_V2:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum TITLE_ONLY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 480160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "HAPPY_BIRTHDAY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->HAPPY_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "FACEBOOK_VOICE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "EGO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->EGO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480165
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "PAGE_LIKE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480166
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "CONNECTION_QUESTIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->CONNECTION_QUESTIONS:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480167
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "TITLE_ONLY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->TITLE_ONLY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480168
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "DAILY_DIALOGUE_LIGHTWEIGHT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->DAILY_DIALOGUE_LIGHTWEIGHT:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480169
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "THROWBACK_SHARED_STORY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480170
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "MY_ACTIVITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->MY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480171
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "LEARNING_MODULE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "THROWBACK_SHARED_STORY_V2"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY_V2:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "FUNDRAISER_UPSELL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FUNDRAISER_UPSELL:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const-string v1, "GROUPS_MEMBER_BIO"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->GROUPS_MEMBER_BIO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480175
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->HAPPY_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->EGO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->CONNECTION_QUESTIONS:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->TITLE_ONLY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->DAILY_DIALOGUE_LIGHTWEIGHT:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->MY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY_V2:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FUNDRAISER_UPSELL:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->GROUPS_MEMBER_BIO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 480176
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;
    .locals 1

    .prologue
    .line 480177
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 480178
    :goto_0
    return-object v0

    .line 480179
    :cond_1
    const-string v0, "FALLBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 480180
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480181
    :cond_2
    const-string v0, "HAPPY_BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 480182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->HAPPY_BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480183
    :cond_3
    const-string v0, "FACEBOOK_VOICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 480184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480185
    :cond_4
    const-string v0, "EGO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 480186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->EGO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480187
    :cond_5
    const-string v0, "PAGE_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 480188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480189
    :cond_6
    const-string v0, "CONNECTION_QUESTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 480190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->CONNECTION_QUESTIONS:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480191
    :cond_7
    const-string v0, "TITLE_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 480192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->TITLE_ONLY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480193
    :cond_8
    const-string v0, "DAILY_DIALOGUE_LIGHTWEIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 480194
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->DAILY_DIALOGUE_LIGHTWEIGHT:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480195
    :cond_9
    const-string v0, "THROWBACK_SHARED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 480196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480197
    :cond_a
    const-string v0, "THROWBACK_SHARED_STORY_V2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 480198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY_V2:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480199
    :cond_b
    const-string v0, "MY_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 480200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->MY_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto :goto_0

    .line 480201
    :cond_c
    const-string v0, "LEARNING_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 480202
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto/16 :goto_0

    .line 480203
    :cond_d
    const-string v0, "FUNDRAISER_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 480204
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FUNDRAISER_UPSELL:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto/16 :goto_0

    .line 480205
    :cond_e
    const-string v0, "GROUPS_MEMBER_BIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 480206
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->GROUPS_MEMBER_BIO:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto/16 :goto_0

    .line 480207
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;
    .locals 1

    .prologue
    .line 480208
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;
    .locals 1

    .prologue
    .line 480209
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    return-object v0
.end method
