.class public final enum Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum EXTERNAL_URLS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum JOBS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum LISTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum MESSAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum PAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum POSTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum PROFILES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "ARCHIVED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "BOOKS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "EVENTS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "LINKS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "MOVIES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "MUSIC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365939
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "PLACES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365940
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "TV_SHOWS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365941
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "VIDEOS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365942
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "PAGES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365943
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "PHOTOS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365944
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "PRODUCTS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365945
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "PROFILES"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PROFILES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365946
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "MESSAGES"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MESSAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "FUNDRAISERS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "MEDIA"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "POSTS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->POSTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "LISTS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LISTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "EXTERNAL_URLS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EXTERNAL_URLS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const-string v1, "JOBS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->JOBS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365953
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PROFILES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MESSAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->POSTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LISTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EXTERNAL_URLS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->JOBS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 365930
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 1

    .prologue
    .line 365883
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 365884
    :goto_0
    return-object v0

    .line 365885
    :cond_1
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365886
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365887
    :cond_2
    const-string v0, "ARCHIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365888
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365889
    :cond_3
    const-string v0, "BOOKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365890
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365891
    :cond_4
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365892
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365893
    :cond_5
    const-string v0, "LINKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365894
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365895
    :cond_6
    const-string v0, "MOVIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 365896
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365897
    :cond_7
    const-string v0, "MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 365898
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365899
    :cond_8
    const-string v0, "PLACES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 365900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PLACES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365901
    :cond_9
    const-string v0, "TV_SHOWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 365902
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365903
    :cond_a
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 365904
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365905
    :cond_b
    const-string v0, "PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 365906
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_0

    .line 365907
    :cond_c
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 365908
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365909
    :cond_d
    const-string v0, "PRODUCTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 365910
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PRODUCTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365911
    :cond_e
    const-string v0, "PROFILES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 365912
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->PROFILES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365913
    :cond_f
    const-string v0, "MESSAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 365914
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MESSAGES:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365915
    :cond_10
    const-string v0, "FUNDRAISERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 365916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365917
    :cond_11
    const-string v0, "JOBS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 365918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->JOBS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365919
    :cond_12
    const-string v0, "MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 365920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365921
    :cond_13
    const-string v0, "POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 365922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->POSTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365923
    :cond_14
    const-string v0, "EXTERNAL_URLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 365924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->EXTERNAL_URLS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365925
    :cond_15
    const-string v0, "LISTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 365926
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->LISTS:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0

    .line 365927
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 1

    .prologue
    .line 365929
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 1

    .prologue
    .line 365928
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-object v0
.end method
