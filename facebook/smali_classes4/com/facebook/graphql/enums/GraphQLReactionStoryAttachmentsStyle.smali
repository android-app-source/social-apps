.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum ADMINED_PAGES_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum APP_AD_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum APP_AD_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum BOOSTED_LOCAL_AWARENESS_TIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum CITY_GUIDE_PLACE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum CREATE_OWNED_PAGE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum CRITIC_REVIEW_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum DISCOVERY_PLACE_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum EVENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum EVENT_CARD_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum EVENT_CARD_LARGE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum EVENT_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum EVENT_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FEED_STORY_ATTACHMENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FEED_STORY_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FEELING_ICON_STRIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum FUNDRAISER_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum GROUP_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum HEADLINE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum IMAGE_TEXT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum NEARBY_FRIENDS_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum NEXT_PLACE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum NO_ACTOR_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum OG_OBJECT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_ATTRIBUTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_COMMERCE_UNITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_LIKES_AND_VISITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_PERMALINK_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_POST_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_PRODUCT_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_PROMOTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_SERVICE_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTOS_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTOS_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTO_COLLAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PHOTO_WITH_CAPTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PLACE_QUESTION_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PLACE_QUESTION_VERTICAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PLACE_SURVEY_THANK_YOU:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PROFILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PROFILE_MULTILINE_SUBTITLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum PROFILE_TWO_LINE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum RATING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum RATINGS_INCL_DRAFTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum RESIDENCE_MIGRATION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum REVIEW_NEEDY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SIMPLE_LEFT_RIGHT_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SIMPLE_TEXT_WITHOUT_LABEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SINGLE_LARGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SONG_PLAYING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SPORTS_GAME_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SPORTS_GAME_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum SPOTLIGHT_STORY_PREVIEW:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum TODAY_GENERIC_MLE_IMAGE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum TOPIC_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public static final enum VIDEO_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 534328
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534329
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PROFILE_BLOCKS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534330
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTOS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534331
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FEED_STORY_PROFILE_BLOCKS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534332
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SINGLE_LARGE_PHOTO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SINGLE_LARGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534333
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTO_COLLAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_COLLAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534334
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FACEPILE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534335
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "TOPIC_LIST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TOPIC_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534336
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "RATING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534337
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_PRODUCT_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PRODUCT_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534338
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "EVENT_BLOCKS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534339
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FEED_STORY_SINGLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534340
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PROFILE_TWO_LINE_HORIZONTAL_SCROLL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_TWO_LINE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534341
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "EVENT_HORIZONTAL_SCROLL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534342
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "GROUP_HORIZONTAL_SCROLL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->GROUP_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "DISCOVERY_PLACE_PROFILE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->DISCOVERY_PLACE_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534344
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "VIDEO_HORIZONTAL_SCROLL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEO_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534345
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "NEARBY_FRIENDS_FACEPILE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEARBY_FRIENDS_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534346
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PLACE_QUESTION_HORIZONTAL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534347
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PLACE_QUESTION_VERTICAL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_VERTICAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534348
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FACEPILE_HORIZONTAL_SCROLL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534349
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PLACE_SURVEY_THANK_YOU"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_SURVEY_THANK_YOU:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "TODAY_GENERIC_MLE_IMAGE_BLOCKS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TODAY_GENERIC_MLE_IMAGE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "RESIDENCE_MIGRATION"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RESIDENCE_MIGRATION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534352
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_LIKES_AND_VISITS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_LIKES_AND_VISITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534353
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SIMPLE_TEXT"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534354
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SIMPLE_LEFT_RIGHT_TEXT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_LEFT_RIGHT_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534355
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "CRITIC_REVIEW_HORIZONTAL_SCROLL"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CRITIC_REVIEW_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534356
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "EVENT_PROFILE_BLOCKS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534357
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SIMPLE_TEXT_WITHOUT_LABEL"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT_WITHOUT_LABEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534358
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "IMAGE_TEXT_BLOCK"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->IMAGE_TEXT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534359
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_SERVICE_LIST"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_SERVICE_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534360
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "EVENT_CARD_LARGE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTOS_LARGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "REVIEW_NEEDY"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->REVIEW_NEEDY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PROFILE_MULTILINE_SUBTITLE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_MULTILINE_SUBTITLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "ADMINED_PAGES_LIST"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->ADMINED_PAGES_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "EVENT_CARD_LARGE_HORIZONTAL_SCROLL"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534366
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_ATTRIBUTION_FOOTER"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_ATTRIBUTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534367
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_COMMERCE_UNITS"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_COMMERCE_UNITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534368
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTOS_WITH_ATTRIBUTION"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534369
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_WELCOME_HOME"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534370
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "VIDEOS"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534372
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_POST_STORY"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534373
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "RATINGS_INCL_DRAFTS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATINGS_INCL_DRAFTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534374
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "CREATE_OWNED_PAGE_UPSELL"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CREATE_OWNED_PAGE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTOS_FULL_WIDTH_COUNTER"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SPOTLIGHT_STORY_PREVIEW"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPOTLIGHT_STORY_PREVIEW:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FUNDRAISER_LIST"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FUNDRAISER_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_INFO_CARD_PLACEHOLDER"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "BOOSTED_LOCAL_AWARENESS_TIP"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->BOOSTED_LOCAL_AWARENESS_TIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_PERMALINK_STORY"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PERMALINK_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FEED_STORY_ATTACHMENT_BLOCKS"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_ATTACHMENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PROFILE_HORIZONTAL_SCROLL"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "APP_AD_BLOCKS"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534385
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "NO_ACTOR_FEED_STORY"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NO_ACTOR_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PHOTO_WITH_CAPTION"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_WITH_CAPTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534387
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "FEELING_ICON_STRIP"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEELING_ICON_STRIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SPORTS_GAME_SINGLE"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534389
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SPORTS_GAME_LIST"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534390
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "APP_AD_SINGLE"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534391
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "HEADLINE_STORY"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->HEADLINE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "NOTIFICATIONS_LIST"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "OG_OBJECT_BLOCKS"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->OG_OBJECT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "SONG_PLAYING"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SONG_PLAYING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "NEXT_PLACE_HORIZONTAL_SCROLL"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEXT_PLACE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_PROMOTION"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PROMOTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "CITY_GUIDE_PLACE_BLOCK"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CITY_GUIDE_PLACE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534398
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "PAGE_POST_HORIZONTAL_SCROLL"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534399
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    const-string v1, "LIVE_VIDEOS"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534400
    const/16 v0, 0x48

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SINGLE_LARGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_COLLAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TOPIC_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PRODUCT_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_TWO_LINE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->GROUP_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->DISCOVERY_PLACE_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEO_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEARBY_FRIENDS_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_VERTICAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_SURVEY_THANK_YOU:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TODAY_GENERIC_MLE_IMAGE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RESIDENCE_MIGRATION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_LIKES_AND_VISITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_LEFT_RIGHT_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CRITIC_REVIEW_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT_WITHOUT_LABEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->IMAGE_TEXT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_SERVICE_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->REVIEW_NEEDY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_MULTILINE_SUBTITLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->ADMINED_PAGES_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_ATTRIBUTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_COMMERCE_UNITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATINGS_INCL_DRAFTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CREATE_OWNED_PAGE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPOTLIGHT_STORY_PREVIEW:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FUNDRAISER_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->BOOSTED_LOCAL_AWARENESS_TIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PERMALINK_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_ATTACHMENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NO_ACTOR_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_WITH_CAPTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEELING_ICON_STRIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->HEADLINE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->OG_OBJECT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SONG_PLAYING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEXT_PLACE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PROMOTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CITY_GUIDE_PLACE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 534401
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
    .locals 2

    .prologue
    .line 534402
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 534403
    :goto_0
    return-object v0

    .line 534404
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 534405
    packed-switch v0, :pswitch_data_0

    .line 534406
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534407
    :pswitch_1
    const-string v0, "NEARBY_FRIENDS_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534408
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEARBY_FRIENDS_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534409
    :cond_2
    const-string v0, "SPORTS_GAME_SINGLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 534410
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534411
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534412
    :pswitch_2
    const-string v0, "FUNDRAISER_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 534413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FUNDRAISER_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534414
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534415
    :pswitch_3
    const-string v0, "FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 534416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FRIEND_YOU_MAY_INVITE_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534417
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534418
    :pswitch_4
    const-string v0, "PROFILE_MULTILINE_SUBTITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 534419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_MULTILINE_SUBTITLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534420
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534421
    :pswitch_5
    const-string v0, "FEELING_ICON_STRIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 534422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEELING_ICON_STRIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534423
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto :goto_0

    .line 534424
    :pswitch_6
    const-string v0, "TOPIC_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 534425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TOPIC_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534426
    :cond_8
    const-string v0, "SIMPLE_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 534427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534428
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534429
    :pswitch_7
    const-string v0, "NOTIFICATIONS_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 534430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534431
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534432
    :pswitch_8
    const-string v0, "PAGE_PRODUCT_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 534433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PRODUCT_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534434
    :cond_b
    const-string v0, "PAGE_SERVICE_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 534435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_SERVICE_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534436
    :cond_c
    const-string v0, "BOOSTED_LOCAL_AWARENESS_TIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 534437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->BOOSTED_LOCAL_AWARENESS_TIP:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534438
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534439
    :pswitch_9
    const-string v0, "CREATE_OWNED_PAGE_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 534440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CREATE_OWNED_PAGE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534441
    :cond_e
    const-string v0, "SPORTS_GAME_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 534442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPORTS_GAME_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534443
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534444
    :pswitch_a
    const-string v0, "EVENT_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 534445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534446
    :cond_10
    const-string v0, "PAGE_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 534447
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PROMOTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534448
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534449
    :pswitch_b
    const-string v0, "GROUP_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 534450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->GROUP_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534451
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534452
    :pswitch_c
    const-string v0, "FACEPILE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 534453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534454
    :cond_13
    const-string v0, "PHOTO_WITH_CAPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 534455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_WITH_CAPTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534456
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534457
    :pswitch_d
    const-string v0, "SIMPLE_LEFT_RIGHT_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 534458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_LEFT_RIGHT_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534459
    :cond_15
    const-string v0, "PAGE_ATTRIBUTION_FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 534460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_ATTRIBUTION_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534461
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534462
    :pswitch_e
    const-string v0, "CRITIC_REVIEW_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 534463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CRITIC_REVIEW_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534464
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534465
    :pswitch_f
    const-string v0, "RESIDENCE_MIGRATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 534466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RESIDENCE_MIGRATION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534467
    :cond_18
    const-string v0, "PHOTOS_FULL_WIDTH_COUNTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 534468
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_FULL_WIDTH_COUNTER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534469
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534470
    :pswitch_10
    const-string v0, "PAGE_INFO_CARD_PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 534471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_INFO_CARD_PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534472
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534473
    :pswitch_11
    const-string v0, "PHOTOS_WITH_ATTRIBUTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 534474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534475
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534476
    :pswitch_12
    const-string v0, "PLACE_QUESTION_VERTICAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 534477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_VERTICAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534478
    :cond_1c
    const-string v0, "EVENT_CARD_LARGE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 534479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534480
    :cond_1d
    const-string v0, "APP_AD_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 534481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534482
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534483
    :pswitch_13
    const-string v0, "PLACE_QUESTION_HORIZONTAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 534484
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_QUESTION_HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534485
    :cond_1f
    const-string v0, "PROFILE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 534486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534487
    :cond_20
    const-string v0, "HEADLINE_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 534488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->HEADLINE_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534489
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534490
    :pswitch_14
    const-string v0, "EVENT_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 534491
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534492
    :cond_22
    const-string v0, "NEXT_PLACE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 534493
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NEXT_PLACE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534494
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534495
    :pswitch_15
    const-string v0, "PAGE_POST_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 534496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534497
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534498
    :pswitch_16
    const-string v0, "SIMPLE_TEXT_WITHOUT_LABEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 534499
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SIMPLE_TEXT_WITHOUT_LABEL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534500
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534501
    :pswitch_17
    const-string v0, "VIDEO_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 534502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEO_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534503
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534504
    :pswitch_18
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 534505
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534506
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534507
    :pswitch_19
    const-string v0, "LIVE_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 534508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534509
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534510
    :pswitch_1a
    const-string v0, "REVIEW_NEEDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 534511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->REVIEW_NEEDY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534512
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534513
    :pswitch_1b
    const-string v0, "PROFILE_TWO_LINE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 534514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_TWO_LINE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534515
    :cond_2a
    const-string v0, "EVENT_PROFILE_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 534516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534517
    :cond_2b
    const-string v0, "PAGE_POST_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 534518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534519
    :cond_2c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534520
    :pswitch_1c
    const-string v0, "PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 534521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_YOU_MAY_LIKE_HORIZONTAL_SCROLL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534522
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534523
    :pswitch_1d
    const-string v0, "NO_ACTOR_FEED_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 534524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->NO_ACTOR_FEED_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534525
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534526
    :pswitch_1e
    const-string v0, "FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 534527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534528
    :cond_2f
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 534529
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534530
    :cond_30
    const-string v0, "APP_AD_SINGLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 534531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->APP_AD_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534532
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534533
    :pswitch_1f
    const-string v0, "PROFILE_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 534534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534535
    :cond_32
    const-string v0, "PAGE_PERMALINK_STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 534536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_PERMALINK_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534537
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534538
    :pswitch_20
    const-string v0, "FEED_STORY_PROFILE_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 534539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534540
    :cond_34
    const-string v0, "OG_OBJECT_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 534541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->OG_OBJECT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534542
    :cond_35
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534543
    :pswitch_21
    const-string v0, "IMAGE_TEXT_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 534544
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->IMAGE_TEXT_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534545
    :cond_36
    const-string v0, "CITY_GUIDE_PLACE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 534546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CITY_GUIDE_PLACE_BLOCK:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534547
    :cond_37
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534548
    :pswitch_22
    const-string v0, "FEED_STORY_ATTACHMENT_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 534549
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_ATTACHMENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534550
    :cond_38
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534551
    :pswitch_23
    const-string v0, "EVENT_CARD_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 534552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_CARD_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534553
    :cond_39
    const-string v0, "PAGE_COMMERCE_UNITS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 534554
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_COMMERCE_UNITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534555
    :cond_3a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534556
    :pswitch_24
    const-string v0, "RATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 534557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534558
    :cond_3b
    const-string v0, "PLACE_SURVEY_THANK_YOU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 534559
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PLACE_SURVEY_THANK_YOU:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534560
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534561
    :pswitch_25
    const-string v0, "FEED_STORY_SINGLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 534562
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534563
    :cond_3d
    const-string v0, "PAGE_LIKES_AND_VISITS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 534564
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_LIKES_AND_VISITS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534565
    :cond_3e
    const-string v0, "RATINGS_INCL_DRAFTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 534566
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->RATINGS_INCL_DRAFTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534567
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534568
    :pswitch_26
    const-string v0, "SPOTLIGHT_STORY_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 534569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SPOTLIGHT_STORY_PREVIEW:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534570
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534571
    :pswitch_27
    const-string v0, "SINGLE_LARGE_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 534572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SINGLE_LARGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534573
    :cond_41
    const-string v0, "DISCOVERY_PLACE_PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 534574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->DISCOVERY_PLACE_PROFILE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534575
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534576
    :pswitch_28
    const-string v0, "PHOTOS_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 534577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534578
    :cond_43
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534579
    :pswitch_29
    const-string v0, "PHOTO_COLLAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 534580
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTO_COLLAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534581
    :cond_44
    const-string v0, "SONG_PLAYING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 534582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SONG_PLAYING:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534583
    :cond_45
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534584
    :pswitch_2a
    const-string v0, "PAGE_WELCOME_HOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 534585
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_WELCOME_HOME:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534586
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534587
    :pswitch_2b
    const-string v0, "TODAY_GENERIC_MLE_IMAGE_BLOCKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 534588
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TODAY_GENERIC_MLE_IMAGE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534589
    :cond_47
    const-string v0, "ADMINED_PAGES_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 534590
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->ADMINED_PAGES_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    .line 534591
    :cond_48
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2a
        :pswitch_0
        :pswitch_0
        :pswitch_2b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
    .locals 1

    .prologue
    .line 534592
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
    .locals 1

    .prologue
    .line 534593
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    return-object v0
.end method
