.class public final enum Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public static final enum COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public static final enum PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public static final enum PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 368848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const-string v1, "PUBLIC_TYPE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const-string v1, "PRIVATE_TYPE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const-string v1, "COMMUNITY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368853
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368854
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 1

    .prologue
    .line 368855
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 368856
    :goto_0
    return-object v0

    .line 368857
    :cond_1
    const-string v0, "PRIVATE_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0

    .line 368859
    :cond_2
    const-string v0, "PUBLIC_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0

    .line 368861
    :cond_3
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368862
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0

    .line 368863
    :cond_4
    const-string v0, "COMMUNITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 368864
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0

    .line 368865
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 1

    .prologue
    .line 368866
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 1

    .prologue
    .line 368867
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method
