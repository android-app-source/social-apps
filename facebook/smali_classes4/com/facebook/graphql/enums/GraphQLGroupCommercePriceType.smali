.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum FIRM:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum FIXED:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum FREE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum TRADE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 501069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "FIXED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIXED:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "NEGOTIABLE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "TRADE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->TRADE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "FREE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FREE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    const-string v1, "FIRM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIRM:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501075
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIXED:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->TRADE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FREE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIRM:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 501068
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
    .locals 1

    .prologue
    .line 501076
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 501077
    :goto_0
    return-object v0

    .line 501078
    :cond_1
    const-string v0, "FIXED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501079
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIXED:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0

    .line 501080
    :cond_2
    const-string v0, "FIRM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 501081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FIRM:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0

    .line 501082
    :cond_3
    const-string v0, "TRADE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 501083
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->TRADE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0

    .line 501084
    :cond_4
    const-string v0, "NEGOTIABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 501085
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0

    .line 501086
    :cond_5
    const-string v0, "FREE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 501087
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->FREE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0

    .line 501088
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
    .locals 1

    .prologue
    .line 501067
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;
    .locals 1

    .prologue
    .line 501066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    return-object v0
.end method
