.class public final enum Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum CANNOT_WAVE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public static final enum WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 493022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "CANNOT_WAVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->CANNOT_WAVE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "NO_WAVE_SENT_OR_RECEIVED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "WAVE_SENT"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "WAVE_RECEIVED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const-string v1, "WAVE_SENT_AND_RECEIVED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493028
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->CANNOT_WAVE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 493044
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;
    .locals 1

    .prologue
    .line 493031
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 493032
    :goto_0
    return-object v0

    .line 493033
    :cond_1
    const-string v0, "CANNOT_WAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 493034
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->CANNOT_WAVE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0

    .line 493035
    :cond_2
    const-string v0, "NO_WAVE_SENT_OR_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493036
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->NO_WAVE_SENT_OR_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0

    .line 493037
    :cond_3
    const-string v0, "WAVE_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 493038
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0

    .line 493039
    :cond_4
    const-string v0, "WAVE_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 493040
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0

    .line 493041
    :cond_5
    const-string v0, "WAVE_SENT_AND_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 493042
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->WAVE_SENT_AND_RECEIVED:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0

    .line 493043
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;
    .locals 1

    .prologue
    .line 493030
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;
    .locals 1

    .prologue
    .line 493029
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    return-object v0
.end method
