.class public final enum Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum LIKED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum SAVED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 368929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "HOST"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "NOT_GOING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "LIKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "SAVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SAVED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "SUBSCRIBED_ADMIN_CALENDAR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368939
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SAVED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368928
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 1

    .prologue
    .line 368905
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 368906
    :goto_0
    return-object v0

    .line 368907
    :cond_1
    const-string v0, "HOST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368908
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368909
    :cond_2
    const-string v0, "GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368910
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368911
    :cond_3
    const-string v0, "INVITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368912
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368913
    :cond_4
    const-string v0, "MAYBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 368914
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368915
    :cond_5
    const-string v0, "NOT_GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 368916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368917
    :cond_6
    const-string v0, "LIKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 368918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->LIKED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368919
    :cond_7
    const-string v0, "SAVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 368920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SAVED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368921
    :cond_8
    const-string v0, "SUBSCRIBED_ADMIN_CALENDAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 368922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368923
    :cond_9
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 368924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 368925
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 1

    .prologue
    .line 368927
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 1

    .prologue
    .line 368926
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method
