.class public final enum Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum DEPRECATED_1:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum DEPRECATED_7:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public static final enum VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 466057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "DEPRECATED_1"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->DEPRECATED_1:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "PREVIEW"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466060
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466061
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "LIVE_STOPPED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466062
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "VOD_READY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "SEAL_STARTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466064
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "DEPRECATED_7"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->DEPRECATED_7:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466065
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "SCHEDULED_PREVIEW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "SCHEDULED_LIVE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "SCHEDULED_EXPIRED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const-string v1, "SCHEDULED_CANCELED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466069
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->DEPRECATED_1:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->DEPRECATED_7:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 466070
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 466071
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 466072
    :goto_0
    return-object v0

    .line 466073
    :cond_1
    const-string v0, "PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466074
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466075
    :cond_2
    const-string v0, "LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 466076
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466077
    :cond_3
    const-string v0, "LIVE_STOPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 466078
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466079
    :cond_4
    const-string v0, "VOD_READY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 466080
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466081
    :cond_5
    const-string v0, "SEAL_STARTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 466082
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466083
    :cond_6
    const-string v0, "SCHEDULED_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 466084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466085
    :cond_7
    const-string v0, "SCHEDULED_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 466086
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466087
    :cond_8
    const-string v0, "SCHEDULED_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 466088
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466089
    :cond_9
    const-string v0, "SCHEDULED_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 466090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0

    .line 466091
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 466092
    const-class v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 466093
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method
