.class public final enum Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum MARK_AS_READ:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum MARK_AS_UNREAD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum MODSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_DEVICE_PUSH_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_EVENT_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_GROUP_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_GROUP_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_SOUNDS_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum OPEN_SUB_PAGE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum REPORT_BUG:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum SERVER_ACTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum SHOW_MORE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum TURN_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

.field public static final enum UNSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 531468
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531469
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531470
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "UNSUB"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531471
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "MODSUB"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MODSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531472
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "SHOW_MORE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SHOW_MORE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531473
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "MARK_AS_READ"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_READ:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531474
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "MARK_AS_UNREAD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_UNREAD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531475
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "SETTINGS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_SUB_PAGE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SUB_PAGE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531477
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_ACTION_SHEET"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531478
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "SERVER_ACTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SERVER_ACTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531479
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "TURN_OFF"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->TURN_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531480
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "REPORT_BUG"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->REPORT_BUG:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531481
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_GROUP_SETTING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531482
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_EVENT_SETTING"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531483
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_GROUP_DASHBOARD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531484
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_EVENT_DASHBOARD"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531485
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_DEVICE_PUSH_SETTINGS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_DEVICE_PUSH_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531486
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "OPEN_SOUNDS_SETTING"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SOUNDS_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531487
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "SAVE_ITEM"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531488
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const-string v1, "UNSAVE_ITEM"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531489
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MODSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SHOW_MORE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_READ:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_UNREAD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SUB_PAGE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SERVER_ACTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->TURN_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->REPORT_BUG:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_DEVICE_PUSH_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SOUNDS_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 531467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
    .locals 1

    .prologue
    .line 531424
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 531425
    :goto_0
    return-object v0

    .line 531426
    :cond_1
    const-string v0, "HIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 531427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531428
    :cond_2
    const-string v0, "UNSUB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 531429
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531430
    :cond_3
    const-string v0, "MODSUB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 531431
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MODSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531432
    :cond_4
    const-string v0, "SHOW_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 531433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SHOW_MORE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531434
    :cond_5
    const-string v0, "MARK_AS_READ"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 531435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_READ:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531436
    :cond_6
    const-string v0, "MARK_AS_UNREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 531437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->MARK_AS_UNREAD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531438
    :cond_7
    const-string v0, "SETTINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 531439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531440
    :cond_8
    const-string v0, "OPEN_SUB_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 531441
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SUB_PAGE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531442
    :cond_9
    const-string v0, "OPEN_ACTION_SHEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 531443
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531444
    :cond_a
    const-string v0, "SERVER_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 531445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SERVER_ACTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531446
    :cond_b
    const-string v0, "TURN_OFF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 531447
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->TURN_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto :goto_0

    .line 531448
    :cond_c
    const-string v0, "REPORT_BUG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 531449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->REPORT_BUG:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531450
    :cond_d
    const-string v0, "OPEN_GROUP_SETTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 531451
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531452
    :cond_e
    const-string v0, "OPEN_EVENT_SETTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 531453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531454
    :cond_f
    const-string v0, "OPEN_GROUP_DASHBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 531455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_GROUP_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531456
    :cond_10
    const-string v0, "OPEN_EVENT_DASHBOARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 531457
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_EVENT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531458
    :cond_11
    const-string v0, "OPEN_DEVICE_PUSH_SETTINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 531459
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_DEVICE_PUSH_SETTINGS:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531460
    :cond_12
    const-string v0, "OPEN_SOUNDS_SETTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 531461
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_SOUNDS_SETTING:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531462
    :cond_13
    const-string v0, "SAVE_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 531463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531464
    :cond_14
    const-string v0, "UNSAVE_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 531465
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0

    .line 531466
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
    .locals 1

    .prologue
    .line 531422
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
    .locals 1

    .prologue
    .line 531423
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    return-object v0
.end method
