.class public final enum Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum ANGER:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum CONFUSED:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum DOROTHY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum FLAME:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum HAHA:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum LOVE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum OUCH:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum PLANE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum SELFIE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum SORRY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum TOTO:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum WOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

.field public static final enum YAY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 522364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522366
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522367
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "LOVE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LOVE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522368
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "WOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->WOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522369
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "HAHA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->HAHA:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522370
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "YAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->YAY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "OUCH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->OUCH:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522372
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "SORRY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SORRY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522373
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "ANGER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->ANGER:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522374
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "CONFUSED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->CONFUSED:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "DOROTHY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->DOROTHY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "TOTO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->TOTO:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "SELFIE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SELFIE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "FLAME"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->FLAME:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const-string v1, "PLANE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->PLANE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522380
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LOVE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->WOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->HAHA:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->YAY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->OUCH:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SORRY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->ANGER:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->CONFUSED:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->DOROTHY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->TOTO:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SELFIE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->FLAME:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->PLANE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 522416
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 1

    .prologue
    .line 522383
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 522384
    :goto_0
    return-object v0

    .line 522385
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 522386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->NONE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522387
    :cond_2
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 522388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522389
    :cond_3
    const-string v0, "LOVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 522390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->LOVE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522391
    :cond_4
    const-string v0, "WOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 522392
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->WOW:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522393
    :cond_5
    const-string v0, "HAHA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 522394
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->HAHA:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522395
    :cond_6
    const-string v0, "YAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 522396
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->YAY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522397
    :cond_7
    const-string v0, "OUCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 522398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->OUCH:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522399
    :cond_8
    const-string v0, "SORRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 522400
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SORRY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522401
    :cond_9
    const-string v0, "ANGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 522402
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->ANGER:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522403
    :cond_a
    const-string v0, "CONFUSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 522404
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->CONFUSED:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522405
    :cond_b
    const-string v0, "DOROTHY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 522406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->DOROTHY:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto :goto_0

    .line 522407
    :cond_c
    const-string v0, "TOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 522408
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->TOTO:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto/16 :goto_0

    .line 522409
    :cond_d
    const-string v0, "SELFIE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 522410
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->SELFIE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto/16 :goto_0

    .line 522411
    :cond_e
    const-string v0, "FLAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 522412
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->FLAME:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto/16 :goto_0

    .line 522413
    :cond_f
    const-string v0, "PLANE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 522414
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->PLANE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto/16 :goto_0

    .line 522415
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 1

    .prologue
    .line 522382
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 1

    .prologue
    .line 522381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-object v0
.end method
