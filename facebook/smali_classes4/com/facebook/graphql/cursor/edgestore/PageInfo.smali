.class public final Lcom/facebook/graphql/cursor/edgestore/PageInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public final f:Z

.field public final g:I

.field public final h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 464644
    new-instance v0, LX/2nV;

    invoke-direct {v0}, LX/2nV;-><init>()V

    sput-object v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 464645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464646
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    .line 464647
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    .line 464648
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    .line 464649
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    .line 464650
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    .line 464651
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    .line 464652
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->g:I

    .line 464653
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    .line 464654
    return-void

    :cond_0
    move v0, v2

    .line 464655
    goto :goto_0

    :cond_1
    move v1, v2

    .line 464656
    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIJ)V
    .locals 4

    .prologue
    const/16 v3, 0x18

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 464657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464658
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 464659
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 464660
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 464661
    if-ltz p7, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 464662
    iput-object p1, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    .line 464663
    iput-object p2, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    .line 464664
    iput-object p3, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    .line 464665
    iput-object p4, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    .line 464666
    iput-boolean p5, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    .line 464667
    iput-boolean p6, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    .line 464668
    iput p7, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->g:I

    .line 464669
    iput-wide p8, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    .line 464670
    return-void

    :cond_0
    move v0, v2

    .line 464671
    goto :goto_0

    :cond_1
    move v0, v2

    .line 464672
    goto :goto_1

    :cond_2
    move v0, v2

    .line 464673
    goto :goto_2

    :cond_3
    move v1, v2

    .line 464674
    goto :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 10

    .prologue
    .line 464675
    new-instance v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIJ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 10

    .prologue
    .line 464676
    new-instance v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v9}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIJ)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 464677
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 464678
    iget-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 464679
    iget-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 464680
    iget-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 464681
    iget-object v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 464682
    iget-boolean v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 464683
    iget-boolean v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 464684
    iget v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 464685
    iget-wide v0, p0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 464686
    return-void

    :cond_0
    move v0, v2

    .line 464687
    goto :goto_0

    :cond_1
    move v1, v2

    .line 464688
    goto :goto_1
.end method
