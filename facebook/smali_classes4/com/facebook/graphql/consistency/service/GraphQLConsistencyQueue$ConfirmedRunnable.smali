.class public final Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0tA;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:LX/3Bq;


# direct methods
.method public constructor <init>(LX/0tA;Ljava/lang/String;Ljava/lang/String;LX/3Bq;)V
    .locals 0

    .prologue
    .line 534719
    iput-object p1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534720
    iput-object p2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    .line 534721
    iput-object p3, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->c:Ljava/lang/String;

    .line 534722
    iput-object p4, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->d:LX/3Bq;

    .line 534723
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const v6, 0x950002

    .line 534724
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 534725
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v1, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 534726
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 534727
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 534728
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Bq;

    .line 534729
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 534730
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 534731
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->d:LX/3Bq;

    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v1

    .line 534732
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-interface {v0, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534733
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    const-string v4, "visitor_name"

    iget-object v5, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->d:LX/3Bq;

    .line 534734
    invoke-interface {v5}, LX/3Bq;->b()Ljava/lang/String;

    move-result-object v7

    move-object v5, v7

    .line 534735
    invoke-interface {v0, v6, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 534736
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    const v3, 0x950001

    iget-object v4, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v0, v3, v4, v1}, LX/0tA;->a$redex0(LX/0tA;IILjava/util/Collection;)V

    .line 534737
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sk;

    .line 534738
    iget-object v4, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->d:LX/3Bq;

    iget-object v5, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v2, v5}, LX/0sk;->a(Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V

    goto :goto_1

    .line 534739
    :cond_2
    iget-object v0, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->a:LX/0tA;

    iget-object v0, v0, LX/0tA;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, Lcom/facebook/graphql/consistency/service/GraphQLConsistencyQueue$ConfirmedRunnable;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 534740
    return-void
.end method
