.class public final Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:D

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368674
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368766
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368763
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368764
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o:LX/0x2;

    .line 368765
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368760
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368761
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->g:Ljava/lang/String;

    .line 368762
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 368757
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o:LX/0x2;

    if-nez v0, :cond_0

    .line 368758
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o:LX/0x2;

    .line 368759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 368734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368735
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 368737
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 368738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 368739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 368740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 368741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 368742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 368743
    const/16 v5, 0xa

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 368744
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 368745
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 368746
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368747
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 368748
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 368749
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 368750
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 368751
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 368752
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 368753
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 368754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368755
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 368756
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368706
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368707
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 368709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368711
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 368712
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368713
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 368714
    if-eqz v2, :cond_1

    .line 368715
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368716
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->f:Ljava/util/List;

    move-object v1, v0

    .line 368717
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 368718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368719
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 368720
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368721
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368722
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 368723
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 368724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 368725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368726
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLUser;

    .line 368727
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 368728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368729
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 368730
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 368731
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368732
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368733
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368704
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 368705
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 368700
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 368701
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k:D

    .line 368702
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368697
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368698
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n:Ljava/lang/String;

    .line 368699
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368696
    const v0, -0x2c7d6158

    return v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368693
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368694
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->f:Ljava/util/List;

    .line 368695
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 368690
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->h:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368691
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->h:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->h:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    .line 368692
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->h:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368687
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368688
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368689
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368684
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368685
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLUser;

    .line 368686
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 368681
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 368682
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 368683
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k:D

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l:Ljava/lang/String;

    .line 368680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368675
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368676
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368677
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
