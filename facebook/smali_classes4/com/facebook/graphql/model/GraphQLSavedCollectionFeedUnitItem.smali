.class public final Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16h;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 441232
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 441271
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 441268
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 441269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    .line 441270
    return-void
.end method

.method public constructor <init>(LX/4Yh;)V
    .locals 1

    .prologue
    .line 441257
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 441258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    .line 441259
    iget-object v0, p1, LX/4Yh;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    .line 441260
    iget-object v0, p1, LX/4Yh;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441261
    iget-object v0, p1, LX/4Yh;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->g:Ljava/lang/String;

    .line 441262
    iget-object v0, p1, LX/4Yh;->e:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 441263
    iget-object v0, p1, LX/4Yh;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441264
    iget-object v0, p1, LX/4Yh;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441265
    iget-object v0, p1, LX/4Yh;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k:Ljava/lang/String;

    .line 441266
    iget-object v0, p1, LX/4Yh;->i:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    .line 441267
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 441254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    if-nez v0, :cond_0

    .line 441255
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    .line 441256
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 441236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 441237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 441238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 441239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 441240
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 441241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 441242
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 441243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 441244
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 441245
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 441246
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 441247
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 441248
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 441249
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 441250
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 441251
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 441252
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 441253
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 441233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    .line 441235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 441272
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 441273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 441274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 441275
    if-eqz v1, :cond_5

    .line 441276
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 441277
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->e:Ljava/util/List;

    move-object v1, v0

    .line 441278
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 441279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 441281
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 441282
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441283
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 441284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 441285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 441286
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 441287
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 441288
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 441289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 441291
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 441292
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441293
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 441294
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 441296
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 441297
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441298
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 441299
    if-nez v1, :cond_4

    :goto_1
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441213
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441214
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k:Ljava/lang/String;

    .line 441215
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 441216
    const v0, 0x190f8b7a

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441217
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441218
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441219
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->g:Ljava/lang/String;

    .line 441222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 441225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441229
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441230
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 441231
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
