.class public final Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLActivityTemplateToken$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLActivityTemplateToken$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371151
    const-class v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371152
    const-class v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 371132
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371133
    return-void
.end method

.method public constructor <init>(LX/4Vm;)V
    .locals 1

    .prologue
    .line 371147
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371148
    iget v0, p1, LX/4Vm;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->e:I

    .line 371149
    iget-object v0, p1, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 371150
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 371144
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 371145
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 371146
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371154
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 371155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 371156
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 371157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371158
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 371159
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 371141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371142
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371143
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 371138
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 371139
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->e:I

    .line 371140
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 371137
    const v0, -0x17e20950

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 371134
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371135
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 371136
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->f:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    return-object v0
.end method
