.class public final Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479971
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479972
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 479973
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479974
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479978
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479979
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->h:Ljava/lang/String;

    .line 479980
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 479975
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->i:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479976
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->i:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->i:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    .line 479977
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->i:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 479981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 479983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 479984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 479985
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 479986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 479987
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 479988
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 479989
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 479990
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 479991
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 479992
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->o()Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 479993
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 479994
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 479995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479996
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 479997
    goto :goto_0

    .line 479998
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->o()Lcom/facebook/graphql/enums/GraphQLGroupsSectionHeaderType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 479958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479959
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 479960
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 479961
    if-eqz v1, :cond_2

    .line 479962
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    .line 479963
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->f:Ljava/util/List;

    move-object v1, v0

    .line 479964
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479965
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 479966
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 479967
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    .line 479968
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 479969
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479970
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 479953
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 479954
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 479955
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 479956
    const/4 v0, 0x0

    .line 479957
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 479936
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 479937
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k:Z

    .line 479938
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 479939
    const v0, 0x3cbbcc5d

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479940
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479941
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->f:Ljava/util/List;

    .line 479942
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->g:Ljava/lang/String;

    .line 479945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479946
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479947
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 479948
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 479949
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 479950
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 479951
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k:Z

    return v0
.end method
