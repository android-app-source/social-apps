.class public final Lcom/facebook/graphql/model/GraphQLGroupPurpose;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupPurpose$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupPurpose$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368210
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368209
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368149
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368150
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368206
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368207
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 368208
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368204
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->f:Ljava/lang/String;

    .line 368205
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->f:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368200
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368201
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368202
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368197
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368198
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k:Ljava/lang/String;

    .line 368199
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 368194
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368195
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 368196
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 368174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368175
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 368177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 368178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 368179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 368180
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 368181
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 368182
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 368183
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368184
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 368185
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    if-ne v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 368186
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 368187
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 368188
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 368189
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 368190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368191
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 368192
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    goto :goto_0

    .line 368193
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->p()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368162
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 368164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->l()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 368166
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 368167
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368168
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 368169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 368170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 368171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368172
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368173
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368158
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368159
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->g:Ljava/lang/String;

    .line 368160
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368157
    const v0, -0x182c8a41

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 368154
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->h:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368155
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->h:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->h:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 368156
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->h:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368151
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368152
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j:Ljava/lang/String;

    .line 368153
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j:Ljava/lang/String;

    return-object v0
.end method
