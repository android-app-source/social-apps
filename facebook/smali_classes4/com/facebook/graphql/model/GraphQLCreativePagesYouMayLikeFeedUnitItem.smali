.class public final Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16o;
.implements LX/17x;
.implements LX/0jR;
.implements LX/25E;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368595
    const-class v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368598
    const-class v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368599
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->o:LX/0x2;

    .line 368601
    return-void
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368603
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->g:Ljava/lang/String;

    .line 368604
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 368605
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->o:LX/0x2;

    if-nez v0, :cond_0

    .line 368606
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->o:LX/0x2;

    .line 368607
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->o:LX/0x2;

    return-object v0
.end method

.method public final M_()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1

    .prologue
    .line 368608
    invoke-static {p0}, LX/18M;->a(LX/17x;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 368609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 368612
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 368613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 368614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 368615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 368616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 368617
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 368618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 368619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 368620
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 368621
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 368622
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 368623
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368624
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 368625
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 368626
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 368627
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 368628
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 368629
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 368630
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 368631
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 368636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368638
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368639
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 368642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368643
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368644
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 368645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 368646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 368647
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368648
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 368649
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 368650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 368651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 368652
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368653
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->k:Lcom/facebook/graphql/model/GraphQLPage;

    .line 368654
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 368655
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 368657
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368658
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368659
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 368660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 368661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 368662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 368663
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 368664
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368665
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 368666
    const/4 v0, 0x0

    move-object v0, v0

    .line 368667
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368668
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368669
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->n:Ljava/lang/String;

    .line 368670
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368671
    const v0, -0x47f1afd9

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 1

    .prologue
    .line 368596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    move-object v0, v0

    .line 368597
    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->q()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 368673
    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 368564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    move-object v0, v0

    .line 368565
    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 1

    .prologue
    .line 368566
    const/4 v0, 0x0

    move-object v0, v0

    .line 368567
    return-object v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 368568
    const/4 v0, 0x0

    move v0, v0

    .line 368569
    return v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368570
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368571
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368572
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368573
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368574
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->f:Ljava/lang/String;

    .line 368575
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368577
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->h:Ljava/lang/String;

    .line 368578
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368582
    invoke-static {p0}, LX/18M;->a(LX/16o;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 368585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->k:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->k:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->k:Lcom/facebook/graphql/model/GraphQLPage;

    .line 368588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->k:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 368594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->m:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method
