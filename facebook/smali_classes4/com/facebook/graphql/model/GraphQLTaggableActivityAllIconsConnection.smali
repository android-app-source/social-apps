.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371131
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371130
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 371128
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371129
    return-void
.end method

.method public constructor <init>(LX/4Z4;)V
    .locals 1

    .prologue
    .line 371124
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371125
    iget v0, p1, LX/4Z4;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->e:I

    .line 371126
    iget-object v0, p1, LX/4Z4;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    .line 371127
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    .line 371101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 371121
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 371122
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 371123
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 371114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371115
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 371116
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 371117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->a()I

    move-result v1

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 371118
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 371119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371120
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 371106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371107
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 371108
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 371109
    if-eqz v1, :cond_0

    .line 371110
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 371111
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->f:Ljava/util/List;

    .line 371112
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371113
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 371103
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 371104
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->e:I

    .line 371105
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 371102
    const v0, -0x4263e873

    return v0
.end method
