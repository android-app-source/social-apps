.class public final Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPhotoTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479690
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479689
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 479687
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479688
    return-void
.end method

.method public constructor <init>(LX/4Y3;)V
    .locals 1

    .prologue
    .line 479682
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479683
    iget-object v0, p1, LX/4Y3;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->e:Ljava/lang/String;

    .line 479684
    iget-object v0, p1, LX/4Y3;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 479685
    iget-object v0, p1, LX/4Y3;->d:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 479686
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 479649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 479651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 479652
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 479653
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 479654
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 479655
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 479656
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 479657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479658
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 479669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479670
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479671
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 479672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->j()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 479673
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    .line 479674
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 479675
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 479676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 479677
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->k()Lcom/facebook/graphql/model/GraphQLPhotoTag;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 479678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    .line 479679
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 479680
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479681
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479666
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479667
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->e:Ljava/lang/String;

    .line 479668
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 479665
    const v0, 0xa91e1a8

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479662
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479663
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 479664
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPhotoTag;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTag;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    .line 479661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;->g:Lcom/facebook/graphql/model/GraphQLPhotoTag;

    return-object v0
.end method
