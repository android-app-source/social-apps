.class public final Lcom/facebook/graphql/model/GraphQLFaceBox;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBox$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBox$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 486197
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBox$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 486196
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBox$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 486198
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 486199
    return-void
.end method

.method public constructor <init>(LX/4WG;)V
    .locals 1

    .prologue
    .line 486200
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 486201
    iget-object v0, p1, LX/4WG;->b:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486202
    iget-object v0, p1, LX/4WG;->c:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486203
    iget-object v0, p1, LX/4WG;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->g:Ljava/lang/String;

    .line 486204
    iget-object v0, p1, LX/4WG;->e:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 486205
    iget-object v0, p1, LX/4WG;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->i:Ljava/lang/String;

    .line 486206
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486207
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 486208
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->i:Ljava/lang/String;

    .line 486209
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 486164
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 486165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 486166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 486167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 486168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 486169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 486170
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 486171
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 486172
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 486173
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 486174
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 486175
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 486176
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 486177
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 486178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 486179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 486180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->j()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 486182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFaceBox;

    .line 486183
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486184
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 486185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->k()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 486187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFaceBox;

    .line 486188
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486189
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 486190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 486191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 486192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFaceBox;

    .line 486193
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 486194
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 486195
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBox;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 486162
    const v0, 0x221c4e0e

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 486160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->e:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 486157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 486158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->f:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 486151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->g:Ljava/lang/String;

    .line 486152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 486154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 486155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBox;->h:Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    return-object v0
.end method
