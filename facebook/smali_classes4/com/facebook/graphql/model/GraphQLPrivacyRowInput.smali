.class public final Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;
.implements LX/2cq;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyRowInput$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyRowInput$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 442134
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 442133
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 442131
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 442132
    return-void
.end method

.method public constructor <init>(LX/4YK;)V
    .locals 1

    .prologue
    .line 442125
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 442126
    iget-object v0, p1, LX/4YK;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->e:Ljava/util/List;

    .line 442127
    iget-object v0, p1, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 442128
    iget-object v0, p1, LX/4YK;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->g:Ljava/util/List;

    .line 442129
    iget-object v0, p1, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 442130
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 442113
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 442114
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 442115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 442116
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 442117
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 442118
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v4, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v3, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 442119
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 442120
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 442121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 442122
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 442123
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    goto :goto_0

    .line 442124
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v1

    goto :goto_1
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442135
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 442136
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->e:Ljava/util/List;

    .line 442137
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 442110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 442111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 442112
    return-object p0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 442107
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 442108
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 442109
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->f:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442104
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 442105
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->g:Ljava/util/List;

    .line 442106
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 442101
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 442102
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 442103
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->h:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 442100
    const v0, 0x598fd98

    return v0
.end method
