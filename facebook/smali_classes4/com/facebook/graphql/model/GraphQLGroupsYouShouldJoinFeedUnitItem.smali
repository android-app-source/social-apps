.class public final Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/254;
.implements LX/0jR;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368080
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368038
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368077
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368078
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->h:LX/0x2;

    .line 368079
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368074
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368075
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368076
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 368071
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->h:LX/0x2;

    if-nez v0, :cond_0

    .line 368072
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->h:LX/0x2;

    .line 368073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->h:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 368061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 368064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 368065
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 368066
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 368067
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 368068
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368070
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368049
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368050
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 368053
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368054
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 368056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 368057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 368058
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 368059
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368060
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 1

    .prologue
    .line 368046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    move-object v0, v0

    .line 368047
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368043
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368044
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->g:Ljava/lang/String;

    .line 368045
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368042
    const v0, 0x17c30bfd

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 368041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method
