.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371236
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371235
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 371233
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371234
    return-void
.end method

.method public constructor <init>(LX/4Z5;)V
    .locals 1

    .prologue
    .line 371224
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371225
    iget-object v0, p1, LX/4Z5;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->e:Ljava/lang/String;

    .line 371226
    iget-object v0, p1, LX/4Z5;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->f:Ljava/lang/String;

    .line 371227
    iget-object v0, p1, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371228
    iget-object v0, p1, LX/4Z5;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371229
    iget-object v0, p1, LX/4Z5;->f:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371230
    iget-object v0, p1, LX/4Z5;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371231
    iget-object v0, p1, LX/4Z5;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k:Ljava/lang/String;

    .line 371232
    return-void
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371221
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371222
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371223
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371218
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371219
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371220
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371215
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371216
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k:Ljava/lang/String;

    .line 371217
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 371197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 371199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 371200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 371201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 371202
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 371203
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 371204
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 371205
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 371206
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 371207
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 371208
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 371209
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 371210
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 371211
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 371212
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 371213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371214
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 371162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 371164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 371165
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371166
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 371167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 371169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 371170
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371171
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 371172
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371173
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 371174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 371175
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371176
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 371177
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 371179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 371180
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371181
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371182
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 371195
    const v0, 0x2615e4cf

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->e:Ljava/lang/String;

    .line 371194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371189
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371190
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->f:Ljava/lang/String;

    .line 371191
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method
