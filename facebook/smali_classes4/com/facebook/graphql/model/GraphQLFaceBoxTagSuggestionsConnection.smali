.class public final Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495874
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 495875
    const-class v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 495876
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 495877
    return-void
.end method

.method public constructor <init>(LX/4WH;)V
    .locals 1

    .prologue
    .line 495878
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 495879
    iget-object v0, p1, LX/4WH;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    .line 495880
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 495881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 495882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 495883
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 495884
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 495885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 495886
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 495888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    .line 495889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 495890
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 495891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 495892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 495893
    if-eqz v1, :cond_0

    .line 495894
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;

    .line 495895
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFaceBoxTagSuggestionsConnection;->e:Ljava/util/List;

    .line 495896
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 495897
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 495898
    const v0, -0x6278f01f

    return v0
.end method
