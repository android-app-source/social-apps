.class public final Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 458374
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 458373
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 458371
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 458372
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 458359
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 458360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 458361
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 458362
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 458363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 458364
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 458365
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 458366
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 458367
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 458368
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 458369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 458370
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 458346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 458347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 458348
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 458349
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 458350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    .line 458351
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 458352
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 458353
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    .line 458354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 458355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    .line 458356
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    .line 458357
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 458358
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->e:Ljava/lang/String;

    .line 458335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 458345
    const v0, -0x367aad15

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 458344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    .line 458341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->g:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->h:Ljava/lang/String;

    .line 458338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->h:Ljava/lang/String;

    return-object v0
.end method
