.class public Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/Flattenable;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 486339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 486332
    iget-object v0, p0, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;->a:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    invoke-virtual {p1, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    .line 486333
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 486334
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 486335
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/15i;I)V
    .locals 2

    .prologue
    .line 486337
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;->a:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 486338
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 486336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
