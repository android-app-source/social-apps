.class public final Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371267
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 371266
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 371264
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371265
    return-void
.end method

.method public constructor <init>(LX/4YJ;)V
    .locals 1

    .prologue
    .line 371244
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 371245
    iget-boolean v0, p1, LX/4YJ;->b:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->e:Z

    .line 371246
    iget-object v0, p1, LX/4YJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 371247
    iget-object v0, p1, LX/4YJ;->d:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 371248
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 371255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 371257
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 371258
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 371259
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 371260
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 371261
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371262
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 371263
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 371270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 371271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 371272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    .line 371273
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 371274
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371275
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 371252
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 371253
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->e:Z

    .line 371254
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 371249
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 371250
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 371251
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 371243
    const v0, -0x4844ee20

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 371242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 371237
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371238
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 371239
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-object v0
.end method
