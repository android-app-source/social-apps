.class public final Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 535560
    const-class v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 535559
    const-class v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 535541
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 535542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    .line 535543
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 535556
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    if-nez v0, :cond_0

    .line 535557
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    .line 535558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->f:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 535561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 535562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 535563
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 535564
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 535565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 535566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 535548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 535549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 535550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 535551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 535552
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;

    .line 535553
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 535554
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 535555
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 535545
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 535546
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 535547
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 535544
    const v0, 0x4544133d

    return v0
.end method
