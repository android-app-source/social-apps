.class public final Lcom/facebook/graphql/model/GraphQLNamePart;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/2qo;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNamePart$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNamePart$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 496984
    const-class v0, Lcom/facebook/graphql/model/GraphQLNamePart$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 496956
    const-class v0, Lcom/facebook/graphql/model/GraphQLNamePart$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 496982
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 496983
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 496979
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 496980
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 496981
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 496971
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 496972
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 496973
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNamePart;->a()I

    move-result v0

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 496974
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNamePart;->n_()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 496975
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNamePart;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 496976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 496977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 496978
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNamePart;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 496968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 496969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 496970
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 496964
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 496965
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->e:I

    .line 496966
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->f:I

    .line 496967
    return-void
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 496961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->g:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 496962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->g:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->g:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    .line 496963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->g:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 496960
    const v0, 0x718d793e

    return v0
.end method

.method public final n_()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 496957
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 496958
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 496959
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLNamePart;->f:I

    return v0
.end method
