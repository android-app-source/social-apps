.class public final Lcom/facebook/graphql/model/GraphQLViewer$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 370173
    const-class v0, Lcom/facebook/graphql/model/GraphQLViewer;

    new-instance v1, Lcom/facebook/graphql/model/GraphQLViewer$Deserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLViewer$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 370174
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 370175
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 370176
    const/4 v0, 0x1

    const/4 p2, 0x0

    .line 370177
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 370178
    invoke-static {p1, v2}, LX/262;->a(LX/15w;LX/186;)I

    move-result v1

    .line 370179
    if-eqz v0, :cond_0

    .line 370180
    const/4 p0, 0x2

    invoke-virtual {v2, p0}, LX/186;->c(I)V

    .line 370181
    invoke-virtual {v2, p2, v3, p2}, LX/186;->a(ISI)V

    .line 370182
    const/4 p0, 0x1

    invoke-virtual {v2, p0, v1}, LX/186;->b(II)V

    .line 370183
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 370184
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 370185
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 370186
    move-object v2, v0

    .line 370187
    new-instance v1, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLViewer;-><init>()V

    .line 370188
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 370189
    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v3

    move-object v0, v1

    .line 370190
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 370191
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_1

    .line 370192
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 370193
    :cond_1
    return-object v1
.end method
