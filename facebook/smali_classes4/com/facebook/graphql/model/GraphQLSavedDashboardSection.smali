.class public final Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedDashboardSection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSavedDashboardSection$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 442153
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 442154
    const-class v0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 442151
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 442152
    return-void
.end method

.method public constructor <init>(LX/4Yi;)V
    .locals 1

    .prologue
    .line 442155
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 442156
    iget-object v0, p1, LX/4Yi;->b:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 442157
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 442145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 442146
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 442147
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 442148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 442149
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 442150
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 442142
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 442143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 442144
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 442138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 442139
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 442140
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->e:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 442141
    const v0, -0x72d00dc8

    return v0
.end method
