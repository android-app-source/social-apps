.class public final Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368037
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368036
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368034
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368035
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 368022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 368025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 368026
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 368027
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 368028
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 368029
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 368030
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368031
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368032
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 368033
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 368012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 368014
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368015
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 368018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 368019
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368020
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368021
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 367996
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 367997
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 367998
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368008
    const v0, 0x236c5b4

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368005
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368006
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->f:Ljava/lang/String;

    .line 368007
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 368002
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368003
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 368004
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->g:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 367999
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368000
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 368001
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method
