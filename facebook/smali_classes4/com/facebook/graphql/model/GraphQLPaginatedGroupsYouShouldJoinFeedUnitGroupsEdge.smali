.class public final Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/254;
.implements LX/0jR;
.implements LX/16h;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 441992
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 441991
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 441988
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 441989
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    .line 441990
    return-void
.end method

.method public constructor <init>(LX/4Xl;)V
    .locals 1

    .prologue
    .line 441982
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 441983
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    .line 441984
    iget-object v0, p1, LX/4Xl;->b:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 441985
    iget-object v0, p1, LX/4Xl;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->f:Ljava/lang/String;

    .line 441986
    iget-object v0, p1, LX/4Xl;->d:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    .line 441987
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 441954
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    if-nez v0, :cond_0

    .line 441955
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    .line 441956
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->g:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 441974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 441975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 441976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 441977
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 441978
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 441979
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 441980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 441981
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 441966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 441967
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 441968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 441969
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 441970
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    .line 441971
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 441972
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 441973
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 1

    .prologue
    .line 441964
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    move-object v0, v0

    .line 441965
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->f:Ljava/lang/String;

    .line 441963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 441960
    const v0, -0x2d52cf72

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441957
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 441958
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 441959
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method
