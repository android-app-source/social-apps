.class public final Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 492766
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 492765
    const-class v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 492762
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 492763
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->i:LX/0x2;

    .line 492764
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 492759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->i:LX/0x2;

    if-nez v0, :cond_0

    .line 492760
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->i:LX/0x2;

    .line 492761
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->i:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 492747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 492748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 492749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 492750
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 492751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 492752
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 492753
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 492754
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 492755
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 492756
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 492757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 492758
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 492729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 492730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 492731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 492733
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 492734
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492735
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 492736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492737
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 492738
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 492739
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492740
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 492741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 492742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 492743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    .line 492744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLUser;

    .line 492745
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 492746
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 492726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 492727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 492716
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 492717
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->g:Ljava/lang/String;

    .line 492718
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 492725
    const v0, -0x5269f517

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 492722
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 492723
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492724
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 492719
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 492720
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLUser;

    .line 492721
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->h:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method
