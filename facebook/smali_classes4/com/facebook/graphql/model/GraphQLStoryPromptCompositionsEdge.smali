.class public final Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479722
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479691
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 479720
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479721
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 479710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479711
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 479712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 479713
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 479714
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 479715
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 479716
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 479717
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 479718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 479702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479705
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 479706
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    .line 479707
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479708
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479709
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479699
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479700
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479701
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->e:Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 479698
    const v0, -0x5a6e91e1

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->f:Ljava/lang/String;

    .line 479697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479692
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479693
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->g:Ljava/lang/String;

    .line 479694
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->g:Ljava/lang/String;

    return-object v0
.end method
