.class public final Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 458592
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 458591
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 458589
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 458590
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 458559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 458560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 458561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 458562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 458563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 458564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 458565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 458566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 458567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 458568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 458569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 458570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 458571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->t()LX/0Px;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 458572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    move-result-object v12

    invoke-static {p1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 458573
    const/16 v13, 0xd

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 458574
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 458575
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 458576
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 458577
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 458578
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 458579
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 458580
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 458581
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 458582
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 458583
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 458584
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 458585
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 458586
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 458587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 458588
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 458491
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 458492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 458493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 458494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 458495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458496
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 458497
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 458498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 458499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 458500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458501
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458502
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 458503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 458505
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458506
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458507
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 458508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 458510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458511
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458512
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 458513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 458515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458516
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458517
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 458518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 458519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 458520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458521
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458522
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 458523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 458524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 458525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458526
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458527
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 458528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 458530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458531
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458532
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 458533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 458535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458536
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458537
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 458538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 458540
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458541
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458542
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 458543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    .line 458544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 458545
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458546
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    .line 458547
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->t()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 458548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->t()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 458549
    if-eqz v2, :cond_b

    .line 458550
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458551
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p:Ljava/util/List;

    move-object v1, v0

    .line 458552
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 458553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 458555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 458556
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458557
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 458558
    if-nez v1, :cond_d

    :goto_0
    return-object p0

    :cond_d
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 458490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 458487
    const v0, -0x2635c4db

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458484
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458485
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458486
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458451
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458452
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458453
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458481
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458482
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458483
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->h:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458478
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458479
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458480
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458475
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458476
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458477
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458472
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458473
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 458474
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458469
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458470
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458471
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458466
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458467
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    .line 458468
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458463
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458464
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458465
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 458462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p:Ljava/util/List;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p:Ljava/util/List;

    .line 458459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 458454
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 458455
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    .line 458456
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    return-object v0
.end method
