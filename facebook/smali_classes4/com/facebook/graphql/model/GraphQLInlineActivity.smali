.class public final Lcom/facebook/graphql/model/GraphQLInlineActivity;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInlineActivity$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInlineActivity$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 370866
    const-class v0, Lcom/facebook/graphql/model/GraphQLInlineActivity$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 370808
    const-class v0, Lcom/facebook/graphql/model/GraphQLInlineActivity$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 370864
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 370865
    return-void
.end method

.method public constructor <init>(LX/4X0;)V
    .locals 1

    .prologue
    .line 370857
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 370858
    iget-object v0, p1, LX/4X0;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->e:Ljava/lang/String;

    .line 370859
    iget-object v0, p1, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 370860
    iget-object v0, p1, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 370861
    iget-object v0, p1, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 370862
    iget-object v0, p1, LX/4X0;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->i:Ljava/lang/String;

    .line 370863
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 370843
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 370844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 370845
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 370846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 370847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 370848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 370849
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 370850
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 370851
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 370852
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 370853
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 370854
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 370855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 370856
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 370825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 370826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 370827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 370828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 370829
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 370830
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 370831
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 370832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 370833
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 370834
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 370835
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 370836
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 370837
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 370838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 370839
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 370840
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 370841
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 370842
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 370824
    const v0, 0x4cff1ce8    # 1.3375264E8f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370821
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370822
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->e:Ljava/lang/String;

    .line 370823
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370818
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370819
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 370820
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->f:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370815
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370816
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 370817
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->g:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370812
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370813
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 370814
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->h:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370809
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370810
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->i:Ljava/lang/String;

    .line 370811
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineActivity;->i:Ljava/lang/String;

    return-object v0
.end method
