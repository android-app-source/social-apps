.class public final Lcom/facebook/graphql/model/GraphQLSuggestedComposition;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSuggestedComposition$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSuggestedComposition$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLMaskEffect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLMemeCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLParticleEffect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLShaderFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479866
    const-class v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479865
    const-class v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 479863
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479864
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 16

    .prologue
    .line 479831
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479832
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 479833
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 479834
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 479835
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 479836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 479837
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 479838
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 479839
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 479840
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 479841
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r()Lcom/facebook/graphql/model/GraphQLMaskEffect;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 479842
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->s()Lcom/facebook/graphql/model/GraphQLMemeCategory;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 479843
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->t()Lcom/facebook/graphql/model/GraphQLParticleEffect;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 479844
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->u()Lcom/facebook/graphql/model/GraphQLShaderFilter;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 479845
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 479846
    const/16 v15, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 479847
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1}, LX/186;->b(II)V

    .line 479848
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 479849
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 479850
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 479851
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 479852
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 479853
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 479854
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 479855
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 479856
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 479857
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 479858
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 479859
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 479860
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 479861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479862
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    return v1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 479723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479725
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 479726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 479727
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479728
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->e:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 479729
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 479730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 479731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 479732
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479733
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 479734
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 479735
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    .line 479736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 479737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479738
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    .line 479739
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 479740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    .line 479741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 479742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479743
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    .line 479744
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r()Lcom/facebook/graphql/model/GraphQLMaskEffect;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 479745
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r()Lcom/facebook/graphql/model/GraphQLMaskEffect;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 479746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r()Lcom/facebook/graphql/model/GraphQLMaskEffect;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 479747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479748
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n:Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 479749
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->s()Lcom/facebook/graphql/model/GraphQLMemeCategory;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 479750
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->s()Lcom/facebook/graphql/model/GraphQLMemeCategory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMemeCategory;

    .line 479751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->s()Lcom/facebook/graphql/model/GraphQLMemeCategory;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 479752
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479753
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o:Lcom/facebook/graphql/model/GraphQLMemeCategory;

    .line 479754
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 479755
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 479756
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 479757
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479758
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->f:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 479759
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->t()Lcom/facebook/graphql/model/GraphQLParticleEffect;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 479760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->t()Lcom/facebook/graphql/model/GraphQLParticleEffect;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 479761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->t()Lcom/facebook/graphql/model/GraphQLParticleEffect;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 479762
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479763
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p:Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 479764
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 479765
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 479766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 479767
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479768
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->g:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 479769
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->u()Lcom/facebook/graphql/model/GraphQLShaderFilter;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 479770
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->u()Lcom/facebook/graphql/model/GraphQLShaderFilter;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLShaderFilter;

    .line 479771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->u()Lcom/facebook/graphql/model/GraphQLShaderFilter;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 479772
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479773
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q:Lcom/facebook/graphql/model/GraphQLShaderFilter;

    .line 479774
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 479775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 479776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 479777
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479778
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 479779
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 479780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 479781
    if-eqz v2, :cond_b

    .line 479782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479783
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j:Ljava/util/List;

    move-object v1, v0

    .line 479784
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 479785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 479786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 479787
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    .line 479788
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 479789
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479790
    if-nez v1, :cond_d

    :goto_0
    return-object p0

    :cond_d
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479828
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->e:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479829
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->e:Lcom/facebook/graphql/model/GraphQLPlace;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->e:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 479830
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->e:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 479827
    const v0, -0x5b830959

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLInlineActivity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->f:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->f:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->f:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    .line 479826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->f:Lcom/facebook/graphql/model/GraphQLInlineActivity;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479821
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->g:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479822
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->g:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->g:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 479823
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->g:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479818
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479819
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->h:Ljava/util/List;

    .line 479820
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 479869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479815
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479816
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j:Ljava/util/List;

    .line 479817
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479812
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479813
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    .line 479814
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k:Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479809
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479810
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    .line 479811
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l:Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479806
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479807
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    .line 479808
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m:Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLMaskEffect;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479803
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n:Lcom/facebook/graphql/model/GraphQLMaskEffect;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479804
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n:Lcom/facebook/graphql/model/GraphQLMaskEffect;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMaskEffect;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n:Lcom/facebook/graphql/model/GraphQLMaskEffect;

    .line 479805
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n:Lcom/facebook/graphql/model/GraphQLMaskEffect;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLMemeCategory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479800
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o:Lcom/facebook/graphql/model/GraphQLMemeCategory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479801
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o:Lcom/facebook/graphql/model/GraphQLMemeCategory;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLMemeCategory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMemeCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o:Lcom/facebook/graphql/model/GraphQLMemeCategory;

    .line 479802
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o:Lcom/facebook/graphql/model/GraphQLMemeCategory;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLParticleEffect;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479797
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p:Lcom/facebook/graphql/model/GraphQLParticleEffect;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479798
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p:Lcom/facebook/graphql/model/GraphQLParticleEffect;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffect;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p:Lcom/facebook/graphql/model/GraphQLParticleEffect;

    .line 479799
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p:Lcom/facebook/graphql/model/GraphQLParticleEffect;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLShaderFilter;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479794
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q:Lcom/facebook/graphql/model/GraphQLShaderFilter;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479795
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q:Lcom/facebook/graphql/model/GraphQLShaderFilter;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLShaderFilter;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLShaderFilter;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q:Lcom/facebook/graphql/model/GraphQLShaderFilter;

    .line 479796
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q:Lcom/facebook/graphql/model/GraphQLShaderFilter;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 479792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    .line 479793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r:Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    return-object v0
.end method
