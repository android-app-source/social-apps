.class public final Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16h;
.implements LX/16q;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368563
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 368527
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368551
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 368552
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 368553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368554
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 368555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 368556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 368557
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 368558
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 368559
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 368560
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 368561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 368535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 368536
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368537
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368538
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 368539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    .line 368540
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 368541
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 368542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 368543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 368544
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    .line 368545
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLUser;

    .line 368546
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 368547
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368532
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368533
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLUser;

    .line 368534
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->f:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368529
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 368530
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->g:Ljava/lang/String;

    .line 368531
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 368528
    const v0, 0xd677fe

    return v0
.end method
