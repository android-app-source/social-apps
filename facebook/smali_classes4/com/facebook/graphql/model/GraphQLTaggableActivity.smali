.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivity;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivity$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivity$Serializer;
.end annotation


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Z

.field public z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 370949
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivity$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 370946
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivity$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 370947
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 370948
    return-void
.end method

.method public constructor <init>(LX/4Z3;)V
    .locals 1

    .prologue
    .line 370916
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 370917
    iget-object v0, p1, LX/4Z3;->b:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 370918
    iget-object v0, p1, LX/4Z3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 370919
    iget-object v0, p1, LX/4Z3;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 370920
    iget-object v0, p1, LX/4Z3;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->h:Ljava/lang/String;

    .line 370921
    iget-boolean v0, p1, LX/4Z3;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->i:Z

    .line 370922
    iget-object v0, p1, LX/4Z3;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j:Ljava/lang/String;

    .line 370923
    iget v0, p1, LX/4Z3;->h:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k:I

    .line 370924
    iget-object v0, p1, LX/4Z3;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l:Ljava/lang/String;

    .line 370925
    iget-object v0, p1, LX/4Z3;->j:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370926
    iget-object v0, p1, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370927
    iget-object v0, p1, LX/4Z3;->l:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370928
    iget-object v0, p1, LX/4Z3;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370929
    iget-object v0, p1, LX/4Z3;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370930
    iget-object v0, p1, LX/4Z3;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370931
    iget-object v0, p1, LX/4Z3;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370932
    iget-object v0, p1, LX/4Z3;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370933
    iget-object v0, p1, LX/4Z3;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370934
    iget-object v0, p1, LX/4Z3;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370935
    iget-object v0, p1, LX/4Z3;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370936
    iget-object v0, p1, LX/4Z3;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x:Ljava/lang/String;

    .line 370937
    iget-boolean v0, p1, LX/4Z3;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y:Z

    .line 370938
    iget-boolean v0, p1, LX/4Z3;->w:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z:Z

    .line 370939
    iget-boolean v0, p1, LX/4Z3;->x:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A:Z

    .line 370940
    iget-object v0, p1, LX/4Z3;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B:Ljava/lang/String;

    .line 370941
    return-void
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370953
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370954
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370955
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370956
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370957
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370958
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370960
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370961
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 371097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method private G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370962
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370963
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B:Ljava/lang/String;

    .line 370964
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 370965
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 370966
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 370967
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A:Z

    return v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 370968
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 370969
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 370970
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 370971
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 370972
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 370973
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 370974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 370975
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 370976
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 370977
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 370978
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->C()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 370979
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 370980
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 370981
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 370982
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 370983
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->D()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 370984
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->E()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 370985
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->F()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 370986
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 370987
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->G()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 370988
    const/16 v21, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 370989
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 370990
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 370991
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 370992
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 370993
    const/4 v2, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 370994
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 370995
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 370996
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 370997
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 370998
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 370999
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 371000
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 371001
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 371002
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 371003
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 371004
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 371005
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 371006
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 371007
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 371008
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 371009
    const/16 v2, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 371010
    const/16 v2, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 371011
    const/16 v2, 0x17

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 371012
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 371013
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371014
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 371016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 371017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 371018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 371019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 371021
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 371022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 371024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371025
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371026
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 371027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 371028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 371029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371030
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 371031
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 371032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 371034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371035
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371036
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 371037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 371039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371040
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371041
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 371042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371043
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->B()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 371044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371045
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371046
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->C()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 371047
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->C()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371048
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->C()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 371049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371050
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371051
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 371052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 371054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371055
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371056
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 371057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 371059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371060
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371061
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 371062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 371064
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371065
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371066
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 371067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 371069
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371070
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371071
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->D()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 371072
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->D()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371073
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->D()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 371074
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371075
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371076
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->E()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 371077
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->E()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371078
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->E()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 371079
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371080
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371081
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->F()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 371082
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->F()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371083
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->F()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 371084
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 371085
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 371086
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 371087
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 371089
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 371090
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->i:Z

    .line 371091
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k:I

    .line 371092
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y:Z

    .line 371093
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z:Z

    .line 371094
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A:Z

    .line 371095
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 370942
    const v0, -0xe40ca

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    .line 370945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370868
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370869
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 370870
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370871
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370872
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 370873
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370874
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370875
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->h:Ljava/lang/String;

    .line 370876
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 370877
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 370878
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 370879
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->i:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370880
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370881
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j:Ljava/lang/String;

    .line 370882
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 370883
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 370884
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 370885
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k:I

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370886
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370887
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l:Ljava/lang/String;

    .line 370888
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370889
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370890
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370891
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370892
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370893
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370894
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370895
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370896
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370897
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370898
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370899
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370900
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370901
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370902
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370903
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370904
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370905
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 370906
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 370907
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 370908
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x:Ljava/lang/String;

    .line 370909
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 370910
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 370911
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 370912
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 370913
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 370914
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 370915
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z:Z

    return v0
.end method
