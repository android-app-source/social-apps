.class public final Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 443648
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 443647
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 443618
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 443619
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 443640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 443641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 443642
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 443643
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 443644
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 443645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 443646
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 443632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 443633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 443634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    .line 443635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 443636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;

    .line 443637
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    .line 443638
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 443639
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 443629
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 443630
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->e:Z

    .line 443631
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 443623
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->e:Z

    .line 443624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 443625
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 443626
    if-eqz v0, :cond_0

    .line 443627
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 443628
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 443620
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 443621
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 443622
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 443617
    const v0, -0x67ff2740

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLDocumentLogo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443614
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443615
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    .line 443616
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;->f:Lcom/facebook/graphql/model/GraphQLDocumentLogo;

    return-object v0
.end method
