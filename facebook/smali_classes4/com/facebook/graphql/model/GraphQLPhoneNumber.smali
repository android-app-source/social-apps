.class public final Lcom/facebook/graphql/model/GraphQLPhoneNumber;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhoneNumber$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhoneNumber$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 443577
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoneNumber$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 443613
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoneNumber$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 443611
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 443612
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 443608
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443609
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 443610
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->e:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 443594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 443595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 443596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 443597
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 443598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 443599
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 443600
    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->m()Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    if-ne v0, v6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 443601
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 443602
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 443603
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 443604
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 443605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 443606
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 443607
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->m()Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 443591
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 443592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 443593
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443588
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443589
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->f:Ljava/lang/String;

    .line 443590
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 443587
    const v0, 0x1c4e6237

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443584
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443585
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->g:Ljava/lang/String;

    .line 443586
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443581
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443582
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->h:Ljava/lang/String;

    .line 443583
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443578
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 443579
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->i:Ljava/lang/String;

    .line 443580
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;->i:Ljava/lang/String;

    return-object v0
.end method
