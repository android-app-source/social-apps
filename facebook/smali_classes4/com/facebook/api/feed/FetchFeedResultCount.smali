.class public Lcom/facebook/api/feed/FetchFeedResultCount;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/api/feed/FetchFeedParams;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feed/FetchFeedParams;I)V
    .locals 0

    .prologue
    .line 486397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486398
    iput-object p1, p0, Lcom/facebook/api/feed/FetchFeedResultCount;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 486399
    iput p2, p0, Lcom/facebook/api/feed/FetchFeedResultCount;->a:I

    .line 486400
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 486401
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 486402
    iget v0, p0, Lcom/facebook/api/feed/FetchFeedResultCount;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 486403
    return-void
.end method
