.class public final Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 561884
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 561885
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 561882
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 561883
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 561858
    if-nez p1, :cond_0

    .line 561859
    :goto_0
    return v1

    .line 561860
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 561861
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 561862
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 561863
    const v2, 0x2981c15f

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 561864
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 561865
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 561866
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 561867
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 561868
    const v2, 0x135fe2bd

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 561869
    invoke-virtual {p0, p1, v10, v1}, LX/15i;->a(III)I

    move-result v2

    .line 561870
    invoke-virtual {p3, v11}, LX/186;->c(I)V

    .line 561871
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 561872
    invoke-virtual {p3, v10, v2, v1}, LX/186;->a(III)V

    .line 561873
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 561874
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 561875
    invoke-virtual {p0, p1, v10, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 561876
    invoke-virtual {p0, p1, v11, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 561877
    const/4 v0, 0x3

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 561878
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v10

    move-wide v2, v6

    .line 561879
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v11

    move-wide v2, v8

    .line 561880
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 561881
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x135fe2bd -> :sswitch_2
        0x2981c15f -> :sswitch_1
        0x56ac9323 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 561857
    new-instance v0, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 561850
    sparse-switch p2, :sswitch_data_0

    .line 561851
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 561852
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 561853
    const v1, 0x2981c15f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 561854
    :goto_0
    :sswitch_1
    return-void

    .line 561855
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 561856
    const v1, 0x135fe2bd

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x135fe2bd -> :sswitch_1
        0x2981c15f -> :sswitch_2
        0x56ac9323 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 561844
    if-eqz p1, :cond_0

    .line 561845
    invoke-static {p0, p1, p2}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;

    move-result-object v1

    .line 561846
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;

    .line 561847
    if-eq v0, v1, :cond_0

    .line 561848
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 561849
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 561843
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 561815
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 561816
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 561886
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 561887
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 561888
    :cond_0
    iput-object p1, p0, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 561889
    iput p2, p0, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->b:I

    .line 561890
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 561842
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 561841
    new-instance v0, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 561838
    iget v0, p0, LX/1vt;->c:I

    .line 561839
    move v0, v0

    .line 561840
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 561835
    iget v0, p0, LX/1vt;->c:I

    .line 561836
    move v0, v0

    .line 561837
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 561832
    iget v0, p0, LX/1vt;->b:I

    .line 561833
    move v0, v0

    .line 561834
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561829
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 561830
    move-object v0, v0

    .line 561831
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 561820
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 561821
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 561822
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 561823
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 561824
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 561825
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 561826
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 561827
    invoke-static {v3, v9, v2}, Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/location/write/graphql/LocationMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 561828
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 561817
    iget v0, p0, LX/1vt;->c:I

    .line 561818
    move v0, v0

    .line 561819
    return v0
.end method
