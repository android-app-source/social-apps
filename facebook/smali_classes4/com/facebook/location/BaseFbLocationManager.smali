.class public abstract Lcom/facebook/location/BaseFbLocationManager;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0y3;

.field private final b:LX/0SG;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final f:LX/0Zb;

.field private final g:LX/0y2;

.field public final h:LX/0Uo;

.field public i:LX/2vk;

.field public j:LX/2vj;

.field public k:Lcom/facebook/common/callercontext/CallerContext;

.field public l:Ljava/util/concurrent/ExecutorService;

.field public final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public n:Ljava/util/concurrent/ScheduledFuture;

.field public o:Lcom/facebook/location/ImmutableLocation;

.field private p:J

.field public q:I


# direct methods
.method public constructor <init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0y3;",
            "LX/0SG;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Zb;",
            "LX/0y2;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 478602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478603
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 478604
    iput-object p1, p0, Lcom/facebook/location/BaseFbLocationManager;->a:LX/0y3;

    .line 478605
    iput-object p2, p0, Lcom/facebook/location/BaseFbLocationManager;->b:LX/0SG;

    .line 478606
    iput-object p3, p0, Lcom/facebook/location/BaseFbLocationManager;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 478607
    iput-object p4, p0, Lcom/facebook/location/BaseFbLocationManager;->d:LX/0Or;

    .line 478608
    iput-object p5, p0, Lcom/facebook/location/BaseFbLocationManager;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 478609
    iput-object p6, p0, Lcom/facebook/location/BaseFbLocationManager;->f:LX/0Zb;

    .line 478610
    iput-object p7, p0, Lcom/facebook/location/BaseFbLocationManager;->g:LX/0y2;

    .line 478611
    iput-object p8, p0, Lcom/facebook/location/BaseFbLocationManager;->h:LX/0Uo;

    .line 478612
    return-void
.end method

.method private static a(Lcom/facebook/location/BaseFbLocationManager;LX/2sk;)V
    .locals 1

    .prologue
    .line 478600
    invoke-virtual {p1}, LX/2sk;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;Ljava/lang/String;)V

    .line 478601
    return-void
.end method

.method public static a(Lcom/facebook/location/BaseFbLocationManager;LX/2vl;)V
    .locals 3

    .prologue
    .line 478598
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/2vl;->perfMarkerId:I

    iget-object v2, p1, LX/2vl;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 478599
    return-void
.end method

.method public static a(Lcom/facebook/location/BaseFbLocationManager;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 478588
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/location/BaseFbLocationManager;->p:J

    sub-long v2, v0, v2

    .line 478589
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 478590
    iget-object v4, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v1, v4

    .line 478591
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 478592
    const-string v1, "com.facebook."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 478593
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 478594
    :cond_0
    sget-object v1, LX/35U;->a:LX/35U;

    move-object v1, v1

    .line 478595
    iget-object v4, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v4, v4, LX/2vk;->a:LX/0yF;

    invoke-virtual {v1, v0, v4, v2, v3}, LX/35U;->a(Ljava/lang/String;LX/0yF;J)V

    .line 478596
    return-void

    .line 478597
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "-"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;JF)Z
    .locals 2

    .prologue
    .line 478587
    invoke-virtual {p0, p1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v0, p4

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/location/BaseFbLocationManager;LX/2Ce;)V
    .locals 3

    .prologue
    .line 478585
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/location/BaseFbLocationManager$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/location/BaseFbLocationManager$3;-><init>(Lcom/facebook/location/BaseFbLocationManager;LX/2Ce;)V

    const v2, 0x65a38a25

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 478586
    return-void
.end method

.method public static b(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 478581
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    move v0, v1

    .line 478582
    :goto_0
    return v0

    .line 478583
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 478584
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-wide v4, v0, LX/2vk;->g:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized e$redex0(Lcom/facebook/location/BaseFbLocationManager;)V
    .locals 2

    .prologue
    .line 478575
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/location/BaseFbLocationManager;->b()V

    .line 478576
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->j(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478577
    sget-object v0, LX/2sk;->TIMEOUT:LX/2sk;

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2sk;)V

    .line 478578
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->TIMEOUT:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/BaseFbLocationManager;LX/2Ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478579
    monitor-exit p0

    return-void

    .line 478580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g(Lcom/facebook/location/BaseFbLocationManager;)V
    .locals 2

    .prologue
    .line 478571
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->n:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 478572
    :goto_0
    return-void

    .line 478573
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->n:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 478574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->n:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static h(Lcom/facebook/location/BaseFbLocationManager;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 478564
    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    .line 478565
    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->j:LX/2vj;

    .line 478566
    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 478567
    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->o:Lcom/facebook/location/ImmutableLocation;

    .line 478568
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/location/BaseFbLocationManager;->p:J

    .line 478569
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/location/BaseFbLocationManager;->q:I

    .line 478570
    return-void
.end method

.method private static i(Lcom/facebook/location/BaseFbLocationManager;)V
    .locals 10

    .prologue
    .line 478461
    invoke-static {}, LX/2vl;->values()[LX/2vl;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 478462
    new-instance v4, LX/0Yj;

    iget v5, v3, LX/2vl;->perfMarkerId:I

    iget-object v3, v3, LX/2vl;->perfLoggerName:Ljava/lang/String;

    invoke-direct {v4, v5, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 478463
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 478464
    const-string v7, "caller_context_class"

    iget-object v8, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 478465
    iget-object v9, v8, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v8, v9

    .line 478466
    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478467
    const-string v7, "caller_context_tag"

    iget-object v8, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478468
    const-string v7, "param_priority"

    iget-object v8, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v8, v8, LX/2vk;->a:LX/0yF;

    invoke-virtual {v8}, LX/0yF;->name()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478469
    const-string v7, "param_interval_ms"

    iget-object v8, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-wide v8, v8, LX/2vk;->e:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478470
    const-string v7, "is_active"

    .line 478471
    iget-object v8, p0, Lcom/facebook/location/BaseFbLocationManager;->h:LX/0Uo;

    invoke-virtual {v8}, LX/0Uo;->l()Z

    move-result v8

    move v8, v8

    .line 478472
    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478473
    move-object v3, v6

    .line 478474
    invoke-virtual {v4, v3}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 478475
    iget-object v3, p0, Lcom/facebook/location/BaseFbLocationManager;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 478476
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478477
    :cond_0
    return-void
.end method

.method private static j(Lcom/facebook/location/BaseFbLocationManager;)V
    .locals 6

    .prologue
    .line 478560
    invoke-static {}, LX/2vl;->values()[LX/2vl;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 478561
    iget-object v4, p0, Lcom/facebook/location/BaseFbLocationManager;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v5, v3, LX/2vl;->perfMarkerId:I

    iget-object v3, v3, LX/2vl;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 478562
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478563
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()LX/1wZ;
.end method

.method public final declared-synchronized a(LX/2Ce;)V
    .locals 1

    .prologue
    .line 478555
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->g(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478556
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->j(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478557
    invoke-static {p0, p1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/BaseFbLocationManager;LX/2Ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478558
    monitor-exit p0

    return-void

    .line 478559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract a(LX/2vk;)V
.end method

.method public final declared-synchronized a(LX/2vk;LX/2vj;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 478536
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 478537
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vk;

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    .line 478538
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vj;

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->j:LX/2vj;

    .line 478539
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 478540
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/BaseFbLocationManager;->p:J

    .line 478541
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->l:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 478542
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->l:Ljava/util/concurrent/ExecutorService;

    .line 478543
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->a:LX/0y3;

    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v1, v1, LX/2vk;->a:LX/0yF;

    const/4 v2, 0x0

    .line 478544
    invoke-static {v0, v1, v2, v2}, LX/0y3;->a(LX/0y3;LX/0yF;LX/0cA;LX/0cA;)LX/0yG;

    move-result-object v2

    move-object v0, v2

    .line 478545
    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_2

    .line 478546
    sget-object v0, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2sk;)V

    .line 478547
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/BaseFbLocationManager;LX/2Ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478548
    :goto_1
    monitor-exit p0

    return-void

    .line 478549
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 478550
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v3, v3, LX/2vk;->d:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-nez v3, :cond_3

    .line 478551
    :goto_2
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->i(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478552
    invoke-virtual {p0, p1}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2vk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 478553
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 478554
    :cond_3
    iget-object v4, p0, Lcom/facebook/location/BaseFbLocationManager;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/location/BaseFbLocationManager$1;

    invoke-direct {v5, p0}, Lcom/facebook/location/BaseFbLocationManager$1;-><init>(Lcom/facebook/location/BaseFbLocationManager;)V

    iget-object v3, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v3, v3, LX/2vk;->d:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v7, v8, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/location/BaseFbLocationManager;->n:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_2
.end method

.method public final declared-synchronized a(Lcom/facebook/location/ImmutableLocation;)V
    .locals 8

    .prologue
    .line 478501
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->g:LX/0y2;

    .line 478502
    iget-object v2, v0, LX/0y2;->e:Lcom/facebook/location/ImmutableLocation;

    .line 478503
    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v1, v3, v1

    if-lez v1, :cond_1

    .line 478504
    :cond_0
    iput-object p1, v0, LX/0y2;->e:Lcom/facebook/location/ImmutableLocation;

    .line 478505
    :cond_1
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 478506
    :goto_0
    monitor-exit p0

    return-void

    .line 478507
    :cond_2
    :try_start_1
    sget-object v1, LX/2vl;->ANY_LOCATION:LX/2vl;

    invoke-static {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2vl;)V

    .line 478508
    const-wide/32 v1, 0xdbba0

    const/high16 v3, 0x447a0000    # 1000.0f

    invoke-static {p0, p1, v1, v2, v3}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;JF)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 478509
    sget-object v1, LX/2vl;->CITY_GRANULARITY:LX/2vl;

    invoke-static {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2vl;)V

    .line 478510
    :cond_3
    const-wide/32 v1, 0x493e0

    const/high16 v3, 0x437a0000    # 250.0f

    invoke-static {p0, p1, v1, v2, v3}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;JF)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 478511
    sget-object v1, LX/2vl;->BLOCK_GRANULARITY:LX/2vl;

    invoke-static {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2vl;)V

    .line 478512
    :cond_4
    const-wide/32 v1, 0xea60

    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {p0, p1, v1, v2, v3}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;JF)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 478513
    sget-object v1, LX/2vl;->EXACT_GRANULARITY:LX/2vl;

    invoke-static {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;LX/2vl;)V

    .line 478514
    :cond_5
    const/4 v3, 0x0

    .line 478515
    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v2, v2, LX/2vk;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0, p1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v2, v2, LX/2vk;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-lez v2, :cond_9

    move v2, v3

    .line 478516
    :goto_1
    move v1, v2

    .line 478517
    if-nez v1, :cond_7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478518
    :cond_6
    :goto_2
    goto :goto_0

    .line 478519
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 478520
    :cond_7
    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->o:Lcom/facebook/location/ImmutableLocation;

    const/4 v2, 0x1

    .line 478521
    if-eqz v1, :cond_8

    invoke-static {p0, v1, p1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 478522
    :cond_8
    :goto_3
    move v1, v2

    .line 478523
    if-eqz v1, :cond_6

    .line 478524
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->g(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478525
    iput-object p1, p0, Lcom/facebook/location/BaseFbLocationManager;->o:Lcom/facebook/location/ImmutableLocation;

    .line 478526
    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/location/BaseFbLocationManager$2;

    invoke-direct {v2, p0, p1}, Lcom/facebook/location/BaseFbLocationManager$2;-><init>(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;)V

    const v3, -0x6995bdcd

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 478527
    goto :goto_2

    .line 478528
    :cond_9
    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v2, v2, LX/2vk;->c:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-object v2, v2, LX/2vk;->c:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, v4, v2

    if-lez v2, :cond_a

    move v2, v3

    .line 478529
    goto :goto_1

    .line 478530
    :cond_a
    const/4 v2, 0x1

    goto :goto_1

    .line 478531
    :cond_b
    const/4 v4, 0x0

    .line 478532
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpg-float v3, v5, v3

    if-gez v3, :cond_e

    move v3, v4

    .line 478533
    :goto_4
    move v3, v3

    .line 478534
    if-eqz v3, :cond_c

    invoke-static {p0, p1, v1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/BaseFbLocationManager;Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 478535
    :cond_c
    invoke-static {v1, p1}, LX/3GC;->b(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    iget-object v3, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget-wide v6, v3, LX/2vk;->e:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_d

    invoke-static {v1, p1}, LX/3GC;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)F

    move-result v3

    iget-object v4, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget v4, v4, LX/2vk;->f:F

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_8

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_e
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v5, p0, Lcom/facebook/location/BaseFbLocationManager;->i:LX/2vk;

    iget v5, v5, LX/2vk;->h:F

    mul-float/2addr v5, v3

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v3, v5, v3

    if-ltz v3, :cond_f

    const/4 v3, 0x1

    goto :goto_4

    :cond_f
    move v3, v4

    goto :goto_4
.end method

.method public final declared-synchronized a(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 478497
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must call this before operation starts"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 478498
    iput-object p1, p0, Lcom/facebook/location/BaseFbLocationManager;->l:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478499
    monitor-exit p0

    return-void

    .line 478500
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/facebook/location/ImmutableLocation;)J
    .locals 4

    .prologue
    .line 478496
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    return-wide v0
.end method

.method public abstract b()V
.end method

.method public final declared-synchronized c()V
    .locals 6

    .prologue
    .line 478486
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/BaseFbLocationManager;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 478487
    :goto_0
    monitor-exit p0

    return-void

    .line 478488
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->g(Lcom/facebook/location/BaseFbLocationManager;)V

    .line 478489
    invoke-virtual {p0}, Lcom/facebook/location/BaseFbLocationManager;->b()V

    .line 478490
    invoke-static {}, LX/2vl;->values()[LX/2vl;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 478491
    iget-object v4, p0, Lcom/facebook/location/BaseFbLocationManager;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v5, v3, LX/2vl;->perfMarkerId:I

    iget-object v3, v3, LX/2vl;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 478492
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 478493
    :cond_1
    const-string v0, ""

    invoke-static {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/BaseFbLocationManager;Ljava/lang/String;)V

    .line 478494
    invoke-static {p0}, Lcom/facebook/location/BaseFbLocationManager;->h(Lcom/facebook/location/BaseFbLocationManager;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 478478
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fb_location_no_recent_cached"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "location"

    .line 478479
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 478480
    move-object v0, v0

    .line 478481
    const-string v1, "caller_context_class"

    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 478482
    iget-object v3, v2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v3

    .line 478483
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "caller_context_tag"

    iget-object v2, p0, Lcom/facebook/location/BaseFbLocationManager;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "impl"

    invoke-virtual {p0}, Lcom/facebook/location/BaseFbLocationManager;->a()LX/1wZ;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 478484
    iget-object v1, p0, Lcom/facebook/location/BaseFbLocationManager;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 478485
    return-void
.end method
