.class public final Lcom/facebook/location/GooglePlayFbLocationManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2vg;


# direct methods
.method public constructor <init>(LX/2vg;)V
    .locals 0

    .prologue
    .line 478797
    iput-object p1, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 478798
    iget-object v1, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    monitor-enter v1

    .line 478799
    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    iget-boolean v0, v0, LX/2vg;->j:Z

    if-nez v0, :cond_0

    .line 478800
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478801
    :goto_0
    return-void

    .line 478802
    :cond_0
    :try_start_1
    sget-object v0, LX/2vm;->b:LX/2vu;

    iget-object v2, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    iget-object v2, v2, LX/2vg;->h:LX/2wX;

    iget-object v4, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    iget-object v4, v4, LX/2vg;->g:LX/2vk;

    .line 478803
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v6

    .line 478804
    iget-object v7, v4, LX/2vk;->a:LX/0yF;

    invoke-static {v7}, LX/2xI;->a(LX/0yF;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    .line 478805
    iget-wide v8, v4, LX/2vk;->e:J

    invoke-virtual {v6, v8, v9}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    .line 478806
    move-object v5, v6

    .line 478807
    move-object v3, v5

    .line 478808
    iget-object v4, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    iget-object v4, v4, LX/2vg;->i:LX/2vh;

    invoke-interface {v0, v2, v3, v4}, LX/2vu;->a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;LX/2vi;)LX/2wg;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478809
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 478810
    :catch_0
    :try_start_3
    iget-object v0, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    .line 478811
    invoke-static {v0}, LX/2vg;->g$redex0(LX/2vg;)V

    .line 478812
    iget-object v0, p0, Lcom/facebook/location/GooglePlayFbLocationManager$2;->a:LX/2vg;

    new-instance v2, LX/2Ce;

    sget-object v3, LX/2sk;->PERMISSION_DENIED:LX/2sk;

    invoke-direct {v2, v3}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-virtual {v0, v2}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2Ce;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
