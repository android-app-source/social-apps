.class public Lcom/facebook/location/CdmaCellInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/CdmaCellInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/Double;

.field public final e:Ljava/lang/Double;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478285
    new-instance v0, LX/6ZB;

    invoke-direct {v0}, LX/6ZB;-><init>()V

    sput-object v0, Lcom/facebook/location/CdmaCellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IIILjava/lang/Double;Ljava/lang/Double;)V
    .locals 0

    .prologue
    .line 478286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478287
    iput p1, p0, Lcom/facebook/location/CdmaCellInfo;->a:I

    .line 478288
    iput p2, p0, Lcom/facebook/location/CdmaCellInfo;->b:I

    .line 478289
    iput p3, p0, Lcom/facebook/location/CdmaCellInfo;->c:I

    .line 478290
    iput-object p4, p0, Lcom/facebook/location/CdmaCellInfo;->d:Ljava/lang/Double;

    .line 478291
    iput-object p5, p0, Lcom/facebook/location/CdmaCellInfo;->e:Ljava/lang/Double;

    .line 478292
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 478293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478294
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/location/CdmaCellInfo;->a:I

    .line 478295
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/location/CdmaCellInfo;->b:I

    .line 478296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/location/CdmaCellInfo;->c:I

    .line 478297
    const-class v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/location/CdmaCellInfo;->d:Ljava/lang/Double;

    .line 478298
    const-class v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    iput-object v0, p0, Lcom/facebook/location/CdmaCellInfo;->e:Ljava/lang/Double;

    .line 478299
    return-void
.end method

.method private static a(I)D
    .locals 4

    .prologue
    .line 478300
    int-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, -0x278d00

    if-lt p0, v0, :cond_0

    const v0, 0x278d00

    if-le p0, v0, :cond_1

    .line 478301
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid coordiante value:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478302
    :cond_1
    int-to-double v0, p0

    const-wide v2, 0x40cc200000000000L    # 14400.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/telephony/cdma/CdmaCellLocation;)Lcom/facebook/location/CdmaCellInfo;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 478303
    :try_start_0
    invoke-virtual {p0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/location/CdmaCellInfo;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 478304
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/location/CdmaCellInfo;->a(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 478305
    :goto_0
    new-instance v0, Lcom/facebook/location/CdmaCellInfo;

    invoke-virtual {p0}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v1

    invoke-virtual {p0}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v2

    invoke-virtual {p0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/location/CdmaCellInfo;-><init>(IIILjava/lang/Double;Ljava/lang/Double;)V

    return-object v0

    :catch_0
    move-object v4, v5

    goto :goto_0

    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 478306
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 478307
    iget v0, p0, Lcom/facebook/location/CdmaCellInfo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 478308
    iget v0, p0, Lcom/facebook/location/CdmaCellInfo;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 478309
    iget v0, p0, Lcom/facebook/location/CdmaCellInfo;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 478310
    iget-object v0, p0, Lcom/facebook/location/CdmaCellInfo;->d:Ljava/lang/Double;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 478311
    iget-object v0, p0, Lcom/facebook/location/CdmaCellInfo;->e:Ljava/lang/Double;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 478312
    return-void
.end method
