.class public Lcom/facebook/location/GeneralCellInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/GeneralCellInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Z

.field public final k:Lcom/facebook/location/CdmaCellInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478199
    new-instance v0, LX/2TX;

    invoke-direct {v0}, LX/2TX;-><init>()V

    sput-object v0, Lcom/facebook/location/GeneralCellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 478200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->a:Ljava/lang/String;

    .line 478202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->b:Ljava/lang/String;

    .line 478203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->c:Ljava/lang/String;

    .line 478204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->d:Ljava/lang/String;

    .line 478205
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/location/GeneralCellInfo;->e:Z

    .line 478206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    .line 478207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->g:Ljava/lang/String;

    .line 478208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->h:Ljava/lang/String;

    .line 478209
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->i:Ljava/lang/String;

    .line 478210
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/location/GeneralCellInfo;->j:Z

    .line 478211
    const-class v0, Lcom/facebook/location/CdmaCellInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/CdmaCellInfo;

    iput-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->k:Lcom/facebook/location/CdmaCellInfo;

    .line 478212
    return-void

    :cond_0
    move v0, v2

    .line 478213
    goto :goto_0

    :cond_1
    move v1, v2

    .line 478214
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/location/CdmaCellInfo;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/location/CdmaCellInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 478215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478216
    iput-object p1, p0, Lcom/facebook/location/GeneralCellInfo;->a:Ljava/lang/String;

    .line 478217
    iput-object p2, p0, Lcom/facebook/location/GeneralCellInfo;->b:Ljava/lang/String;

    .line 478218
    iput-object p3, p0, Lcom/facebook/location/GeneralCellInfo;->c:Ljava/lang/String;

    .line 478219
    iput-object p4, p0, Lcom/facebook/location/GeneralCellInfo;->d:Ljava/lang/String;

    .line 478220
    iput-boolean p5, p0, Lcom/facebook/location/GeneralCellInfo;->e:Z

    .line 478221
    iput-object p6, p0, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    .line 478222
    iput-object p7, p0, Lcom/facebook/location/GeneralCellInfo;->g:Ljava/lang/String;

    .line 478223
    iput-object p8, p0, Lcom/facebook/location/GeneralCellInfo;->h:Ljava/lang/String;

    .line 478224
    iput-object p9, p0, Lcom/facebook/location/GeneralCellInfo;->i:Ljava/lang/String;

    .line 478225
    iput-boolean p10, p0, Lcom/facebook/location/GeneralCellInfo;->j:Z

    .line 478226
    iput-object p11, p0, Lcom/facebook/location/GeneralCellInfo;->k:Lcom/facebook/location/CdmaCellInfo;

    .line 478227
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 478228
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 478229
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/0Qh;->toStringHelper(Ljava/lang/Class;)LX/0zA;

    move-result-object v0

    const-string v1, "phoneType"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "simCountryIso"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "simOperatorMccMnc"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "simOperatorName"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "hasIccCard"

    iget-boolean v2, p0, Lcom/facebook/location/GeneralCellInfo;->e:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "networkType"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "networkCountryIso"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "networkOperatorMccMnc"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "networkOperatorName"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "isNetworkRoaming"

    iget-boolean v2, p0, Lcom/facebook/location/GeneralCellInfo;->j:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "cdmaCellInfo"

    iget-object v2, p0, Lcom/facebook/location/GeneralCellInfo;->k:Lcom/facebook/location/CdmaCellInfo;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 478230
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478231
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478232
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478233
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478234
    iget-boolean v0, p0, Lcom/facebook/location/GeneralCellInfo;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 478235
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478236
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478237
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478238
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478239
    iget-boolean v0, p0, Lcom/facebook/location/GeneralCellInfo;->j:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 478240
    iget-object v0, p0, Lcom/facebook/location/GeneralCellInfo;->k:Lcom/facebook/location/CdmaCellInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 478241
    return-void

    :cond_0
    move v0, v2

    .line 478242
    goto :goto_0

    :cond_1
    move v1, v2

    .line 478243
    goto :goto_1
.end method
