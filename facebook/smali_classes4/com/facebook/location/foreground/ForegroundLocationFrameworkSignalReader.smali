.class public Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Cc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1rl;

.field private final f:LX/1sE;

.field public final g:LX/1ra;

.field public final h:LX/1sN;

.field public final i:LX/03V;

.field public final j:Ljava/util/concurrent/ExecutorService;

.field public final k:LX/0Uo;

.field public final l:LX/0SG;

.field private m:Lcom/facebook/location/FbLocationOperationParams;

.field private n:LX/1sO;

.field public o:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 478113
    const-class v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    const-string v1, "foreground_location_framework"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tf;LX/0Or;LX/0Or;LX/1rl;LX/1sE;LX/1ra;LX/1sN;LX/03V;LX/0Uo;LX/0SG;)V
    .locals 0
    .param p2    # LX/0Tf;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Tf;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Cc;",
            ">;",
            "LX/1rl;",
            "LX/1sE;",
            "LX/1ra;",
            "LX/1sN;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Uo;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 478100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478101
    iput-object p1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b:Landroid/content/Context;

    .line 478102
    iput-object p3, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->c:LX/0Or;

    .line 478103
    iput-object p4, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->d:LX/0Or;

    .line 478104
    iput-object p5, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 478105
    iput-object p6, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->f:LX/1sE;

    .line 478106
    iput-object p7, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    .line 478107
    iput-object p9, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->i:LX/03V;

    .line 478108
    iput-object p8, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->h:LX/1sN;

    .line 478109
    iput-object p2, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->j:Ljava/util/concurrent/ExecutorService;

    .line 478110
    iput-object p10, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->k:LX/0Uo;

    .line 478111
    iput-object p11, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->l:LX/0SG;

    .line 478112
    return-void
.end method

.method public static a$redex0(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;LX/2TS;)V
    .locals 9

    .prologue
    .line 478060
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-nez v0, :cond_0

    .line 478061
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    invoke-virtual {v0, p1}, LX/1ra;->a(LX/2TS;)V

    .line 478062
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->h:LX/1sN;

    invoke-virtual {v0}, LX/1sN;->c()V

    .line 478063
    :goto_0
    return-void

    .line 478064
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    const/4 v1, 0x1

    .line 478065
    invoke-static {v0, v1}, LX/1ra;->a(LX/1ra;Z)V

    .line 478066
    invoke-static {v0, v1}, LX/1ra;->b(LX/1ra;Z)V

    .line 478067
    invoke-static {v0, v1}, LX/1ra;->c(LX/1ra;Z)V

    .line 478068
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1ra;->d(LX/1ra;Z)V

    .line 478069
    iget v1, v0, LX/1ra;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1ra;->l:I

    .line 478070
    const-string v1, "fgl_scan_success"

    invoke-static {v0, v1}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    .line 478071
    if-eqz v3, :cond_1

    .line 478072
    iget-object v1, v0, LX/1ra;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v5

    .line 478073
    iget-object v1, v0, LX/1ra;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v7

    move-object v2, v0

    move-object v4, p1

    .line 478074
    invoke-static/range {v2 .. v8}, LX/1ra;->a(LX/1ra;LX/0oG;LX/2TS;JJ)V

    .line 478075
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 478076
    :cond_1
    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, v0, LX/1ra;->g:J

    .line 478077
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->f:LX/1sE;

    .line 478078
    invoke-virtual {v0}, LX/1sE;->a()V

    .line 478079
    iget-object v1, v0, LX/1sE;->d:LX/1ra;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 478080
    invoke-static {v1, v5}, LX/1ra;->a(LX/1ra;Z)V

    .line 478081
    invoke-static {v1, v5}, LX/1ra;->b(LX/1ra;Z)V

    .line 478082
    invoke-static {v1, v4}, LX/1ra;->c(LX/1ra;Z)V

    .line 478083
    invoke-static {v1, v4}, LX/1ra;->d(LX/1ra;Z)V

    .line 478084
    iget v4, v1, LX/1ra;->m:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, LX/1ra;->m:I

    .line 478085
    iget-object v4, v1, LX/1ra;->c:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v1, LX/1ra;->h:J

    .line 478086
    const-string v4, "fgl_write_start"

    invoke-static {v1, v4}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    .line 478087
    if-eqz v4, :cond_2

    .line 478088
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 478089
    :cond_2
    new-instance v1, LX/3Fy;

    invoke-direct {v1}, LX/3Fy;-><init>()V

    move-object v1, v1

    .line 478090
    const-string v2, "input"

    invoke-static {v0, p1}, LX/1sE;->b(LX/1sE;LX/2TS;)LX/2vN;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 478091
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 478092
    iget-object v2, v0, LX/1sE;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 478093
    new-instance v2, LX/3G9;

    invoke-direct {v2, v0}, LX/3G9;-><init>(LX/1sE;)V

    invoke-static {v1, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    iput-object v2, v0, LX/1sE;->g:LX/1Mv;

    .line 478094
    iget-object v2, v0, LX/1sE;->g:LX/1Mv;

    .line 478095
    iget-object v3, v2, LX/1Mv;->b:LX/0Ve;

    move-object v2, v3

    .line 478096
    iget-object v3, v0, LX/1sE;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 478097
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->h:LX/1sN;

    .line 478098
    const-string v1, "FOREGROUND_LOCATION_AVAILABLE"

    invoke-static {v0, v1}, LX/1sN;->a(LX/1sN;Ljava/lang/String;)V

    .line 478099
    goto/16 :goto_0
.end method

.method public static b(LX/1sR;)Z
    .locals 1

    .prologue
    .line 478027
    sget-object v0, LX/1sR;->MEDIUM_POWER:LX/1sR;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1sR;->LOW_POWER:LX/1sR;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)Lcom/facebook/location/GeneralCellInfo;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 478053
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/telephony/TelephonyManager;

    .line 478054
    if-nez v10, :cond_0

    .line 478055
    :goto_0
    return-object v12

    .line 478056
    :cond_0
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v11

    .line 478057
    new-instance v0, Lcom/facebook/location/GeneralCellInfo;

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    invoke-static {v1}, LX/0km;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    move-result v5

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v6

    invoke-static {v6}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v10

    instance-of v13, v11, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v13, :cond_1

    check-cast v11, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-static {v11}, Lcom/facebook/location/CdmaCellInfo;->a(Landroid/telephony/cdma/CdmaCellLocation;)Lcom/facebook/location/CdmaCellInfo;

    move-result-object v11

    :goto_1
    invoke-direct/range {v0 .. v11}, Lcom/facebook/location/GeneralCellInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/location/CdmaCellInfo;)V

    move-object v12, v0

    .line 478058
    goto :goto_0

    :cond_1
    move-object v11, v12

    .line 478059
    goto :goto_1
.end method

.method public static d(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)Lcom/facebook/location/FbLocationOperationParams;
    .locals 12

    .prologue
    .line 478039
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->m:Lcom/facebook/location/FbLocationOperationParams;

    if-nez v0, :cond_0

    .line 478040
    sget-object v0, LX/0yF;->BALANCED_POWER_AND_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    invoke-virtual {v1}, LX/1rl;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 478041
    sget-wide v6, LX/1rm;->b:J

    sget-wide v8, LX/0X5;->eM:J

    const-wide/16 v10, 0x1388

    move-object v5, v1

    invoke-static/range {v5 .. v11}, LX/1rl;->a(LX/1rl;JJJ)J

    move-result-wide v4

    move-wide v2, v4

    .line 478042
    iput-wide v2, v0, LX/1S7;->b:J

    .line 478043
    move-object v0, v0

    .line 478044
    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 478045
    sget v4, LX/1rm;->a:F

    sget-wide v6, LX/0X5;->eN:J

    const/16 v5, 0x64

    invoke-static {v1, v4, v6, v7, v5}, LX/1rl;->a(LX/1rl;FJI)F

    move-result v4

    move v1, v4

    .line 478046
    iput v1, v0, LX/1S7;->c:F

    .line 478047
    move-object v0, v0

    .line 478048
    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    invoke-virtual {v1}, LX/1rl;->e()J

    move-result-wide v2

    .line 478049
    iput-wide v2, v0, LX/1S7;->d:J

    .line 478050
    move-object v0, v0

    .line 478051
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->m:Lcom/facebook/location/FbLocationOperationParams;

    .line 478052
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->m:Lcom/facebook/location/FbLocationOperationParams;

    return-object v0
.end method

.method public static e(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)LX/1sO;
    .locals 6

    .prologue
    .line 478036
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->n:LX/1sO;

    if-nez v0, :cond_0

    .line 478037
    new-instance v0, LX/1sO;

    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    invoke-virtual {v1}, LX/1rl;->e()J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    invoke-virtual {v1}, LX/1rl;->a()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/1sO;-><init>(JJ)V

    iput-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->n:LX/1sO;

    .line 478038
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->n:LX/1sO;

    return-object v0
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 478028
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    if-eqz v0, :cond_0

    .line 478029
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 478030
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    .line 478031
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    const-wide/high16 v2, -0x8000000000000000L

    .line 478032
    iput-wide v2, v0, LX/1ra;->g:J

    .line 478033
    iput-wide v2, v0, LX/1ra;->f:J

    .line 478034
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->f:LX/1sE;

    invoke-virtual {v0}, LX/1sE;->a()V

    .line 478035
    return-void
.end method
