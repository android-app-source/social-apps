.class public Lcom/facebook/location/LocationSignalDataPackage;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/LocationSignalDataPackage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/location/ImmutableLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/wifiscan/WifiScanResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/location/GeneralCellInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/blescan/BleScanResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478137
    new-instance v0, LX/2Tk;

    invoke-direct {v0}, LX/2Tk;-><init>()V

    sput-object v0, Lcom/facebook/location/LocationSignalDataPackage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2TT;)V
    .locals 1

    .prologue
    .line 478128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478129
    iget-object v0, p1, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    .line 478130
    iget-object v0, p1, LX/2TT;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    .line 478131
    iget-object v0, p1, LX/2TT;->c:Lcom/facebook/wifiscan/WifiScanResult;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 478132
    iget-object v0, p1, LX/2TT;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    .line 478133
    iget-object v0, p1, LX/2TT;->e:Lcom/facebook/location/GeneralCellInfo;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    .line 478134
    iget-object v0, p1, LX/2TT;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    .line 478135
    iget-object v0, p1, LX/2TT;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->g:Ljava/util/List;

    .line 478136
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 478138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478139
    const-class v0, Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    .line 478140
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    .line 478141
    const-class v0, Lcom/facebook/wifiscan/WifiScanResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    .line 478142
    const-class v0, Lcom/facebook/wifiscan/WifiScanResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/wifiscan/WifiScanResult;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 478143
    const-class v0, Lcom/facebook/location/GeneralCellInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/GeneralCellInfo;

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    .line 478144
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 478145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    .line 478146
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    sget-object v1, Landroid/telephony/CellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 478147
    :goto_0
    const-class v0, Lcom/facebook/blescan/BleScanResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->g:Ljava/util/List;

    .line 478148
    return-void

    .line 478149
    :cond_0
    iput-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 478127
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 478126
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "location_manager_info"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "is_user_in_app"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "wifi_scan_results"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "connected_wifi"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "general_cell_info"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "cell_scan"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "ble_scan_results"

    iget-object v2, p0, Lcom/facebook/location/LocationSignalDataPackage;->g:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 478117
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 478118
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 478119
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 478120
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 478121
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 478122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 478123
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 478124
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/LocationSignalDataPackage;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 478125
    return-void
.end method
