.class public abstract Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FeedUnit::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        "E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TFeedUnit;>;",
        "LX/C3x;",
        "TE;",
        "LX/C3y;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 364025
    new-instance v0, LX/3XV;

    invoke-direct {v0}, LX/3XV;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 364026
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 364027
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 364028
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 364029
    sget-object v0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x537c396f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364030
    check-cast p2, LX/C3x;

    check-cast p4, LX/C3y;

    .line 364031
    iget v1, p2, LX/C3x;->f:I

    invoke-virtual {p4, v1}, LX/C3y;->setContentSummaryColor(I)V

    .line 364032
    iget-object v1, p2, LX/C3x;->a:Ljava/lang/String;

    iget-object v2, p2, LX/C3x;->b:Ljava/lang/CharSequence;

    iget-object p1, p2, LX/C3x;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p4, v1, v2, p1}, LX/C3y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 364033
    iget v1, p2, LX/C3x;->l:F

    .line 364034
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 364035
    iget-object v2, p4, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 364036
    invoke-virtual {p4}, LX/C3y;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p1

    iput p1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 364037
    :cond_0
    iget-boolean v1, p2, LX/C3x;->j:Z

    invoke-virtual {p4, v1}, LX/C3y;->setOverlapMode(Z)V

    .line 364038
    iget-boolean v1, p2, LX/C3x;->d:Z

    invoke-virtual {p4, v1}, LX/C3y;->setMenuButtonActive(Z)V

    .line 364039
    iget-object v1, p2, LX/C3x;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iget-boolean v2, p2, LX/C3x;->g:Z

    iget-object p1, p0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2, p1}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 364040
    iget-object v1, p2, LX/C3x;->h:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v2, p0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 364041
    iget-object v1, p2, LX/C3x;->i:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v2, p0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 364042
    iput-object v1, p4, LX/C3y;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 364043
    iget-object p1, p4, LX/C3y;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p0, 0x0

    invoke-static {p4, v1, p1, v2, p0}, LX/C3y;->a(LX/C3y;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;I)V

    .line 364044
    iget v1, p2, LX/C3x;->k:I

    .line 364045
    invoke-virtual {p4}, LX/C3y;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b1cbd

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 364046
    if-nez v1, :cond_1

    .line 364047
    const/4 v2, 0x0

    .line 364048
    :cond_1
    iget-object p1, p4, LX/C3y;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 364049
    iget-object v2, p4, LX/C3y;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 364050
    const/16 v1, 0x1f

    const v2, 0x371ccc56

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
