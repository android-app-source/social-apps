.class public Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0s6;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;LX/0s6;LX/0Uh;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499612
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 499613
    iput-object p1, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    .line 499614
    iput-object p2, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->c:LX/0s6;

    .line 499615
    iput-object p3, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->d:LX/0Uh;

    .line 499616
    iput-object p4, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    .line 499617
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/8TZ;LX/8Ta;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 6

    .prologue
    .line 499659
    new-instance v0, LX/C4d;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/C4d;-><init>(Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;LX/8TZ;LX/8Ta;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;
    .locals 7

    .prologue
    .line 499648
    const-class v1, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    monitor-enter v1

    .line 499649
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499650
    sput-object v2, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499651
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499652
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499653
    new-instance p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;-><init>(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;LX/0s6;LX/0Uh;Lcom/facebook/content/SecureContextHelper;)V

    .line 499654
    move-object v0, p0

    .line 499655
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499656
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499657
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3

    .prologue
    const v2, 0x1172f9ae

    .line 499660
    invoke-static {p0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499661
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    move-object v1, v1

    .line 499662
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 499663
    :goto_0
    return-object v0

    .line 499664
    :cond_0
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499665
    if-nez v0, :cond_1

    .line 499666
    const/4 v0, 0x0

    goto :goto_0

    .line 499667
    :cond_1
    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499647
    sget-object v0, LX/3WJ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 499625
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v7, 0x0

    .line 499626
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499627
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499628
    invoke-static {v0}, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 499629
    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    .line 499630
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 499631
    new-instance v3, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 499632
    sget-object v2, LX/8TZ;->FB_FEED_SCREENSHOT:LX/8TZ;

    .line 499633
    sget-object v1, LX/8Ta;->Story:LX/8Ta;

    .line 499634
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    sget-object v6, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v5, v6, :cond_1

    .line 499635
    sget-object v2, LX/8TZ;->FB_GROUP_MALL_SCREENSHOT:LX/8TZ;

    .line 499636
    sget-object v1, LX/8Ta;->Group:LX/8Ta;

    .line 499637
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 499638
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    move-object v8, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v8

    .line 499639
    :goto_0
    invoke-direct {p0, v4, v1, v0, v2}, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/8TZ;LX/8Ta;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p2, v7, v0, v1, v7}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)LX/C2O;

    move-result-object v0

    .line 499640
    iget-object v1, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 499641
    :cond_0
    return-object v7

    .line 499642
    :cond_1
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    sget-object v6, LX/1Qt;->FEED:LX/1Qt;

    if-ne v5, v6, :cond_2

    invoke-static {v0}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 499643
    sget-object v2, LX/8TZ;->FB_GROUP_NEWSFEED_SCREENSHOT:LX/8TZ;

    .line 499644
    sget-object v1, LX/8Ta;->Group:LX/8Ta;

    .line 499645
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 499646
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    move-object v8, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_0

    :cond_2
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 499618
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499619
    iget-object v0, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->c:LX/0s6;

    invoke-static {v0}, LX/36d;->a(LX/0s6;)Z

    move-result v2

    .line 499620
    iget-object v0, p0, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->d:LX/0Uh;

    const/16 v3, 0x4c3

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 499621
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499622
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499623
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 499624
    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
