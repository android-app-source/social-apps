.class public Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1b0;

.field public final c:LX/1Ck;

.field public final d:LX/36e;

.field public final e:LX/0kL;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;LX/1b0;LX/1Ck;LX/36e;LX/0kL;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499744
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 499745
    iput-object p1, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    .line 499746
    iput-object p2, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->b:LX/1b0;

    .line 499747
    iput-object p3, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->c:LX/1Ck;

    .line 499748
    iput-object p4, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->d:LX/36e;

    .line 499749
    iput-object p5, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->e:LX/0kL;

    .line 499750
    iput-object p6, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->f:LX/0ad;

    .line 499751
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 499752
    new-instance v0, LX/C3r;

    invoke-direct {v0, p0, p1}, LX/C3r;-><init>(Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;
    .locals 10

    .prologue
    .line 499753
    const-class v1, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    monitor-enter v1

    .line 499754
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499755
    sput-object v2, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499756
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499757
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499758
    new-instance v3, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-static {v0}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v5

    check-cast v5, LX/1b0;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/36e;->b(LX/0QB;)LX/36e;

    move-result-object v7

    check-cast v7, LX/36e;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;-><init>(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;LX/1b0;LX/1Ck;LX/36e;LX/0kL;LX/0ad;)V

    .line 499759
    move-object v0, v3

    .line 499760
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499761
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499762
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499763
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Landroid/app/Activity;Ljava/lang/String;LX/0Px;)V
    .locals 6
    .param p1    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 499764
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    const-string v1, "framesCallToAction"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 499765
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 499766
    iget-object v0, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->b:LX/1b0;

    const/4 v2, 0x5

    new-instance v3, LX/89V;

    invoke-direct {v3}, LX/89V;-><init>()V

    const/4 v5, 0x1

    .line 499767
    iput-boolean v5, v3, LX/89V;->b:Z

    .line 499768
    move-object v3, v3

    .line 499769
    sget-object v5, LX/4gI;->PHOTO_ONLY:LX/4gI;

    .line 499770
    iput-object v5, v3, LX/89V;->f:LX/4gI;

    .line 499771
    move-object v3, v3

    .line 499772
    iput-object p3, v3, LX/89V;->h:LX/0Px;

    .line 499773
    move-object v3, v3

    .line 499774
    iput-object p2, v3, LX/89V;->i:Ljava/lang/String;

    .line 499775
    move-object v3, v3

    .line 499776
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 499777
    iput-object v1, v3, LX/89V;->k:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 499778
    move-object v1, v3

    .line 499779
    invoke-virtual {v1}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, LX/1b0;->a(Landroid/app/Activity;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 499780
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499781
    sget-object v0, LX/3WJ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 499782
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    .line 499783
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499784
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499785
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499786
    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020b28

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p2, v1, v0, v4, v2}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)LX/C2O;

    move-result-object v0

    .line 499787
    iget-object v1, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 499788
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499789
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499790
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499791
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499792
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499793
    if-nez v0, :cond_0

    move v0, v1

    .line 499794
    :goto_0
    return v0

    .line 499795
    :cond_0
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499796
    if-nez v0, :cond_1

    move v0, v1

    .line 499797
    goto :goto_0

    .line 499798
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 499799
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x271d842a

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->f:LX/0ad;

    sget-short v2, LX/8Fn;->c:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
