.class public Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535597
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 535598
    iput-object p1, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 535599
    iput-object p2, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 535600
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;
    .locals 5

    .prologue
    .line 535617
    const-class v1, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    monitor-enter v1

    .line 535618
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535619
    sput-object v2, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535620
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535621
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535622
    new-instance p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V

    .line 535623
    move-object v0, p0

    .line 535624
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535625
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535626
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 535616
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 535612
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535613
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535614
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v1, LX/24b;

    sget-object v2, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v1, p2, v2}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535615
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2856cc64

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 535602
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/2ee;

    .line 535603
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 535604
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 535605
    if-eqz v1, :cond_0

    .line 535606
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 535607
    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 535608
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 535609
    check-cast v1, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, v1, v2}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 535610
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 535611
    const/16 v1, 0x1f

    const v2, 0x291556d5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 535601
    const/4 v0, 0x1

    return v0
.end method
