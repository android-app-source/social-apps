.class public Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

.field private final c:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535567
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 535568
    iput-object p1, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->a:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    .line 535569
    iput-object p2, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    .line 535570
    iput-object p3, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->c:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    .line 535571
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;
    .locals 6

    .prologue
    .line 535572
    const-class v1, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;

    monitor-enter v1

    .line 535573
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535574
    sput-object v2, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535577
    new-instance p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;-><init>(Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;)V

    .line 535578
    move-object v0, p0

    .line 535579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 535583
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535584
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->a:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 535585
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 535586
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesPartDefinition;->c:Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 535587
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 535588
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 535589
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535590
    if-eqz v0, :cond_0

    .line 535591
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535592
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 535593
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535594
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 535595
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535596
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
