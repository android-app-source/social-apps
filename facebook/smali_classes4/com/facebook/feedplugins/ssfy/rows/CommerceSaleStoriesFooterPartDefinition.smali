.class public Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final f:LX/0Zb;

.field private g:LX/1EV;

.field public final h:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 536191
    new-instance v0, LX/2vH;

    invoke-direct {v0}, LX/2vH;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->a:LX/1Cz;

    .line 536192
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 536193
    iput v1, v0, LX/1UY;->b:F

    .line 536194
    move-object v0, v0

    .line 536195
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Zb;LX/1EV;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536196
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 536197
    iput-object p1, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 536198
    iput-object p2, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    .line 536199
    iput-object p3, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 536200
    iput-object p4, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->f:LX/0Zb;

    .line 536201
    iput-object p5, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->g:LX/1EV;

    .line 536202
    iput-object p6, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->h:LX/17W;

    .line 536203
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;
    .locals 10

    .prologue
    .line 536204
    const-class v1, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    monitor-enter v1

    .line 536205
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536206
    sput-object v2, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536207
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536208
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536209
    new-instance v3, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v8

    check-cast v8, LX/1EV;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Zb;LX/1EV;LX/17W;)V

    .line 536210
    move-object v0, v3

    .line 536211
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536212
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536213
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 536215
    sget-object v0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 536216
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 536217
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536218
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 536219
    new-instance v2, LX/JYz;

    invoke-direct {v2, p0, v1}, LX/JYz;-><init>(Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;LX/1PT;)V

    move-object v1, v2

    .line 536220
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536221
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    const v2, 0x7f081032

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536222
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 536223
    iget-object v0, p0, Lcom/facebook/feedplugins/ssfy/rows/CommerceSaleStoriesFooterPartDefinition;->g:LX/1EV;

    .line 536224
    iget-object v1, v0, LX/1EV;->a:LX/0Uh;

    const/16 p0, 0x30d

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 536225
    return v0
.end method
