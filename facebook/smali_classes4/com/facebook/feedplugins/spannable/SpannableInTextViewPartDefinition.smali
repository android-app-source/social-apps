.class public Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1xz;",
        "LX/1yT;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1xu;

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(LX/1xu;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496132
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 496133
    iput-object p1, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a:LX/1xu;

    .line 496134
    iput-object p2, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->b:LX/03V;

    .line 496135
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
    .locals 5

    .prologue
    .line 496136
    const-class v1, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    monitor-enter v1

    .line 496137
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496138
    sput-object v2, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496139
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496140
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496141
    new-instance p0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v3

    check-cast v3, LX/1xu;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;-><init>(LX/1xu;LX/03V;)V

    .line 496142
    move-object v0, p0

    .line 496143
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496144
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496145
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 496147
    check-cast p2, LX/1xz;

    check-cast p3, LX/1Pr;

    .line 496148
    iget-object v0, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a:LX/1xu;

    invoke-virtual {v0, p2, p3}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 496149
    invoke-interface {p2}, LX/1xz;->a()LX/1KL;

    move-result-object v0

    invoke-interface {p2}, LX/1xz;->c()LX/0jW;

    move-result-object v1

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yT;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x18780e81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 496150
    check-cast p1, LX/1xz;

    check-cast p2, LX/1yT;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 496151
    :try_start_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 496152
    iget-object v1, p2, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 496153
    iget-object v1, p2, LX/1yT;->c:LX/2nR;

    if-eqz v1, :cond_0

    .line 496154
    iget-object v1, p2, LX/1yT;->c:LX/2nR;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setAttachDetachListener(LX/2nR;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496155
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x4db7364c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 496156
    :catch_0
    move-exception v1

    move-object v4, v1

    .line 496157
    invoke-interface {p1}, LX/1xz;->c()LX/0jW;

    move-result-object v1

    .line 496158
    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v1

    .line 496159
    :goto_1
    iget-object v5, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->b:LX/03V;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v2, "SpannableInTextViewPartDefinition"

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    const-string v2, "_withZombie"

    :goto_2
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "JellyBean setText bug with text: %s and zombie: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object p3, p2, LX/1yT;->a:Landroid/text/Spannable;

    aput-object p3, v7, v8

    const/4 v8, 0x1

    aput-object v1, v7, v8

    invoke-static {v6, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 496160
    iput-object v4, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 496161
    move-object v1, v1

    .line 496162
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/03V;->a(LX/0VG;)V

    .line 496163
    iget-object v1, p2, LX/1yT;->a:Landroid/text/Spannable;

    if-eqz v1, :cond_0

    .line 496164
    iget-object v1, p2, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 496165
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 496166
    :cond_2
    const-string v2, ""

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 496167
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 496168
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/widget/text/BetterTextView;->setAttachDetachListener(LX/2nR;)V

    .line 496169
    return-void
.end method
