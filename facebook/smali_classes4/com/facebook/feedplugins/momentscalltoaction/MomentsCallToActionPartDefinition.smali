.class public Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Vr;

.field public final c:Landroid/content/Context;

.field public final d:LX/C92;

.field public final e:LX/C93;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/content/Context;LX/C92;LX/C93;LX/1Vg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;",
            "Landroid/content/Context;",
            "LX/C92;",
            "LX/C93;",
            "LX/1Vg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499737
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 499738
    iput-object p1, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->a:LX/0Ot;

    .line 499739
    new-instance v0, LX/1Wx;

    invoke-direct {v0}, LX/1Wx;-><init>()V

    invoke-virtual {p5, v0}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->b:LX/1Vr;

    .line 499740
    iput-object p2, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c:Landroid/content/Context;

    .line 499741
    iput-object p3, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->d:LX/C92;

    .line 499742
    iput-object p4, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->e:LX/C93;

    .line 499743
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;
    .locals 9

    .prologue
    .line 499723
    const-class v1, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    monitor-enter v1

    .line 499724
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499725
    sput-object v2, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499726
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499727
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499728
    new-instance v3, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    const/16 v4, 0x871

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/C92;->a(LX/0QB;)LX/C92;

    move-result-object v6

    check-cast v6, LX/C92;

    .line 499729
    new-instance v8, LX/C93;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct {v8, v7}, LX/C93;-><init>(LX/0Zb;)V

    .line 499730
    move-object v7, v8

    .line 499731
    check-cast v7, LX/C93;

    const-class v8, LX/1Vg;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Vg;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;-><init>(LX/0Ot;Landroid/content/Context;LX/C92;LX/C93;LX/1Vg;)V

    .line 499732
    move-object v0, v3

    .line 499733
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499734
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499735
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Pf;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pf;",
            ">(TE;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 499722
    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-virtual {v0}, LX/1Qt;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 499718
    invoke-static {p0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 499719
    :cond_0
    :goto_0
    return v0

    .line 499720
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 499721
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x1c475be4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 499715
    invoke-static {p0}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499716
    const/4 v0, 0x0

    .line 499717
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499714
    sget-object v0, LX/3WJ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 499681
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v3, 0x0

    .line 499682
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499683
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499684
    invoke-static {p3}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->a(LX/1Pf;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 499685
    new-instance v2, LX/C94;

    invoke-direct {v2, p0, v1, v0}, LX/C94;-><init>(Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 499686
    iget-object v1, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020f1d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p2, v3, v0, v3, v1}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)LX/C2O;

    move-result-object v1

    .line 499687
    iget-object v0, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 499688
    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x838482b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 499702
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 499703
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 499704
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499705
    iget-object v2, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->d:LX/C92;

    invoke-virtual {v2, v1}, LX/C92;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 499706
    iget-object v2, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->e:LX/C93;

    invoke-static {p3}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->a(LX/1Pf;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v1}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 499707
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "moments_app_photo_overlay_call_to_action"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 499708
    const-string p1, "event_type"

    const-string p3, "impression"

    invoke-virtual {p0, p1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 499709
    const-string p1, "feed_name"

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 499710
    if-eqz v1, :cond_0

    .line 499711
    const-string p1, "promotion_tag"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 499712
    :cond_0
    iget-object p1, v2, LX/C93;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 499713
    const/16 v1, 0x1f

    const v2, -0x71577306

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    .line 499689
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499690
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499691
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499692
    invoke-static {v0}, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 499693
    :goto_0
    return v0

    .line 499694
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->b:LX/1Vr;

    invoke-virtual {v2}, LX/1Vr;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 499695
    goto :goto_0

    .line 499696
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->d:LX/C92;

    const/4 v3, 0x1

    .line 499697
    invoke-static {v1, v0}, LX/C92;->c(LX/C92;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 499698
    :cond_2
    :goto_1
    move v0, v3

    .line 499699
    goto :goto_0

    .line 499700
    :cond_3
    invoke-static {v1}, LX/C92;->g(LX/C92;)V

    .line 499701
    invoke-static {v1}, LX/C92;->e(LX/C92;)J

    move-result-wide v5

    invoke-static {v1}, LX/C92;->b(LX/C92;)I

    move-result v4

    int-to-long v7, v4

    cmp-long v4, v5, v7

    if-ltz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_1
.end method
