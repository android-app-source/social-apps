.class public Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Ua;

.field public static final b:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460617
    sget-object v0, LX/1Ua;->p:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a:LX/1Ua;

    .line 460618
    new-instance v0, LX/2mT;

    invoke-direct {v0}, LX/2mT;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460619
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460620
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->c:LX/0Ot;

    .line 460621
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;
    .locals 4

    .prologue
    .line 460622
    const-class v1, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    monitor-enter v1

    .line 460623
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460624
    sput-object v2, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460625
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460626
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460627
    new-instance v3, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    const/16 p0, 0x756

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;-><init>(LX/0Ot;)V

    .line 460628
    move-object v0, v3

    .line 460629
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460630
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460631
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460632
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460633
    sget-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 460634
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460635
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460636
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460637
    const/4 v0, 0x1

    return v0
.end method
