.class public Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3Dt;",
        "LX/3Dv;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/20e;

.field public final c:LX/36F;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/And;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/20e;LX/36F;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/20e;",
            "LX/36F;",
            "LX/0Ot",
            "<",
            "LX/And;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498504
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 498505
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a:LX/0Or;

    .line 498506
    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->b:LX/20e;

    .line 498507
    iput-object p3, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->c:LX/36F;

    .line 498508
    iput-object p4, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->d:LX/0Ot;

    .line 498509
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;
    .locals 7

    .prologue
    .line 498468
    const-class v1, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    monitor-enter v1

    .line 498469
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498470
    sput-object v2, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498471
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498472
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498473
    new-instance v5, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    const/16 v3, 0x13a4

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/20e;->b(LX/0QB;)LX/20e;

    move-result-object v3

    check-cast v3, LX/20e;

    invoke-static {v0}, LX/36F;->a(LX/0QB;)LX/36F;

    move-result-object v4

    check-cast v4, LX/36F;

    const/16 p0, 0x1ea2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v6, v3, v4, p0}, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;-><init>(LX/0Or;LX/20e;LX/36F;LX/0Ot;)V

    .line 498474
    move-object v0, v5

    .line 498475
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498476
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498477
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 498489
    check-cast p2, LX/3Dt;

    check-cast p3, LX/1Po;

    .line 498490
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    move-object v0, p3

    .line 498491
    check-cast v0, LX/1Pr;

    iget-object v1, p2, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498492
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 498493
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v1, v2}, LX/212;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/util/EnumMap;

    move-result-object v1

    .line 498494
    iget-object v0, p2, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498495
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 498496
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v2

    iget-object v0, p2, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498497
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 498498
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v3

    iget-object v0, p2, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498499
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 498500
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    iget-object v4, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a:LX/0Or;

    invoke-static {v2, v3, v0, v1, v4}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 498501
    iget-object v0, p2, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v2, p2, LX/3Dt;->b:Z

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    .line 498502
    new-instance v4, LX/3Du;

    invoke-direct {v4, p0, v0, v2, v3}, LX/3Du;-><init>(Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLX/1PT;)V

    move-object v0, v4

    .line 498503
    new-instance v2, LX/3Dv;

    invoke-direct {v2, v0, v1}, LX/3Dv;-><init>(LX/20Z;Ljava/util/EnumMap;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5e8d4b57

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 498481
    check-cast p1, LX/3Dt;

    check-cast p2, LX/3Dv;

    .line 498482
    move-object v1, p4

    check-cast v1, LX/1wK;

    iget-object v4, p2, LX/3Dv;->b:Ljava/util/EnumMap;

    iget-object v2, p1, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498483
    iget-object p3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, p3

    .line 498484
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object p3, p2, LX/3Dv;->a:LX/20Z;

    invoke-static {v1, v4, v2, p3}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 498485
    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->b:LX/20e;

    check-cast p4, LX/1wK;

    iget-object v1, p1, LX/3Dt;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498486
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 498487
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, p4, v1}, LX/20e;->a(LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 498488
    const/16 v1, 0x1f

    const v2, -0x4f1b7215

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 498479
    check-cast p4, LX/1wK;

    invoke-interface {p4}, LX/1wK;->a()V

    .line 498480
    return-void
.end method
