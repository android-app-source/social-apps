.class public Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Ans;",
        "LX/Ant;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/1We;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1We;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447002
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 447003
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a:Landroid/content/res/Resources;

    .line 447004
    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->b:LX/1We;

    .line 447005
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;
    .locals 5

    .prologue
    .line 446991
    const-class v1, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    monitor-enter v1

    .line 446992
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446993
    sput-object v2, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446994
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446995
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446996
    new-instance p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v4

    check-cast v4, LX/1We;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;-><init>(Landroid/content/res/Resources;LX/1We;)V

    .line 446997
    move-object v0, p0

    .line 446998
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446999
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447000
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 447001
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 447006
    check-cast p2, LX/Ans;

    .line 447007
    if-eqz p2, :cond_0

    iget-boolean v0, p2, LX/Ans;->a:Z

    if-eqz v0, :cond_0

    .line 447008
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->b:LX/1We;

    sget-object v1, LX/1Wi;->HAS_INLINE_SURVEY:LX/1Wi;

    invoke-virtual {v0, v1}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    .line 447009
    :goto_0
    iget-object v1, v0, LX/1Wj;->c:LX/1Wh;

    .line 447010
    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a:Landroid/content/res/Resources;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, v2, v3}, LX/1Wj;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 447011
    new-instance v2, LX/Ant;

    invoke-direct {v2, v1, v0}, LX/Ant;-><init>(LX/1Wh;Landroid/graphics/drawable/Drawable;)V

    return-object v2

    .line 447012
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->b:LX/1We;

    sget-object v1, LX/1Wi;->TOP:LX/1Wi;

    invoke-virtual {v0, v1}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x45384c1c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446985
    check-cast p2, LX/Ant;

    .line 446986
    iget-object v1, p2, LX/Ant;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 446987
    iget-object v1, p2, LX/Ant;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 446988
    :cond_0
    iget-object v1, p2, LX/Ant;->a:LX/1Wh;

    if-eqz v1, :cond_1

    .line 446989
    iget-object v1, p2, LX/Ant;->a:LX/1Wh;

    invoke-virtual {v1, p4}, LX/1Wh;->a(Landroid/view/View;)V

    .line 446990
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x75e80d21

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
