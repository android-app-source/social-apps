.class public Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3VG;",
        "E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Anm;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 535874
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 535875
    iput v1, v0, LX/1UY;->d:F

    .line 535876
    move-object v0, v0

    .line 535877
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535870
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535871
    iput-object p1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    .line 535872
    iput-object p2, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    .line 535873
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;
    .locals 5

    .prologue
    .line 535878
    const-class v1, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    monitor-enter v1

    .line 535879
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535880
    sput-object v2, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535881
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535882
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535883
    new-instance p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;)V

    .line 535884
    move-object v0, p0

    .line 535885
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535886
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535887
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 535863
    check-cast p2, LX/Anm;

    .line 535864
    iget-object v0, p2, LX/Anm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535865
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 535866
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 535867
    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    iget-object v2, p2, LX/Anm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535868
    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    new-instance v2, LX/Anj;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p2, LX/Anm;->a:Z

    invoke-direct {v2, v0, v3, v4, v5}, LX/Anj;-><init>(LX/0jW;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Z)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535869
    const/4 v0, 0x0

    return-object v0
.end method
