.class public Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3VG;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Anj;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ann;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ann;",
            ">;",
            "Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535889
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535890
    iput-object p1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->c:LX/0Ot;

    .line 535891
    iput-object p3, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 535892
    iput-object p2, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->b:LX/0Ot;

    .line 535893
    iput-object p4, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->d:LX/0ad;

    .line 535894
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;
    .locals 7

    .prologue
    .line 535895
    const-class v1, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    monitor-enter v1

    .line 535896
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535897
    sput-object v2, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535900
    new-instance v5, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    const/16 v3, 0x12a4

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x1ea4

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/0ad;)V

    .line 535901
    move-object v0, v5

    .line 535902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 535906
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->k()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 535907
    check-cast p2, LX/Anj;

    const/4 v5, 0x0

    .line 535908
    iget-object v8, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->a:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v0, LX/82M;

    new-instance v1, LX/Anh;

    iget-object v2, p2, LX/Anj;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-boolean v3, p2, LX/Anj;->d:Z

    invoke-direct {v1, v2, v3}, LX/Anh;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, LX/Anj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, LX/Anj;->a:LX/0jW;

    new-instance v4, LX/Ani;

    iget-object v6, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Rg;

    new-instance v9, LX/Ank;

    invoke-direct {v9, p0}, LX/Ank;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;)V

    iget-object v7, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Ann;

    invoke-direct {v4, v6, v9, v7}, LX/Ani;-><init>(LX/1Rg;LX/Ank;LX/Ann;)V

    invoke-direct/range {v0 .. v5}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535909
    return-object v5
.end method
