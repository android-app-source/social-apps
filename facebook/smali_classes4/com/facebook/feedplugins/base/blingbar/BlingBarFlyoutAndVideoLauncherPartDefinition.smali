.class public Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pe;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1EN;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:LX/14w;

.field public final d:LX/1WR;


# direct methods
.method public constructor <init>(LX/1EN;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/14w;LX/1WR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522521
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 522522
    iput-object p1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->a:LX/1EN;

    .line 522523
    iput-object p2, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 522524
    iput-object p3, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->c:LX/14w;

    .line 522525
    iput-object p4, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->d:LX/1WR;

    .line 522526
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;
    .locals 7

    .prologue
    .line 522527
    const-class v1, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    monitor-enter v1

    .line 522528
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522529
    sput-object v2, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522532
    new-instance p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    invoke-static {v0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v3

    check-cast v3, LX/1EN;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-static {v0}, LX/1WR;->a(LX/0QB;)LX/1WR;

    move-result-object v6

    check-cast v6, LX/1WR;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;-><init>(LX/1EN;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/14w;LX/1WR;)V

    .line 522533
    move-object v0, p0

    .line 522534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 522516
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 522517
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522518
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 522519
    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/Anl;

    invoke-direct {v2, p0, v0, p3, p2}, LX/Anl;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522520
    const/4 v0, 0x0

    return-object v0
.end method
