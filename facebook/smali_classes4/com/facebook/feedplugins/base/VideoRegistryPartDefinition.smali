.class public Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/Void;",
        "LX/1Pe;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461193
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 461194
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;
    .locals 3

    .prologue
    .line 461195
    const-class v1, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    monitor-enter v1

    .line 461196
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461197
    sput-object v2, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461198
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461199
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 461200
    new-instance v0, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;-><init>()V

    .line 461201
    move-object v0, v0

    .line 461202
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461203
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461204
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x711e8a21

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461206
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    check-cast p3, LX/1Pe;

    .line 461207
    invoke-interface {p3, p1, p4}, LX/1Pe;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 461208
    const/16 v1, 0x1f

    const v2, 0x7f8429bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 461209
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    check-cast p3, LX/1Pe;

    .line 461210
    invoke-interface {p3, p1}, LX/1Pe;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 461211
    return-void
.end method
