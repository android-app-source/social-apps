.class public Lcom/facebook/feedplugins/base/TextLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/350;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/34z;


# direct methods
.method public constructor <init>(LX/0Or;LX/34z;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/ShouldStoryMessageLinkToFlyout;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/34z;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527607
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 527608
    iput-object p1, p0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->a:LX/0Or;

    .line 527609
    iput-object p2, p0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->b:LX/34z;

    .line 527610
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/TextLinkPartDefinition;
    .locals 5

    .prologue
    .line 527611
    const-class v1, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    monitor-enter v1

    .line 527612
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527613
    sput-object v2, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527616
    new-instance v4, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    const/16 v3, 0x14b4

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v3, LX/34z;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/34z;

    invoke-direct {v4, p0, v3}, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;-><init>(LX/0Or;LX/34z;)V

    .line 527617
    move-object v0, v4

    .line 527618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 527622
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 527623
    iget-object v0, p0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 527624
    const/4 v0, 0x0

    .line 527625
    :goto_0
    return-object v0

    .line 527626
    :cond_0
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    .line 527627
    iget-object v1, p0, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->b:LX/34z;

    invoke-virtual {v1, v0}, LX/34z;->a(LX/1PT;)LX/31m;

    move-result-object v1

    .line 527628
    sget-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, p2, v0}, LX/31m;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;)V

    .line 527629
    new-instance v0, LX/350;

    invoke-direct {v0, v1}, LX/350;-><init>(LX/31j;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xc99376a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527630
    check-cast p2, LX/350;

    .line 527631
    if-eqz p2, :cond_0

    .line 527632
    invoke-virtual {p2, p4}, LX/350;->a(Landroid/view/View;)V

    .line 527633
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x5a3e552b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 527634
    check-cast p2, LX/350;

    .line 527635
    if-eqz p2, :cond_0

    .line 527636
    invoke-static {p4}, LX/350;->b(Landroid/view/View;)V

    .line 527637
    :cond_0
    return-void
.end method
