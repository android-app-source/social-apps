.class public Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Integer;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/39G;

.field private final b:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;


# direct methods
.method public constructor <init>(LX/39G;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522470
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 522471
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->a:LX/39G;

    .line 522472
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    .line 522473
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    .line 522474
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;
    .locals 6

    .prologue
    .line 522459
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    monitor-enter v1

    .line 522460
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522461
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522464
    new-instance p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-static {v0}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v3

    check-cast v3, LX/39G;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;-><init>(LX/39G;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;)V

    .line 522465
    move-object v0, p0

    .line 522466
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522467
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522468
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 522445
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522446
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522447
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 522448
    const/4 v1, 0x0

    .line 522449
    invoke-static {v0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 522450
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522451
    invoke-static {v0}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    .line 522452
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 522453
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1d531fa2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 522454
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 522455
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 522456
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 522457
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1, p2, p4}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 522458
    const/16 v1, 0x1f

    const v2, 0x75846fc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
