.class public Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/2dq;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

.field public final f:LX/1LV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 446779
    sget-object v0, LX/2eF;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446761
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 446762
    iput-object p2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->c:LX/2dq;

    .line 446763
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 446764
    iput-object p3, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 446765
    iput-object p4, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->e:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    .line 446766
    iput-object p5, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->f:LX/1LV;

    .line 446767
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;
    .locals 9

    .prologue
    .line 446768
    const-class v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;

    monitor-enter v1

    .line 446769
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446770
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446771
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446772
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446773
    new-instance v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v5

    check-cast v5, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v8

    check-cast v8, LX/1LV;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/1LV;)V

    .line 446774
    move-object v0, v3

    .line 446775
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446776
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446777
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446778
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 446760
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 446749
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446750
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 446751
    check-cast v5, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446752
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446753
    iget-object v6, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->c:LX/2dq;

    const/high16 v2, 0x437c0000    # 252.0f

    sget-object v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->I_()I

    move-result v2

    .line 446754
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 446755
    check-cast v3, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446756
    invoke-static {v3}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v4

    .line 446757
    new-instance p3, LX/2fc;

    invoke-direct {p3, p0, v4, p2, v3}, LX/2fc;-><init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)V

    move-object v3, p3

    .line 446758
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446759
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 446748
    const/4 v0, 0x1

    return v0
.end method
