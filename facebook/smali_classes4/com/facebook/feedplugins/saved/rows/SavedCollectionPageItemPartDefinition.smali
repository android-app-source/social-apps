.class public Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/2fd;",
        "LX/2fe;",
        "LX/1Ps;",
        "LX/3Yv;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/0gh;

.field public final d:LX/1nA;

.field public final e:LX/17W;

.field public final f:LX/1nG;

.field public final g:LX/17Q;

.field public final h:LX/1Ad;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

.field public final k:LX/D3w;

.field public final l:LX/0Uh;

.field private final m:LX/1VK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446780
    const-class v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    const-string v1, "saved_reminders"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1nA;LX/17W;LX/1nG;LX/17Q;LX/1Ad;Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;LX/1VK;LX/D3w;LX/0Uh;LX/0Or;LX/0gh;)V
    .locals 0
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/saved/gating/annotations/IsSavedReminderVideoAutoplayEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/1nA;",
            "LX/17W;",
            "LX/1nG;",
            "LX/17Q;",
            "LX/1Ad;",
            "Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;",
            "LX/1VK;",
            "LX/D3w;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0gh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446897
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 446898
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b:LX/0Zb;

    .line 446899
    iput-object p12, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->c:LX/0gh;

    .line 446900
    iput-object p2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->d:LX/1nA;

    .line 446901
    iput-object p3, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->e:LX/17W;

    .line 446902
    iput-object p4, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->f:LX/1nG;

    .line 446903
    iput-object p5, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->g:LX/17Q;

    .line 446904
    iput-object p6, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->h:LX/1Ad;

    .line 446905
    iput-object p7, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->j:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    .line 446906
    iput-object p8, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->m:LX/1VK;

    .line 446907
    iput-object p9, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->k:LX/D3w;

    .line 446908
    iput-object p10, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->l:LX/0Uh;

    .line 446909
    iput-object p11, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->i:LX/0Or;

    .line 446910
    return-void
.end method

.method private a(LX/2fd;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 446896
    new-instance v0, LX/2fg;

    invoke-direct {v0, p0, p1, p2}, LX/2fg;-><init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/2fd;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;
    .locals 3

    .prologue
    .line 446888
    const-class v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    monitor-enter v1

    .line 446889
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446890
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446891
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446892
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446893
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446894
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 446887
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProfile;)Z
    .locals 4
    .param p0    # Lcom/facebook/graphql/model/GraphQLProfile;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 446886
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->z()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->G()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;
    .locals 13

    .prologue
    .line 446884
    new-instance v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v2

    check-cast v2, LX/1nA;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v4

    check-cast v4, LX/1nG;

    invoke-static {p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-static {p0}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    const-class v8, LX/1VK;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1VK;

    invoke-static {p0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v9

    check-cast v9, LX/D3w;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v11, 0x361

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v12

    check-cast v12, LX/0gh;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;-><init>(LX/0Zb;LX/1nA;LX/17W;LX/1nG;LX/17Q;LX/1Ad;Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;LX/1VK;LX/D3w;LX/0Uh;LX/0Or;LX/0gh;)V

    .line 446885
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 446883
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;)Z
    .locals 3

    .prologue
    .line 446882
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->l:LX/0Uh;

    const/16 v1, 0x50b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLProfile;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLProfile;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 446881
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3Yv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 446880
    sget-object v0, LX/3Yv;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 446833
    check-cast p2, LX/2fd;

    const/4 v7, 0x0

    .line 446834
    iget-object v0, p2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 446835
    const/4 v0, 0x0

    .line 446836
    if-nez v1, :cond_2

    .line 446837
    :cond_0
    :goto_0
    move-object v0, v0

    .line 446838
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v5

    .line 446839
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446840
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    sget-object v2, LX/097;->FROM_STREAM:LX/097;

    .line 446841
    iput-object v2, v0, LX/2oE;->e:LX/097;

    .line 446842
    move-object v0, v0

    .line 446843
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->G()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 446844
    iput-object v2, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 446845
    move-object v0, v0

    .line 446846
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v2

    .line 446847
    iget-object v0, p2, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446848
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 446849
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446850
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    .line 446851
    iput-object v3, v2, LX/2oH;->b:Ljava/lang/String;

    .line 446852
    move-object v2, v2

    .line 446853
    iget-object v3, p2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-static {v3, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    .line 446854
    iput-object v3, v2, LX/2oH;->e:LX/162;

    .line 446855
    move-object v2, v2

    .line 446856
    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v6

    .line 446857
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "CoverImageParamsKey"

    invoke-virtual {v2, v3, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    .line 446858
    new-instance v2, LX/2oI;

    invoke-direct {v2}, LX/2oI;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->G()Ljava/lang/String;

    move-result-object v3

    .line 446859
    iput-object v3, v2, LX/2oI;->aT:Ljava/lang/String;

    .line 446860
    move-object v2, v2

    .line 446861
    invoke-virtual {v2}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 446862
    iget-object v3, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->j:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    new-instance v4, LX/2oJ;

    new-instance v8, LX/2oK;

    iget-object v9, p2, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v10, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->m:LX/1VK;

    invoke-direct {v8, v9, v2, v10}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v8, v0, v1}, LX/2oJ;-><init>(LX/2oK;LX/0jW;Ljava/lang/String;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446863
    :goto_1
    new-instance v0, LX/2fe;

    iget-object v1, p2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->a()LX/0Px;

    move-result-object v1

    const v2, -0x3625f733

    invoke-static {v1, v2}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 446864
    new-instance v2, LX/2ff;

    invoke-direct {v2, p0, p2}, LX/2ff;-><init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;LX/2fd;)V

    move-object v2, v2

    .line 446865
    iget-object v3, p2, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446866
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 446867
    check-cast v3, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446868
    iget-object v4, p2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-static {v4, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v4

    invoke-static {v3}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "saved_collection_ego_item_image_clicked"

    invoke-static {v4, v3, v8}, LX/17Q;->b(LX/0lF;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object v3, v3

    .line 446869
    invoke-direct {p0, p2, v3}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(LX/2fd;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 446870
    iget-object v4, p2, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446871
    iget-object v8, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v8

    .line 446872
    check-cast v4, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446873
    iget-object v8, p2, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-static {v8, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v8

    invoke-static {v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Ljava/lang/String;

    move-result-object v4

    const-string v9, "saved_collection_ego_item_title_clicked"

    invoke-static {v8, v4, v9}, LX/17Q;->b(LX/0lF;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object v4, v4

    .line 446874
    invoke-direct {p0, p2, v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(LX/2fd;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct/range {v0 .. v7}, LX/2fe;-><init>(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/1bf;Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;)V

    return-object v0

    :cond_1
    move-object v6, v7

    goto :goto_1

    .line 446875
    :cond_2
    invoke-static {v1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 446876
    invoke-static {v1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 446877
    goto/16 :goto_0

    .line 446878
    :cond_3
    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 446879
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x43467122

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446786
    check-cast p1, LX/2fd;

    check-cast p2, LX/2fe;

    check-cast p4, LX/3Yv;

    .line 446787
    iget-boolean v1, p1, LX/2fd;->c:Z

    invoke-virtual {p4, v1}, LX/3Yv;->setFullWidth(Z)V

    .line 446788
    iget-object v1, p1, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v1

    .line 446789
    iget-object v2, p4, LX/3Yv;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446790
    iget-object v1, p1, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v1

    .line 446791
    iget-object v2, p4, LX/3Yv;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446792
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 446793
    iget-object v2, p4, LX/3Yv;->h:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 446794
    :goto_0
    invoke-static {p4}, LX/3Yv;->b(LX/3Yv;)V

    .line 446795
    iget-object v1, p1, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v1

    .line 446796
    iget-object v2, p4, LX/3Yv;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446797
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 446798
    iget-object v2, p4, LX/3Yv;->i:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 446799
    :goto_1
    invoke-static {p4}, LX/3Yv;->b(LX/3Yv;)V

    .line 446800
    invoke-static {p0}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->b(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p1, LX/2fd;->b:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 446801
    :goto_2
    iget-object v4, p4, LX/3Yv;->n:Landroid/view/View;

    if-eqz v1, :cond_8

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 446802
    iget-object v1, p2, LX/2fe;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Yv;->setTextContainerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446803
    iget-object v1, p2, LX/2fe;->f:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_5

    .line 446804
    iget-object v1, p2, LX/2fe;->f:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, p2, LX/2fe;->g:LX/0P1;

    .line 446805
    invoke-static {p4}, LX/3Yv;->e(LX/3Yv;)V

    .line 446806
    iget-object v4, p4, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->setVisibility(I)V

    .line 446807
    iget-object v4, p4, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v4, :cond_0

    .line 446808
    iget-object v4, p4, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 446809
    :cond_0
    iget-object v4, p4, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-virtual {v4, v1, v2}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;)V

    .line 446810
    iget-object v1, p2, LX/2fe;->c:Landroid/view/View$OnClickListener;

    .line 446811
    if-eqz v1, :cond_1

    .line 446812
    invoke-static {p4}, LX/3Yv;->e(LX/3Yv;)V

    .line 446813
    :cond_1
    iget-object v2, p4, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-eqz v2, :cond_2

    .line 446814
    iget-object v2, p4, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-virtual {v2, v1}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446815
    :cond_2
    :goto_4
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->l:LX/0Uh;

    const/4 p0, 0x0

    .line 446816
    iget-object v2, p2, LX/2fe;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-eqz v2, :cond_3

    const/16 v2, 0x485

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_9

    .line 446817
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {p4, v2}, LX/3Yv;->setSaveButtonVisibility(I)V

    .line 446818
    :goto_5
    const/16 v1, 0x1f

    const v2, -0xaa9a7dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 446819
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 446820
    :cond_5
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->h:LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v2, p2, LX/2fe;->e:LX/1bf;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    .line 446821
    iget-object v2, p4, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v2, :cond_a

    .line 446822
    const/4 v2, 0x0

    .line 446823
    :goto_6
    move-object v2, v2

    .line 446824
    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 446825
    invoke-virtual {p4, v1}, LX/3Yv;->setMainImageController(LX/1aZ;)V

    .line 446826
    iget-object v1, p2, LX/2fe;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Yv;->setMainImageOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 446827
    :cond_6
    iget-object v2, p4, LX/3Yv;->h:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 446828
    :cond_7
    iget-object v2, p4, LX/3Yv;->i:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 446829
    :cond_8
    const/16 v2, 0x8

    goto/16 :goto_3

    .line 446830
    :cond_9
    iget-object v2, p2, LX/2fe;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    const-string v4, "native_netego"

    iget-object v5, p1, LX/2fd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p3, p2, LX/2fe;->b:Landroid/view/View$OnClickListener;

    .line 446831
    iget-object v1, p4, LX/3Yv;->m:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    invoke-virtual {v1, v2, v4, v5, p3}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V

    .line 446832
    invoke-virtual {p4, p0}, LX/3Yv;->setSaveButtonVisibility(I)V

    goto :goto_5

    :cond_a
    iget-object v2, p4, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    goto :goto_6
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 446781
    check-cast p4, LX/3Yv;

    const/4 v0, 0x0

    .line 446782
    invoke-virtual {p4, v0}, LX/3Yv;->setMainImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446783
    invoke-virtual {p4, v0}, LX/3Yv;->setMainImageController(LX/1aZ;)V

    .line 446784
    invoke-virtual {p4, v0}, LX/3Yv;->setTextContainerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446785
    return-void
.end method
