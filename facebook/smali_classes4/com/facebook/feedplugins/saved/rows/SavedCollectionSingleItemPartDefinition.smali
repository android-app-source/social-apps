.class public Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3Yv;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446932
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 446933
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->a:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    .line 446934
    iput-object p2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 446935
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;
    .locals 5

    .prologue
    .line 446936
    const-class v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;

    monitor-enter v1

    .line 446937
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446938
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446939
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446940
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446941
    new-instance p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;-><init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 446942
    move-object v0, p0

    .line 446943
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446944
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446945
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 446947
    sget-object v0, LX/3Yv;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 446948
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446949
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 446950
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446951
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446952
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionSingleItemPartDefinition;->a:Lcom/facebook/feedplugins/saved/rows/SavedCollectionPageItemPartDefinition;

    new-instance v2, LX/2fd;

    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v0, v3}, LX/2fd;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;Z)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446953
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 446954
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 446955
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 446956
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446957
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
