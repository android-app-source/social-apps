.class public Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2oJ;",
        "LX/CCZ;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1AV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446911
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 446912
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;->a:LX/1AV;

    .line 446913
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;
    .locals 4

    .prologue
    .line 446921
    const-class v1, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    monitor-enter v1

    .line 446922
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446923
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446924
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446925
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446926
    new-instance p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v3

    check-cast v3, LX/1AV;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;-><init>(LX/1AV;)V

    .line 446927
    move-object v0, p0

    .line 446928
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446929
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446930
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 446917
    check-cast p2, LX/2oJ;

    check-cast p3, LX/1Pr;

    .line 446918
    iget-object v0, p2, LX/2oJ;->a:LX/2oK;

    iget-object v1, p2, LX/2oJ;->b:LX/0jW;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    .line 446919
    new-instance v1, LX/CCa;

    iget-object v2, p2, LX/2oJ;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, LX/CCa;-><init>(Ljava/lang/String;LX/2oL;)V

    .line 446920
    new-instance v2, LX/CCZ;

    invoke-direct {v2, v0, v1}, LX/CCZ;-><init>(LX/2oL;LX/2oV;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x14d2c9fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446914
    check-cast p2, LX/CCZ;

    .line 446915
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionItemInlineVideoAutoplayPartDefinition;->a:LX/1AV;

    iget-object v2, p2, LX/CCZ;->b:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 446916
    const/16 v1, 0x1f

    const v2, 0x200d397b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
