.class public Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/17W;

.field private final c:LX/17Q;

.field private final d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final g:LX/79x;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17W;LX/17Q;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/79x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446958
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 446959
    iput-object p2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->b:LX/17W;

    .line 446960
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->a:LX/0Zb;

    .line 446961
    iput-object p3, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->c:LX/17Q;

    .line 446962
    iput-object p4, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 446963
    iput-object p5, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 446964
    iput-object p6, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 446965
    iput-object p7, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->g:LX/79x;

    .line 446966
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;
    .locals 11

    .prologue
    .line 446967
    const-class v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    monitor-enter v1

    .line 446968
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446969
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446970
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446971
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446972
    new-instance v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v6

    check-cast v6, LX/17Q;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/79x;->b(LX/0QB;)LX/79x;

    move-result-object v10

    check-cast v10, LX/79x;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;-><init>(LX/0Zb;LX/17W;LX/17Q;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/79x;)V

    .line 446973
    move-object v0, v3

    .line 446974
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446975
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446976
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 446978
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 446979
    check-cast p2, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    const/4 v3, 0x0

    .line 446980
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446981
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o()LX/2ci;

    move-result-object v2

    iget-object v2, v2, LX/2ci;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 446982
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/2fr;

    invoke-direct {v1, p0, p2}, LX/2fr;-><init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446983
    return-object v3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 446984
    const/4 v0, 0x1

    return v0
.end method
