.class public Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;",
        ">;",
        "Ljava/lang/String;",
        "TE;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446691
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 446692
    iput v1, v0, LX/1UY;->b:F

    .line 446693
    move-object v0, v0

    .line 446694
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 446695
    iput v1, v0, LX/1UY;->e:F

    .line 446696
    move-object v0, v0

    .line 446697
    const/high16 v1, 0x41300000    # 11.0f

    .line 446698
    iput v1, v0, LX/1UY;->c:F

    .line 446699
    move-object v0, v0

    .line 446700
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446725
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 446726
    iput-object p1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 446727
    iput-object p2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 446728
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;
    .locals 5

    .prologue
    .line 446714
    const-class v1, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;

    monitor-enter v1

    .line 446715
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446716
    sput-object v2, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446719
    new-instance p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V

    .line 446720
    move-object v0, p0

    .line 446721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 446713
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 446706
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 446707
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 446708
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 446709
    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446710
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 446711
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 446712
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x795c514

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 446702
    check-cast p2, Ljava/lang/String;

    check-cast p4, LX/2ee;

    .line 446703
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 446704
    sget-object v1, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, p2, v1}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 446705
    const/16 v1, 0x1f

    const v2, -0x17c56a77

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 446701
    const/4 v0, 0x1

    return v0
.end method
