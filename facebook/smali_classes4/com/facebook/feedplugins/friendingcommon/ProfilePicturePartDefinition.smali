.class public Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2ey;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446106
    const-class v0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446084
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 446085
    iput-object p1, p0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 446086
    iput-object p2, p0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 446087
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;
    .locals 5

    .prologue
    .line 446095
    const-class v1, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    monitor-enter v1

    .line 446096
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446097
    sput-object v2, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446098
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446099
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446100
    new-instance p0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V

    .line 446101
    move-object v0, p0

    .line 446102
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446103
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446104
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 446088
    check-cast p2, LX/2ey;

    .line 446089
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v1, LX/2f8;

    invoke-direct {v1}, LX/2f8;-><init>()V

    iget-object v2, p2, LX/2ey;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 446090
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 446091
    move-object v1, v1

    .line 446092
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446093
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    iget-object v1, p2, LX/2ey;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446094
    const/4 v0, 0x0

    return-object v0
.end method
