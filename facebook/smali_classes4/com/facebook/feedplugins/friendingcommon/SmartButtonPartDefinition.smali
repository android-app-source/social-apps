.class public Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2f5;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

.field private final b:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445902
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 445903
    iput-object p1, p0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    .line 445904
    iput-object p2, p0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->b:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    .line 445905
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;
    .locals 5

    .prologue
    .line 445906
    const-class v1, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    monitor-enter v1

    .line 445907
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445908
    sput-object v2, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445909
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445910
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445911
    new-instance p0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;-><init>(Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;)V

    .line 445912
    move-object v0, p0

    .line 445913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 445917
    check-cast p2, LX/2f5;

    .line 445918
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->b:Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    new-instance v1, LX/2eW;

    iget-object v2, p2, LX/2f5;->a:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/2f5;->b:Ljava/lang/CharSequence;

    invoke-direct {v1, v2, v3}, LX/2eW;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445919
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    new-instance v1, LX/2eV;

    iget v2, p2, LX/2f5;->c:I

    iget-object v3, p2, LX/2f5;->d:Ljava/lang/Integer;

    invoke-direct {v1, v2, v3}, LX/2eV;-><init>(ILjava/lang/Integer;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445920
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x38e91f0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 445921
    check-cast p1, LX/2f5;

    check-cast p4, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 445922
    iget v1, p1, LX/2f5;->e:I

    if-eqz v1, :cond_1

    .line 445923
    iget v1, p1, LX/2f5;->e:I

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 445924
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6ac969e0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 445925
    :cond_1
    iget-object v1, p1, LX/2f5;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 445926
    iget-object v1, p1, LX/2f5;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 445927
    iget v1, p1, LX/2f5;->f:I

    invoke-virtual {p4, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setBackgroundResource(I)V

    goto :goto_0
.end method
