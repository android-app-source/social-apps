.class public Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/String;",
        "LX/1Ps;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444645
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444646
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 444647
    iput-object p1, p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->b:Landroid/content/res/Resources;

    .line 444648
    iput-object p2, p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 444649
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;
    .locals 5

    .prologue
    .line 444650
    const-class v1, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;

    monitor-enter v1

    .line 444651
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444652
    sput-object v2, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444653
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444654
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444655
    new-instance p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 444656
    move-object v0, p0

    .line 444657
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444658
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444659
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 444661
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 444662
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444663
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 444664
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 444665
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 444666
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;

    if-eqz v0, :cond_0

    .line 444667
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f080f6f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 444668
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f080f77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x172e5af8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 444669
    check-cast p2, Ljava/lang/String;

    check-cast p4, LX/2ee;

    .line 444670
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 444671
    sget-object v1, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, p2, v1}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 444672
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 444673
    const/16 v1, 0x1f

    const v2, 0x14b5aa69

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 444674
    const/4 v0, 0x1

    return v0
.end method
