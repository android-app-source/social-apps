.class public Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2eW;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444340
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 444341
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;
    .locals 3

    .prologue
    .line 444342
    const-class v1, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    monitor-enter v1

    .line 444343
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444344
    sput-object v2, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 444347
    new-instance v0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;-><init>()V

    .line 444348
    move-object v0, v0

    .line 444349
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444350
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/friendingcommon/SmartButtonTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444351
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5fd0941c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 444353
    check-cast p1, LX/2eW;

    check-cast p4, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 444354
    iget-object v1, p1, LX/2eW;->a:Ljava/lang/CharSequence;

    iget-object v2, p1, LX/2eW;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 444355
    const/16 v1, 0x1f

    const v2, -0x1c183765

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
