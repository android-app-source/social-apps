.class public Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/C2v;

.field private final c:LX/1nu;

.field public final d:LX/C2m;

.field private final e:LX/BcE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 491846
    const-class v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/C2v;LX/1nu;LX/C2m;LX/BcE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491848
    iput-object p1, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->b:LX/C2v;

    .line 491849
    iput-object p2, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->c:LX/1nu;

    .line 491850
    iput-object p3, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->d:LX/C2m;

    .line 491851
    iput-object p4, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->e:LX/BcE;

    .line 491852
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C2l;)LX/1Dg;
    .locals 7
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/C2l;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 491853
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 491854
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 491855
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    .line 491856
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 491857
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 491858
    invoke-static {v3, v0}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a(LX/0Pz;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 491859
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 491860
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 491861
    :goto_1
    move-object v5, v0

    .line 491862
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491863
    const/4 v0, 0x0

    .line 491864
    :goto_2
    return-object v0

    .line 491865
    :cond_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 491866
    iget-object v0, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->b:LX/C2v;

    const v3, 0x3f333333    # 0.7f

    sget-object v4, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/C2v;->a(LX/1De;IFLcom/facebook/common/callercontext/CallerContext;LX/0Px;LX/C2l;)LX/C2u;

    move-result-object v0

    .line 491867
    iget-object v1, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->e:LX/BcE;

    invoke-virtual {v1, p1}, LX/BcE;->c(LX/1De;)LX/BcC;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/BcC;->a(LX/5Jq;)LX/BcC;

    move-result-object v0

    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, LX/BcC;->h(I)LX/BcC;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_2

    .line 491868
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->c:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b010f

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    .line 491869
    const v1, -0x3abbaddb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 491870
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_2

    .line 491871
    :cond_3
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 491872
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3, v0}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a(LX/0Pz;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 491873
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;
    .locals 7

    .prologue
    .line 491874
    const-class v1, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;

    monitor-enter v1

    .line 491875
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491876
    sput-object v2, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491877
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491878
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491879
    new-instance p0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;

    const-class v3, LX/C2v;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/C2v;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    const-class v5, LX/C2m;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/C2m;

    invoke-static {v0}, LX/BcE;->a(LX/0QB;)LX/BcE;

    move-result-object v6

    check-cast v6, LX/BcE;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;-><init>(LX/C2v;LX/1nu;LX/C2m;LX/BcE;)V

    .line 491880
    move-object v0, p0

    .line 491881
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491882
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491883
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0Pz;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 491885
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_2

    .line 491886
    :cond_0
    const/4 v0, 0x0

    .line 491887
    :goto_0
    move-object v0, v0

    .line 491888
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 491889
    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 491890
    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 491891
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 491892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v3

    .line 491893
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 491894
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->COMMERCE_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v5, :cond_0

    .line 491895
    const/4 v0, 0x1

    .line 491896
    :goto_1
    return v0

    .line 491897
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 491898
    goto :goto_1
.end method
