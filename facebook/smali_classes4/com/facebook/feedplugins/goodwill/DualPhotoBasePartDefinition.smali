.class public abstract Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FeedUnit::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        "E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TFeedUnit;>;",
        "LX/Es2;",
        "TE;",
        "Lcom/facebook/feedplugins/goodwill/DualPhotoView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Lcom/facebook/common/callercontext/CallerContext;

.field private final c:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 364204
    new-instance v0, LX/3Xc;

    invoke-direct {v0}, LX/3Xc;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;)V
    .locals 2

    .prologue
    .line 364205
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 364206
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 364207
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->c:LX/1Ad;

    .line 364208
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 364209
    sget-object v0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1Ps;Ljava/lang/String;Ljava/lang/String;LX/C4f;)LX/Es2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/C4f;",
            ")",
            "LX/Es2;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 364210
    invoke-static {p2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 364211
    invoke-static {p3}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    move-object v0, p1

    .line 364212
    check-cast v0, LX/1Pt;

    .line 364213
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 364214
    invoke-interface {v0, v1, v3}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 364215
    check-cast p1, LX/1Pt;

    .line 364216
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 364217
    invoke-interface {p1, v2, v0}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 364218
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->c:LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v4}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 364219
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 364220
    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 364221
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->c:LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v4}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 364222
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 364223
    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 364224
    new-instance v2, LX/Es2;

    invoke-direct {v2, v1, v0, p4}, LX/Es2;-><init>(LX/1aZ;LX/1aZ;LX/C4f;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6a778444

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364225
    check-cast p2, LX/Es2;

    check-cast p4, Lcom/facebook/feedplugins/goodwill/DualPhotoView;

    .line 364226
    iget-object v1, p2, LX/Es2;->a:LX/1aZ;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->setPolaroidLeft(LX/1aZ;)V

    .line 364227
    iget-object v1, p2, LX/Es2;->b:LX/1aZ;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->setPolaroidRight(LX/1aZ;)V

    .line 364228
    iget-object v1, p2, LX/Es2;->c:LX/C4f;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->setDualPhotoViewConfig(LX/C4f;)V

    .line 364229
    const/16 v1, 0x1f

    const v2, -0x37766b58    # -281765.25f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
