.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C4r;",
        "LX/C4s;",
        "TE;",
        "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460125
    new-instance v0, LX/2mM;

    invoke-direct {v0}, LX/2mM;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460121
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460122
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 460123
    iput-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->b:LX/0Ot;

    .line 460124
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;
    .locals 4

    .prologue
    .line 460108
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    monitor-enter v1

    .line 460109
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460110
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460111
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460112
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460113
    new-instance v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    invoke-direct {v3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;-><init>()V

    .line 460114
    const/16 p0, 0x844

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 460115
    iput-object p0, v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->b:LX/0Ot;

    .line 460116
    move-object v0, v3

    .line 460117
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460118
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460119
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460120
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460107
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 460101
    check-cast p2, LX/C4r;

    .line 460102
    iget-object v0, p2, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 460103
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v2, v4

    .line 460104
    new-instance v1, LX/C4s;

    invoke-direct {v1, v0, v2, v3}, LX/C4s;-><init>(Lcom/facebook/graphql/model/GraphQLVideo;D)V

    .line 460105
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/36r;

    iget-object v3, p2, LX/C4r;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, LX/36r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460106
    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x36dcc299

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460092
    check-cast p1, LX/C4r;

    check-cast p2, LX/C4s;

    check-cast p4, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;

    .line 460093
    iget-object v4, p2, LX/C4s;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v4, :cond_0

    .line 460094
    const/4 v4, 0x0

    invoke-virtual {p4, v4}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;->setVisibility(I)V

    .line 460095
    invoke-virtual {p4}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 460096
    iget v5, p1, LX/C4r;->c:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 460097
    iget v5, p1, LX/C4r;->c:I

    int-to-double v6, v5

    iget-wide v8, p2, LX/C4s;->b:D

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 460098
    iget-object v4, p1, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, p2, LX/C4s;->b:D

    invoke-virtual {p4, v4, v5, v6, v7}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;->a(Ljava/lang/String;Ljava/lang/String;D)V

    .line 460099
    invoke-virtual {p4}, LX/2oW;->jj_()V

    .line 460100
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x611a0b9a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460090
    check-cast p1, LX/C4r;

    .line 460091
    iget-object v0, p1, LX/C4r;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/C4r;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 460086
    check-cast p4, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;

    .line 460087
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;->setVisibility(I)V

    .line 460088
    invoke-virtual {p4}, LX/2oW;->b()V

    .line 460089
    return-void
.end method
