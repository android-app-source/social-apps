.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/36Y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/36Y",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/36Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499194
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499195
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 499196
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->e:LX/36Y;

    .line 499197
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 499198
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->e:LX/36Y;

    const/4 v1, 0x0

    .line 499199
    new-instance v2, LX/C4x;

    invoke-direct {v2, v0}, LX/C4x;-><init>(LX/36Y;)V

    .line 499200
    iget-object v3, v0, LX/36Y;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C4w;

    .line 499201
    if-nez v3, :cond_0

    .line 499202
    new-instance v3, LX/C4w;

    invoke-direct {v3, v0}, LX/C4w;-><init>(LX/36Y;)V

    .line 499203
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C4w;->a$redex0(LX/C4w;LX/1De;IILX/C4x;)V

    .line 499204
    move-object v2, v3

    .line 499205
    move-object v1, v2

    .line 499206
    move-object v0, v1

    .line 499207
    iget-object v1, v0, LX/C4w;->a:LX/C4x;

    iput-object p3, v1, LX/C4x;->b:LX/1Pp;

    .line 499208
    iget-object v1, v0, LX/C4w;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499209
    move-object v0, v0

    .line 499210
    iget-object v1, v0, LX/C4w;->a:LX/C4x;

    iput-object p2, v1, LX/C4x;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499211
    iget-object v1, v0, LX/C4w;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499212
    move-object v0, v0

    .line 499213
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499214
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->m:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 499182
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499183
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499184
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499185
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499186
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499187
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/36Y;->a(LX/0QB;)LX/36Y;

    move-result-object v5

    check-cast v5, LX/36Y;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/36Y;)V

    .line 499188
    move-object v0, p0

    .line 499189
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499190
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499191
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499193
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499181
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/calltoaction/CulturalMomentHolidayCardCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499173
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499174
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499176
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 499177
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 499178
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x211e8156

    if-ne v0, v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 499179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 499180
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 499171
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499172
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
