.class public Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/23o;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(LX/23o;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365240
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 365241
    iput-object p1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a:LX/23o;

    .line 365242
    iput-object p2, p0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 365243
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 365244
    const-class v1, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    monitor-enter v1

    .line 365245
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365246
    sput-object v2, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365247
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365248
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365249
    new-instance p0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-static {v0}, LX/23o;->a(LX/0QB;)LX/23o;

    move-result-object v3

    check-cast v3, LX/23o;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;-><init>(LX/23o;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 365250
    move-object v0, p0

    .line 365251
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365252
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365253
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365254
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 365255
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365256
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a:LX/23o;

    invoke-virtual {v1, p2}, LX/23o;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 365257
    const/4 v0, 0x0

    return-object v0
.end method
