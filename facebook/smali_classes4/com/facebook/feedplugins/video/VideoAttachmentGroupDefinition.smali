.class public Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""

# interfaces
.implements LX/1RB;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;",
        "LX/1RB",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/WatchAndShopCallToActionAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0tQ;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/WatchAndShopCallToActionAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;",
            ">;",
            "LX/0tQ;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501258
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 501259
    iput-object p1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->a:LX/0Ot;

    .line 501260
    iput-object p2, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->b:LX/0Ot;

    .line 501261
    iput-object p3, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->c:LX/0Ot;

    .line 501262
    iput-object p4, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->d:LX/0Ot;

    .line 501263
    iput-object p5, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->e:LX/0Ot;

    .line 501264
    iput-object p6, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->f:LX/0Ot;

    .line 501265
    iput-object p7, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->g:LX/0Ot;

    .line 501266
    iput-object p8, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->j:LX/0Ot;

    .line 501267
    iput-object p10, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->l:LX/0Ot;

    .line 501268
    iput-object p9, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->k:LX/0Ot;

    .line 501269
    iput-object p11, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->h:LX/0Ot;

    .line 501270
    iput-object p12, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->i:LX/0Ot;

    .line 501271
    iput-object p13, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->m:LX/0tQ;

    .line 501272
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;
    .locals 3

    .prologue
    .line 501250
    const-class v1, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;

    monitor-enter v1

    .line 501251
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 501252
    sput-object v2, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 501253
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501254
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 501255
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501256
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 501257
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;
    .locals 15

    .prologue
    .line 501248
    new-instance v0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;

    const/16 v1, 0xa51

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x873

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x87b

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x876

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x878

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa50

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x874

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x87e

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x7df

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x87d

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xa47

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1eef

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v13

    check-cast v13, LX/0tQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0tQ;LX/0ad;)V

    .line 501249
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 501236
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501237
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501238
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->m:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501239
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501240
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 501241
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501242
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501243
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501244
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 501245
    const/4 v0, 0x0

    return-object v0

    .line 501246
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->m:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501247
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v1, LX/CCx;

    invoke-direct {v1, p2}, LX/CCx;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 501235
    const/4 v0, 0x1

    return v0
.end method
