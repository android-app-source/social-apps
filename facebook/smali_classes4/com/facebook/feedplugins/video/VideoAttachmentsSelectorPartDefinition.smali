.class public Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field private final b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;LX/03V;Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460726
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 460727
    iput-object p5, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a:LX/03V;

    .line 460728
    iput-object p1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    .line 460729
    iput-object p3, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->d:Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    .line 460730
    iput-object p2, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    .line 460731
    iput-object p4, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->e:Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    .line 460732
    iput-object p6, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->f:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    .line 460733
    iput-object p7, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->g:Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    .line 460734
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;
    .locals 11

    .prologue
    .line 460735
    const-class v1, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    monitor-enter v1

    .line 460736
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460737
    sput-object v2, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460740
    new-instance v3, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;LX/03V;Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;)V

    .line 460741
    move-object v0, v3

    .line 460742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 460746
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 460747
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->e:Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->d:Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->g:Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    invoke-virtual {v0, v1, v2, p2}, LX/1RG;->a(ZLcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->f:Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    new-instance v2, LX/33s;

    invoke-direct {v2, p2}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 460748
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 460749
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460750
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 460751
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460752
    const/4 v0, 0x1

    .line 460753
    :goto_0
    return v0

    .line 460754
    :cond_0
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 460755
    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a:LX/03V;

    const-string v2, "FullscreenVideoAttachmentPartDefinition"

    const-string v3, "Unexpected null video with zombie: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v5

    invoke-static {v3, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 460756
    const/4 v0, 0x0

    goto :goto_0

    .line 460757
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460758
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
