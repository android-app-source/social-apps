.class public Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/23p;

.field private final f:LX/1V0;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bys;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 522145
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/23p;LX/1V0;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/23p;",
            "LX/1V0;",
            "LX/0Ot",
            "<",
            "LX/Bys;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522140
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 522141
    iput-object p2, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->e:LX/23p;

    .line 522142
    iput-object p3, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->f:LX/1V0;

    .line 522143
    iput-object p4, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->g:LX/0Ot;

    .line 522144
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 522146
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bys;

    const/4 v1, 0x0

    .line 522147
    new-instance v2, LX/Byr;

    invoke-direct {v2, v0}, LX/Byr;-><init>(LX/Bys;)V

    .line 522148
    sget-object v3, LX/Bys;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Byq;

    .line 522149
    if-nez v3, :cond_0

    .line 522150
    new-instance v3, LX/Byq;

    invoke-direct {v3}, LX/Byq;-><init>()V

    .line 522151
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Byq;->a$redex0(LX/Byq;LX/1De;IILX/Byr;)V

    .line 522152
    move-object v2, v3

    .line 522153
    move-object v1, v2

    .line 522154
    move-object v1, v1

    .line 522155
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522156
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 522157
    iget-object v2, v1, LX/Byq;->a:LX/Byr;

    iput-object v0, v2, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 522158
    iget-object v2, v1, LX/Byq;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 522159
    move-object v0, v1

    .line 522160
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 522161
    iget-object v1, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;
    .locals 7

    .prologue
    .line 522128
    const-class v1, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;

    monitor-enter v1

    .line 522129
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522130
    sput-object v2, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522131
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522132
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522133
    new-instance v6, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/23p;->b(LX/0QB;)LX/23p;

    move-result-object v4

    check-cast v4, LX/23p;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    const/16 p0, 0x1e33

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;-><init>(Landroid/content/Context;LX/23p;LX/1V0;LX/0Ot;)V

    .line 522134
    move-object v0, v6

    .line 522135
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522136
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522137
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 522139
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 522122
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 522126
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522127
    iget-object v0, p0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->e:LX/23p;

    invoke-virtual {v0, p1}, LX/23p;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 522123
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522124
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 522125
    sget-object v0, Lcom/facebook/feedplugins/video/VideoAttachmentViewCountComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
