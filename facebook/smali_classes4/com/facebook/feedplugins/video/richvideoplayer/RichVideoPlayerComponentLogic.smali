.class public Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/392;",
        ":",
        "LX/3FP;",
        ":",
        "LX/3FT;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final H:Lcom/facebook/common/callercontext/CallerContext;

.field private static I:LX/0Xm;

.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/1Ad;

.field private final B:LX/198;

.field public final C:LX/0yc;

.field private final D:LX/1YQ;

.field private final E:LX/1AY;

.field private final F:Z

.field private final G:Z

.field private final b:LX/0Uh;

.field private final c:Z

.field private final d:LX/1qa;

.field private final e:LX/2mZ;

.field private final f:LX/1VK;

.field private final g:LX/2mk;

.field public final h:LX/2ml;

.field private final i:LX/23s;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0tK;

.field public final l:LX/2mn;

.field private final m:LX/1C2;

.field private final n:LX/1Yd;

.field private final o:LX/0bH;

.field private final p:LX/2mo;

.field private final q:LX/1CK;

.field private final r:LX/2mp;

.field private final s:LX/1Yk;

.field public final t:LX/2mq;

.field private final u:LX/0iX;

.field private final v:LX/0f8;

.field public final w:LX/0ad;

.field private final x:LX/2mu;

.field private final y:LX/2ms;

.field private final z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bwf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 461506
    const-string v0, "chromeless:content:fragment:tag"

    const-string v1, "consumptionsnowflake:fragment:tag"

    sget-object v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n:Ljava/lang/String;

    sget-object v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->m:Ljava/lang/String;

    sget-object v4, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a:LX/0Px;

    .line 461507
    const-class v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->H:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/1qa;LX/2mZ;LX/1VK;LX/2mk;LX/2ml;LX/0Or;LX/23s;LX/0tK;LX/2mn;LX/1C2;LX/1Yd;LX/0bH;LX/2mo;LX/1CK;LX/2mp;LX/1Yk;LX/2mq;LX/1Ad;LX/198;LX/0yc;LX/1AY;LX/1YQ;LX/0iX;LX/2mr;LX/0ad;LX/2ms;LX/2mu;LX/0Ot;LX/0Or;)V
    .locals 4
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsAlwaysPlayVideoUnmutedEnabled;
        .end annotation
    .end param
    .param p30    # LX/0Or;
        .annotation runtime Lcom/facebook/feedplugins/video/richvideoplayer/IsVideoGraphqlSubscriptionEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1qa;",
            "LX/2mZ;",
            "LX/1VK;",
            "LX/2mk;",
            "LX/2ml;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/23s;",
            "LX/0tK;",
            "LX/2mn;",
            "LX/1C2;",
            "LX/1Yd;",
            "LX/0bH;",
            "LX/2mo;",
            "LX/1CK;",
            "LX/2mp;",
            "LX/1Yk;",
            "LX/2mq;",
            "LX/1Ad;",
            "LX/198;",
            "LX/0yc;",
            "LX/1AY;",
            "LX/1YQ;",
            "LX/0iX;",
            "LX/2mr;",
            "LX/0ad;",
            "LX/2ms;",
            "LX/2mu;",
            "LX/0Ot",
            "<",
            "LX/Bwf;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461273
    iput-object p1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b:LX/0Uh;

    .line 461274
    iput-object p2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->d:LX/1qa;

    .line 461275
    iput-object p3, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->e:LX/2mZ;

    .line 461276
    iput-object p4, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->f:LX/1VK;

    .line 461277
    iput-object p5, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->g:LX/2mk;

    .line 461278
    iput-object p6, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->h:LX/2ml;

    .line 461279
    iput-object p8, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->i:LX/23s;

    .line 461280
    iput-object p7, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->j:LX/0Or;

    .line 461281
    iput-object p9, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->k:LX/0tK;

    .line 461282
    iput-object p10, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->l:LX/2mn;

    .line 461283
    iput-object p11, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->m:LX/1C2;

    .line 461284
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->n:LX/1Yd;

    .line 461285
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->o:LX/0bH;

    .line 461286
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->p:LX/2mo;

    .line 461287
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->q:LX/1CK;

    .line 461288
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->r:LX/2mp;

    .line 461289
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->s:LX/1Yk;

    .line 461290
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->t:LX/2mq;

    .line 461291
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->A:LX/1Ad;

    .line 461292
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->B:LX/198;

    .line 461293
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->C:LX/0yc;

    .line 461294
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->E:LX/1AY;

    .line 461295
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->D:LX/1YQ;

    .line 461296
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->u:LX/0iX;

    .line 461297
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->v:LX/0f8;

    .line 461298
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->w:LX/0ad;

    .line 461299
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->x:LX/2mu;

    .line 461300
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b:LX/0Uh;

    const/16 v2, 0x2bf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->F:Z

    .line 461301
    iget-boolean v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->F:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->G:Z

    .line 461302
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->y:LX/2ms;

    .line 461303
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->z:LX/0Ot;

    .line 461304
    invoke-interface/range {p30 .. p30}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->c:Z

    .line 461305
    return-void

    .line 461306
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b:LX/0Uh;

    const/16 v2, 0x2c3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    goto :goto_0
.end method

.method public static a(LX/0am;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/CDf;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 461508
    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461509
    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDf;

    iget v0, v0, LX/CDf;->c:I

    .line 461510
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/CDf;)I
    .locals 1

    .prologue
    .line 461511
    if-eqz p0, :cond_0

    iget v0, p0, LX/CDf;->c:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLVideo;)LX/1bf;
    .locals 1

    .prologue
    .line 461522
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/3EE;)LX/3FO;
    .locals 6

    .prologue
    .line 461512
    iget-object v0, p1, LX/3EE;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461513
    iget-object v0, p1, LX/3EE;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LX/CDf;

    .line 461514
    new-instance v0, LX/3FO;

    iget v1, v4, LX/CDf;->a:I

    iget v2, v4, LX/CDf;->b:I

    iget v3, v4, LX/CDf;->a:I

    iget v4, v4, LX/CDf;->b:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/3FO;-><init>(IIIII)V

    .line 461515
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 461516
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->k:LX/0tK;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0tK;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 461517
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->l:LX/2mn;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v1

    .line 461518
    :goto_1
    move-object v0, v1

    .line 461519
    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->l:LX/2mn;

    iget-object v3, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->t:LX/2mq;

    .line 461520
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 461521
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3, v1}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v1

    goto :goto_1
.end method

.method private a(LX/3FN;LX/1Pq;)LX/3Ix;
    .locals 2

    .prologue
    .line 461524
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461525
    iget-object v0, p1, LX/3FN;->j:LX/2oL;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461526
    iget-object v0, p1, LX/3FN;->j:LX/2oL;

    .line 461527
    iget-object v1, v0, LX/2oL;->g:LX/3Iy;

    move-object v0, v1

    .line 461528
    check-cast v0, LX/3Ix;

    .line 461529
    if-nez v0, :cond_0

    .line 461530
    new-instance v0, LX/3Ix;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->q:LX/1CK;

    invoke-direct {v0, p1, p2, v1}, LX/3Ix;-><init>(LX/3FN;LX/1Pq;LX/1CK;)V

    .line 461531
    :goto_0
    iget-object v1, p1, LX/3FN;->j:LX/2oL;

    .line 461532
    iput-object v0, v1, LX/2oL;->g:LX/3Iy;

    .line 461533
    return-object v0

    .line 461534
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461535
    iput-object p1, v0, LX/3Ix;->c:LX/3FN;

    .line 461536
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
    .locals 3

    .prologue
    .line 461494
    const-class v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    monitor-enter v1

    .line 461495
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->I:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461496
    sput-object v2, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->I:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461497
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461498
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3FN;LX/04g;)V
    .locals 1

    .prologue
    .line 461502
    iget-object v0, p0, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 461503
    iput-object p1, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 461504
    iget-object v0, p0, LX/3FN;->j:LX/2oL;

    invoke-virtual {v0, p1}, LX/2oL;->a(LX/04g;)V

    .line 461505
    return-void
.end method

.method private a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2oO;LX/04D;)V
    .locals 7
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 461470
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->r:LX/2mp;

    invoke-virtual {p2}, LX/2oO;->l()Z

    move-result v1

    iget-boolean v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    const/4 v3, 0x0

    .line 461471
    iget-object v4, v0, LX/2mp;->a:LX/0ka;

    invoke-virtual {v4}, LX/0ka;->b()Z

    move-result v6

    .line 461472
    if-eqz v2, :cond_2

    iget-object v4, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v4, v4, LX/0wq;->I:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_2

    iget-object v4, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v4, v4, LX/0wq;->K:Z

    if-eqz v4, :cond_2

    :cond_0
    iget-object v4, v0, LX/2mp;->c:LX/0wq;

    iget-object v5, p3, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/0wq;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    .line 461473
    :goto_0
    if-nez v1, :cond_4

    .line 461474
    if-eqz v6, :cond_3

    iget-object v5, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v5, v5, LX/0wq;->A:Z

    .line 461475
    :goto_1
    if-nez v5, :cond_4

    if-nez v4, :cond_4

    .line 461476
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 461477
    :goto_2
    move v0, v3

    .line 461478
    if-eqz v0, :cond_1

    .line 461479
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->s:LX/1Yk;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 461480
    iget-object v2, v0, LX/1Yk;->c:LX/0aq;

    invoke-virtual {v2, v1, p1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461481
    :cond_1
    return-void

    :cond_2
    move v4, v3

    .line 461482
    goto :goto_0

    .line 461483
    :cond_3
    iget-object v5, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v5, v5, LX/0wq;->B:Z

    goto :goto_1

    .line 461484
    :cond_4
    iget-object v5, v0, LX/2mp;->c:LX/0wq;

    invoke-virtual {v5}, LX/0wq;->e()Z

    move-result p2

    .line 461485
    if-eqz p2, :cond_5

    move v5, v4

    .line 461486
    :goto_3
    if-nez v5, :cond_6

    .line 461487
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_2

    .line 461488
    :cond_5
    iget-object v5, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v5, v5, LX/0wq;->p:Z

    goto :goto_3

    .line 461489
    :cond_6
    if-eqz v2, :cond_7

    move v3, v4

    .line 461490
    goto :goto_2

    .line 461491
    :cond_7
    if-eqz v6, :cond_8

    .line 461492
    iget-object v3, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v3, v3, LX/0wq;->r:Z

    goto :goto_2

    .line 461493
    :cond_8
    iget-object v3, v0, LX/2mp;->c:LX/0wq;

    iget-boolean v3, v3, LX/0wq;->q:Z

    goto :goto_2
.end method

.method public static a(Lcom/facebook/video/player/RichVideoPlayer;LX/3FN;LX/198;LX/04g;LX/7K4;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 461452
    iget-boolean v1, p1, LX/3FN;->q:Z

    if-nez v1, :cond_0

    .line 461453
    iput-boolean v0, p1, LX/3FN;->q:Z

    .line 461454
    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/198;->a(LX/1YD;)V

    .line 461455
    :cond_0
    iget-object v1, p1, LX/3FN;->j:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->c()LX/04g;

    move-result-object v1

    sget-object v2, LX/04g;->UNSET:LX/04g;

    invoke-virtual {v1, v2}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, p3}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, p3}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 461456
    :cond_1
    invoke-static {p1, p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3FN;LX/04g;)V

    .line 461457
    :cond_2
    iget-object v1, p1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    if-eqz v1, :cond_3

    .line 461458
    iget-object v1, p1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 461459
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v2

    .line 461460
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 461461
    iget-object v1, p1, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 461462
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 461463
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 461464
    :cond_3
    iget-boolean v1, p1, LX/3FN;->d:Z

    if-nez v1, :cond_5

    iget-object v1, p1, LX/3FN;->u:LX/0iX;

    iget-object v2, p1, LX/3FN;->f:LX/04D;

    iget-object v3, p1, LX/3FN;->h:LX/2pa;

    invoke-virtual {v1, v2, v3}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 461465
    sget-object v0, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    if-eq p3, v0, :cond_4

    .line 461466
    iget v0, p4, LX/7K4;->c:I

    invoke-virtual {p0, v0, p3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 461467
    :cond_4
    iget v0, p4, LX/7K4;->d:I

    invoke-virtual {p0, p3, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 461468
    return-void

    .line 461469
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/392;",
            ">(TV;)Z"
        }
    .end annotation

    .prologue
    .line 461428
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461429
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 461430
    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0f8;

    .line 461431
    if-eqz v1, :cond_2

    .line 461432
    invoke-interface {v1}, LX/0f8;->g()Z

    move-result v1

    .line 461433
    :goto_0
    move v0, v1

    .line 461434
    if-nez v0, :cond_0

    check-cast p0, LX/392;

    invoke-interface {p0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    const/4 v2, 0x0

    .line 461435
    if-nez v0, :cond_3

    move v1, v2

    .line 461436
    :goto_1
    move v0, v1

    .line 461437
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 461438
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 461439
    const-class v3, LX/0ew;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ew;

    .line 461440
    if-eqz v1, :cond_4

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v3

    if-nez v3, :cond_5

    :cond_4
    move v1, v2

    .line 461441
    goto :goto_1

    .line 461442
    :cond_5
    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v4

    .line 461443
    sget-object v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_8

    sget-object v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 461444
    invoke-virtual {v4, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 461445
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result p0

    if-eqz p0, :cond_7

    .line 461446
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object p0, p0

    .line 461447
    if-eqz p0, :cond_7

    instance-of p0, v1, LX/7LM;

    if-nez p0, :cond_7

    .line 461448
    iget-object v3, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v3

    .line 461449
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_1

    .line 461450
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :cond_8
    move v1, v2

    .line 461451
    goto :goto_1
.end method

.method private static b(LX/3EE;LX/1Pe;)LX/04D;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "TE;)",
            "LX/04D;"
        }
    .end annotation

    .prologue
    .line 461420
    iget-object v0, p0, LX/3EE;->e:LX/04D;

    if-eqz v0, :cond_0

    .line 461421
    iget-object v0, p0, LX/3EE;->e:LX/04D;

    .line 461422
    :goto_0
    return-object v0

    .line 461423
    :cond_0
    instance-of v0, p1, LX/3Ij;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 461424
    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    check-cast p1, LX/3Ij;

    .line 461425
    iget-object v1, p1, LX/3Ij;->p:Ljava/lang/String;

    move-object v1, v1

    .line 461426
    invoke-static {v0, v1}, LX/3Ik;->a(LX/1PT;Ljava/lang/String;)LX/04D;

    move-result-object v0

    goto :goto_0

    .line 461427
    :cond_1
    check-cast p1, LX/1Po;

    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
    .locals 33

    .prologue
    .line 461418
    new-instance v2, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const-class v5, LX/2mZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2mZ;

    const-class v6, LX/1VK;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1VK;

    invoke-static/range {p0 .. p0}, LX/2mk;->a(LX/0QB;)LX/2mk;

    move-result-object v7

    check-cast v7, LX/2mk;

    invoke-static/range {p0 .. p0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v8

    check-cast v8, LX/2ml;

    const/16 v9, 0x1494

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v10

    check-cast v10, LX/23s;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v11

    check-cast v11, LX/0tK;

    invoke-static/range {p0 .. p0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v12

    check-cast v12, LX/2mn;

    invoke-static/range {p0 .. p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v13

    check-cast v13, LX/1C2;

    invoke-static/range {p0 .. p0}, LX/1Yd;->a(LX/0QB;)LX/1Yd;

    move-result-object v14

    check-cast v14, LX/1Yd;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v15

    check-cast v15, LX/0bH;

    const-class v16, LX/2mo;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/2mo;

    invoke-static/range {p0 .. p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v17

    check-cast v17, LX/1CK;

    invoke-static/range {p0 .. p0}, LX/2mp;->a(LX/0QB;)LX/2mp;

    move-result-object v18

    check-cast v18, LX/2mp;

    invoke-static/range {p0 .. p0}, LX/1Yk;->a(LX/0QB;)LX/1Yk;

    move-result-object v19

    check-cast v19, LX/1Yk;

    invoke-static/range {p0 .. p0}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v20

    check-cast v20, LX/2mq;

    invoke-static/range {p0 .. p0}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v21

    check-cast v21, LX/1Ad;

    invoke-static/range {p0 .. p0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v22

    check-cast v22, LX/198;

    invoke-static/range {p0 .. p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v23

    check-cast v23, LX/0yc;

    invoke-static/range {p0 .. p0}, LX/1AY;->a(LX/0QB;)LX/1AY;

    move-result-object v24

    check-cast v24, LX/1AY;

    invoke-static/range {p0 .. p0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v25

    check-cast v25, LX/1YQ;

    invoke-static/range {p0 .. p0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v26

    check-cast v26, LX/0iX;

    invoke-static/range {p0 .. p0}, LX/2mr;->a(LX/0QB;)LX/2mr;

    move-result-object v27

    check-cast v27, LX/2mr;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v28

    check-cast v28, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/2ms;->a(LX/0QB;)LX/2ms;

    move-result-object v29

    check-cast v29, LX/2ms;

    const-class v30, LX/2mu;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/2mu;

    const/16 v31, 0x1d74

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x14b9

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v32

    invoke-direct/range {v2 .. v32}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;-><init>(LX/0Uh;LX/1qa;LX/2mZ;LX/1VK;LX/2mk;LX/2ml;LX/0Or;LX/23s;LX/0tK;LX/2mn;LX/1C2;LX/1Yd;LX/0bH;LX/2mo;LX/1CK;LX/2mp;LX/1Yk;LX/2mq;LX/1Ad;LX/198;LX/0yc;LX/1AY;LX/1YQ;LX/0iX;LX/2mr;LX/0ad;LX/2ms;LX/2mu;LX/0Ot;LX/0Or;)V

    .line 461419
    return-object v2
.end method


# virtual methods
.method public final a(LX/3EE;LX/1Pe;)LX/3FN;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "TE;)",
            "LX/3FN;"
        }
    .end annotation

    .prologue
    .line 461365
    move-object/from16 v0, p1

    iget-object v6, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v12

    check-cast v12, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 461366
    move-object/from16 v0, p1

    iget-object v6, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v6}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v13

    .line 461367
    invoke-static {v13}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461368
    invoke-virtual {v13}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    move-object v14, v6

    check-cast v14, Lcom/facebook/graphql/model/GraphQLStory;

    .line 461369
    invoke-static/range {p1 .. p2}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b(LX/3EE;LX/1Pe;)LX/04D;

    move-result-object v16

    .line 461370
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-static {v6}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v10

    .line 461371
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 461372
    invoke-direct/range {p0 .. p1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3EE;)LX/3FO;

    move-result-object v24

    .line 461373
    invoke-static {v13}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v7

    .line 461374
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->d:LX/1qa;

    move-object/from16 v0, v24

    iget v9, v0, LX/3FO;->a:I

    sget-object v11, LX/26P;->Video:LX/26P;

    invoke-virtual {v8, v6, v9, v11}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {v6}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v9

    .line 461375
    new-instance v8, LX/2oK;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->f:LX/1VK;

    invoke-direct {v8, v13, v10, v6}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    move-object/from16 v6, p2

    .line 461376
    check-cast v6, LX/1Pr;

    invoke-static {v13}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v11

    invoke-interface {v6, v8, v11}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, LX/2oL;

    .line 461377
    invoke-virtual/range {v20 .. v20}, LX/2oL;->b()LX/2oO;

    move-result-object v6

    invoke-virtual {v6, v10}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 461378
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->i:LX/23s;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v6, p2

    check-cast v6, LX/1Po;

    invoke-interface {v6}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    invoke-interface {v6}, LX/1PT;->a()LX/1Qt;

    move-result-object v6

    invoke-virtual {v8, v11, v6}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v6

    .line 461379
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->e:LX/2mZ;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v8, v11, v10}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v8

    .line 461380
    invoke-virtual/range {v20 .. v20}, LX/2oL;->b()LX/2oO;

    move-result-object v11

    invoke-virtual {v11}, LX/2oO;->l()Z

    move-result v11

    .line 461381
    const/4 v15, 0x1

    invoke-virtual {v8, v15, v11}, LX/3Im;->a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v15

    .line 461382
    invoke-static {v13}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v8

    .line 461383
    new-instance v11, LX/0AW;

    invoke-direct {v11, v8}, LX/0AW;-><init>(LX/162;)V

    invoke-virtual {v11, v7}, LX/0AW;->a(Z)LX/0AW;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/0AW;->a(LX/04H;)LX/0AW;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, LX/2oL;->c()LX/04g;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0AW;->a(LX/04g;)LX/0AW;

    move-result-object v6

    invoke-virtual {v6}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v8

    .line 461384
    new-instance v6, LX/395;

    new-instance v7, LX/0AV;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v7

    move-object/from16 v0, p1

    iget-object v11, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v6 .. v11}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 461385
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, LX/395;->a(LX/04D;)LX/395;

    .line 461386
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v7

    .line 461387
    const-string v11, "GraphQLStoryProps"

    move-object/from16 v0, p1

    iget-object v0, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v11, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v11

    const-string v17, "SubtitlesLocalesKey"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v11

    const-string v17, "ShowDeleteOptionKey"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 461388
    if-eqz v9, :cond_0

    .line 461389
    const-string v11, "CoverImageParamsKey"

    invoke-virtual {v7, v11, v9}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 461390
    :cond_0
    move-object/from16 v0, v24

    iget v11, v0, LX/3FO;->d:I

    if-lez v11, :cond_5

    move-object/from16 v0, v24

    iget v11, v0, LX/3FO;->a:I

    int-to-double v0, v11

    move-wide/from16 v18, v0

    move-object/from16 v0, v24

    iget v11, v0, LX/3FO;->d:I

    int-to-double v0, v11

    move-wide/from16 v22, v0

    div-double v18, v18, v22

    .line 461391
    :goto_0
    invoke-static {v10}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Lcom/facebook/graphql/model/GraphQLVideo;)LX/1bf;

    move-result-object v11

    .line 461392
    if-eqz v11, :cond_1

    .line 461393
    const-string v17, "BlurredCoverImageParamsKey"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 461394
    :cond_1
    const-string v11, "WATCH_AND_SHOP_PLUGIN_VIDEO_PARAMS_KEY"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->g:LX/2mk;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/2mk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v11, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 461395
    invoke-virtual/range {v20 .. v20}, LX/2oL;->i()LX/7DJ;

    move-result-object v11

    .line 461396
    if-eqz v11, :cond_2

    .line 461397
    const-string v11, "SphericalViewportStateKey"

    invoke-virtual/range {v20 .. v20}, LX/2oL;->i()LX/7DJ;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v11, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 461398
    :cond_2
    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    .line 461399
    new-instance v11, LX/2pZ;

    invoke-direct {v11}, LX/2pZ;-><init>()V

    invoke-virtual {v11, v15}, LX/2pZ;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pZ;

    move-result-object v11

    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v11

    invoke-virtual {v11, v7}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v7

    invoke-virtual {v7}, LX/2pZ;->b()LX/2pa;

    move-result-object v18

    .line 461400
    new-instance v21, LX/3Is;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, LX/3Is;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/2oL;LX/1Pe;LX/3EE;)V

    .line 461401
    new-instance v25, LX/3Iu;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3, v14}, LX/3Iu;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/2oL;LX/1Pe;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 461402
    const/16 v27, 0x0

    .line 461403
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->F:Z

    if-eqz v7, :cond_3

    .line 461404
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->x:LX/2mu;

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v7, v11}, LX/2mu;->a(Ljava/lang/Boolean;)Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-result-object v27

    .line 461405
    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/3Ge;->a(LX/2pa;)V

    .line 461406
    :cond_3
    new-instance v11, LX/3FN;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->j:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    invoke-virtual/range {v20 .. v20}, LX/2oL;->b()LX/2oO;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->u:LX/0iX;

    move-object/from16 v26, v0

    move-object v14, v10

    move-object/from16 v17, v8

    move-object/from16 v22, v9

    move-object/from16 v23, v6

    invoke-direct/range {v11 .. v27}, LX/3FN;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ZLX/04D;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/2pa;LX/2oO;LX/2oL;LX/3It;LX/1bf;LX/395;LX/3FO;LX/3Iv;LX/0iX;Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;)V

    .line 461407
    new-instance v19, LX/3Iw;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->D:LX/1YQ;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->B:LX/198;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->m:LX/1C2;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->n:LX/1Yd;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->o:LX/0bH;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/3EE;->c:LX/0am;

    move-object/from16 v26, v0

    move-object/from16 v21, v11

    invoke-direct/range {v19 .. v26}, LX/3Iw;-><init>(LX/1YQ;LX/3FN;LX/198;LX/1C2;LX/1Yd;LX/0bH;LX/0am;)V

    move-object/from16 v0, v19

    iput-object v0, v11, LX/3FN;->t:LX/3Iw;

    move-object/from16 v6, p2

    .line 461408
    check-cast v6, LX/1Pq;

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v6}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3FN;LX/1Pq;)LX/3Ix;

    move-result-object v7

    .line 461409
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->p:LX/2mo;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v6, p2

    check-cast v6, LX/1Pq;

    invoke-virtual {v8, v10, v7, v6}, LX/2mo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3Iy;LX/1Pq;)LX/3Iz;

    move-result-object v6

    .line 461410
    iget-object v7, v11, LX/3FN;->t:LX/3Iw;

    invoke-virtual {v7, v6}, LX/3Iw;->a(LX/3J0;)V

    .line 461411
    move-object/from16 v0, p1

    iget-object v7, v0, LX/3EE;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 461412
    check-cast p2, LX/1Pt;

    sget-object v6, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->H:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    invoke-interface {v0, v9, v6}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 461413
    iget-object v6, v11, LX/3FN;->h:LX/2pa;

    iget-object v6, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v11, LX/3FN;->i:LX/2oO;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v6, v7, v1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2oO;LX/04D;)V

    .line 461414
    invoke-virtual/range {v18 .. v18}, LX/2pa;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->c:Z

    if-eqz v6, :cond_4

    .line 461415
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->z:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bwf;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, LX/Bwf;->a(LX/2pa;)V

    .line 461416
    :cond_4
    return-object v11

    .line 461417
    :cond_5
    const-wide/16 v18, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/3EE;LX/3FN;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "LX/3FN;",
            "TV;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 461307
    iget-object v1, p2, LX/3FN;->t:LX/3Iw;

    move-object v0, p3

    check-cast v0, LX/3FT;

    invoke-interface {v0}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 461308
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v1, LX/3Iw;->i:Ljava/lang/ref/WeakReference;

    .line 461309
    iput-object p3, p2, LX/3FN;->l:Ljava/lang/Object;

    .line 461310
    invoke-static {p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461311
    :goto_0
    return-void

    .line 461312
    :cond_0
    iget-object v0, p2, LX/3FN;->v:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    if-eqz v0, :cond_6

    iget-object v0, p2, LX/3FN;->v:Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-object v1, v0

    :goto_1
    move-object v0, p3

    .line 461313
    check-cast v0, LX/3FT;

    invoke-interface {v0}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v4, p2, LX/3FN;->h:LX/2pa;

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v4

    .line 461314
    iget-object v0, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 461315
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 461316
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 461317
    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_1
    move v1, v2

    .line 461318
    :goto_2
    if-nez v1, :cond_8

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 461319
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->B:LX/198;

    invoke-static {}, LX/3JE;->c()LX/3JE;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/198;->c(LX/1YD;)V

    .line 461320
    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 461321
    iget-object v1, p2, LX/3FN;->p:LX/3FO;

    iget v1, v1, LX/3FO;->a:I

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 461322
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->C:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->C:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->d()I

    move-result v1

    :goto_4
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 461323
    invoke-virtual {v4, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 461324
    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v2

    .line 461325
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->t:LX/2mq;

    invoke-virtual {v1, v4, v0}, LX/2mq;->a(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 461326
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->A:LX/1Ad;

    sget-object v5, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->H:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v5, p2, LX/3FN;->n:LX/1bf;

    invoke-virtual {v1, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 461327
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->A:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 461328
    iget-object v0, p1, LX/3EE;->c:LX/0am;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/0am;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundResource(I)V

    .line 461329
    iget-object v0, p2, LX/3FN;->f:LX/04D;

    invoke-virtual {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 461330
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    invoke-virtual {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 461331
    iget-object v0, p2, LX/3FN;->h:LX/2pa;

    iget-boolean v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->G:Z

    .line 461332
    iget-object v2, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 461333
    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 461334
    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v4, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 461335
    :cond_2
    if-nez v1, :cond_b

    .line 461336
    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->w:LX/0ad;

    sget-short v5, LX/0ws;->eD:S

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 461337
    invoke-virtual {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2pa;)V

    .line 461338
    :goto_5
    iget-object v0, p2, LX/3FN;->m:LX/3It;

    .line 461339
    iput-object v0, v4, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 461340
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->h:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-eqz v0, :cond_3

    .line 461341
    iget-object v0, p2, LX/3FN;->a:LX/3Iv;

    .line 461342
    iput-object v0, v4, Lcom/facebook/video/player/RichVideoPlayer;->y:LX/3Iv;

    .line 461343
    :cond_3
    invoke-static {p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p2, LX/3FN;->r:Z

    .line 461344
    iget-object v0, p2, LX/3FN;->k:LX/093;

    invoke-virtual {v0, v3}, LX/093;->a(Z)V

    .line 461345
    iget-object v0, p2, LX/3FN;->j:LX/2oL;

    .line 461346
    iget-boolean v1, v0, LX/2oL;->a:Z

    move v0, v1

    .line 461347
    if-eqz v0, :cond_4

    .line 461348
    sget-object v0, LX/04g;->UNSET:LX/04g;

    invoke-static {p2, v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3FN;LX/04g;)V

    .line 461349
    :cond_4
    iget-object v0, p2, LX/3FN;->j:LX/2oL;

    .line 461350
    iget-object v1, v0, LX/2oL;->h:LX/2oN;

    move-object v0, v1

    .line 461351
    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_5

    .line 461352
    iget-boolean v0, v4, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    move v0, v0

    .line 461353
    if-eqz v0, :cond_5

    iget-object v0, p2, LX/3FN;->j:LX/2oL;

    .line 461354
    iget-boolean v1, v0, LX/2oL;->d:Z

    move v0, v1

    .line 461355
    if-eqz v0, :cond_5

    .line 461356
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->B:LX/198;

    sget-object v1, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    new-instance v2, LX/7K4;

    iget-object v5, p2, LX/3FN;->j:LX/2oL;

    invoke-virtual {v5}, LX/2oL;->a()I

    move-result v5

    iget-object v6, p2, LX/3FN;->j:LX/2oL;

    invoke-virtual {v6}, LX/2oL;->a()I

    move-result v6

    invoke-direct {v2, v5, v6}, LX/7K4;-><init>(II)V

    invoke-static {v4, p2, v0, v1, v2}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/3FN;LX/198;LX/04g;LX/7K4;)V

    .line 461357
    iput-boolean v3, v4, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 461358
    :cond_5
    check-cast p3, LX/392;

    iget-object v0, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p3, v0}, LX/392;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, p3

    .line 461359
    check-cast v0, LX/392;

    invoke-interface {v0}, LX/392;->getPluginSelector()LX/3Ge;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :cond_7
    move v1, v3

    .line 461360
    goto/16 :goto_2

    :cond_8
    move v1, v3

    .line 461361
    goto/16 :goto_3

    .line 461362
    :cond_9
    iget-object v1, p2, LX/3FN;->p:LX/3FO;

    iget v1, v1, LX/3FO;->b:I

    goto/16 :goto_4

    .line 461363
    :cond_a
    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 461364
    :cond_b
    invoke-virtual {v4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_5
.end method

.method public final b(LX/3EE;LX/3FN;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "LX/3FN;",
            "TV;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 461255
    iput-object v4, p2, LX/3FN;->l:Ljava/lang/Object;

    .line 461256
    invoke-static {p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461257
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p3

    .line 461258
    check-cast v0, LX/392;

    iget-object v1, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v0, v1}, LX/392;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 461259
    check-cast p3, LX/3FT;

    invoke-interface {p3}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    .line 461260
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v0

    .line 461261
    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->E:LX/1AY;

    invoke-virtual {v2, v1}, LX/1AY;->a(Landroid/view/View;)LX/7zf;

    move-result-object v2

    invoke-static {v2}, LX/1AY;->a(LX/7zf;)Z

    move-result v2

    .line 461262
    iget-object v3, p2, LX/3FN;->j:LX/2oL;

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 461263
    :goto_1
    iput-boolean v0, v3, LX/2oL;->d:Z

    .line 461264
    iput-object v4, v1, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 461265
    iput-object v4, v1, Lcom/facebook/video/player/RichVideoPlayer;->y:LX/3Iv;

    .line 461266
    iget-boolean v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->F:Z

    if-eqz v0, :cond_0

    .line 461267
    iget-object v0, p2, LX/3FN;->j:LX/2oL;

    .line 461268
    iget-boolean v2, v0, LX/2oL;->d:Z

    move v0, v2

    .line 461269
    if-nez v0, :cond_0

    .line 461270
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    goto :goto_0

    .line 461271
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
