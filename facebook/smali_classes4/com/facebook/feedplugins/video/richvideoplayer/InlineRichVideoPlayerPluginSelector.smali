.class public Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final n:Landroid/content/Context;

.field private final o:LX/0tQ;

.field private final p:LX/0xX;

.field private final q:LX/0iY;

.field private final r:LX/0Uh;

.field private final s:LX/0W3;

.field private t:Lcom/facebook/video/player/plugins/VideoPlugin;

.field private u:LX/2pM;

.field private v:LX/3Gg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 541424
    const-class v0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/0xX;LX/0iY;LX/0tQ;LX/0Uh;LX/0W3;LX/3Gg;)V
    .locals 2
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 541411
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 541412
    iput-object p1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    .line 541413
    iput-object p4, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->p:LX/0xX;

    .line 541414
    iput-object p5, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->q:LX/0iY;

    .line 541415
    iput-object p6, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->o:LX/0tQ;

    .line 541416
    iput-object p7, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->r:LX/0Uh;

    .line 541417
    iput-object p8, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->s:LX/0W3;

    .line 541418
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->k:Z

    .line 541419
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    const-class v1, LX/3GZ;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->a:LX/0Px;

    .line 541420
    iput-object p9, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->v:LX/3Gg;

    .line 541421
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541422
    invoke-virtual {p0}, LX/3Ge;->a()V

    .line 541423
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 541401
    const-class v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 541402
    :goto_0
    const-class v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 541403
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 541404
    sget-object v0, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    .line 541405
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 541406
    goto :goto_0

    :cond_1
    move v1, v2

    .line 541407
    goto :goto_1

    .line 541408
    :cond_2
    if-eqz v0, :cond_3

    .line 541409
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_2

    .line 541410
    :cond_3
    invoke-super {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 541396
    invoke-virtual {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v2

    .line 541397
    sget-object v3, LX/3J9;->a:[I

    invoke-virtual {v2}, LX/3J8;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 541398
    if-ne p2, v2, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    .line 541399
    :pswitch_0
    sget-object v2, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    if-ne p2, v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 541400
    :pswitch_1
    sget-object v2, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_2

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    if-ne p2, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541384
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 541385
    new-instance v0, LX/2pM;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2pM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->u:LX/2pM;

    .line 541386
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Gh;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Gh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 541387
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->p:LX/0xX;

    invoke-virtual {v1}, LX/0xX;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 541388
    new-instance v1, LX/3Gm;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Gm;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541389
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->q:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 541390
    new-instance v1, LX/Bx2;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bx2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541391
    :cond_1
    :goto_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 541392
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->v:LX/3Gg;

    .line 541393
    iget-object v2, v1, LX/3Gg;->a:LX/0ad;

    sget-short v3, LX/0ws;->dI:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 541394
    if-eqz v1, :cond_1

    .line 541395
    new-instance v1, LX/7N8;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/7N8;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541373
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->u:LX/2pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Hp;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Hp;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541425
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 541426
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->o:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->C()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 541427
    new-instance v1, LX/Bwm;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bwm;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541428
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->o:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->J()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 541429
    new-instance v1, LX/BwY;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/BwY;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541430
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->r:LX/0Uh;

    const/16 v2, 0x427

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 541431
    new-instance v1, LX/31H;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/31H;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541432
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->s:LX/0W3;

    sget-wide v2, LX/0X5;->ey:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 541433
    new-instance v1, LX/Bwq;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bwq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541434
    :cond_2
    new-instance v1, LX/3Hz;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Hz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3I6;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3I6;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 541435
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->u:LX/2pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 541436
    :cond_4
    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->o:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 541437
    new-instance v1, LX/Bwa;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bwa;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1
.end method

.method public final f()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541383
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->u:LX/2pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3I7;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3I7;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Ie;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Ie;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541380
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    .line 541381
    new-instance v1, LX/3Hp;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Hp;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 541382
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541377
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-nez v0, :cond_0

    .line 541378
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 541379
    :cond_0
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->t:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Ig;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Ig;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541376
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541375
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->u:LX/2pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3I7;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3I7;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Ie;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Ie;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541374
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;->f()LX/0Px;

    move-result-object v0

    return-object v0
.end method
