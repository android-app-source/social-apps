.class public Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/392;",
        ":",
        "LX/3FP;",
        ":",
        "LX/3FT;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3EE;",
        "LX/3FN;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field public final a:LX/198;

.field private final b:LX/1VK;

.field private final c:LX/094;

.field private final d:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

.field private final i:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field public final j:LX/1YQ;

.field private final k:LX/03V;

.field private final l:LX/2mr;

.field public final m:LX/0iY;

.field public final n:LX/0iX;

.field private final o:LX/2ms;


# direct methods
.method public constructor <init>(LX/1C2;LX/198;LX/094;LX/1VK;Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/1YQ;LX/03V;LX/2mr;LX/0iY;LX/2ms;LX/0iX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1C2;",
            "LX/198;",
            "LX/094;",
            "LX/1VK;",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            "Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;",
            "LX/1YQ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2mr;",
            "LX/0iY;",
            "LX/2ms;",
            "LX/0iX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461072
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 461073
    iput-object p10, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->i:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    .line 461074
    iput-object p2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a:LX/198;

    .line 461075
    iput-object p3, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->c:LX/094;

    .line 461076
    iput-object p4, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->b:LX/1VK;

    .line 461077
    iput-object p5, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->g:Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    .line 461078
    iput-object p6, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->h:Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    .line 461079
    iput-object p7, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->d:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    .line 461080
    iput-object p8, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->e:LX/0Ot;

    .line 461081
    iput-object p9, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->f:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    .line 461082
    iput-object p11, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->j:LX/1YQ;

    .line 461083
    iput-object p12, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->k:LX/03V;

    .line 461084
    iput-object p13, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->l:LX/2mr;

    .line 461085
    iput-object p14, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->m:LX/0iY;

    .line 461086
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->o:LX/2ms;

    .line 461087
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->n:LX/0iX;

    .line 461088
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .locals 3

    .prologue
    .line 461089
    const-class v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    monitor-enter v1

    .line 461090
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461091
    sput-object v2, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461092
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461093
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461094
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461095
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/3EE;LX/3FN;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "LX/3FN;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 461097
    const-string v0, "RichVideoPlayerPartDefinition.bind"

    const v1, 0x758b6582

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 461098
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->i:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3EE;LX/3FN;Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461099
    const v0, -0x21491c3b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 461100
    return-void

    .line 461101
    :catchall_0
    move-exception v0

    const v1, 0x2279cd53

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .locals 18

    .prologue
    .line 461102
    new-instance v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static/range {p0 .. p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static/range {p0 .. p0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v3

    check-cast v3, LX/198;

    invoke-static/range {p0 .. p0}, LX/094;->a(LX/0QB;)LX/094;

    move-result-object v4

    check-cast v4, LX/094;

    const-class v5, LX/1VK;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1VK;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    const/16 v9, 0x846

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-static/range {p0 .. p0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v12

    check-cast v12, LX/1YQ;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/2mr;->a(LX/0QB;)LX/2mr;

    move-result-object v14

    check-cast v14, LX/2mr;

    invoke-static/range {p0 .. p0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v15

    check-cast v15, LX/0iY;

    invoke-static/range {p0 .. p0}, LX/2ms;->a(LX/0QB;)LX/2ms;

    move-result-object v16

    check-cast v16, LX/2ms;

    invoke-static/range {p0 .. p0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v17

    check-cast v17, LX/0iX;

    invoke-direct/range {v1 .. v17}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;-><init>(LX/1C2;LX/198;LX/094;LX/1VK;Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/1YQ;LX/03V;LX/2mr;LX/0iY;LX/2ms;LX/0iX;)V

    .line 461103
    return-object v1
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 461104
    check-cast p2, LX/3EE;

    check-cast p3, LX/1Pe;

    .line 461105
    const-string v0, "RichVideoPlayerPartDefinition.prepare"

    const v1, -0x273c58d1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 461106
    :try_start_0
    iget-object v0, p2, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 461107
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 461108
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 461109
    iget-object v1, p2, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 461110
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461111
    iget-object v1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v1

    .line 461112
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 461113
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 461114
    new-instance v1, LX/2oK;

    iget-object v4, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->b:LX/1VK;

    invoke-direct {v1, v3, v0, v4}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 461115
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->i:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v0, p2, p3}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3EE;LX/1Pe;)LX/3FN;

    move-result-object v8

    .line 461116
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->d:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    new-instance v3, LX/36r;

    iget-object v4, p2, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v5, p2, LX/3EE;->b:I

    invoke-direct {v3, v4, v5}, LX/36r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 461117
    iget-object v9, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->f:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    new-instance v0, LX/3J1;

    iget-object v3, v8, LX/3FN;->k:LX/093;

    iget-object v4, v8, LX/3FN;->h:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v8, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v6, v8, LX/3FN;->f:LX/04D;

    new-instance v7, LX/3GD;

    invoke-direct {v7, p0}, LX/3GD;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V

    invoke-direct/range {v0 .. v7}, LX/3J1;-><init>(LX/1KL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    invoke-interface {p1, v9, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 461118
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/3J3;

    iget-object v2, v8, LX/3FN;->h:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, v8, LX/3FN;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3J3;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 461119
    iget-object v7, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->g:Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    new-instance v9, LX/3FZ;

    new-instance v0, LX/3FW;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a:LX/198;

    iget-object v3, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->c:LX/094;

    iget-object v4, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->k:LX/03V;

    iget-object v5, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->o:LX/2ms;

    iget-object v6, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->l:LX/2mr;

    move-object v1, v8

    invoke-direct/range {v0 .. v6}, LX/3FW;-><init>(LX/3FN;LX/198;LX/094;LX/03V;LX/2ms;LX/2mr;)V

    .line 461120
    sget-object v1, LX/3J4;->INSTANCE:LX/3J4;

    move-object v1, v1

    .line 461121
    invoke-direct {v9, v0, v1}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;LX/0QK;)V

    invoke-interface {p1, v7, v9}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 461122
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->h:Lcom/facebook/feedplugins/base/VideoRegistryPartDefinition;

    iget-object v1, v8, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461123
    const v0, 0xebbe4ac

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v8

    :catchall_0
    move-exception v0

    const v1, -0x6ec7ba9a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x312ce348

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461124
    check-cast p1, LX/3EE;

    check-cast p2, LX/3FN;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/3EE;LX/3FN;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, 0x44bc203

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 461125
    check-cast p1, LX/3EE;

    check-cast p2, LX/3FN;

    .line 461126
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->i:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v0, p1, p2, p4}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->b(LX/3EE;LX/3FN;Landroid/view/View;)V

    .line 461127
    return-void
.end method
