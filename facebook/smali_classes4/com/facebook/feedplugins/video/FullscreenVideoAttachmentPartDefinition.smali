.class public Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1PV;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/7gM;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/2mZ;

.field private final e:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:LX/2mY;

.field private final i:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

.field public final j:LX/1VK;

.field private final k:LX/0qn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460808
    const-class v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 460809
    new-instance v0, LX/2mW;

    invoke-direct {v0}, LX/2mW;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;LX/2mY;LX/2mZ;Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/1VK;LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460761
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460762
    iput-object p1, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    .line 460763
    iput-object p2, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    .line 460764
    iput-object p3, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    .line 460765
    iput-object p4, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->h:LX/2mY;

    .line 460766
    iput-object p5, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->d:LX/2mZ;

    .line 460767
    iput-object p6, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    .line 460768
    iput-object p7, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    .line 460769
    iput-object p8, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->j:LX/1VK;

    .line 460770
    iput-object p9, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->k:LX/0qn;

    .line 460771
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;
    .locals 13

    .prologue
    .line 460797
    const-class v1, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 460798
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460799
    sput-object v2, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460800
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460801
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460802
    new-instance v3, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    const-class v7, LX/2mY;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2mY;

    const-class v8, LX/2mZ;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    const-class v11, LX/1VK;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1VK;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v12

    check-cast v12, LX/0qn;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;LX/2mY;LX/2mZ;Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/1VK;LX/0qn;)V

    .line 460803
    move-object v0, v3

    .line 460804
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460805
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460806
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460810
    sget-object v0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 460773
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 460774
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->h:LX/2mY;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v1, v2}, LX/2mY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0am;Lcom/facebook/common/callercontext/CallerContext;)LX/C2F;

    move-result-object v0

    .line 460775
    iget-object v1, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->d:LX/2mZ;

    .line 460776
    iget-object v2, v0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v2, v2

    .line 460777
    invoke-virtual {v1, p2, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    .line 460778
    new-instance v2, LX/C2G;

    invoke-direct {v2, v0, v1}, LX/C2G;-><init>(LX/C2F;LX/3Im;)V

    .line 460779
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460780
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460781
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460782
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 460783
    iget-object v1, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->k:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_0

    .line 460784
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460785
    iget-object v0, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->i:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    new-instance v1, LX/3FV;

    .line 460786
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 460787
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460788
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 460789
    new-instance v4, LX/2oK;

    iget-object v5, p0, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->j:LX/1VK;

    invoke-direct {v4, v3, v2, v5}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 460790
    check-cast p3, LX/1Pr;

    .line 460791
    iget-object v2, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460792
    check-cast v2, LX/0jW;

    invoke-interface {p3, v4, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2oL;

    move-object v2, v2

    .line 460793
    invoke-virtual {v2}, LX/2oL;->b()LX/2oO;

    move-result-object v2

    .line 460794
    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v4, LX/CD0;

    invoke-direct {v4, p0, v2}, LX/CD0;-><init>(Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;LX/2oO;)V

    invoke-direct {v3, v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object v2, v3

    .line 460795
    invoke-direct {v1, p2, v2}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460796
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460772
    const/4 v0, 0x1

    return v0
.end method
