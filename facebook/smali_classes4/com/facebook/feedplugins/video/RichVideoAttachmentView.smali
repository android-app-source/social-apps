.class public Lcom/facebook/feedplugins/video/RichVideoAttachmentView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/3FP;
.implements LX/3FQ;
.implements LX/392;
.implements LX/0hY;
.implements LX/3FR;
.implements LX/3FS;
.implements LX/3FT;
.implements LX/3FU;
.implements LX/1a7;


# instance fields
.field public a:LX/3Ge;

.field public b:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2mu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/38r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2mk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/video/player/RichVideoPlayer;

.field private h:Z

.field private i:Z

.field private j:LX/3GZ;

.field private k:LX/7QP;

.field private l:Landroid/view/View;

.field private m:Z

.field private final n:LX/3GX;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 539566
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 539567
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 539560
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 539561
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 539568
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 539569
    iput-boolean v3, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->h:Z

    .line 539570
    new-instance v0, LX/3Fc;

    invoke-direct {v0, p0}, LX/3Fc;-><init>(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->n:LX/3GX;

    .line 539571
    const-class v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 539572
    const v0, 0x7f0311f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 539573
    const v0, 0x7f0d2a31

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    .line 539574
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 539575
    new-instance v0, LX/3GZ;

    invoke-direct {v0, p1}, LX/3GZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->j:LX/3GZ;

    .line 539576
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->j:LX/3GZ;

    .line 539577
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 539578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->k:LX/7QP;

    .line 539579
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->f:LX/0Uh;

    const/16 v1, 0x2bf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 539580
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->c:LX/2mu;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2mu;->a(Ljava/lang/Boolean;)Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a:LX/3Ge;

    .line 539581
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/19m;LX/2mu;LX/38r;LX/2mk;LX/0Uh;)V
    .locals 0

    .prologue
    .line 539582
    iput-object p1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->b:LX/19m;

    iput-object p2, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->c:LX/2mu;

    iput-object p3, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->d:LX/38r;

    iput-object p4, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->e:LX/2mk;

    iput-object p5, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->f:LX/0Uh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-static {v5}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v1

    check-cast v1, LX/19m;

    const-class v2, LX/2mu;

    invoke-interface {v5, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2mu;

    invoke-static {v5}, LX/38r;->a(LX/0QB;)LX/38r;

    move-result-object v3

    check-cast v3, LX/38r;

    invoke-static {v5}, LX/2mk;->a(LX/0QB;)LX/2mk;

    move-result-object v4

    check-cast v4, LX/2mk;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static/range {v0 .. v5}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/19m;LX/2mu;LX/38r;LX/2mk;LX/0Uh;)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 539583
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 539584
    if-eqz v0, :cond_0

    .line 539585
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539586
    if-eqz v1, :cond_0

    .line 539587
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v1

    .line 539588
    invoke-virtual {v0}, LX/2pa;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 539589
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    const-class v2, LX/3I7;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, LX/3I7;

    .line 539590
    const/4 v2, 0x0

    .line 539591
    if-eqz v0, :cond_3

    .line 539592
    invoke-virtual {v0}, LX/3I7;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539593
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 539594
    :cond_0
    invoke-virtual {v0, p1}, LX/3I7;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 539595
    if-nez v2, :cond_2

    .line 539596
    iget-boolean v3, v0, LX/3I7;->n:Z

    move v0, v3

    .line 539597
    if-eqz v0, :cond_2

    .line 539598
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->performClick()Z

    move v0, v1

    .line 539599
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v2

    :goto_1
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 539600
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->d:LX/38r;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->n:LX/3GX;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 539601
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 539604
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->d:LX/38r;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->n:LX/3GX;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 539605
    return-void
.end method


# virtual methods
.method public final a(LX/04g;I)V
    .locals 1

    .prologue
    .line 539602
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 539603
    return-void
.end method

.method public final a(LX/7QP;)V
    .locals 2

    .prologue
    .line 539635
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->k:LX/7QP;

    if-eq v0, p1, :cond_0

    .line 539636
    iput-object p1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->k:LX/7QP;

    .line 539637
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/7QP;)V

    .line 539638
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Z)V

    .line 539639
    :cond_0
    return-void

    .line 539640
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 539632
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->e:LX/2mk;

    invoke-virtual {v0, p1}, LX/2mk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539633
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->e()V

    .line 539634
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 539621
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    if-eq v0, p1, :cond_0

    .line 539622
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 539623
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->h:Z

    if-nez v0, :cond_2

    .line 539624
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 539625
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v0, :cond_1

    .line 539626
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 539627
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 539628
    :cond_2
    iput-object p1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 539629
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->h:Z

    .line 539630
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->requestLayout()V

    .line 539631
    return-void
.end method

.method public final a(LX/31M;II)Z
    .locals 1

    .prologue
    .line 539641
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a()Z

    move-result v0

    return v0
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 539619
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 539620
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 539616
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->e:LX/2mk;

    invoke-virtual {v0, p1}, LX/2mk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539617
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->f()V

    .line 539618
    :cond_0
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 2

    .prologue
    .line 539606
    iget-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->h:Z

    if-eqz v0, :cond_0

    .line 539607
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 539608
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 539609
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 539610
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 539611
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->l:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 539612
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->detachViewFromParent(Landroid/view/View;)V

    .line 539613
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->requestLayout()V

    .line 539614
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->h:Z

    .line 539615
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 539564
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 539565
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getAdditionalPlugins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 539507
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAndClearShowLiveCommentDialogFragment()Z
    .locals 2

    .prologue
    .line 539508
    iget-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->i:Z

    .line 539509
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->i:Z

    .line 539510
    return v0
.end method

.method public getAudioChannelLayout()LX/03z;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 539511
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 539512
    if-eqz v0, :cond_0

    .line 539513
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539514
    if-eqz v1, :cond_0

    .line 539515
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539516
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_0

    .line 539517
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539518
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v1, :cond_0

    .line 539519
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v1

    .line 539520
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->a()LX/03z;

    move-result-object v0

    .line 539521
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 539522
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v0

    return v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 539523
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    return-object v0
.end method

.method public getPluginSelector()LX/3Ge;
    .locals 1

    .prologue
    .line 539524
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a:LX/3Ge;

    return-object v0
.end method

.method public getProjectionType()LX/19o;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 539525
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 539526
    if-eqz v0, :cond_0

    .line 539527
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539528
    if-eqz v1, :cond_0

    .line 539529
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539530
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_0

    .line 539531
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 539532
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v1, :cond_0

    .line 539533
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v1

    .line 539534
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 539535
    iget-object v1, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    move-object v0, v1

    .line 539536
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 539537
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 539538
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public getTransitionNode()LX/3FT;
    .locals 0

    .prologue
    .line 539539
    return-object p0
.end method

.method public getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 539540
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 539541
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 539542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->m:Z

    .line 539543
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 539544
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    .line 539545
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->m:Z

    if-eqz v0, :cond_0

    .line 539546
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->requestLayout()V

    .line 539547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->m:Z

    .line 539548
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x62dcd39b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 539549
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539550
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x4c93ac6d

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 539551
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0xcaca60

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 539552
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 539553
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->j:LX/3GZ;

    new-instance v1, LX/3JI;

    invoke-direct {v1, p0, p1, p0}, LX/3JI;-><init>(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;Landroid/view/View$OnClickListener;Lcom/facebook/feedplugins/video/RichVideoAttachmentView;)V

    .line 539554
    iput-object v1, v0, LX/3GZ;->o:Landroid/view/View$OnClickListener;

    .line 539555
    return-void
.end method

.method public setShowLiveCommentDialogFragment(Z)V
    .locals 0

    .prologue
    .line 539556
    iput-boolean p1, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->i:Z

    .line 539557
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 539558
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->x()V

    .line 539559
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 539562
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->y()V

    .line 539563
    return-void
.end method
