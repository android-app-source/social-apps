.class public Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private static s:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2my;

.field private final f:LX/2nI;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7k;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7i;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2mx;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D81;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D85;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D8o;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2nF;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7m;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2mk;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D8B;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D8C;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Or;
    .annotation runtime Lcom/facebook/video/abtest/VideoInline;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460966
    new-instance v0, LX/2mh;

    invoke-direct {v0}, LX/2mh;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;LX/2mx;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2my;LX/2nI;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 1
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            "LX/2mx;",
            "LX/0Ot",
            "<",
            "LX/2nF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D7m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D81;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D85;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D8B;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D8C;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2mk;",
            ">;",
            "LX/2my;",
            "LX/2nI;",
            "LX/0Ot",
            "<",
            "LX/D7k;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D7i;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D8o;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460967
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460968
    iput-object p1, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    .line 460969
    iput-object p2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->c:LX/0Ot;

    .line 460970
    iput-object p3, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 460971
    iput-object p4, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    .line 460972
    iput-object p5, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->m:LX/0Ot;

    .line 460973
    iput-object p6, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->n:LX/0Ot;

    .line 460974
    iput-object p7, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->j:LX/0Ot;

    .line 460975
    iput-object p8, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->k:LX/0Ot;

    .line 460976
    iput-object p11, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->o:LX/0Ot;

    .line 460977
    iput-object p12, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->e:LX/2my;

    .line 460978
    iput-object p13, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->f:LX/2nI;

    .line 460979
    iput-object p14, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->g:LX/0Ot;

    .line 460980
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->h:LX/0Ot;

    .line 460981
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->l:LX/0Ot;

    .line 460982
    iput-object p9, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->p:LX/0Ot;

    .line 460983
    iput-object p10, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->q:LX/0Ot;

    .line 460984
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->r:LX/0Or;

    .line 460985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 460956
    const-class v1, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    monitor-enter v1

    .line 460957
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460958
    sput-object v2, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;
    .locals 20

    .prologue
    .line 460964
    new-instance v2, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    const/16 v4, 0x1317

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    const-class v6, LX/2mx;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2mx;

    const/16 v7, 0x138b

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x384f

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3856

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3857

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3858

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3859

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1392

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2my;->a(LX/0QB;)LX/2my;

    move-result-object v14

    check-cast v14, LX/2my;

    invoke-static/range {p0 .. p0}, LX/2nI;->a(LX/0QB;)LX/2nI;

    move-result-object v15

    check-cast v15, LX/2nI;

    const/16 v16, 0x384e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x384d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x385f

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x382

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;LX/2mx;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2my;LX/2nI;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 460965
    return-object v2
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460955
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 460905
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460906
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460907
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 460908
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v2, LX/3EE;

    const/4 v3, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    invoke-direct {v2, p2, v3, v4, v1}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460909
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mk;

    invoke-virtual {v0, p2}, LX/2mk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460910
    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mz;

    invoke-virtual {v2, v0}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460911
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 460912
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D8C;

    .line 460913
    iget-object v2, v0, LX/D8C;->a:LX/0ad;

    sget-short v3, LX/D8D;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 460914
    if-eqz v2, :cond_9

    const/4 v3, 0x0

    .line 460915
    invoke-static {p2}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v3

    .line 460916
    :goto_1
    move v2, v2

    .line 460917
    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 460918
    if-eqz v0, :cond_2

    .line 460919
    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mz;

    invoke-virtual {v2, v0}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 460920
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D7m;

    const/4 v4, 0x0

    .line 460921
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460922
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 460923
    invoke-static {p2}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, LX/D7m;->a:LX/0Uh;

    const/16 p3, 0x588

    invoke-virtual {v3, p3, v4}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_3
    move v2, v4

    .line 460924
    :goto_3
    move v0, v2

    .line 460925
    if-eqz v0, :cond_4

    .line 460926
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->f:LX/2nI;

    invoke-virtual {v0, v2}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 460927
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/2nF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 460928
    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mz;

    invoke-virtual {v2, v0}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 460929
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nF;

    const/4 v3, 0x0

    .line 460930
    invoke-static {p2}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_e

    move v2, v3

    .line 460931
    :goto_4
    move v0, v2

    .line 460932
    if-eqz v0, :cond_6

    .line 460933
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->e:LX/2my;

    invoke-virtual {v0, v2}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 460934
    :cond_6
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nF;

    invoke-virtual {v0, p2}, LX/2nF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 460935
    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mz;

    invoke-virtual {v2, v0}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 460936
    :cond_7
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D81;

    const/4 v3, 0x0

    .line 460937
    invoke-static {p2}, LX/2nH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v0, LX/D81;->b:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v0, LX/D81;->b:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->c()Z

    move-result v2

    if-nez v2, :cond_10

    :cond_8
    move v2, v3

    .line 460938
    :goto_5
    move v0, v2

    .line 460939
    if-eqz v0, :cond_0

    .line 460940
    iget-object v2, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->i:LX/2mx;

    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mz;

    invoke-virtual {v2, v0}, LX/2mx;->a(LX/2mz;)Lcom/facebook/video/watchandmore/WatchAndMoreLauncherPartDefinition;

    move-result-object v0

    new-instance v2, LX/D8O;

    invoke-direct {v2, p2, v1}, LX/D8O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 460941
    :cond_a
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460942
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 460943
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v4, :cond_b

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_b
    move v2, v3

    goto/16 :goto_1

    .line 460944
    :cond_c
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 460945
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, p3, v2}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v2

    .line 460946
    if-eqz v2, :cond_d

    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_d
    move v2, v4

    goto/16 :goto_3

    .line 460947
    :cond_e
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460948
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v4, -0x1e53800c

    invoke-static {v2, v4}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 460949
    invoke-static {v2}, LX/2nF;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v4

    if-nez v4, :cond_f

    invoke-static {v0, v2}, LX/2nF;->a(LX/2nF;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    goto/16 :goto_4

    :cond_f
    move v2, v3

    goto/16 :goto_4

    .line 460950
    :cond_10
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-gt v2, v4, :cond_11

    move v2, v3

    .line 460951
    goto :goto_5

    .line 460952
    :cond_11
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460953
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v4, 0x46a1c4a4

    invoke-static {v2, v4}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 460954
    if-eqz v2, :cond_12

    const/4 v2, 0x1

    goto/16 :goto_5

    :cond_12
    move v2, v3

    goto/16 :goto_5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 460898
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 460899
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460900
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 460901
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37Y;

    invoke-virtual {v1}, LX/37Y;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/37Y;

    invoke-virtual {v1}, LX/37Y;->g()LX/38p;

    move-result-object v1

    invoke-virtual {v1}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v3

    .line 460902
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/RichVideoDirectResponseAttachmentPartDefinition;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v4}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, LX/2v7;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/2v7;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-nez v2, :cond_2

    :goto_1
    return v3

    :cond_1
    move v2, v4

    .line 460903
    goto :goto_0

    :cond_2
    move v3, v4

    .line 460904
    goto :goto_1
.end method
