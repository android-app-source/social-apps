.class public Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2G;",
        "LX/CD4;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1C2;

.field public final b:LX/1Bv;

.field private final c:LX/23s;

.field public d:LX/093;

.field public e:Lcom/facebook/video/engine/VideoPlayerParams;


# direct methods
.method public constructor <init>(LX/1C2;LX/1Bv;LX/23s;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460813
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 460814
    iput-object p1, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->a:LX/1C2;

    .line 460815
    iput-object p2, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->b:LX/1Bv;

    .line 460816
    new-instance v0, LX/093;

    invoke-direct {v0}, LX/093;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->d:LX/093;

    .line 460817
    iput-object p3, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->c:LX/23s;

    .line 460818
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;
    .locals 6

    .prologue
    .line 460819
    const-class v1, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    monitor-enter v1

    .line 460820
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460821
    sput-object v2, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460822
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460823
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460824
    new-instance p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v3

    check-cast v3, LX/1C2;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v4

    check-cast v4, LX/1Bv;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v5

    check-cast v5, LX/23s;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;-><init>(LX/1C2;LX/1Bv;LX/23s;)V

    .line 460825
    move-object v0, p0

    .line 460826
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460827
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460828
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 460830
    check-cast p2, LX/C2G;

    check-cast p3, LX/1Po;

    .line 460831
    iget-object v1, p2, LX/C2G;->a:LX/C2F;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v2

    move-object v0, p3

    check-cast v0, LX/1Pt;

    invoke-virtual {v1, v2, v0}, LX/C2F;->a(LX/04D;LX/1Pt;)V

    .line 460832
    iget-object v0, p2, LX/C2G;->b:LX/3Im;

    invoke-virtual {v0}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->e:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 460833
    iget-object v0, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->c:LX/23s;

    iget-object v1, p2, LX/C2G;->a:LX/C2F;

    .line 460834
    iget-object v2, v1, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 460835
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v0

    .line 460836
    new-instance v1, LX/CD4;

    iget-object v2, p2, LX/C2G;->a:LX/C2F;

    invoke-direct {v1, p0, v2, v0}, LX/CD4;-><init>(Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;LX/C2F;LX/04H;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x328e12f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460837
    check-cast p2, LX/CD4;

    check-cast p3, LX/1Po;

    .line 460838
    iput-object p4, p2, LX/CD4;->e:Landroid/view/View;

    .line 460839
    check-cast p3, LX/1PV;

    invoke-interface {p3, p2}, LX/1PV;->a(LX/5Oj;)V

    .line 460840
    iget-object v1, p0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->d:LX/093;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/093;->a(Z)V

    .line 460841
    const/16 v1, 0x1f

    const v2, -0x7f99627c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 460842
    check-cast p2, LX/CD4;

    check-cast p3, LX/1Po;

    .line 460843
    check-cast p3, LX/1PV;

    invoke-interface {p3, p2}, LX/1PV;->b(LX/5Oj;)V

    .line 460844
    const/4 v0, 0x0

    .line 460845
    iput-object v0, p2, LX/CD4;->e:Landroid/view/View;

    .line 460846
    return-void
.end method
