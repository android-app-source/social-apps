.class public Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/33s;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation runtime Lcom/facebook/video/abtest/VideoInline;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 365209
    new-instance v0, LX/23n;

    invoke-direct {v0}, LX/23n;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;LX/0Ot;Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            ">;",
            "Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365210
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 365211
    iput-object p1, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    .line 365212
    iput-object p2, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->c:LX/0Ot;

    .line 365213
    iput-object p3, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    .line 365214
    iput-object p4, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->e:LX/0Or;

    .line 365215
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;
    .locals 7

    .prologue
    .line 365216
    const-class v1, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    monitor-enter v1

    .line 365217
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365218
    sput-object v2, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365221
    new-instance v5, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    const/16 v4, 0xa59

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    const/16 p0, 0x382

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v6, v4, p0}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;-><init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;LX/0Ot;Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/0Or;)V

    .line 365222
    move-object v0, v5

    .line 365223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365227
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 365228
    check-cast p2, LX/33s;

    .line 365229
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->b:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    iget-object v1, p2, LX/33s;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 365230
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 365231
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3EE;

    iget-object v3, p2, LX/33s;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 365232
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    new-instance v2, LX/3FV;

    iget-object v3, p2, LX/33s;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/33s;->b:LX/04D;

    invoke-direct {v2, v3, v1, v4}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 365233
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/33s;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 365234
    iget-object v0, p0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/33s;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365235
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 365236
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 365237
    check-cast p1, LX/33s;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/33s;)Z

    move-result v0

    return v0
.end method
