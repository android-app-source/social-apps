.class public Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1et;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23o;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1WM;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1WM;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1et;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/23o;",
            ">;",
            "LX/1WM;",
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1xC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463810
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 463811
    iput-object p2, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->d:LX/0Ot;

    .line 463812
    iput-object p3, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->e:LX/0Ot;

    .line 463813
    iput-object p4, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->f:LX/1WM;

    .line 463814
    iput-object p5, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->g:LX/0Ot;

    .line 463815
    iput-object p6, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->h:LX/0Ot;

    .line 463816
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 463803
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463804
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 463805
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1et;

    move-object v1, p3

    check-cast v1, LX/1Po;

    invoke-virtual {v0, p2, v1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1f6;

    move-result-object v4

    .line 463806
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1V0;

    move-object v1, p3

    check-cast v1, LX/1Ps;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/23o;

    invoke-virtual {v2, p2}, LX/23o;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v5

    iget-object v2, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1xC;

    invoke-virtual {v2, p1}, LX/1xC;->c(LX/1De;)LX/C0p;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/C0p;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/C0p;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/C0p;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0p;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/C0p;->a(LX/1Pb;)LX/C0p;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/C0p;->a(LX/1f6;)LX/C0p;

    move-result-object v2

    const/4 v3, 0x1

    .line 463807
    iget-object v4, v2, LX/C0p;->a:LX/C0q;

    iput-boolean v3, v4, LX/C0q;->c:Z

    .line 463808
    move-object v2, v2

    .line 463809
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v5, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;
    .locals 10

    .prologue
    .line 463791
    const-class v1, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    monitor-enter v1

    .line 463792
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 463793
    sput-object v2, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 463794
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463795
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 463796
    new-instance v3, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x82d

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa4f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v7

    check-cast v7, LX/1WM;

    const/16 v8, 0x6fd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x821

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1WM;LX/0Ot;LX/0Ot;)V

    .line 463797
    move-object v0, v3

    .line 463798
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 463799
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463800
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 463801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 463802
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 463790
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 463784
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463785
    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/ObjectionableContentRichVideoPlayerComponentPartDefinition;->f:LX/1WM;

    .line 463786
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463787
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 463788
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463789
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
