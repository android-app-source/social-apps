.class public Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static n:LX/0Xm;


# instance fields
.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/361;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDQ;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1V0;

.field private final h:LX/23o;

.field private final i:LX/0Or;
    .annotation runtime Lcom/facebook/video/abtest/VideoInline;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0ad;

.field private k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 463817
    new-instance v0, LX/2nJ;

    invoke-direct {v0}, LX/2nJ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Or;LX/0ad;LX/1V0;LX/23o;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/361;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CDQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0ad;",
            "LX/1V0;",
            "LX/23o;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463818
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 463819
    iput-object p2, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->e:LX/0Ot;

    .line 463820
    iput-object p3, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->f:LX/0Ot;

    .line 463821
    iput-object p6, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->g:LX/1V0;

    .line 463822
    iput-object p7, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->h:LX/23o;

    .line 463823
    iput-object p4, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->i:LX/0Or;

    .line 463824
    iput-object p5, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->j:LX/0ad;

    .line 463825
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 463826
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 463827
    new-instance v2, LX/3EE;

    const/4 v1, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-direct {v2, p2, v1, v3, v0}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 463828
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463829
    iget-object v3, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->g:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->h:LX/23o;

    invoke-virtual {v1, p2}, LX/23o;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CDQ;

    invoke-virtual {v1, p1}, LX/CDQ;->c(LX/1De;)LX/CDO;

    move-result-object v1

    iget-object v5, v2, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v5}, LX/CDO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CDO;

    move-result-object v1

    iget-object v2, v2, LX/3EE;->e:LX/04D;

    invoke-virtual {v1, v2}, LX/CDO;->a(LX/04D;)LX/CDO;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/CDO;->a(LX/1Pe;)LX/CDO;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v3, p1, v0, v4, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 463830
    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->g:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->h:LX/23o;

    invoke-virtual {v1, p2}, LX/23o;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/361;

    invoke-virtual {v1, p1}, LX/361;->c(LX/1De;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/CDK;->a(LX/3EE;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/CDK;->a(LX/1Pe;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v3, p1, v0, v4, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;
    .locals 11

    .prologue
    .line 463831
    const-class v1, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    monitor-enter v1

    .line 463832
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 463833
    sput-object v2, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 463834
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463835
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 463836
    new-instance v3, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0xa56

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x21ec

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x382

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v9

    check-cast v9, LX/1V0;

    invoke-static {v0}, LX/23o;->a(LX/0QB;)LX/23o;

    move-result-object v10

    check-cast v10, LX/23o;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Or;LX/0ad;LX/1V0;LX/23o;)V

    .line 463837
    move-object v0, v3

    .line 463838
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 463839
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463840
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 463841
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private i()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 463842
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 463843
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->j:LX/0ad;

    sget-short v1, LX/1Dd;->B:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->m:Ljava/lang/Boolean;

    .line 463844
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 463845
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 463846
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 463847
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 463848
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463849
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463850
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 463851
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 463852
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->j:LX/0ad;

    sget-short v2, LX/1Dd;->A:S

    const/4 p1, 0x0

    invoke-interface {v0, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->l:Ljava/lang/Boolean;

    .line 463853
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 463854
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 463855
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 463856
    sget-object v0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 463857
    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->k:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 463858
    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->j:LX/0ad;

    sget-short v2, LX/1Dd;->C:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->k:Ljava/lang/Boolean;

    .line 463859
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/components/RichVideoPlayerComponentPartDefinition;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-super {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public final iV_()Z
    .locals 1

    .prologue
    .line 463860
    const/4 v0, 0x0

    return v0
.end method
