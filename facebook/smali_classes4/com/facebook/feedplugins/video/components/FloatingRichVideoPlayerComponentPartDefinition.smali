.class public Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final d:LX/2nK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2nK",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;

.field private final f:LX/23o;

.field private final g:LX/0Or;
    .annotation runtime Lcom/facebook/video/abtest/VideoInline;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;

.field public i:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2nK;LX/0Or;LX/0ad;LX/1V0;LX/23o;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2nK;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0ad;",
            "LX/1V0;",
            "LX/23o;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463863
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 463864
    iput-object p2, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->d:LX/2nK;

    .line 463865
    iput-object p3, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->g:LX/0Or;

    .line 463866
    iput-object p4, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->h:LX/0ad;

    .line 463867
    iput-object p5, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->e:LX/1V0;

    .line 463868
    iput-object p6, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->f:LX/23o;

    .line 463869
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 463870
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 463871
    new-instance v1, LX/3EE;

    const/4 v2, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-direct {v1, p2, v2, v3, v0}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 463872
    invoke-static {p1}, LX/CD8;->c(LX/1De;)LX/CD6;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->e:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    iget-object v4, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->f:LX/23o;

    invoke-virtual {v4, p2}, LX/23o;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->d:LX/2nK;

    const/4 v6, 0x0

    .line 463873
    new-instance v7, LX/CDH;

    invoke-direct {v7, v5}, LX/CDH;-><init>(LX/2nK;)V

    .line 463874
    iget-object p2, v5, LX/2nK;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/CDG;

    .line 463875
    if-nez p2, :cond_0

    .line 463876
    new-instance p2, LX/CDG;

    invoke-direct {p2, v5}, LX/CDG;-><init>(LX/2nK;)V

    .line 463877
    :cond_0
    invoke-static {p2, p1, v6, v6, v7}, LX/CDG;->a$redex0(LX/CDG;LX/1De;IILX/CDH;)V

    .line 463878
    move-object v7, p2

    .line 463879
    move-object v6, v7

    .line 463880
    move-object v5, v6

    .line 463881
    iget-object v6, v5, LX/CDG;->a:LX/CDH;

    iput-object v1, v6, LX/CDH;->a:LX/3EE;

    .line 463882
    iget-object v6, v5, LX/CDG;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 463883
    move-object v5, v5

    .line 463884
    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-virtual {p0, v1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 463885
    iget-object v6, v5, LX/CDG;->a:LX/CDH;

    iput-object v1, v6, LX/CDH;->c:Ljava/lang/Boolean;

    .line 463886
    iget-object v6, v5, LX/CDG;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 463887
    move-object v1, v5

    .line 463888
    invoke-direct {p0}, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->h()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 463889
    iget-object v6, v1, LX/CDG;->a:LX/CDH;

    iput-object v5, v6, LX/CDH;->d:Ljava/lang/Boolean;

    .line 463890
    iget-object v6, v1, LX/CDG;->e:Ljava/util/BitSet;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 463891
    move-object v1, v1

    .line 463892
    iget-object v5, v1, LX/CDG;->a:LX/CDH;

    iput-object p3, v5, LX/CDH;->b:LX/1Pe;

    .line 463893
    iget-object v5, v1, LX/CDG;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 463894
    move-object v1, v1

    .line 463895
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v3, p1, v0, v4, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/CD6;->a(LX/1X1;)LX/CD6;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;
    .locals 10

    .prologue
    .line 463896
    const-class v1, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    monitor-enter v1

    .line 463897
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 463898
    sput-object v2, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 463899
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463900
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 463901
    new-instance v3, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2nK;->a(LX/0QB;)LX/2nK;

    move-result-object v5

    check-cast v5, LX/2nK;

    const/16 v6, 0x382

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-static {v0}, LX/23o;->a(LX/0QB;)LX/23o;

    move-result-object v9

    check-cast v9, LX/23o;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;-><init>(Landroid/content/Context;LX/2nK;LX/0Or;LX/0ad;LX/1V0;LX/23o;)V

    .line 463902
    move-object v0, v3

    .line 463903
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 463904
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463905
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 463906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 463907
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->j:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 463908
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->h:LX/0ad;

    sget-short v1, LX/1Dd;->C:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->j:Ljava/lang/Boolean;

    .line 463909
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 463910
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 463911
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 463912
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 463913
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463914
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463915
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 463916
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 463917
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->h:LX/0ad;

    sget-short v2, LX/1Dd;->z:S

    const/4 p1, 0x0

    invoke-interface {v0, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->i:Ljava/lang/Boolean;

    .line 463918
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/components/FloatingRichVideoPlayerComponentPartDefinition;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 463919
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 463920
    const/4 v0, 0x0

    return-object v0
.end method
