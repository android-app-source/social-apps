.class public Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2G;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/2mX;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(LX/2mX;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460847
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 460848
    iput-object p1, p0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->a:LX/2mX;

    .line 460849
    iput-object p2, p0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460850
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;
    .locals 5

    .prologue
    .line 460851
    const-class v1, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    monitor-enter v1

    .line 460852
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460853
    sput-object v2, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460856
    new-instance p0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    const-class v3, LX/2mX;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mX;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;-><init>(LX/2mX;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 460857
    move-object v0, p0

    .line 460858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 460862
    check-cast p2, LX/C2G;

    check-cast p3, LX/1Pt;

    .line 460863
    iget-object v1, p2, LX/C2G;->a:LX/C2F;

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v0

    invoke-virtual {v1, v0, p3}, LX/C2F;->a(LX/04D;LX/1Pt;)V

    .line 460864
    iget-object v0, p0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->a:LX/2mX;

    iget-object v1, p2, LX/C2G;->a:LX/C2F;

    invoke-virtual {v0, v1}, LX/2mX;->a(LX/C2F;)LX/CD3;

    move-result-object v0

    .line 460865
    iget-object v1, p0, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/CD5;

    invoke-direct {v2, p0, v0}, LX/CD5;-><init>(Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;LX/CD3;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460866
    const/4 v0, 0x0

    return-object v0
.end method
