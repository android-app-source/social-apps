.class public final Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x736f878b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 525663
    const-class v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 525664
    const-class v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 525621
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 525622
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 525651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 525652
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 525653
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 525654
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 525655
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 525656
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 525657
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 525658
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 525659
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 525660
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 525661
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 525662
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 525633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 525634
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525635
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525636
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 525637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    .line 525638
    iput-object v0, v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525639
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 525640
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525641
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 525642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    .line 525643
    iput-object v0, v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525644
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 525645
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525646
    invoke-virtual {p0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 525647
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    .line 525648
    iput-object v0, v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525649
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 525650
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525631
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525632
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 525665
    new-instance v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;-><init>()V

    .line 525666
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 525667
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 525630
    const v0, -0x320214c3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 525629
    const v0, -0x3874fcf0    # -71174.125f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525627
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->f:Ljava/lang/String;

    .line 525628
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525625
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525626
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525623
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 525624
    iget-object v0, p0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method
