.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535844
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535845
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    .line 535846
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;

    .line 535847
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;
    .locals 5

    .prologue
    .line 535848
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    monitor-enter v1

    .line 535849
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535850
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535851
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535852
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535853
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;)V

    .line 535854
    move-object v0, p0

    .line 535855
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535856
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535857
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 535859
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535860
    const v0, 0x7f0d150c

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    new-instance v2, LX/Anm;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v3}, LX/Anm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535861
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageUFIPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535862
    const/4 v0, 0x0

    return-object v0
.end method
