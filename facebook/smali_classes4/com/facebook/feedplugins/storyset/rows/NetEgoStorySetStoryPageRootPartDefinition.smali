.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGK;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/DHl;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536041
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 536042
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    .line 536043
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    .line 536044
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    .line 536045
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 536046
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;
    .locals 7

    .prologue
    .line 536047
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    monitor-enter v1

    .line 536048
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536049
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536050
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536051
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536052
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 536053
    move-object v0, p0

    .line 536054
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536055
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536056
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536057
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 536058
    check-cast p2, LX/DGK;

    .line 536059
    iget-object v0, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 536060
    new-instance v1, LX/DGK;

    .line 536061
    iget-object v2, p2, LX/DGK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 536062
    iget v3, p2, LX/DGK;->c:I

    move v3, v3

    .line 536063
    iget v4, p2, LX/DGK;->d:I

    move v4, v4

    .line 536064
    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/DGK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    .line 536065
    const v2, 0x7f0d1d87

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536066
    const v2, 0x7f0d1d8a

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536067
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    .line 536068
    iget-object v2, v1, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 536069
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536070
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->h()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536071
    const/4 v0, 0x0

    return-object v0
.end method
