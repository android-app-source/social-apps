.class public Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/EmbeddedComponentPartDefinition",
        "<",
        "LX/DGK;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/3Dm;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3Dm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535773
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 535774
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->b:LX/3Dm;

    .line 535775
    return-void
.end method

.method private a(LX/1De;LX/DGK;LX/1Pd;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/DGK;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 535778
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->b:LX/3Dm;

    invoke-virtual {v0, p1}, LX/3Dm;->c(LX/1De;)LX/DH2;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/DH2;->a(LX/1Pd;)LX/DH2;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/DH2;->a(LX/DGK;)LX/DH2;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 535779
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    monitor-enter v1

    .line 535780
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535781
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535782
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535783
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535784
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3Dm;->a(LX/0QB;)LX/3Dm;

    move-result-object v4

    check-cast v4, LX/3Dm;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/3Dm;)V

    .line 535785
    move-object v0, p0

    .line 535786
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535787
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535788
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 535777
    check-cast p2, LX/DGK;

    check-cast p3, LX/1Pd;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->a(LX/1De;LX/DGK;LX/1Pd;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 535776
    check-cast p2, LX/DGK;

    check-cast p3, LX/1Pd;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->a(LX/1De;LX/DGK;LX/1Pd;)LX/1X1;

    move-result-object v0

    return-object v0
.end method
