.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536097
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 536098
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    .line 536099
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    .line 536100
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;
    .locals 5

    .prologue
    .line 536105
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    monitor-enter v1

    .line 536106
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536107
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536108
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536109
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536110
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;)V

    .line 536111
    move-object v0, p0

    .line 536112
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536113
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536114
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536115
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 536101
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 536102
    const v0, 0x7f0d2de9

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536103
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536104
    const/4 v0, 0x0

    return-object v0
.end method
