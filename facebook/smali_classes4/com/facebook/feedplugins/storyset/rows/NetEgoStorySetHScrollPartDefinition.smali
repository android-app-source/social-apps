.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:LX/1LV;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public final e:LX/1DR;

.field public final f:LX/2xr;

.field public final g:LX/1K9;

.field private final h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

.field public final j:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

.field public final k:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1DR;LX/1LV;LX/2xr;LX/1K9;LX/0Or;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsHScrollVideoAutoPlayEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/1DR;",
            "LX/1LV;",
            "LX/2xr;",
            "LX/1K9;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535628
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 535629
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->d:Landroid/content/Context;

    .line 535630
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 535631
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->b:LX/1LV;

    .line 535632
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->e:LX/1DR;

    .line 535633
    iput-object p8, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 535634
    iput-object p9, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->i:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    .line 535635
    iput-object p10, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->j:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    .line 535636
    iput-object p11, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->k:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;

    .line 535637
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->c:LX/0Or;

    .line 535638
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->f:LX/2xr;

    .line 535639
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->g:LX/1K9;

    .line 535640
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;
    .locals 15

    .prologue
    .line 535660
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    monitor-enter v1

    .line 535661
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535662
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535665
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v6

    check-cast v6, LX/1DR;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, LX/2xr;->b(LX/0QB;)LX/2xr;

    move-result-object v8

    check-cast v8, LX/2xr;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v9

    check-cast v9, LX/1K9;

    const/16 v10, 0x315

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1DR;LX/1LV;LX/2xr;LX/1K9;LX/0Or;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryPageRootPartDefinition;)V

    .line 535666
    move-object v0, v3

    .line 535667
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535668
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535669
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535659
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 535642
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535643
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 535644
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->g:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    move-object v1, v1

    .line 535645
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535646
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 535647
    check-cast v5, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 535648
    iget-object v6, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    .line 535649
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->e:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    .line 535650
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->d:Landroid/content/Context;

    int-to-float v1, v1

    invoke-static {v2, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 535651
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->e:LX/1DR;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->d:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2, v1}, LX/2eF;->a(FI)LX/2eF;

    move-result-object v1

    move-object v1, v1

    .line 535652
    invoke-interface {v5}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v2

    .line 535653
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 535654
    check-cast v3, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v3}, LX/25C;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v3

    .line 535655
    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->e:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    .line 535656
    new-instance p3, LX/DGH;

    invoke-direct {p3, p0, v3, p2, v4}, LX/DGH;-><init>(Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    move-object v3, p3

    .line 535657
    invoke-interface {v5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535658
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 535641
    const/4 v0, 0x1

    return v0
.end method
