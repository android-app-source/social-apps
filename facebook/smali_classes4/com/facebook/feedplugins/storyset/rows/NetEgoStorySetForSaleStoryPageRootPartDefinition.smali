.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGK;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/DHl;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

.field private final e:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535751
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535752
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    .line 535753
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    .line 535754
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 535755
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    .line 535756
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->e:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    .line 535757
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;
    .locals 9

    .prologue
    .line 535740
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    monitor-enter v1

    .line 535741
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535742
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535743
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535744
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535745
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;)V

    .line 535746
    move-object v0, v3

    .line 535747
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535748
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535749
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535750
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 535758
    check-cast p2, LX/DGK;

    .line 535759
    new-instance v0, LX/DGK;

    .line 535760
    iget-object v1, p2, LX/DGK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 535761
    iget v2, p2, LX/DGK;->c:I

    move v2, v2

    .line 535762
    iget v3, p2, LX/DGK;->d:I

    move v3, v3

    .line 535763
    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LX/DGK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    .line 535764
    const v1, 0x7f0d1d87

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/components/NetEgoStorySetPageHeaderComponentPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535765
    iget-object v1, v0, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 535766
    const v2, 0x7f0d1d88

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535767
    const v2, 0x7f0d1d89

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->e:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535768
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    .line 535769
    iget-object v2, v0, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 535770
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535771
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->h()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535772
    const/4 v0, 0x0

    return-object v0
.end method
