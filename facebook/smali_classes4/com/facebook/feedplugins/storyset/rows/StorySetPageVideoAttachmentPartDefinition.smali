.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/2mY;

.field private final e:LX/2mZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 536164
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2mY;LX/2mZ;Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536165
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 536166
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    .line 536167
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    .line 536168
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->d:LX/2mY;

    .line 536169
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->e:LX/2mZ;

    .line 536170
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 536171
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 536172
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536173
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536174
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536175
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536176
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    const-class v3, LX/2mY;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mY;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;-><init>(LX/2mY;LX/2mZ;Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;)V

    .line 536177
    move-object v0, p0

    .line 536178
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536179
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536180
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 536182
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 536183
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->d:LX/2mY;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v1, v2}, LX/2mY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0am;Lcom/facebook/common/callercontext/CallerContext;)LX/C2F;

    move-result-object v0

    .line 536184
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->e:LX/2mZ;

    .line 536185
    iget-object v2, v0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v2, v2

    .line 536186
    invoke-virtual {v1, p2, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    .line 536187
    new-instance v2, LX/C2G;

    invoke-direct {v2, v0, v1}, LX/C2G;-><init>(LX/C2F;LX/3Im;)V

    .line 536188
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536189
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/PlayFullscreenVideoPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536190
    const/4 v0, 0x0

    return-object v0
.end method
