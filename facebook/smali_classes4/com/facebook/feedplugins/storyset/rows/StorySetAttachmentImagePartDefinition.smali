.class public Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3Ds;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/view/GenericDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition",
            "<",
            "Lcom/facebook/drawee/view/GenericDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Ad;

.field public final d:Landroid/content/res/Resources;

.field public final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 535733
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    const-string v1, "newsfeed_storyset_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/1Ad;LX/03V;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535734
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535735
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->b:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    .line 535736
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->c:LX/1Ad;

    .line 535737
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->d:Landroid/content/res/Resources;

    .line 535738
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->e:LX/03V;

    .line 535739
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;
    .locals 7

    .prologue
    .line 535722
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    monitor-enter v1

    .line 535723
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535724
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535725
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535726
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535727
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;-><init>(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/1Ad;LX/03V;Landroid/content/Context;)V

    .line 535728
    move-object v0, p0

    .line 535729
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535730
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535731
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535732
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 535718
    check-cast p2, LX/3Ds;

    .line 535719
    new-instance v0, LX/DGW;

    invoke-direct {v0, p0, p2}, LX/DGW;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;LX/3Ds;)V

    .line 535720
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->b:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    new-instance v2, LX/AmL;

    sget-object v3, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v3, v0, v4, v5}, LX/AmL;-><init>(Lcom/facebook/common/callercontext/CallerContext;LX/AmK;ZZ)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535721
    const/4 v0, 0x0

    return-object v0
.end method
