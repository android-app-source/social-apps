.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536116
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 536117
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 536118
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->b:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    .line 536119
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;
    .locals 5

    .prologue
    .line 536120
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    monitor-enter v1

    .line 536121
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536122
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536125
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;)V

    .line 536126
    move-object v0, p0

    .line 536127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 536131
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 536132
    const v0, 0x7f0d2dea

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v2, LX/20d;

    sget-object v3, LX/1Wi;->PAGE:LX/1Wi;

    invoke-direct {v2, p2, v3}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536133
    const v0, 0x7f0d2dea

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsUFIPartDefinition;->b:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    new-instance v2, LX/20y;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v3}, LX/20y;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536134
    const/4 v0, 0x0

    return-object v0
.end method
