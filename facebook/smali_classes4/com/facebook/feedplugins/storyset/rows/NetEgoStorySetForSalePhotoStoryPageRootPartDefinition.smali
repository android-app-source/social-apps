.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGK;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/DHl;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/3Dl;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

.field private final e:LX/3Do;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 535671
    new-instance v0, LX/3Dl;

    invoke-direct {v0}, LX/3Dl;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->a:LX/3Dl;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;LX/3Do;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535672
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 535673
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    .line 535674
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 535675
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    .line 535676
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->e:LX/3Do;

    .line 535677
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;
    .locals 7

    .prologue
    .line 535678
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    monitor-enter v1

    .line 535679
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535680
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535681
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535682
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535683
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    const-class v6, LX/3Do;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/3Do;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;LX/3Do;)V

    .line 535684
    move-object v0, p0

    .line 535685
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535686
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535687
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535689
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->a:LX/3Dl;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 535690
    check-cast p2, LX/DGK;

    .line 535691
    iget-object v0, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 535692
    invoke-static {v2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 535693
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535694
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535695
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 535696
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 535697
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    .line 535698
    if-nez v6, :cond_2

    move-object v1, v3

    .line 535699
    :cond_0
    :goto_0
    move-object v0, v1

    .line 535700
    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v0, v0

    .line 535701
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSaleStoryPageRootPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535702
    const v1, 0x7f0d2de6

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    new-instance v4, LX/3Ds;

    .line 535703
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 535704
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v4, v0}, LX/3Ds;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535705
    const v0, 0x7f0d2de6

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetForSalePhotoStoryPageRootPartDefinition;->e:LX/3Do;

    invoke-virtual {v3, v2}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 535706
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 535707
    :cond_2
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v5

    :goto_2
    if-ge v4, v7, :cond_5

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 535708
    if-eqz v1, :cond_4

    .line 535709
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    if-nez p3, :cond_0

    .line 535710
    :cond_3
    sget-object p3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, p3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 535711
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object p3

    .line 535712
    if-eqz p3, :cond_4

    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 535713
    invoke-virtual {p3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto/16 :goto_0

    .line 535714
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_5
    move-object v1, v3

    .line 535715
    goto/16 :goto_0
.end method
