.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGK;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/DHl;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/3Dq;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 536012
    new-instance v0, LX/3Dq;

    invoke-direct {v0}, LX/3Dq;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->a:LX/3Dq;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536023
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 536024
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    .line 536025
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 536026
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    .line 536027
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;
    .locals 6

    .prologue
    .line 536028
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    monitor-enter v1

    .line 536029
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536030
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536031
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536032
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536033
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;)V

    .line 536034
    move-object v0, p0

    .line 536035
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536036
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536037
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536022
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->a:LX/3Dq;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 536013
    check-cast p2, LX/DGK;

    .line 536014
    iget-object v0, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 536015
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 536016
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetStoryPageRootPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536017
    const v2, 0x7f0d2de6

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    new-instance v4, LX/3Ds;

    .line 536018
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 536019
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v4, v0}, LX/3Ds;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536020
    const v0, 0x7f0d2de6

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryPageRootPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v3, LX/2ya;

    invoke-direct {v3, v1}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 536021
    const/4 v0, 0x0

    return-object v0
.end method
