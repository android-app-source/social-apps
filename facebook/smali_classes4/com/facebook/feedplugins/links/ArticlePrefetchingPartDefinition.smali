.class public Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "LX/2ym;",
        "LX/1Pn;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 482520
    const-class v0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482480
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 482481
    iput-object p1, p0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->b:LX/0Ot;

    .line 482482
    iput-object p2, p0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->c:LX/0ad;

    .line 482483
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;
    .locals 5

    .prologue
    .line 482509
    const-class v1, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    monitor-enter v1

    .line 482510
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482511
    sput-object v2, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482512
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482513
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 482514
    new-instance v4, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    const/16 v3, 0x2524

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, p0, v3}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;-><init>(LX/0Ot;LX/0ad;)V

    .line 482515
    move-object v0, v4

    .line 482516
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482517
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482518
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;LX/2ym;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 482505
    iget-object v0, p1, LX/2ym;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 482506
    iget-object v0, p0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CH7;

    iget-object v1, p1, LX/2ym;->b:Ljava/lang/String;

    sget-object v2, LX/CH2;->FEED:LX/CH2;

    sget-object v3, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v1, v2, v3}, LX/CH7;->a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;

    move-result-object v0

    .line 482507
    iput-object v0, p1, LX/2ym;->a:LX/CGw;

    .line 482508
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;)Z
    .locals 3

    .prologue
    .line 482504
    iget-object v0, p0, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->c:LX/0ad;

    sget-short v1, LX/0fe;->af:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 482494
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    check-cast p3, LX/1Pn;

    .line 482495
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 482496
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    .line 482497
    if-eqz v0, :cond_1

    .line 482498
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v0

    .line 482499
    :goto_0
    move-object v0, v0

    .line 482500
    new-instance v1, LX/2ym;

    invoke-direct {v1, v0}, LX/2ym;-><init>(Ljava/lang/String;)V

    .line 482501
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482502
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;LX/2ym;Landroid/content/Context;)V

    .line 482503
    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4617ef31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 482490
    check-cast p2, LX/2ym;

    .line 482491
    if-eqz p4, :cond_0

    invoke-static {p0}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 482492
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, p2, v1}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a(Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;LX/2ym;Landroid/content/Context;)V

    .line 482493
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x62be26e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 482484
    check-cast p2, LX/2ym;

    .line 482485
    iget-object p0, p2, LX/2ym;->a:LX/CGw;

    if-eqz p0, :cond_0

    .line 482486
    iget-object p0, p2, LX/2ym;->a:LX/CGw;

    invoke-interface {p0}, LX/CGw;->a()V

    .line 482487
    const/4 p0, 0x0

    .line 482488
    iput-object p0, p2, LX/2ym;->a:LX/CGw;

    .line 482489
    :cond_0
    return-void
.end method
