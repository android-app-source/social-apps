.class public Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2yf;",
        "LX/2yn;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/1Be;

.field public final c:LX/0Uh;

.field public final d:Landroid/os/Handler;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Bf;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2nE;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2n5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 482649
    const-class v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Be;LX/0Uh;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Be;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/1Bf;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/2nE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2n5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482650
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 482651
    iput-object p1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    .line 482652
    iput-object p2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->c:LX/0Uh;

    .line 482653
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    .line 482654
    iget-object p1, v0, LX/1Be;->j:Landroid/os/Handler;

    move-object v0, p1

    .line 482655
    iput-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    .line 482656
    iput-object p3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->e:LX/0Ot;

    .line 482657
    iput-object p4, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    .line 482658
    iput-object p5, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->g:LX/0Ot;

    .line 482659
    iput-object p6, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->h:LX/0Ot;

    .line 482660
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;
    .locals 10

    .prologue
    .line 482620
    const-class v1, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    monitor-enter v1

    .line 482621
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482622
    sput-object v2, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482623
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482624
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 482625
    new-instance v3, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    invoke-static {v0}, LX/1Be;->a(LX/0QB;)LX/1Be;

    move-result-object v4

    check-cast v4, LX/1Be;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0x1eb

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const/16 v8, 0x1f6

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x69b

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;-><init>(LX/1Be;LX/0Uh;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;)V

    .line 482626
    move-object v0, v3

    .line 482627
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482628
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482629
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482630
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 482631
    invoke-static {p1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482632
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, LX/1Be;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482633
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2n5;

    .line 482634
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 482635
    :cond_0
    :goto_0
    return-void

    .line 482636
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2n5;

    .line 482637
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 482638
    :cond_2
    goto :goto_0

    .line 482639
    :cond_3
    iget-object v2, v0, LX/2n5;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 482640
    const-string p0, "ATTACHMENT_LINK"

    const/4 p1, 0x1

    invoke-static {v2, p2, v1, p0, p1}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 482641
    goto :goto_1

    .line 482642
    :cond_4
    iget-object v2, v0, LX/2n5;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 482643
    const/4 p0, 0x2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 482644
    invoke-static {v2, p0, v1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->b(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 482645
    goto :goto_2

    .line 482646
    :cond_5
    iget-object v2, v0, LX/2n5;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 482647
    const-string p0, "ATTACHMENT_LINK"

    const/4 v0, 0x0

    invoke-static {v2, p2, v1, p0, v0}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 482648
    goto :goto_3
.end method

.method public static a(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;)Z
    .locals 1

    .prologue
    .line 482661
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    .line 482662
    iget-object p0, v0, LX/1Be;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Bn;

    invoke-virtual {p0}, LX/1Bn;->e()Z

    move-result p0

    move v0, p0

    .line 482663
    return v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 482521
    check-cast p2, LX/2yf;

    .line 482522
    new-instance v0, LX/2yn;

    invoke-direct {v0}, LX/2yn;-><init>()V

    .line 482523
    const/4 p3, 0x0

    .line 482524
    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short p1, LX/1Bm;->C:S

    invoke-interface {v1, p1, p3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 482525
    :cond_0
    :goto_0
    iget-boolean v1, p2, LX/2yf;->c:Z

    if-eqz v1, :cond_1

    .line 482526
    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482527
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 482528
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_f

    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482529
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 482530
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 482531
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v2}, LX/1Be;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v2}, LX/1Be;->e()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 482532
    :cond_1
    :goto_2
    return-object v0

    .line 482533
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short p1, LX/1Bi;->r:S

    invoke-interface {v1, p1, p3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 482534
    const/4 v1, 0x1

    .line 482535
    iput-boolean v1, v0, LX/2yn;->f:Z

    .line 482536
    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v1}, LX/1Be;->a()V

    goto :goto_0

    .line 482537
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->c:LX/0Uh;

    const/16 p1, 0xce

    invoke-virtual {v1, p1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    sget-object p1, LX/03R;->YES:LX/03R;

    if-ne v1, p1, :cond_0

    .line 482538
    new-instance v1, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$1;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$1;-><init>(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;)V

    .line 482539
    iput-object v1, v0, LX/2yn;->b:Ljava/lang/Runnable;

    .line 482540
    goto :goto_0

    .line 482541
    :cond_4
    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 482542
    if-eqz v2, :cond_1

    .line 482543
    iget-object v3, p2, LX/2yf;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 482544
    invoke-static {v3}, LX/2yo;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 482545
    if-eq v4, v3, :cond_1

    if-eqz v4, :cond_1

    .line 482546
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 482547
    if-eqz v6, :cond_1

    invoke-static {v6}, LX/047;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 482548
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short v4, LX/1Bi;->c:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 482549
    invoke-static {p0}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 482550
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v6, v3}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    .line 482551
    :cond_5
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    .line 482552
    iget-object v4, v3, LX/1Be;->I:Ljava/lang/Boolean;

    if-nez v4, :cond_6

    .line 482553
    iget-object v4, v3, LX/1Be;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1C0;->g:LX/0Tn;

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v3, LX/1Be;->I:Ljava/lang/Boolean;

    .line 482554
    :cond_6
    iget-object v4, v3, LX/1Be;->I:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move v3, v4

    .line 482555
    if-eqz v3, :cond_7

    .line 482556
    const-wide/16 v2, 0x1

    .line 482557
    iput-wide v2, v0, LX/2yn;->c:J

    .line 482558
    const-wide/16 v2, 0x1

    .line 482559
    iput-wide v2, v0, LX/2yn;->d:J

    .line 482560
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->RENDER_BLOCKING:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 482561
    :goto_3
    iget-wide v2, v0, LX/2yn;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_9

    iget-wide v2, v0, LX/2yn;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_9

    .line 482562
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    sget-object v3, LX/1Bu;->NO_VPV:LX/1Bu;

    invoke-virtual {v2, v6, v3}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto/16 :goto_2

    .line 482563
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    .line 482564
    if-nez v2, :cond_8

    .line 482565
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    sget-object v3, LX/1Bu;->NO_CONTEXT:LX/1Bu;

    invoke-virtual {v2, v6, v3}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    goto/16 :goto_2

    .line 482566
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m()I

    move-result v3

    int-to-long v4, v3

    .line 482567
    iput-wide v4, v0, LX/2yn;->c:J

    .line 482568
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l()I

    move-result v3

    int-to-long v4, v3

    .line 482569
    iput-wide v4, v0, LX/2yn;->d:J

    .line 482570
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v7

    goto :goto_3

    .line 482571
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short v3, LX/1Bi;->d:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 482572
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    const/4 v3, 0x1

    invoke-virtual {v2, v6, v3}, LX/1Be;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 482573
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-static {}, LX/1Bu;->getCatchThemAllReasonInPrepare()LX/1Bu;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 482574
    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v8

    .line 482575
    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short v3, LX/1Bm;->t:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_b

    if-nez v8, :cond_b

    .line 482576
    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482577
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 482578
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 482579
    iget-object v2, p2, LX/2yf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482580
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v4

    .line 482581
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    .line 482582
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    :goto_4
    move-object v5, v2

    .line 482583
    :goto_5
    new-instance v2, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;

    move-object v3, p0

    move-object v9, v0

    invoke-direct/range {v2 .. v9}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;-><init>(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;ZLX/2yn;)V

    .line 482584
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v3}, LX/1Be;->f()Z

    move-result v3

    if-eqz v3, :cond_c

    iget-wide v4, v0, LX/2yn;->c:J

    .line 482585
    :goto_6
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short v6, LX/1Bi;->e:S

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 482586
    const-wide/32 v6, 0xd9038

    cmp-long v3, v4, v6

    if-nez v3, :cond_d

    .line 482587
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    const v4, -0x673dc777

    invoke-static {v3, v2, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_2

    .line 482588
    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    .line 482589
    :cond_b
    const/4 v4, 0x0

    .line 482590
    const/4 v5, 0x0

    goto :goto_5

    .line 482591
    :cond_c
    iget-wide v4, v0, LX/2yn;->d:J

    goto :goto_6

    .line 482592
    :cond_d
    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->f:LX/0ad;

    sget-short v6, LX/1Bi;->i:S

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 482593
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 482594
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 482595
    iput-object v3, v0, LX/2yn;->e:Ljava/lang/Long;

    .line 482596
    :cond_e
    iput-object v2, v0, LX/2yn;->a:Ljava/lang/Runnable;

    .line 482597
    goto/16 :goto_2

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x50ff2ff2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 482598
    check-cast p1, LX/2yf;

    check-cast p2, LX/2yn;

    .line 482599
    if-nez p4, :cond_0

    .line 482600
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x4fa11b37

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 482601
    :cond_0
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 482602
    iget-boolean v5, p2, LX/2yn;->f:Z

    if-eqz v5, :cond_2

    .line 482603
    iget-object v5, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v5}, LX/1Be;->a()V

    .line 482604
    :cond_1
    :goto_1
    iget-object v5, p2, LX/2yn;->a:Ljava/lang/Runnable;

    if-nez v5, :cond_3

    .line 482605
    :goto_2
    goto :goto_0

    .line 482606
    :cond_2
    iget-object v5, p2, LX/2yn;->b:Ljava/lang/Runnable;

    if-eqz v5, :cond_1

    .line 482607
    iget-object v5, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    iget-object v6, p2, LX/2yn;->b:Ljava/lang/Runnable;

    const-wide/16 v7, 0x7d0

    const v9, 0x67b46652

    invoke-static {v5, v6, v7, v8, v9}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    .line 482608
    :cond_3
    iget-object v5, p2, LX/2yn;->e:Ljava/lang/Long;

    if-eqz v5, :cond_5

    .line 482609
    iget-object v5, p2, LX/2yn;->e:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 482610
    :cond_4
    iget-object v7, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    iget-object v8, p2, LX/2yn;->a:Ljava/lang/Runnable;

    const v9, -0x51d05fdf

    invoke-static {v7, v8, v5, v6, v9}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_2

    .line 482611
    :cond_5
    iget-object v5, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    invoke-virtual {v5}, LX/1Be;->f()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-wide v5, p2, LX/2yn;->c:J

    .line 482612
    :goto_3
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_4

    goto :goto_2

    .line 482613
    :cond_6
    iget-wide v5, p2, LX/2yn;->d:J

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 482614
    check-cast p2, LX/2yn;

    .line 482615
    iget-object p1, p2, LX/2yn;->a:Ljava/lang/Runnable;

    if-nez p1, :cond_0

    .line 482616
    :goto_0
    iget-object p1, p2, LX/2yn;->b:Ljava/lang/Runnable;

    if-nez p1, :cond_1

    .line 482617
    :goto_1
    return-void

    .line 482618
    :cond_0
    iget-object p1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    iget-object p3, p2, LX/2yn;->a:Ljava/lang/Runnable;

    invoke-static {p1, p3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 482619
    :cond_1
    iget-object p1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->d:Landroid/os/Handler;

    iget-object p3, p2, LX/2yn;->b:Ljava/lang/Runnable;

    invoke-static {p1, p3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_1
.end method
