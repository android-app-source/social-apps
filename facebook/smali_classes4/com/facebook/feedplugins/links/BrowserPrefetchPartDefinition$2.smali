.class public final Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public final synthetic e:Z

.field public final synthetic f:LX/2yn;

.field public final synthetic g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;ZLX/2yn;)V
    .locals 0

    .prologue
    .line 492204
    iput-object p1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    iput-object p2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iput-boolean p6, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->e:Z

    iput-object p7, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->f:LX/2yn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 492185
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 492186
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nE;

    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->b:Ljava/lang/String;

    .line 492187
    iget-object v4, v0, LX/2nE;->b:LX/1Bn;

    invoke-virtual {v4, v1}, LX/1Bn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 492188
    iget-object v5, v0, LX/2nE;->a:Landroid/util/LruCache;

    invoke-virtual {v5, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 492189
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->b:LX/1Be;

    iget-object v1, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iget-boolean v3, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->e:Z

    invoke-virtual {v0, v1, v2, v3}, LX/1Be;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Z)V

    .line 492190
    iget-object v0, p0, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition$2;->f:LX/2yn;

    const/4 v1, 0x0

    .line 492191
    iput-object v1, v0, LX/2yn;->a:Ljava/lang/Runnable;

    .line 492192
    return-void

    .line 492193
    :cond_1
    iget-object v5, v0, LX/2nE;->c:LX/1Br;

    invoke-virtual {v5, v4}, LX/1Br;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 492194
    iget-object v5, v0, LX/2nE;->c:LX/1Br;

    const/4 v6, 0x0

    .line 492195
    iget-object v7, v5, LX/1Br;->g:LX/0ad;

    sget v8, LX/1Bm;->i:I

    invoke-interface {v7, v8, v6}, LX/0ad;->a(II)I

    move-result v7

    .line 492196
    invoke-static {v5}, LX/1Br;->d(LX/1Br;)I

    move-result v8

    .line 492197
    invoke-static {v5}, LX/1Br;->c(LX/1Br;)Z

    move-result v1

    if-nez v1, :cond_2

    if-gt v8, v7, :cond_3

    :cond_2
    const/4 v6, 0x1

    :cond_3
    move v5, v6

    .line 492198
    if-eqz v5, :cond_0

    .line 492199
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 492200
    new-instance v5, LX/7hz;

    invoke-direct {v5}, LX/7hz;-><init>()V

    .line 492201
    iput-object v2, v5, LX/7hz;->a:Ljava/lang/String;

    .line 492202
    iput-object v3, v5, LX/7hz;->b:Ljava/lang/String;

    .line 492203
    iget-object v6, v0, LX/2nE;->a:Landroid/util/LruCache;

    invoke-virtual {v6, v4, v5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
