.class public Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C8Z;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2mt;

.field private final e:LX/38q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 521538
    const-string v0, "cta_click"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/2mt;LX/0Ot;LX/0Ot;LX/38q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2mt;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;",
            ">;",
            "LX/38q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521539
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 521540
    iput-object p1, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->d:LX/2mt;

    .line 521541
    iput-object p2, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->b:LX/0Ot;

    .line 521542
    iput-object p3, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->c:LX/0Ot;

    .line 521543
    iput-object p4, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->e:LX/38q;

    .line 521544
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;
    .locals 7

    .prologue
    .line 521527
    const-class v1, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    monitor-enter v1

    .line 521528
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521529
    sput-object v2, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521532
    new-instance v5, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v3

    check-cast v3, LX/2mt;

    const/16 v4, 0x972

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0x200e

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v4, LX/38q;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/38q;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;-><init>(LX/2mt;LX/0Ot;LX/0Ot;LX/38q;)V

    .line 521533
    move-object v0, v5

    .line 521534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 521508
    check-cast p2, LX/C8Z;

    const/4 v6, 0x0

    .line 521509
    iget-object v0, p2, LX/C8Z;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v0, :cond_1

    .line 521510
    :cond_0
    :goto_0
    return-object v6

    .line 521511
    :cond_1
    iget-object v0, p2, LX/C8Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521512
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 521513
    if-eqz v0, :cond_2

    .line 521514
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 521515
    instance-of v1, v1, LX/16m;

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 521516
    :cond_2
    iget-object v0, p2, LX/C8Z;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->A(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 521517
    iget-object v0, p2, LX/C8Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->d:LX/2mt;

    invoke-static {v0, v1}, LX/C8a;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2mt;)Ljava/lang/String;

    move-result-object v2

    .line 521518
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 521519
    invoke-static {v2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 521520
    iget-object v0, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/C8n;

    iget-object v4, p2, LX/C8Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p2, LX/C8Z;->d:LX/C8b;

    sget-object v5, LX/C8b;->CTA_BUTTON:LX/C8b;

    if-ne v1, v5, :cond_3

    sget-object v1, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->a:Ljava/util/Map;

    :goto_1
    iget-object v5, p2, LX/C8Z;->c:LX/2yF;

    invoke-direct {v3, v4, v1, v5, v2}, LX/C8n;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;LX/2yF;Ljava/lang/String;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 521521
    :cond_3
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 521522
    goto :goto_1

    .line 521523
    :cond_4
    iget-object v0, p2, LX/C8Z;->e:LX/2yN;

    .line 521524
    if-nez v0, :cond_5

    .line 521525
    iget-object v0, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->e:LX/38q;

    sget-object v1, LX/C8b;->CTA_BUTTON:LX/C8b;

    invoke-virtual {v0, v1}, LX/38q;->a(LX/C8b;)LX/C8c;

    move-result-object v0

    move-object v1, v0

    .line 521526
    :goto_2
    iget-object v0, p0, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/2yd;

    iget-object v4, p2, LX/C8Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p2, LX/C8Z;->c:LX/2yF;

    invoke-direct {v3, v4, v1, v5, v2}, LX/2yd;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;LX/2yF;Ljava/lang/String;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    move-object v1, v0

    goto :goto_2
.end method
