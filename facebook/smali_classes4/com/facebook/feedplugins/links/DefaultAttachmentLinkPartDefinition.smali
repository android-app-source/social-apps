.class public Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2yd;",
        "LX/2yj;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2yr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2yJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C8h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2yr;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2yJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/C8h;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482453
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 482454
    iput-object p1, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->a:LX/0Ot;

    .line 482455
    iput-object p2, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->b:LX/0Ot;

    .line 482456
    iput-object p3, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->c:LX/0Ot;

    .line 482457
    iput-object p4, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->d:LX/0Ot;

    .line 482458
    iput-object p5, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->e:LX/0Ot;

    .line 482459
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;
    .locals 9

    .prologue
    .line 482442
    const-class v1, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;

    monitor-enter v1

    .line 482443
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482444
    sput-object v2, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482445
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482446
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 482447
    new-instance v3, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;

    const/16 v4, 0xbc4

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x96e

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x970

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x971

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x200c

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 482448
    move-object v0, v3

    .line 482449
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482450
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482451
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/2yd;Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 482441
    iget-object v0, p0, LX/2yd;->c:LX/2yF;

    invoke-interface {v0, p1}, LX/2yF;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 482428
    check-cast p2, LX/2yd;

    check-cast p3, LX/1Pq;

    .line 482429
    iget-object v0, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    iget-object v1, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482430
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 482431
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 482432
    iget-object v0, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/2yf;

    iget-object v2, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/2yd;->d:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, LX/2yf;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 482433
    iget-object v0, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 482434
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 482435
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2yg;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    .line 482436
    if-eqz v5, :cond_0

    move-object v0, p3

    check-cast v0, LX/1Pr;

    new-instance v1, LX/2yU;

    iget-object v2, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v1, v2}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v0, v1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yV;

    move-object v3, v0

    :goto_0
    move-object v0, p3

    .line 482437
    check-cast v0, LX/1Pr;

    new-instance v1, LX/2yh;

    iget-object v2, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v1, v2}, LX/2yh;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object v2, p2, LX/2yd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2yi;

    .line 482438
    new-instance v7, LX/2yj;

    new-instance v0, LX/2yk;

    move-object v1, p0

    move-object v2, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/2yk;-><init>(Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;LX/2yd;LX/2yV;LX/2yi;ZLX/1Pq;)V

    new-instance v1, LX/2yl;

    invoke-direct {v1, p0, p2}, LX/2yl;-><init>(Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;LX/2yd;)V

    invoke-direct {v7, v0, v1}, LX/2yj;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 482439
    return-object v7

    .line 482440
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x61e65197

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 482401
    check-cast p1, LX/2yd;

    check-cast p2, LX/2yj;

    .line 482402
    if-nez p4, :cond_1

    .line 482403
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x5c939323

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 482404
    :cond_1
    invoke-static {p1, p4}, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->b(LX/2yd;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 482405
    if-eqz v1, :cond_2

    .line 482406
    iget-object v2, p2, LX/2yj;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482407
    iget-object v2, p2, LX/2yj;->b:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 482408
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2yr;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object p3, p1, LX/2yd;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, p3}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 482409
    iget-object v1, p1, LX/2yd;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482410
    instance-of v1, p4, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    if-eqz v1, :cond_0

    .line 482411
    check-cast p4, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    const/4 v1, 0x1

    .line 482412
    iput-boolean v1, p4, Lcom/facebook/attachments/angora/AngoraAttachmentView;->h:Z

    .line 482413
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 482414
    check-cast p1, LX/2yd;

    const/4 v1, 0x0

    .line 482415
    invoke-static {p1, p4}, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->b(LX/2yd;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 482416
    if-eqz v0, :cond_0

    .line 482417
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482418
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 482419
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yr;

    .line 482420
    iget-object v1, v0, LX/2yr;->b:LX/CHQ;

    if-eqz v1, :cond_3

    .line 482421
    iget-object v1, v0, LX/2yr;->b:LX/CHQ;

    .line 482422
    iget-object p0, v1, LX/CHQ;->b:LX/1ca;

    if-eqz p0, :cond_1

    .line 482423
    iget-object p0, v1, LX/CHQ;->b:LX/1ca;

    invoke-interface {p0}, LX/1ca;->g()Z

    .line 482424
    :cond_1
    iget-object p0, v1, LX/CHQ;->d:LX/8Ys;

    if-eqz p0, :cond_2

    .line 482425
    iget-object p0, v1, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p1, v1, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p1}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/8bW;->a(Ljava/lang/String;)V

    .line 482426
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, LX/2yr;->b:LX/CHQ;

    .line 482427
    :goto_0
    return-void

    :cond_3
    goto :goto_0
.end method
