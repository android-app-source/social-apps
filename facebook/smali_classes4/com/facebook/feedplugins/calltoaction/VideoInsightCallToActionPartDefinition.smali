.class public Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/38y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/38y",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/38y;LX/1V0;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521838
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 521839
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->d:LX/38y;

    .line 521840
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->e:LX/1V0;

    .line 521841
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->f:LX/0ad;

    .line 521842
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 521819
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->d:LX/38y;

    const/4 v1, 0x0

    .line 521820
    new-instance v2, LX/C2e;

    invoke-direct {v2, v0}, LX/C2e;-><init>(LX/38y;)V

    .line 521821
    iget-object v3, v0, LX/38y;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C2d;

    .line 521822
    if-nez v3, :cond_0

    .line 521823
    new-instance v3, LX/C2d;

    invoke-direct {v3, v0}, LX/C2d;-><init>(LX/38y;)V

    .line 521824
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C2d;->a$redex0(LX/C2d;LX/1De;IILX/C2e;)V

    .line 521825
    move-object v2, v3

    .line 521826
    move-object v1, v2

    .line 521827
    move-object v0, v1

    .line 521828
    iget-object v1, v0, LX/C2d;->a:LX/C2e;

    iput-object p2, v1, LX/C2e;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521829
    iget-object v1, v0, LX/C2d;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 521830
    move-object v1, v0

    .line 521831
    move-object v0, p3

    check-cast v0, LX/1Pr;

    .line 521832
    iget-object v2, v1, LX/C2d;->a:LX/C2e;

    iput-object v0, v2, LX/C2e;->b:LX/1Pr;

    .line 521833
    iget-object v2, v1, LX/C2d;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 521834
    move-object v0, v1

    .line 521835
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 521836
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 521837
    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;
    .locals 7

    .prologue
    .line 521808
    const-class v1, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;

    monitor-enter v1

    .line 521809
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521810
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521811
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521812
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521813
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/38y;->a(LX/0QB;)LX/38y;

    move-result-object v4

    check-cast v4, LX/38y;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;-><init>(Landroid/content/Context;LX/38y;LX/1V0;LX/0ad;)V

    .line 521814
    move-object v0, p0

    .line 521815
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521816
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521817
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 521807
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 521806
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 521797
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 521798
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 521799
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x41e065f

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x403827a

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 521800
    :goto_0
    return v0

    .line 521801
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521802
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v2, -0x674bf213

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 521803
    if-nez v0, :cond_2

    move v0, v1

    .line 521804
    goto :goto_0

    .line 521805
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/VideoInsightCallToActionPartDefinition;->f:LX/0ad;

    sget-short v2, LX/0fe;->bM:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 521795
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521796
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
