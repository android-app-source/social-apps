.class public Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ":",
        "LX/3Vw;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/1qb;

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/2ye;

.field private final f:LX/38x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition",
            "<TE;TV;>.CallToActionAttachment",
            "LinkedViewAdapter;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

.field private final h:LX/1V7;

.field private final i:LX/38q;


# direct methods
.method public constructor <init>(LX/1qb;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;LX/2ye;Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;LX/1V7;LX/38q;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521759
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 521760
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->a:LX/1qb;

    .line 521761
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 521762
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 521763
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->e:LX/2ye;

    .line 521764
    iput-object p5, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    .line 521765
    iput-object p7, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->h:LX/1V7;

    .line 521766
    new-instance v0, LX/38x;

    invoke-direct {v0, p0}, LX/38x;-><init>(Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->f:LX/38x;

    .line 521767
    iput-object p6, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    .line 521768
    iput-object p8, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->i:LX/38q;

    .line 521769
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;
    .locals 12

    .prologue
    .line 521770
    const-class v1, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;

    monitor-enter v1

    .line 521771
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521772
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521775
    new-instance v3, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static {v0}, LX/2ye;->a(LX/0QB;)LX/2ye;

    move-result-object v7

    check-cast v7, LX/2ye;

    invoke-static {v0}, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v10

    check-cast v10, LX/1V7;

    const-class v11, LX/38q;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/38q;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;-><init>(LX/1qb;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;LX/2ye;Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;LX/1V7;LX/38q;)V

    .line 521776
    move-object v0, v3

    .line 521777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 521781
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 521782
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 521783
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    new-instance v1, LX/C2R;

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->a:LX/1qb;

    invoke-direct {v1, p2, v2}, LX/C2R;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1qb;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521784
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    new-instance v1, LX/C8Z;

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->e:LX/2ye;

    sget-object v3, LX/C8b;->ATTACHMENT:LX/C8b;

    iget-object v4, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->i:LX/38q;

    sget-object v5, LX/C8b;->ATTACHMENT:LX/C8b;

    invoke-virtual {v4, v5}, LX/38q;->a(LX/C8b;)LX/C8c;

    move-result-object v4

    invoke-direct {v1, p2, v2, v3, v4}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521785
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;

    new-instance v1, LX/C8Z;

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->f:LX/38x;

    sget-object v3, LX/C8b;->CTA_BUTTON:LX/C8b;

    iget-object v4, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->i:LX/38q;

    sget-object v5, LX/C8b;->CTA_BUTTON:LX/C8b;

    invoke-virtual {v4, v5}, LX/38q;->a(LX/C8b;)LX/C8c;

    move-result-object v4

    invoke-direct {v1, p2, v2, v3, v4}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521786
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->h:LX/1V7;

    invoke-virtual {v3}, LX/1V7;->h()LX/1Ua;

    move-result-object v3

    const v4, 0x7f020ae3

    const/4 v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521787
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521788
    return-object v6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 521789
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521790
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521791
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521792
    invoke-static {v0}, LX/2v7;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
