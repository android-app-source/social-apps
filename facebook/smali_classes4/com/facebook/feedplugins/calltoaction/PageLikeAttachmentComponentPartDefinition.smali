.class public Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pd;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/38z;

.field private final e:LX/1V0;

.field private final f:LX/1V7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/38z;LX/1V0;LX/1V7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521889
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 521890
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->d:LX/38z;

    .line 521891
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->e:LX/1V0;

    .line 521892
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->f:LX/1V7;

    .line 521893
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 521894
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->f:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->h()LX/1Ua;

    move-result-object v2

    const v3, 0x7f020ae3

    const/4 v4, -0x1

    invoke-direct {v1, v0, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 521895
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->d:LX/38z;

    const/4 v2, 0x0

    .line 521896
    new-instance v3, LX/C2a;

    invoke-direct {v3, v0}, LX/C2a;-><init>(LX/38z;)V

    .line 521897
    iget-object v4, v0, LX/38z;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C2Z;

    .line 521898
    if-nez v4, :cond_0

    .line 521899
    new-instance v4, LX/C2Z;

    invoke-direct {v4, v0}, LX/C2Z;-><init>(LX/38z;)V

    .line 521900
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C2Z;->a$redex0(LX/C2Z;LX/1De;IILX/C2a;)V

    .line 521901
    move-object v3, v4

    .line 521902
    move-object v2, v3

    .line 521903
    move-object v0, v2

    .line 521904
    iget-object v2, v0, LX/C2Z;->a:LX/C2a;

    iput-object p2, v2, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521905
    iget-object v2, v0, LX/C2Z;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 521906
    move-object v2, v0

    .line 521907
    move-object v0, p3

    check-cast v0, LX/1Pd;

    .line 521908
    iget-object v3, v2, LX/C2Z;->a:LX/C2a;

    iput-object v0, v3, LX/C2a;->b:LX/1Pd;

    .line 521909
    iget-object v3, v2, LX/C2Z;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 521910
    move-object v0, v2

    .line 521911
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 521912
    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;
    .locals 7

    .prologue
    .line 521913
    const-class v1, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 521914
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521915
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521916
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521917
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521918
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/38z;->a(LX/0QB;)LX/38z;

    move-result-object v4

    check-cast v4, LX/38z;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V7;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/38z;LX/1V0;LX/1V7;)V

    .line 521919
    move-object v0, p0

    .line 521920
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521921
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521922
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521923
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 521924
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 521925
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/calltoaction/PageLikeAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 521926
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521927
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521928
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521929
    invoke-static {v0}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 521930
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521931
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
