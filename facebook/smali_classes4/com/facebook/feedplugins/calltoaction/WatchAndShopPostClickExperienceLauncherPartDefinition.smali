.class public Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2g;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/38r;


# direct methods
.method public constructor <init>(LX/38r;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521597
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 521598
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;->a:LX/38r;

    .line 521599
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;
    .locals 4

    .prologue
    .line 521600
    const-class v1, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;

    monitor-enter v1

    .line 521601
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521602
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521603
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521604
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521605
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;

    invoke-static {v0}, LX/38r;->a(LX/0QB;)LX/38r;

    move-result-object v3

    check-cast v3, LX/38r;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;-><init>(LX/38r;)V

    .line 521606
    move-object v0, p0

    .line 521607
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521608
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521609
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 521611
    check-cast p2, LX/C2g;

    .line 521612
    new-instance v0, LX/C2h;

    iget-object v1, p2, LX/C2g;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;->a:LX/38r;

    invoke-direct {v0, p0, v1, v2}, LX/C2h;-><init>(Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/38r;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4e0aa1fe    # 5.8146803E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 521613
    check-cast p1, LX/C2g;

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 521614
    iget-object v1, p1, LX/C2g;->b:LX/2yF;

    invoke-interface {v1, p4}, LX/2yF;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    .line 521615
    if-eqz v2, :cond_1

    .line 521616
    instance-of v1, v2, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 521617
    check-cast v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 521618
    iget-object p1, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, p1

    .line 521619
    invoke-virtual {v1, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521620
    :cond_0
    invoke-virtual {v2, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521621
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x7536658b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 521622
    check-cast p1, LX/C2g;

    const/4 v2, 0x0

    .line 521623
    iget-object v0, p1, LX/C2g;->b:LX/2yF;

    invoke-interface {v0, p4}, LX/2yF;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 521624
    if-eqz v1, :cond_1

    .line 521625
    instance-of v0, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 521626
    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 521627
    iget-object p0, v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, p0

    .line 521628
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521629
    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521630
    :cond_1
    return-void
.end method
