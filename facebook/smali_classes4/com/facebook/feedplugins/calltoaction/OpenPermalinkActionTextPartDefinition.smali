.class public Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ":",
        "LX/3Vw;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "LX/C2Y;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1Uj;


# direct methods
.method public constructor <init>(LX/1Uj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522088
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 522089
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;->a:LX/1Uj;

    .line 522090
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;
    .locals 4

    .prologue
    .line 522091
    const-class v1, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    monitor-enter v1

    .line 522092
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522093
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522096
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v3

    check-cast v3, LX/1Uj;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;-><init>(LX/1Uj;)V

    .line 522097
    move-object v0, p0

    .line 522098
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522099
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522100
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 522102
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 522103
    const v0, -0x5e32ca2f

    invoke-static {p2, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 522104
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 522105
    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;->a:LX/1Uj;

    invoke-virtual {v2}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 522106
    new-instance v2, LX/C2Y;

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v3, v0}, LX/C2Y;-><init>(Landroid/text/Spannable;Landroid/text/Spannable;Ljava/lang/String;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x39c52a39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 522107
    check-cast p2, LX/C2Y;

    const/4 v2, 0x0

    .line 522108
    move-object v1, p4

    check-cast v1, LX/3Vw;

    invoke-interface {v1, v2}, LX/3Vw;->setShowRating(Z)V

    move-object v1, p4

    .line 522109
    check-cast v1, LX/3Vw;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, LX/3Vw;->setRating(F)V

    move-object v1, p4

    .line 522110
    check-cast v1, LX/3Vw;

    invoke-interface {v1, v2}, LX/3Vw;->setNumberOfStars(I)V

    move-object v1, p4

    .line 522111
    check-cast v1, LX/2yX;

    iget-object p0, p2, LX/C2Y;->a:Landroid/text/Spannable;

    invoke-interface {v1, p0}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 522112
    check-cast v1, LX/2yX;

    iget-object p0, p2, LX/C2Y;->b:Landroid/text/Spannable;

    invoke-interface {v1, p0}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 522113
    check-cast v1, LX/3Vx;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    .line 522114
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object p0

    .line 522115
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v1

    .line 522116
    iget-object p1, p2, LX/C2Y;->c:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 522117
    iget-object v1, p2, LX/C2Y;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 522118
    const/16 v1, 0x1f

    const v2, 0x5201127

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 522119
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 522120
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 522121
    return-void
.end method
