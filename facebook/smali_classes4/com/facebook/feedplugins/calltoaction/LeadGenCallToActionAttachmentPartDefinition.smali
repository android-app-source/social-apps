.class public Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ":",
        "LX/3Vw;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/C2U;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/1Cz;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V7;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2sb;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2yI;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521465
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/2sb;LX/0Ot;LX/2yI;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1V7;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/2sb;",
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;",
            "LX/2yI;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521456
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 521457
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->b:LX/0Ot;

    .line 521458
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->c:LX/0Ot;

    .line 521459
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->d:LX/0Ot;

    .line 521460
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->e:LX/2sb;

    .line 521461
    iput-object p5, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->f:LX/0Ot;

    .line 521462
    iput-object p6, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->g:LX/2yI;

    .line 521463
    iput-object p7, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->h:LX/0Ot;

    .line 521464
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 521445
    const-class v1, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;

    monitor-enter v1

    .line 521446
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521447
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521448
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521449
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521450
    new-instance v3, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;

    const/16 v4, 0x756

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x759

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe0f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object v7

    check-cast v7, LX/2sb;

    const/16 v8, 0x781

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, LX/2yI;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2yI;

    const/16 v10, 0x86f

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/2sb;LX/0Ot;LX/2yI;LX/0Ot;)V

    .line 521451
    move-object v0, v3

    .line 521452
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521453
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521454
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521455
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 521444
    sget-object v0, LX/3WR;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 521435
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    .line 521436
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521437
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521438
    new-instance v2, LX/C2U;

    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qb;

    invoke-virtual {v1, v0}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {v2, v1, v0}, LX/C2U;-><init>(Landroid/text/Spannable;Landroid/text/Spannable;)V

    .line 521439
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->g:LX/2yI;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, p3}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v1

    .line 521440
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521441
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1V7;

    invoke-virtual {v1}, LX/1V7;->h()LX/1Ua;

    move-result-object v1

    const v5, 0x7f020ae3

    const/4 v6, -0x1

    invoke-direct {v3, v4, v1, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521442
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/LeadGenCallToActionAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521443
    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3f495318    # 0.78642416f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 521427
    check-cast p2, LX/C2U;

    const/4 p0, 0x0

    .line 521428
    move-object v1, p4

    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/C2U;->a:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521429
    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/C2U;->b:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521430
    check-cast v1, LX/3Vx;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521431
    check-cast v1, LX/3Vw;

    invoke-interface {v1, p0}, LX/3Vw;->setShowRating(Z)V

    move-object v1, p4

    .line 521432
    check-cast v1, LX/3Vw;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/3Vw;->setRating(F)V

    .line 521433
    check-cast p4, LX/3Vw;

    invoke-interface {p4, p0}, LX/3Vw;->setNumberOfStars(I)V

    .line 521434
    const/16 v1, 0x1f

    const v2, -0x36d691e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 521422
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521423
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521424
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 521425
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 521426
    return-void
.end method
