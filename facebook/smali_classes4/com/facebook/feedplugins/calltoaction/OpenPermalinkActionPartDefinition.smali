.class public Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ":",
        "LX/3Vw;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/C2X;",
        "LX/1Ps;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/1V7;

.field private final c:Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final d:LX/1xP;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;LX/1xP;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522050
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 522051
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 522052
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->b:LX/1V7;

    .line 522053
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    .line 522054
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->d:LX/1xP;

    .line 522055
    iput-object p5, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 522056
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;
    .locals 9

    .prologue
    .line 522057
    const-class v1, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;

    monitor-enter v1

    .line 522058
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522059
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522060
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522061
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522062
    new-instance v3, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V7;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v7

    check-cast v7, LX/1xP;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;LX/1xP;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 522063
    move-object v0, v3

    .line 522064
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522065
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522066
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 522068
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 522069
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522070
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522071
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x5e32ca2f

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 522072
    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionTextPartDefinition;

    .line 522073
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 522074
    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522075
    new-instance v1, LX/C2W;

    invoke-direct {v1, p0, v0}, LX/C2W;-><init>(Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    move-object v0, v1

    .line 522076
    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522077
    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/calltoaction/OpenPermalinkActionPartDefinition;->b:LX/1V7;

    invoke-virtual {v4}, LX/1V7;->h()LX/1Ua;

    move-result-object v4

    const v5, 0x7f020ae3

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522078
    new-instance v1, LX/C2X;

    invoke-direct {v1, v0}, LX/C2X;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x78b283e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 522079
    check-cast p2, LX/C2X;

    .line 522080
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    iget-object v2, p2, LX/C2X;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 522081
    const/16 v1, 0x1f

    const v2, 0x58a7ba4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 522082
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522083
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522084
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 522085
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    .line 522086
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 522087
    return-void
.end method
