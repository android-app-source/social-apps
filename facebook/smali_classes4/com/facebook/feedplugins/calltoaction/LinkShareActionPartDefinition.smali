.class public Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;

.field private final d:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

.field private final e:LX/1V7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521973
    new-instance v0, LX/390;

    invoke-direct {v0}, LX/390;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;LX/1V7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521974
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 521975
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 521976
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;

    .line 521977
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    .line 521978
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->e:LX/1V7;

    .line 521979
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;
    .locals 7

    .prologue
    .line 521980
    const-class v1, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;

    monitor-enter v1

    .line 521981
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521982
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521983
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521984
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521985
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V7;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;LX/1V7;)V

    .line 521986
    move-object v0, p0

    .line 521987
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521988
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521989
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521990
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 521991
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 521992
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521993
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521994
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521995
    const v1, 0x7f0d192b

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/LinkShareActionTextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 521996
    const v1, 0x7f0d192c

    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 521997
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->e:LX/1V7;

    invoke-virtual {v3}, LX/1V7;->h()LX/1Ua;

    move-result-object v3

    const v4, 0x7f020ae3

    const/4 v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521998
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 521999
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522000
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522001
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
