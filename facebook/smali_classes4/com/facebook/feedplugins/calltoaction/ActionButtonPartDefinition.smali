.class public Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2sO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2sO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480704
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 480705
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a:LX/0Ot;

    .line 480706
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .locals 4

    .prologue
    .line 480707
    const-class v1, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    monitor-enter v1

    .line 480708
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480709
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480710
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480711
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480712
    new-instance v3, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    const/16 p0, 0x14f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;-><init>(LX/0Ot;)V

    .line 480713
    move-object v0, v3

    .line 480714
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480715
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480716
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480718
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480719
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v0

    .line 480720
    invoke-virtual {v0}, LX/2y5;->a()LX/1Nt;

    move-result-object v0

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480721
    const/4 v0, 0x0

    return-object v0
.end method
