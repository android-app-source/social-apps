.class public Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:LX/1Kf;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/1Kf;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522045
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 522046
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->a:Landroid/app/Activity;

    .line 522047
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->b:LX/1Kf;

    .line 522048
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 522049
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;
    .locals 6

    .prologue
    .line 522034
    const-class v1, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    monitor-enter v1

    .line 522035
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 522036
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 522037
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522038
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 522039
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v4

    check-cast v4, LX/1Kf;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;-><init>(Landroid/app/Activity;LX/1Kf;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 522040
    move-object v0, p0

    .line 522041
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 522042
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522043
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 522044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522026
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 522027
    const v0, 0x278a7d5

    invoke-static {p2, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 522028
    if-nez v0, :cond_0

    .line 522029
    const/4 v0, 0x0

    .line 522030
    :goto_0
    return-object v0

    .line 522031
    :cond_0
    new-instance v1, LX/C2V;

    invoke-direct {v1, p0, v0}, LX/C2V;-><init>(Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    .line 522032
    iget-object v2, p0, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 522033
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x699b359d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 522023
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 522024
    invoke-virtual {p4, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 522025
    const/16 v1, 0x1f

    const v2, 0x541efdcd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
