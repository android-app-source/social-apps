.class public Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ":",
        "LX/1Pq;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ":",
        "LX/3Vw;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2R;",
        "LX/C2S;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/2yr;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2yr;Landroid/content/Context;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521549
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 521550
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->a:LX/2yr;

    .line 521551
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->b:Landroid/content/Context;

    .line 521552
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 521553
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;
    .locals 6

    .prologue
    .line 521554
    const-class v1, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    monitor-enter v1

    .line 521555
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521556
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521557
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521558
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521559
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;

    invoke-static {v0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v3

    check-cast v3, LX/2yr;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;-><init>(LX/2yr;Landroid/content/Context;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 521560
    move-object v0, p0

    .line 521561
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521562
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521563
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 521565
    check-cast p2, LX/C2R;

    .line 521566
    iget-object v0, p2, LX/C2R;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521567
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 521568
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521569
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLRating;->j()I

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v9

    double-to-float v9, v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_1

    const/4 v9, 0x1

    :goto_0
    move v6, v9

    .line 521570
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRating;->j()I

    move-result v1

    :goto_1
    move v1, v1

    .line 521571
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v9

    double-to-float v9, v9

    :goto_2
    move v2, v9

    .line 521572
    iget-object v3, p2, LX/C2R;->b:LX/1qb;

    invoke-virtual {v3, v0}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v3

    .line 521573
    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v4

    .line 521574
    iget-object v5, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->a:LX/2yr;

    iget-object v7, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 521575
    const/4 v5, 0x0

    .line 521576
    invoke-static {v0}, LX/2v7;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521577
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/2v7;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 521578
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    iget-object v7, p2, LX/C2R;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1, v0, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521579
    new-instance v0, LX/C2S;

    invoke-direct/range {v0 .. v6}, LX/C2S;-><init>(IFLandroid/text/Spannable;Landroid/text/Spannable;Landroid/text/Spannable;Z)V

    return-object v0

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x735a8097

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 521580
    check-cast p2, LX/C2S;

    .line 521581
    move-object v1, p4

    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/C2S;->c:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521582
    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/C2S;->d:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521583
    check-cast v1, LX/3Vx;

    iget-object v2, p2, LX/C2S;->e:Landroid/text/Spannable;

    invoke-interface {v1, v2}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    move-object v1, p4

    .line 521584
    check-cast v1, LX/3Vw;

    iget v2, p2, LX/C2S;->a:I

    invoke-interface {v1, v2}, LX/3Vw;->setNumberOfStars(I)V

    move-object v1, p4

    .line 521585
    check-cast v1, LX/3Vw;

    iget v2, p2, LX/C2S;->b:F

    invoke-interface {v1, v2}, LX/3Vw;->setRating(F)V

    .line 521586
    check-cast p4, LX/3Vw;

    iget-boolean v1, p2, LX/C2S;->f:Z

    invoke-interface {p4, v1}, LX/3Vw;->setShowRating(Z)V

    .line 521587
    const/16 v1, 0x1f

    const v2, 0x3ad190a9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 521588
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;->a:LX/2yr;

    .line 521589
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    if-eqz p0, :cond_2

    .line 521590
    iget-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 521591
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    if-eqz p1, :cond_0

    .line 521592
    iget-object p1, p0, LX/CHQ;->b:LX/1ca;

    invoke-interface {p1}, LX/1ca;->g()Z

    .line 521593
    :cond_0
    iget-object p1, p0, LX/CHQ;->d:LX/8Ys;

    if-eqz p1, :cond_1

    .line 521594
    iget-object p1, p0, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p1, p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p2, p0, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p2}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/8bW;->a(Ljava/lang/String;)V

    .line 521595
    :cond_1
    const/4 p0, 0x0

    iput-object p0, v0, LX/2yr;->b:LX/CHQ;

    .line 521596
    :goto_0
    return-void

    :cond_2
    goto :goto_0
.end method
