.class public Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pg;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;


# direct methods
.method public constructor <init>(LX/14w;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498447
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 498448
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->a:LX/14w;

    .line 498449
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    .line 498450
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;
    .locals 5

    .prologue
    .line 498451
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    monitor-enter v1

    .line 498452
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498453
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498456
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;-><init>(LX/14w;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;)V

    .line 498457
    move-object v0, p0

    .line 498458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 498462
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 498463
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498464
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    new-instance v1, LX/3Dt;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, LX/3Dt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498465
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498466
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498467
    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
