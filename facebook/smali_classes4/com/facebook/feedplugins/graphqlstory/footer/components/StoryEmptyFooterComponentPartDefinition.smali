.class public Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static e:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 498769
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498767
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 498768
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;
    .locals 4

    .prologue
    .line 498756
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;

    monitor-enter v1

    .line 498757
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498758
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498759
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498760
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498761
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 498762
    move-object v0, p0

    .line 498763
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498764
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498765
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private g()LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 498755
    new-instance v0, LX/C5v;

    invoke-direct {v0, p0}, LX/C5v;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 498770
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->g()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 498754
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->g()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498753
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 498751
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498752
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 498750
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
