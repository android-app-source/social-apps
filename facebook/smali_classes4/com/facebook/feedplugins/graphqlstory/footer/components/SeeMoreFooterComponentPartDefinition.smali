.class public Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/36D;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40800000    # 4.0f

    .line 498339
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 498340
    iput v1, v0, LX/1UY;->b:F

    .line 498341
    move-object v0, v0

    .line 498342
    iput v1, v0, LX/1UY;->c:F

    .line 498343
    move-object v0, v0

    .line 498344
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/36D;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498345
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 498346
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->e:LX/1V0;

    .line 498347
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->f:LX/36D;

    .line 498348
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 498349
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->f:LX/36D;

    const/4 v1, 0x0

    .line 498350
    new-instance v2, LX/C5t;

    invoke-direct {v2, v0}, LX/C5t;-><init>(LX/36D;)V

    .line 498351
    sget-object v3, LX/36D;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C5s;

    .line 498352
    if-nez v3, :cond_0

    .line 498353
    new-instance v3, LX/C5s;

    invoke-direct {v3}, LX/C5s;-><init>()V

    .line 498354
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C5s;->a$redex0(LX/C5s;LX/1De;IILX/C5t;)V

    .line 498355
    move-object v2, v3

    .line 498356
    move-object v1, v2

    .line 498357
    move-object v0, v1

    .line 498358
    iget-object v1, v0, LX/C5s;->a:LX/C5t;

    iput-object p2, v1, LX/C5t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498359
    iget-object v1, v0, LX/C5s;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 498360
    move-object v0, v0

    .line 498361
    iget-object v1, v0, LX/C5s;->a:LX/C5t;

    iput-object p3, v1, LX/C5t;->b:LX/1Ps;

    .line 498362
    iget-object v1, v0, LX/C5s;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 498363
    move-object v0, v0

    .line 498364
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 498365
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->e:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;
    .locals 6

    .prologue
    .line 498366
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    monitor-enter v1

    .line 498367
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498368
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498369
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498370
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498371
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/36D;->a(LX/0QB;)LX/36D;

    move-result-object v4

    check-cast v4, LX/36D;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/36D;LX/1V0;)V

    .line 498372
    move-object v0, p0

    .line 498373
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498374
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498375
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498376
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 498377
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 498378
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498379
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498380
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 498381
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 498382
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->l()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 498383
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498384
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
