.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final c:LX/36i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 499944
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 499945
    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/36i;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 499941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499942
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->c:LX/36i;

    .line 499943
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;
    .locals 4

    .prologue
    .line 499930
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;

    monitor-enter v1

    .line 499931
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499932
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499933
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499934
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499935
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;

    invoke-static {v0}, LX/36i;->a(LX/0QB;)LX/36i;

    move-result-object v3

    check-cast v3, LX/36i;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;-><init>(LX/36i;)V

    .line 499936
    move-object v0, p0

    .line 499937
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499938
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499939
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;ZLcom/facebook/common/callercontext/CallerContext;Z)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 499897
    if-eqz p4, :cond_3

    if-nez p6, :cond_3

    .line 499898
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499899
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 499900
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->c:LX/36i;

    invoke-virtual {v1, p1, v0}, LX/36i;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    .line 499901
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->c:LX/36i;

    const v4, 0x7f0b08f9

    const v5, 0x7f0b00d4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    invoke-virtual/range {v0 .. v5}, LX/36i;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/common/callercontext/CallerContext;II)LX/1Di;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x2

    const/4 v7, 0x1

    .line 499902
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 499903
    check-cast v2, Lcom/facebook/graphql/model/GraphQLComment;

    .line 499904
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 499905
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 499906
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499907
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 499908
    check-cast v4, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v4}, LX/36l;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v4

    .line 499909
    const/4 p2, 0x1

    .line 499910
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->n()I

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result p0

    if-eq p0, p2, :cond_1

    const/4 p0, 0x0

    .line 499911
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    if-nez p3, :cond_6

    .line 499912
    :cond_0
    :goto_1
    move p0, p0

    .line 499913
    if-eqz p0, :cond_5

    .line 499914
    :cond_1
    const-string p0, ""

    .line 499915
    :goto_2
    move-object v2, p0

    .line 499916
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a0443

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v2, 0x0

    :goto_3
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 499917
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 499918
    invoke-static {p1, v6}, LX/36i;->a(LX/1De;Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    .line 499919
    if-eqz p4, :cond_2

    .line 499920
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/1ne;->j(I)LX/1ne;

    .line 499921
    :cond_2
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 499922
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 499923
    const v1, 0x519181a9

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 499924
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 499925
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->c:LX/36i;

    .line 499926
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 499927
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x5a

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/36i;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a010e

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    goto :goto_3

    :cond_5
    const p0, 0x7f0810d6

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p5, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->n()I

    move-result p6

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p6

    aput-object p6, p3, p5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result p5

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    aput-object p5, p3, p2

    invoke-virtual {p1, p0, p3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_2

    .line 499928
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object p3

    .line 499929
    if-eqz p3, :cond_0

    sget-object p5, LX/21y;->RANKED_ORDER:LX/21y;

    iget-object p5, p5, LX/21y;->toString:Ljava/lang/String;

    invoke-virtual {p5, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p0, 0x1

    goto/16 :goto_1
.end method
