.class public final Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/21i;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Lcom/facebook/common/callercontext/CallerContext;

.field public e:Z

.field public f:LX/1EO;

.field public final synthetic g:LX/21i;


# direct methods
.method public constructor <init>(LX/21i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 499865
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->g:LX/21i;

    .line 499866
    move-object v0, p1

    .line 499867
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 499868
    iput-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->c:Z

    .line 499869
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 499870
    iput-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->e:Z

    .line 499871
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499872
    const-string v0, "InlineCommentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 499873
    if-ne p0, p1, :cond_1

    .line 499874
    :cond_0
    :goto_0
    return v0

    .line 499875
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 499876
    goto :goto_0

    .line 499877
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    .line 499878
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 499879
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 499880
    if-eq v2, v3, :cond_0

    .line 499881
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 499882
    goto :goto_0

    .line 499883
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 499884
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 499885
    goto :goto_0

    .line 499886
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 499887
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->c:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 499888
    goto :goto_0

    .line 499889
    :cond_a
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 499890
    goto :goto_0

    .line 499891
    :cond_c
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_b

    .line 499892
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->e:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->e:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 499893
    goto :goto_0

    .line 499894
    :cond_e
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    invoke-virtual {v2, v3}, LX/1EO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 499895
    goto :goto_0

    .line 499896
    :cond_f
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
