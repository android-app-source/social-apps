.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EN;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1nu;

.field public final f:LX/1vg;

.field public final g:LX/0WJ;

.field public final h:LX/1Ve;

.field public final i:Lcom/facebook/feedback/ui/CommentComposerHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523018
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 523019
    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;LX/0WJ;LX/1Ve;Lcom/facebook/feedback/ui/CommentComposerHelper;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 523020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523021
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 523022
    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->c:LX/0Ot;

    .line 523023
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 523024
    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->d:LX/0Ot;

    .line 523025
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->e:LX/1nu;

    .line 523026
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->f:LX/1vg;

    .line 523027
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->g:LX/0WJ;

    .line 523028
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->h:LX/1Ve;

    .line 523029
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->i:Lcom/facebook/feedback/ui/CommentComposerHelper;

    .line 523030
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;
    .locals 9

    .prologue
    .line 523031
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    monitor-enter v1

    .line 523032
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523033
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523034
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523035
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523036
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v6

    check-cast v6, LX/0WJ;

    invoke-static {v0}, LX/1Ve;->a(LX/0QB;)LX/1Ve;

    move-result-object v7

    check-cast v7, LX/1Ve;

    invoke-static {v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(LX/0QB;)Lcom/facebook/feedback/ui/CommentComposerHelper;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/CommentComposerHelper;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;-><init>(LX/1nu;LX/1vg;LX/0WJ;LX/1Ve;Lcom/facebook/feedback/ui/CommentComposerHelper;)V

    .line 523037
    const/16 v4, 0x775

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 523038
    iput-object v4, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->c:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->d:LX/0Ot;

    .line 523039
    move-object v0, v3

    .line 523040
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523041
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523042
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
