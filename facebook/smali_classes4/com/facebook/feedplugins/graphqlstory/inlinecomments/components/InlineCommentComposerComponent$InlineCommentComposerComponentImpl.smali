.class public final Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/35M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/35M;


# direct methods
.method public constructor <init>(LX/35M;)V
    .locals 1

    .prologue
    .line 522995
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->d:LX/35M;

    .line 522996
    move-object v0, p1

    .line 522997
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 522998
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 522999
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523000
    const-string v0, "InlineCommentComposerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523001
    if-ne p0, p1, :cond_1

    .line 523002
    :cond_0
    :goto_0
    return v0

    .line 523003
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523004
    goto :goto_0

    .line 523005
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    .line 523006
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 523007
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 523008
    if-eq v2, v3, :cond_0

    .line 523009
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 523010
    goto :goto_0

    .line 523011
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 523012
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 523013
    goto :goto_0

    .line 523014
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_7

    .line 523015
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 523016
    goto :goto_0

    .line 523017
    :cond_a
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
