.class public final Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y5;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x79e50ef1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 486605
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 486596
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 486597
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 486598
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 486599
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->h:Ljava/lang/String;

    .line 486600
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 486601
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 486602
    if-eqz v0, :cond_0

    .line 486603
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 486604
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486622
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->i:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->i:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 486623
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->i:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 486606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 486607
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 486608
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 486609
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 486610
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 486611
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 486612
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 486613
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 486614
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 486615
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 486616
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 486617
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 486618
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 486619
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 486620
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 486621
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 486625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 486626
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 486627
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 486628
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 486629
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    .line 486630
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->i:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 486631
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 486632
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 486624
    new-instance v0, LX/AoO;

    invoke-direct {v0, p1}, LX/AoO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486589
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 486590
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486591
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->v_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 486592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 486593
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 486594
    :goto_0
    return-void

    .line 486595
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 486578
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486579
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->a(Ljava/lang/String;)V

    .line 486580
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486575
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 486576
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 486577
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 486572
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;-><init>()V

    .line 486573
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 486574
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486570
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 486571
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 486569
    const v0, 0x136ea10b

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486581
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->g:Ljava/lang/String;

    .line 486582
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 486588
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486583
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->j:Ljava/lang/String;

    .line 486584
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486585
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->k()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486586
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->h:Ljava/lang/String;

    .line 486587
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->h:Ljava/lang/String;

    return-object v0
.end method
