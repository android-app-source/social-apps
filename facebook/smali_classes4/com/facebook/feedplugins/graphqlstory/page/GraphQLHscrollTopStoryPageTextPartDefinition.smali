.class public Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/3Do;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(LX/3Do;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 536072
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 536073
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->a:LX/3Do;

    .line 536074
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 536075
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 536076
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;
    .locals 6

    .prologue
    .line 536077
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    monitor-enter v1

    .line 536078
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 536079
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 536080
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536081
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 536082
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;

    const-class v3, LX/3Do;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3Do;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;-><init>(LX/3Do;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 536083
    move-object v0, p0

    .line 536084
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 536085
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536086
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 536087
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 536088
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 536089
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 536090
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 536091
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536092
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLHscrollTopStoryPageTextPartDefinition;->a:LX/3Do;

    invoke-virtual {v1, p2}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536093
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x209be998

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 536094
    check-cast p4, Landroid/widget/TextView;

    .line 536095
    const/4 v1, 0x2

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 536096
    const/16 v1, 0x1f

    const v2, -0x4b2acad3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
