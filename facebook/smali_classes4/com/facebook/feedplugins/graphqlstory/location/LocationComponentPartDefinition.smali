.class public Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/0ad;

.field private final e:LX/1V0;

.field private final f:LX/3BO;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/1V0;LX/3BO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528105
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 528106
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->d:LX/0ad;

    .line 528107
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->e:LX/1V0;

    .line 528108
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->f:LX/3BO;

    .line 528109
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 528110
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 528111
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    const v3, 0x7f020a45

    const/4 v4, -0x1

    invoke-direct {v1, v0, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 528112
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->f:LX/3BO;

    const/4 v2, 0x0

    .line 528113
    new-instance v3, LX/C6z;

    invoke-direct {v3, v0}, LX/C6z;-><init>(LX/3BO;)V

    .line 528114
    sget-object v4, LX/3BO;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C6y;

    .line 528115
    if-nez v4, :cond_0

    .line 528116
    new-instance v4, LX/C6y;

    invoke-direct {v4}, LX/C6y;-><init>()V

    .line 528117
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C6y;->a$redex0(LX/C6y;LX/1De;IILX/C6z;)V

    .line 528118
    move-object v3, v4

    .line 528119
    move-object v2, v3

    .line 528120
    move-object v0, v2

    .line 528121
    iget-object v2, v0, LX/C6y;->a:LX/C6z;

    iput-object p2, v2, LX/C6z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 528122
    iget-object v2, v0, LX/C6y;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 528123
    move-object v0, v0

    .line 528124
    iget-object v2, v0, LX/C6y;->a:LX/C6z;

    iput-object p3, v2, LX/C6z;->b:LX/1Pm;

    .line 528125
    iget-object v2, v0, LX/C6y;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 528126
    move-object v0, v0

    .line 528127
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 528128
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;
    .locals 7

    .prologue
    .line 528129
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;

    monitor-enter v1

    .line 528130
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528131
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528132
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528133
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528134
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/3BO;->a(LX/0QB;)LX/3BO;

    move-result-object v6

    check-cast v6, LX/3BO;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/1V0;LX/3BO;)V

    .line 528135
    move-object v0, p0

    .line 528136
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528137
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528138
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 528140
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 528141
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 528142
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 528143
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->d:LX/0ad;

    sget-short v1, LX/1Dd;->i:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->g:Ljava/lang/Boolean;

    .line 528144
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 528145
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 528146
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
