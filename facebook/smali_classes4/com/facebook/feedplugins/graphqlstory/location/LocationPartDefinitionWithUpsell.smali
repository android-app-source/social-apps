.class public Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

.field private final c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

.field private final d:LX/3BN;

.field private final e:LX/3BI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 527921
    new-instance v0, LX/3BJ;

    invoke-direct {v0}, LX/3BJ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;LX/3BN;LX/3BI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527868
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 527869
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->b:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    .line 527870
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    .line 527871
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    .line 527872
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->e:LX/3BI;

    .line 527873
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;
    .locals 7

    .prologue
    .line 527910
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;

    monitor-enter v1

    .line 527911
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527912
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527915
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    invoke-static {v0}, LX/3BN;->b(LX/0QB;)LX/3BN;

    move-result-object v5

    check-cast v5, LX/3BN;

    invoke-static {v0}, LX/3BI;->a(LX/0QB;)LX/3BI;

    move-result-object v6

    check-cast v6, LX/3BI;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;-><init>(Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;LX/3BN;LX/3BI;)V

    .line 527916
    move-object v0, p0

    .line 527917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 527909
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 527890
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 527891
    const v0, 0x7f0d2b07

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->b:Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527892
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 527893
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 527894
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 527895
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    .line 527896
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 527897
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v0, v1}, LX/3BN;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    .line 527898
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 527899
    const-string v2, "rexUpsellOnCityCheckin"

    invoke-static {v0, v1, v2}, LX/3BN;->a(LX/3BN;LX/1PT;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    move-object v0, v2

    .line 527900
    :goto_0
    const v1, 0x7f0d2b08

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchCallToActionAttachmentPartDefinition;

    new-instance v3, LX/BbR;

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    .line 527901
    iget-object v5, v4, LX/3BN;->a:Landroid/content/Context;

    const p3, 0x7f0829e0

    invoke-virtual {v5, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 527902
    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    .line 527903
    iget-object p0, v5, LX/3BN;->e:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_1

    iget-object p0, v5, LX/3BN;->a:Landroid/content/Context;

    const p3, 0x7f0829df

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_1
    move-object v5, p0

    .line 527904
    invoke-direct {v3, p2, v0, v4, v5}, LX/BbR;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527905
    const/4 v0, 0x0

    return-object v0

    .line 527906
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 527907
    const-string v2, "rexUpsellOnTravelStory"

    invoke-static {v0, v1, v2}, LX/3BN;->a(LX/3BN;LX/1PT;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    move-object v0, v2

    .line 527908
    goto :goto_0

    :cond_1
    iget-object p0, v5, LX/3BN;->a:Landroid/content/Context;

    const p3, 0x7f0829de

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 527874
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 527875
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 527876
    if-eqz v0, :cond_0

    .line 527877
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 527878
    if-nez v1, :cond_1

    .line 527879
    :cond_0
    :goto_0
    return v4

    .line 527880
    :cond_1
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 527881
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 527882
    invoke-static {v0}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    const/4 v2, 0x0

    .line 527883
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v1, v0}, LX/3BN;->b(LX/3BN;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v1, LX/3BN;->d:LX/0ad;

    sget-short v6, LX/5HH;->o:S

    invoke-interface {v5, v6, v2}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x1

    :cond_2
    move v1, v2

    .line 527884
    if-eqz v1, :cond_4

    move v2, v3

    .line 527885
    :goto_1
    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->d:LX/3BN;

    .line 527886
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 527887
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5, v0, v1}, LX/3BN;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    .line 527888
    if-nez v2, :cond_3

    if-eqz v0, :cond_0

    :cond_3
    move v4, v3

    goto :goto_0

    :cond_4
    move v2, v4

    .line 527889
    goto :goto_1
.end method
