.class public Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Lcom/facebook/analytics/logger/HoneyClientEvent;",
        "LX/1Pf;",
        "LX/C71;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/17V;

.field private final e:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yH;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

.field private final h:LX/3BI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x41000000    # 8.0f

    .line 527591
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    .line 527592
    iput v1, v0, LX/1UY;->b:F

    .line 527593
    move-object v0, v0

    .line 527594
    iput v1, v0, LX/1UY;->c:F

    .line 527595
    move-object v0, v0

    .line 527596
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->b:LX/1Ua;

    .line 527597
    new-instance v0, LX/2vC;

    invoke-direct {v0}, LX/2vC;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17V;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;LX/0Or;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/3BI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/17V;",
            "Lcom/facebook/feedplugins/base/TextLinkPartDefinition;",
            "LX/0Or",
            "<",
            "LX/0yH;",
            ">;",
            "Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;",
            "LX/3BI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527583
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 527584
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 527585
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->d:LX/17V;

    .line 527586
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->e:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 527587
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->f:LX/0Or;

    .line 527588
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->g:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    .line 527589
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->h:LX/3BI;

    .line 527590
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;
    .locals 10

    .prologue
    .line 527572
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;

    monitor-enter v1

    .line 527573
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527574
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527577
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v5

    check-cast v5, LX/17V;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    const/16 v7, 0x13ba

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    invoke-static {v0}, LX/3BI;->a(LX/0QB;)LX/3BI;

    move-result-object v9

    check-cast v9, LX/3BI;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17V;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;LX/0Or;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/3BI;)V

    .line 527578
    move-object v0, v3

    .line 527579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 2

    .prologue
    .line 527598
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 527599
    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527600
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 527601
    :goto_0
    return-object v0

    .line 527602
    :cond_0
    invoke-static {p0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    .line 527604
    :cond_1
    invoke-static {p0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 527605
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    .line 527606
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 527571
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 527561
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 527562
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 527563
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 527564
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 527565
    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    .line 527566
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->b:LX/1Ua;

    invoke-direct {v4, v1, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 527567
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->e:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v3, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 527568
    const v3, 0x7f0d116c

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->g:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    new-instance v5, LX/31T;

    const/4 v6, 0x1

    invoke-direct {v5, v0, v2, v6}, LX/31T;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;Z)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527569
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->d:LX/17V;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    const-string v6, "native_newsfeed"

    invoke-virtual {v0, v3, v4, v5, v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    move-object v0, v0

    .line 527570
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7a20e966

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527555
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    check-cast p3, LX/1Pf;

    check-cast p4, LX/C71;

    .line 527556
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 527557
    iget-object v1, p4, LX/C71;->a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    move-object v4, v1

    .line 527558
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 527559
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    invoke-virtual {v4, v2, v1, p2, p3}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1Pf;)V

    .line 527560
    const/16 v1, 0x1f

    const v2, 0x47c3ecbb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 527554
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yH;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method
