.class public Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Lcom/facebook/analytics/logger/HoneyClientEvent;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static p:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final e:LX/17V;

.field private final f:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

.field private final g:Lcom/facebook/maps/rows/MapPartDefinition;

.field private final h:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final l:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

.field public final m:LX/3BI;

.field private final n:LX/0ad;

.field private final o:LX/3BL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 527986
    new-instance v0, LX/3BK;

    invoke-direct {v0}, LX/3BK;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17V;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/3BI;LX/0ad;LX/3BL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/17V;",
            "Lcom/facebook/feedplugins/base/TextLinkPartDefinition;",
            "Lcom/facebook/maps/rows/MapPartDefinition;",
            "Lcom/facebook/maps/rows/PrefetchMapPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;",
            "LX/3BI;",
            "LX/0ad;",
            "LX/3BL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527970
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 527971
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->c:Landroid/content/Context;

    .line 527972
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->b:LX/0Ot;

    .line 527973
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 527974
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->e:LX/17V;

    .line 527975
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->f:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 527976
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->g:Lcom/facebook/maps/rows/MapPartDefinition;

    .line 527977
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->h:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    .line 527978
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->i:LX/0Ot;

    .line 527979
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->j:LX/0Ot;

    .line 527980
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 527981
    iput-object p11, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->l:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    .line 527982
    iput-object p12, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->m:LX/3BI;

    .line 527983
    iput-object p13, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->n:LX/0ad;

    .line 527984
    iput-object p14, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->o:LX/3BL;

    .line 527985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;
    .locals 3

    .prologue
    .line 527960
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    monitor-enter v1

    .line 527961
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527962
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527963
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527964
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527965
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527966
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;
    .locals 15

    .prologue
    .line 527968
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xbc

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {p0}, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-static {p0}, Lcom/facebook/maps/rows/MapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/MapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/maps/rows/MapPartDefinition;

    invoke-static {p0}, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    const/16 v8, 0x97

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x262e

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {p0}, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    invoke-static {p0}, LX/3BI;->a(LX/0QB;)LX/3BI;

    move-result-object v12

    check-cast v12, LX/3BI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static {p0}, LX/3BL;->a(LX/0QB;)LX/3BL;

    move-result-object v14

    check-cast v14, LX/3BL;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17V;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/3BI;LX/0ad;LX/3BL;)V

    .line 527969
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 527959
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 527933
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    .line 527934
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 527935
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 527936
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 527937
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->m:LX/3BI;

    invoke-static {v1, v0}, LX/3fQ;->a(LX/3BI;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v5

    .line 527938
    new-instance v1, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    const v6, 0x7f020a45

    const/4 v7, -0x1

    invoke-direct {v1, v4, v3, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 527939
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v3, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 527940
    if-eqz v5, :cond_0

    .line 527941
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 527942
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v3

    .line 527943
    invoke-static {v0}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 527944
    const-string v1, "checkin_story"

    invoke-static {v0, v1}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v3

    .line 527945
    const/4 v1, 0x0

    .line 527946
    :goto_0
    iget-object v6, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->o:LX/3BL;

    invoke-virtual {v6, v4}, LX/3BL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v6

    .line 527947
    const v7, 0x3ff33333    # 1.9f

    invoke-static {v6, v7}, LX/3BL;->a(IF)I

    move-result v7

    .line 527948
    const v8, 0x7f0d1cd0

    iget-object v9, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->g:Lcom/facebook/maps/rows/MapPartDefinition;

    new-instance v10, LX/3BY;

    invoke-direct {v10, v3, v1, v6, v7}, LX/3BY;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;ZII)V

    invoke-interface {p1, v8, v9, v10}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527949
    const v1, 0x7f0d1cd0

    iget-object v8, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 527950
    new-instance v9, LX/3BZ;

    invoke-direct {v9, p0, v4, v5}, LX/3BZ;-><init>(Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)V

    move-object v9, v9

    .line 527951
    invoke-interface {p1, v1, v8, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527952
    const v1, 0x7f0d1cd0

    iget-object v8, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->h:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    new-instance v9, LX/3Ba;

    invoke-direct {v9, v4, v3, v6, v7}, LX/3Ba;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;II)V

    invoke-interface {p1, v1, v8, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527953
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->f:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 527954
    const v1, 0x7f0d116c

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->l:Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    new-instance v6, LX/31T;

    invoke-direct {v6, v0, v5, v2}, LX/31T;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;Z)V

    invoke-interface {p1, v1, v3, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 527955
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->e:LX/17V;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const-string v6, "native_newsfeed"

    invoke-virtual {v0, v1, v2, v3, v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    move-object v0, v0

    .line 527956
    return-object v0

    .line 527957
    :cond_1
    const-string v6, "checkin_story"

    invoke-static {v3, v6, v1}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    move-object v3, v1

    move v1, v2

    .line 527958
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x47a664b4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 527924
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    check-cast p3, LX/1Pf;

    check-cast p4, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;

    .line 527925
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->c:Landroid/content/Context;

    invoke-static {v1}, LX/3Bc;->a(Landroid/content/Context;)V

    .line 527926
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 527927
    iget-object v1, p4, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    move-object v4, v1

    .line 527928
    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->m:LX/3BI;

    .line 527929
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 527930
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5, v1}, LX/3fQ;->a(LX/3BI;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    invoke-virtual {v4, v2, v1, p2, p3}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1Pf;)V

    .line 527931
    const/16 v1, 0x1f

    const v2, 0x1da1a268

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 527932
    const/4 v0, 0x1

    return v0
.end method
