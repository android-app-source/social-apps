.class public Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/36b;

.field private final f:LX/1E5;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/36b;LX/1E5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499496
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499497
    iput-object p2, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 499498
    iput-object p3, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->e:LX/36b;

    .line 499499
    iput-object p4, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->f:LX/1E5;

    .line 499500
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 499475
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499476
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499477
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499478
    iget-object v1, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->e:LX/36b;

    const/4 v2, 0x0

    .line 499479
    new-instance v3, LX/C41;

    invoke-direct {v3, v1}, LX/C41;-><init>(LX/36b;)V

    .line 499480
    iget-object v4, v1, LX/36b;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C40;

    .line 499481
    if-nez v4, :cond_0

    .line 499482
    new-instance v4, LX/C40;

    invoke-direct {v4, v1}, LX/C40;-><init>(LX/36b;)V

    .line 499483
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C40;->a$redex0(LX/C40;LX/1De;IILX/C41;)V

    .line 499484
    move-object v3, v4

    .line 499485
    move-object v2, v3

    .line 499486
    move-object v1, v2

    .line 499487
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 499488
    iget-object v2, v1, LX/C40;->a:LX/C41;

    iput-object v0, v2, LX/C41;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499489
    iget-object v2, v1, LX/C40;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 499490
    move-object v0, v1

    .line 499491
    iget-object v1, v0, LX/C40;->a:LX/C41;

    iput-object p3, v1, LX/C41;->b:LX/1Pf;

    .line 499492
    iget-object v1, v0, LX/C40;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499493
    move-object v0, v0

    .line 499494
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499495
    iget-object v1, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;
    .locals 7

    .prologue
    .line 499464
    const-class v1, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499465
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499466
    sput-object v2, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499467
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499468
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499469
    new-instance p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/36b;->a(LX/0QB;)LX/36b;

    move-result-object v5

    check-cast v5, LX/36b;

    invoke-static {v0}, LX/1E5;->b(LX/0QB;)LX/1E5;

    move-result-object v6

    check-cast v6, LX/1E5;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/36b;LX/1E5;)V

    .line 499470
    move-object v0, p0

    .line 499471
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499472
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499473
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499474
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499501
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499463
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499454
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499455
    iget-object v0, p0, Lcom/facebook/feedplugins/facecast/CreateLiveVideoCallToActionComponentPartDefinition;->f:LX/1E5;

    invoke-virtual {v0}, LX/1E5;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 499456
    :goto_0
    return v0

    .line 499457
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499458
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499459
    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 499460
    goto :goto_0

    .line 499461
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499462
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x10e1b97c

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 499452
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499453
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
