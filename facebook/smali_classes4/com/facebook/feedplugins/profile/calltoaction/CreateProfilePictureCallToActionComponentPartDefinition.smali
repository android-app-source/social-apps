.class public Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/36Z;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/36Z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499242
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499243
    iput-object p2, p0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 499244
    iput-object p3, p0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->e:LX/36Z;

    .line 499245
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 499246
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->e:LX/36Z;

    const/4 v1, 0x0

    .line 499247
    new-instance v2, LX/CAV;

    invoke-direct {v2, v0}, LX/CAV;-><init>(LX/36Z;)V

    .line 499248
    iget-object v3, v0, LX/36Z;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CAU;

    .line 499249
    if-nez v3, :cond_0

    .line 499250
    new-instance v3, LX/CAU;

    invoke-direct {v3, v0}, LX/CAU;-><init>(LX/36Z;)V

    .line 499251
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CAU;->a$redex0(LX/CAU;LX/1De;IILX/CAV;)V

    .line 499252
    move-object v2, v3

    .line 499253
    move-object v1, v2

    .line 499254
    move-object v0, v1

    .line 499255
    iget-object v1, v0, LX/CAU;->a:LX/CAV;

    iput-object p2, v1, LX/CAV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499256
    iget-object v1, v0, LX/CAU;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499257
    move-object v0, v0

    .line 499258
    iget-object v1, v0, LX/CAU;->a:LX/CAV;

    iput-object p3, v1, LX/CAV;->b:LX/1Pp;

    .line 499259
    iget-object v1, v0, LX/CAU;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499260
    move-object v0, v0

    .line 499261
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499262
    iget-object v1, p0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 499263
    const-class v1, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499264
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499265
    sput-object v2, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499266
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499267
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499268
    new-instance p0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/36Z;->a(LX/0QB;)LX/36Z;

    move-result-object v5

    check-cast v5, LX/36Z;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/36Z;)V

    .line 499269
    move-object v0, p0

    .line 499270
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499271
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499272
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499274
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499275
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/profile/calltoaction/CreateProfilePictureCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499276
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499277
    const/4 v1, 0x0

    .line 499278
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499279
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499280
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 499281
    :goto_0
    move v0, v0

    .line 499282
    return v0

    .line 499283
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499284
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499285
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 499286
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const p0, 0x5afb6c65

    if-ne v2, p0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 499287
    goto :goto_0

    .line 499288
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v0

    .line 499289
    if-nez v0, :cond_4

    move v0, v1

    .line 499290
    goto :goto_0

    .line 499291
    :cond_4
    sget-object v2, LX/CAW;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 499292
    goto :goto_0

    .line 499293
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 499294
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499295
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
