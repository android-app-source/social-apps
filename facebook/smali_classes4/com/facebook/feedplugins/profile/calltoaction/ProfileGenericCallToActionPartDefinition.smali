.class public Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499418
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 499419
    iput-object p1, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->a:LX/0Ot;

    .line 499420
    iput-object p3, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->b:LX/0Ot;

    .line 499421
    iput-object p4, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->c:LX/0Ot;

    .line 499422
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;
    .locals 7

    .prologue
    .line 499423
    const-class v1, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    monitor-enter v1

    .line 499424
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499425
    sput-object v2, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499426
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499427
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499428
    new-instance v3, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    const/16 v4, 0x871

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xc49

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;-><init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 499429
    move-object v0, v3

    .line 499430
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499431
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499432
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499434
    sget-object v0, LX/3WJ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 499435
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 499436
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499437
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499438
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 499439
    new-instance p3, LX/CAf;

    invoke-direct {p3, p0, v1}, LX/CAf;-><init>(Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    move-object v0, p3

    .line 499440
    invoke-static {p2, v2, v0, v2, v2}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)LX/C2O;

    move-result-object v1

    .line 499441
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 499442
    return-object v2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 499443
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499444
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499445
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499446
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 499447
    :goto_0
    return v0

    .line 499448
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499449
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, -0x4b252c60

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 499450
    goto :goto_0

    .line 499451
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
