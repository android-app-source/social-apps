.class public Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field public final b:LX/0i4;

.field private final c:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/17T;

.field public final f:Landroid/content/Context;

.field public final g:Landroid/content/pm/PackageManager;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/content/pm/PackageInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 491980
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0i4;Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/17T;Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491981
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 491982
    iput-object p1, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->b:LX/0i4;

    .line 491983
    iput-object p2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    .line 491984
    iput-object p3, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 491985
    iput-object p4, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->e:LX/17T;

    .line 491986
    iput-object p5, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->f:Landroid/content/Context;

    .line 491987
    iput-object p6, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->g:Landroid/content/pm/PackageManager;

    .line 491988
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    .locals 10

    .prologue
    .line 491989
    const-class v1, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    monitor-enter v1

    .line 491990
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491991
    sput-object v2, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491992
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491993
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491994
    new-instance v3, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    const-class v4, LX/0i4;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0i4;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v7

    check-cast v7, LX/17T;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v9

    check-cast v9, Landroid/content/pm/PackageManager;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;-><init>(LX/0i4;Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/17T;Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    .line 491995
    move-object v0, v3

    .line 491996
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491997
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491998
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 492000
    new-instance v0, Landroid/content/Intent;

    const-string v1, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "me.msqrd.android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 492001
    sget-object v0, LX/3WJ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 492002
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 492003
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 492004
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 492005
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 492006
    new-instance v0, LX/CAc;

    invoke-direct {v0, p0, v4}, LX/CAc;-><init>(Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    move-object v2, v0

    .line 492007
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    :goto_1
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v6

    :goto_2
    move-object v0, p2

    move-object v4, v1

    invoke-static/range {v0 .. v6}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Ljava/lang/String;)LX/C2O;

    move-result-object v0

    .line 492008
    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 492009
    return-object v1

    .line 492010
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 492011
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 492012
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 492013
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 492014
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 492015
    :goto_0
    return v0

    .line 492016
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 492017
    if-nez v0, :cond_2

    move v0, v1

    .line 492018
    goto :goto_0

    .line 492019
    :cond_2
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 492020
    if-nez v0, :cond_3

    move v0, v1

    .line 492021
    goto :goto_0

    .line 492022
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 492023
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x352bf2b1

    if-eq v2, v3, :cond_5

    :cond_4
    move v0, v1

    .line 492024
    goto :goto_0

    .line 492025
    :cond_5
    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    if-eqz v2, :cond_8

    .line 492026
    :goto_1
    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->i:Landroid/content/pm/PackageInfo;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->i:Landroid/content/pm/PackageInfo;

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0x23

    if-lt v2, v3, :cond_6

    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    move v0, v1

    .line 492027
    goto :goto_0

    .line 492028
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 492029
    :cond_8
    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->f:Landroid/content/Context;

    const-string v3, "me.msqrd.android"

    const/16 p1, 0x40

    invoke-static {v2, v3, p1}, LX/05U;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->i:Landroid/content/pm/PackageInfo;

    .line 492030
    iget-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->g:Landroid/content/pm/PackageManager;

    invoke-static {}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->d()Landroid/content/Intent;

    move-result-object v3

    const/high16 p1, 0x10000

    invoke-virtual {v2, v3, p1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    goto/16 :goto_1
.end method
