.class public Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/354;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/354",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/355;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/355",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/354;LX/355;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491929
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 491930
    iput-object p2, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 491931
    iput-object p3, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->e:LX/354;

    .line 491932
    iput-object p4, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->f:LX/355;

    .line 491933
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 491963
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->e:LX/354;

    const/4 v1, 0x0

    .line 491964
    new-instance v2, LX/CAh;

    invoke-direct {v2, v0}, LX/CAh;-><init>(LX/354;)V

    .line 491965
    iget-object v3, v0, LX/354;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CAg;

    .line 491966
    if-nez v3, :cond_0

    .line 491967
    new-instance v3, LX/CAg;

    invoke-direct {v3, v0}, LX/CAg;-><init>(LX/354;)V

    .line 491968
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CAg;->a$redex0(LX/CAg;LX/1De;IILX/CAh;)V

    .line 491969
    move-object v2, v3

    .line 491970
    move-object v1, v2

    .line 491971
    move-object v0, v1

    .line 491972
    iget-object v1, v0, LX/CAg;->a:LX/CAh;

    iput-object p3, v1, LX/CAh;->b:LX/1Pp;

    .line 491973
    iget-object v1, v0, LX/CAg;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 491974
    move-object v0, v0

    .line 491975
    iget-object v1, v0, LX/CAg;->a:LX/CAh;

    iput-object p2, v1, LX/CAh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491976
    iget-object v1, v0, LX/CAg;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 491977
    move-object v0, v0

    .line 491978
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 491979
    iget-object v1, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->m:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    .locals 7

    .prologue
    .line 491952
    const-class v1, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 491953
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491954
    sput-object v2, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491955
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491956
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491957
    new-instance p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/354;->a(LX/0QB;)LX/354;

    move-result-object v5

    check-cast v5, LX/354;

    invoke-static {v0}, LX/355;->a(LX/0QB;)LX/355;

    move-result-object v6

    check-cast v6, LX/355;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/354;LX/355;)V

    .line 491958
    move-object v0, p0

    .line 491959
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491960
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491961
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 491951
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 491950
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 491936
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491937
    iget-object v0, p0, Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;->f:LX/355;

    const/4 v3, 0x0

    .line 491938
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 491939
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 491940
    iget-object v2, v0, LX/355;->f:LX/356;

    invoke-virtual {v2}, LX/356;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v1, v3

    .line 491941
    :goto_0
    move v0, v1

    .line 491942
    return v0

    .line 491943
    :cond_1
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 491944
    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 491945
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 491946
    :goto_1
    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 491947
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const p0, -0x3d5832f5

    if-ne v2, p0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    .line 491948
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move v1, v3

    .line 491949
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 491934
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491935
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
