.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/2dy;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field public final b:LX/2e1;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/2dq;

.field public final e:LX/1LV;

.field public final f:Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

.field public final g:Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

.field public final h:LX/2e2;

.field private final i:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

.field public final j:Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

.field public final k:Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

.field private final l:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final m:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444735
    const-class v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;LX/1LV;Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;LX/2e2;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0ad;LX/2e1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444721
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 444722
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 444723
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->d:LX/2dq;

    .line 444724
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->e:LX/1LV;

    .line 444725
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->f:Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    .line 444726
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->g:Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    .line 444727
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->h:LX/2e2;

    .line 444728
    iput-object p7, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->i:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    .line 444729
    iput-object p8, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->j:Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    .line 444730
    iput-object p9, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->k:Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    .line 444731
    iput-object p10, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->l:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 444732
    iput-object p11, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->m:LX/0ad;

    .line 444733
    iput-object p12, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b:LX/2e1;

    .line 444734
    return-void
.end method

.method private a(FLX/1Ua;)LX/2eF;
    .locals 1

    .prologue
    .line 444718
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->d:LX/2dq;

    .line 444719
    const/4 p0, 0x0

    invoke-virtual {v0, p1, p2, p0}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object p0

    move-object v0, p0

    .line 444720
    return-object v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;LX/1Ps;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "LX/2dx;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 444683
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 444684
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v2

    .line 444685
    new-instance v0, LX/2ek;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/2ek;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;LX/1Ps;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    .locals 3

    .prologue
    .line 444710
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    monitor-enter v1

    .line 444711
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444712
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444713
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444714
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444715
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444716
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;ILX/0ad;)Z
    .locals 2

    .prologue
    .line 444707
    sget v0, LX/2dw;->c:I

    const/4 v1, -0x1

    invoke-interface {p2, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 444708
    :cond_0
    const/4 v0, 0x0

    .line 444709
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    .locals 13

    .prologue
    .line 444705
    new-instance v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v2

    check-cast v2, LX/2dq;

    invoke-static {p0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v3

    check-cast v3, LX/1LV;

    invoke-static {p0}, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    invoke-static {p0}, LX/2e2;->a(LX/0QB;)LX/2e2;

    move-result-object v6

    check-cast v6, LX/2e2;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/2e1;->a(LX/0QB;)LX/2e1;

    move-result-object v12

    check-cast v12, LX/2e1;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;LX/1LV;Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;LX/2e2;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0ad;LX/2e1;)V

    .line 444706
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444704
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 444687
    check-cast p2, LX/2dy;

    check-cast p3, LX/1Ps;

    const/4 v6, 0x0

    .line 444688
    sget-object v1, LX/2eF;->a:LX/1Ua;

    .line 444689
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    iget-object v3, p2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v2, v3, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 444690
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->i:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    iget-object v2, p2, LX/2dy;->b:LX/2dx;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 444691
    iget-object v0, p2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444692
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v2

    .line 444693
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 444694
    iget-object v7, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->l:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    const/high16 v2, 0x43980000    # 304.0f

    invoke-direct {p0, v2, v1}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(FLX/1Ua;)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->I_()I

    move-result v2

    iget-object v3, p2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/2dy;->b:LX/2dx;

    invoke-direct {p0, v3, v4, p3}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;LX/1Ps;)LX/2eJ;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 444695
    invoke-static {v5}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v6

    move v1, v6

    .line 444696
    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fa;

    .line 444697
    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 444698
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 444699
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    move-object v0, p3

    .line 444700
    check-cast v0, LX/1Pt;

    sget-object v6, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v5, v6}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 444701
    add-int/lit8 v0, v1, 0x1

    int-to-float v1, v0

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v1, v1, v5

    if-gez v1, :cond_0

    .line 444702
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 444703
    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 444686
    const/4 v0, 0x1

    return v0
.end method
