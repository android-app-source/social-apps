.class public Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/DFn;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DFn;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444982
    new-instance v0, LX/2e6;

    invoke-direct {v0}, LX/2e6;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444983
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 444984
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;
    .locals 3

    .prologue
    .line 444985
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    monitor-enter v1

    .line 444986
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444987
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444988
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444989
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 444990
    new-instance v0, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;-><init>()V

    .line 444991
    move-object v0, v0

    .line 444992
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444993
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444994
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444995
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DFn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444996
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
