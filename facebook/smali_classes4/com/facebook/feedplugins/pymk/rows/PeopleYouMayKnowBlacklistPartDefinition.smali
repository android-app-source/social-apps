.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2ex;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/23P;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/2ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ew",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/2ew;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 446016
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 446017
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->a:LX/23P;

    .line 446018
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 446019
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->c:Landroid/content/res/Resources;

    .line 446020
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    .line 446021
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 446022
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->f:LX/2ew;

    .line 446023
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
    .locals 10

    .prologue
    .line 446024
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    monitor-enter v1

    .line 446025
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446026
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446027
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446028
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446029
    new-instance v3, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, LX/2ew;->a(LX/0QB;)LX/2ew;

    move-result-object v9

    check-cast v9, LX/2ew;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;-><init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/2ew;)V

    .line 446030
    move-object v0, v3

    .line 446031
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446032
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446033
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 446035
    check-cast p2, LX/2ex;

    check-cast p3, LX/1Pq;

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 446036
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/2ex;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v3, p2, LX/2ex;->b:LX/1Fa;

    .line 446037
    new-instance v4, LX/2f7;

    invoke-direct {v4, p0, v1, v3, p3}, LX/2f7;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pq;)V

    move-object v1, v4

    .line 446038
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446039
    iget-object v7, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    new-instance v0, LX/2f5;

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->a:LX/23P;

    iget-object v3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f08186b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    const v3, 0x7f02081c

    iget-object v4, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->c:Landroid/content/res/Resources;

    const v6, 0x7f0a00a3

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v6, 0x7f020b0b

    invoke-direct/range {v0 .. v6}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446040
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    iget-object v1, p2, LX/2ex;->c:LX/2ep;

    iget-object v1, v1, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v3, :cond_0

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 446041
    return-object v2

    .line 446042
    :cond_0
    const/16 v5, 0x8

    goto :goto_0
.end method
