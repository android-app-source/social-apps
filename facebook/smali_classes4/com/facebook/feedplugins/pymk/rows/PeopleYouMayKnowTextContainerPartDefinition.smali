.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2er;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/2eq;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2eq;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445768
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 445769
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 445770
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->b:LX/2eq;

    .line 445771
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 445772
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;
    .locals 6

    .prologue
    .line 445773
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    monitor-enter v1

    .line 445774
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445775
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445776
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445777
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445778
    new-instance p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/2eq;->a(LX/0QB;)LX/2eq;

    move-result-object v4

    check-cast v4, LX/2eq;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2eq;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 445779
    move-object v0, p0

    .line 445780
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445781
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445782
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 445784
    check-cast p2, LX/2er;

    .line 445785
    const v0, 0x7f0d1147

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->b:LX/2eq;

    iget-object v3, p2, LX/2er;->c:Ljava/lang/String;

    iget-object v4, p2, LX/2er;->e:LX/2ep;

    invoke-virtual {v2, v3, v4}, LX/2eq;->a(Ljava/lang/String;LX/2ep;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 445786
    const v0, 0x7f0d1146

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/2er;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 445787
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/2er;->a:Ljava/lang/String;

    iget-object v2, p2, LX/2er;->d:Ljava/lang/String;

    iget-object v3, p2, LX/2er;->b:Ljava/lang/String;

    iget-object v4, p2, LX/2er;->f:LX/162;

    .line 445788
    new-instance v5, LX/2f3;

    move-object v6, p0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, LX/2f3;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)V

    move-object v1, v5

    .line 445789
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445790
    const/4 v0, 0x0

    return-object v0
.end method
