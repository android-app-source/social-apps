.class public Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/Context;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/friendingcommon/FriendingCommonHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444580
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 444581
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->a:LX/0Ot;

    .line 444582
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->b:LX/0Ot;

    .line 444583
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->c:LX/0Ot;

    .line 444584
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->f:Landroid/content/Context;

    .line 444585
    iput-object p7, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->g:LX/0Ot;

    .line 444586
    iput-object p8, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->h:LX/0ad;

    .line 444587
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->d:LX/0Ot;

    .line 444588
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->e:LX/0Ot;

    .line 444589
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;
    .locals 12

    .prologue
    .line 444590
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;

    monitor-enter v1

    .line 444591
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444592
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444593
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444594
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444595
    new-instance v3, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;

    const/16 v4, 0x8b8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x9cf

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x9ce

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x9d6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x20dd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    const/16 v10, 0xbd2

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/0Ot;LX/0ad;)V

    .line 444596
    move-object v0, v3

    .line 444597
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444598
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444599
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 444601
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 444602
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 444603
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 444604
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->h:LX/0ad;

    sget-short v2, LX/2dw;->d:S

    invoke-interface {v1, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 444605
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 444606
    :cond_0
    :goto_0
    return-object v6

    .line 444607
    :cond_1
    new-instance v2, LX/2dx;

    invoke-direct {v2}, LX/2dx;-><init>()V

    .line 444608
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 444609
    new-instance v3, LX/2dy;

    invoke-direct {v3, p2, v2}, LX/2dy;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;)V

    .line 444610
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 444611
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->h:LX/0ad;

    sget-short v2, LX/2dw;->a:S

    invoke-interface {v1, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 444612
    if-eqz v2, :cond_2

    .line 444613
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 444614
    :cond_2
    new-instance v1, LX/2e7;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/2e8;

    invoke-direct {v4, v5, v6}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-direct {v1, v3, v4}, LX/2e7;-><init>(Ljava/lang/String;LX/2e8;)V

    .line 444615
    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2e8;

    const/4 v3, 0x0

    .line 444616
    iget-boolean v1, v0, LX/2e8;->a:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/2e8;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    if-nez v1, :cond_5

    :cond_3
    move v1, v3

    .line 444617
    :goto_1
    move v0, v1

    .line 444618
    if-eqz v0, :cond_4

    .line 444619
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 444620
    :cond_4
    if-nez v2, :cond_0

    .line 444621
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 444622
    :cond_5
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iA;

    iget-object v4, v0, LX/2e8;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v5, LX/3ka;

    invoke-virtual {v1, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/13D;

    .line 444623
    if-nez v1, :cond_6

    const/4 v1, 0x0

    .line 444624
    :goto_2
    if-nez v1, :cond_7

    move v1, v3

    .line 444625
    goto :goto_1

    .line 444626
    :cond_6
    iget-object v4, p0, Lcom/facebook/feedplugins/pymk/rows/PaginatedPeopleYouMayKnowPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v1, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2

    .line 444627
    :cond_7
    const-string v4, "qp_definition"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 444628
    invoke-virtual {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    .line 444629
    sget-object v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->FEED_PYMK:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-ne v1, v4, :cond_8

    const/4 v1, 0x1

    goto :goto_1

    :cond_8
    move v1, v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 444630
    const/4 v0, 0x1

    return v0
.end method
