.class public Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/2el;",
        "LX/2f0",
        "<TE;>;TE;",
        "LX/2fB;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/2fB;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2e2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2do;

.field private final d:LX/0ad;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444736
    new-instance v0, LX/2dz;

    invoke-direct {v0}, LX/2dz;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/2do;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2e2;",
            ">;",
            "LX/2do;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444737
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 444738
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->b:LX/0Ot;

    .line 444739
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->c:LX/2do;

    .line 444740
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->d:LX/0ad;

    .line 444741
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->e:LX/0Ot;

    .line 444742
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->f:LX/0Ot;

    .line 444743
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->g:LX/0Ot;

    .line 444744
    iput-object p7, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->h:LX/0Ot;

    .line 444745
    iput-object p8, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->i:LX/0Ot;

    .line 444746
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;
    .locals 12

    .prologue
    .line 444747
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    monitor-enter v1

    .line 444748
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444749
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444750
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444751
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444752
    new-instance v3, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    const/16 v4, 0x9c6

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v5

    check-cast v5, LX/2do;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    const/16 v7, 0x9cd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x9d0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x9cc

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x8b9

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xafc

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;-><init>(LX/0Ot;LX/2do;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 444753
    move-object v0, v3

    .line 444754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/2fB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444758
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 444759
    check-cast p2, LX/2el;

    check-cast p3, LX/1Pc;

    .line 444760
    new-instance v2, LX/2en;

    iget-object v0, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v1}, LX/2eo;->a(LX/1Fa;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-direct {v2, v0, v1}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v0, p3

    .line 444761
    check-cast v0, LX/1Pr;

    iget-object v1, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444762
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 444763
    check-cast v1, LX/0jW;

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2ep;

    .line 444764
    const v8, 0x7f0d1145

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/1Nt;

    new-instance v0, LX/2er;

    iget-object v1, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v2}, LX/2es;->c(LX/1Fa;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v3}, LX/2es;->b(LX/1Fa;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v4}, LX/2es;->a(LX/1Fa;)Ljava/lang/String;

    move-result-object v4

    iget-object v9, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    iget-object v6, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444765
    iget-object v10, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v10

    .line 444766
    check-cast v6, LX/16h;

    invoke-static {v9, v6}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/2er;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ep;LX/162;)V

    invoke-interface {p1, v8, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444767
    const v2, 0x7f0d1148

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/2ev;

    iget-object v1, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444768
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 444769
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v4, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    iget-object v6, p2, LX/2el;->c:LX/2dx;

    invoke-direct {v3, v1, v4, v5, v6}, LX/2ev;-><init>(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/2dx;)V

    invoke-interface {p1, v2, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444770
    const v2, 0x7f0d1149

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/2ex;

    iget-object v1, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444771
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 444772
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v4, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-direct {v3, v1, v4, v5}, LX/2ex;-><init>(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;)V

    invoke-interface {p1, v2, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444773
    const v1, 0x7f0d1144

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/2ey;

    iget-object v3, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v3}, LX/2es;->a(LX/1Fa;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v4}, LX/2es;->c(LX/1Fa;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2ey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444774
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->d:LX/0ad;

    sget-short v1, LX/2ez;->l:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444775
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    iget-object v2, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    iget-object v0, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 444776
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, LX/DEy;

    iget-object v3, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    check-cast p3, LX/1Pr;

    invoke-direct {v2, v3, v4, p3}, LX/DEy;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/1Pr;)V

    iget-object v3, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 444777
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 444778
    :cond_0
    new-instance v0, LX/2f0;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p2, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444779
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 444780
    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p2, LX/2el;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2, v3}, LX/2f0;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    .line 444781
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->c:LX/2do;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x38a6a207

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 444782
    check-cast p1, LX/2el;

    .line 444783
    iget-object v1, p1, LX/2el;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 444784
    iget-object p1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, p1

    .line 444785
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 444786
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->I_()I

    move-result p1

    .line 444787
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2e2;

    invoke-virtual {v2, v1, p1}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 444788
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x2963608

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 444789
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 444790
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2e2;

    invoke-virtual {v2, v1}, LX/2e2;->b(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    goto :goto_0

    .line 444791
    :cond_1
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2e2;

    invoke-virtual {v2, v1}, LX/2e2;->c(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    goto :goto_0
.end method
