.class public Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DFm;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DFm;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final d:Landroid/content/Context;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/17Y;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2e1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444794
    new-instance v0, LX/2e0;

    invoke-direct {v0}, LX/2e0;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/2e1;LX/0Or;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/2e1;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444795
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 444796
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 444797
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 444798
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->d:Landroid/content/Context;

    .line 444799
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 444800
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->f:LX/17Y;

    .line 444801
    iput-object p7, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->g:LX/0Or;

    .line 444802
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->h:LX/2e1;

    .line 444803
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;
    .locals 11

    .prologue
    .line 444804
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    monitor-enter v1

    .line 444805
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444806
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444807
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444808
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444809
    new-instance v3, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {v0}, LX/2e1;->a(LX/0QB;)LX/2e1;

    move-result-object v9

    check-cast v9, LX/2e1;

    const/16 v10, 0x12cb

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/2e1;LX/0Or;)V

    .line 444810
    move-object v0, v3

    .line 444811
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444812
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444813
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444814
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2e1;LX/17Y;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 4

    .prologue
    .line 444815
    iget-object v0, p0, LX/2e1;->a:LX/0Zb;

    const-string v1, "pymk_ccu_promo_card_clicked"

    invoke-static {p0, v1}, LX/2e1;->a(LX/2e1;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 444816
    sget-object v0, LX/0ax;->ff:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, LX/89v;->NEWS_FEED_PYMK:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "INLINE_CARD"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 444817
    invoke-interface {p1, p2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 444818
    if-eqz v0, :cond_0

    .line 444819
    invoke-interface {p3, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 444820
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DFm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444821
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 444822
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 444823
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->d:Landroid/content/Context;

    const v1, 0x7f08186e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 444824
    const v1, 0x7f0d1140

    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444825
    const v0, 0x7f0d1142

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 444826
    new-instance v2, LX/DEv;

    invoke-direct {v2, p0}, LX/DEv;-><init>(Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;)V

    move-object v2, v2

    .line 444827
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444828
    const/4 v0, 0x0

    return-object v0
.end method
