.class public Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/DFo;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DFo;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 444956
    new-instance v0, LX/2e5;

    invoke-direct {v0}, LX/2e5;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444973
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 444974
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 444975
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 444976
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;
    .locals 5

    .prologue
    .line 444962
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    monitor-enter v1

    .line 444963
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444964
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444965
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444966
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444967
    new-instance p0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 444968
    move-object v0, p0

    .line 444969
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444970
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444971
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/View;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 2

    .prologue
    .line 444977
    sget-object v0, LX/0ax;->cV:Ljava/lang/String;

    sget-object v1, LX/5Oz;->FEED_PYMK:LX/5Oz;

    invoke-virtual {v1}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 444978
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 444979
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DFo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444961
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 444957
    const v0, 0x7f0d27af

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 444958
    new-instance v2, LX/DEz;

    invoke-direct {v2, p0}, LX/DEz;-><init>(Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;)V

    move-object v2, v2

    .line 444959
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 444960
    const/4 v0, 0x0

    return-object v0
.end method
