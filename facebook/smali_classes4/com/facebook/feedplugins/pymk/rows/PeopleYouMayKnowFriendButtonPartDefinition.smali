.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2ev;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final c:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/2et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2et",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/2et;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445877
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 445878
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 445879
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 445880
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->c:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    .line 445881
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 445882
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->e:LX/2et;

    .line 445883
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;
    .locals 9

    .prologue
    .line 445854
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;

    monitor-enter v1

    .line 445855
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445856
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445857
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445858
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445859
    new-instance v3, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, LX/2et;->a(LX/0QB;)LX/2et;

    move-result-object v8

    check-cast v8, LX/2et;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;LX/2et;)V

    .line 445860
    move-object v0, v3

    .line 445861
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445862
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445863
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 445865
    check-cast p2, LX/2ev;

    check-cast p3, LX/1Pc;

    .line 445866
    iget-object v0, p2, LX/2ev;->c:LX/2ep;

    iget-object v0, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v0}, LX/2et;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v0

    .line 445867
    if-eqz v0, :cond_0

    .line 445868
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->e:LX/2et;

    iget-object v2, p2, LX/2ev;->c:LX/2ep;

    iget-object v2, v2, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v2}, LX/2et;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/2f5;

    move-result-object v1

    .line 445869
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    iget-object v3, v1, LX/2f5;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445870
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v3, p2, LX/2ev;->c:LX/2ep;

    .line 445871
    new-instance v4, LX/2f6;

    invoke-direct {v4, p0, p2, v3, p3}, LX/2f6;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;LX/2ev;LX/2ep;LX/1Pc;)V

    move-object v3, v4

    .line 445872
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445873
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->c:Lcom/facebook/feedplugins/friendingcommon/SmartButtonPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445874
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFriendButtonPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 445875
    const/4 v0, 0x0

    return-object v0

    .line 445876
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method
