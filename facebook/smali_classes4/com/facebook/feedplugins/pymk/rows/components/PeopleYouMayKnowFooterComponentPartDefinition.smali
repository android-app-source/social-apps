.class public Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/2e9;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2e9;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445009
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 445010
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->d:LX/2e9;

    .line 445011
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->e:LX/1V0;

    .line 445012
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 445013
    new-instance v0, LX/1X6;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 445014
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->d:LX/2e9;

    invoke-virtual {v2, p1}, LX/2e9;->c(LX/1De;)LX/DFF;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v1, p1, p3, v0, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;
    .locals 6

    .prologue
    .line 445015
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;

    monitor-enter v1

    .line 445016
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 445017
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445018
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445019
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 445020
    new-instance p0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2e9;->a(LX/0QB;)LX/2e9;

    move-result-object v4

    check-cast v4, LX/2e9;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/2e9;LX/1V0;)V

    .line 445021
    move-object v0, p0

    .line 445022
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 445023
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445024
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 445025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 445026
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 445027
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymk/rows/components/PeopleYouMayKnowFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 445028
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 445029
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445030
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 445031
    check-cast v0, LX/0jW;

    return-object v0
.end method
