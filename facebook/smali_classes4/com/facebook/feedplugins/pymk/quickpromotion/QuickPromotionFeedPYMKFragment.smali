.class public Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;
.super Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;
.source ""


# instance fields
.field public a:Ljava/lang/Runnable;

.field private b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450376
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/77y;Z)V
    .locals 1

    .prologue
    .line 450372
    invoke-super {p0, p1, p2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(LX/77y;Z)V

    .line 450373
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 450374
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 450375
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 450358
    invoke-super {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/os/Bundle;)V

    .line 450359
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-object v0, v0

    .line 450360
    iput-object v0, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 450361
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1006534a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 450371
    const v1, 0x7f030f25

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x21c968a0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 450362
    invoke-super {p0, p1, p2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 450363
    const v0, 0x7f0d24bd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 450364
    const v1, 0x7f0d24be

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 450365
    const v2, 0x7f0d00bd

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 450366
    iget-object v3, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450367
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450368
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->b:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 450369
    new-instance v0, LX/DEu;

    invoke-direct {v0, p0}, LX/DEu;-><init>(Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;)V

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450370
    return-void
.end method
