.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feed/rows/views/ContentTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/Class;

.field private static j:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/03V;

.field private final g:LX/1VB;

.field public final h:LX/1EV;

.field public final i:LX/34w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496076
    const-class v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->b:Ljava/lang/Class;

    .line 496077
    new-instance v0, LX/34v;

    invoke-direct {v0}, LX/34v;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/0Or;LX/03V;LX/1VB;LX/1EV;LX/34w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;",
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/feed/rows/styling/PaddingStyleConfig;",
            "LX/1EV;",
            "LX/34w;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496119
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 496120
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 496121
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->d:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 496122
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->e:LX/0Or;

    .line 496123
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->f:LX/03V;

    .line 496124
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->g:LX/1VB;

    .line 496125
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->h:LX/1EV;

    .line 496126
    iput-object p7, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->i:LX/34w;

    .line 496127
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;
    .locals 3

    .prologue
    .line 496111
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    monitor-enter v1

    .line 496112
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496113
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496114
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496115
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496116
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496117
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496118
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;
    .locals 15

    .prologue
    .line 496106
    new-instance v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    const/16 v3, 0x6b9

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/1V9;->b(LX/0QB;)LX/1VB;

    move-result-object v5

    check-cast v5, LX/1VB;

    invoke-static {p0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v6

    check-cast v6, LX/1EV;

    .line 496107
    new-instance v8, LX/34w;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {p0}, LX/34x;->a(LX/0QB;)LX/34x;

    move-result-object v12

    check-cast v12, LX/34x;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v13

    check-cast v13, LX/0wM;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v8 .. v14}, LX/34w;-><init>(LX/0Zb;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/34x;LX/0wM;LX/0ad;)V

    .line 496108
    move-object v7, v8

    .line 496109
    check-cast v7, LX/34w;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;LX/0Or;LX/03V;LX/1VB;LX/1EV;LX/34w;)V

    .line 496110
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 496105
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 496083
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 496084
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 496085
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 496086
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 496087
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 496088
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 496089
    instance-of v0, v1, LX/37R;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 496090
    :goto_0
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 496091
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 496092
    iput-object v2, v0, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 496093
    move-object v0, v0

    .line 496094
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 496095
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->d:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v4, LX/37S;

    invoke-direct {v4, p0, v0, v1}, LX/37S;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v2, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 496096
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v2

    const/high16 v4, 0x41000000    # 8.0f

    .line 496097
    iput v4, v2, LX/1UY;->b:F

    .line 496098
    move-object v2, v2

    .line 496099
    const/high16 p0, 0x40c00000    # 6.0f

    move v4, p0

    .line 496100
    iput v4, v2, LX/1UY;->c:F

    .line 496101
    move-object v2, v2

    .line 496102
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    invoke-direct {v1, v3, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 496103
    const/4 v0, 0x0

    return-object v0

    .line 496104
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 496078
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 496079
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 496080
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 496081
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 496082
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
