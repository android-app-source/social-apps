.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/34s;

.field private final e:LX/1V0;

.field private final f:LX/1VB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/34s;LX/1V0;LX/1VB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495952
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 495953
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->d:LX/34s;

    .line 495954
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->e:LX/1V0;

    .line 495955
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->f:LX/1VB;

    .line 495956
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 495957
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->e:LX/1V0;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v3

    .line 495958
    const/high16 v5, 0x40c00000    # 6.0f

    move v4, v5

    .line 495959
    iput v4, v3, LX/1UY;->b:F

    .line 495960
    move-object v3, v3

    .line 495961
    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->d:LX/34s;

    const/4 v3, 0x0

    .line 495962
    new-instance v4, LX/37L;

    invoke-direct {v4, v2}, LX/37L;-><init>(LX/34s;)V

    .line 495963
    sget-object v5, LX/34s;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/37M;

    .line 495964
    if-nez v5, :cond_0

    .line 495965
    new-instance v5, LX/37M;

    invoke-direct {v5}, LX/37M;-><init>()V

    .line 495966
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/37M;->a$redex0(LX/37M;LX/1De;IILX/37L;)V

    .line 495967
    move-object v4, v5

    .line 495968
    move-object v3, v4

    .line 495969
    move-object v2, v3

    .line 495970
    iget-object v3, v2, LX/37M;->a:LX/37L;

    iput-object p2, v3, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 495971
    iget-object v3, v2, LX/37M;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 495972
    move-object v2, v2

    .line 495973
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;
    .locals 7

    .prologue
    .line 495974
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    monitor-enter v1

    .line 495975
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 495976
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 495977
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495978
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 495979
    new-instance p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/34s;->a(LX/0QB;)LX/34s;

    move-result-object v4

    check-cast v4, LX/34s;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1V9;->b(LX/0QB;)LX/1VB;

    move-result-object v6

    check-cast v6, LX/1VB;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;-><init>(Landroid/content/Context;LX/34s;LX/1V0;LX/1VB;)V

    .line 495980
    move-object v0, p0

    .line 495981
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 495982
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495983
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 495984
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 495985
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 495986
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 495987
    const/4 v0, 0x1

    return v0
.end method
