.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:LX/34u;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496013
    new-instance v0, LX/34t;

    invoke-direct {v0}, LX/34t;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/34u;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496014
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 496015
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 496016
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->b:Landroid/content/Context;

    .line 496017
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 496018
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->e:LX/34u;

    .line 496019
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;
    .locals 7

    .prologue
    .line 496020
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    monitor-enter v1

    .line 496021
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496022
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496023
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496024
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496025
    new-instance p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/34u;->a(LX/0QB;)LX/34u;

    move-result-object v6

    check-cast v6, LX/34u;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/34u;)V

    .line 496026
    move-object v0, p0

    .line 496027
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496028
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496029
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496030
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 496031
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 496032
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 496033
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 496034
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 496035
    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/37O;

    move-result-object v1

    .line 496036
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 496037
    iget-object v3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->e:LX/34u;

    invoke-static {v0, v1, v2, v3, v4}, LX/37Q;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/37O;Landroid/text/SpannableStringBuilder;Landroid/content/Context;LX/34u;)V

    .line 496038
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 496039
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 496040
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 496041
    const/4 v0, 0x1

    return v0
.end method
