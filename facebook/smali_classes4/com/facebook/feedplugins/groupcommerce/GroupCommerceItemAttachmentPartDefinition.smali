.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/350;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/34z;

.field private final d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

.field private final f:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

.field private final g:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;

.field private final h:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 495951
    const-class v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;LX/03V;LX/34z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 495942
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 495943
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    .line 495944
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    .line 495945
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    .line 495946
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;

    .line 495947
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    .line 495948
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->b:LX/03V;

    .line 495949
    iput-object p7, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->c:LX/34z;

    .line 495950
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)LX/37O;
    .locals 1

    .prologue
    .line 495899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495900
    sget-object v0, LX/37O;->SOLD:LX/37O;

    .line 495901
    :goto_0
    return-object v0

    .line 495902
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495903
    sget-object v0, LX/37O;->EXPIRED:LX/37O;

    goto :goto_0

    .line 495904
    :cond_1
    sget-object v0, LX/37O;->AVAILABLE:LX/37O;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 495931
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;

    monitor-enter v1

    .line 495932
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 495933
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 495934
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495935
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 495936
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const-class v10, LX/34z;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/34z;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;LX/03V;LX/34z;)V

    .line 495937
    move-object v0, v3

    .line 495938
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 495939
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495940
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 495941
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 495921
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    .line 495922
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 495923
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentTitleComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 495924
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPriceAndPickupPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 495925
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 495926
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 495927
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 495928
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->c:LX/34z;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/34z;->a(LX/1PT;)LX/31m;

    move-result-object v1

    .line 495929
    sget-object v2, LX/1EO;->GROUP:LX/1EO;

    invoke-virtual {v1, v0, v2}, LX/31m;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;)V

    .line 495930
    new-instance v0, LX/350;

    invoke-direct {v0, v1}, LX/350;-><init>(LX/31j;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 495917
    check-cast p2, LX/350;

    .line 495918
    if-eqz p2, :cond_0

    .line 495919
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/350;->a(Landroid/view/View;)V

    .line 495920
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 495909
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 495910
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 495911
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 495912
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x261131e8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 495913
    :goto_0
    if-nez v0, :cond_0

    .line 495914
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->b:LX/03V;

    sget-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Corrupt story: GraphQL missing attachment target."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 495915
    :cond_0
    return v0

    .line 495916
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 495905
    check-cast p2, LX/350;

    .line 495906
    if-eqz p2, :cond_0

    .line 495907
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/350;->b(Landroid/view/View;)V

    .line 495908
    :cond_0
    return-void
.end method
