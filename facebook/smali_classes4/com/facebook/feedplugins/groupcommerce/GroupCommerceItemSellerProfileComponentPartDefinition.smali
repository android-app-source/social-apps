.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/1EV;

.field private final g:LX/34y;

.field public final h:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496251
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/1EV;LX/34y;LX/17W;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 496252
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 496253
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->e:LX/1V0;

    .line 496254
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->f:LX/1EV;

    .line 496255
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->g:LX/34y;

    .line 496256
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->h:LX/17W;

    .line 496257
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 496258
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 496259
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->g:LX/34y;

    const/4 v2, 0x0

    .line 496260
    new-instance v3, LX/DDm;

    invoke-direct {v3, v1}, LX/DDm;-><init>(LX/34y;)V

    .line 496261
    sget-object v4, LX/34y;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DDl;

    .line 496262
    if-nez v4, :cond_0

    .line 496263
    new-instance v4, LX/DDl;

    invoke-direct {v4}, LX/DDl;-><init>()V

    .line 496264
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/DDl;->a$redex0(LX/DDl;LX/1De;IILX/DDm;)V

    .line 496265
    move-object v3, v4

    .line 496266
    move-object v2, v3

    .line 496267
    move-object v1, v2

    .line 496268
    new-instance v2, LX/DDn;

    invoke-direct {v2, p0, p2}, LX/DDn;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 496269
    iget-object v3, v1, LX/DDl;->a:LX/DDm;

    iput-object v2, v3, LX/DDm;->a:Landroid/view/View$OnClickListener;

    .line 496270
    move-object v1, v1

    .line 496271
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 496272
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;
    .locals 9

    .prologue
    .line 496273
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    monitor-enter v1

    .line 496274
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496275
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496276
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496277
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496278
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v6

    check-cast v6, LX/1EV;

    invoke-static {v0}, LX/34y;->a(LX/0QB;)LX/34y;

    move-result-object v7

    check-cast v7, LX/34y;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/1EV;LX/34y;LX/17W;)V

    .line 496279
    move-object v0, v3

    .line 496280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 496284
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 496285
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 496286
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->f:LX/1EV;

    .line 496287
    iget-object v1, v0, LX/1EV;->a:LX/0Uh;

    const/16 p0, 0x30c

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 496288
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 496289
    const/4 v0, 0x0

    return-object v0
.end method
