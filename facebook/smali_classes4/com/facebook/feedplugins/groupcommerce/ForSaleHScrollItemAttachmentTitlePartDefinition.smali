.class public Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Integer;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final d:LX/3Do;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/17Q;

.field public final g:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535967
    new-instance v0, LX/3Dn;

    invoke-direct {v0}, LX/3Dn;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/3Do;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535959
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 535960
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->b:Landroid/content/Context;

    .line 535961
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 535962
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->d:LX/3Do;

    .line 535963
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 535964
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->g:LX/0Zb;

    .line 535965
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->f:LX/17Q;

    .line 535966
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;
    .locals 10

    .prologue
    .line 535948
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    monitor-enter v1

    .line 535949
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535950
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535951
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535952
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535953
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    const-class v6, LX/3Do;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/3Do;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v9

    check-cast v9, LX/17Q;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/3Do;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Zb;LX/17Q;)V

    .line 535954
    move-object v0, v3

    .line 535955
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535956
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535957
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 535947
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 535929
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535930
    invoke-static {p2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 535931
    if-eqz v0, :cond_0

    .line 535932
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 535933
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 535934
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 535935
    if-eqz v1, :cond_0

    .line 535936
    invoke-static {v1}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/37O;

    move-result-object v1

    .line 535937
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->b:Landroid/content/Context;

    invoke-static {v2, v0}, LX/37P;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 535938
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->b:Landroid/content/Context;

    invoke-static {v2, v1}, LX/37Q;->a(Landroid/content/Context;LX/37O;)I

    move-result v1

    .line 535939
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535940
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/DDb;

    invoke-direct {v2, p0, p2}, LX/DDb;-><init>(Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 535941
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 535942
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4421ae5d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 535944
    check-cast p2, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 535945
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 535946
    const/16 v1, 0x1f

    const v2, -0x6559ae41

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 535943
    const/4 v0, 0x1

    return v0
.end method
