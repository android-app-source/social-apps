.class public Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:LX/34u;

.field public final e:LX/3Do;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final g:LX/17Q;

.field public final h:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535976
    new-instance v0, LX/3Dp;

    invoke-direct {v0}, LX/3Dp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/34u;LX/3Do;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 535977
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 535978
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->b:Landroid/content/Context;

    .line 535979
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 535980
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->d:LX/34u;

    .line 535981
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->e:LX/3Do;

    .line 535982
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 535983
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->h:LX/0Zb;

    .line 535984
    iput-object p7, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->g:LX/17Q;

    .line 535985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;
    .locals 11

    .prologue
    .line 535986
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    monitor-enter v1

    .line 535987
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 535988
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 535989
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535990
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 535991
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/34u;->a(LX/0QB;)LX/34u;

    move-result-object v6

    check-cast v6, LX/34u;

    const-class v7, LX/3Do;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/3Do;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v10

    check-cast v10, LX/17Q;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/34u;LX/3Do;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Zb;LX/17Q;)V

    .line 535992
    move-object v0, v3

    .line 535993
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 535994
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535995
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 535996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 535997
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 535998
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 535999
    invoke-static {p2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 536000
    if-eqz v0, :cond_0

    .line 536001
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 536002
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 536003
    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/37O;

    move-result-object v1

    .line 536004
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 536005
    iget-object v3, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->d:LX/34u;

    invoke-static {v0, v1, v2, v3, v4}, LX/37Q;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/37O;Landroid/text/SpannableStringBuilder;Landroid/content/Context;LX/34u;)V

    .line 536006
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536007
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/DDa;

    invoke-direct {v1, p0, p2}, LX/DDa;-><init>(Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentPriceAndLocationPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 536008
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 536009
    const/4 v0, 0x1

    return v0
.end method
