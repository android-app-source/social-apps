.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/2mN;

.field private final e:LX/1V0;

.field private final f:LX/2mO;

.field private final g:LX/2mJ;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/2mN;LX/2mO;LX/2mJ;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1V0;",
            "LX/2mN;",
            "LX/2mO;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460128
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 460129
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->e:LX/1V0;

    .line 460130
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->d:LX/2mN;

    .line 460131
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->f:LX/2mO;

    .line 460132
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->g:LX/2mJ;

    .line 460133
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->h:LX/0Ot;

    .line 460134
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 460135
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460136
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460137
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460138
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    .line 460139
    sget-object v2, LX/2nM;->NEWSFEED_LARGE_IMAGE:LX/2nM;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2nM;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 460140
    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->g:LX/2mJ;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13P;

    invoke-static {v0, v3, v1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;LX/2mJ;LX/13P;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 460141
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->e:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v3, p2, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->d:LX/2mN;

    const/4 v5, 0x0

    .line 460142
    new-instance v6, LX/CBh;

    invoke-direct {v6, v4}, LX/CBh;-><init>(LX/2mN;)V

    .line 460143
    iget-object p0, v4, LX/2mN;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CBg;

    .line 460144
    if-nez p0, :cond_0

    .line 460145
    new-instance p0, LX/CBg;

    invoke-direct {p0, v4}, LX/CBg;-><init>(LX/2mN;)V

    .line 460146
    :cond_0
    invoke-static {p0, p1, v5, v5, v6}, LX/CBg;->a$redex0(LX/CBg;LX/1De;IILX/CBh;)V

    .line 460147
    move-object v6, p0

    .line 460148
    move-object v5, v6

    .line 460149
    move-object v4, v5

    .line 460150
    iget-object v5, v4, LX/CBg;->a:LX/CBh;

    iput-object p3, v5, LX/CBh;->c:LX/1Pn;

    .line 460151
    iget-object v5, v4, LX/CBg;->e:Ljava/util/BitSet;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 460152
    move-object v4, v4

    .line 460153
    iget-object v5, v4, LX/CBg;->a:LX/CBh;

    iput-object v1, v5, LX/CBh;->b:Landroid/view/View$OnClickListener;

    .line 460154
    iget-object v5, v4, LX/CBg;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 460155
    move-object v1, v4

    .line 460156
    iget-object v4, v1, LX/CBg;->a:LX/CBh;

    iput-object p2, v4, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460157
    iget-object v4, v1, LX/CBg;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 460158
    move-object v1, v1

    .line 460159
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v2, p1, v0, v3, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->f:LX/2mO;

    invoke-virtual {v2, p1}, LX/2mO;->c(LX/1De;)LX/CBU;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/CBU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBU;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;
    .locals 10

    .prologue
    .line 460160
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    monitor-enter v1

    .line 460161
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460162
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460163
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460164
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460165
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/2mN;->a(LX/0QB;)LX/2mN;

    move-result-object v6

    check-cast v6, LX/2mN;

    invoke-static {v0}, LX/2mO;->a(LX/0QB;)LX/2mO;

    move-result-object v7

    check-cast v7, LX/2mO;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v8

    check-cast v8, LX/2mJ;

    const/16 v9, 0x104c

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/2mN;LX/2mO;LX/2mJ;LX/0Ot;)V

    .line 460166
    move-object v0, v3

    .line 460167
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460168
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460169
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460170
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 460171
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 460172
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460173
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460174
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460176
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object p0

    .line 460177
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 460178
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 460179
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460180
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460181
    check-cast v0, LX/0jW;

    return-object v0
.end method
