.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460675
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 460676
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    .line 460677
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 460678
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 460679
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460680
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460681
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460682
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460683
    new-instance p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;)V

    .line 460684
    move-object v0, p0

    .line 460685
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460686
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460687
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 460689
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460690
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460691
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    .line 460692
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460693
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460694
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460695
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 460696
    if-nez v1, :cond_1

    .line 460697
    const/4 v0, 0x0

    .line 460698
    :goto_0
    move-object v1, v0

    .line 460699
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460700
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 460701
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 460702
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 460703
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 460704
    :cond_1
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 460705
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p3

    if-eqz p3, :cond_2

    .line 460706
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p3

    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p3

    .line 460707
    iput-object p3, v2, LX/39x;->q:LX/0Px;

    .line 460708
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    .line 460709
    iput-object v1, v2, LX/39x;->p:LX/0Px;

    .line 460710
    :cond_2
    new-instance v1, LX/23u;

    invoke-direct {v1}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 460711
    iput-object v0, v1, LX/23u;->m:Ljava/lang/String;

    .line 460712
    move-object v0, v1

    .line 460713
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 460714
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 460715
    move-object v0, v0

    .line 460716
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 460717
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 460718
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460719
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460720
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460721
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 460722
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 460723
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    if-nez v0, :cond_1

    .line 460724
    :cond_0
    const/4 v0, 0x0

    .line 460725
    :goto_0
    return v0

    :cond_1
    sget-object v0, LX/2nM;->NEWSFEED_BRANDED_VIDEO:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2nM;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
