.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

.field private final b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

.field private final d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

.field private final e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

.field private final f:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459427
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 459428
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    .line 459429
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    .line 459430
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    .line 459431
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    .line 459432
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    .line 459433
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 459434
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;
    .locals 10

    .prologue
    .line 459435
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;

    monitor-enter v1

    .line 459436
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459437
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459438
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459439
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459440
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V

    .line 459441
    move-object v0, v3

    .line 459442
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459443
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459444
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459446
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459447
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459448
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459449
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionVideoAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459450
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459451
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459452
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 459453
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 459454
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459455
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459456
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459457
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 459458
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    .line 459459
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/2nM;->UNKNOWN:LX/2nM;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2nM;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 459460
    if-nez v0, :cond_1

    move v1, v2

    .line 459461
    :goto_0
    move v0, v1

    .line 459462
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 459463
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v5

    .line 459464
    if-eqz v5, :cond_2

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v1, v2

    .line 459465
    goto :goto_0

    .line 459466
    :cond_3
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    :goto_2
    if-ge v4, v6, :cond_6

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 459467
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->k()Z

    move-result p0

    if-eqz p0, :cond_5

    .line 459468
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object p0

    .line 459469
    sget-object p1, LX/CBI;->a:[I

    invoke-virtual {p0}, LX/CBr;->ordinal()I

    move-result p0

    aget p0, p1, p0

    packed-switch p0, :pswitch_data_0

    move v1, v3

    .line 459470
    goto :goto_0

    .line 459471
    :pswitch_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBm;->fromString(Ljava/lang/String;)LX/CBm;

    move-result-object v1

    .line 459472
    sget-object p0, LX/CBm;->UNKNOWN:LX/CBm;

    invoke-virtual {v1, p0}, LX/CBm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459473
    goto :goto_0

    .line 459474
    :pswitch_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2nO;->fromString(Ljava/lang/String;)LX/2nO;

    move-result-object v1

    .line 459475
    sget-object p0, LX/2nO;->UNKNOWN:LX/2nO;

    invoke-virtual {v1, p0}, LX/2nO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459476
    goto :goto_0

    .line 459477
    :pswitch_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBn;->fromString(Ljava/lang/String;)LX/CBn;

    move-result-object v1

    .line 459478
    sget-object p0, LX/CBn;->UNKNOWN:LX/CBn;

    invoke-virtual {v1, p0}, LX/CBn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459479
    goto :goto_0

    .line 459480
    :pswitch_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBl;->fromString(Ljava/lang/String;)LX/CBl;

    move-result-object v1

    .line 459481
    sget-object p0, LX/CBl;->UNKNOWN:LX/CBl;

    invoke-virtual {v1, p0}, LX/CBl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459482
    goto/16 :goto_0

    .line 459483
    :pswitch_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/CBj;->fromString(Ljava/lang/String;)LX/CBj;

    move-result-object p0

    .line 459484
    sget-object p1, LX/CBj;->UNKNOWN:LX/CBj;

    invoke-virtual {p0, p1}, LX/CBj;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    move v1, v3

    .line 459485
    goto/16 :goto_0

    .line 459486
    :cond_4
    :pswitch_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBq;->fromString(Ljava/lang/String;)LX/CBq;

    move-result-object v1

    .line 459487
    sget-object p0, LX/CBq;->UNKNOWN:LX/CBq;

    invoke-virtual {v1, p0}, LX/CBq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459488
    goto/16 :goto_0

    .line 459489
    :pswitch_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBp;->fromString(Ljava/lang/String;)LX/CBp;

    move-result-object v1

    .line 459490
    sget-object p0, LX/CBp;->UNKNOWN:LX/CBp;

    invoke-virtual {v1, p0}, LX/CBp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 459491
    goto/16 :goto_0

    .line 459492
    :cond_5
    :pswitch_7
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_2

    :cond_6
    move v1, v2

    .line 459493
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
