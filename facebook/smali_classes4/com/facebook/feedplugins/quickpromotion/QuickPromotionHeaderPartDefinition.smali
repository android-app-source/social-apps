.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/2ee;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459494
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 459495
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 459496
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 459497
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;
    .locals 5

    .prologue
    .line 459498
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    monitor-enter v1

    .line 459499
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459500
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459503
    new-instance p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V

    .line 459504
    move-object v0, p0

    .line 459505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 459509
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 459510
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459511
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459512
    const v0, 0x7f0d0bde

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v2, LX/24b;

    sget-object v3, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v2, p2, v3}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 459513
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x41af3a72

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459514
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/2ee;

    .line 459515
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 459516
    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459517
    sget-object v2, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v2}, LX/2ee;->setStyle(LX/2ei;)V

    .line 459518
    invoke-static {v1}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    .line 459519
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 459520
    invoke-virtual {p4, v2}, LX/2ee;->setMenuButtonActive(Z)V

    .line 459521
    invoke-static {v1}, LX/2nL;->d(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, v1, v2}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 459522
    const/16 v1, 0x1f

    const v2, 0x79d79ef0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 459523
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459524
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459525
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459526
    invoke-static {v0}, LX/2nN;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 459527
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    return-object v0
.end method
