.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "LX/CBB;",
        "TE;",
        "LX/CBy;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static q:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field public final h:LX/0wM;

.field private final i:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field public final j:LX/2mJ;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Landroid/content/res/Resources;

.field private final n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final o:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 459991
    new-instance v0, LX/2mL;

    invoke-direct {v0}, LX/2mL;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a:LX/1Cz;

    .line 459992
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 459993
    iput v1, v0, LX/1UY;->b:F

    .line 459994
    move-object v0, v0

    .line 459995
    iput v1, v0, LX/1UY;->c:F

    .line 459996
    move-object v0, v0

    .line 459997
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->b:LX/1Ua;

    .line 459998
    const-class v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/0wM;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/2mJ;LX/0Ot;LX/0Or;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            "LX/0wM;",
            "Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;",
            "Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;",
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459999
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460000
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 460001
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    .line 460002
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460003
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 460004
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->h:LX/0wM;

    .line 460005
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->i:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 460006
    iput-object p7, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->j:LX/2mJ;

    .line 460007
    iput-object p8, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->k:LX/0Ot;

    .line 460008
    iput-object p10, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    .line 460009
    iput-object p9, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->l:LX/0Or;

    .line 460010
    iput-object p11, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 460011
    iput-object p12, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->o:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 460012
    iput-object p13, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 460013
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBB;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;)",
            "LX/CBB;"
        }
    .end annotation

    .prologue
    .line 460020
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460021
    const v5, 0x7f0d27d1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->i:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v7, LX/24b;

    sget-object v8, LX/1dl;->CLICKABLE:LX/1dl;

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v8}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460022
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v6, LX/1X6;

    sget-object v7, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->b:LX/1Ua;

    move-object/from16 v0, p2

    invoke-direct {v6, v0, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460023
    invoke-static {v4}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v12

    .line 460024
    invoke-static {v4}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v13

    .line 460025
    invoke-static {v13}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460026
    invoke-static {v12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460027
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->g(LX/0Px;)LX/CBk;

    move-result-object v14

    .line 460028
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->h(LX/0Px;)I

    move-result v9

    .line 460029
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->i(LX/0Px;)I

    move-result v8

    .line 460030
    const/4 v4, -0x1

    if-ne v8, v4, :cond_1

    const/16 v6, 0x102

    .line 460031
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->l:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-virtual {v4}, LX/0sd;->c()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, v5

    float-to-int v10, v4

    .line 460032
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->l:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-virtual {v4}, LX/0sd;->c()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3f59999a    # 0.85f

    mul-float/2addr v4, v5

    float-to-int v11, v4

    .line 460033
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    const v5, 0x7f0b1ce2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 460034
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    const v5, 0x7f0b1ce7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 460035
    const v5, 0x7f0d27ce

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object/from16 v0, p1

    invoke-interface {v0, v5, v7, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460036
    const v5, 0x7f0d27cf

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    :goto_2
    move-object/from16 v0, p1

    invoke-interface {v0, v5, v7, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460037
    const v5, 0x7f0d27d0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    :goto_3
    move-object/from16 v0, p1

    invoke-interface {v0, v5, v7, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460038
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->a(LX/0Px;)LX/CBl;

    move-result-object v4

    .line 460039
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v13, v5}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;LX/CBl;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 460040
    invoke-static {v13}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v7

    .line 460041
    if-eqz v7, :cond_5

    .line 460042
    const v17, 0x7f0d0bbd

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    move-object/from16 v18, v0

    new-instance v19, LX/C4r;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->l:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-virtual {v4}, LX/0sd;->c()I

    move-result v4

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v7, v4}, LX/C4r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;I)V

    move-object/from16 v0, p1

    move/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-interface {v0, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460043
    if-eqz v5, :cond_0

    .line 460044
    const v4, 0x7f0d0bbd

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v7, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460045
    :cond_0
    const v4, 0x7f0d0bbe

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460046
    const v4, 0x7f0d0bbd

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460047
    :goto_4
    sget-object v4, LX/CBk;->BLUE:LX/CBk;

    if-ne v14, v4, :cond_7

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 460048
    const v4, 0x7f0d27d3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v7

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v7

    sget-object v17, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, LX/2f8;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/2f8;

    move-result-object v7

    invoke-virtual {v7}, LX/2f8;->a()LX/2f9;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460049
    const v4, 0x7f0d27d3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460050
    const v4, 0x7f0d27d2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460051
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    const v5, 0x7f0a07cd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 460052
    :goto_5
    const/4 v5, 0x0

    .line 460053
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 460054
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 460055
    new-instance v4, LX/CB8;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13, v12}, LX/CB8;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    .line 460056
    const v13, 0x7f0d0551

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v13, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460057
    const v4, 0x7f0d0551

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v4, v13, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460058
    :goto_6
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    invoke-static {v4, v15, v13}, LX/2nN;->a(LX/0Px;ILandroid/content/res/Resources;)I

    move-result v4

    .line 460059
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    move/from16 v0, v16

    invoke-static {v12, v0, v13}, LX/2nN;->b(LX/0Px;ILandroid/content/res/Resources;)I

    move-result v12

    .line 460060
    const v13, 0x7f0d27ce

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->o:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v16, LX/1ds;

    const/high16 v17, -0x80000000

    const/high16 v18, -0x80000000

    const/high16 v19, -0x80000000

    move-object/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v4, v2, v3}, LX/1ds;-><init>(IIII)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v0, v13, v15, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460061
    const v4, 0x7f0d0bbc

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->o:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v15, LX/1ds;

    const/high16 v16, -0x80000000

    const/high16 v17, -0x80000000

    const/high16 v18, -0x80000000

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v15, v0, v1, v2, v12}, LX/1ds;-><init>(IIII)V

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v13, v15}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460062
    new-instance v4, LX/CBB;

    sget-object v12, LX/CBk;->WHITE:LX/CBk;

    if-ne v14, v12, :cond_a

    const/4 v12, 0x1

    :goto_7
    invoke-direct/range {v4 .. v12}, LX/CBB;-><init>(Lcom/facebook/graphql/model/GraphQLTextWithEntities;IIIIIIZ)V

    return-object v4

    .line 460063
    :cond_1
    const/16 v6, 0x202

    goto/16 :goto_0

    .line 460064
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 460065
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 460066
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 460067
    :cond_5
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v7

    .line 460068
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v17

    .line 460069
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->l:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-virtual {v4}, LX/0sd;->c()I

    move-result v4

    mul-int v4, v4, v17

    div-int/2addr v4, v7

    .line 460070
    const v7, 0x7f0d0bbe

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v18

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v18

    const/16 v19, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v4}, LX/2f8;->a(II)LX/2f8;

    move-result-object v4

    sget-object v18, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, LX/2f8;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/2f8;

    move-result-object v4

    invoke-virtual {v4}, LX/2f8;->a()LX/2f9;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v7, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460071
    if-eqz v5, :cond_6

    .line 460072
    const v4, 0x7f0d0bbe

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v7, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460073
    :cond_6
    const v4, 0x7f0d0bbe

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460074
    const v4, 0x7f0d0bbd

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 460075
    :cond_7
    sget-object v4, LX/CBk;->WHITE:LX/CBk;

    if-ne v14, v4, :cond_8

    .line 460076
    const v4, 0x7f0d27d3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460077
    const v4, 0x7f0d27d2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460078
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    const v5, 0x7f0a07cc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    goto/16 :goto_5

    .line 460079
    :cond_8
    const v4, 0x7f0d27d3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460080
    const v4, 0x7f0d27d2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460081
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->m:Landroid/content/res/Resources;

    const v5, 0x7f0a07cc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    goto/16 :goto_5

    .line 460082
    :cond_9
    const v4, 0x7f0d0551

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->p:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v17, 0x8

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v4, v13, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 460083
    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_7
.end method

.method private static a(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;LX/CBl;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 460014
    sget-object v0, LX/CBl;->USE_PRIMARY_ACTION:LX/CBl;

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460015
    new-instance v0, LX/CB9;

    invoke-direct {v0, p0, p2, p3}, LX/CB9;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Ljava/lang/String;)V

    .line 460016
    :goto_0
    return-object v0

    .line 460017
    :cond_0
    sget-object v0, LX/CBl;->USE_SECONDARY_ACTION:LX/CBl;

    if-ne p1, v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460018
    new-instance v0, LX/CBA;

    invoke-direct {v0, p0, p2, p3}, LX/CBA;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Ljava/lang/String;)V

    goto :goto_0

    .line 460019
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;
    .locals 3

    .prologue
    .line 459942
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    monitor-enter v1

    .line 459943
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459944
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459945
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459946
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459947
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459948
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;)Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 459986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 459987
    :cond_1
    :goto_0
    return-object v0

    .line 459988
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 459989
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move-object v0, v1

    .line 459990
    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;
    .locals 14

    .prologue
    .line 459984
    new-instance v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {p0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v7

    check-cast v7, LX/2mJ;

    const/16 v8, 0x104c

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1224

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/0wM;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/2mJ;LX/0Ot;LX/0Or;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 459985
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 459983
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459982
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBB;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4f007723

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459960
    check-cast p2, LX/CBB;

    check-cast p4, LX/CBy;

    .line 459961
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/CBy;->setMenuButtonActive(Z)V

    .line 459962
    iget-object v1, p2, LX/CBB;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 459963
    if-eqz v1, :cond_0

    .line 459964
    iget-object v2, p4, LX/CBy;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 459965
    :cond_0
    iget v1, p2, LX/CBB;->b:I

    .line 459966
    iget-object v2, p4, LX/CBy;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 459967
    iget v1, p2, LX/CBB;->f:I

    .line 459968
    iget-object v2, p4, LX/CBy;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    .line 459969
    iget v1, p2, LX/CBB;->g:I

    .line 459970
    iget-object v2, p4, LX/CBy;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    .line 459971
    iget v1, p2, LX/CBB;->d:I

    .line 459972
    iget-object v2, p4, LX/CBy;->c:Landroid/widget/LinearLayout;

    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 459973
    iget v1, p2, LX/CBB;->e:I

    .line 459974
    iget-object v2, p4, LX/CBy;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 459975
    iget-object v2, p4, LX/CBy;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 459976
    iget v1, p2, LX/CBB;->c:I

    .line 459977
    iget-object v2, p4, LX/CBy;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 459978
    iget-boolean v1, p2, LX/CBB;->h:Z

    if-eqz v1, :cond_1

    .line 459979
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->h:LX/0wM;

    .line 459980
    iget-object v2, p4, LX/CBy;->f:Lcom/facebook/fbui/glyph/GlyphView;

    const p1, 0x7f020737

    const/4 p0, -0x1

    invoke-virtual {v1, p1, p0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 459981
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x1112ed86

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 459950
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 459951
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459952
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459953
    if-nez v0, :cond_0

    move v0, v1

    .line 459954
    :goto_0
    return v0

    .line 459955
    :cond_0
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 459956
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    .line 459957
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 459958
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 459959
    if-eqz v0, :cond_1

    sget-object v0, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
