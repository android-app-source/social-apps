.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "LX/8t9;",
        "LX/1Ps;",
        "LX/CBz;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition",
            "<",
            "LX/CBz;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 459624
    const-class v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    const-string v1, "quick_promotion_feed"

    const-string v2, "quickpromotion"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 459625
    new-instance v0, LX/2mH;

    invoke-direct {v0}, LX/2mH;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459619
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 459620
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 459621
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    .line 459622
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->e:Landroid/content/Context;

    .line 459623
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/CBm;)LX/8t9;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 459609
    if-eqz p2, :cond_0

    sget-object v0, LX/CBm;->UNKNOWN:LX/CBm;

    invoke-virtual {p2, v0}, LX/CBm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v6

    .line 459610
    :goto_0
    return-object v0

    .line 459611
    :cond_1
    sget-object v0, LX/CBm;->CIRCLE_CROP:LX/CBm;

    sget-object v1, LX/8ue;->NONE:LX/8ue;

    sget-object v2, LX/CBm;->FACEBOOK_BADGE:LX/CBm;

    sget-object v3, LX/8ue;->FACEBOOK:LX/8ue;

    sget-object v4, LX/CBm;->MESSENGER_BADGE:LX/CBm;

    sget-object v5, LX/8ue;->MESSENGER:LX/8ue;

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 459612
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 459613
    if-eqz v1, :cond_2

    invoke-virtual {v0, p2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v6

    .line 459614
    goto :goto_0

    .line 459615
    :cond_3
    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8ue;

    .line 459616
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1cdb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 459617
    new-instance v3, Lcom/facebook/user/model/PicSquare;

    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v2, v1}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-direct {v3, v4, v6, v6}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    .line 459618
    invoke-static {v3, v0}, LX/8t9;->a(Lcom/facebook/user/model/PicSquare;LX/8ue;)LX/8t9;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;
    .locals 6

    .prologue
    .line 459598
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    monitor-enter v1

    .line 459599
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459600
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459601
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459602
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459603
    new-instance p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;Landroid/content/Context;)V

    .line 459604
    move-object v0, p0

    .line 459605
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459606
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459607
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 459597
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 459577
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 459578
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459579
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459580
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459581
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 459582
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    .line 459583
    const/4 v3, 0x0

    .line 459584
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v5

    .line 459585
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move-object v2, v3

    .line 459586
    :goto_0
    move-object v0, v2

    .line 459587
    if-eqz v0, :cond_1

    sget-object v2, LX/CBm;->UNKNOWN:LX/CBm;

    invoke-virtual {v0, v2}, LX/CBm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 459588
    :cond_1
    new-instance v2, LX/CBF;

    invoke-direct {v2, p0, v1}, LX/CBF;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;)V

    .line 459589
    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    new-instance v4, LX/AmL;

    sget-object v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v4, v5, v2, v6, v6}, LX/AmL;-><init>(Lcom/facebook/common/callercontext/CallerContext;LX/AmK;ZZ)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459590
    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/CBm;)LX/8t9;

    move-result-object v0

    return-object v0

    .line 459591
    :cond_3
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 459592
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object p2

    .line 459593
    sget-object p3, LX/CBr;->IMAGE_OVERLAY:LX/CBr;

    invoke-virtual {p3, p2}, LX/CBr;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 459594
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CBm;->fromString(Ljava/lang/String;)LX/CBm;

    move-result-object v2

    goto :goto_0

    .line 459595
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_5
    move-object v2, v3

    .line 459596
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3a6acc88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459555
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/8t9;

    check-cast p4, LX/CBz;

    .line 459556
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 459557
    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459558
    invoke-static {v1}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 459559
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 459560
    invoke-static {v2}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 459561
    iget-object p0, p4, LX/CBz;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 p3, 0x8

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 459562
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 459563
    invoke-static {v2}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 459564
    iget-object p0, p4, LX/CBz;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 p3, 0x8

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 459565
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 459566
    iget-object v1, p4, LX/CBz;->m:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v1, p2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 459567
    const/16 v1, 0x13

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 459568
    iget-object v1, p4, LX/CBz;->m:Lcom/facebook/user/tiles/UserTileView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 459569
    iget-object v1, p4, LX/CBz;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 459570
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 459571
    const/16 v1, 0x10

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 459572
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x5e97ae8a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 459573
    :cond_1
    iget-object p0, p4, LX/CBz;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object p3, p4, LX/CBz;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result p3

    const/4 p1, 0x1

    invoke-virtual {p0, v2, p3, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 459574
    iget-object p0, p4, LX/CBz;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 p3, 0x0

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0

    .line 459575
    :cond_2
    iget-object p0, p4, LX/CBz;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object p3, p4, LX/CBz;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result p3

    const/4 p1, 0x1

    invoke-virtual {p0, v2, p3, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 459576
    iget-object p0, p4, LX/CBz;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 p3, 0x0

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 459549
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459550
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459551
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 459552
    check-cast p4, LX/CBz;

    .line 459553
    invoke-virtual {p4}, LX/CBz;->d()V

    .line 459554
    return-void
.end method
