.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        "LX/CBv;",
        "LX/1Ps;",
        "LX/3W0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/2mR;

.field public static final b:LX/2mR;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field public final d:LX/2mJ;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460450
    sget-object v0, LX/2mR;->RIGHT:LX/2mR;

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a:LX/2mR;

    .line 460451
    sget-object v0, LX/2mR;->LEFT:LX/2mR;

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->b:LX/2mR;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460443
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460444
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 460445
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->d:LX/2mJ;

    .line 460446
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->e:LX/0Ot;

    .line 460447
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460448
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460449
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 460406
    new-instance v0, LX/CBt;

    invoke-direct {v0, p0, p2, p3, p1}, LX/CBt;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;
    .locals 9

    .prologue
    .line 460432
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    monitor-enter v1

    .line 460433
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460434
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460435
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460436
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460437
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v5

    check-cast v5, LX/2mJ;

    const/16 v6, 0x104c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 460438
    move-object v0, v3

    .line 460439
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460440
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460441
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460452
    sget-object v0, LX/3W0;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 460422
    check-cast p2, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460423
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    new-instance v1, LX/Ans;

    invoke-static {p2}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v2

    invoke-direct {v1, v2}, LX/Ans;-><init>(Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460424
    invoke-static {p2}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 460425
    invoke-static {p2}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    .line 460426
    const v0, 0x7f0d2fbd

    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v5

    sget-object v6, LX/77m;->SECONDARY_ACTION:LX/77m;

    invoke-direct {p0, v4, v5, v6}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460427
    const v0, 0x7f0d2fbe

    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v5

    sget-object v6, LX/77m;->PRIMARY_ACTION:LX/77m;

    invoke-direct {p0, v4, v5, v6}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460428
    sget-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    .line 460429
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 460430
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->e(LX/0Px;)LX/CBp;

    move-result-object v0

    .line 460431
    :cond_0
    new-instance v3, LX/CBv;

    invoke-direct {v3, v1, v2, v0}, LX/CBv;-><init>(Lcom/facebook/graphql/model/GraphQLQuickPromotion;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/CBp;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4c26d964    # 4.3738512E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460414
    check-cast p2, LX/CBv;

    check-cast p4, LX/3W0;

    .line 460415
    iget-object v1, p2, LX/CBv;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 460416
    sget-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->b:LX/2mR;

    invoke-virtual {p4, v2, v1}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 460417
    iget-object v1, p2, LX/CBv;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 460418
    sget-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a:LX/2mR;

    invoke-virtual {p4, v2, v1}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 460419
    sget-object v1, LX/CBu;->a:[I

    iget-object v2, p2, LX/CBv;->c:LX/CBp;

    invoke-virtual {v2}, LX/CBp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 460420
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x278f12ee    # -1.059604E15f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 460421
    :pswitch_0
    sget-object v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a:LX/2mR;

    const v2, 0x7f0a00d2

    invoke-virtual {p4, v1, v2}, LX/3W0;->a(LX/2mR;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460412
    check-cast p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460413
    invoke-static {p1}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 460407
    check-cast p4, LX/3W0;

    const/4 v2, 0x0

    .line 460408
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a:LX/2mR;

    invoke-virtual {p4, v0, v2}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 460409
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a:LX/2mR;

    const v1, 0x7f0a00e6

    invoke-virtual {p4, v0, v1}, LX/3W0;->a(LX/2mR;I)V

    .line 460410
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->b:LX/2mR;

    invoke-virtual {p4, v0, v2}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 460411
    return-void
.end method
