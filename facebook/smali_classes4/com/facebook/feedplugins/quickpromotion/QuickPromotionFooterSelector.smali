.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

.field private final c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

.field private final d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

.field private final e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

.field private final f:Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460297
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 460298
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->a:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    .line 460299
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    .line 460300
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    .line 460301
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    .line 460302
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    .line 460303
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->f:Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;

    .line 460304
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;
    .locals 10

    .prologue
    .line 460313
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    monitor-enter v1

    .line 460314
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460315
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460316
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460317
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460318
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;)V

    .line 460319
    move-object v0, v3

    .line 460320
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460321
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460322
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 460306
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460307
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460308
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460309
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->e:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonFooterPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->f:Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    .line 460310
    invoke-static {v0}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460311
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFooterSelector;->a:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 460312
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460305
    const/4 v0, 0x1

    return v0
.end method
