.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        "LX/CBx;",
        "LX/1Ps;",
        "LX/Ao7;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:LX/2mQ;

.field public static final c:LX/2mQ;

.field private static j:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field public final e:LX/2mJ;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final i:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460395
    new-instance v0, LX/2mP;

    invoke-direct {v0}, LX/2mP;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    .line 460396
    sget-object v0, LX/2mQ;->RIGHT:LX/2mQ;

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    .line 460397
    sget-object v0, LX/2mQ;->LEFT:LX/2mQ;

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460387
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460388
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 460389
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->e:LX/2mJ;

    .line 460390
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->f:LX/0Ot;

    .line 460391
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460392
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460393
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->i:LX/0wM;

    .line 460394
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 460386
    new-instance v0, LX/CBw;

    invoke-direct {v0, p0, p2, p3, p1}, LX/CBw;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;
    .locals 10

    .prologue
    .line 460324
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    monitor-enter v1

    .line 460325
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460326
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460329
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v5

    check-cast v5, LX/2mJ;

    const/16 v6, 0x104c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V

    .line 460330
    move-object v0, v3

    .line 460331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460385
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 460368
    check-cast p2, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460369
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    new-instance v1, LX/Ans;

    invoke-static {p2}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v2

    invoke-direct {v1, v2}, LX/Ans;-><init>(Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460370
    invoke-static {p2}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    .line 460371
    invoke-static {p2}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v3

    .line 460372
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 460373
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 460374
    if-eqz v0, :cond_0

    .line 460375
    const v4, 0x7f0d2fbe

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v7

    sget-object v8, LX/77m;->PRIMARY_ACTION:LX/77m;

    invoke-direct {p0, v6, v7, v8}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460376
    const v4, 0x7f0d2fbd

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v3

    sget-object v7, LX/77m;->SECONDARY_ACTION:LX/77m;

    invoke-direct {p0, v6, v3, v7}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;LX/77m;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-interface {p1, v4, v5, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460377
    :cond_0
    sget-object v3, LX/2nO;->UNKNOWN:LX/2nO;

    .line 460378
    sget-object v4, LX/CBq;->UNKNOWN:LX/CBq;

    .line 460379
    sget-object v5, LX/CBp;->UNKNOWN:LX/CBp;

    .line 460380
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 460381
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/2nN;->c(LX/0Px;)LX/2nO;

    move-result-object v3

    .line 460382
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/2nN;->d(LX/0Px;)LX/CBq;

    move-result-object v4

    .line 460383
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->e(LX/0Px;)LX/CBp;

    move-result-object v5

    .line 460384
    :cond_1
    new-instance v0, LX/CBx;

    invoke-direct/range {v0 .. v5}, LX/CBx;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2nO;LX/CBq;LX/CBp;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1e2f2f43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460349
    check-cast p2, LX/CBx;

    check-cast p4, LX/Ao7;

    .line 460350
    iget-object v1, p2, LX/CBx;->b:Ljava/lang/String;

    iget-object v2, p2, LX/CBx;->d:LX/CBq;

    const p3, -0x6e685d

    .line 460351
    sget-object v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    invoke-virtual {p4, v4, v1}, LX/Ao7;->a(LX/2mQ;Ljava/lang/CharSequence;)V

    .line 460352
    sget-object v4, LX/CBq;->DISCLOSURE:LX/CBq;

    if-ne v2, v4, :cond_2

    .line 460353
    sget-object v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->i:LX/0wM;

    const p1, 0x7f0207eb

    invoke-virtual {v5, p1, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p4, v4, v5}, LX/Ao7;->a(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460354
    :cond_0
    :goto_0
    iget-object v1, p2, LX/CBx;->a:Ljava/lang/String;

    iget-object v2, p2, LX/CBx;->c:LX/2nO;

    iget-object v4, p2, LX/CBx;->e:LX/CBp;

    .line 460355
    sget-object v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    invoke-virtual {p4, v5, v1}, LX/Ao7;->a(LX/2mQ;Ljava/lang/CharSequence;)V

    .line 460356
    sget-object v5, LX/CBp;->PRIMARY:LX/CBp;

    if-ne v4, v5, :cond_3

    .line 460357
    const v5, -0xa76f01

    .line 460358
    sget-object p1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    const p3, 0x7f0a00d2

    invoke-virtual {p4, p1, p3}, LX/Ao7;->a(LX/2mQ;I)V

    .line 460359
    :goto_1
    sget-object p1, LX/2nO;->DISCLOSURE:LX/2nO;

    if-ne v2, p1, :cond_4

    .line 460360
    sget-object p1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    iget-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->i:LX/0wM;

    const p2, 0x7f0207eb

    invoke-virtual {p3, p2, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p4, p1, v5}, LX/Ao7;->a(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460361
    :cond_1
    :goto_2
    const/16 v1, 0x1f

    const v2, -0x789c9e5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 460362
    :cond_2
    sget-object v4, LX/CBq;->SHARE:LX/CBq;

    if-ne v2, v4, :cond_0

    .line 460363
    sget-object v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->i:LX/0wM;

    const p1, 0x7f0209c7

    invoke-virtual {v5, p1, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p4, v4, v5}, LX/Ao7;->b(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 460364
    :cond_3
    const v5, -0x6e685d

    .line 460365
    sget-object p1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    const p3, 0x7f0a010e

    invoke-virtual {p4, p1, p3}, LX/Ao7;->a(LX/2mQ;I)V

    goto :goto_1

    .line 460366
    :cond_4
    sget-object p1, LX/2nO;->SHARE:LX/2nO;

    if-ne v2, p1, :cond_1

    .line 460367
    sget-object p1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    iget-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->i:LX/0wM;

    const p2, 0x7f0209c7

    invoke-virtual {p3, p2, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p4, p1, v5}, LX/Ao7;->b(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 460344
    check-cast p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    const/4 v0, 0x0

    .line 460345
    if-nez p1, :cond_1

    .line 460346
    :cond_0
    :goto_0
    return v0

    .line 460347
    :cond_1
    invoke-static {p1}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 460348
    invoke-static {p1}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/2nN;->c(LX/0Px;)LX/2nO;

    move-result-object v2

    sget-object v3, LX/2nO;->UNKNOWN:LX/2nO;

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2nN;->d(LX/0Px;)LX/CBq;

    move-result-object v1

    sget-object v2, LX/CBq;->UNKNOWN:LX/CBq;

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 460335
    check-cast p4, LX/Ao7;

    const/4 v1, 0x0

    .line 460336
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->a(LX/2mQ;Ljava/lang/CharSequence;)V

    .line 460337
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->a(LX/2mQ;Ljava/lang/CharSequence;)V

    .line 460338
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->a(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460339
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->a(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460340
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->b(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460341
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->c:LX/2mQ;

    invoke-virtual {p4, v0, v1}, LX/Ao7;->b(LX/2mQ;Landroid/graphics/drawable/Drawable;)V

    .line 460342
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->b:LX/2mQ;

    const v1, 0x7f0a010e

    invoke-virtual {p4, v0, v1}, LX/Ao7;->a(LX/2mQ;I)V

    .line 460343
    return-void
.end method
