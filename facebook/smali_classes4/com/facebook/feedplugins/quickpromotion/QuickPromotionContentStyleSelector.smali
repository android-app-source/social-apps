.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459532
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 459533
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    .line 459534
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    .line 459535
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    .line 459536
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    .line 459537
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;
    .locals 7

    .prologue
    .line 459538
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    monitor-enter v1

    .line 459539
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459540
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459541
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459542
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459543
    new-instance p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;)V

    .line 459544
    move-object v0, p0

    .line 459545
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459546
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459547
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459548
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 459529
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459530
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->a:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->d:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionContentStyleSelector;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 459531
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 459528
    const/4 v0, 0x1

    return v0
.end method
