.class public Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/24a;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

.field public h:Landroid/widget/ImageView;

.field public i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public l:I

.field private n:LX/0wY;

.field private o:LX/0wa;

.field private p:I

.field private q:LX/0W9;

.field private r:Z

.field private s:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 464253
    const-class v0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;

    const-string v1, "quick_promotion_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 464254
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 464255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->r:Z

    .line 464256
    const v0, 0x7f0310c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 464257
    const v0, 0x7f0d27ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 464258
    const v0, 0x7f0d27cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 464259
    const v0, 0x7f0d27dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 464260
    const v0, 0x7f0d27d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    .line 464261
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;IDLcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 464262
    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 464263
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 464264
    int-to-double v2, p1

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->q()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v6, v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 464265
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 464266
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    int-to-double v2, v1

    mul-double/2addr v2, p2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 464267
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;ILcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V
    .locals 10

    .prologue
    const-wide v8, 0x3f947ae140000000L    # 0.019999999552965164

    const/4 v6, 0x0

    .line 464268
    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 464269
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 464270
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->q()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->o()D

    move-result-wide v4

    add-double/2addr v2, v4

    add-double/2addr v2, v8

    double-to-float v2, v2

    .line 464271
    int-to-float v3, p1

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v6, v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 464272
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 464273
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->p()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->o()D

    move-result-wide v4

    sub-double/2addr v2, v4

    sub-double/2addr v2, v8

    .line 464274
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 464275
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;I)V
    .locals 4

    .prologue
    .line 464318
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->j()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 464319
    iget v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->p:I

    if-eq v0, v1, :cond_0

    .line 464320
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->q:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 464321
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464322
    iput v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->p:I

    .line 464323
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 4

    .prologue
    .line 464276
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    int-to-double v0, v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 464277
    iget v2, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    move v2, v2

    .line 464278
    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;IDJ)V
    .locals 9

    .prologue
    .line 464279
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->s:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p5

    long-to-double v2, v0

    .line 464280
    const-wide v0, 0x408f400000000000L    # 1000.0

    div-double v0, v2, v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->k()I

    move-result v4

    int-to-double v4, v4

    cmpl-double v0, v0, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 464281
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->j()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->m()I

    move-result v4

    if-le v1, v4, :cond_2

    const/4 v1, 0x1

    .line 464282
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p3, v0

    if-gtz v0, :cond_3

    .line 464283
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->p()D

    move-result-wide v0

    invoke-static {p0, p2, v0, v1, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;IDLcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V

    .line 464284
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464285
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 464286
    :goto_2
    return-void

    .line 464287
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 464288
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 464289
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->r:Z

    if-nez v0, :cond_4

    .line 464290
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->o()D

    move-result-wide v0

    invoke-static {p0, p2, v0, v1, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;IDLcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V

    .line 464291
    invoke-static {p0, p2, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;ILcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V

    .line 464292
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 464293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->r:Z

    .line 464294
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->n()D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    add-double/2addr v0, p3

    .line 464295
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {p0, p1, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;I)V

    .line 464296
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->o:LX/0wa;

    const-wide v2, 0x4050800000000000L    # 66.0

    const-wide v4, 0x408f400000000000L    # 1000.0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->n()D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/0wY;->a(LX/0wa;J)V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 464297
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 464298
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464299
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464300
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    .line 464301
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464302
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464303
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_2

    .line 464304
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464305
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    if-eqz v0, :cond_3

    .line 464306
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;->setVisibility(I)V

    .line 464307
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->o:LX/0wa;

    if-eqz v0, :cond_4

    .line 464308
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->o:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->b(LX/0wa;)V

    .line 464309
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_5

    .line 464310
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 464311
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_6

    .line 464312
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 464313
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->r:Z

    .line 464314
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464315
    invoke-virtual {p0, v3}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464316
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464317
    return-void
.end method

.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 464229
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 464230
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;Lcom/facebook/graphql/model/GraphQLImage;LX/CBo;)V
    .locals 4

    .prologue
    .line 464231
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 464232
    const v0, 0x7f0d27db

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 464233
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 464234
    :cond_0
    const v0, 0x7f0d27dc

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 464235
    sget-object v0, LX/CBo;->CENTERED_LARGE:LX/CBo;

    if-ne p3, v0, :cond_3

    .line 464236
    const-wide v0, 0x3fdd70a3d70a3d71L    # 0.46

    .line 464237
    :goto_0
    invoke-static {p0, p2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a$redex0(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v1, v0

    .line 464238
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 464239
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 464240
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 464241
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v2

    .line 464242
    const-string v0, ""

    .line 464243
    if-eqz v2, :cond_1

    .line 464244
    invoke-virtual {v2, v1}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    .line 464245
    if-eqz v1, :cond_1

    .line 464246
    iget-object v0, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 464247
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464248
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 464249
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 464250
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464251
    return-void

    .line 464252
    :cond_3
    const-wide v0, 0x3fd999999999999aL    # 0.4

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLImage;Landroid/view/View$OnClickListener;I)V
    .locals 3

    .prologue
    .line 464213
    iput p3, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    .line 464214
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 464215
    const v0, 0x7f0d27d7

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 464216
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 464217
    :cond_0
    const v0, 0x7f0d27d8

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 464218
    const v0, 0x7f0d27d4

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 464219
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 464220
    if-nez p1, :cond_1

    .line 464221
    :goto_0
    return-void

    .line 464222
    :cond_1
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 464223
    iget p3, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    move p3, p3

    .line 464224
    iput p3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 464225
    invoke-static {p0, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a$redex0(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result p3

    iput p3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 464226
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464227
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    sget-object p3, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 464228
    invoke-virtual {v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;Lcom/facebook/graphql/model/GraphQLImage;LX/0wY;LX/0W9;LX/0SG;DJ)V
    .locals 8

    .prologue
    .line 464201
    if-nez p1, :cond_0

    .line 464202
    :goto_0
    return-void

    .line 464203
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    if-nez v0, :cond_1

    .line 464204
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    .line 464205
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->q:LX/0W9;

    if-nez v0, :cond_2

    .line 464206
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->q:LX/0W9;

    .line 464207
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->s:LX/0SG;

    if-nez v0, :cond_3

    .line 464208
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->s:LX/0SG;

    .line 464209
    :cond_3
    const v0, 0x7f0d27d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 464210
    const v0, 0x7f0d27da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 464211
    new-instance v0, LX/CC0;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p6

    move-wide/from16 v6, p8

    invoke-direct/range {v0 .. v7}, LX/CC0;-><init>(Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;Lcom/facebook/graphql/model/GraphQLImage;DJ)V

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->o:LX/0wa;

    .line 464212
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->n:LX/0wY;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->o:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V
    .locals 6
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464192
    invoke-static {p1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464193
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 464194
    :goto_0
    return-void

    .line 464195
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 464196
    if-lez p2, :cond_1

    .line 464197
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const-wide v2, 0x3fe999999999999aL    # 0.8

    int-to-double v4, p2

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setWidth(I)V

    .line 464198
    :cond_1
    if-eqz p3, :cond_2

    .line 464199
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464200
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 464172
    invoke-static {p1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464173
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 464174
    :goto_0
    return-void

    .line 464175
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 464176
    sget-object v0, LX/CBj;->BRANDING_BOTTOM_DIVIDER:LX/CBj;

    .line 464177
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464178
    sget-object v5, LX/CBr;->NEWSFEED_BRANDING_STYLE:LX/CBr;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object p1

    if-ne v5, p1, :cond_2

    .line 464179
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CBj;->fromString(Ljava/lang/String;)LX/CBj;

    move-result-object v1

    .line 464180
    :goto_2
    move-object v1, v1

    .line 464181
    if-ne v0, v1, :cond_1

    .line 464182
    const/4 v4, 0x0

    .line 464183
    invoke-virtual {p0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020839

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 464184
    invoke-virtual {p0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cdd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 464185
    move-object v0, v0

    .line 464186
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v3, v0, v3, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 464187
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setCompoundDrawablePadding(I)V

    .line 464188
    :goto_3
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0

    .line 464189
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 464190
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 464191
    :cond_3
    sget-object v1, LX/CBj;->UNKNOWN:LX/CBj;

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/graphql/model/GraphQLImage;I)V
    .locals 7

    .prologue
    .line 464156
    iput p3, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    .line 464157
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    if-nez v0, :cond_0

    .line 464158
    const v0, 0x7f0d27d5

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 464159
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 464160
    :cond_0
    const v0, 0x7f0d27d6

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    .line 464161
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->g:Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;

    .line 464162
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    .line 464163
    invoke-virtual {v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 464164
    iget v4, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    move v4, v4

    .line 464165
    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 464166
    iget v4, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->l:I

    move v4, v4

    .line 464167
    int-to-double v5, v4

    div-double/2addr v5, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v4, v5

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 464168
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionFbVideoView;->setVisibility(I)V

    .line 464169
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;->a(Ljava/lang/String;Ljava/lang/String;D)V

    .line 464170
    invoke-virtual {v0}, LX/2oW;->jj_()V

    .line 464171
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V
    .locals 6
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464147
    invoke-static {p1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464148
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 464149
    :goto_0
    return-void

    .line 464150
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 464151
    if-lez p2, :cond_1

    .line 464152
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const-wide v2, 0x3fe999999999999aL    # 0.8

    int-to-double v4, p2

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setWidth(I)V

    .line 464153
    :cond_1
    if-eqz p3, :cond_2

    .line 464154
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464155
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 464145
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 464146
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBrandingImage(Lcom/facebook/graphql/model/GraphQLImage;)V
    .locals 3

    .prologue
    .line 464132
    if-nez p1, :cond_1

    .line 464133
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 464134
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464135
    :cond_0
    :goto_0
    return-void

    .line 464136
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_2

    .line 464137
    const v0, 0x7f0d27de

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 464138
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 464139
    :cond_2
    const v0, 0x7f0d27df

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 464140
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 464141
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 464142
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 464143
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 464144
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 464129
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->h:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464130
    return-void

    .line 464131
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
