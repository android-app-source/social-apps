.class public final Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field public final c:LX/2mJ;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460495
    const v0, 0x7f0310cf

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/TextAppearancePartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460487
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460488
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 460489
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->c:LX/2mJ;

    .line 460490
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->d:LX/0Ot;

    .line 460491
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460492
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 460493
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 460494
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;
    .locals 10

    .prologue
    .line 460476
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    monitor-enter v1

    .line 460477
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460478
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460479
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460480
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460481
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v5

    check-cast v5, LX/2mJ;

    const/16 v6, 0x104c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;)V

    .line 460482
    move-object v0, v3

    .line 460483
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460484
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460485
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460475
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 460461
    check-cast p2, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460462
    invoke-static {p2}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 460463
    invoke-static {p2}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    .line 460464
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    .line 460465
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    new-instance v3, LX/Ans;

    invoke-static {p2}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v4

    invoke-direct {v3, v4}, LX/Ans;-><init>(Z)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460466
    const v2, 0x7f0d175f

    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460467
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/CBQ;

    invoke-direct {v3, p0, v0, v1}, LX/CBQ;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460468
    sget-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    .line 460469
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 460470
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->e(LX/0Px;)LX/CBp;

    move-result-object v0

    .line 460471
    :cond_0
    sget-object v1, LX/CBR;->a:[I

    invoke-virtual {v0}, LX/CBp;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 460472
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    const v2, 0x7f0e0a36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 460473
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 460474
    :pswitch_0
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    const v2, 0x7f0e0a35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 460459
    check-cast p1, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460460
    invoke-static {p1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    return v0
.end method
