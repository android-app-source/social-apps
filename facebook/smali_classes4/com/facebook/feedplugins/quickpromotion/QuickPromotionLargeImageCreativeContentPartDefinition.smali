.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "LX/CBJ;",
        "TE;",
        "Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final c:LX/1Ua;

.field private static p:LX/0Xm;


# instance fields
.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final f:LX/2mJ;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1Bv;

.field private final j:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

.field private final k:LX/0wY;

.field private final l:LX/0W9;

.field private final m:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final o:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 459738
    new-instance v0, LX/2mI;

    invoke-direct {v0}, LX/2mI;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a:LX/1Cz;

    .line 459739
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40000000    # -2.0f

    .line 459740
    iput v1, v0, LX/1UY;->b:F

    .line 459741
    move-object v0, v0

    .line 459742
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/2mJ;LX/0Ot;LX/0Or;LX/1Bv;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0wY;LX/0W9;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;",
            "LX/1Bv;",
            "Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;",
            "LX/0wY;",
            "LX/0W9;",
            "Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;",
            "Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459743
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 459744
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 459745
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 459746
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->f:LX/2mJ;

    .line 459747
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->g:LX/0Ot;

    .line 459748
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->h:LX/0Or;

    .line 459749
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->i:LX/1Bv;

    .line 459750
    iput-object p7, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->j:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    .line 459751
    iput-object p8, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->k:LX/0wY;

    .line 459752
    iput-object p9, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->l:LX/0W9;

    .line 459753
    iput-object p10, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->m:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 459754
    iput-object p11, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 459755
    iput-object p12, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->o:LX/0SG;

    .line 459756
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBJ;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;)",
            "LX/CBJ;"
        }
    .end annotation

    .prologue
    .line 459765
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459766
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459767
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->c:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459768
    invoke-static {v0}, LX/2nN;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 459769
    const v1, 0x7f0d27d1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 459770
    :cond_0
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 459771
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    .line 459772
    invoke-static {v0}, LX/2nN;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v10

    .line 459773
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v3

    .line 459774
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_7

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 459775
    sget-object v7, LX/CBr;->NEWSFEED_LARGE_IMAGE_PROFILE_PHOTO:LX/CBr;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v8

    if-ne v7, v8, :cond_6

    .line 459776
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBo;->fromString(Ljava/lang/String;)LX/CBo;

    move-result-object v4

    .line 459777
    :goto_1
    move-object v11, v4

    .line 459778
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 459779
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 459780
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->u()Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    move-result-object v12

    .line 459781
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-static {v5}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    .line 459782
    :goto_2
    if-eqz v6, :cond_1

    .line 459783
    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->j:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    new-instance v7, LX/36r;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    invoke-virtual {p2, v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, LX/36r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v5, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459784
    :cond_1
    iget-object v7, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->f:LX/2mJ;

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/13P;

    invoke-static {v0, v7, v5}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;LX/2mJ;LX/13P;)Landroid/view/View$OnClickListener;

    move-result-object v9

    .line 459785
    if-eqz v12, :cond_2

    .line 459786
    const v0, 0x7f0d27d9

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->m:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    const-string v7, ""

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 459787
    const v0, 0x7f0d27da

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->n:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    const-string v7, ""

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 459788
    :cond_2
    new-instance v0, LX/CBJ;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_3
    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-direct/range {v0 .. v12}, LX/CBJ;-><init>(Lcom/facebook/graphql/model/GraphQLQuickPromotion;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Landroid/view/View$OnClickListener;ZLX/CBo;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V

    return-object v0

    .line 459789
    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    :cond_5
    move-object v5, v6

    .line 459790
    goto :goto_3

    .line 459791
    :cond_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_0

    .line 459792
    :cond_7
    sget-object v4, LX/CBo;->UNKNOWN:LX/CBo;

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;
    .locals 3

    .prologue
    .line 459757
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    monitor-enter v1

    .line 459758
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459759
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459760
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459761
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459762
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459763
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CBJ;LX/1Ps;Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;",
            "LX/CBJ;",
            "TE;",
            "Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 459701
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sd;

    .line 459702
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 459703
    iget-object v1, p2, LX/CBJ;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v2, p2, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v0, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V

    .line 459704
    iget-object v1, p2, LX/CBJ;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v2, p2, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v0, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V

    .line 459705
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->b:LX/0Or;

    .line 459706
    iput-object v1, p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->f:LX/0Or;

    .line 459707
    iget-object v1, p2, LX/CBJ;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v2, p2, LX/CBJ;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)V

    .line 459708
    iget-object v1, p2, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 459709
    iget-object v1, p2, LX/CBJ;->d:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setBrandingImage(Lcom/facebook/graphql/model/GraphQLImage;)V

    .line 459710
    iget-object v1, p2, LX/CBJ;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v1

    .line 459711
    if-eqz v1, :cond_0

    .line 459712
    invoke-virtual {p4}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0e83

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 459713
    invoke-virtual {p4}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/2nN;->a(LX/0Px;ILandroid/content/res/Resources;)I

    move-result v2

    .line 459714
    iget-object v3, p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v4, p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getPaddingLeft()I

    move-result v4

    iget-object v5, p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getPaddingRight()I

    move-result v5

    iget-object v6, p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->i:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v6}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, v2, v5, v6}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setPadding(IIII)V

    .line 459715
    :cond_0
    iget-object v1, p2, LX/CBJ;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 459716
    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->i:LX/1Bv;

    invoke-virtual {v2}, LX/1Bv;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v1, :cond_9

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 459717
    if-eqz v1, :cond_6

    .line 459718
    iget-object v1, p2, LX/CBJ;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v2, p2, LX/CBJ;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {p4, v1, v2, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/graphql/model/GraphQLImage;I)V

    .line 459719
    :goto_1
    iget-object v0, p2, LX/CBJ;->k:LX/CBo;

    sget-object v1, LX/CBo;->CENTERED:LX/CBo;

    if-eq v0, v1, :cond_1

    iget-object v0, p2, LX/CBJ;->k:LX/CBo;

    sget-object v1, LX/CBo;->CENTERED_LARGE:LX/CBo;

    if-ne v0, v1, :cond_7

    :cond_1
    const/4 v0, 0x1

    .line 459720
    :goto_2
    if-eqz v0, :cond_2

    .line 459721
    iget-object v0, p2, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    iget-object v1, p2, LX/CBJ;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v2, p2, LX/CBJ;->k:LX/CBo;

    invoke-virtual {p4, v0, v1, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Landroid/view/View$OnClickListener;Lcom/facebook/graphql/model/GraphQLImage;LX/CBo;)V

    .line 459722
    :cond_2
    iget-object v0, p2, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    if-eqz v0, :cond_5

    .line 459723
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459724
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459725
    check-cast p3, LX/1Pr;

    new-instance v1, LX/CBE;

    invoke-direct {v1, v0}, LX/CBE;-><init>(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBD;

    .line 459726
    invoke-virtual {v0}, LX/CBD;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 459727
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->o:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 459728
    iput-wide v2, v0, LX/CBD;->a:J

    .line 459729
    :cond_3
    invoke-virtual {v0}, LX/CBD;->a()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_4

    .line 459730
    iget-object v1, p2, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->j()I

    move-result v1

    int-to-double v2, v1

    iget-object v1, p2, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->n()D

    move-result-wide v4

    iget-object v1, p2, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;->k()I

    move-result v1

    int-to-double v6, v1

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    .line 459731
    iput-wide v2, v0, LX/CBD;->b:D

    .line 459732
    :cond_4
    iget-object v1, p2, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    iget-object v2, p2, LX/CBJ;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->k:LX/0wY;

    iget-object v4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->l:LX/0W9;

    iget-object v5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->o:LX/0SG;

    invoke-virtual {v0}, LX/CBD;->a()D

    move-result-wide v6

    invoke-virtual {v0}, LX/CBD;->b()J

    move-result-wide v8

    move-object v0, p4

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;Lcom/facebook/graphql/model/GraphQLImage;LX/0wY;LX/0W9;LX/0SG;DJ)V

    .line 459733
    :cond_5
    iget-boolean v0, p2, LX/CBJ;->j:Z

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setMenuButtonActive(Z)V

    .line 459734
    return-void

    .line 459735
    :cond_6
    iget-object v1, p2, LX/CBJ;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v2, p2, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2, v0}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLImage;Landroid/view/View$OnClickListener;I)V

    goto/16 :goto_1

    .line 459736
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 459737
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;
    .locals 13

    .prologue
    .line 459684
    new-instance v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {p0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v3

    check-cast v3, LX/2mJ;

    const/16 v4, 0x104c

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1224

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v6

    check-cast v6, LX/1Bv;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    invoke-static {p0}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v8

    check-cast v8, LX/0wY;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/2mJ;LX/0Ot;LX/0Or;LX/1Bv;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;LX/0wY;LX/0W9;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/0SG;)V

    .line 459685
    const/16 v1, 0x12cb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 459686
    iput-object v1, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->b:LX/0Or;

    .line 459687
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 459700
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459699
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBJ;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x631631ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459698
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/CBJ;

    check-cast p3, LX/1Ps;

    check-cast p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CBJ;LX/1Ps;Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;)V

    const/16 v1, 0x1f

    const v2, -0x69617a07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 459691
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 459692
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459693
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459694
    if-nez v0, :cond_0

    move v0, v1

    .line 459695
    :goto_0
    return v0

    .line 459696
    :cond_0
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 459697
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, LX/2nM;->NEWSFEED_LARGE_IMAGE:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 459688
    check-cast p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;

    .line 459689
    invoke-virtual {p4}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a()V

    .line 459690
    return-void
.end method
