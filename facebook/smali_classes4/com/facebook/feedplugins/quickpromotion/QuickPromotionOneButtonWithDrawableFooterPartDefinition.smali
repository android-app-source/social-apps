.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "LX/CBT;",
        "LX/1Ps;",
        "LX/Ao3;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field public final e:LX/2mJ;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final h:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 460515
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 460516
    iput v1, v0, LX/1UY;->b:F

    .line 460517
    move-object v0, v0

    .line 460518
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 460519
    iput v1, v0, LX/1UY;->c:F

    .line 460520
    move-object v0, v0

    .line 460521
    const/high16 v1, -0x3ee00000    # -10.0f

    .line 460522
    iput v1, v0, LX/1UY;->d:F

    .line 460523
    move-object v0, v0

    .line 460524
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->b:LX/1Ua;

    .line 460525
    new-instance v0, LX/2mS;

    invoke-direct {v0}, LX/2mS;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460526
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 460527
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 460528
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 460529
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->e:LX/2mJ;

    .line 460530
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->f:LX/0Ot;

    .line 460531
    iput-object p5, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 460532
    iput-object p6, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460533
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;
    .locals 10

    .prologue
    .line 460534
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    monitor-enter v1

    .line 460535
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460536
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460537
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460538
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460539
    new-instance v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v6

    check-cast v6, LX/2mJ;

    const/16 v7, 0x104c

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;LX/2mJ;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V

    .line 460540
    move-object v0, v3

    .line 460541
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460542
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460543
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 460545
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 460546
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 460547
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460548
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460549
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 460550
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    .line 460551
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    if-eqz v3, :cond_1

    sget-object v3, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 460552
    invoke-static {v0}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    .line 460553
    :goto_0
    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->b:LX/1Ua;

    invoke-direct {v4, p2, v5, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460554
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/CBS;

    invoke-direct {v3, p0, v2, v1}, LX/CBS;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 460555
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->e(LX/0Px;)LX/CBp;

    move-result-object v0

    .line 460556
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2nN;->c(LX/0Px;)LX/2nO;

    move-result-object v1

    .line 460557
    new-instance v3, LX/CBT;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2, v0, v1}, LX/CBT;-><init>(Ljava/lang/String;LX/CBp;LX/2nO;)V

    return-object v3

    .line 460558
    :cond_0
    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_0

    .line 460559
    :cond_1
    iget-object v3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    new-instance v4, LX/Ans;

    invoke-static {v0}, LX/2nN;->f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    invoke-direct {v4, v0}, LX/Ans;-><init>(Z)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x65ec2681

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460560
    check-cast p2, LX/CBT;

    check-cast p4, LX/Ao3;

    .line 460561
    iget-object v1, p2, LX/CBT;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/Ao3;->setFooterText(Ljava/lang/CharSequence;)V

    .line 460562
    iget-object v1, p2, LX/CBT;->b:LX/CBp;

    sget-object v2, LX/CBp;->PRIMARY:LX/CBp;

    if-ne v1, v2, :cond_3

    .line 460563
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->DISCLOSURE:LX/2nO;

    if-ne v1, v2, :cond_1

    .line 460564
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460565
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00d2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460566
    iget-object v2, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    const p0, 0x7f0207ed

    const p2, -0xa76f01

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460567
    iget-object v2, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460568
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x1792a06f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 460569
    :cond_1
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->SHARE:LX/2nO;

    if-ne v1, v2, :cond_2

    .line 460570
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460571
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00d2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460572
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const p0, 0x7f0209c7

    const p2, -0xa76f01

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460573
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460574
    goto :goto_0

    .line 460575
    :cond_2
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->COMPOSE:LX/2nO;

    if-ne v1, v2, :cond_0

    .line 460576
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460577
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00d2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460578
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const p0, 0x7f020810

    const p2, -0xa76f01

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460579
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460580
    goto :goto_0

    .line 460581
    :cond_3
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->DISCLOSURE:LX/2nO;

    if-ne v1, v2, :cond_4

    .line 460582
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460583
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a010e

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460584
    iget-object v2, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    const p0, 0x7f0207ed

    const p2, -0x6e685d

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460585
    iget-object v2, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460586
    goto/16 :goto_0

    .line 460587
    :cond_4
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->SHARE:LX/2nO;

    if-ne v1, v2, :cond_5

    .line 460588
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460589
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a010e

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460590
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const p0, 0x7f0209c7

    const p2, -0x6e685d

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460591
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460592
    goto/16 :goto_0

    .line 460593
    :cond_5
    iget-object v1, p2, LX/CBT;->c:LX/2nO;

    sget-object v2, LX/2nO;->COMPOSE:LX/2nO;

    if-ne v1, v2, :cond_0

    .line 460594
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    .line 460595
    iget-object v2, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00a3

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460596
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const p0, 0x7f020810

    const p2, -0x6f6b64

    invoke-virtual {v1, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460597
    iget-object v2, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460598
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 460599
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 460600
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 460601
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 460602
    if-nez v0, :cond_0

    move v0, v1

    .line 460603
    :goto_0
    return v0

    .line 460604
    :cond_0
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 460605
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v0}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->c(LX/0Px;)LX/2nO;

    move-result-object v0

    sget-object v3, LX/2nO;->UNKNOWN:LX/2nO;

    if-ne v0, v3, :cond_1

    sget-object v0, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    if-ne v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 460606
    check-cast p4, LX/Ao3;

    .line 460607
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/Ao3;->setFooterText(Ljava/lang/CharSequence;)V

    .line 460608
    iget-object v0, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->h:LX/0wM;

    const/16 p3, 0x8

    const p2, -0x6e685d

    .line 460609
    iget-object v1, p4, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, LX/Ao3;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0a010e

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 460610
    iget-object v1, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    const p0, 0x7f0209c7

    invoke-virtual {v0, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460611
    iget-object v1, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    const p0, 0x7f0207ed

    invoke-virtual {v0, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460612
    iget-object v1, p4, LX/Ao3;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460613
    iget-object v1, p4, LX/Ao3;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460614
    return-void
.end method
