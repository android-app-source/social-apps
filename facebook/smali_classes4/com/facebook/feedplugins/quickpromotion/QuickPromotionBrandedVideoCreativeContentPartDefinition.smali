.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;",
        "LX/CBC;",
        "TE;",
        "Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 459935
    new-instance v0, LX/2mK;

    invoke-direct {v0}, LX/2mK;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->a:LX/1Cz;

    .line 459936
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40000000    # -2.0f

    .line 459937
    iput v1, v0, LX/1UY;->b:F

    .line 459938
    move-object v0, v0

    .line 459939
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;",
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459887
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 459888
    iput-object p1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 459889
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 459890
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->e:LX/0Or;

    .line 459891
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;
    .locals 6

    .prologue
    .line 459924
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    monitor-enter v1

    .line 459925
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459926
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459927
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459928
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459929
    new-instance v5, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    const/16 p0, 0x1224

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/0Or;)V

    .line 459930
    move-object v0, v5

    .line 459931
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459932
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459933
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459934
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 459923
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 459910
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459911
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459912
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459913
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 459914
    const v1, 0x7f0d27d1

    iget-object v2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 459915
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 459916
    invoke-static {v0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v0

    .line 459917
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 459918
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 459919
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 459920
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 459921
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 459922
    new-instance v0, LX/CBC;

    invoke-direct/range {v0 .. v6}, LX/CBC;-><init>(Lcom/facebook/graphql/model/GraphQLQuickPromotion;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x36c2c9fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459900
    check-cast p2, LX/CBC;

    check-cast p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;

    const/4 v2, 0x0

    const/4 p1, 0x0

    .line 459901
    iget-object v1, p2, LX/CBC;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p4, v1, v2, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V

    .line 459902
    iget-object v1, p2, LX/CBC;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p4, v1, v2, p1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;ILandroid/view/View$OnClickListener;)V

    .line 459903
    iget-object v1, p2, LX/CBC;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v2, p2, LX/CBC;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)V

    .line 459904
    iget-object v1, p2, LX/CBC;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setBrandingImage(Lcom/facebook/graphql/model/GraphQLImage;)V

    .line 459905
    iget-object v2, p2, LX/CBC;->b:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0sd;

    .line 459906
    iget p0, v1, LX/0sd;->c:I

    move v1, p0

    .line 459907
    invoke-virtual {p4, v2, p1, v1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a(Lcom/facebook/graphql/model/GraphQLImage;Landroid/view/View$OnClickListener;I)V

    .line 459908
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->setMenuButtonActive(Z)V

    .line 459909
    const/16 v1, 0x1f

    const v2, -0x1dfaaf78

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 459895
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 459896
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 459897
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 459898
    invoke-static {v0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    .line 459899
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, LX/2nM;->NEWSFEED_BRANDED_VIDEO:LX/2nM;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 459892
    check-cast p4, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;

    .line 459893
    invoke-virtual {p4}, Lcom/facebook/feedplugins/quickpromotion/ui/QuickPromotionLargeImageCreativeContentView;->a()V

    .line 459894
    return-void
.end method
