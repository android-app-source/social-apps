.class public Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366151
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 366152
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->c:LX/0Ot;

    .line 366153
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->a:LX/0Ot;

    .line 366154
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->b:LX/0Ot;

    .line 366155
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->d:LX/0Ot;

    .line 366156
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->e:LX/0Ot;

    .line 366157
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->f:LX/0Ot;

    .line 366158
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;
    .locals 10

    .prologue
    .line 366159
    const-class v1, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;

    monitor-enter v1

    .line 366160
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366161
    sput-object v2, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366162
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366163
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366164
    new-instance v3, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;

    const/16 v4, 0x7f2

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x7f5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x7d7

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x81e

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x7f8

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1e39

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 366165
    move-object v0, v3

    .line 366166
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366167
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366168
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 366170
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 366171
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->f:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->d:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 366172
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 366173
    const/4 v0, 0x1

    return v0
.end method
