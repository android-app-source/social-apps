.class public Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/2yY;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/36R;",
        "LX/1aZ;",
        "LX/1Pt;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1Ad;


# direct methods
.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498724
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 498725
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->a:LX/1Ad;

    .line 498726
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;
    .locals 4

    .prologue
    .line 498727
    const-class v1, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    monitor-enter v1

    .line 498728
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498729
    sput-object v2, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498732
    new-instance p0, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;-><init>(LX/1Ad;)V

    .line 498733
    move-object v0, p0

    .line 498734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 498717
    check-cast p2, LX/36R;

    check-cast p3, LX/1Pt;

    .line 498718
    iget-object v0, p2, LX/36R;->a:Lcom/facebook/graphql/model/GraphQLImage;

    if-nez v0, :cond_0

    .line 498719
    const/4 v0, 0x0

    .line 498720
    :goto_0
    return-object v0

    .line 498721
    :cond_0
    iget-object v0, p2, LX/36R;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 498722
    iget-object v1, p2, LX/36R;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 498723
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->a:LX/1Ad;

    iget-object v2, p2, LX/36R;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xfa3500e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 498714
    check-cast p2, LX/1aZ;

    .line 498715
    check-cast p4, LX/2yY;

    invoke-interface {p4, p2}, LX/2yY;->setSideImageController(LX/1aZ;)V

    .line 498716
    const/16 v1, 0x1f

    const v2, -0x60175f3f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 498712
    check-cast p4, LX/2yY;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/2yY;->setSideImageController(LX/1aZ;)V

    .line 498713
    return-void
.end method
