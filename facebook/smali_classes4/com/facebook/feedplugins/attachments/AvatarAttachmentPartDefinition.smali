.class public Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/36T;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/36S;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final j:LX/1qb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 498653
    const-class v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 498654
    const-class v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;LX/1qb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/36S;",
            ">;",
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;",
            "Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;",
            "LX/1qb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498643
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 498644
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 498645
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 498646
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->e:LX/0Ot;

    .line 498647
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 498648
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 498649
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    .line 498650
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    .line 498651
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->j:LX/1qb;

    .line 498652
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;
    .locals 12

    .prologue
    .line 498632
    const-class v1, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    monitor-enter v1

    .line 498633
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498634
    sput-object v2, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498637
    new-instance v3, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    const/16 v6, 0x7db

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v11

    check-cast v11, LX/1qb;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;LX/0Ot;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;LX/1qb;)V

    .line 498638
    move-object v0, v3

    .line 498639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 498631
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 498603
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const v5, 0x4059999a    # 3.4f

    .line 498604
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 498605
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 498606
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498607
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498608
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498609
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 498610
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 498611
    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 498612
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object v1, v2

    .line 498613
    :goto_1
    move-object v1, v1

    .line 498614
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->j:LX/1qb;

    invoke-virtual {v2, v1, v5}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 498615
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    new-instance v3, LX/36Q;

    sget-object v4, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, v1, v4, v5}, LX/36Q;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;F)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498616
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    new-instance v2, LX/36R;

    invoke-static {v0}, LX/36S;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    sget-object v3, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v0, v3}, LX/36R;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498617
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498618
    new-instance v1, LX/36T;

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36S;

    .line 498619
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 498620
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 498621
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 498622
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 498623
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 498624
    iget-object v4, v0, LX/36S;->a:LX/1Uj;

    invoke-virtual {v4}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 p1, 0x11

    invoke-virtual {v3, v4, v5, v2, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 498625
    :cond_1
    invoke-static {p2}, LX/36S;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 498626
    if-eqz v2, :cond_2

    .line 498627
    invoke-static {v3}, LX/1qb;->a(Landroid/text/SpannableStringBuilder;)V

    .line 498628
    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 498629
    :cond_2
    move-object v2, v3

    .line 498630
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36S;

    invoke-virtual {v0, p2}, LX/36S;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/36T;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v1

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70556f8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 498594
    check-cast p2, LX/36T;

    .line 498595
    move-object v1, p4

    check-cast v1, LX/2yX;

    iget-object v2, p2, LX/36T;->a:Ljava/lang/CharSequence;

    invoke-interface {v1, v2}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    .line 498596
    check-cast p4, LX/2yX;

    iget-object v1, p2, LX/36T;->b:Ljava/lang/CharSequence;

    invoke-interface {p4, v1}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    .line 498597
    const/16 v1, 0x1f

    const v2, 0x6284f464

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498602
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 498598
    const/4 v1, 0x0

    .line 498599
    move-object v0, p4

    check-cast v0, LX/2yX;

    invoke-interface {v0, v1}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    .line 498600
    check-cast p4, LX/2yX;

    invoke-interface {p4, v1}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    .line 498601
    return-void
.end method
