.class public Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1RB;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/ByA;",
        "TE;",
        "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;",
        ">;",
        "LX/1RB",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Landroid/content/Context;

.field private final c:LX/1V7;

.field public final d:LX/38w;

.field private final e:Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/1V7;Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;LX/38w;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521691
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 521692
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 521693
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->b:Landroid/content/Context;

    .line 521694
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->c:LX/1V7;

    .line 521695
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->e:Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    .line 521696
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->d:LX/38w;

    .line 521697
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 521698
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/ByA;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/ByA;"
        }
    .end annotation

    .prologue
    .line 521679
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521680
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521681
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->e:Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521682
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 521683
    if-eqz v1, :cond_0

    .line 521684
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->c:LX/1V7;

    invoke-virtual {v4}, LX/1V7;->h()LX/1Ua;

    move-result-object v4

    const v5, 0x7f020ae3

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 521685
    :cond_0
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 521686
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 521687
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->b:Landroid/content/Context;

    invoke-static {v2, v0}, LX/Bng;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/Bnf;

    move-result-object v0

    .line 521688
    const v2, 0x7f0d0da9

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    iget-object v4, v0, LX/Bnf;->a:Ljava/lang/String;

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 521689
    const v2, 0x7f0d0daa

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    iget-object v4, v0, LX/Bnf;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 521690
    new-instance v2, LX/ByA;

    iget-object v3, v0, LX/Bnf;->d:Ljava/util/Date;

    iget-object v4, v0, LX/Bnf;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v2, v3, v4, v0}, LX/ByA;-><init>(Ljava/util/Date;Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-object v2

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;
    .locals 3

    .prologue
    .line 521671
    const-class v1, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    monitor-enter v1

    .line 521672
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521673
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521674
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521675
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521676
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521677
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521678
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/ByA;LX/1Pq;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/ByA;",
            "TE;",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 521667
    iget-object v0, p2, LX/ByA;->a:Ljava/util/Date;

    invoke-virtual {p4, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 521668
    iget-object v0, p2, LX/ByA;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 521669
    new-instance v0, LX/By9;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/By9;-><init>(Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/ByA;)V

    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521670
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 521664
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->d()V

    .line 521665
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 521666
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 521662
    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 521663
    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;
    .locals 7

    .prologue
    .line 521653
    new-instance v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v3

    check-cast v3, LX/1V7;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    invoke-static {p0}, LX/38w;->b(LX/0QB;)LX/38w;

    move-result-object v5

    check-cast v5, LX/38w;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/1V7;Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;LX/38w;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;)V

    .line 521654
    return-object v0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 521660
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521661
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521659
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->k:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 521658
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/ByA;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x263de176

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 521657
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/ByA;

    check-cast p3, LX/1Pq;

    check-cast p4, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/ByA;LX/1Pq;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;)V

    const/16 v1, 0x1f

    const v2, 0x798b94d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 521656
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 521655
    check-cast p4, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;

    invoke-static {p4}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;)V

    return-void
.end method
