.class public Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/2v1;

.field private final f:LX/19m;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/2v1;LX/19m;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499110
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499111
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 499112
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->e:LX/2v1;

    .line 499113
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->f:LX/19m;

    .line 499114
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 499089
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499090
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499091
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499092
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->e:LX/2v1;

    const/4 v2, 0x0

    .line 499093
    new-instance v3, LX/Byi;

    invoke-direct {v3, v1}, LX/Byi;-><init>(LX/2v1;)V

    .line 499094
    iget-object v4, v1, LX/2v1;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Byh;

    .line 499095
    if-nez v4, :cond_0

    .line 499096
    new-instance v4, LX/Byh;

    invoke-direct {v4, v1}, LX/Byh;-><init>(LX/2v1;)V

    .line 499097
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Byh;->a$redex0(LX/Byh;LX/1De;IILX/Byi;)V

    .line 499098
    move-object v3, v4

    .line 499099
    move-object v2, v3

    .line 499100
    move-object v1, v2

    .line 499101
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 499102
    iget-object v2, v1, LX/Byh;->a:LX/Byi;

    iput-object v0, v2, LX/Byi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499103
    iget-object v2, v1, LX/Byh;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 499104
    move-object v0, v1

    .line 499105
    iget-object v1, v0, LX/Byh;->a:LX/Byi;

    iput-object p3, v1, LX/Byi;->b:LX/1Pf;

    .line 499106
    iget-object v1, v0, LX/Byh;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499107
    move-object v0, v0

    .line 499108
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499109
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;
    .locals 7

    .prologue
    .line 499078
    const-class v1, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499079
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499080
    sput-object v2, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499081
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499082
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499083
    new-instance p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/2v1;->a(LX/0QB;)LX/2v1;

    move-result-object v5

    check-cast v5, LX/2v1;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v6

    check-cast v6, LX/19m;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/2v1;LX/19m;)V

    .line 499084
    move-object v0, p0

    .line 499085
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499086
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499087
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499077
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499061
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499064
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 499065
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->f:LX/19m;

    .line 499066
    iget-boolean v2, v0, LX/19m;->i:Z

    move v0, v2

    .line 499067
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SphericalVideoFallbackCallToActionComponentPartDefinition;->f:LX/19m;

    .line 499068
    iget-boolean v2, v0, LX/19m;->d:Z

    move v0, v2

    .line 499069
    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 499070
    :goto_0
    return v0

    .line 499071
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499072
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499073
    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 499074
    goto :goto_0

    .line 499075
    :cond_2
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 499076
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, -0x5a250750

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 499062
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499063
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
