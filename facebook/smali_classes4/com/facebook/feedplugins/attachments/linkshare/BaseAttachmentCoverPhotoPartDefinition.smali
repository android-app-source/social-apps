.class public Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/2yA;

.field private final d:LX/1Ad;

.field private final e:Lcom/facebook/common/callercontext/CallerContext;

.field public final f:F

.field public final g:LX/0Uh;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481420
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0Ot;LX/1Ad;LX/03V;Lcom/facebook/common/callercontext/CallerContext;FLX/2yA;)V
    .locals 0
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/2yA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "LX/1Ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "F",
            "LX/2yA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481421
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 481422
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->d:LX/1Ad;

    .line 481423
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->b:LX/03V;

    .line 481424
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 481425
    iput p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->f:F

    .line 481426
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->c:LX/2yA;

    .line 481427
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->g:LX/0Uh;

    .line 481428
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->h:LX/0Ot;

    .line 481429
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 481430
    invoke-interface {p2, p0, p1}, LX/2yA;->a(Lcom/facebook/graphql/model/GraphQLMedia;F)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 481431
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 481432
    :cond_0
    const/4 v0, 0x0

    .line 481433
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 481434
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 481435
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481436
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481437
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 481438
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 481439
    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 481440
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->b:LX/03V;

    sget-object v3, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Story attachment without media "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 481441
    :goto_0
    return-object v0

    .line 481442
    :cond_0
    iget v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->f:F

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->c:LX/2yA;

    invoke-static {v3, v0, v4}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;

    move-result-object v4

    .line 481443
    if-nez v4, :cond_1

    move-object v0, v1

    .line 481444
    goto :goto_0

    .line 481445
    :cond_1
    invoke-static {p3}, LX/35u;->a(LX/1Po;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->g:LX/0Uh;

    sget v5, LX/2SU;->V:I

    invoke-virtual {v0, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->g:LX/0Uh;

    sget v5, LX/2SU;->U:I

    invoke-virtual {v0, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 481446
    goto :goto_0

    .line 481447
    :cond_3
    invoke-static {v4}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    move-object v0, p3

    .line 481448
    check-cast v0, LX/1Pt;

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v1, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 481449
    const/4 v5, 0x0

    .line 481450
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->g:LX/0Uh;

    const/16 v4, 0x4ae

    invoke-virtual {v0, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->g:LX/0Uh;

    const/16 v4, 0x4af

    invoke-virtual {v0, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-nez v0, :cond_5

    .line 481451
    :cond_4
    const/4 v0, 0x0

    .line 481452
    :goto_1
    move-object v2, v0

    .line 481453
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->d:LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 481454
    check-cast p3, LX/1Pp;

    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v2, v1, v3}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 481455
    :cond_5
    iget v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->f:F

    new-instance v4, LX/BzQ;

    invoke-direct {v4, p0}, LX/BzQ;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;)V

    invoke-static {v3, v0, v4}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;

    move-result-object v0

    .line 481456
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x70eb5e62

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 481457
    check-cast p2, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    .line 481458
    move-object v1, p4

    check-cast v1, LX/35q;

    invoke-interface {v1, p2}, LX/35q;->setLargeImageController(LX/1aZ;)V

    .line 481459
    check-cast p4, LX/35q;

    iget v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->f:F

    invoke-interface {p4, v1}, LX/35q;->setLargeImageAspectRatio(F)V

    .line 481460
    const/16 v1, 0x1f

    const v2, -0x547132cd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 481461
    check-cast p4, LX/35q;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/35q;->setLargeImageController(LX/1aZ;)V

    .line 481462
    return-void
.end method
