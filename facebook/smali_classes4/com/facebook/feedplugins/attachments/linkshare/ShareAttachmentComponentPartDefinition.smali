.class public Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final e:LX/35h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/35h",
            "<",
            "LX/1Pm;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2y3;

.field private final g:LX/2sO;

.field private final h:LX/1V4;

.field private final i:LX/1V0;

.field private final j:LX/35i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 491819
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/35h;LX/2y3;LX/35i;LX/2sO;LX/1V4;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491838
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 491839
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->e:LX/35h;

    .line 491840
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->f:LX/2y3;

    .line 491841
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->j:LX/35i;

    .line 491842
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->g:LX/2sO;

    .line 491843
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->h:LX/1V4;

    .line 491844
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->i:LX/1V0;

    .line 491845
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 491820
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v0

    .line 491821
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->e:LX/35h;

    const/4 v2, 0x0

    .line 491822
    new-instance v3, LX/C0F;

    invoke-direct {v3, v1}, LX/C0F;-><init>(LX/35h;)V

    .line 491823
    iget-object v4, v1, LX/35h;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C0E;

    .line 491824
    if-nez v4, :cond_0

    .line 491825
    new-instance v4, LX/C0E;

    invoke-direct {v4, v1}, LX/C0E;-><init>(LX/35h;)V

    .line 491826
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C0E;->a$redex0(LX/C0E;LX/1De;IILX/C0F;)V

    .line 491827
    move-object v3, v4

    .line 491828
    move-object v2, v3

    .line 491829
    move-object v1, v2

    .line 491830
    iget-object v2, v1, LX/C0E;->a:LX/C0F;

    iput-object p2, v2, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491831
    iget-object v2, v1, LX/C0E;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 491832
    move-object v1, v1

    .line 491833
    iget-object v2, v1, LX/C0E;->a:LX/C0F;

    iput-object p3, v2, LX/C0F;->b:LX/1Pm;

    .line 491834
    iget-object v2, v1, LX/C0E;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 491835
    move-object v1, v1

    .line 491836
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 491837
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->i:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;
    .locals 11

    .prologue
    .line 491807
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 491808
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491809
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491810
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491811
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491812
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/35h;->a(LX/0QB;)LX/35h;

    move-result-object v5

    check-cast v5, LX/35h;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v6

    check-cast v6, LX/2y3;

    invoke-static {v0}, LX/35i;->a(LX/0QB;)LX/35i;

    move-result-object v7

    check-cast v7, LX/35i;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v8

    check-cast v8, LX/2sO;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v9

    check-cast v9, LX/1V4;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v10

    check-cast v10, LX/1V0;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/35h;LX/2y3;LX/35i;LX/2sO;LX/1V4;LX/1V0;)V

    .line 491813
    move-object v0, v3

    .line 491814
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491815
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491816
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 491818
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 491806
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 491800
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491801
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->f:LX/2y3;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->j:LX/35i;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->g:LX/2sO;

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->h:LX/1V4;

    .line 491802
    iget-object p0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 491803
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 491804
    invoke-static {p0}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p1, v0, v3}, LX/35j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;LX/1V4;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p1, v2, v3}, LX/35k;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;LX/1V4;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p1, v1, v3}, LX/35l;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/35i;LX/1V4;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {v3}, LX/35k;->a(LX/1V4;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p1, v3}, LX/35m;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1V4;)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 491805
    return v0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 491798
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491799
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 491797
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
