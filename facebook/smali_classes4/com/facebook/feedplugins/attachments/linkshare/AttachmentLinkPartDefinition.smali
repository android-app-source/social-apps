.class public Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2ya;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2mt;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2yN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ye;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2yI;

.field private final i:LX/2yJ;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2yI;LX/2yJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/2mt;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/DefaultAttachmentLinkPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ye;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/2yI;",
            "LX/2yJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481463
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 481464
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a:LX/0ad;

    .line 481465
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->b:LX/0Ot;

    .line 481466
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->c:LX/0Ot;

    .line 481467
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->d:LX/0Ot;

    .line 481468
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->g:LX/0Ot;

    .line 481469
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->h:LX/2yI;

    .line 481470
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->i:LX/2yJ;

    .line 481471
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->i:LX/2yJ;

    invoke-virtual {v0}, LX/2yJ;->a()LX/2yN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->e:LX/2yN;

    .line 481472
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->f:LX/0Ot;

    .line 481473
    return-void
.end method

.method private a(LX/2ya;Ljava/lang/String;)LX/C8n;
    .locals 4

    .prologue
    .line 481474
    new-instance v1, LX/C8n;

    iget-object v2, p1, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yF;

    invoke-direct {v1, v2, v3, v0, p2}, LX/C8n;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;LX/2yF;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .locals 12

    .prologue
    .line 481475
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    monitor-enter v1

    .line 481476
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481477
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481478
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481479
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481480
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0xc7a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x972

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x200e

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x973

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xe0f

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/2yI;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/2yI;

    invoke-static {v0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v11

    check-cast v11, LX/2yJ;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;-><init>(LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2yI;LX/2yJ;)V

    .line 481481
    move-object v0, v3

    .line 481482
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481483
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481484
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481485
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 481486
    check-cast p2, LX/2ya;

    check-cast p3, LX/1Pq;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 481487
    iget-object v0, p2, LX/2ya;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v0, :cond_1

    .line 481488
    :cond_0
    :goto_0
    return-object v7

    .line 481489
    :cond_1
    iget-object v0, p2, LX/2ya;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2yb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 481490
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->h:LX/2yI;

    iget-object v2, p2, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2, v7, p3}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 481491
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mt;

    iget-object v3, p2, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v3}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v4

    .line 481492
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 481493
    invoke-static {v4}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481494
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/C8n;

    iget-object v3, p2, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2yF;

    invoke-direct {v2, v3, v7, v1, v4}, LX/C8n;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;LX/2yF;Ljava/lang/String;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 481495
    :cond_3
    iget-object v0, p2, LX/2ya;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 481496
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v3, v1

    .line 481497
    :goto_1
    if-eqz v3, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v6, :cond_5

    move v0, v1

    .line 481498
    :goto_2
    if-eqz v3, :cond_6

    if-nez v0, :cond_6

    .line 481499
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/2ya;Ljava/lang/String;)LX/C8n;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 481500
    goto :goto_1

    :cond_5
    move v0, v2

    .line 481501
    goto :goto_2

    .line 481502
    :cond_6
    if-eqz v0, :cond_7

    .line 481503
    const v1, 0x7f0d052e

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/2ya;Ljava/lang/String;)LX/C8n;

    move-result-object v2

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 481504
    :cond_7
    iget-object v0, p2, LX/2ya;->c:Ljava/util/Map;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->e:LX/2yN;

    move-object v2, v0

    .line 481505
    :goto_3
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/2yd;

    iget-object v5, p2, LX/2ya;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2yF;

    invoke-direct {v3, v5, v2, v1, v4}, LX/2yd;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;LX/2yF;Ljava/lang/String;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 481506
    :cond_8
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->i:LX/2yJ;

    iget-object v1, p2, LX/2ya;->c:Ljava/util/Map;

    .line 481507
    new-instance v2, LX/2yM;

    invoke-direct {v2, v0, v1}, LX/2yM;-><init>(LX/2yJ;Ljava/util/Map;)V

    move-object v0, v2

    .line 481508
    move-object v2, v0

    goto :goto_3
.end method
