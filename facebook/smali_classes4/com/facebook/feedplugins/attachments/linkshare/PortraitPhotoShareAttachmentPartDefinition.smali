.class public Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3Ve;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/2sO;

.field private final b:LX/1qa;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "LX/3Ve;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition",
            "<TE;",
            "LX/3Ve;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2sO;LX/1qa;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481841
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 481842
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->a:LX/2sO;

    .line 481843
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->b:LX/1qa;

    .line 481844
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 481845
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    .line 481846
    const v0, 0x7f0b00c5

    invoke-static {p1, v0}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->e:I

    .line 481847
    const v0, 0x7f0b00c5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->f:I

    .line 481848
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 481849
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;

    monitor-enter v1

    .line 481850
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481851
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481852
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481853
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481854
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v5

    check-cast v5, LX/2sO;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v6

    check-cast v6, LX/1qa;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;LX/2sO;LX/1qa;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;)V

    .line 481855
    move-object v0, v3

    .line 481856
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481857
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481858
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 481860
    sget-object v0, LX/3Ve;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 481861
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481862
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481863
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481864
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481865
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    new-instance v2, LX/C0G;

    iget v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->e:I

    invoke-direct {v2, v0, v3}, LX/C0G;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481866
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 481867
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481868
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 481869
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 481870
    if-nez v2, :cond_2

    .line 481871
    :cond_0
    :goto_0
    move v0, v0

    .line 481872
    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->f:I

    const/4 v2, 0x0

    .line 481873
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PortraitPhotoShareAttachmentPartDefinition;->a:LX/2sO;

    invoke-virtual {v1, p1}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 481874
    :goto_1
    move v0, v1

    .line 481875
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 481876
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 481877
    goto :goto_0

    .line 481878
    :cond_3
    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 481879
    if-eqz v2, :cond_0

    .line 481880
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 481881
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x25d6af

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 481882
    :cond_4
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 481883
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 481884
    if-nez v1, :cond_5

    move v1, v2

    .line 481885
    goto :goto_1

    .line 481886
    :cond_5
    invoke-static {v1, v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 481887
    if-eqz v1, :cond_6

    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLImage;)F

    move-result v1

    const v3, 0x3f4ccccd    # 0.8f

    cmpg-float v1, v1, v3

    if-gez v1, :cond_6

    const/4 v1, 0x1

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_1
.end method
