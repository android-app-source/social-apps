.class public Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/2yW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xc;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1xc;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480581
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 480582
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a:LX/0Ot;

    .line 480583
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->b:LX/0Ot;

    .line 480584
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->e:LX/0Ot;

    .line 480585
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->d:LX/0Ot;

    .line 480586
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->f:LX/0Ot;

    .line 480587
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->c:LX/0Ot;

    .line 480588
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 480589
    const v0, 0x7f020a45

    invoke-static {p0, v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/1X6;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/1X6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 480590
    new-instance v0, LX/1X6;

    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/1Ua;->g:LX/1Ua;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, p1, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    return-object v0
.end method

.method public static a(LX/1qb;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 480591
    invoke-virtual {p0, p1}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 480592
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    monitor-enter v1

    .line 480593
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480594
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480595
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480596
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480597
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    const/16 v4, 0x819

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x7fe

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x781

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x959

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x81a

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x756

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 480598
    move-object v0, v3

    .line 480599
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480600
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480601
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 480603
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xc;

    invoke-virtual {v0, p1, p2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    :cond_0
    return-object p2
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 480604
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    const/4 v6, 0x0

    .line 480605
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 480606
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480607
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480608
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v4, LX/2yZ;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qb;

    invoke-static {v2, v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/1qb;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v2

    invoke-direct {p0, v3, v2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v5, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v4, v2, v0}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480609
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480610
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480611
    check-cast p3, LX/1Pr;

    new-instance v0, LX/2yU;

    invoke-direct {v0, p2}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p3, v0, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yV;

    .line 480612
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 480613
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    .line 480614
    iget-boolean v2, v0, LX/2yV;->b:Z

    move v0, v2

    .line 480615
    if-eqz v0, :cond_1

    .line 480616
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->l:LX/1Ua;

    const v4, 0x7f020a48

    const/4 v5, -0x1

    invoke-direct {v0, v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    move-object v0, v0

    .line 480617
    :goto_0
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480618
    :cond_0
    return-object v6

    .line 480619
    :cond_1
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v0

    goto :goto_0
.end method
