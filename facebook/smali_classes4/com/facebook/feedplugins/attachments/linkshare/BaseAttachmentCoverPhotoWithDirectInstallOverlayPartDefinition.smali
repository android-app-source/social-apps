.class public Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BzZ;",
        "TE;",
        "LX/3Wn;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/1Ad;

.field private final c:LX/1nA;

.field public final d:LX/2v5;

.field public final e:LX/2v6;

.field private final f:Lcom/facebook/common/callercontext/CallerContext;

.field private final g:LX/2yA;

.field public final h:F


# direct methods
.method public constructor <init>(LX/0Zb;LX/1Ad;LX/1nA;LX/2v5;LX/2v6;Lcom/facebook/common/callercontext/CallerContext;FLX/2yA;)V
    .locals 0
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2yA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480885
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 480886
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a:LX/0Zb;

    .line 480887
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->b:LX/1Ad;

    .line 480888
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->c:LX/1nA;

    .line 480889
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->d:LX/2v5;

    .line 480890
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->e:LX/2v6;

    .line 480891
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 480892
    iput p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->h:F

    .line 480893
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->g:LX/2yA;

    .line 480894
    return-void
.end method

.method private a(LX/BzZ;LX/1Po;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/6Qd;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BzZ;",
            "TE;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/6Qd;"
        }
    .end annotation

    .prologue
    .line 480760
    new-instance v0, LX/BzR;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/BzR;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;LX/BzZ;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method private a(LX/BzZ;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BzZ;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 480761
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->c:LX/1nA;

    invoke-virtual {v0, p3, p5}, LX/1nA;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 480762
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 480763
    const-string v2, "cta_click"

    const-string v3, "1"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480764
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->c:LX/1nA;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 480765
    invoke-static {p3, p4, v0}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v5

    .line 480766
    new-instance v0, LX/3i6;

    const v2, 0x7f0d007f

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v3

    const v4, 0x7f0d0080

    invoke-direct/range {v0 .. v5}, LX/3i6;-><init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;)V

    .line 480767
    new-instance v2, LX/BzS;

    move-object v3, p0

    move-object v4, p1

    move-object v6, p2

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, LX/BzS;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;LX/BzZ;LX/47G;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method public static synthetic a(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/util/Map;
    .locals 3

    .prologue
    .line 480768
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 480769
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 480770
    if-eqz p2, :cond_3

    .line 480771
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 480772
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 480773
    :goto_1
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 480774
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0lF;->k()LX/0nH;

    move-result-object p0

    sget-object p1, LX/0nH;->STRING:LX/0nH;

    if-ne p0, p1, :cond_0

    invoke-virtual {v2}, LX/0lF;->q()Z

    move-result p0

    if-nez p0, :cond_1

    :cond_0
    invoke-virtual {v2}, LX/0lF;->k()LX/0nH;

    move-result-object p0

    sget-object p1, LX/0nH;->STRING:LX/0nH;

    if-eq p0, p1, :cond_5

    invoke-virtual {v2}, LX/0lF;->e()I

    move-result p0

    if-nez p0, :cond_5

    :cond_1
    const/4 p0, 0x1

    :goto_2
    move p0, p0

    .line 480775
    if-nez p0, :cond_2

    .line 480776
    const-string p0, "sponsored"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480777
    const-string v0, "tracking"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480778
    :cond_2
    move-object v0, v1

    .line 480779
    return-object v0

    .line 480780
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    const/4 p0, 0x0

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Ljava/lang/String;LX/47G;Ljava/util/Map;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/47G;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 480781
    if-eqz p2, :cond_0

    .line 480782
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, LX/47G;->s:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "package_name"

    iget-object v2, p2, LX/47G;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-object v1, p2, LX/47G;->l:Ljava/lang/String;

    .line 480783
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 480784
    move-object v0, v0

    .line 480785
    if-eqz p4, :cond_1

    .line 480786
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 480787
    :cond_0
    :goto_0
    return-void

    .line 480788
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 480789
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 480790
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 480791
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480792
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 480793
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 480794
    const v1, -0x1e53800c

    invoke-static {v3, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 480795
    iget v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->h:F

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->g:LX/2yA;

    invoke-static {v0, v1, v2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;

    move-result-object v0

    .line 480796
    if-nez v0, :cond_0

    .line 480797
    const/4 v1, 0x0

    .line 480798
    :goto_0
    return-object v1

    .line 480799
    :cond_0
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    move-object v0, p3

    .line 480800
    check-cast v0, LX/1Pt;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v1, v2}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 480801
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->b:LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    move-object v0, p3

    .line 480802
    check-cast v0, LX/1Pp;

    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v6, v1, v7}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object v0, p3

    .line 480803
    check-cast v0, LX/1Pr;

    new-instance v1, LX/BzY;

    invoke-direct {v1, v3}, LX/BzY;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {v0, v1, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BzZ;

    .line 480804
    iget-object v0, v1, LX/BzZ;->a:LX/6Qd;

    move-object v0, v0

    .line 480805
    if-nez v0, :cond_1

    .line 480806
    invoke-direct {p0, v1, p3, v5, p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a(LX/BzZ;LX/1Po;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/6Qd;

    move-result-object v0

    .line 480807
    iput-object v0, v1, LX/BzZ;->a:LX/6Qd;

    .line 480808
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->e:LX/2v6;

    invoke-virtual {v0}, LX/2v6;->a()V

    .line 480809
    iget-object v0, v1, LX/BzZ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 480810
    if-nez v0, :cond_2

    .line 480811
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->d:LX/2v5;

    invoke-static {v0, v3, v5}, LX/AEB;->a(LX/2v5;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 480812
    iput-object v0, v1, LX/BzZ;->f:Ljava/lang/String;

    .line 480813
    :cond_2
    iget-object v0, v1, LX/BzZ;->c:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-object v0, v0

    .line 480814
    if-nez v0, :cond_3

    .line 480815
    iput-object v2, v1, LX/BzZ;->c:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    .line 480816
    :cond_3
    iget-object v0, v1, LX/BzZ;->e:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 480817
    if-nez v0, :cond_4

    .line 480818
    new-instance v0, LX/BzT;

    invoke-direct {v0, p0, v3, v4, p2}, LX/BzT;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v0

    .line 480819
    iput-object v0, v1, LX/BzZ;->e:Landroid/view/View$OnClickListener;

    .line 480820
    :cond_4
    iget-object v0, v1, LX/BzZ;->d:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 480821
    if-nez v0, :cond_5

    move-object v0, p0

    move-object v2, p2

    .line 480822
    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a(LX/BzZ;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 480823
    iput-object v0, v1, LX/BzZ;->d:Landroid/view/View$OnClickListener;

    .line 480824
    :cond_5
    iget-object v0, v1, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v0

    .line 480825
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-eq v0, v2, :cond_6

    .line 480826
    iget-object v0, v1, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v0

    .line 480827
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-ne v0, v2, :cond_7

    .line 480828
    :cond_6
    iput-boolean v8, v1, LX/BzZ;->g:Z

    .line 480829
    iput-boolean v9, v1, LX/BzZ;->h:Z

    .line 480830
    :goto_1
    check-cast p3, LX/1Pr;

    new-instance v0, LX/BzY;

    invoke-direct {v0, v3}, LX/BzY;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 480831
    :cond_7
    iget-object v0, v1, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v0

    .line 480832
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-ne v0, v2, :cond_8

    .line 480833
    iput-boolean v8, v1, LX/BzZ;->g:Z

    .line 480834
    iput-boolean v8, v1, LX/BzZ;->h:Z

    .line 480835
    goto :goto_1

    .line 480836
    :cond_8
    iput-boolean v9, v1, LX/BzZ;->g:Z

    .line 480837
    iput-boolean v8, v1, LX/BzZ;->h:Z

    .line 480838
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3e0dc878

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 480839
    check-cast p2, LX/BzZ;

    check-cast p4, LX/3Wn;

    const/4 v2, 0x1

    const/4 p1, 0x0

    .line 480840
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->e:LX/2v6;

    .line 480841
    iget-object p3, p2, LX/BzZ;->a:LX/6Qd;

    move-object p3, p3

    .line 480842
    invoke-virtual {v1, p3}, LX/2v6;->a(LX/6Qd;)V

    .line 480843
    iget-object v1, p2, LX/BzZ;->c:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-object v1, v1

    .line 480844
    invoke-virtual {p4, v1}, LX/3Wn;->setLargeImageController(LX/1aZ;)V

    .line 480845
    iget v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->h:F

    invoke-virtual {p4, v1}, LX/3Wn;->setLargeImageAspectRatio(F)V

    .line 480846
    iget-object v1, p2, LX/BzZ;->e:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 480847
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz p3, :cond_0

    .line 480848
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    invoke-virtual {p3, v1}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 480849
    :cond_0
    iget-object v1, p2, LX/BzZ;->d:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 480850
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz p3, :cond_1

    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480851
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object p3, p0

    .line 480852
    if-eqz p3, :cond_1

    .line 480853
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480854
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object p3, p0

    .line 480855
    invoke-virtual {p3, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 480856
    :cond_1
    iget-object v1, p2, LX/BzZ;->f:Ljava/lang/String;

    move-object v1, v1

    .line 480857
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz p3, :cond_2

    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480858
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->g:Landroid/widget/TextView;

    move-object p3, p0

    .line 480859
    if-eqz p3, :cond_2

    .line 480860
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480861
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->g:Landroid/widget/TextView;

    move-object p3, p0

    .line 480862
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480863
    :cond_2
    iget-boolean v1, p2, LX/BzZ;->h:Z

    move v1, v1

    .line 480864
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz p3, :cond_3

    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480865
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object p3, p0

    .line 480866
    if-eqz p3, :cond_3

    .line 480867
    iget-object p3, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 480868
    iget-object p0, p3, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object p3, p0

    .line 480869
    invoke-virtual {p3, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setEnabled(Z)V

    .line 480870
    :cond_3
    iget-boolean v1, p2, LX/BzZ;->g:Z

    move v1, v1

    .line 480871
    if-nez v1, :cond_5

    move v1, v2

    :goto_0
    invoke-virtual {p4, v1}, LX/3Wn;->setEnabled(Z)V

    .line 480872
    iget-boolean v1, p2, LX/BzZ;->g:Z

    move v1, v1

    .line 480873
    if-nez v1, :cond_6

    .line 480874
    :goto_1
    iget-object v1, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    if-eqz v1, :cond_4

    .line 480875
    iget-object v1, p4, LX/3Wn;->b:Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setEnabled(Z)V

    .line 480876
    :cond_4
    const/16 v1, 0x1f

    const v2, 0x4afc2763    # 8262577.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_5
    move v1, p1

    .line 480877
    goto :goto_0

    :cond_6
    move v2, p1

    .line 480878
    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 480879
    check-cast p2, LX/BzZ;

    check-cast p4, LX/3Wn;

    .line 480880
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->e:LX/2v6;

    .line 480881
    iget-object v1, p2, LX/BzZ;->a:LX/6Qd;

    move-object v1, v1

    .line 480882
    invoke-virtual {v0, v1}, LX/2v6;->b(LX/6Qd;)V

    .line 480883
    invoke-virtual {p4}, LX/35n;->a()V

    .line 480884
    return-void
.end method
