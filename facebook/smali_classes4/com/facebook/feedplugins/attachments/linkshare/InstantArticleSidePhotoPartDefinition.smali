.class public Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3Vc;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "LX/3Vc;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:LX/1V4;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1V4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;",
            ">;",
            "LX/1V4;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481835
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 481836
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->a:LX/0Ot;

    .line 481837
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->b:LX/0Ot;

    .line 481838
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->c:LX/1V4;

    .line 481839
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;
    .locals 6

    .prologue
    .line 481824
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;

    monitor-enter v1

    .line 481825
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481826
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481827
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481828
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481829
    new-instance v4, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;

    const/16 v3, 0x7ff

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1e4e

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v3

    check-cast v3, LX/1V4;

    invoke-direct {v4, v5, p0, v3}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/1V4;)V

    .line 481830
    move-object v0, v4

    .line 481831
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481832
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481833
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481834
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 481819
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481820
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481821
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    .line 481822
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 481823
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 481840
    sget-object v0, LX/3Vc;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 481815
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481816
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481817
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481818
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 481814
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->c:LX/1V4;

    invoke-virtual {v0}, LX/1V4;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 481813
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
