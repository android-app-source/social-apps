.class public Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482025
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 482026
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    .line 482027
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->b:LX/0Uh;

    .line 482028
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;
    .locals 5

    .prologue
    .line 482029
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;

    monitor-enter v1

    .line 482030
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482031
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482032
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482033
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 482034
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;LX/0Uh;)V

    .line 482035
    move-object v0, p0

    .line 482036
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482037
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482038
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482039
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 482040
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 482041
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 482042
    new-instance v1, LX/2yh;

    invoke-direct {v1, p2}, LX/2yh;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yi;

    .line 482043
    iget-boolean v1, v0, LX/2yi;->a:Z

    move v0, v1

    .line 482044
    if-eqz v0, :cond_0

    .line 482045
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 482046
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 482047
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 482048
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_1

    .line 482049
    :cond_0
    :goto_0
    return v1

    .line 482050
    :cond_1
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    .line 482051
    if-eqz v3, :cond_0

    .line 482052
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v4, :cond_3

    :cond_2
    move v0, v2

    .line 482053
    :goto_1
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveGroupDefinition;->b:LX/0Uh;

    const/16 v5, 0x497

    invoke-virtual {v4, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->LINK:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    if-ne v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 482054
    goto :goto_1
.end method
