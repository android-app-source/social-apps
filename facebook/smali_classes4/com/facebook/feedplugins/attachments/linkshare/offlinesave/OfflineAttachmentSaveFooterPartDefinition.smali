.class public Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/2yi;",
        "TE;",
        "LX/3Uy;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/1Sj;

.field public final c:LX/0W3;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Sj;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482088
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 482089
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 482090
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->b:LX/1Sj;

    .line 482091
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->c:LX/0W3;

    .line 482092
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 482093
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;
    .locals 7

    .prologue
    .line 482077
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    monitor-enter v1

    .line 482078
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 482079
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 482080
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482081
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 482082
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v4

    check-cast v4, LX/1Sj;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Sj;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 482083
    move-object v0, p0

    .line 482084
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 482085
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482086
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 482087
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3Uy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482055
    sget-object v0, LX/3Uy;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 482066
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    .line 482067
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 482068
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 482069
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, p3

    .line 482070
    check-cast v0, LX/1Pr;

    new-instance v1, LX/2yh;

    invoke-direct {v1, p2}, LX/2yh;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v0, v1, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2yi;

    .line 482071
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482072
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 482073
    :goto_0
    iput-boolean v0, v2, LX/2yi;->b:Z

    .line 482074
    :cond_0
    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v0, LX/C0f;

    move-object v1, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/C0f;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;LX/2yi;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 482075
    return-object v2

    .line 482076
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x16c6a3d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 482057
    check-cast p2, LX/2yi;

    check-cast p4, LX/3Uy;

    .line 482058
    iget-object v1, p4, LX/3Uy;->c:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    .line 482059
    iget-object v1, p4, LX/3Uy;->b:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p4, LX/3Uy;->c:Landroid/widget/LinearLayout;

    .line 482060
    const v1, 0x7f0d1e67

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    iput-object v1, p4, LX/3Uy;->d:Lcom/facebook/resources/ui/FbButton;

    .line 482061
    invoke-static {p4}, LX/3Uy;->b(LX/3Uy;)V

    .line 482062
    :cond_0
    iget-boolean v1, p2, LX/2yi;->b:Z

    move v1, v1

    .line 482063
    iget-object v2, p4, LX/3Uy;->c:Landroid/widget/LinearLayout;

    if-nez v2, :cond_1

    .line 482064
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x76f83595

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 482065
    :cond_1
    iget-object p2, p4, LX/3Uy;->d:Lcom/facebook/resources/ui/FbButton;

    if-eqz v1, :cond_2

    const v2, 0x7f081a50

    :goto_1
    invoke-virtual {p2, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_0

    :cond_2
    const v2, 0x7f081a4f

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 482056
    const/4 v0, 0x1

    return v0
.end method
