.class public Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Bzd;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;",
            "LX/Bzd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final d:LX/2y3;

.field private final e:LX/1qb;

.field public final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481337
    new-instance v0, LX/2yG;

    invoke-direct {v0}, LX/2yG;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/1qb;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481338
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 481339
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 481340
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->d:LX/2y3;

    .line 481341
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 481342
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->e:LX/1qb;

    .line 481343
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->f:LX/0ad;

    .line 481344
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 481345
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;

    monitor-enter v1

    .line 481346
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481347
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481348
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481349
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481350
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v5

    check-cast v5, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v7

    check-cast v7, LX/1qb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/1qb;LX/0ad;)V

    .line 481351
    move-object v0, v3

    .line 481352
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481353
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481354
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481355
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/Bzd;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "LX/Bzd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 481356
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481357
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481358
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->e:LX/1qb;

    invoke-virtual {v1, v0}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v1

    .line 481359
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 481360
    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    .line 481361
    iget-object v3, p3, LX/Bzd;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481362
    iget-object v1, p3, LX/Bzd;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481363
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 481364
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 481365
    invoke-static {v3}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 481366
    if-eqz v3, :cond_7

    .line 481367
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 481368
    if-eqz v3, :cond_7

    .line 481369
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 481370
    :cond_0
    :goto_0
    move v0, v0

    .line 481371
    if-eqz v0, :cond_2

    .line 481372
    iget-object v0, p3, LX/Bzd;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 481373
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->f:LX/0ad;

    sget-short v1, LX/2yO;->a:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 481374
    if-eqz v0, :cond_1

    .line 481375
    iget-object v0, p3, LX/Bzd;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 481376
    iget-object v0, p3, LX/Bzd;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 481377
    invoke-virtual {p3}, LX/Bzd;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v3, 0x41800000    # 16.0f

    invoke-static {v1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 481378
    iget-object v1, p3, LX/Bzd;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 481379
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->f:LX/0ad;

    sget-short v1, LX/2yO;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 481380
    if-eqz v0, :cond_5

    .line 481381
    if-nez v2, :cond_3

    .line 481382
    :goto_2
    return-void

    .line 481383
    :cond_2
    iget-object v0, p3, LX/Bzd;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 481384
    goto :goto_1

    .line 481385
    :cond_3
    check-cast p2, LX/1Pr;

    new-instance v0, LX/Bzh;

    invoke-direct {v0, v2}, LX/Bzh;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p2, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bzi;

    .line 481386
    iget-boolean v1, v0, LX/Bzi;->a:Z

    move v1, v1

    .line 481387
    if-eqz v1, :cond_4

    .line 481388
    const/16 v1, 0xbb8

    .line 481389
    iget-object v7, p3, LX/Bzd;->h:Landroid/animation/ValueAnimator;

    const-wide/16 v9, 0x3e8

    invoke-virtual {v7, v9, v10}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 481390
    iget-object v7, p3, LX/Bzd;->h:Landroid/animation/ValueAnimator;

    new-instance v8, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v8}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 481391
    iget-object v7, p3, LX/Bzd;->h:Landroid/animation/ValueAnimator;

    new-instance v8, LX/Bzb;

    invoke-direct {v8, p3}, LX/Bzb;-><init>(LX/Bzd;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 481392
    iget-object v7, p3, LX/Bzd;->h:Landroid/animation/ValueAnimator;

    new-instance v8, LX/Bzc;

    invoke-direct {v8, p3}, LX/Bzc;-><init>(LX/Bzd;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 481393
    iget-object v3, p3, LX/Bzd;->i:Ljava/lang/Runnable;

    int-to-long v5, v1

    invoke-virtual {p3, v3, v5, v6}, LX/Bzd;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 481394
    const/4 v1, 0x0

    .line 481395
    iput-boolean v1, v0, LX/Bzi;->a:Z

    .line 481396
    goto :goto_2

    .line 481397
    :cond_4
    invoke-virtual {p3}, LX/Bzd;->d()V

    goto :goto_2

    .line 481398
    :cond_5
    invoke-virtual {p3}, LX/Bzd;->d()V

    goto :goto_2

    .line 481399
    :cond_6
    invoke-static {v3}, LX/16z;->r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-gtz v4, :cond_0

    invoke-static {v3}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-gtz v3, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 481400
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 481401
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 481402
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481403
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481404
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481405
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x54e53249

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 481406
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/Bzd;

    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/Bzd;)V

    const/16 v1, 0x1f

    const v2, 0xf255813

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 481407
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481408
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481409
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481410
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 481411
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 481412
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->f:LX/0ad;

    sget-short v3, LX/2yO;->c:S

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 481413
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->d:LX/2y3;

    invoke-virtual {v1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
