.class public Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ":",
        "LX/35r;",
        ":",
        "LX/3VQ;",
        ":",
        "LX/3VR;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field public final a:LX/0iA;

.field private final b:Landroid/content/Context;

.field public final c:LX/0So;

.field public final d:LX/2v4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition",
            "<TV;>.IconClick",
            "Listener;"
        }
    .end annotation
.end field

.field private final e:LX/2yP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition",
            "<TV;>.TooltipOnCancel",
            "Listener;"
        }
    .end annotation
.end field

.field public f:LX/0hs;

.field public g:Z

.field public h:J

.field public i:LX/0i1;

.field public j:J

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public final o:Landroid/graphics/Rect;

.field public final p:LX/0hB;

.field public q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0iA;LX/0So;LX/0hB;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 481796
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 481797
    new-instance v0, LX/2v4;

    invoke-direct {v0, p0}, LX/2v4;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->d:LX/2v4;

    .line 481798
    new-instance v0, LX/2yP;

    invoke-direct {v0, p0}, LX/2yP;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->e:LX/2yP;

    .line 481799
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    .line 481800
    iput-boolean v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->q:Z

    .line 481801
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b:Landroid/content/Context;

    .line 481802
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a:LX/0iA;

    .line 481803
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->c:LX/0So;

    .line 481804
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->p:LX/0hB;

    .line 481805
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->h:J

    .line 481806
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;
    .locals 7

    .prologue
    .line 481741
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    monitor-enter v1

    .line 481742
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481743
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481746
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;-><init>(Landroid/content/Context;LX/0iA;LX/0So;LX/0hB;)V

    .line 481747
    move-object v0, p0

    .line 481748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)LX/0hs;
    .locals 4

    .prologue
    .line 481782
    new-instance v0, LX/0hs;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 481783
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 481784
    const v2, 0x7f080d22

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 481785
    const v2, 0x7f080d23

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 481786
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->e:LX/2yP;

    .line 481787
    iput-object v2, v0, LX/0ht;->I:LX/2yQ;

    .line 481788
    const v2, 0x7f020cdf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 481789
    invoke-virtual {v0, v2}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 481790
    const v2, 0x7f0b062b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    const v3, 0x7f0b062b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 481791
    iget-object v3, v0, LX/0hs;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v3, v2, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 481792
    const/4 v1, -0x1

    .line 481793
    iput v1, v0, LX/0hs;->t:I

    .line 481794
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 481795
    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 481773
    move-object v0, p1

    check-cast v0, LX/3VR;

    invoke-interface {v0}, LX/3VR;->getTooltipAnchor()Landroid/view/View;

    move-result-object v0

    .line 481774
    if-nez v0, :cond_0

    .line 481775
    :goto_0
    return-void

    .line 481776
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 481777
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 481778
    iput-wide v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->j:J

    .line 481779
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iput v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->k:I

    .line 481780
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iput v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->l:I

    .line 481781
    new-instance v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;Landroid/view/View;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 481766
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 481767
    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->g:Z

    .line 481768
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a:LX/0iA;

    sget-object v2, LX/CnD;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/CnD;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->i:LX/0i1;

    .line 481769
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->i:LX/0i1;

    if-nez v1, :cond_0

    .line 481770
    :goto_0
    return-object v4

    .line 481771
    :cond_0
    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)LX/0hs;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->f:LX/0hs;

    .line 481772
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->f:LX/0hs;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->g:Z

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6001f486

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 481758
    move-object v1, p4

    check-cast v1, LX/3VQ;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/3VQ;->setCoverPhotoArticleIconVisibility(I)V

    move-object v1, p4

    .line 481759
    check-cast v1, LX/3VR;

    invoke-interface {v1}, LX/3VR;->getTooltipAnchor()Landroid/view/View;

    move-result-object v1

    .line 481760
    if-eqz v1, :cond_0

    .line 481761
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->d:LX/2v4;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481762
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->q:Z

    .line 481763
    iget-boolean v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->g:Z

    if-eqz v1, :cond_1

    .line 481764
    invoke-static {p0, p4}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b$redex0(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;)V

    .line 481765
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x4c138ed6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 481752
    move-object v0, p4

    check-cast v0, LX/3VQ;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, LX/3VQ;->setCoverPhotoArticleIconVisibility(I)V

    .line 481753
    check-cast p4, LX/3VR;

    invoke-interface {p4}, LX/3VR;->getTooltipAnchor()Landroid/view/View;

    move-result-object v0

    .line 481754
    if-eqz v0, :cond_0

    .line 481755
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481756
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->q:Z

    .line 481757
    return-void
.end method
