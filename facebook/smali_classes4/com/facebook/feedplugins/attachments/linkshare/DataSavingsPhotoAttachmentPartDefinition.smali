.class public Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tK;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0tK;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;",
            ">;",
            "LX/0tK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481155
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 481156
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->a:LX/0Ot;

    .line 481157
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->b:LX/0Ot;

    .line 481158
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->c:LX/0tK;

    .line 481159
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->d:LX/0Ot;

    .line 481160
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 481161
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 481162
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481163
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481164
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481165
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481166
    new-instance v4, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;

    const/16 v3, 0x1e65

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1e66

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v3

    check-cast v3, LX/0tK;

    const/16 p0, 0x809

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v6, v3, p0}, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0tK;LX/0Ot;)V

    .line 481167
    move-object v0, v4

    .line 481168
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481169
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481170
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481171
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 481172
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481173
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;

    invoke-virtual {v0, p2}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleSidePhotoPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481174
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->d:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 481175
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 481176
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->c:LX/0tK;

    invoke-virtual {v0}, LX/0tK;->b()LX/49B;

    move-result-object v0

    sget-object v1, LX/49B;->MEDIUM:LX/49B;

    if-ne v0, v1, :cond_1

    .line 481177
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0

    .line 481178
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->a:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 481179
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 481180
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->c:LX/0tK;

    invoke-virtual {v0, v1}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->c:LX/0tK;

    invoke-virtual {v0}, LX/0tK;->b()LX/49B;

    move-result-object v0

    sget-object v2, LX/49B;->NO_CHANGE:LX/49B;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/DataSavingsPhotoAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 481181
    goto :goto_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1
.end method
