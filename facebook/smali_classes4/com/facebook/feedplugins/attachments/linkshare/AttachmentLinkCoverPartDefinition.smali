.class public Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Cko;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481213
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->a:Ljava/lang/String;

    .line 481214
    new-instance v0, LX/2v8;

    invoke-direct {v0}, LX/2v8;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481215
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 481216
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->d:LX/0Ot;

    .line 481217
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->c:LX/0Ot;

    .line 481218
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->e:LX/0Ot;

    .line 481219
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->f:LX/0ad;

    .line 481220
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;
    .locals 7

    .prologue
    .line 481221
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;

    monitor-enter v1

    .line 481222
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481223
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481224
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481225
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481226
    new-instance v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;

    const/16 v3, 0x756

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1e4f

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x7fe

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v5, v6, p0, v3}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 481227
    move-object v0, v4

    .line 481228
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481229
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481230
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 481232
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 481233
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481234
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->b:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481235
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481236
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/Bzo;

    sget-object v2, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->a:Ljava/lang/String;

    invoke-direct {v1, p2, v2}, LX/Bzo;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481237
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 481238
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481239
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481240
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481241
    const/4 v1, 0x0

    .line 481242
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->f:LX/0ad;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->f:LX/0ad;

    sget-short p1, LX/2yD;->y:S

    invoke-interface {v2, p1, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v1, v1

    .line 481243
    if-eqz v1, :cond_1

    .line 481244
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 481245
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
