.class public Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ":",
        "LX/3Vw;",
        ":",
        "LX/3Vx;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;TV;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2y3;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ye;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2yE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition",
            "<TE;TV;>.CallToActionAttachment",
            "LinkedViewAdapter;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition",
            "<TE;TV;>;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition",
            "<TE;TV;>;>;"
        }
    .end annotation
.end field

.field private final k:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2y3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ye;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/links/AttachmentCallToActionButtonLinkPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentBasePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481293
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 481294
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->b:LX/0Ot;

    .line 481295
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->c:LX/0Ot;

    .line 481296
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->d:LX/0Ot;

    .line 481297
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->e:LX/0Ot;

    .line 481298
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->f:LX/0Ot;

    .line 481299
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->a:LX/0Ot;

    .line 481300
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->h:LX/0Ot;

    .line 481301
    iput-object p10, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->i:LX/0Ot;

    .line 481302
    new-instance v0, LX/2yE;

    invoke-direct {v0, p0}, LX/2yE;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->g:LX/2yE;

    .line 481303
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->j:LX/0Ot;

    .line 481304
    const v0, 0x7f0b00c4

    invoke-virtual {p7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->k:I

    .line 481305
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;
    .locals 14

    .prologue
    .line 481323
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;

    monitor-enter v1

    .line 481324
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481325
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481326
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481327
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481328
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;

    const/16 v4, 0x781

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x819

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x800

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x973

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x817

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x7fc

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    const/16 v11, 0x96f

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x872

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x7fa

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 481329
    move-object v0, v3

    .line 481330
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481331
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481332
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481333
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 481334
    sget-object v0, LX/3W6;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 481310
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 481311
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481312
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481313
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 481314
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2y3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 481315
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481316
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/C2R;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qb;

    invoke-direct {v2, p2, v1}, LX/C2R;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1qb;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481317
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/C8Z;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2yF;

    invoke-direct {v2, p2, v1}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481318
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/C8Z;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->g:LX/2yE;

    invoke-direct {v1, p2, v2}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481319
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481320
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481321
    return-object v4

    .line 481322
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v2, LX/C0G;

    iget v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/RatingBarShareAttachmentPartDefinition;->k:I

    invoke-direct {v2, v0, v3}, LX/C0G;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 481306
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 481307
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481308
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 481309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
