.class public Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35r;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0yc;


# direct methods
.method public constructor <init>(LX/0yc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491775
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 491776
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->a:LX/0yc;

    .line 491777
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;
    .locals 4

    .prologue
    .line 491778
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    monitor-enter v1

    .line 491779
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491780
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491781
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491782
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491783
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v3

    check-cast v3, LX/0yc;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;-><init>(LX/0yc;)V

    .line 491784
    move-object v0, p0

    .line 491785
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491786
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491787
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491788
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x20d1fb4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 491789
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/16 p2, 0x8

    .line 491790
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v1

    if-nez v1, :cond_1

    .line 491791
    :cond_0
    check-cast p4, LX/35r;

    invoke-interface {p4, p2}, LX/35r;->setCoverPhotoPlayIconVisibility(I)V

    .line 491792
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x5e0cd329

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 491793
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->a:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 491794
    check-cast p4, LX/35r;

    const/4 v1, 0x0

    invoke-interface {p4, v1}, LX/35r;->setCoverPhotoPlayIconVisibility(I)V

    goto :goto_0

    .line 491795
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->a:LX/0yc;

    new-instance v2, LX/C0B;

    invoke-direct {v2, p0, p4}, LX/C0B;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;Landroid/view/View;)V

    invoke-virtual {v1, v2}, LX/0yc;->a(LX/0yL;)V

    .line 491796
    check-cast p4, LX/35r;

    invoke-interface {p4, p2}, LX/35r;->setCoverPhotoPlayIconVisibility(I)V

    goto :goto_0
.end method
