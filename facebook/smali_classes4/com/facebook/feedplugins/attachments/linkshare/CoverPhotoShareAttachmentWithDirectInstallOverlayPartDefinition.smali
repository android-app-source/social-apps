.class public Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/2y3;

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/2sO;

.field private final e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;

.field private final g:LX/2y8;

.field private final h:LX/1sd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 480529
    const-class v0, LX/2y3;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;LX/1qb;LX/2y7;LX/0Uh;LX/2y8;LX/1sd;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480530
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 480531
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->a:LX/2y3;

    .line 480532
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 480533
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->d:LX/2sO;

    .line 480534
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 480535
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->i:Lcom/facebook/common/callercontext/CallerContext;

    const v1, 0x3ff745d1

    invoke-static {p5}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/1qb;)LX/2yA;

    move-result-object v2

    invoke-virtual {p6, v0, v1, v2}, LX/2y7;->a(Lcom/facebook/common/callercontext/CallerContext;FLX/2yA;)Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    .line 480536
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->f:LX/0Uh;

    .line 480537
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->g:LX/2y8;

    .line 480538
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->h:LX/1sd;

    .line 480539
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;
    .locals 13

    .prologue
    .line 480540
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;

    monitor-enter v1

    .line 480541
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480542
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480543
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480544
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480545
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v4

    check-cast v4, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v6

    check-cast v6, LX/2sO;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v8

    check-cast v8, LX/1qb;

    const-class v9, LX/2y7;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2y7;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/2y8;->b(LX/0QB;)LX/2y8;

    move-result-object v11

    check-cast v11, LX/2y8;

    invoke-static {v0}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v12

    check-cast v12, LX/1sd;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;-><init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;LX/1qb;LX/2y7;LX/0Uh;LX/2y8;LX/1sd;)V

    .line 480546
    move-object v0, v3

    .line 480547
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480548
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480549
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 480551
    sget-object v0, LX/3Wn;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480552
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480553
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480554
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->d:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480555
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480556
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480557
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 480558
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 480559
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480560
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480561
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->f:LX/0Uh;

    const/16 v3, 0x328

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->a:LX/2y3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 480562
    :goto_0
    return v0

    .line 480563
    :cond_1
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v0

    .line 480564
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/47G;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, LX/47G;->d:LX/0Px;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->g:LX/2y8;

    invoke-virtual {v0}, LX/2y8;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentWithDirectInstallOverlayPartDefinition;->h:LX/1sd;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/1sd;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
