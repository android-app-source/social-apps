.class public Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2y3;

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final d:LX/2sO;

.field private final e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491922
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 491923
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->a:LX/2y3;

    .line 491924
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 491925
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 491926
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->d:LX/2sO;

    .line 491927
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 491928
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 491899
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;

    monitor-enter v1

    .line 491900
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491901
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491904
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v4

    check-cast v4, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v7

    check-cast v7, LX/2sO;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;-><init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 491905
    move-object v0, v3

    .line 491906
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491907
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491908
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2y3;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 491919
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 491920
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 491921
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 491918
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 491912
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491913
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 491914
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->d:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491915
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 491916
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 491917
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 491910
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 491911
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->a:LX/2y3;

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;)Z

    move-result v0

    return v0
.end method
