.class public Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bzz",
            "<",
            "LX/1Pm;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Bzz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480384
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 480385
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->d:LX/0Ot;

    .line 480386
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->e:LX/0Ot;

    .line 480387
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 480388
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 480389
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bzz;

    const/4 v2, 0x0

    .line 480390
    new-instance v3, LX/Bzy;

    invoke-direct {v3, v0}, LX/Bzy;-><init>(LX/Bzz;)V

    .line 480391
    iget-object v4, v0, LX/Bzz;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bzx;

    .line 480392
    if-nez v4, :cond_0

    .line 480393
    new-instance v4, LX/Bzx;

    invoke-direct {v4, v0}, LX/Bzx;-><init>(LX/Bzz;)V

    .line 480394
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Bzx;->a$redex0(LX/Bzx;LX/1De;IILX/Bzy;)V

    .line 480395
    move-object v3, v4

    .line 480396
    move-object v2, v3

    .line 480397
    move-object v0, v2

    .line 480398
    iget-object v2, v0, LX/Bzx;->a:LX/Bzy;

    iput-object p2, v2, LX/Bzy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480399
    iget-object v2, v0, LX/Bzx;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 480400
    move-object v0, v0

    .line 480401
    iget-object v2, v0, LX/Bzx;->a:LX/Bzy;

    iput-object p3, v2, LX/Bzy;->b:LX/1Pm;

    .line 480402
    iget-object v2, v0, LX/Bzx;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 480403
    move-object v0, v0

    .line 480404
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    .line 480405
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1V0;

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;
    .locals 6

    .prologue
    .line 480406
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 480407
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480408
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480411
    new-instance v4, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0x6fd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1e53

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V

    .line 480412
    move-object v0, v4

    .line 480413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 480417
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 480418
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/MisinformationShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 480419
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480420
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480421
    if-eqz v0, :cond_0

    .line 480422
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480423
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const p0, -0x67292209

    invoke-static {v0, p0}, LX/1VX;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 480424
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 480425
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480426
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
