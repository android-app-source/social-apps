.class public Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yY;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C0G;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition",
            "<TE;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481888
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 481889
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->a:LX/0Ot;

    .line 481890
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;
    .locals 4

    .prologue
    .line 481891
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    monitor-enter v1

    .line 481892
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481893
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481894
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481895
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481896
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    const/16 p0, 0x1e61

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;-><init>(LX/0Ot;)V

    .line 481897
    move-object v0, v3

    .line 481898
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481899
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481900
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481901
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 481902
    check-cast p2, LX/C0G;

    .line 481903
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/C0H;

    iget-object v2, p2, LX/C0G;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget v3, p2, LX/C0G;->b:I

    invoke-direct {v1, v2, v3}, LX/C0H;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 481904
    const/4 v0, 0x0

    return-object v0
.end method
