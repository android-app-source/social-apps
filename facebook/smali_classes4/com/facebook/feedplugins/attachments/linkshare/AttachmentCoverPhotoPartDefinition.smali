.class public Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/1qb;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 480733
    const-class v0, LX/2y3;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/2yH;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480734
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 480735
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->c:LX/1qb;

    .line 480736
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v1, 0x3ff745d1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->c:LX/1qb;

    invoke-static {v2}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/1qb;)LX/2yA;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, LX/2yH;->a(Lcom/facebook/common/callercontext/CallerContext;FLX/2yA;)Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    .line 480737
    return-void
.end method

.method public static a(LX/1qb;)LX/2yA;
    .locals 1

    .prologue
    .line 480738
    new-instance v0, LX/2y9;

    invoke-direct {v0, p0}, LX/2y9;-><init>(LX/1qb;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .locals 5

    .prologue
    .line 480739
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    monitor-enter v1

    .line 480740
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480741
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480742
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480743
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480744
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    const-class v4, LX/2yH;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2yH;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;-><init>(LX/1qb;LX/2yH;)V

    .line 480745
    move-object v0, p0

    .line 480746
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480747
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480748
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480749
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480750
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480751
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480752
    const/4 v0, 0x0

    return-object v0
.end method
