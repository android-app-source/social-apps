.class public Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/C02;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;",
            "LX/C02;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition",
            "<TE;",
            "LX/C02;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2y3;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 480515
    new-instance v0, LX/2y2;

    invoke-direct {v0}, LX/2y2;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2y3;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480451
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 480452
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->b:LX/0Ot;

    .line 480453
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->e:LX/0Ot;

    .line 480454
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->c:LX/0Ot;

    .line 480455
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->f:LX/0Ot;

    .line 480456
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->d:LX/0Ot;

    .line 480457
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    .line 480458
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 480516
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 480517
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480518
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480521
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;

    const/16 v4, 0x7fc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x7fe

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x781

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1e69

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x800

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 480522
    move-object v0, v3

    .line 480523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 480513
    invoke-static {p0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;)F
    .locals 3

    .prologue
    .line 480514
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget v1, LX/2yO;->e:F

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 480512
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 480504
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480505
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480506
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480507
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2y3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480508
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480509
    :goto_0
    const v1, 0x7f0d1cfe

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480510
    const/4 v0, 0x0

    return-object v0

    .line 480511
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x20387f5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 480459
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/C02;

    const/4 v4, 0x0

    .line 480460
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 480461
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480462
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 480463
    :goto_0
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qb;

    invoke-virtual {v2, v1}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 480464
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 480465
    :cond_0
    iget-object v2, p4, LX/C02;->d:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 480466
    :goto_2
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 480467
    iget-object p2, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, p2

    .line 480468
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480469
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    .line 480470
    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {v1}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    .line 480471
    :goto_3
    iget-object v2, p4, LX/C02;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480472
    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->e(Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 480473
    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->e(Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;)F

    move-result v1

    .line 480474
    const/high16 v2, -0x3ea00000    # -14.0f

    sub-float/2addr v2, v1

    float-to-int v2, v2

    .line 480475
    invoke-virtual {p4}, LX/C02;->getContext()Landroid/content/Context;

    move-result-object v4

    int-to-float v2, v2

    invoke-static {v4, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 480476
    iget-object v4, p4, LX/C02;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p4}, LX/C02;->getContext()Landroid/content/Context;

    move-result-object p2

    const/high16 p1, 0x41400000    # 12.0f

    invoke-static {p2, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p2

    invoke-static {v4, v2, p2}, LX/C02;->a(Landroid/view/View;II)V

    .line 480477
    const/4 v2, 0x0

    add-float/2addr v2, v1

    float-to-int v2, v2

    .line 480478
    invoke-virtual {p4}, LX/C02;->getContext()Landroid/content/Context;

    move-result-object v4

    int-to-float v2, v2

    invoke-static {v4, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 480479
    iget-object v4, p4, LX/C02;->f:Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-static {v4, p2, v2}, LX/C02;->a(Landroid/view/View;II)V

    .line 480480
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v2, LX/2yO;->i:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 480481
    iget-object v4, p4, LX/C02;->f:Landroid/widget/ImageView;

    if-eqz v1, :cond_7

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480482
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget v2, LX/2yO;->d:I

    const/16 v4, 0xc

    invoke-interface {v1, v2, v4}, LX/0ad;->a(II)I

    move-result v1

    move v1, v1

    .line 480483
    iget-object v2, p4, LX/C02;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 480484
    if-nez v2, :cond_8

    .line 480485
    :goto_5
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v2, LX/2yO;->j:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 480486
    iget-object v4, p4, LX/C02;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_9

    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480487
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v2, LX/2yO;->k:S

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 480488
    iget-object v4, p4, LX/C02;->c:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_a

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 480489
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v2, LX/2yO;->l:S

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 480490
    if-eqz v1, :cond_2

    .line 480491
    iget-object v2, p4, LX/C02;->d:Landroid/widget/TextView;

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p2, LX/0xr;->REGULAR:LX/0xr;

    iget-object p0, p4, LX/C02;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p0

    invoke-static {v2, v4, p2, p0}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 480492
    :cond_2
    const/16 v1, 0x1f

    const v2, -0x4dbffc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    move v2, v4

    .line 480493
    goto/16 :goto_0

    .line 480494
    :cond_4
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qb;

    invoke-virtual {v2, v1}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v2

    goto/16 :goto_1

    .line 480495
    :cond_5
    iget-object p2, p4, LX/C02;->d:Landroid/widget/TextView;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 480496
    iget-object p2, p4, LX/C02;->d:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480497
    goto/16 :goto_2

    :cond_6
    move-object v1, v2

    .line 480498
    goto/16 :goto_3

    .line 480499
    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_4

    .line 480500
    :cond_8
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v1, v4, v1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 480501
    iget-object v4, p4, LX/C02;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    .line 480502
    :cond_9
    const/16 v2, 0x8

    goto :goto_6

    .line 480503
    :cond_a
    const/16 v2, 0x8

    goto :goto_7
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 480427
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 480428
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480429
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 480430
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 480431
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v5, 0x68aef5ca

    if-ne v1, v5, :cond_1

    move v1, v2

    .line 480432
    :goto_0
    if-nez v1, :cond_2

    .line 480433
    :cond_0
    :goto_1
    return v3

    :cond_1
    move v1, v3

    .line 480434
    goto :goto_0

    .line 480435
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 480436
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 480437
    if-eqz v1, :cond_4

    .line 480438
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v5, LX/2yO;->g:S

    const/4 p1, 0x0

    invoke-interface {v1, v5, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 480439
    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2y3;

    invoke-virtual {v1, v4}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 480440
    :goto_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_4
    move v4, v4

    .line 480441
    if-eqz v4, :cond_5

    .line 480442
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v5, LX/2yO;->f:S

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0ad;->a(SZ)Z

    move-result v4

    move v4, v4

    .line 480443
    if-eqz v4, :cond_5

    move v4, v2

    .line 480444
    :goto_5
    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 480445
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/NativeStoryPhotoAttachmentPartDefinition;->g:LX/0ad;

    sget-short v5, LX/2yO;->h:S

    const/4 p1, 0x0

    invoke-interface {v0, v5, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 480446
    if-eqz v0, :cond_6

    move v0, v2

    .line 480447
    :goto_6
    if-nez v4, :cond_3

    if-nez v0, :cond_3

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v1, v3

    .line 480448
    goto :goto_3

    :cond_5
    move v4, v3

    .line 480449
    goto :goto_5

    :cond_6
    move v0, v3

    .line 480450
    goto :goto_6

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_4
.end method
