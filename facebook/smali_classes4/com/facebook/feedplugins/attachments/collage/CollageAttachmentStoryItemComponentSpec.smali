.class public Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/26D;

.field private final c:LX/0qn;

.field private final d:LX/367;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 486149
    const-class v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    const-string v1, "newsfeed_story_attachment_photo_grid_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/26D;LX/0qn;LX/367;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486109
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    .line 486110
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->c:LX/0qn;

    .line 486111
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->d:LX/367;

    .line 486112
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;LX/1De;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)LX/1dQ;
    .locals 2
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Pm;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsDetailsFields;",
            ")",
            "LX/1dQ;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 486138
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->c:LX/0qn;

    invoke-static {v0, p3}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486139
    const/4 v0, 0x0

    .line 486140
    :goto_0
    return-object v0

    .line 486141
    :cond_0
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->REACTION:LX/1Qt;

    if-ne v0, v1, :cond_1

    .line 486142
    const v0, -0x19b97078

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 486143
    goto :goto_0

    .line 486144
    :cond_1
    if-eqz p4, :cond_2

    .line 486145
    const v0, -0x2470a0af

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 486146
    goto :goto_0

    .line 486147
    :cond_2
    const v0, 0x1f7564dd

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 486148
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;
    .locals 6

    .prologue
    .line 486127
    const-class v1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    monitor-enter v1

    .line 486128
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 486129
    sput-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 486130
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486131
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 486132
    new-instance p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;

    invoke-static {v0}, LX/26D;->a(LX/0QB;)LX/26D;

    move-result-object v3

    check-cast v3, LX/26D;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v4

    check-cast v4, LX/0qn;

    invoke-static {v0}, LX/367;->a(LX/0QB;)LX/367;

    move-result-object v5

    check-cast v5, LX/367;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;-><init>(LX/26D;LX/0qn;LX/367;)V

    .line 486133
    move-object v0, p0

    .line 486134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 486135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 486137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/26M;LX/1bf;LX/1aZ;Landroid/graphics/PointF;IIILX/1Pm;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Z)LX/1Dg;
    .locals 4
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/26M;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1bf;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1aZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Landroid/graphics/PointF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/26M;",
            "LX/1bf;",
            "LX/1aZ;",
            "Landroid/graphics/PointF;",
            "III",
            "LX/1Pm;",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsDetailsFields;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 486120
    invoke-virtual {p3}, LX/26M;->d()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 486121
    sget-object v1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p10, p4, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 486122
    if-eqz p4, :cond_0

    .line 486123
    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p10, p5, v1, p4, v2}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 486124
    :cond_0
    if-eqz p12, :cond_1

    const/4 v1, 0x0

    .line 486125
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->d:LX/367;

    invoke-virtual {v2, p1}, LX/367;->c(LX/1De;)LX/369;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/369;->a(LX/26N;)LX/369;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/369;->a(LX/1aZ;)LX/369;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/369;->a(Landroid/graphics/PointF;)LX/369;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/369;->h(I)LX/369;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/369;->i(I)LX/369;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/369;->j(I)LX/369;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    invoke-virtual {v3, v0}, LX/26D;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/369;->b(Ljava/lang/String;)LX/369;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/369;->a(LX/1dQ;)LX/369;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 486126
    :cond_1
    invoke-static {p0, p1, p10, p2, p11}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;LX/1De;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)LX/1dQ;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/26M;LX/1aZ;)V
    .locals 9
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1bf;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/26M;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1aZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1bf;",
            "LX/26M;",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 486118
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, p6, p4}, LX/26D;->a(Landroid/view/View;LX/1aZ;LX/1bf;)LX/9hN;

    move-result-object v2

    sget-object v7, LX/74S;->REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

    const/4 v8, 0x0

    move-object v3, p3

    move v4, p2

    move-object v5, p5

    move-object v6, p4

    invoke-virtual/range {v0 .. v8}, LX/26D;->a(Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1bf;LX/74S;Z)V

    .line 486119
    return-void
.end method

.method public final a(Landroid/view/View;ILcom/facebook/feed/rows/core/props/FeedProps;LX/26M;LX/1bf;IILX/1Pm;LX/1aZ;)V
    .locals 8
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/26M;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1bf;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/1aZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/26M;",
            "LX/1bf;",
            "II",
            "LX/1Pm;",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 486113
    invoke-static {p2, p4, p6, p7}, LX/26D;->a(ILX/26M;II)Z

    move-result v1

    .line 486114
    if-eqz v1, :cond_0

    .line 486115
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-static {p1, v0, p5}, LX/26D;->a(Landroid/view/View;LX/1aZ;LX/1bf;)LX/9hN;

    move-result-object v6

    move-object v3, p3

    move v4, p2

    move-object v5, p5

    invoke-virtual/range {v1 .. v6}, LX/26D;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1bf;LX/9hN;)V

    .line 486116
    :goto_0
    return-void

    .line 486117
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->b:LX/26D;

    move-object v2, p1

    move-object v3, p3

    move v4, p2

    move-object v5, p4

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, LX/26D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1Po;LX/1aZ;)V

    goto :goto_0
.end method
