.class public Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/Bz3;",
        "TE;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/26D;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VL;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0ad;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 364351
    const-class v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    const-string v1, "newsfeed_story_attachment_photo_grid_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 364352
    const-class v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    const-string v1, "newsfeed_story_attachment_photo_feed_prefetch"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 364353
    new-instance v0, LX/23Q;

    invoke-direct {v0}, LX/23Q;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/26D;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1VL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364342
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 364343
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    .line 364344
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->e:LX/0Ot;

    .line 364345
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->f:LX/0Ot;

    .line 364346
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->g:LX/0Ot;

    .line 364347
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->h:LX/0Ot;

    .line 364348
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->i:LX/0ad;

    .line 364349
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->j:LX/0Ot;

    .line 364350
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;[LX/1bf;ILX/1Po;)LX/Aj7;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;[",
            "LX/1bf;",
            "ITE;)",
            "LX/Aj7",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364341
    new-instance v0, LX/Bz1;

    move-object v1, p0

    move-object v2, p2

    move v3, p4

    move-object v4, p3

    move-object v5, p1

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/Bz1;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;LX/0Px;I[LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;ZLX/0Px;[LX/1bf;ILX/1Po;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)LX/Aj7;
    .locals 6
    .param p7    # LX/1Po;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1PT;",
            "Z",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;[",
            "LX/1bf;",
            "ITE;",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsDetailsFields;",
            ")",
            "LX/Aj7",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364332
    if-eqz p3, :cond_0

    .line 364333
    const/4 v0, 0x0

    .line 364334
    :goto_0
    return-object v0

    .line 364335
    :cond_0
    invoke-interface {p2}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->REACTION:LX/1Qt;

    if-ne v0, v1, :cond_1

    .line 364336
    sget-object v0, LX/74S;->REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

    const/4 v1, 0x0

    invoke-static {p0, p1, p5, v0, v1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;[LX/1bf;LX/74S;Z)LX/Aj7;

    move-result-object v0

    goto :goto_0

    .line 364337
    :cond_1
    if-eqz p8, :cond_2

    .line 364338
    new-instance v0, LX/Bz2;

    invoke-direct {v0, p0}, LX/Bz2;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;)V

    move-object v0, v0

    .line 364339
    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move-object v5, p7

    .line 364340
    invoke-static/range {v0 .. v5}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;[LX/1bf;ILX/1Po;)LX/Aj7;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;[LX/1bf;LX/74S;Z)LX/Aj7;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;[",
            "LX/1bf;",
            "LX/74S;",
            "Z)",
            "LX/Aj7",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364331
    new-instance v0, LX/Bz0;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/Bz0;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;[LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/74S;Z)V

    return-object v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/Bz3;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/Bz3;"
        }
    .end annotation

    .prologue
    .line 364302
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 364303
    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    .line 364304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/26D;

    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, LX/26D;->a(LX/1PT;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26O;

    move-result-object v11

    .line 364305
    invoke-virtual {v11}, LX/26O;->a()LX/0Px;

    move-result-object v6

    .line 364306
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    .line 364307
    new-array v0, v10, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 364308
    new-array v12, v10, [LX/1aZ;

    .line 364309
    new-array v7, v10, [LX/1bf;

    .line 364310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    sget-object v5, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v13

    .line 364311
    const/4 v3, 0x0

    move v9, v3

    :goto_0
    if-ge v9, v10, :cond_1

    .line 364312
    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/26M;

    .line 364313
    invoke-virtual {v3}, LX/26M;->d()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 364314
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/26D;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v8, v0, v1, v3}, LX/26D;->a(LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/26M;)LX/1bf;

    move-result-object v8

    move-object/from16 v3, p2

    .line 364315
    check-cast v3, LX/1Pt;

    sget-object v14, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v3, v8, v14}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 364316
    aput-object v8, v7, v9

    .line 364317
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/26D;

    move-object/from16 v0, p1

    invoke-virtual {v3, v13, v8, v0, v5}, LX/26D;->a(LX/1Ad;LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1aZ;

    move-result-object v14

    .line 364318
    if-eqz v8, :cond_0

    move-object/from16 v3, p2

    .line 364319
    check-cast v3, LX/1Pp;

    invoke-static/range {p1 .. p1}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v15

    invoke-virtual {v15}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, v16

    invoke-interface {v3, v14, v15, v8, v0}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 364320
    :cond_0
    aput-object v14, v12, v9

    .line 364321
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/26D;

    invoke-virtual {v3, v5}, LX/26D;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v9

    .line 364322
    add-int/lit8 v3, v9, 0x1

    move v9, v3

    goto :goto_0

    .line 364323
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/26D;

    sget-object v8, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v5, p2

    check-cast v5, LX/1Pt;

    invoke-virtual {v3, v8, v2, v5, v6}, LX/26D;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pt;LX/0Px;)V

    .line 364324
    invoke-static {v2}, LX/26D;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    move-result-object v10

    .line 364325
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    invoke-static {v2, v3, v10}, LX/26D;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)I

    move-result v8

    .line 364326
    const/4 v13, 0x0

    .line 364327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yc;

    invoke-virtual {v2}, LX/0yc;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 364328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->p()LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v13

    .line 364329
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    invoke-static/range {v2 .. v10}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;ZLX/0Px;[LX/1bf;ILX/1Po;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)LX/Aj7;

    move-result-object v15

    .line 364330
    new-instance v9, LX/Bz3;

    move-object v10, v11

    move-object v11, v6

    move-object v14, v7

    move/from16 v16, v8

    invoke-direct/range {v9 .. v17}, LX/Bz3;-><init>(LX/26O;LX/0Px;[LX/1aZ;LX/1aZ;[LX/1bf;LX/Aj7;I[Ljava/lang/String;)V

    return-object v9
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 364291
    const-class v1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    monitor-enter v1

    .line 364292
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364293
    sput-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364294
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364295
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 364296
    new-instance v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    const/16 v4, 0x7f4

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x509

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x756

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x5e3

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x4e0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x110

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0Ot;)V

    .line 364297
    move-object v0, v3

    .line 364298
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364299
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364300
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bz3;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/Bz3;",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 364354
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26D;

    iget-object v1, p2, LX/Bz3;->c:LX/0Px;

    invoke-virtual {v0, p1, v1}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;)LX/26E;

    move-result-object v0

    .line 364355
    iget-boolean v1, v0, LX/26E;->a:Z

    if-nez v1, :cond_0

    .line 364356
    const/4 v0, 0x0

    invoke-virtual {p3, v2, v2, v0, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(IILX/Aj7;I)V

    .line 364357
    :goto_0
    return-void

    .line 364358
    :cond_0
    iget-object v1, p2, LX/Bz3;->e:[LX/1bf;

    sget-object v2, LX/74S;->NEWSFEED:LX/74S;

    invoke-static {p0, p1, v1, v2, v3}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;[LX/1bf;LX/74S;Z)LX/Aj7;

    move-result-object v1

    .line 364359
    iget v2, v0, LX/26E;->c:I

    iget v0, v0, LX/26E;->b:I

    invoke-virtual {p3, v3, v2, v1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(IILX/Aj7;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 364290
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 364287
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 364288
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 364289
    invoke-direct {p0, p2, p3}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/Bz3;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6884bb7c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364248
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/Bz3;

    check-cast p3, LX/1Po;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 364249
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 364250
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 364251
    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 364252
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yc;

    invoke-virtual {v2}, LX/0yc;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p2, LX/Bz3;->a:LX/1aZ;

    instance-of v2, v2, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v2, :cond_2

    iget-object v2, p2, LX/Bz3;->a:LX/1aZ;

    check-cast v2, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    .line 364253
    iget-boolean v4, v2, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    move v2, v4

    .line 364254
    if-eqz v2, :cond_2

    .line 364255
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yc;

    invoke-virtual {v2}, LX/0yc;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 364256
    iget-object v4, p2, LX/Bz3;->d:[LX/1aZ;

    .line 364257
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v1, v4

    if-ge v2, v1, :cond_0

    .line 364258
    aget-object v1, v4, v2

    check-cast v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-static {v1, v2}, LX/26D;->a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;I)V

    .line 364259
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 364260
    :cond_0
    iget-object v1, p2, LX/Bz3;->b:LX/26O;

    invoke-virtual {p4, v1, v4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 364261
    iget-object v1, p2, LX/Bz3;->f:LX/Aj7;

    .line 364262
    iput-object v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 364263
    iget v1, p2, LX/Bz3;->g:I

    .line 364264
    iput v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 364265
    invoke-static {p0, p1, p2, p4}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bz3;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    .line 364266
    :goto_1
    iget-object v1, p2, LX/Bz3;->h:[Ljava/lang/String;

    .line 364267
    iput-object v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->r:[Ljava/lang/String;

    .line 364268
    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b()V

    .line 364269
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    .line 364270
    iput-boolean v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 364271
    const/16 v1, 0x1f

    const v2, -0x401c980a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 364272
    :cond_1
    iget-object v2, p2, LX/Bz3;->a:LX/1aZ;

    check-cast v2, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    iget-object v4, p2, LX/Bz3;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    iget v5, p2, LX/Bz3;->g:I

    invoke-static {v2, v1, v4, v5}, LX/26D;->a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;II)V

    .line 364273
    iget-object v1, p2, LX/Bz3;->b:LX/26O;

    iget-object v2, p2, LX/Bz3;->a:LX/1aZ;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;LX/1aZ;)V

    goto :goto_1

    .line 364274
    :cond_2
    iget-object v1, p2, LX/Bz3;->b:LX/26O;

    iget-object v2, p2, LX/Bz3;->d:[LX/1aZ;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 364275
    iget-object v1, p2, LX/Bz3;->f:LX/Aj7;

    .line 364276
    iput-object v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 364277
    iget v1, p2, LX/Bz3;->g:I

    .line 364278
    iput v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 364279
    invoke-static {p0, p1, p2, p4}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bz3;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    .line 364280
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/26D;

    iget-object v2, p2, LX/Bz3;->c:LX/0Px;

    .line 364281
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_4

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/26M;

    .line 364282
    invoke-virtual {v4}, LX/26M;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 364283
    invoke-static {v1, p3, v7}, LX/26D;->b(LX/26D;LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 364284
    iget-object v8, v1, LX/26D;->l:LX/1WN;

    const-string p1, "warning_screen_shown"

    invoke-static {v7}, LX/1WM;->c(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v7

    invoke-virtual {v4}, LX/26M;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, p1, v7, v4}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 364285
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 364286
    :cond_4
    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 364239
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->i:LX/0ad;

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1VL;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qn;

    .line 364240
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 364241
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 364242
    invoke-static {v1, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 364243
    invoke-virtual {v0, v3}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    .line 364244
    :goto_0
    move v0, v3

    .line 364245
    return v0

    :cond_0
    invoke-virtual {v0, v3}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 364246
    sget-short v3, LX/1EB;->al:S

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 364247
    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 364238
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 364231
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 364232
    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 364233
    iput-object v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 364234
    iput v0, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 364235
    invoke-virtual {p4, v0, v0, v1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(IILX/Aj7;I)V

    .line 364236
    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c()V

    .line 364237
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 364230
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
