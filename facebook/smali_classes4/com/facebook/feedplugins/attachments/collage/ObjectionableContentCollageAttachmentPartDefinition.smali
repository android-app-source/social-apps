.class public Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/BJ3;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366174
    new-instance v0, LX/23w;

    invoke-direct {v0}, LX/23w;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1WM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366210
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 366211
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->b:LX/0Ot;

    .line 366212
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->c:LX/0Ot;

    .line 366213
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->d:LX/0Ot;

    .line 366214
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 366198
    const-class v1, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;

    monitor-enter v1

    .line 366199
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366200
    sput-object v2, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366203
    new-instance v3, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;

    const/16 v4, 0x2f14

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x7f5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xf44

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 366204
    move-object v0, v3

    .line 366205
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366206
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366207
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BJ3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366209
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 366185
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 366186
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 366187
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    .line 366188
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 366190
    if-eqz v0, :cond_1

    .line 366191
    :goto_0
    move-object v3, v0

    .line 366192
    const v1, 0x7f0d1cc1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 366193
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    if-nez v3, :cond_0

    move-object v1, v2

    :goto_1
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v3}, LX/1WM;->c(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v3

    .line 366194
    new-instance v5, LX/Ccm;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p2

    move-object v7, v1

    move v8, v3

    invoke-direct/range {v5 .. v10}, LX/Ccm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZLX/24k;Z)V

    move-object v1, v5

    .line 366195
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366196
    return-object v2

    .line 366197
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 366175
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 366176
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 366177
    :goto_0
    return v0

    .line 366178
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 366179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 366180
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 366181
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/ObjectionableContentCollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WM;

    invoke-virtual {v0, v5}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366182
    const/4 v0, 0x1

    goto :goto_0

    .line 366183
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 366184
    goto :goto_0
.end method
