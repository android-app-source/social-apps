.class public Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Landroid/graphics/Rect;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field public final e:LX/26D;

.field private final f:LX/360;

.field private final g:LX/0yc;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/361;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/361",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3u;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8wz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 366408
    const-class v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;

    const-string v1, "newsfeed_story_attachment_photo_feed_prefetch"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 366409
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->c:LX/0Zi;

    .line 366410
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a:Landroid/graphics/Rect;

    .line 366411
    new-instance v0, LX/23z;

    invoke-direct {v0}, LX/23z;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->d:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/26D;LX/360;LX/0yc;LX/0Or;LX/361;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26D;",
            "LX/360;",
            "LX/0yc;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/361;",
            "LX/0Ot",
            "<",
            "LX/C3u;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8wz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366400
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    .line 366401
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->f:LX/360;

    .line 366402
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    .line 366403
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->h:LX/0Or;

    .line 366404
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->i:LX/361;

    .line 366405
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->j:LX/0Ot;

    .line 366406
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->k:LX/0Ot;

    .line 366407
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;LX/1De;ILandroid/graphics/Rect;LX/1Pe;LX/26O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;IIIILX/26E;LX/1np;LX/1Ad;Z)LX/1Dg;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Landroid/graphics/Rect;",
            "TE;",
            "LX/26O;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;IIII",
            "LX/26E;",
            "LX/1np",
            "<",
            "LX/Byw;",
            ">;",
            "LX/1Ad;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 366369
    iget v2, p3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p5

    move/from16 v1, p11

    invoke-virtual {v0, v1, v2}, LX/26O;->a(II)I

    move-result v2

    .line 366370
    iget v3, p3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p5

    move/from16 v1, p11

    invoke-virtual {v0, v1, v3}, LX/26O;->b(II)I

    move-result v3

    .line 366371
    invoke-static {}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a()Landroid/graphics/Rect;

    move-result-object v4

    .line 366372
    move-object/from16 v0, p5

    move/from16 v1, p11

    invoke-virtual {v0, p2, v1, v4}, LX/26O;->a(IILandroid/graphics/Rect;)V

    .line 366373
    iget v5, v4, Landroid/graphics/Rect;->left:I

    add-int v9, v5, v2

    .line 366374
    iget v2, v4, Landroid/graphics/Rect;->top:I

    iget v5, p3, Landroid/graphics/Rect;->top:I

    add-int v10, v2, v5

    .line 366375
    iget v2, v4, Landroid/graphics/Rect;->right:I

    sub-int v5, v2, v3

    .line 366376
    iget v2, v4, Landroid/graphics/Rect;->bottom:I

    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    sub-int v6, v2, v3

    .line 366377
    invoke-static {v4}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(Landroid/graphics/Rect;)V

    .line 366378
    move-object/from16 v0, p7

    move/from16 v1, p11

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/26M;

    .line 366379
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object v3, p4

    check-cast v3, LX/1Po;

    move-object/from16 v0, p6

    invoke-virtual {v4, v3, v0, v2}, LX/26D;->a(LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/26M;)LX/1bf;

    move-result-object v7

    .line 366380
    invoke-virtual {v2}, LX/26M;->d()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 366381
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object/from16 v0, p14

    move-object/from16 v1, p6

    invoke-virtual {v4, v0, v7, v1, v3}, LX/26D;->a(LX/1Ad;LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1aZ;

    move-result-object v8

    .line 366382
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    instance-of v3, v8, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v3, :cond_0

    move-object v3, v8

    check-cast v3, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->r()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v8

    .line 366383
    check-cast v3, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    .line 366384
    move/from16 v0, p11

    invoke-static {v3, v0}, LX/26D;->a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;I)V

    .line 366385
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e(Z)V

    .line 366386
    :cond_0
    move/from16 v0, p10

    move/from16 v1, p11

    if-ne v0, v1, :cond_2

    .line 366387
    sub-int v3, v5, v9

    .line 366388
    sub-int v4, v6, v10

    .line 366389
    if-eqz p15, :cond_1

    const/4 v2, 0x0

    .line 366390
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7, v10}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7, v9}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->i:LX/361;

    invoke-virtual {v6, p1}, LX/361;->c(LX/1De;)LX/CDK;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object/from16 v0, p6

    move/from16 v1, p10

    invoke-virtual {v7, v0, v1}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/3EE;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/CDK;->a(LX/3EE;)LX/CDK;

    move-result-object v6

    invoke-virtual {v6, p4}, LX/CDK;->a(LX/1Pe;)LX/CDK;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/CDK;->a(Landroid/view/View$OnClickListener;)LX/CDK;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v2, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v2, v6, v10}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    const/4 v6, 0x0

    invoke-interface {v2, v6, v9}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 366391
    :goto_1
    return-object v2

    :cond_1
    move-object v2, p0

    move-object/from16 v5, p6

    move/from16 v6, p11

    .line 366392
    invoke-static/range {v2 .. v7}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;IILcom/facebook/feed/rows/core/props/FeedProps;ILX/1bf;)Landroid/view/View$OnClickListener;

    move-result-object v2

    goto :goto_0

    .line 366393
    :cond_2
    move-object/from16 v0, p12

    iget-boolean v3, v0, LX/26E;->a:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p12

    iget v3, v0, LX/26E;->b:I

    move/from16 v0, p11

    if-ne v3, v0, :cond_4

    const/4 v3, 0x1

    .line 366394
    :goto_2
    if-eqz v3, :cond_3

    .line 366395
    new-instance v4, LX/Byw;

    move/from16 v0, p11

    invoke-direct {v4, v2, v0, v7, v8}, LX/Byw;-><init>(LX/26M;ILX/1bf;LX/1aZ;)V

    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    .line 366396
    :cond_3
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->f:LX/360;

    invoke-virtual {v4, p1}, LX/360;->c(LX/1De;)LX/366;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, LX/366;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/366;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/366;->a(LX/26M;)LX/366;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/366;->a(LX/1aZ;)LX/366;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/366;->a(LX/1bf;)LX/366;

    move-result-object v2

    move-object/from16 v0, p5

    move/from16 v1, p11

    invoke-virtual {v0, v1}, LX/26O;->b(I)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/366;->a(Landroid/graphics/PointF;)LX/366;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, LX/366;->i(I)LX/366;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, LX/366;->j(I)LX/366;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/366;->h(I)LX/366;

    move-result-object v2

    check-cast p4, LX/1Pm;

    invoke-virtual {v2, p4}, LX/366;->a(LX/1Pm;)LX/366;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/366;->a(Z)LX/366;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    if-eqz v3, :cond_5

    sget-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->d:Landroid/util/SparseArray;

    :goto_3
    invoke-interface {v4, v2}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3, v10}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3, v9}, LX/1Di;->k(II)LX/1Di;

    move-result-object v2

    sub-int v3, v5, v9

    invoke-interface {v2, v3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    sub-int v3, v6, v10

    invoke-interface {v2, v3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_1

    .line 366397
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 366398
    :cond_5
    const/4 v2, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;LX/1De;LX/26E;)LX/1Dg;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 366364
    iget-boolean v0, p2, LX/26E;->a:Z

    if-nez v0, :cond_0

    .line 366365
    const/4 v0, 0x0

    .line 366366
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8wz;

    invoke-virtual {v0, p1}, LX/8wz;->c(LX/1De;)LX/8wx;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8wx;->h(I)LX/8wx;

    move-result-object v0

    iget v1, p2, LX/26E;->c:I

    invoke-virtual {v0, v1}, LX/8wx;->i(I)LX/8wx;

    move-result-object v0

    .line 366367
    const v1, -0x4a6c9e2b

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 366368
    invoke-virtual {v0, v1}, LX/8wx;->a(LX/1dQ;)LX/8wx;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, v2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 366412
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->c:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 366413
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;IILcom/facebook/feed/rows/core/props/FeedProps;ILX/1bf;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/1bf;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 366363
    new-instance v0, LX/Byv;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p5

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, LX/Byv;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;IILX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;
    .locals 11

    .prologue
    .line 366352
    const-class v1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;

    monitor-enter v1

    .line 366353
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366354
    sput-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366355
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366356
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366357
    new-instance v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;

    invoke-static {v0}, LX/26D;->a(LX/0QB;)LX/26D;

    move-result-object v4

    check-cast v4, LX/26D;

    invoke-static {v0}, LX/360;->a(LX/0QB;)LX/360;

    move-result-object v5

    check-cast v5, LX/360;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v6

    check-cast v6, LX/0yc;

    const/16 v7, 0x509

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/361;->a(LX/0QB;)LX/361;

    move-result-object v8

    check-cast v8, LX/361;

    const/16 v9, 0x1ed3

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x179f

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;-><init>(LX/26D;LX/360;LX/0yc;LX/0Or;LX/361;LX/0Ot;LX/0Ot;)V

    .line 366358
    move-object v0, v3

    .line 366359
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366360
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366361
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 366349
    invoke-virtual {p0}, Landroid/graphics/Rect;->setEmpty()V

    .line 366350
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->c:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 366351
    return-void
.end method


# virtual methods
.method public final a(LX/1De;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;Landroid/graphics/Rect;ZLX/1np;)LX/1Dg;
    .locals 19
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Rect;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Landroid/graphics/Rect;",
            "Z",
            "LX/1np",
            "<",
            "LX/Byw;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 366316
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-object/from16 v3, p4

    .line 366317
    check-cast v3, LX/1Po;

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    .line 366318
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object/from16 v0, p3

    invoke-virtual {v4, v3, v0}, LX/26D;->a(LX/1PT;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26O;

    move-result-object v7

    .line 366319
    invoke-virtual {v7}, LX/26O;->a()LX/0Px;

    move-result-object v9

    .line 366320
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    sget-object v6, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-object/from16 v4, p4

    check-cast v4, LX/1Pt;

    invoke-virtual {v5, v6, v3, v4, v9}, LX/26D;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pt;LX/0Px;)V

    .line 366321
    invoke-static {v2}, LX/26D;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    move-result-object v3

    .line 366322
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v4

    invoke-static {v2, v4, v3}, LX/26D;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)I

    move-result v11

    .line 366323
    invoke-static/range {p2 .. p2}, LX/1mh;->b(I)I

    move-result v4

    .line 366324
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-static {v3}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/0yc;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->d()I

    move-result v3

    .line 366325
    :goto_0
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 366326
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v18

    .line 366327
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->g:LX/0yc;

    invoke-virtual {v3}, LX/0yc;->k()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    .line 366328
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->h:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, LX/1Ad;

    .line 366329
    sget-object v5, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentStoryItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 366330
    if-eqz v3, :cond_2

    invoke-virtual/range {v16 .. v16}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    .line 366331
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0, v9}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;)LX/26E;

    move-result-object v14

    .line 366332
    instance-of v3, v5, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v3, :cond_3

    move-object v3, v5

    check-cast v3, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->r()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v3, v5

    .line 366333
    check-cast v3, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-static {v3, v2, v10, v11}, LX/26D;->a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;II)V

    .line 366334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3u;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/C3u;->c(LX/1De;)LX/C3s;

    move-result-object v2

    check-cast p4, LX/1Pq;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, LX/C3s;->a(LX/1Pq;)LX/C3s;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/C3s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3s;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static/range {p1 .. p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 366335
    :goto_3
    return-object v2

    .line 366336
    :cond_0
    invoke-virtual {v7, v4}, LX/26O;->a(I)I

    move-result v3

    goto/16 :goto_0

    .line 366337
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 366338
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 366339
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v12

    .line 366340
    invoke-virtual/range {p1 .. p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 366341
    invoke-static {}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a()Landroid/graphics/Rect;

    move-result-object v5

    .line 366342
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 366343
    if-eqz p5, :cond_4

    move-object/from16 v5, p5

    .line 366344
    :cond_4
    const/4 v13, 0x0

    :goto_4
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-ge v13, v2, :cond_5

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p3

    move-object/from16 v15, p7

    move/from16 v17, p6

    .line 366345
    invoke-static/range {v2 .. v17}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;LX/1De;ILandroid/graphics/Rect;LX/1Pe;LX/26O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;IIIILX/26E;LX/1np;LX/1Ad;Z)LX/1Dg;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 366346
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 366347
    :cond_5
    invoke-static {v5}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(Landroid/graphics/Rect;)V

    .line 366348
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;LX/1De;LX/26E;)LX/1Dg;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_3
.end method
