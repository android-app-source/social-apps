.class public Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static e:Ljava/lang/Boolean;

.field private static n:LX/0Xm;


# instance fields
.field private final f:LX/1VL;

.field private final g:LX/0ad;

.field private final h:LX/0Uh;

.field private final i:LX/0qn;

.field private final j:LX/23t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/23t",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final k:LX/1V0;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366217
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/23t;LX/1V0;LX/0qn;LX/0ad;LX/0Uh;LX/1VL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366274
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 366275
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->j:LX/23t;

    .line 366276
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->k:LX/1V0;

    .line 366277
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->i:LX/0qn;

    .line 366278
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->g:LX/0ad;

    .line 366279
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->h:LX/0Uh;

    .line 366280
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->f:LX/1VL;

    .line 366281
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 366271
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 366272
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->j:LX/23t;

    invoke-virtual {v1, p1}, LX/23t;->c(LX/1De;)LX/240;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/240;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/240;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/240;->a(LX/1Pe;)LX/240;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 366273
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->k:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;
    .locals 11

    .prologue
    .line 366260
    const-class v1, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 366261
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366262
    sput-object v2, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366263
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366264
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366265
    new-instance v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/23t;->a(LX/0QB;)LX/23t;

    move-result-object v5

    check-cast v5, LX/23t;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v7

    check-cast v7, LX/0qn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v10

    check-cast v10, LX/1VL;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/23t;LX/1V0;LX/0qn;LX/0ad;LX/0Uh;LX/1VL;)V

    .line 366266
    move-object v0, v3

    .line 366267
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366268
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366269
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366270
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qn;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 366254
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 366255
    if-eqz v2, :cond_2

    .line 366256
    invoke-static {v2}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 366257
    :cond_0
    :goto_0
    return v0

    .line 366258
    :cond_1
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 366259
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 366253
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 366252
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 366229
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 366230
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->i:LX/0qn;

    invoke-static {v1, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366231
    :cond_0
    :goto_0
    return v0

    .line 366232
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->h:LX/0Uh;

    const/16 v2, 0x36

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 366233
    if-eqz v1, :cond_0

    .line 366234
    const/4 v2, 0x0

    .line 366235
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 366236
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    .line 366237
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    const/4 v3, 0x5

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v3, v2

    .line 366238
    :goto_1
    if-ge v3, v5, :cond_5

    .line 366239
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 366240
    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 366241
    const/4 v1, 0x1

    .line 366242
    :goto_2
    move v1, v1

    .line 366243
    if-eqz v1, :cond_3

    .line 366244
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 366245
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->g:LX/0ad;

    sget-short v2, LX/1Dd;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->l:Ljava/lang/Boolean;

    .line 366246
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366247
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->f:LX/1VL;

    .line 366248
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 366249
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    goto :goto_0

    .line 366250
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_5
    move v1, v2

    .line 366251
    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 366227
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 366228
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 3

    .prologue
    .line 366224
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 366225
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->g:LX/0ad;

    sget-short v1, LX/1Dd;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->e:Ljava/lang/Boolean;

    .line 366226
    :cond_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->d:LX/1Cz;

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->d()LX/1Cz;

    move-result-object v0

    goto :goto_0
.end method

.method public final iV_()Z
    .locals 3

    .prologue
    .line 366218
    invoke-super {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->iV_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366219
    const/4 v0, 0x1

    .line 366220
    :goto_0
    return v0

    .line 366221
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 366222
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->g:LX/0ad;

    sget-short v1, LX/1Dd;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    .line 366223
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
