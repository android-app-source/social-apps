.class public Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3J1;",
        "LX/2oL;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/2mj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2mj",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2mj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461229
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 461230
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a:LX/2mj;

    .line 461231
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
    .locals 4

    .prologue
    .line 461218
    const-class v1, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    monitor-enter v1

    .line 461219
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461220
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461221
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461222
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 461223
    new-instance p0, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    invoke-static {v0}, LX/2mj;->a(LX/0QB;)LX/2mj;

    move-result-object v3

    check-cast v3, LX/2mj;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;-><init>(LX/2mj;)V

    .line 461224
    move-object v0, p0

    .line 461225
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461226
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461227
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461228
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/3J1;LX/2oL;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3J1;",
            "LX/2oL;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 461216
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a:LX/2mj;

    iget-object v3, p1, LX/3J1;->c:LX/093;

    iget-object v4, p1, LX/3J1;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, p1, LX/3J1;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v6, p1, LX/3J1;->f:LX/04D;

    iget-object v7, p1, LX/3J1;->g:LX/3FX;

    move-object v1, p3

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, LX/2mj;->a(Landroid/view/View;LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    .line 461217
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 461212
    check-cast p2, LX/3J1;

    check-cast p3, LX/1Pr;

    .line 461213
    iget-object v0, p2, LX/3J1;->a:LX/1KL;

    iget-object p0, p2, LX/3J1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p3, v0, p0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    move-object v0, v0

    .line 461214
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x792eb213

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461215
    check-cast p1, LX/3J1;

    check-cast p2, LX/2oL;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a(LX/3J1;LX/2oL;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, -0x7f127ad

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
