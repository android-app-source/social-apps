.class public Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1Bv;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1VK;

.field private final d:LX/1JD;

.field private final e:LX/1Lr;


# direct methods
.method public constructor <init>(LX/1Bv;LX/0Or;LX/1VK;LX/1JD;LX/1Lr;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsHScrollVideoAutoPlayEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Bv;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1VK;",
            "LX/1JD;",
            "LX/1Lr;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459845
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a:LX/1Bv;

    .line 459846
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->b:LX/0Or;

    .line 459847
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->c:LX/1VK;

    .line 459848
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->d:LX/1JD;

    .line 459849
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->e:LX/1Lr;

    .line 459850
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;
    .locals 9

    .prologue
    .line 459851
    const-class v1, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    monitor-enter v1

    .line 459852
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459853
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459856
    new-instance v3, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v4

    check-cast v4, LX/1Bv;

    const/16 v5, 0x315

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, LX/1VK;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1VK;

    const-class v7, LX/1JD;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1JD;

    invoke-static {v0}, LX/1Lr;->a(LX/0QB;)LX/1Lr;

    move-result-object v8

    check-cast v8, LX/1Lr;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;-><init>(LX/1Bv;LX/0Or;LX/1VK;LX/1JD;LX/1Lr;)V

    .line 459857
    move-object v0, v3

    .line 459858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILX/1Po;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "I",
            "LX/1Po;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 459862
    invoke-interface {p4}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    .line 459863
    sget-object v0, LX/1Qt;->PAGE_TIMELINE:LX/1Qt;

    invoke-virtual {v3, v0}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1Qt;->MY_TIMELINE:LX/1Qt;

    invoke-virtual {v3, v0}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1Qt;->OTHER_PERSON_TIMELINE:LX/1Qt;

    invoke-virtual {v3, v0}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1Qt;->MY_TIMELINE_VIDEO:LX/1Qt;

    invoke-virtual {v3, v0}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1Qt;->OTHER_PERSON_TIMELINE_VIDEO:LX/1Qt;

    invoke-virtual {v3, v0}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    .line 459864
    :goto_0
    const/4 v2, 0x0

    .line 459865
    sget-object v4, LX/1Qt;->FEED:LX/1Qt;

    invoke-virtual {v3, v4}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, LX/1Qt;->GOOD_FRIENDS_FEED:LX/1Qt;

    invoke-virtual {v3, v4}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 459866
    :cond_1
    sget-object v0, LX/1Li;->NEWSFEED:LX/1Li;

    move-object v2, v0

    .line 459867
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->e:LX/1Lr;

    iget-boolean v0, v0, LX/1Lr;->d:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 459868
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 459869
    goto :goto_0

    .line 459870
    :cond_5
    if-eqz v0, :cond_6

    .line 459871
    sget-object v0, LX/1Li;->TIMELINE:LX/1Li;

    move-object v2, v0

    goto :goto_1

    .line 459872
    :cond_6
    sget-object v0, LX/1Qt;->VIDEO_CHANNEL:LX/1Qt;

    if-ne v3, v0, :cond_7

    .line 459873
    sget-object v0, LX/1Li;->CHANNEL:LX/1Li;

    move-object v2, v0

    goto :goto_1

    .line 459874
    :cond_7
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->e:LX/1Lr;

    iget-boolean v0, v0, LX/1Lr;->e:Z

    if-eqz v0, :cond_2

    .line 459875
    sget-object v0, LX/1Li;->MISC:LX/1Li;

    move-object v2, v0

    goto :goto_1

    .line 459876
    :cond_8
    if-eq p3, v5, :cond_9

    .line 459877
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 459878
    if-nez p3, :cond_3

    .line 459879
    :cond_9
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 459880
    if-eqz v0, :cond_3

    .line 459881
    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 459882
    if-eqz v0, :cond_3

    .line 459883
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->c:LX/1VK;

    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v0, v4}, LX/1VK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;)LX/2oO;

    move-result-object v0

    .line 459884
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/2oO;->v:Z

    .line 459885
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->d:LX/1JD;

    const-class v1, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    .line 459886
    invoke-virtual {v0, p2}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/Void;

    goto :goto_2
.end method
