.class public Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2G;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/7gM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460871
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 460872
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;
    .locals 3

    .prologue
    .line 460873
    const-class v1, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    monitor-enter v1

    .line 460874
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460875
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460876
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460877
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 460878
    new-instance v0, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;-><init>()V

    .line 460879
    move-object v0, v0

    .line 460880
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460881
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/FullscreenVideoCoverPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460882
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 460884
    check-cast p2, LX/C2G;

    check-cast p3, LX/1Pt;

    .line 460885
    iget-object v1, p2, LX/C2G;->a:LX/C2F;

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v0

    invoke-virtual {v1, v0, p3}, LX/C2F;->a(LX/04D;LX/1Pt;)V

    .line 460886
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x15203c63

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 460887
    check-cast p1, LX/C2G;

    check-cast p4, LX/7gM;

    .line 460888
    iget-object v1, p1, LX/C2G;->a:LX/C2F;

    invoke-virtual {v1, p4}, LX/C2F;->a(LX/3iB;)V

    .line 460889
    const/16 v1, 0x1f

    const v2, -0x176e1454

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
