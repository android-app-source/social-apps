.class public Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/36r;",
        "Ljava/lang/Void;",
        "LX/1Po;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459841
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 459842
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    .line 459843
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;
    .locals 4

    .prologue
    .line 459825
    const-class v1, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    monitor-enter v1

    .line 459826
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459827
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459828
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459829
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459830
    new-instance p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;)V

    .line 459831
    move-object v0, p0

    .line 459832
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 459836
    check-cast p2, LX/36r;

    check-cast p3, LX/1Po;

    .line 459837
    const-string v0, "VideoPrefetchPartDefinition.prepare"

    const v1, -0x284d526

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 459838
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    .line 459839
    iget-object v1, p2, LX/36r;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p0, p2, LX/36r;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget p1, p2, LX/36r;->c:I

    invoke-virtual {v0, v1, p0, p1, p3}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILX/1Po;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459840
    const v0, 0x18ee1e37

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x5b965261

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
