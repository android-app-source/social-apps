.class public Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3FZ;",
        "LX/3FY;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/2mi;


# direct methods
.method public constructor <init>(LX/2mi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461128
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 461129
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;->a:LX/2mi;

    .line 461130
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;
    .locals 4

    .prologue
    .line 461137
    const-class v1, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    monitor-enter v1

    .line 461138
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461139
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461140
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461141
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 461142
    new-instance p0, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;

    invoke-static {v0}, LX/2mi;->a(LX/0QB;)LX/2mi;

    move-result-object v3

    check-cast v3, LX/2mi;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;-><init>(LX/2mi;)V

    .line 461143
    move-object v0, p0

    .line 461144
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461145
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461146
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 461148
    check-cast p2, LX/3FZ;

    .line 461149
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;->a:LX/2mi;

    invoke-virtual {v0, p2}, LX/2mi;->a(LX/3FZ;)LX/3FY;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x44a003f6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461134
    check-cast p1, LX/3FZ;

    check-cast p2, LX/3FY;

    .line 461135
    invoke-static {p1, p2, p4}, LX/2mi;->a(LX/3FZ;LX/3FY;Landroid/view/View;)V

    .line 461136
    const/16 v1, 0x1f

    const v2, -0xabc4bb8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 461131
    check-cast p1, LX/3FZ;

    check-cast p2, LX/3FY;

    .line 461132
    invoke-static {p1, p2, p4}, LX/2mi;->b(LX/3FZ;LX/3FY;Landroid/view/View;)V

    .line 461133
    return-void
.end method
