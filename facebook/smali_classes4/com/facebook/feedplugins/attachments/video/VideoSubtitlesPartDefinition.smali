.class public Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3FS;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3J3;",
        "LX/3Fa;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/3Fb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Fb",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Fb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 547424
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 547425
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;->a:LX/3Fb;

    .line 547426
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;
    .locals 4

    .prologue
    .line 547409
    const-class v1, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;

    monitor-enter v1

    .line 547410
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 547411
    sput-object v2, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 547412
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547413
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 547414
    new-instance p0, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;

    invoke-static {v0}, LX/3Fb;->a(LX/0QB;)LX/3Fb;

    move-result-object v3

    check-cast v3, LX/3Fb;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;-><init>(LX/3Fb;)V

    .line 547415
    move-object v0, p0

    .line 547416
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 547417
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547418
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 547419
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 547420
    check-cast p2, LX/3J3;

    .line 547421
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/video/VideoSubtitlesPartDefinition;->a:LX/3Fb;

    .line 547422
    iget-object p0, p2, LX/3J3;->b:LX/0Px;

    iget-object p1, p2, LX/3J3;->a:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, LX/3Fb;->a(LX/0Px;Ljava/lang/String;)LX/3Fa;

    move-result-object p0

    move-object v0, p0

    .line 547423
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1168829b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 547408
    check-cast p2, LX/3Fa;

    invoke-static {p2, p4}, LX/3Fb;->a(LX/3Fa;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, -0x5dc3c31a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 547405
    check-cast p2, LX/3Fa;

    .line 547406
    invoke-static {p2}, LX/3Fb;->a(LX/3Fa;)V

    .line 547407
    return-void
.end method
