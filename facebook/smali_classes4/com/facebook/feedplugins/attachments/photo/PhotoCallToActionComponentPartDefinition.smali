.class public Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/36c;

.field private final e:LX/1V0;

.field public final f:LX/0Uh;

.field private final g:LX/1nA;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/36c;LX/1V0;LX/0Uh;LX/1nA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499526
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499527
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->d:LX/36c;

    .line 499528
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->e:LX/1V0;

    .line 499529
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->f:LX/0Uh;

    .line 499530
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->g:LX/1nA;

    .line 499531
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1;"
        }
    .end annotation

    .prologue
    .line 499558
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 499559
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->d:LX/36c;

    const/4 v2, 0x0

    .line 499560
    new-instance v3, LX/C0u;

    invoke-direct {v3, v1}, LX/C0u;-><init>(LX/36c;)V

    .line 499561
    sget-object v4, LX/36c;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C0t;

    .line 499562
    if-nez v4, :cond_0

    .line 499563
    new-instance v4, LX/C0t;

    invoke-direct {v4}, LX/C0t;-><init>()V

    .line 499564
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C0t;->a$redex0(LX/C0t;LX/1De;IILX/C0u;)V

    .line 499565
    move-object v3, v4

    .line 499566
    move-object v2, v3

    .line 499567
    move-object v1, v2

    .line 499568
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 499569
    iget-object v3, v1, LX/C0t;->a:LX/C0u;

    iput-object v2, v3, LX/C0u;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 499570
    iget-object v3, v1, LX/C0t;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 499571
    move-object v1, v1

    .line 499572
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->g:LX/1nA;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 499573
    iget-object v2, v1, LX/C0t;->a:LX/C0u;

    iput-object v0, v2, LX/C0u;->c:Landroid/view/View$OnClickListener;

    .line 499574
    iget-object v2, v1, LX/C0t;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 499575
    move-object v0, v1

    .line 499576
    iget-object v1, v0, LX/C0t;->a:LX/C0u;

    iput-object p3, v1, LX/C0u;->b:LX/1Pm;

    .line 499577
    iget-object v1, v0, LX/C0t;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499578
    move-object v0, v0

    .line 499579
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499580
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->e:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;
    .locals 9

    .prologue
    .line 499547
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499548
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499549
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499550
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499551
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499552
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/36c;->a(LX/0QB;)LX/36c;

    move-result-object v5

    check-cast v5, LX/36c;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v8

    check-cast v8, LX/1nA;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/36c;LX/1V0;LX/0Uh;LX/1nA;)V

    .line 499553
    move-object v0, v3

    .line 499554
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499555
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499556
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499557
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;"
        }
    .end annotation

    .prologue
    .line 499544
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499545
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499546
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499543
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499542
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499532
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499533
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499534
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 499535
    const/4 v0, 0x0

    .line 499536
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const/4 v1, 0x0

    .line 499537
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 499538
    :cond_1
    :goto_1
    move v0, v1

    .line 499539
    goto :goto_0

    .line 499540
    :cond_2
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 499541
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoCallToActionComponentPartDefinition;->f:LX/0Uh;

    const/16 p1, 0x2f3

    invoke-virtual {v2, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1
.end method
