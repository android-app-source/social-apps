.class public Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BnW;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/38u;

.field private final b:LX/38v;


# direct methods
.method public constructor <init>(LX/38u;LX/38v;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521727
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 521728
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->a:LX/38u;

    .line 521729
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->b:LX/38v;

    .line 521730
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;
    .locals 5

    .prologue
    .line 521716
    const-class v1, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    monitor-enter v1

    .line 521717
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 521718
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 521719
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521720
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 521721
    new-instance p0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;

    const-class v3, LX/38u;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/38u;

    const-class v4, LX/38v;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/38v;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;-><init>(LX/38u;LX/38v;)V

    .line 521722
    move-object v0, p0

    .line 521723
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 521724
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521725
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 521726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 521706
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 521707
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521708
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 521709
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Bnh;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 521710
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->v()Z

    move-result v1

    if-nez v1, :cond_1

    .line 521711
    :cond_0
    const/4 v0, 0x0

    .line 521712
    :goto_0
    return-object v0

    .line 521713
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    .line 521714
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->a:LX/38u;

    invoke-virtual {v1, p2, v0}, LX/38u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLEvent;)LX/By4;

    move-result-object v1

    .line 521715
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventActionButtonPartDefinition;->b:LX/38v;

    invoke-virtual {v2, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x74543c8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 521699
    check-cast p2, LX/BnW;

    check-cast p4, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;

    .line 521700
    iget-object v1, p4, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-object v1, v1

    .line 521701
    if-nez p2, :cond_0

    .line 521702
    invoke-virtual {v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a()V

    .line 521703
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x2b10375a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 521704
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 521705
    invoke-virtual {v1, p2}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    goto :goto_0
.end method
