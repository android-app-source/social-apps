.class public Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/2v2;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1kZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/2v2;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1V0;",
            "LX/2v2;",
            "LX/0Or",
            "<",
            "LX/1kZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499115
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 499116
    iput-object p2, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 499117
    iput-object p3, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->e:LX/2v2;

    .line 499118
    iput-object p4, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->f:LX/0Or;

    .line 499119
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 499120
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499121
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 499122
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499123
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 499124
    iget-object v2, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->e:LX/2v2;

    const/4 v3, 0x0

    .line 499125
    new-instance v4, LX/CCf;

    invoke-direct {v4, v2}, LX/CCf;-><init>(LX/2v2;)V

    .line 499126
    iget-object v5, v2, LX/2v2;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CCe;

    .line 499127
    if-nez v5, :cond_0

    .line 499128
    new-instance v5, LX/CCe;

    invoke-direct {v5, v2}, LX/CCe;-><init>(LX/2v2;)V

    .line 499129
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/CCe;->a$redex0(LX/CCe;LX/1De;IILX/CCf;)V

    .line 499130
    move-object v4, v5

    .line 499131
    move-object v3, v4

    .line 499132
    move-object v2, v3

    .line 499133
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 499134
    iget-object v3, v2, LX/CCe;->a:LX/CCf;

    iput-object v1, v3, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499135
    iget-object v3, v2, LX/CCe;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 499136
    move-object v1, v2

    .line 499137
    iget-object v2, v1, LX/CCe;->a:LX/CCf;

    iput-object v0, v2, LX/CCf;->a:Ljava/lang/String;

    .line 499138
    iget-object v2, v1, LX/CCe;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 499139
    move-object v0, v1

    .line 499140
    iget-object v1, v0, LX/CCe;->a:LX/CCf;

    iput-object p3, v1, LX/CCf;->c:LX/1Pf;

    .line 499141
    iget-object v1, v0, LX/CCe;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 499142
    move-object v0, v0

    .line 499143
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 499144
    iget-object v1, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;
    .locals 7

    .prologue
    .line 499145
    const-class v1, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 499146
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499147
    sput-object v2, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499148
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499149
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499150
    new-instance v6, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/2v2;->a(LX/0QB;)LX/2v2;

    move-result-object v5

    check-cast v5, LX/2v2;

    const/16 p0, 0x1225

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/2v2;LX/0Or;)V

    .line 499151
    move-object v0, v6

    .line 499152
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499153
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499154
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 499156
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 499157
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 499158
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 499159
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 499160
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 499161
    const/4 v1, 0x0

    .line 499162
    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 499163
    :cond_0
    :goto_0
    move v0, v1

    .line 499164
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feedplugins/storyline/CreateStorylineVideoCallToActionComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kZ;

    const/4 v1, 0x0

    .line 499165
    iget-object v2, v0, LX/1kZ;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 499166
    iget-object v2, v0, LX/1kZ;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/1kZ;->a:LX/0ad;

    sget-short p0, LX/1vU;->n:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1kZ;->d:Ljava/lang/Boolean;

    .line 499167
    :cond_2
    iget-object v1, v0, LX/1kZ;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 499168
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 499169
    :cond_4
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 499170
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const p1, -0x4e2ad180

    if-ne v2, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
