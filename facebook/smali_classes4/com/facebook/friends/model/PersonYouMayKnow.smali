.class public Lcom/facebook/friends/model/PersonYouMayKnow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/2lq;
.implements LX/2np;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:I

.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final g:Z

.field public h:Ljava/lang/String;

.field public i:Z

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 465221
    new-instance v0, LX/3Qz;

    invoke-direct {v0}, LX/3Qz;-><init>()V

    sput-object v0, Lcom/facebook/friends/model/PersonYouMayKnow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLFriendshipStatus;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 465209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465210
    iput-wide p1, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    .line 465211
    iput-object p3, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    .line 465212
    iput-object p4, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->c:Ljava/lang/String;

    .line 465213
    iput p5, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    .line 465214
    iput-object p6, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465216
    iput-boolean p7, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    .line 465217
    iput-object p8, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    .line 465218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    .line 465219
    iput-object p9, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    .line 465220
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 465195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465196
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    .line 465197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    .line 465198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->c:Ljava/lang/String;

    .line 465199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    .line 465200
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465201
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465202
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    .line 465203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    .line 465204
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    .line 465205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    .line 465206
    return-void

    :cond_0
    move v0, v2

    .line 465207
    goto :goto_0

    :cond_1
    move v1, v2

    .line 465208
    goto :goto_1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 465194
    iget-wide v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465193
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 465191
    iput-object p1, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465192
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 465222
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 465190
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 465189
    iget v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465173
    if-ne p0, p1, :cond_1

    .line 465174
    :cond_0
    :goto_0
    return v0

    .line 465175
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 465176
    goto :goto_0

    .line 465177
    :cond_3
    check-cast p1, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 465178
    iget-wide v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 465179
    iget-object v3, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v3

    .line 465180
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    .line 465181
    iget-boolean v3, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    move v3, v3

    .line 465182
    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    .line 465183
    iget-object v3, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v3, v3

    .line 465184
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    .line 465185
    iget-boolean v3, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    move v3, v3

    .line 465186
    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    .line 465187
    iget-object v3, p1, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v3

    .line 465188
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 465172
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 465171
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 465169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    .line 465170
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 465156
    iget-wide v4, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 465157
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465158
    invoke-virtual {p0}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465159
    iget v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465160
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 465161
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 465162
    iget-boolean v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465163
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465164
    iget-boolean v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->i:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465165
    iget-object v0, p0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465166
    return-void

    :cond_0
    move v0, v2

    .line 465167
    goto :goto_0

    :cond_1
    move v1, v2

    .line 465168
    goto :goto_1
.end method
