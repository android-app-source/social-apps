.class public Lcom/facebook/friends/model/FriendRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/2lq;
.implements LX/2ls;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:J

.field public final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Z

.field private final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/2lu;

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459113
    new-instance v0, LX/2lt;

    invoke-direct {v0}, LX/2lt;-><init>()V

    sput-object v0, Lcom/facebook/friends/model/FriendRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 459136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459137
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/friends/model/FriendRequest;->a:J

    .line 459138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    .line 459139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->c:Ljava/lang/String;

    .line 459140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->d:Ljava/lang/String;

    .line 459141
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 459142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->f:Ljava/lang/String;

    .line 459143
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    .line 459144
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2lu;

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 459145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    .line 459146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->i:Ljava/lang/String;

    .line 459147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/friends/model/FriendRequest;->k:Z

    .line 459148
    return-void

    :cond_0
    move v0, v2

    .line 459149
    goto :goto_0

    :cond_1
    move v1, v2

    .line 459150
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/String;LX/0Px;LX/2lu;ZLjava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2lu;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 459151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459152
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friends/model/FriendRequest;->a:J

    .line 459153
    iput-object p1, p0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    .line 459154
    iput-object p2, p0, Lcom/facebook/friends/model/FriendRequest;->c:Ljava/lang/String;

    .line 459155
    iput-object p3, p0, Lcom/facebook/friends/model/FriendRequest;->d:Ljava/lang/String;

    .line 459156
    iput-object p4, p0, Lcom/facebook/friends/model/FriendRequest;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 459157
    iput-object p5, p0, Lcom/facebook/friends/model/FriendRequest;->f:Ljava/lang/String;

    .line 459158
    iput-object p6, p0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    .line 459159
    iput-object p7, p0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 459160
    iput-boolean p8, p0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    .line 459161
    iput-object p9, p0, Lcom/facebook/friends/model/FriendRequest;->i:Ljava/lang/String;

    .line 459162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friends/model/FriendRequest;->k:Z

    .line 459163
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 459165
    iget-wide v0, p0, Lcom/facebook/friends/model/FriendRequest;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 459164
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 459134
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 459135
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 459133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FriendRequest model does not support this operation."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 459132
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 459131
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 459130
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 459128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friends/model/FriendRequest;->k:Z

    .line 459129
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 459114
    iget-wide v4, p0, Lcom/facebook/friends/model/FriendRequest;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 459115
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 459116
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 459117
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 459118
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 459119
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 459120
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 459121
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 459122
    iget-boolean v0, p0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 459123
    iget-object v0, p0, Lcom/facebook/friends/model/FriendRequest;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 459124
    iget-boolean v0, p0, Lcom/facebook/friends/model/FriendRequest;->k:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 459125
    return-void

    :cond_0
    move v0, v2

    .line 459126
    goto :goto_0

    :cond_1
    move v1, v2

    .line 459127
    goto :goto_1
.end method
