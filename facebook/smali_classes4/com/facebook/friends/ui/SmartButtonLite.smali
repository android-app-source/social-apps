.class public Lcom/facebook/friends/ui/SmartButtonLite;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/lang/CharSequence;

.field private c:F

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 445372
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;)V

    .line 445373
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 445374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 445336
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 445337
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 445338
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 445369
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 445370
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 445371
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 445364
    const-class v0, Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-static {v0, p0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 445365
    sget-object v0, LX/03r;->SmartButtonLite:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 445366
    const/16 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->c:F

    .line 445367
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 445368
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friends/ui/SmartButtonLite;

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->a:LX/0Ot;

    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 445359
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 445360
    if-nez v1, :cond_1

    .line 445361
    :cond_0
    :goto_0
    return v0

    .line 445362
    :cond_1
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 445363
    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 445375
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 445376
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextAppearance(Landroid/content/Context;I)V

    .line 445377
    iget v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 445378
    const/4 v0, 0x0

    iget v1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->c:F

    invoke-virtual {p0, v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextSize(IF)V

    .line 445379
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 445355
    invoke-virtual {p0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 445356
    iput-object p2, p0, Lcom/facebook/friends/ui/SmartButtonLite;->b:Ljava/lang/CharSequence;

    .line 445357
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->requestLayout()V

    .line 445358
    return-void
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 445354
    const/4 v0, 0x0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 445348
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onLayout(ZIIII)V

    .line 445349
    iget-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445350
    :cond_0
    :goto_0
    return-void

    .line 445351
    :cond_1
    invoke-direct {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445352
    iget-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 445353
    iget v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->d:I

    iget v1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->e:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->measure(II)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5167f208

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 445344
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onMeasure(II)V

    .line 445345
    iput p1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->d:I

    .line 445346
    iput p2, p0, Lcom/facebook/friends/ui/SmartButtonLite;->e:I

    .line 445347
    const/16 v1, 0x2d

    const v2, -0x1ecfddf9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setPreferredFontPixelSize(F)V
    .locals 0

    .prologue
    .line 445342
    iput p1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->c:F

    .line 445343
    return-void
.end method

.method public setShorterText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 445339
    iput-object p1, p0, Lcom/facebook/friends/ui/SmartButtonLite;->b:Ljava/lang/CharSequence;

    .line 445340
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->requestLayout()V

    .line 445341
    return-void
.end method

.method public setStyle(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 445323
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 445324
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 445325
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    sget-object v2, LX/03r;->SmartButtonLite:[I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 445326
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 445327
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 445328
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 445329
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 445330
    invoke-virtual {p0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 445331
    :cond_0
    :goto_0
    return-void

    .line 445332
    :cond_1
    if-nez v1, :cond_2

    .line 445333
    iget-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "SmartButtonLite.setStyle"

    const-string v3, "backgroundResId is not defined or not a resource."

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445334
    :cond_2
    if-nez v2, :cond_0

    .line 445335
    iget-object v0, p0, Lcom/facebook/friends/ui/SmartButtonLite;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "SmartButtonLite.setStyle"

    const-string v2, "textAppearanceResId is not defined or not a resource."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
