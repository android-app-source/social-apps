.class public Lcom/facebook/gif/AnimatedImagePlayButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:LX/6Wv;

.field public b:LX/6Wv;

.field public c:Landroid/widget/ImageView;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/widget/ImageView;

.field private f:Landroid/animation/Animator;

.field private g:Landroid/animation/Animator;

.field public h:LX/0wc;

.field private i:LX/0SG;

.field private j:J

.field private k:LX/0yc;

.field private l:LX/0yL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 547062
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 547063
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a()V

    .line 547064
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 547059
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 547060
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a()V

    .line 547061
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 547056
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 547057
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a()V

    .line 547058
    return-void
.end method

.method private a(F)Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 547051
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    const-string v1, "rotation"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    const/high16 v4, 0x43b40000    # 360.0f

    add-float/2addr v4, p1

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 547052
    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 547053
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 547054
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 547055
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 547041
    const-class v0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    invoke-static {v0, p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 547042
    const v0, 0x7f0307ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 547043
    const v0, 0x7f0d1475

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    .line 547044
    const v0, 0x7f0d1474

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    .line 547045
    const v0, 0x7f0d1473

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    .line 547046
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(F)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f:Landroid/animation/Animator;

    .line 547047
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f:Landroid/animation/Animator;

    new-instance v1, LX/3Ih;

    invoke-direct {v1, p0}, LX/3Ih;-><init>(Lcom/facebook/gif/AnimatedImagePlayButtonView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 547048
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->k:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547049
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->b()V

    .line 547050
    :cond_0
    return-void
.end method

.method private a(LX/0wc;LX/0SG;LX/0yc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 547037
    iput-object p1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->h:LX/0wc;

    .line 547038
    iput-object p2, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->i:LX/0SG;

    .line 547039
    iput-object p3, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->k:LX/0yc;

    .line 547040
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 547033
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleX(F)V

    .line 547034
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleY(F)V

    .line 547035
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 547036
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    invoke-static {v2}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v0

    check-cast v0, LX/0wc;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {v2}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v2

    check-cast v2, LX/0yc;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(LX/0wc;LX/0SG;LX/0yc;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 547029
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->l:LX/0yL;

    if-nez v0, :cond_0

    .line 547030
    new-instance v0, LX/6Ws;

    invoke-direct {v0, p0}, LX/6Ws;-><init>(Lcom/facebook/gif/AnimatedImagePlayButtonView;)V

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->l:LX/0yL;

    .line 547031
    :cond_0
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->k:LX/0yc;

    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->l:LX/0yL;

    invoke-virtual {v0, v1}, LX/0yc;->a(LX/0yL;)V

    .line 547032
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 547020
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 547021
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->g:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 547022
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->g:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 547023
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->g:Landroid/animation/Animator;

    .line 547024
    :cond_0
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(Landroid/view/View;)V

    .line 547025
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(Landroid/view/View;)V

    .line 547026
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(Landroid/view/View;)V

    .line 547027
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 547028
    return-void
.end method

.method private f()Landroid/animation/AnimatorSet;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 547007
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 547008
    new-array v1, v10, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    const-string v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v8

    iget-object v2, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    const-string v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v9

    iget-object v2, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    const-string v3, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 547009
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 547010
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 547011
    const/4 v2, 0x6

    new-array v2, v2, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    const-string v4, "scaleX"

    new-array v5, v7, [F

    fill-array-data v5, :array_3

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    new-array v5, v7, [F

    fill-array-data v5, :array_4

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    const-string v4, "scaleX"

    new-array v5, v7, [F

    fill-array-data v5, :array_5

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    new-array v5, v7, [F

    fill-array-data v5, :array_6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v10

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    const-string v5, "scaleX"

    new-array v6, v7, [F

    fill-array-data v6, :array_7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    const-string v5, "scaleY"

    new-array v6, v7, [F

    fill-array-data v6, :array_8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 547012
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 547013
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 547014
    const-wide/16 v4, 0x258

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 547015
    new-array v3, v10, [Landroid/animation/Animator;

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->h()Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 547016
    new-instance v0, LX/6Wt;

    invoke-direct {v0, p0}, LX/6Wt;-><init>(Lcom/facebook/gif/AnimatedImagePlayButtonView;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 547017
    return-object v2

    .line 547018
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 547019
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x3f8ccccd    # 1.1f
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x3f8ccccd    # 1.1f
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x3fc00000    # 1.5f
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x3fc00000    # 1.5f
        0x0
    .end array-data
.end method

.method public static g(Lcom/facebook/gif/AnimatedImagePlayButtonView;)V
    .locals 2

    .prologue
    .line 547003
    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->c:Landroid/widget/ImageView;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 547004
    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e:Landroid/widget/ImageView;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 547005
    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->d:Landroid/widget/ImageView;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 547006
    return-void
.end method

.method private h()Landroid/animation/Animator;
    .locals 5

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    .line 547000
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->j:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const v1, 0x451c4000    # 2500.0f

    div-float/2addr v0, v1

    .line 547001
    mul-float/2addr v0, v4

    rem-float/2addr v0, v4

    .line 547002
    invoke-direct {p0, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a(F)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setState(LX/6Wv;)V
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 546983
    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a:LX/6Wv;

    if-ne p1, v1, :cond_0

    .line 546984
    :goto_0
    return-void

    .line 546985
    :cond_0
    iget-object v1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->k:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546986
    iput-object p1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->b:LX/6Wv;

    .line 546987
    invoke-virtual {p0, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setVisibility(I)V

    goto :goto_0

    .line 546988
    :cond_1
    iput-object p1, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->a:LX/6Wv;

    .line 546989
    sget-object v1, LX/6Wv;->HIDDEN:LX/6Wv;

    if-ne p1, v1, :cond_2

    .line 546990
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setVisibility(I)V

    .line 546991
    sget-object v0, LX/6Wu;->a:[I

    invoke-virtual {p1}, LX/6Wv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 546992
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e()V

    goto :goto_0

    .line 546993
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 546994
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->e()V

    .line 546995
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->j:J

    .line 546996
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 546997
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 546998
    invoke-direct {p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->f()Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->g:Landroid/animation/Animator;

    .line 546999
    iget-object v0, p0, Lcom/facebook/gif/AnimatedImagePlayButtonView;->g:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
