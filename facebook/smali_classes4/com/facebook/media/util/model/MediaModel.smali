.class public final Lcom/facebook/media/util/model/MediaModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/1po;

.field public final c:I

.field public final d:Landroid/net/Uri;

.field public final e:I

.field public final f:I

.field public final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 465609
    new-instance v0, LX/2o4;

    invoke-direct {v0}, LX/2o4;-><init>()V

    sput-object v0, Lcom/facebook/media/util/model/MediaModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 465610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465611
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    .line 465612
    const-class v0, LX/1po;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1po;

    iput-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    .line 465613
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/util/model/MediaModel;->c:I

    .line 465614
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    .line 465615
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/util/model/MediaModel;->e:I

    .line 465616
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/util/model/MediaModel;->f:I

    .line 465617
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/media/util/model/MediaModel;->g:I

    .line 465618
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1po;)V
    .locals 1

    .prologue
    .line 465619
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;I)V

    .line 465620
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1po;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 465621
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;IIII)V

    .line 465622
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1po;IIII)V
    .locals 2

    .prologue
    .line 465623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465624
    iput-object p1, p0, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    .line 465625
    iput-object p2, p0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    .line 465626
    iput p3, p0, Lcom/facebook/media/util/model/MediaModel;->c:I

    .line 465627
    iget-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    .line 465628
    iput p4, p0, Lcom/facebook/media/util/model/MediaModel;->e:I

    .line 465629
    iput p5, p0, Lcom/facebook/media/util/model/MediaModel;->f:I

    .line 465630
    iput p6, p0, Lcom/facebook/media/util/model/MediaModel;->g:I

    .line 465631
    return-void

    .line 465632
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 465633
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 465634
    iget-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465635
    iget-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 465636
    iget v0, p0, Lcom/facebook/media/util/model/MediaModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465637
    iget-object v0, p0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 465638
    iget v0, p0, Lcom/facebook/media/util/model/MediaModel;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465639
    iget v0, p0, Lcom/facebook/media/util/model/MediaModel;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465640
    iget v0, p0, Lcom/facebook/media/util/model/MediaModel;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465641
    return-void
.end method
