.class public Lcom/facebook/divebar/contacts/DivebarFragment;
.super Lcom/facebook/ui/drawers/DrawerContentFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/location/FbLocationOperationParams;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field private C:LX/0Uh;

.field public D:LX/0y3;

.field private E:LX/0ad;

.field private F:LX/3LW;

.field public G:LX/3Na;

.field private H:LX/3Md;

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public M:J

.field public N:J

.field public O:I

.field public P:Z

.field private Q:Z

.field private R:LX/0Xl;

.field private S:LX/0Yb;

.field private T:LX/3Me;

.field public U:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/6Lx;",
            ">;"
        }
    .end annotation
.end field

.field private V:LX/3OO;

.field public W:LX/3OU;

.field private d:Landroid/content/Context;

.field private e:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field private f:LX/3Kk;

.field public g:LX/3Ku;

.field public h:LX/3Kv;

.field private i:LX/3LG;

.field private j:LX/3Kl;

.field private k:LX/3Lb;

.field private l:LX/3LY;

.field private m:LX/0WJ;

.field private n:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/3OZ;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0aG;

.field private q:LX/3LK;

.field public r:LX/3LL;

.field private s:LX/3LS;

.field private t:Ljava/util/concurrent/ExecutorService;

.field private u:LX/3LT;

.field private v:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field private w:LX/3LU;

.field private x:LX/2CH;

.field private y:LX/3Mg;

.field public z:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 548548
    const-class v0, Lcom/facebook/divebar/contacts/DivebarFragment;

    sput-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->a:Ljava/lang/Class;

    .line 548549
    sget-object v0, LX/0yF;->BALANCED_POWER_AND_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    .line 548550
    iput-wide v2, v0, LX/1S7;->b:J

    .line 548551
    move-object v0, v0

    .line 548552
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->b:Lcom/facebook/location/FbLocationOperationParams;

    .line 548553
    const-class v0, Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 548664
    invoke-direct {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;-><init>()V

    .line 548665
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->M:J

    .line 548666
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->O:I

    .line 548667
    return-void
.end method

.method private static a(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3Kk;LX/3Kl;LX/3Ku;LX/3Kv;LX/3LG;LX/0WJ;LX/1Ck;LX/0Or;LX/0aG;LX/3LK;LX/3LL;LX/3LS;Ljava/util/concurrent/ExecutorService;LX/3LT;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Xl;LX/3LU;LX/2CH;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/0y3;LX/0ad;LX/3LW;)V
    .locals 2
    .param p5    # LX/3Kv;
        .annotation runtime Lcom/facebook/messaging/annotations/ForDivebarList;
        .end annotation
    .end param
    .param p13    # LX/3LS;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p20    # Lcom/facebook/quicklog/QuickPerformanceLogger;
        .annotation runtime Lcom/facebook/divebar/contacts/IsDivebarScrollingPerfFixEnabled;
        .end annotation
    .end param
    .param p21    # LX/0Or;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            "LX/3Kk;",
            "LX/3Kl;",
            "LX/3Ku;",
            "LX/3Kv;",
            "LX/3LG;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/0aG;",
            "LX/3LK;",
            "LX/3LL;",
            "LX/3LS;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/3LT;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/0Xl;",
            "LX/3LU;",
            "Lcom/facebook/presence/PresenceManager;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0y3;",
            "LX/0ad;",
            "LX/3LW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 548668
    iput-object p1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->e:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 548669
    iput-object p2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->f:LX/3Kk;

    .line 548670
    iput-object p3, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->j:LX/3Kl;

    .line 548671
    iput-object p4, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->g:LX/3Ku;

    .line 548672
    iput-object p5, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->h:LX/3Kv;

    .line 548673
    iput-object p6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->i:LX/3LG;

    .line 548674
    iput-object p7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->m:LX/0WJ;

    .line 548675
    iput-object p8, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    .line 548676
    iput-object p9, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->o:LX/0Or;

    .line 548677
    iput-object p10, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->p:LX/0aG;

    .line 548678
    iput-object p11, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->q:LX/3LK;

    .line 548679
    iput-object p12, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    .line 548680
    iput-object p13, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->s:LX/3LS;

    .line 548681
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->t:Ljava/util/concurrent/ExecutorService;

    .line 548682
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->u:LX/3LT;

    .line 548683
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->v:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 548684
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->w:LX/3LU;

    .line 548685
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->R:LX/0Xl;

    .line 548686
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    .line 548687
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 548688
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->A:LX/0Or;

    .line 548689
    invoke-virtual/range {p22 .. p22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->B:Z

    .line 548690
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->C:LX/0Uh;

    .line 548691
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->D:LX/0y3;

    .line 548692
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->E:LX/0ad;

    .line 548693
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->F:LX/3LW;

    .line 548694
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 29

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-static/range {v28 .. v28}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static/range {v28 .. v28}, LX/3Kk;->a(LX/0QB;)LX/3Kk;

    move-result-object v4

    check-cast v4, LX/3Kk;

    invoke-static/range {v28 .. v28}, LX/3Kl;->a(LX/0QB;)LX/3Kl;

    move-result-object v5

    check-cast v5, LX/3Kl;

    invoke-static/range {v28 .. v28}, LX/3Ku;->a(LX/0QB;)LX/3Ku;

    move-result-object v6

    check-cast v6, LX/3Ku;

    invoke-static/range {v28 .. v28}, LX/3Kv;->a(LX/0QB;)LX/3Kv;

    move-result-object v7

    check-cast v7, LX/3Kv;

    invoke-static/range {v28 .. v28}, LX/3L6;->a(LX/0QB;)LX/3LG;

    move-result-object v8

    check-cast v8, LX/3LG;

    invoke-static/range {v28 .. v28}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v9

    check-cast v9, LX/0WJ;

    invoke-static/range {v28 .. v28}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    const/16 v11, 0xc81

    move-object/from16 v0, v28

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {v28 .. v28}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v12

    check-cast v12, LX/0aG;

    invoke-static/range {v28 .. v28}, LX/3LK;->a(LX/0QB;)LX/3LK;

    move-result-object v13

    check-cast v13, LX/3LK;

    invoke-static/range {v28 .. v28}, LX/3LL;->a(LX/0QB;)LX/3LL;

    move-result-object v14

    check-cast v14, LX/3LL;

    invoke-static/range {v28 .. v28}, LX/3LS;->a(LX/0QB;)LX/3LS;

    move-result-object v15

    check-cast v15, LX/3LS;

    invoke-static/range {v28 .. v28}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v16

    check-cast v16, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v28 .. v28}, LX/3LT;->a(LX/0QB;)LX/3LT;

    move-result-object v17

    check-cast v17, LX/3LT;

    invoke-static/range {v28 .. v28}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v18

    check-cast v18, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v28 .. v28}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v19

    check-cast v19, LX/0Xl;

    invoke-static/range {v28 .. v28}, LX/3LU;->a(LX/0QB;)LX/3LU;

    move-result-object v20

    check-cast v20, LX/3LU;

    invoke-static/range {v28 .. v28}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v21

    check-cast v21, LX/2CH;

    invoke-static/range {v28 .. v28}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v22

    check-cast v22, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v23, 0x148c

    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {v28 .. v28}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v24

    check-cast v24, Ljava/lang/Boolean;

    invoke-static/range {v28 .. v28}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v25

    check-cast v25, LX/0Uh;

    invoke-static/range {v28 .. v28}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v26

    check-cast v26, LX/0y3;

    invoke-static/range {v28 .. v28}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v27

    check-cast v27, LX/0ad;

    invoke-static/range {v28 .. v28}, LX/3LW;->a(LX/0QB;)LX/3LW;

    move-result-object v28

    check-cast v28, LX/3LW;

    invoke-static/range {v2 .. v28}, Lcom/facebook/divebar/contacts/DivebarFragment;->a(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3Kk;LX/3Kl;LX/3Ku;LX/3Kv;LX/3LG;LX/0WJ;LX/1Ck;LX/0Or;LX/0aG;LX/3LK;LX/3LL;LX/3LS;Ljava/util/concurrent/ExecutorService;LX/3LT;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Xl;LX/3LU;LX/2CH;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/0y3;LX/0ad;LX/3LW;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/location/ImmutableLocation;)V
    .locals 10
    .param p0    # Lcom/facebook/divebar/contacts/DivebarFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 548695
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    sget-object v1, LX/3OZ;->CHAT_CONTEXTS:LX/3OZ;

    new-instance v2, LX/3P2;

    invoke-direct {v2, p0, p1}, LX/3P2;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/location/ImmutableLocation;)V

    new-instance v3, LX/3P3;

    invoke-direct {v3, p0}, LX/3P3;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 548696
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    sget-object v1, LX/3OZ;->CHAT_CONTEXTS:LX/3OZ;

    invoke-virtual {v0, v1}, LX/1Ck;->b(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 548697
    new-instance v1, LX/3P5;

    invoke-direct {v1, p0}, LX/3P5;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 548698
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    if-eqz v1, :cond_0

    .line 548699
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    sget-object v2, LX/3OZ;->NEARBY_FRIENDS:LX/3OZ;

    new-instance v3, LX/3P6;

    invoke-direct {v3, p0, v0}, LX/3P6;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/3P7;

    invoke-direct {v0, p0}, LX/3P7;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 548700
    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->D:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->Q:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548701
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->F:LX/3LW;

    new-instance v1, LX/JHl;

    invoke-direct {v1, p0}, LX/JHl;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 548702
    new-instance v4, LX/Jk3;

    invoke-direct {v4}, LX/Jk3;-><init>()V

    move-object v5, v4

    .line 548703
    if-eqz p1, :cond_1

    .line 548704
    const-string v4, "latitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 548705
    const-string v4, "longitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 548706
    const-string v6, "accuracy_meters"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 548707
    const-string v4, "stale_time_seconds"

    iget-object v6, v0, LX/3LW;->e:LX/0yD;

    invoke-virtual {v6, p1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 548708
    :cond_1
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 548709
    iget-object v5, v0, LX/3LW;->b:LX/1Ck;

    const-string v6, "nearby_section"

    iget-object v7, v0, LX/3LW;->d:LX/0tX;

    invoke-virtual {v7, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    new-instance v7, LX/JHt;

    invoke-direct {v7, v0, v1}, LX/JHt;-><init>(LX/3LW;LX/JHl;)V

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 548710
    :cond_2
    return-void
.end method

.method private n()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 548711
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->K:Ljava/util/List;

    if-eqz v0, :cond_7

    :cond_2
    move v0, v2

    .line 548712
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->P:Z

    if-eqz v3, :cond_8

    const/4 v5, 0x0

    .line 548713
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/util/List;

    iget-object v4, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    aput-object v6, v3, v4

    const/4 v4, 0x2

    iget-object v6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->K:Ljava/util/List;

    aput-object v6, v3, v4

    const/4 v4, 0x3

    iget-object v6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->L:Ljava/util/List;

    aput-object v6, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 548714
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 548715
    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    :goto_2
    add-int/2addr v3, v4

    move v4, v3

    .line 548716
    goto :goto_1

    :cond_3
    move v3, v5

    .line 548717
    goto :goto_2

    .line 548718
    :cond_4
    move v3, v4

    .line 548719
    if-nez v3, :cond_8

    move v3, v2

    .line 548720
    :goto_3
    if-eqz v0, :cond_5

    if-eqz v3, :cond_6

    :cond_5
    move v1, v2

    :cond_6
    return v1

    :cond_7
    move v0, v1

    .line 548721
    goto :goto_0

    :cond_8
    move v3, v1

    .line 548722
    goto :goto_3
.end method

.method public static o(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 1

    .prologue
    .line 548723
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->m:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->m:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->Q:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->A:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548724
    :cond_0
    :goto_0
    return-void

    .line 548725
    :cond_1
    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-eqz v0, :cond_2

    .line 548726
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    invoke-virtual {v0}, LX/3Na;->a()V

    .line 548727
    :cond_2
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->k:LX/3Lb;

    invoke-virtual {v0}, LX/3Lb;->a()V

    goto :goto_0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 548728
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->C:LX/0Uh;

    const/16 v1, 0x534

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548729
    :goto_0
    return-void

    .line 548730
    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    sget-object v1, LX/3OZ;->LOCATION:LX/3OZ;

    new-instance v2, LX/3Oa;

    invoke-direct {v2, p0}, LX/3Oa;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    new-instance v3, LX/3Ob;

    invoke-direct {v3, p0}, LX/3Ob;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static r(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 15

    .prologue
    .line 548731
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-nez v0, :cond_0

    .line 548732
    :goto_0
    return-void

    .line 548733
    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    .line 548734
    iget-object v3, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    .line 548735
    iget-object v4, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->K:Ljava/util/List;

    .line 548736
    iget-object v6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->L:Ljava/util/List;

    .line 548737
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 548738
    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 548739
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 548740
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    invoke-virtual {v1, v0}, LX/3Na;->a(LX/0Px;)V

    .line 548741
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    invoke-virtual {v0}, LX/3Na;->a()V

    goto :goto_0

    .line 548742
    :cond_1
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->V:LX/3OO;

    if-eqz v2, :cond_2

    .line 548743
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->V:LX/3OO;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548744
    :cond_2
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 548745
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->s()Z

    move-result v2

    if-nez v2, :cond_10

    .line 548746
    :cond_3
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    if-eqz v0, :cond_4

    .line 548747
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548748
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->u:LX/3LT;

    new-instance v2, LX/3Ot;

    invoke-direct {v2, p0}, LX/3Ot;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 548749
    iget-object v8, v0, LX/3LT;->d:LX/0Uh;

    const/16 v10, 0x391

    invoke-virtual {v8, v10, v9}, LX/0Uh;->a(IZ)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 548750
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v3, v8

    .line 548751
    :cond_5
    if-eqz v3, :cond_8

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_8

    move v10, v7

    .line 548752
    :goto_2
    if-eqz v4, :cond_9

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_9

    move v8, v7

    .line 548753
    :goto_3
    if-nez v10, :cond_6

    if-eqz v8, :cond_a

    .line 548754
    :cond_6
    const/4 v11, 0x0

    .line 548755
    iget-object v7, v0, LX/3LT;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 548756
    iget-object v12, v0, LX/3LT;->d:LX/0Uh;

    const/16 v13, 0x391

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, LX/0Uh;->a(IZ)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 548757
    const v12, 0x7f0802ed

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v12, v7

    move-object v7, v11

    .line 548758
    :goto_4
    new-instance v13, LX/3Ou;

    new-instance v14, LX/3Ow;

    invoke-direct {v14, v0}, LX/3Ow;-><init>(LX/3LT;)V

    invoke-direct {v13, v12, v7, v14, v11}, LX/3Ou;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    move-object v7, v13

    .line 548759
    invoke-virtual {v1, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548760
    if-eqz v3, :cond_a

    .line 548761
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/user/model/User;

    .line 548762
    iget-object v12, v7, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v12, v12

    .line 548763
    invoke-interface {v5, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 548764
    sget-object v12, LX/3OH;->FAVORITES:LX/3OH;

    invoke-virtual {v2, v7, v12}, LX/3Ot;->a(Lcom/facebook/user/model/User;LX/3OH;)LX/3OO;

    move-result-object v12

    invoke-virtual {v1, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548765
    iget-object v12, v7, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v12

    .line 548766
    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_8
    move v10, v9

    .line 548767
    goto :goto_2

    :cond_9
    move v8, v9

    .line 548768
    goto :goto_3

    .line 548769
    :cond_a
    if-eqz v10, :cond_b

    if-eqz v8, :cond_b

    .line 548770
    new-instance v7, LX/DAb;

    invoke-direct {v7}, LX/DAb;-><init>()V

    invoke-virtual {v1, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548771
    :cond_b
    if-eqz v8, :cond_d

    .line 548772
    const/16 v7, 0xf

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 548773
    invoke-interface {v4, v9, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/user/model/User;

    .line 548774
    iget-object v9, v7, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v9, v9

    .line 548775
    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 548776
    sget-object v9, LX/3OH;->TOP_FRIENDS:LX/3OH;

    invoke-virtual {v2, v7, v9}, LX/3Ot;->a(Lcom/facebook/user/model/User;LX/3OH;)LX/3OO;

    move-result-object v9

    invoke-virtual {v1, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548777
    iget-object v9, v7, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v9

    .line 548778
    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 548779
    :cond_d
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 548780
    if-eqz v6, :cond_e

    .line 548781
    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 548782
    :cond_e
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 548783
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->l:LX/3LY;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 548784
    const/4 v0, 0x0

    .line 548785
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_f
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 548786
    iget-object v3, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 548787
    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 548788
    if-nez v2, :cond_12

    .line 548789
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0802ef

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 548790
    new-instance v3, LX/DAa;

    invoke-direct {v3, v2}, LX/DAa;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548791
    const/4 v3, 0x1

    .line 548792
    :goto_8
    iget-object v6, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->h:LX/3Kv;

    sget-object v7, LX/3OH;->UNKNOWN:LX/3OH;

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    .line 548793
    iget-object v8, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v8, v8

    .line 548794
    invoke-virtual {v2, v8}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Lx;

    invoke-virtual {v6, v0, v7, v2}, LX/3Kv;->a(Lcom/facebook/user/model/User;LX/3OH;LX/6Lx;)LX/3OO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 548795
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v2

    .line 548796
    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 548797
    goto :goto_7

    .line 548798
    :cond_10
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->F:LX/3LW;

    iget-object v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    invoke-virtual {v2, v1, v0, v7, v5}, LX/3LW;->a(LX/0Pz;Ljava/util/List;LX/0P1;Ljava/util/Set;)V

    goto/16 :goto_1

    .line 548799
    :cond_11
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 548800
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    invoke-virtual {v1, v0}, LX/3Na;->a(LX/0Px;)V

    goto/16 :goto_0

    :cond_12
    move v3, v2

    goto :goto_8

    .line 548801
    :cond_13
    const v12, 0x7f0802e8

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 548802
    const v13, 0x7f0802e2

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_4
.end method

.method private s()Z
    .locals 3

    .prologue
    .line 548663
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->E:LX/0ad;

    sget-short v1, LX/3NR;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 548803
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->a(Landroid/os/Bundle;)V

    .line 548804
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0104b1

    const v2, 0x7f0e0552

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->d:Landroid/content/Context;

    .line 548805
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->d:Landroid/content/Context;

    invoke-static {p0, v1}, Lcom/facebook/divebar/contacts/DivebarFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 548806
    new-instance v0, LX/3LY;

    invoke-direct {v0}, LX/3LY;-><init>()V

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->l:LX/3LY;

    .line 548807
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->j:LX/3Kl;

    .line 548808
    sget-object v1, LX/3LZ;->FAVORITE_FRIENDS:LX/3LZ;

    sget-object v2, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    sget-object p1, LX/3LZ;->ONLINE_FRIENDS:LX/3LZ;

    invoke-static {v1, v2, p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 548809
    new-instance v2, LX/3La;

    invoke-direct {v2, v1}, LX/3La;-><init>(Ljava/util/EnumSet;)V

    .line 548810
    iget-object v1, v0, LX/3Kl;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Lb;

    .line 548811
    iput-object v2, v1, LX/3Lb;->z:LX/3La;

    .line 548812
    move-object v0, v1

    .line 548813
    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->k:LX/3Lb;

    .line 548814
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->k:LX/3Lb;

    new-instance v1, LX/3Ma;

    invoke-direct {v1, p0}, LX/3Ma;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 548815
    iput-object v1, v0, LX/3Lb;->B:LX/3Mb;

    .line 548816
    new-instance v0, LX/3Mc;

    invoke-direct {v0, p0}, LX/3Mc;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->H:LX/3Md;

    .line 548817
    new-instance v0, LX/3Me;

    invoke-direct {v0, p0}, LX/3Me;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->T:LX/3Me;

    .line 548818
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->R:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.contacts.CONTACTS_UPLOAD_STATE_CHANGED"

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->T:LX/3Me;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.presence.PRESENCE_MANAGER_SETTINGS_CHANGED"

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->T:LX/3Me;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->T:LX/3Me;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.contacts.FAVORITE_CONTACT_SYNC_PROGRESS"

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->T:LX/3Me;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->S:LX/0Yb;

    .line 548819
    new-instance v0, LX/3Mf;

    invoke-direct {v0, p0}, LX/3Mf;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->y:LX/3Mg;

    .line 548820
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->y:LX/3Mg;

    invoke-virtual {v0, v1}, LX/2CH;->a(LX/3Mg;)V

    .line 548821
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    .line 548822
    iget-object v1, v0, LX/3LL;->i:LX/3LR;

    .line 548823
    iput-object p0, v1, LX/3LR;->a:Landroid/support/v4/app/Fragment;

    .line 548824
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->u:LX/3LT;

    .line 548825
    iget-object v1, v0, LX/3LT;->e:LX/3LR;

    .line 548826
    iput-object p0, v1, LX/3LR;->a:Landroid/support/v4/app/Fragment;

    .line 548827
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5f0001

    const/16 v2, 0x2c

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 548828
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->Q:Z

    .line 548829
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 548659
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->c()V

    .line 548660
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->Q:Z

    .line 548661
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    invoke-virtual {v0, p0}, LX/2CH;->a(Ljava/lang/Object;)V

    .line 548662
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 548654
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->d()V

    .line 548655
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->Q:Z

    .line 548656
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    if-eqz v0, :cond_0

    .line 548657
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    invoke-virtual {v0, p0}, LX/2CH;->b(Ljava/lang/Object;)V

    .line 548658
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x2383d879

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548627
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 548628
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->e:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v1}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 548629
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->g:LX/3Ku;

    invoke-virtual {v1}, LX/3Ku;->b()LX/0Px;

    move-result-object v1

    .line 548630
    if-eqz v1, :cond_0

    .line 548631
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    .line 548632
    :cond_0
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->f:LX/3Kk;

    invoke-virtual {v1}, LX/3Kk;->b()LX/0Px;

    move-result-object v1

    .line 548633
    if-eqz v1, :cond_1

    .line 548634
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->K:Ljava/util/List;

    .line 548635
    :cond_1
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->f:LX/3Kk;

    invoke-virtual {v1}, LX/3Kk;->a()LX/0Px;

    move-result-object v1

    .line 548636
    if-eqz v1, :cond_2

    .line 548637
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->L:Ljava/util/List;

    .line 548638
    :cond_2
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->C:LX/0Uh;

    const/16 v2, 0x534

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 548639
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->q:LX/3LK;

    invoke-virtual {v1}, LX/3LK;->a()LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    .line 548640
    :goto_0
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->s:LX/3LS;

    .line 548641
    iget-object v2, v1, LX/3LS;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Kv;

    iget-object v3, v1, LX/3LS;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 548642
    const/4 p1, 0x0

    sget-object v1, LX/3OH;->SELF_PROFILE:LX/3OH;

    invoke-static {v2, v3, p1, v1}, LX/3Kv;->a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;

    move-result-object p1

    invoke-virtual {p1}, LX/3OJ;->a()LX/3OO;

    move-result-object p1

    move-object v2, p1

    .line 548643
    move-object v1, v2

    .line 548644
    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->V:LX/3OO;

    .line 548645
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    invoke-virtual {v1}, LX/3LL;->a()LX/3OU;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    .line 548646
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    invoke-virtual {v1}, LX/3Na;->a()V

    .line 548647
    invoke-static {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 548648
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->k:LX/3Lb;

    invoke-virtual {v1}, LX/3Lb;->a()V

    .line 548649
    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->q()V

    .line 548650
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->S:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 548651
    const v1, -0x150c41a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 548652
    :cond_3
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 548653
    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3d7d4697

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 548615
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->i:LX/3LG;

    invoke-virtual {v0}, LX/3LG;->d()LX/3Mi;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->H:LX/3Md;

    invoke-interface {v0, v2}, LX/3Mi;->a(LX/3Md;)V

    .line 548616
    new-instance v0, LX/3Na;

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->i:LX/3LG;

    invoke-direct {v0, v2, v3}, LX/3Na;-><init>(Landroid/content/Context;LX/3LG;)V

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    .line 548617
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    const-string v3, "contacts_divebar"

    invoke-static {v2, v3, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 548618
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    new-instance v2, LX/3OF;

    invoke-direct {v2, p0}, LX/3OF;-><init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 548619
    iget-object v3, v0, LX/3Na;->a:LX/3Nb;

    .line 548620
    iget-object p1, v3, LX/3Nc;->g:LX/3Ne;

    new-instance v0, LX/3OG;

    invoke-direct {v0, v3, v2}, LX/3OG;-><init>(LX/3Nc;LX/3Nv;)V

    .line 548621
    iput-object v0, p1, LX/3Ne;->d:LX/3Nv;

    .line 548622
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    iget-boolean v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->B:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0802dd

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 548623
    iget-object v3, v2, LX/3Na;->a:LX/3Nb;

    invoke-virtual {v3, v0}, LX/3Nb;->setSearchHint(Ljava/lang/String;)V

    .line 548624
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x5f0001

    const/16 v3, 0x2d

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 548625
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    const v2, -0x53ea8ee9

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 548626
    :cond_0
    const v0, 0x7f0802dc

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x497618af

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548604
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroy()V

    .line 548605
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->y:LX/3Mg;

    invoke-virtual {v1, v2}, LX/2CH;->b(LX/3Mg;)V

    .line 548606
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->k:LX/3Lb;

    invoke-virtual {v1}, LX/3Lb;->b()V

    .line 548607
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->S:LX/0Yb;

    if-eqz v1, :cond_0

    .line 548608
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->S:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 548609
    :cond_0
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-eqz v1, :cond_1

    .line 548610
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    .line 548611
    iget-object v2, v1, LX/3Na;->c:LX/3O9;

    .line 548612
    invoke-static {v2}, LX/3O9;->c(LX/3O9;)V

    .line 548613
    :cond_1
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x5f0001

    const/4 v3, 0x4

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 548614
    const/16 v1, 0x2b

    const v2, 0x252f687b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x70b87a07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548601
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroyView()V

    .line 548602
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->n:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 548603
    const/16 v1, 0x2b

    const v2, 0x526b3ffb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 11

    .prologue
    const/16 v4, 0x2a

    const/4 v3, 0x2

    const v0, 0xe241089

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548575
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onResume()V

    .line 548576
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->x:LX/2CH;

    invoke-virtual {v1, p0}, LX/2CH;->a(Ljava/lang/Object;)V

    .line 548577
    const/4 v6, 0x1

    .line 548578
    const/4 v5, 0x0

    .line 548579
    iget-object v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->g:LX/3Ku;

    invoke-virtual {v7}, LX/3Ku;->c()J

    move-result-wide v7

    .line 548580
    const-wide/16 v9, 0x0

    cmp-long v9, v7, v9

    if-lez v9, :cond_0

    iget-wide v9, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->M:J

    cmp-long v9, v7, v9

    if-eqz v9, :cond_0

    .line 548581
    iget-object v9, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->g:LX/3Ku;

    invoke-virtual {v9}, LX/3Ku;->b()LX/0Px;

    move-result-object v9

    .line 548582
    if-eqz v9, :cond_0

    .line 548583
    invoke-static {v9}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->J:Ljava/util/List;

    .line 548584
    iput-wide v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->M:J

    move v5, v6

    .line 548585
    :cond_0
    iget-object v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    invoke-virtual {v7}, LX/3LL;->a()LX/3OU;

    move-result-object v7

    .line 548586
    iget-object v8, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    if-nez v8, :cond_1

    if-nez v7, :cond_2

    :cond_1
    iget-object v8, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    if-eqz v8, :cond_3

    if-nez v7, :cond_3

    .line 548587
    :cond_2
    iput-object v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->W:LX/3OU;

    move v5, v6

    .line 548588
    :cond_3
    iget-object v7, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->D:LX/0y3;

    invoke-virtual {v7}, LX/0y3;->a()LX/0yG;

    move-result-object v7

    sget-object v8, LX/0yG;->OKAY:LX/0yG;

    if-eq v7, v8, :cond_7

    .line 548589
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->I:Ljava/util/List;

    .line 548590
    :goto_0
    if-eqz v6, :cond_4

    .line 548591
    invoke-static {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 548592
    :cond_4
    invoke-direct {p0}, Lcom/facebook/divebar/contacts/DivebarFragment;->q()V

    .line 548593
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    .line 548594
    iget-object v2, v1, LX/3Na;->c:LX/3O9;

    iget-object v5, v1, LX/3Na;->e:LX/3OB;

    .line 548595
    iget-object v1, v2, LX/3O9;->b:Ljava/util/Queue;

    invoke-interface {v1, v5}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 548596
    iget-object v1, v2, LX/3O9;->b:Ljava/util/Queue;

    invoke-interface {v1, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 548597
    :cond_5
    iget-object v1, v2, LX/3O9;->d:LX/3OB;

    if-nez v1, :cond_6

    .line 548598
    invoke-static {v2}, LX/3O9;->b(LX/3O9;)V

    .line 548599
    :cond_6
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x5f0001

    invoke-interface {v1, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 548600
    const/16 v1, 0x2b

    const v2, 0x5ed556ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_7
    move v6, v5

    goto :goto_0
.end method

.method public final onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x440935fb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 548572
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onStop()V

    .line 548573
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x5f0001

    const/16 v3, 0x2f

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 548574
    const/16 v1, 0x2b

    const v2, 0x13998905

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 548562
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-eqz v0, :cond_0

    .line 548563
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->w:LX/3LU;

    .line 548564
    iput-object v1, v0, LX/3Na;->g:LX/3LU;

    .line 548565
    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    if-eqz v0, :cond_1

    .line 548566
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->r:LX/3LL;

    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->w:LX/3LU;

    .line 548567
    iput-object v1, v0, LX/3LL;->j:LX/3LU;

    .line 548568
    iget-object p0, v0, LX/3LL;->i:LX/3LR;

    const-string p1, "nearbyFriends"

    invoke-virtual {p0, p1}, LX/3LR;->a(Ljava/lang/String;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object p0

    check-cast p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    .line 548569
    if-eqz p0, :cond_1

    .line 548570
    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->h:LX/3LU;

    .line 548571
    :cond_1
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 548554
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->setUserVisibleHint(Z)V

    .line 548555
    if-nez p1, :cond_0

    .line 548556
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    if-eqz v0, :cond_0

    .line 548557
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarFragment;->G:LX/3Na;

    const-string v1, ""

    .line 548558
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p0

    if-nez p0, :cond_1

    .line 548559
    iget-object p0, v0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {p0}, LX/3Nb;->a()V

    .line 548560
    :cond_0
    :goto_0
    return-void

    .line 548561
    :cond_1
    iget-object p0, v0, LX/3Na;->a:LX/3Nb;

    invoke-virtual {p0, v1}, LX/3Nb;->setSearchBoxText(Ljava/lang/String;)V

    goto :goto_0
.end method
