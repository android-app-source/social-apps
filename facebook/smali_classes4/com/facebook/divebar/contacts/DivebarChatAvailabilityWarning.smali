.class public Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;
.super LX/3Nj;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 559781
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559782
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 559779
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559780
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 559745
    invoke-direct {p0, p1, p2, p3}, LX/3Nj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559746
    const-class v0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    invoke-static {v0, p0}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 559747
    const v0, 0x7f030d01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 559748
    const v0, 0x7f0d2080

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    .line 559749
    iput-object v0, p0, LX/3Nj;->a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    .line 559750
    const v0, 0x7f0d2081

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->d:Landroid/widget/Button;

    .line 559751
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->d:Landroid/widget/Button;

    new-instance v1, LX/3Nn;

    invoke-direct {v1, p0}, LX/3Nn;-><init>(Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 559752
    invoke-virtual {p0}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->f()V

    .line 559753
    return-void
.end method

.method private static a(Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 559778
    iput-object p1, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->c:LX/0Zb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0xfa6

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->a(Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Zb;)V

    return-void
.end method

.method public static g(Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 559762
    sget-object v0, LX/3Np;->ANIMATE_OUT:LX/3Np;

    iput-object v0, p0, LX/3Nj;->d:LX/3Np;

    .line 559763
    invoke-static {p0}, LX/3Nj;->f(LX/3Nj;)V

    .line 559764
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "button"

    .line 559765
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 559766
    move-object v0, v0

    .line 559767
    const-string v1, "divebar_availability_warning_turn_on"

    .line 559768
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 559769
    move-object v0, v0

    .line 559770
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 559771
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "chat_bar_online_status_change"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "chat_bar"

    .line 559772
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 559773
    move-object v0, v0

    .line 559774
    const-string v1, "state"

    invoke-virtual {v0, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "divebar_warning"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 559775
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 559776
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1qz;->a:LX/0Tn;

    invoke-interface {v0, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 559777
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 2

    .prologue
    .line 559754
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CA;

    .line 559755
    sget-object v1, LX/3CA;->DISABLED:LX/3CA;

    if-ne v0, v1, :cond_0

    .line 559756
    sget-object v0, LX/3Np;->SHOW:LX/3Np;

    iput-object v0, p0, LX/3Nj;->d:LX/3Np;

    .line 559757
    invoke-static {p0}, LX/3Nj;->f(LX/3Nj;)V

    .line 559758
    :goto_0
    return-void

    .line 559759
    :cond_0
    sget-object v0, LX/3Np;->HIDE:LX/3Np;

    iput-object v0, p0, LX/3Nj;->d:LX/3Np;

    .line 559760
    invoke-static {p0}, LX/3Nj;->f(LX/3Nj;)V

    .line 559761
    goto :goto_0
.end method
