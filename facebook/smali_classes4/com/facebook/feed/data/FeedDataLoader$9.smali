.class public final Lcom/facebook/feed/data/FeedDataLoader$9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/feed/data/FeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;JI)V
    .locals 0

    .prologue
    .line 486528
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader$9;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iput-wide p2, p0, Lcom/facebook/feed/data/FeedDataLoader$9;->a:J

    iput p4, p0, Lcom/facebook/feed/data/FeedDataLoader$9;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 486529
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 486530
    iget-wide v2, p0, Lcom/facebook/feed/data/FeedDataLoader$9;->a:J

    iget v1, p0, Lcom/facebook/feed/data/FeedDataLoader$9;->b:I

    .line 486531
    iget-object v4, v0, LX/00a;->c:Landroid/content/SharedPreferences;

    if-eqz v4, :cond_3

    iget-wide v4, v0, LX/00a;->f:J

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    iget v4, v0, LX/00a;->h:I

    if-eq v1, v4, :cond_3

    .line 486532
    :cond_0
    iget-object v4, v0, LX/00a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 486533
    iget-wide v6, v0, LX/00a;->f:J

    cmp-long v5, v2, v6

    if-eqz v5, :cond_1

    .line 486534
    iput-wide v2, v0, LX/00a;->f:J

    .line 486535
    const-string v5, "COLD_START_PRIME_INFO/LAST_HEAD_FETCH_TIME"

    iget-wide v6, v0, LX/00a;->f:J

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 486536
    :cond_1
    iget v5, v0, LX/00a;->h:I

    if-eq v1, v5, :cond_2

    .line 486537
    iput v1, v0, LX/00a;->h:I

    .line 486538
    const-string v5, "COLD_START_PRIME_INFO/FROZEN_FEED_TIME"

    iget v6, v0, LX/00a;->h:I

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 486539
    :cond_2
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 486540
    :cond_3
    return-void
.end method
