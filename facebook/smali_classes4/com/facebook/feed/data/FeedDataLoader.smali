.class public Lcom/facebook/feed/data/FeedDataLoader;
.super LX/0gD;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0gC;
.implements LX/0pR;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final D:Lcom/facebook/common/callercontext/CallerContext;

.field public static final E:Ljava/lang/String;


# instance fields
.field public A:LX/0YG;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public B:LX/0gf;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public C:I

.field private final F:LX/0aG;

.field private final G:LX/0Sh;

.field private final H:LX/0r5;

.field private final I:LX/0Sg;

.field private final J:LX/03V;

.field private final K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final L:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/325;",
            ">;"
        }
    .end annotation
.end field

.field public final N:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23G;",
            ">;"
        }
    .end annotation
.end field

.field private final O:Z

.field public final P:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final R:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final S:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1lu;",
            ">;"
        }
    .end annotation
.end field

.field public final T:LX/0pW;

.field private final U:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Ym;",
            ">;"
        }
    .end annotation
.end field

.field private final V:LX/0pX;

.field private final W:LX/22i;

.field public final X:LX/22k;

.field public final Y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rz;",
            ">;"
        }
    .end annotation
.end field

.field public final Z:LX/0pl;

.field private aA:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aB:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aa:LX/0qX;

.field public final ab:LX/0Zb;

.field public final ac:LX/22o;

.field private final ad:LX/0Tf;

.field public final ae:LX/0ad;

.field public final af:LX/0SG;

.field private final ag:LX/22r;

.field public final ah:LX/22j;

.field private final ai:LX/22p;

.field private final aj:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30r;",
            ">;"
        }
    .end annotation
.end field

.field private final ak:LX/0pT;

.field public final al:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rW;",
            ">;"
        }
    .end annotation
.end field

.field public final am:LX/0pd;

.field public final an:LX/0qa;

.field public final ao:LX/22q;

.field public final ap:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ajl;",
            ">;"
        }
    .end annotation
.end field

.field private final aq:LX/0qb;

.field public final ar:LX/0qf;

.field public as:LX/Ajr;

.field public at:J

.field public au:J

.field public av:LX/22v;

.field public aw:LX/22w;

.field private ax:LX/1EM;

.field private final ay:LX/0qj;

.field private final az:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18E;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/22z;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public q:LX/22z;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public r:LX/1Mv;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/22z;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public t:LX/22z;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public u:LX/0r8;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public v:LX/22t;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 360637
    const-class v0, Lcom/facebook/feed/data/FeedDataLoader;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/data/FeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    .line 360638
    const-class v0, Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/data/FeedDataLoader;->E:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/0Sh;LX/0Sg;LX/03V;LX/0pS;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0pT;LX/0pV;LX/0pW;LX/0Ot;LX/0pX;LX/0ad;LX/0pd;LX/0Or;LX/22i;LX/0Ot;LX/22j;LX/22k;LX/0Ot;LX/0pl;LX/0qX;LX/22o;LX/0Tf;LX/22p;LX/0qa;LX/0Ot;LX/0qb;LX/0qf;LX/22q;LX/0Zb;LX/0SG;LX/22r;LX/0qj;LX/0Or;LX/0Ot;LX/0Or;)V
    .locals 6
    .param p8    # LX/0Ot;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedLogFetchErrorsEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p13    # LX/0Ot;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p23    # LX/0Or;
        .annotation runtime Lcom/facebook/api/feed/annotation/IsBackToBackPtrEnabled;
        .end annotation
    .end param
    .param p32    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p43    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
        .end annotation
    .end param
    .param p45    # LX/0Or;
        .annotation runtime Lcom/facebook/work/feedplugins/findgroups/annotations/ShouldShowWorkFeedEndFindGroupsUnit;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0aG;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0pS;",
            "LX/0Ot",
            "<",
            "LX/23G;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/325;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1lu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30r;",
            ">;",
            "LX/0pT;",
            "LX/0pV;",
            "LX/0pW;",
            "LX/0Ot",
            "<",
            "LX/0Ym;",
            ">;",
            "LX/0pX;",
            "LX/0ad;",
            "LX/0pd;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/22i;",
            "LX/0Ot",
            "<",
            "LX/0rW;",
            ">;",
            "LX/22j;",
            "LX/22k;",
            "LX/0Ot",
            "<",
            "LX/0rz;",
            ">;",
            "LX/0pl;",
            "LX/0qX;",
            "LX/22o;",
            "LX/0Tf;",
            "LX/22p;",
            "LX/0qa;",
            "LX/0Ot",
            "<",
            "LX/Ajl;",
            ">;",
            "LX/0qb;",
            "LX/0qf;",
            "LX/22q;",
            "LX/0Zb;",
            "LX/0SG;",
            "LX/22r;",
            "LX/0qj;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/18E;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 360703
    invoke-direct {p0, p1}, LX/0gD;-><init>(Landroid/content/Context;)V

    .line 360704
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    .line 360705
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->w:Z

    .line 360706
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->x:Z

    .line 360707
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->y:Z

    .line 360708
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->z:Z

    .line 360709
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->at:J

    .line 360710
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 360711
    iput-object p2, p0, Lcom/facebook/feed/data/FeedDataLoader;->F:LX/0aG;

    .line 360712
    iput-object p3, p0, Lcom/facebook/feed/data/FeedDataLoader;->G:LX/0Sh;

    .line 360713
    iput-object p4, p0, Lcom/facebook/feed/data/FeedDataLoader;->I:LX/0Sg;

    .line 360714
    iput-object p5, p0, Lcom/facebook/feed/data/FeedDataLoader;->J:LX/03V;

    .line 360715
    iput-object p7, p0, Lcom/facebook/feed/data/FeedDataLoader;->N:LX/0Ot;

    .line 360716
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    .line 360717
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ab:LX/0Zb;

    .line 360718
    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    .line 360719
    new-instance v2, LX/22s;

    invoke-direct {v2, p0}, LX/22s;-><init>(Lcom/facebook/feed/data/FeedDataLoader;)V

    invoke-virtual {p6, v2}, LX/0pS;->a(LX/0r4;)LX/0r5;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    .line 360720
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->R:LX/0Or;

    .line 360721
    iput-object p8, p0, Lcom/facebook/feed/data/FeedDataLoader;->K:LX/0Ot;

    .line 360722
    iput-object p9, p0, Lcom/facebook/feed/data/FeedDataLoader;->L:LX/0Ot;

    .line 360723
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->M:LX/0Ot;

    .line 360724
    invoke-virtual/range {p11 .. p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    .line 360725
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->P:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 360726
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Q:LX/0Ot;

    .line 360727
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->S:LX/0Ot;

    .line 360728
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ah:LX/22j;

    .line 360729
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ai:LX/22p;

    .line 360730
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->W:LX/22i;

    .line 360731
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ag:LX/22r;

    .line 360732
    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ay:LX/0qj;

    .line 360733
    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->az:LX/0Ot;

    .line 360734
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->G:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 360735
    new-instance v2, LX/0r8;

    iget-object v3, p0, LX/0gD;->a:LX/0oy;

    invoke-virtual {v3}, LX/0oy;->u()J

    move-result-wide v4

    move-object/from16 v0, p17

    invoke-direct {v2, v0, v4, v5}, LX/0r8;-><init>(LX/0pV;J)V

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 360736
    new-instance v2, LX/22t;

    move-object/from16 v0, p17

    invoke-direct {v2, v0}, LX/22t;-><init>(LX/0pV;)V

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    .line 360737
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aj:LX/0Ot;

    .line 360738
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    .line 360739
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 360740
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->U:LX/0Ot;

    .line 360741
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->V:LX/0pX;

    .line 360742
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->am:LX/0pd;

    .line 360743
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->al:LX/0Ot;

    .line 360744
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->P:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0pP;->e:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->at:J

    .line 360745
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    iget-wide v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->at:J

    invoke-virtual {v2, v4, v5}, LX/0r5;->a(J)V

    .line 360746
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->X:LX/22k;

    .line 360747
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Y:LX/0Ot;

    .line 360748
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    .line 360749
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aa:LX/0qX;

    .line 360750
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ac:LX/22o;

    .line 360751
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ad:LX/0Tf;

    .line 360752
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->an:LX/0qa;

    .line 360753
    sget-object v2, LX/0gf;->UNKNOWN:LX/0gf;

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    .line 360754
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ao:LX/22q;

    .line 360755
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ap:LX/0Ot;

    .line 360756
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aq:LX/0qb;

    .line 360757
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ar:LX/0qf;

    .line 360758
    iget-object v2, p0, LX/0gD;->e:LX/0qz;

    sget-object v3, Lcom/facebook/feed/data/FeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/0qz;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 360759
    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aA:LX/0Or;

    .line 360760
    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aB:LX/0Or;

    .line 360761
    return-void
.end method

.method private N()I
    .locals 5

    .prologue
    .line 360698
    iget-object v0, p0, LX/0gD;->a:LX/0oy;

    .line 360699
    iget v1, v0, LX/0oy;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 360700
    iget-object v1, v0, LX/0oy;->a:LX/0W3;

    sget-wide v3, LX/0X5;->go:J

    const/16 v2, 0xa

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JI)I

    move-result v1

    iput v1, v0, LX/0oy;->d:I

    .line 360701
    :cond_0
    iget v1, v0, LX/0oy;->d:I

    move v0, v1

    .line 360702
    return v0
.end method

.method private O()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 360664
    sget-object v1, LX/22v;->NOT_SCHEDULED:LX/22v;

    .line 360665
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    .line 360666
    sget-object v1, LX/22w;->NOT_SCHEDULED:LX/22w;

    .line 360667
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    .line 360668
    const/4 v1, 0x1

    move v1, v1

    .line 360669
    if-eqz v1, :cond_0

    .line 360670
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 360671
    iget-object v2, v1, LX/0pW;->a:LX/0Yi;

    .line 360672
    const/4 v1, 0x1

    iput-boolean v1, v2, LX/0Yi;->A:Z

    .line 360673
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Q()V

    .line 360674
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360675
    :cond_1
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->P(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360676
    const/4 v0, 0x0

    .line 360677
    :goto_0
    return v0

    .line 360678
    :cond_2
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->b()V

    .line 360679
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->S()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 360680
    invoke-static {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->b$redex0(Lcom/facebook/feed/data/FeedDataLoader;Z)V

    .line 360681
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 360682
    iget-object v2, v1, LX/0pW;->a:LX/0Yi;

    .line 360683
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/0Yi;->y:Z

    .line 360684
    sget-object v4, LX/0Yi;->c:[LX/0Yj;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 360685
    iget-object v1, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360686
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 360687
    :cond_3
    invoke-static {v2}, LX/0Yi;->N(LX/0Yi;)V

    .line 360688
    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->S()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v1}, LX/0r8;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 360689
    iget-object v2, v1, LX/0r8;->d:LX/230;

    move-object v1, v2

    .line 360690
    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 360691
    iget-object v2, v1, LX/0r8;->d:LX/230;

    move-object v1, v2

    .line 360692
    iget-boolean v2, v1, LX/230;->g:Z

    move v1, v2

    .line 360693
    if-nez v1, :cond_7

    .line 360694
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->b()V

    goto :goto_0

    .line 360695
    :cond_6
    iget-boolean v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->w:Z

    if-nez v1, :cond_4

    .line 360696
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-virtual {p0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    goto :goto_2

    .line 360697
    :cond_7
    iput-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->x:Z

    goto :goto_0
.end method

.method public static P(Lcom/facebook/feed/data/FeedDataLoader;)V
    .locals 3

    .prologue
    .line 360659
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    .line 360660
    iget-object v1, v0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 360661
    if-nez v0, :cond_1

    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->N()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 360662
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    sget-object v1, LX/0gf;->SCROLLING:LX/0gf;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0rS;LX/0gf;Z)LX/0uO;

    .line 360663
    :cond_1
    return-void
.end method

.method private Q()V
    .locals 2

    .prologue
    .line 360655
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360656
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    new-instance v1, LX/22x;

    invoke-direct {v1, p0}, LX/22x;-><init>(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360657
    iput-object v1, v0, LX/0fz;->p:LX/22x;

    .line 360658
    :cond_0
    return-void
.end method

.method private S()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 360647
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->P:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0pP;->h:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360648
    :cond_0
    :goto_0
    return v0

    .line 360649
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Z()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360650
    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1, v2}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360651
    invoke-virtual {p0}, LX/0gD;->J()Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360652
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360653
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/facebook/feed/data/FeedDataLoader;->at:J

    iget-object v7, p0, LX/0gD;->a:LX/0oy;

    invoke-virtual {v7}, LX/0oy;->h()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 360654
    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static T(Lcom/facebook/feed/data/FeedDataLoader;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 360639
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    if-eqz v0, :cond_0

    .line 360640
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 360641
    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    .line 360642
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    if-eqz v0, :cond_1

    .line 360643
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0YG;->cancel(Z)Z

    .line 360644
    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    .line 360645
    :cond_1
    sget-object v0, LX/0rj;->SKIP_TAIL_GAP_FUTURE_CLEARED:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    .line 360646
    return-void
.end method

.method public static U(Lcom/facebook/feed/data/FeedDataLoader;)V
    .locals 4

    .prologue
    .line 360558
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    .line 360559
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 360560
    iget-object v1, v0, LX/0pl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0YG;

    .line 360561
    if-eqz v1, :cond_0

    .line 360562
    invoke-interface {v1, v2}, LX/0YG;->cancel(Z)Z

    .line 360563
    :cond_0
    iget-object v1, v0, LX/0pl;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0YG;

    .line 360564
    if-eqz v1, :cond_1

    .line 360565
    invoke-interface {v1, v2}, LX/0YG;->cancel(Z)Z

    .line 360566
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    if-eqz v0, :cond_2

    .line 360567
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 360568
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    .line 360569
    :cond_2
    return-void
.end method

.method public static V(Lcom/facebook/feed/data/FeedDataLoader;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 360632
    goto :goto_1

    .line 360633
    :cond_0
    :goto_0
    return v0

    .line 360634
    :goto_1
    const/4 v1, 0x1

    move v1, v1

    .line 360635
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->ac:LX/22o;

    invoke-virtual {v1}, LX/22o;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360636
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    invoke-virtual {v0}, LX/22w;->finishNetworkTailFetch()Z

    move-result v0

    goto :goto_0
.end method

.method private W()V
    .locals 2

    .prologue
    .line 360626
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->b()V

    .line 360627
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->a()V

    .line 360628
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    .line 360629
    iget-object v1, v0, LX/0r0;->a:LX/0r1;

    sget-object p0, LX/0r1;->INITIALIZED:LX/0r1;

    if-eq v1, p0, :cond_0

    .line 360630
    sget-object v1, LX/0r1;->NOT_INITIALIZED:LX/0r1;

    iput-object v1, v0, LX/0r0;->a:LX/0r1;

    .line 360631
    :cond_0
    return-void
.end method

.method public static Y(Lcom/facebook/feed/data/FeedDataLoader;)V
    .locals 2

    .prologue
    .line 360619
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->a()V

    .line 360620
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    .line 360621
    iget-object v1, v0, LX/0r0;->a:LX/0r1;

    sget-object p0, LX/0r1;->INITIAL_HEAD_LOAD_COMPLETE:LX/0r1;

    if-ne v1, p0, :cond_0

    .line 360622
    sget-object v1, LX/0r1;->INITIALIZED:LX/0r1;

    iput-object v1, v0, LX/0r0;->a:LX/0r1;

    .line 360623
    :cond_0
    iget-object v1, v0, LX/0r0;->a:LX/0r1;

    sget-object p0, LX/0r1;->INITIALIZED:LX/0r1;

    if-eq v1, p0, :cond_1

    .line 360624
    sget-object v1, LX/0r1;->INITIAL_TAIL_LOAD_COMPLETE:LX/0r1;

    iput-object v1, v0, LX/0r0;->a:LX/0r1;

    .line 360625
    :cond_1
    return-void
.end method

.method private Z()Z
    .locals 1

    .prologue
    .line 360618
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/data/FeedDataLoader;LX/0rS;ZLcom/facebook/api/feed/FetchFeedParams;Lcom/google/common/util/concurrent/SettableFuture;)LX/22z;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rS;",
            "Z",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ">;)",
            "LX/22z;"
        }
    .end annotation

    .prologue
    .line 360617
    new-instance v0, LX/234;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, LX/234;-><init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;Z)V

    return-object v0
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;ZLX/0gf;)LX/230;
    .locals 8

    .prologue
    .line 360607
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v5

    .line 360608
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 360609
    new-instance v0, LX/22y;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move v4, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/22y;-><init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;LX/0gf;ZLcom/google/common/util/concurrent/SettableFuture;LX/0rS;)V

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    .line 360610
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->I:LX/0Sg;

    invoke-virtual {v0, v5}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 360611
    new-instance v0, LX/230;

    iget-object v1, p0, LX/0gD;->o:LX/0rB;

    invoke-virtual {v1}, LX/0rB;->b()LX/0rn;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    .line 360612
    iget-object v5, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v5

    .line 360613
    iget-object v5, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, v5

    .line 360614
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_before"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, LX/230;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;LX/0rn;LX/22z;Ljava/lang/String;J)V

    .line 360615
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->X:LX/22k;

    invoke-virtual {v1, v0}, LX/22k;->a(LX/230;)V

    .line 360616
    return-object v0
.end method

.method private a(Ljava/lang/Throwable;Z)Ljava/lang/String;
    .locals 6

    .prologue
    .line 360591
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v2

    .line 360592
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    if-ne v2, v0, :cond_2

    .line 360593
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 360594
    :goto_0
    invoke-static {p1}, LX/1Bz;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    .line 360595
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    if-eq v2, v0, :cond_0

    .line 360596
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "time="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 360597
    if-eqz p2, :cond_3

    .line 360598
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/325;

    .line 360599
    iget-object v5, v0, LX/325;->a:LX/03V;

    const-string p2, "LastNewerLoadFailure"

    invoke-virtual {v5, p2, v4}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 360600
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ay:LX/0qj;

    invoke-virtual {v0}, LX/0qj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    if-eq v2, v0, :cond_1

    .line 360601
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->J:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Feed_Loading_Error_"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 360602
    :cond_1
    return-object v1

    .line 360603
    :cond_2
    invoke-virtual {v2}, LX/1nY;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 360604
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/325;

    .line 360605
    iget-object v5, v0, LX/325;->a:LX/03V;

    const-string p2, "LastOlderLoadFailure"

    invoke-virtual {v5, p2, v4}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 360606
    goto :goto_1
.end method

.method private static a(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360574
    iget-object v0, p1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 360575
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360576
    :goto_0
    return-object p0

    .line 360577
    :cond_0
    const-string v0, "pymgf_feed_unit"

    .line 360578
    const-string v1, "synthetic_cursor"

    .line 360579
    new-instance v2, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;

    invoke-direct {v2}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;-><init>()V

    .line 360580
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    new-instance v4, LX/1u8;

    invoke-direct {v4}, LX/1u8;-><init>()V

    .line 360581
    iput-object v2, v4, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 360582
    move-object v2, v4

    .line 360583
    iput-object v0, v2, LX/1u8;->d:Ljava/lang/String;

    .line 360584
    move-object v0, v2

    .line 360585
    sget-object v2, LX/0ql;->b:Ljava/lang/String;

    .line 360586
    iput-object v2, v0, LX/1u8;->i:Ljava/lang/String;

    .line 360587
    move-object v0, v0

    .line 360588
    iput-object v1, v0, LX/1u8;->c:Ljava/lang/String;

    .line 360589
    move-object v0, v0

    .line 360590
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 360570
    if-lez p1, :cond_0

    .line 360571
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->z:Z

    .line 360572
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 360573
    :cond_0
    return-void
.end method

.method private a(ILX/0rS;LX/0gf;Z)V
    .locals 14

    .prologue
    .line 361099
    iget-object v3, p0, LX/0gD;->e:LX/0qz;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    const/4 v11, 0x1

    invoke-virtual {p0}, LX/0gD;->E()J

    move-result-wide v12

    move v6, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p4

    invoke-virtual/range {v3 .. v13}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;ZZJ)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v2

    .line 361100
    sget-object v3, LX/0rj;->TAIL_FETCH:LX/0rj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Freshness:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 361101
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 361102
    move-object/from16 v0, p2

    move/from16 v1, p4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;LX/0rS;ZLcom/facebook/api/feed/FetchFeedParams;Lcom/google/common/util/concurrent/SettableFuture;)LX/22z;

    move-result-object v4

    .line 361103
    sget-object v5, LX/22w;->NOT_SCHEDULED:LX/22w;

    invoke-virtual {p0, v5}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/22w;)V

    .line 361104
    iget-object v5, p0, LX/0gD;->m:LX/0qk;

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, LX/0qk;->b(LX/0gf;)V

    .line 361105
    move-object/from16 v0, p2

    invoke-direct {p0, v4, v2, v0, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/22z;Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 361106
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v2}, LX/22t;->b()V

    .line 361107
    return-void
.end method

.method private a(JILX/0gf;)V
    .locals 5

    .prologue
    .line 361093
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    if-eqz v0, :cond_0

    .line 361094
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 361095
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    .line 361096
    :cond_0
    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    sget-object v1, LX/0gf;->RERANK:LX/0gf;

    invoke-virtual {p0}, LX/0gD;->E()J

    move-result-wide v2

    invoke-static {v0, p3, v1, v2, v3}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;ILX/0gf;J)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 361097
    invoke-static {p0, v0, p1, p2, p4}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;JLX/0gf;)V

    .line 361098
    return-void
.end method

.method private a(LX/22z;Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 9

    .prologue
    .line 361086
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    .line 361087
    iget-object v8, p0, Lcom/facebook/feed/data/FeedDataLoader;->X:LX/22k;

    new-instance v0, LX/230;

    iget-object v1, p0, LX/0gD;->o:LX/0rB;

    invoke-virtual {v1}, LX/0rB;->b()LX/0rn;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    .line 361088
    iget-object v5, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v5

    .line 361089
    iget-object v5, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, v5

    .line 361090
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_after"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v7}, LX/230;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;LX/0rn;LX/22z;Ljava/lang/String;J)V

    invoke-virtual {v8, v0}, LX/22k;->a(LX/230;)V

    .line 361091
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->I:LX/0Sg;

    invoke-virtual {v0, p4}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 361092
    return-void
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedParams;Z)V
    .locals 9

    .prologue
    .line 361080
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ad:LX/0Tf;

    new-instance v1, Lcom/facebook/feed/data/FeedDataLoader$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/feed/data/FeedDataLoader$5;-><init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;Z)V

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ac:LX/22o;

    .line 361081
    iget-object v5, v2, LX/22o;->b:LX/0qX;

    const/4 v6, 0x5

    .line 361082
    sget-wide v7, LX/0X5;->ck:J

    invoke-static {v5, v7, v8, v6}, LX/0qX;->a(LX/0qX;JI)I

    move-result v7

    move v5, v7

    .line 361083
    int-to-long v5, v5

    move-wide v2, v5

    .line 361084
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    .line 361085
    return-void
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedResult;LX/0qw;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 361063
    const-string v0, "FeedDataLoader.processSponsoredFeedUnitValidations"

    const v1, -0x64053688

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361064
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v4

    .line 361065
    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 361066
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 361067
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 361068
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->aA:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361069
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    move v1, v1

    .line 361070
    if-nez v1, :cond_1

    .line 361071
    :cond_0
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 361072
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 361073
    :cond_2
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 361074
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 361075
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23G;

    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1, p2}, LX/23G;->a(LX/0fz;Ljava/util/List;ZLX/0qw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361076
    :cond_3
    const v0, -0x51059d6e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361077
    return-void

    :cond_4
    move v1, v2

    .line 361078
    goto :goto_1

    .line 361079
    :catchall_0
    move-exception v0

    const v1, 0x19be78d8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedResult;LX/0ta;)V
    .locals 3

    .prologue
    .line 361054
    const-string v0, "FeedDataLoader.fetchFeedPostProcess"

    const v1, -0x44f6335

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361055
    :try_start_0
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne p2, v0, :cond_0

    .line 361056
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->V:LX/0pX;

    .line 361057
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 361058
    iget-boolean v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v2, v2

    .line 361059
    iget-object p0, v0, LX/0pX;->e:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;

    invoke-direct {p1, v0, v1, v2}, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;-><init>(LX/0pX;Lcom/facebook/api/feed/FetchFeedParams;Z)V

    const p2, 0x379b5dc7

    invoke-static {p0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361060
    :cond_0
    const v0, -0x6be90291

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361061
    return-void

    .line 361062
    :catchall_0
    move-exception v0

    const v1, -0x4930d950

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;JLX/0gf;)V
    .locals 5

    .prologue
    .line 361044
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 361045
    new-instance v0, LX/AjK;

    invoke-direct {v0, p0, p2, p3}, LX/AjK;-><init>(Lcom/facebook/feed/data/FeedDataLoader;J)V

    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    .line 361046
    invoke-virtual {p4}, LX/0gf;->needsReranking()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361047
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    .line 361048
    iget-object v2, v0, LX/0pl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v3, v0, LX/0pl;->b:LX/0Tf;

    new-instance v4, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;

    invoke-direct {v4, v0, v1, p1}, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;-><init>(LX/0pl;LX/0rl;Lcom/facebook/api/feed/FetchFeedParams;)V

    sget-object p0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, p2, p3, p0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 361049
    :cond_0
    :goto_0
    return-void

    .line 361050
    :cond_1
    invoke-virtual {p4}, LX/0gf;->needsEmptyReranking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361051
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    .line 361052
    iget-object v2, v0, LX/0pl;->i:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v3, v0, LX/0pl;->b:LX/0Tf;

    new-instance v4, Lcom/facebook/feed/data/FeedDataLoaderReranker$3;

    invoke-direct {v4, v0, p1, v1}, Lcom/facebook/feed/data/FeedDataLoaderReranker$3;-><init>(LX/0pl;Lcom/facebook/api/feed/FetchFeedParams;LX/0rl;)V

    sget-object p0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, p2, p3, p0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 361053
    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/data/FeedDataLoader;ZLX/18G;)V
    .locals 5

    .prologue
    .line 361039
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->F:LX/0aG;

    const-string v1, "feed_clear_cache"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, Lcom/facebook/feed/data/FeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    const v4, 0x7a4d4372

    invoke-static {v0, v1, v2, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 361040
    new-instance v1, LX/AjL;

    invoke-direct {v1, p0, p1, p2}, LX/AjL;-><init>(Lcom/facebook/feed/data/FeedDataLoader;ZLX/18G;)V

    .line 361041
    new-instance v2, LX/1Mv;

    invoke-direct {v2, v0, v1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->r:LX/1Mv;

    .line 361042
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->G:LX/0Sh;

    invoke-virtual {v2, v0, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 361043
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 361031
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-short v1, LX/0fe;->ay:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ax:LX/1EM;

    if-eqz v0, :cond_1

    .line 361032
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 361033
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 361034
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :goto_1
    move-wide v2, v0

    .line 361035
    goto :goto_0

    .line 361036
    :cond_0
    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 361037
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ax:LX/1EM;

    invoke-virtual {v0, v2, v3}, LX/1EM;->a(J)V

    .line 361038
    :cond_1
    return-void

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method private a(LX/0rS;)Z
    .locals 1

    .prologue
    .line 360804
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ac:LX/22o;

    .line 360805
    const/4 p1, 0x1

    move p1, p1

    .line 360806
    if-eqz p1, :cond_1

    iget-object p1, v0, LX/22o;->a:LX/0kb;

    invoke-virtual {p1}, LX/0kb;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    :goto_0
    move v0, p1

    .line 360807
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    invoke-virtual {v0}, LX/22w;->canSchedule()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/api/feed/FetchFeedResult;)Z
    .locals 2

    .prologue
    .line 361030
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;LX/23E;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 361022
    sget-object v0, LX/23F;->a:[I

    invoke-virtual {p1}, LX/23E;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 361023
    :goto_0
    return-void

    .line 361024
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    if-eqz v0, :cond_0

    .line 361025
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 361026
    :cond_0
    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    goto :goto_0

    .line 361027
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    if-eqz v0, :cond_1

    .line 361028
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 361029
    :cond_1
    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedResult;LX/23E;ZJ)V
    .locals 14

    .prologue
    .line 360916
    const-string v2, "FeedDataLoader.onFetchFeedSuccess"

    const v3, 0x684b11b8

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 360917
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->e()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/api/feed/FetchFeedParams;->i()LX/0gf;

    move-result-object v6

    .line 360918
    if-nez p3, :cond_0

    sget-object v2, LX/0gf;->RERANK:LX/0gf;

    if-eq v6, v2, :cond_0

    .line 360919
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;LX/23E;)V

    .line 360920
    :cond_0
    sget-object v2, LX/23E;->BEFORE:LX/23E;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_1

    sget-object v2, LX/0gf;->RERANK:LX/0gf;

    if-eq v6, v2, :cond_1

    .line 360921
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->U(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360922
    :cond_1
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->az:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/18E;

    invoke-virtual {v2}, LX/18E;->b()V

    .line 360923
    sget-object v5, LX/0qw;->FULL:LX/0qw;

    .line 360924
    sget-object v2, LX/0gf;->RERANK:LX/0gf;

    if-ne v6, v2, :cond_8

    .line 360925
    sget-object v2, LX/0rj;->PTR_RERANKING_COMPLETE:LX/0rj;

    invoke-virtual {p0, v2}, LX/0gD;->a(LX/0rj;)V

    .line 360926
    invoke-static {p1}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/api/feed/FetchFeedResult;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 360927
    sget-object v2, LX/0rj;->PTR_RERANKING_NO_RESULTS:LX/0rj;

    invoke-virtual {p0, v2}, LX/0gD;->a(LX/0rj;)V

    .line 360928
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getFreshness()LX/0ta;

    move-result-object v9

    .line 360929
    sget-object v2, LX/0rj;->DATA_LOADED:LX/0rj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Direction: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 360930
    sget-object v2, LX/23E;->BEFORE:LX/23E;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 360931
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 360932
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v2, v3}, LX/0pT;->a(I)V

    .line 360933
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->W:LX/22i;

    iget-object v4, p0, LX/0gD;->b:LX/0fz;

    iget-object v7, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v7}, LX/0pT;->c()J

    move-result-wide v10

    invoke-virtual {v2, p1, v4, v10, v11}, LX/22i;->a(Lcom/facebook/api/feed/FetchFeedResult;LX/0fz;J)Landroid/util/Pair;

    move-result-object v2

    .line 360934
    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v4, v2}, LX/0pT;->a(Landroid/util/Pair;)V

    .line 360935
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v2}, LX/0pT;->e()V

    .line 360936
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->aj:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/30r;

    iget-object v4, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2, v4, v3}, LX/30r;->a(Lcom/facebook/api/feedtype/FeedType;I)V

    .line 360937
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v4

    .line 360938
    if-eqz v4, :cond_4

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-nez v2, :cond_9

    .line 360939
    :cond_4
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v9, v2, :cond_5

    .line 360940
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->V:LX/0pX;

    invoke-virtual {v2, p1}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedResult;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360941
    :cond_5
    const v2, -0x2be561c6

    invoke-static {v2}, LX/02m;->a(I)V

    .line 360942
    :goto_1
    return-void

    .line 360943
    :cond_6
    :try_start_1
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->m()V

    .line 360944
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->T(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360945
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    if-eqz v2, :cond_7

    .line 360946
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    invoke-virtual {v2}, LX/22z;->c()V

    .line 360947
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    .line 360948
    :cond_7
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 360949
    :catchall_0
    move-exception v2

    const v3, -0x43e53400

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 360950
    :cond_8
    :try_start_2
    sget-object v2, LX/23E;->BEFORE:LX/23E;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 360951
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v2}, LX/0r8;->j()LX/0qw;

    move-result-object v5

    .line 360952
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    sget-object v3, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    invoke-virtual {v2, v3}, LX/0r8;->a(LX/0qw;)V

    goto/16 :goto_0

    .line 360953
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 360954
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ai:LX/22p;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/22p;->a(LX/23E;)V

    .line 360955
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v9, v2, :cond_a

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->e()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/api/feed/FetchFeedParams;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, LX/23E;->AFTER:LX/23E;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_a

    .line 360956
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->V:LX/0pX;

    invoke-virtual {v2, p1}, LX/0pX;->b(Lcom/facebook/api/feed/FetchFeedResult;)Z

    .line 360957
    iget-object v2, p0, LX/0gD;->k:LX/0r3;

    invoke-virtual {v2}, LX/0r3;->d()V

    .line 360958
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360959
    :cond_a
    :goto_2
    sget-object v2, LX/23E;->BEFORE:LX/23E;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_18

    const/4 v2, 0x1

    move v12, v2

    .line 360960
    :goto_3
    const/4 v2, 0x0

    .line 360961
    iget-boolean v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->z:Z

    if-eqz v3, :cond_19

    .line 360962
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(I)V

    move-object v3, v4

    .line 360963
    :goto_4
    const/4 v11, 0x0

    .line 360964
    if-eqz v12, :cond_b

    if-nez v2, :cond_b

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v9, v2, :cond_b

    .line 360965
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v2, v3}, LX/0fz;->a(Ljava/util/List;)I

    move-result v11

    .line 360966
    :cond_b
    sget-object v2, LX/18G;->SUCCESS:LX/18G;

    const/4 v4, 0x0

    invoke-static {p0, v12, p1, v2, v4}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;)V

    .line 360967
    iget-object v2, p0, LX/0gD;->m:LX/0qk;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0qk;->a(Z)V

    .line 360968
    const/4 v2, 0x0

    .line 360969
    if-eqz v12, :cond_c

    iget-object v4, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v4}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    .line 360970
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->e()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/api/feed/FetchFeedParams;->d()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getFreshness()LX/0ta;

    move-result-object v4

    sget-object v7, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v4, v7, :cond_e

    .line 360971
    iget-object v2, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v2}, LX/0qy;->c()Z

    move-result v4

    .line 360972
    iget-object v2, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v2}, LX/0qy;->a()V

    .line 360973
    invoke-virtual {p0}, LX/0gD;->K()LX/0Uh;

    move-result-object v2

    const/16 v7, 0x39d

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_d

    iget-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->aB:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1b

    :cond_d
    const/4 v2, 0x1

    .line 360974
    :goto_5
    if-nez v4, :cond_e

    iget-object v4, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v4}, LX/0qy;->c()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 360975
    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->V:LX/0pX;

    invoke-virtual {v4, p1}, LX/0pX;->b(Lcom/facebook/api/feed/FetchFeedResult;)Z

    .line 360976
    iget-object v4, p0, LX/0gD;->k:LX/0r3;

    invoke-virtual {v4}, LX/0r3;->d()V

    .line 360977
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V

    :cond_e
    move v4, v2

    .line 360978
    const/4 v2, 0x0

    .line 360979
    if-eqz v12, :cond_23

    .line 360980
    invoke-direct {p0, v6}, Lcom/facebook/feed/data/FeedDataLoader;->c(LX/0gf;)Z

    move-result v2

    if-nez v2, :cond_f

    invoke-direct {p0, v6}, Lcom/facebook/feed/data/FeedDataLoader;->d(LX/0gf;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, LX/0gD;->a:LX/0oy;

    invoke-virtual {v2}, LX/0oy;->s()Z

    move-result v2

    if-eqz v2, :cond_1c

    :cond_f
    const/4 v2, 0x1

    :goto_6
    move v8, v2

    .line 360981
    :goto_7
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    invoke-virtual {v2}, LX/0gf;->isAutoRefresh()Z

    move-result v10

    .line 360982
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 360983
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m()Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->y:Z

    .line 360984
    :cond_10
    if-eqz v12, :cond_1f

    .line 360985
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v9, v2, :cond_1d

    const/4 v7, 0x1

    .line 360986
    :goto_8
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_11

    if-eqz v7, :cond_11

    .line 360987
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->d()V

    .line 360988
    :cond_11
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    if-eqz v7, :cond_1e

    if-nez v10, :cond_12

    if-eqz v8, :cond_1e

    :cond_12
    const/4 v6, 0x1

    :goto_9
    invoke-virtual/range {v2 .. v7}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;ZZ)V

    .line 360989
    const/16 v2, 0x1

    .line 360990
    if-eqz v2, :cond_14

    if-eqz v7, :cond_14

    if-nez v10, :cond_13

    if-eqz v8, :cond_14

    .line 360991
    :cond_13
    invoke-direct {p0, v3}, Lcom/facebook/feed/data/FeedDataLoader;->b(Ljava/util/List;)V

    .line 360992
    :cond_14
    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-eq v9, v2, :cond_15

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v9, v2, :cond_16

    .line 360993
    :cond_15
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, LX/0r5;->a(J)V

    .line 360994
    invoke-direct {p0, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Ljava/util/List;)V

    .line 360995
    :cond_16
    :goto_a
    invoke-direct {p0, p1, v9}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/api/feed/FetchFeedResult;LX/0ta;)V

    .line 360996
    invoke-direct {p0, p1, v5}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/api/feed/FetchFeedResult;LX/0qw;)V

    .line 360997
    if-eqz v8, :cond_22

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v2}, LX/0pl;->i()Z

    move-result v2

    if-eqz v2, :cond_22

    sget-object v13, LX/18O;->Avoid:LX/18O;

    .line 360998
    :goto_b
    sget-object v9, LX/18G;->SUCCESS:LX/18G;

    const/4 v10, 0x0

    move-object v6, p0

    move v7, v12

    move-object v8, p1

    move-object v12, v5

    invoke-static/range {v6 .. v13}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V

    .line 360999
    invoke-virtual {p0}, LX/0gD;->H()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 361000
    const v2, -0xde4ab59

    invoke-static {v2}, LX/02m;->a(I)V

    goto/16 :goto_1

    .line 361001
    :cond_17
    :try_start_3
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ai:LX/22p;

    invoke-virtual {v2}, LX/22p;->a()V

    goto/16 :goto_2

    .line 361002
    :cond_18
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_3

    .line 361003
    :cond_19
    if-eqz v12, :cond_1a

    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->ai:LX/22p;

    invoke-virtual {v3}, LX/22p;->b()Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 361004
    iget-object v3, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-direct {p0, v4, v3}, Lcom/facebook/feed/data/FeedDataLoader;->b(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)Ljava/util/List;

    move-result-object v3

    .line 361005
    if-eq v4, v3, :cond_24

    .line 361006
    const/4 v2, 0x1

    goto/16 :goto_4

    .line 361007
    :cond_1a
    if-eqz v12, :cond_24

    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->ai:LX/22p;

    invoke-virtual {v3}, LX/22p;->b()Z

    move-result v3

    if-nez v3, :cond_24

    .line 361008
    iget-object v3, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v4, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)Ljava/util/List;

    move-result-object v3

    .line 361009
    if-eq v4, v3, :cond_24

    .line 361010
    const/4 v2, 0x1

    goto/16 :goto_4

    .line 361011
    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 361012
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 361013
    :cond_1d
    const/4 v7, 0x0

    goto/16 :goto_8

    .line 361014
    :cond_1e
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 361015
    :cond_1f
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    invoke-virtual {v2}, LX/22w;->finishedWithDataOnce()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-static {}, LX/22o;->b()Z

    move-result v2

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->ac:LX/22o;

    invoke-virtual {v2}, LX/22o;->d()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 361016
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v2, v3}, LX/0fz;->b(Ljava/util/List;)V

    goto/16 :goto_a

    .line 361017
    :cond_20
    if-eqz v4, :cond_21

    .line 361018
    invoke-direct {p0, v3}, Lcom/facebook/feed/data/FeedDataLoader;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 361019
    if-eq v3, v2, :cond_21

    move-object v3, v2

    .line 361020
    :cond_21
    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    goto/16 :goto_a

    .line 361021
    :cond_22
    sget-object v13, LX/18O;->Perform:LX/18O;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_b

    :cond_23
    move v8, v2

    goto/16 :goto_7

    :cond_24
    move-object v3, v4

    goto/16 :goto_4
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Ljava/lang/Throwable;LX/23E;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 360883
    invoke-static {p0, p2}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;LX/23E;)V

    .line 360884
    sget-object v0, LX/23E;->BEFORE:LX/23E;

    if-ne p2, v0, :cond_0

    const/4 v1, 0x1

    .line 360885
    :goto_0
    if-eqz v1, :cond_2

    .line 360886
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    sget-object v3, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    if-ne v0, v3, :cond_1

    .line 360887
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->b()V

    .line 360888
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v0, v5}, LX/0pT;->a(I)V

    .line 360889
    new-instance v0, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 360890
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v3, v0}, LX/0pT;->a(Landroid/util/Pair;)V

    .line 360891
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v0}, LX/0pT;->e()V

    .line 360892
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30r;

    iget-object v3, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v3, v5}, LX/30r;->a(Lcom/facebook/api/feedtype/FeedType;I)V

    .line 360893
    :goto_2
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    .line 360894
    sget-object v3, LX/1nY;->OUT_OF_MEMORY:LX/1nY;

    if-ne v0, v3, :cond_3

    .line 360895
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Intentional feed out of memory crash"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360896
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 360897
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->h()V

    goto :goto_1

    .line 360898
    :cond_2
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V

    goto :goto_2

    .line 360899
    :cond_3
    invoke-direct {p0, p1, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(Ljava/lang/Throwable;Z)Ljava/lang/String;

    move-result-object v4

    .line 360900
    sget-object v3, LX/1nY;->CANCELLED:LX/1nY;

    if-ne v0, v3, :cond_6

    sget-object v3, LX/18G;->CANCELLATION:LX/18G;

    .line 360901
    :goto_3
    instance-of v6, p1, LX/69I;

    if-eqz v6, :cond_7

    .line 360902
    invoke-static {p0, v1, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;ZLX/18G;)V

    .line 360903
    :cond_4
    :goto_4
    sget-object v6, LX/1nY;->CANCELLED:LX/1nY;

    if-eq v0, v6, :cond_5

    .line 360904
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 360905
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    invoke-virtual {v0, v6, v7}, LX/0r5;->a(J)V

    .line 360906
    invoke-virtual {p0, v6, v7}, Lcom/facebook/feed/data/FeedDataLoader;->a(J)Z

    .line 360907
    :cond_5
    invoke-static {p0, v1, v2, v3, v4}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;)V

    .line 360908
    sget-object v6, LX/0qw;->FULL:LX/0qw;

    sget-object v7, LX/18O;->Perform:LX/18O;

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V

    .line 360909
    return-void

    .line 360910
    :cond_6
    sget-object v3, LX/18G;->SERVICE_EXCEPTION:LX/18G;

    goto :goto_3

    .line 360911
    :cond_7
    instance-of v6, p1, LX/4Ua;

    if-eqz v6, :cond_4

    .line 360912
    check-cast p1, LX/4Ua;

    .line 360913
    iget-object v6, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v6, v6

    .line 360914
    if-eqz v6, :cond_4

    iget v6, v6, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v7, 0x198f03

    if-ne v6, v7, :cond_4

    .line 360915
    invoke-static {p0, v1, v3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;ZLX/18G;)V

    goto :goto_4
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;)V
    .locals 3
    .param p3    # LX/18G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 360844
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->c()Z

    move-result v0

    move v1, v0

    .line 360845
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ym;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Ym;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360846
    sget-object v0, LX/23F;->b:[I

    invoke-virtual {p3}, LX/18G;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 360847
    :cond_0
    :goto_1
    return-void

    .line 360848
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->v()Z

    move-result v0

    move v1, v0

    goto :goto_0

    .line 360849
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    invoke-virtual {v0, p2, p1}, LX/0pW;->a(Lcom/facebook/api/feed/FetchFeedResult;Z)V

    goto :goto_1

    .line 360850
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 360851
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0pW;->d:Z

    .line 360852
    iget-object v2, v0, LX/0pW;->a:LX/0Yi;

    const/4 p0, 0x0

    .line 360853
    iget-boolean p1, v2, LX/0Yi;->q:Z

    if-eqz p1, :cond_2

    if-nez v1, :cond_2

    .line 360854
    iput-boolean p0, v2, LX/0Yi;->q:Z

    .line 360855
    sget-object p2, LX/0Yi;->b:[LX/0Yj;

    array-length p3, p2

    move p1, p0

    :goto_2
    if-ge p1, p3, :cond_2

    aget-object p4, p2, p1

    .line 360856
    iget-object v0, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360857
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    .line 360858
    :cond_2
    iget-boolean p1, v2, LX/0Yi;->r:Z

    if-eqz p1, :cond_4

    .line 360859
    iput-boolean p0, v2, LX/0Yi;->r:Z

    .line 360860
    sget-object p2, LX/0Yi;->d:[LX/0Yj;

    array-length p3, p2

    move p1, p0

    :goto_3
    if-ge p1, p3, :cond_3

    aget-object p4, p2, p1

    .line 360861
    iget-object v0, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360862
    add-int/lit8 p1, p1, 0x1

    goto :goto_3

    .line 360863
    :cond_3
    iget-boolean p1, v2, LX/0Yi;->y:Z

    if-nez p1, :cond_4

    .line 360864
    iget-object p1, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance p2, LX/0Yj;

    const p3, 0xa0016

    const-string p4, "NNFColdStartTTI"

    invoke-direct {p2, p3, p4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360865
    :cond_4
    iget-object p1, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const p2, 0xa0038

    const-string p3, "NNFHotStartTTI"

    invoke-interface {p1, p2, p3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 360866
    sget-object p2, LX/0Yi;->g:[LX/0Yj;

    array-length p3, p2

    move p1, p0

    :goto_4
    if-ge p1, p3, :cond_5

    aget-object p4, p2, p1

    .line 360867
    iget-object v0, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360868
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    .line 360869
    :cond_5
    sget-object p2, LX/0Yi;->e:[LX/0Yj;

    array-length p3, p2

    move p1, p0

    :goto_5
    if-ge p1, p3, :cond_6

    aget-object p4, p2, p1

    .line 360870
    iget-object v0, v2, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 360871
    add-int/lit8 p1, p1, 0x1

    goto :goto_5

    .line 360872
    :cond_6
    sget-object p1, LX/0Yi;->f:[I

    array-length p2, p1

    :goto_6
    if-ge p0, p2, :cond_7

    aget p3, p1, p0

    .line 360873
    iget-object p4, v2, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {p4, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 360874
    add-int/lit8 p0, p0, 0x1

    goto :goto_6

    .line 360875
    :cond_7
    if-nez v1, :cond_8

    .line 360876
    iget-object p0, v2, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p1, 0x230013

    const/4 p2, 0x4

    invoke-interface {p0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 360877
    :cond_8
    const p0, 0xa0040

    const-string p1, "NNFPullToRefreshNetworkTime"

    invoke-static {v2, p0, p1}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;)V

    .line 360878
    iget-object p0, v2, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p1, 0xa0041

    const/4 p2, 0x3

    invoke-interface {p0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 360879
    const p0, 0xa0055

    const-string p1, "NNFPullToRefreshBeforeExecuteTime"

    invoke-static {v2, p0, p1}, LX/0Yi;->a(LX/0Yi;ILjava/lang/String;)V

    .line 360880
    invoke-static {v2}, LX/0Yi;->N(LX/0Yi;)V

    .line 360881
    goto/16 :goto_1

    .line 360882
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {v0, p4, v1, v2}, LX/0pW;->a(Ljava/lang/String;ZLcom/facebook/api/feedtype/FeedType;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V
    .locals 8
    .param p3    # LX/18G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 360839
    const-string v0, "FeedDataLoader.onLoadingComplete"

    const v1, 0x7e9156c0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 360840
    :try_start_0
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, LX/0qk;->a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360841
    const v0, -0x438b7b42

    invoke-static {v0}, LX/02m;->a(I)V

    .line 360842
    return-void

    .line 360843
    :catchall_0
    move-exception v0

    const v1, 0x369c6e2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResultCount;)V
    .locals 8

    .prologue
    .line 360810
    const-string v0, "FeedDataLoader.loadBeforeDataSuccess"

    const v1, 0x954076b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 360811
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->c(Lcom/facebook/feed/data/FeedDataLoader;Z)V

    .line 360812
    sget-object v0, LX/23E;->BEFORE:LX/23E;

    invoke-static {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;LX/23E;)V

    .line 360813
    invoke-static {p0, p1}, Lcom/facebook/feed/data/FeedDataLoader;->b$redex0(Lcom/facebook/feed/data/FeedDataLoader;Z)V

    .line 360814
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 360815
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->P:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0pP;->e:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 360816
    iget-object v2, p0, LX/0gD;->a:LX/0oy;

    invoke-virtual {v2}, LX/0oy;->h()I

    move-result v2

    .line 360817
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->Y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0rz;

    invoke-virtual {v3}, LX/0rz;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 360818
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->L:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/feed/data/FeedDataLoader$9;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/facebook/feed/data/FeedDataLoader$9;-><init>(Lcom/facebook/feed/data/FeedDataLoader;JI)V

    const v5, -0x17467ec0

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 360819
    :cond_0
    iput-wide v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->at:J

    .line 360820
    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(J)Z

    .line 360821
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    sget-object v3, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    if-eq v2, v3, :cond_1

    .line 360822
    const/4 v2, 0x1

    move v2, v2

    .line 360823
    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 360824
    iget v2, p2, Lcom/facebook/api/feed/FetchFeedResultCount;->a:I

    move v2, v2

    .line 360825
    if-nez v2, :cond_1

    .line 360826
    iget-object v2, p2, Lcom/facebook/api/feed/FetchFeedResultCount;->b:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v2, v2

    .line 360827
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    invoke-virtual {v3}, LX/0gf;->isManual()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 360828
    sget-object v3, LX/0rj;->NO_NETWORK_DATA_PTR_RERANKING_START:LX/0rj;

    invoke-virtual {p0, v3}, LX/0gD;->a(LX/0rj;)V

    .line 360829
    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    .line 360830
    iget v4, v2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v4, v4

    .line 360831
    sget-object v5, LX/0gf;->RERANK:LX/0gf;

    invoke-virtual {p0}, LX/0gD;->E()J

    move-result-wide v6

    invoke-static {v3, v4, v5, v6, v7}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;ILX/0gf;J)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 360832
    const-wide/16 v4, 0x0

    .line 360833
    iget-object v6, v2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v6

    .line 360834
    invoke-static {p0, v3, v4, v5, v2}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;JLX/0gf;)V

    .line 360835
    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v2}, LX/0r8;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360836
    :cond_1
    const v0, -0x5f9ad631

    invoke-static {v0}, LX/02m;->a(I)V

    .line 360837
    return-void

    .line 360838
    :catchall_0
    move-exception v0

    const v1, -0x6ede71b1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/feed/data/FeedDataLoader;
    .locals 48

    .prologue
    .line 360808
    new-instance v2, Lcom/facebook/feed/data/FeedDataLoader;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v6

    check-cast v6, LX/0Sg;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const-class v8, LX/0pS;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/0pS;

    const/16 v9, 0x76a

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x149f

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1416

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x5e2

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v14

    check-cast v14, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v15, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x786

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x676

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0pT;->a(LX/0QB;)LX/0pT;

    move-result-object v18

    check-cast v18, LX/0pT;

    invoke-static/range {p0 .. p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v19

    check-cast v19, LX/0pV;

    invoke-static/range {p0 .. p0}, LX/0pW;->a(LX/0QB;)LX/0pW;

    move-result-object v20

    check-cast v20, LX/0pW;

    const/16 v21, 0x60a

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/0pX;->a(LX/0QB;)LX/0pX;

    move-result-object v22

    check-cast v22, LX/0pX;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0pd;->a(LX/0QB;)LX/0pd;

    move-result-object v24

    check-cast v24, LX/0pd;

    const/16 v25, 0x1446

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/22i;->a(LX/0QB;)LX/22i;

    move-result-object v26

    check-cast v26, LX/22i;

    const/16 v27, 0x68b

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/22j;->a(LX/0QB;)LX/22j;

    move-result-object v28

    check-cast v28, LX/22j;

    invoke-static/range {p0 .. p0}, LX/22k;->a(LX/0QB;)LX/22k;

    move-result-object v29

    check-cast v29, LX/22k;

    const/16 v30, 0xb84

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/0pl;->a(LX/0QB;)LX/0pl;

    move-result-object v31

    check-cast v31, LX/0pl;

    invoke-static/range {p0 .. p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v32

    check-cast v32, LX/0qX;

    invoke-static/range {p0 .. p0}, LX/22o;->a(LX/0QB;)LX/22o;

    move-result-object v33

    check-cast v33, LX/22o;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v34

    check-cast v34, LX/0Tf;

    invoke-static/range {p0 .. p0}, LX/22p;->a(LX/0QB;)LX/22p;

    move-result-object v35

    check-cast v35, LX/22p;

    invoke-static/range {p0 .. p0}, LX/0qa;->a(LX/0QB;)LX/0qa;

    move-result-object v36

    check-cast v36, LX/0qa;

    const/16 v37, 0x1c97

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v37

    invoke-static/range {p0 .. p0}, LX/0qb;->a(LX/0QB;)LX/0qb;

    move-result-object v38

    check-cast v38, LX/0qb;

    invoke-static/range {p0 .. p0}, LX/0qf;->a(LX/0QB;)LX/0qf;

    move-result-object v39

    check-cast v39, LX/0qf;

    const-class v40, LX/22q;

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/22q;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v41

    check-cast v41, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v42

    check-cast v42, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/22r;->a(LX/0QB;)LX/22r;

    move-result-object v43

    check-cast v43, LX/22r;

    invoke-static/range {p0 .. p0}, LX/0qj;->a(LX/0QB;)LX/0qj;

    move-result-object v44

    check-cast v44, LX/0qj;

    const/16 v45, 0x1483

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v45

    const/16 v46, 0x698

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v47, 0x15a5

    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v47

    invoke-direct/range {v2 .. v47}, Lcom/facebook/feed/data/FeedDataLoader;-><init>(Landroid/content/Context;LX/0aG;LX/0Sh;LX/0Sg;LX/03V;LX/0pS;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0pT;LX/0pV;LX/0pW;LX/0Ot;LX/0pX;LX/0ad;LX/0pd;LX/0Or;LX/22i;LX/0Ot;LX/22j;LX/22k;LX/0Ot;LX/0pl;LX/0qX;LX/22o;LX/0Tf;LX/22p;LX/0qa;LX/0Ot;LX/0qb;LX/0qf;LX/22q;LX/0Zb;LX/0SG;LX/22r;LX/0qj;LX/0Or;LX/0Ot;LX/0Or;)V

    .line 360809
    return-object v2
.end method

.method private b(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 360762
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-nez v0, :cond_1

    .line 360763
    :cond_0
    :goto_0
    return-object p1

    .line 360764
    :cond_1
    iput-boolean v4, p0, Lcom/facebook/feed/data/FeedDataLoader;->z:Z

    .line 360765
    const-string v2, "synthetic_cursor"

    .line 360766
    iget-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-nez v0, :cond_2

    .line 360767
    iget-object v0, p2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 360768
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 360769
    new-instance v1, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;-><init>()V

    .line 360770
    const-string v0, "findpages_feed_unit"

    .line 360771
    :goto_1
    new-instance v3, LX/1u8;

    invoke-direct {v3}, LX/1u8;-><init>()V

    .line 360772
    iput-object v1, v3, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 360773
    move-object v1, v3

    .line 360774
    iput-object v0, v1, LX/1u8;->d:Ljava/lang/String;

    .line 360775
    move-object v0, v1

    .line 360776
    sget-object v1, LX/0ql;->b:Ljava/lang/String;

    .line 360777
    iput-object v1, v0, LX/1u8;->i:Ljava/lang/String;

    .line 360778
    move-object v0, v0

    .line 360779
    iput-object v2, v0, LX/1u8;->c:Ljava/lang/String;

    .line 360780
    move-object v0, v0

    .line 360781
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    goto :goto_0

    .line 360782
    :cond_2
    iget-object v0, p2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v0

    .line 360783
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 360784
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p2, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360785
    const/4 v0, 0x1

    move v0, v0

    .line 360786
    if-eqz v0, :cond_3

    .line 360787
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;-><init>()V

    .line 360788
    const-string v0, "nux_good_friends_feed_item_unit"

    goto :goto_1

    .line 360789
    :cond_3
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNoContentGoodFriendsFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNoContentGoodFriendsFeedUnit;-><init>()V

    .line 360790
    const-string v0, "nocontent_good_friends_feed_unit"

    goto :goto_1

    .line 360791
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-nez v0, :cond_5

    .line 360792
    iget-object v0, p0, LX/0gD;->q:LX/0Uh;

    move-object v0, v0

    .line 360793
    const/16 v1, 0x399

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 360794
    new-instance v1, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;-><init>()V

    .line 360795
    const-string v0, "findfriends_feed_unit"

    goto :goto_1

    .line 360796
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-eqz v0, :cond_6

    .line 360797
    new-instance v0, LX/4WP;

    invoke-direct {v0}, LX/4WP;-><init>()V

    .line 360798
    iput-boolean v4, v0, LX/4WP;->c:Z

    .line 360799
    move-object v0, v0

    .line 360800
    invoke-virtual {v0}, LX/4WP;->a()Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;

    move-result-object v1

    .line 360801
    const-string v0, "findgroups_feed_unit"

    goto :goto_1

    .line 360802
    :cond_6
    new-instance v1, Lcom/facebook/graphql/model/GraphQLNoContentFeedUnit;

    invoke-direct {v1}, Lcom/facebook/graphql/model/GraphQLNoContentFeedUnit;-><init>()V

    .line 360803
    const-string v0, "nocontent_feed_unit"

    goto :goto_1
.end method

.method private b(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360282
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aa:LX/0qX;

    const/4 v1, 0x2

    .line 360283
    sget-wide v5, LX/0X5;->cl:J

    invoke-static {v0, v5, v6, v1}, LX/0qX;->a(LX/0qX;JI)I

    move-result v5

    move v0, v5

    .line 360284
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 360285
    if-gtz v0, :cond_0

    .line 360286
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 360287
    :goto_0
    move-object v0, v1

    .line 360288
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->I:LX/0Sg;

    const-string v2, "NewsFeed Staged Feed Image Fetch"

    new-instance v3, Lcom/facebook/feed/data/FeedDataLoader$7;

    invoke-direct {v3, p0, v0}, Lcom/facebook/feed/data/FeedDataLoader$7;-><init>(Lcom/facebook/feed/data/FeedDataLoader;LX/0Px;)V

    sget-object v0, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 360289
    return-void

    .line 360290
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 360291
    invoke-virtual {v1, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 360292
    :cond_1
    const/4 v2, 0x0

    invoke-interface {p1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0
.end method

.method private b(LX/0rS;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 360400
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v1}, LX/22t;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360401
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 360402
    iget-object p0, v1, LX/0r8;->a:LX/0r9;

    sget-object p1, LX/0r9;->LOADING_FOR_UI:LX/0r9;

    if-eq p0, p1, :cond_3

    const/4 p0, 0x1

    :goto_1
    move v1, p0

    .line 360403
    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/feed/data/FeedDataLoader;Z)V
    .locals 3

    .prologue
    .line 360393
    if-eqz p1, :cond_0

    .line 360394
    new-instance v1, LX/30y;

    invoke-direct {v1, p0}, LX/30y;-><init>(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360395
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 360396
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->S:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lu;

    iget-object v2, p0, LX/0gD;->b:LX/0fz;

    .line 360397
    iget-object p0, v2, LX/0fz;->d:LX/0qm;

    move-object v2, p0

    .line 360398
    invoke-virtual {v0, v1, v2}, LX/1lu;->a(LX/1gl;LX/0qm;)V

    .line 360399
    :cond_0
    return-void
.end method

.method private c(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360368
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    .line 360369
    iget-boolean v1, v0, LX/0qy;->b:Z

    move v0, v1

    .line 360370
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    .line 360371
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 360372
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360373
    :cond_0
    :goto_0
    return-object p1

    .line 360374
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-eqz v0, :cond_2

    new-instance v0, LX/4WP;

    invoke-direct {v0}, LX/4WP;-><init>()V

    const/4 v1, 0x0

    .line 360375
    iput-boolean v1, v0, LX/4WP;->c:Z

    .line 360376
    move-object v0, v0

    .line 360377
    invoke-virtual {v0}, LX/4WP;->a()Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;

    move-result-object v0

    :goto_1
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 360378
    iget-boolean v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->O:Z

    if-eqz v1, :cond_3

    const-string v1, "findgroups_feed_unit"

    .line 360379
    :goto_2
    const-string v2, "synthetic_cursor"

    .line 360380
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    new-instance v4, LX/1u8;

    invoke-direct {v4}, LX/1u8;-><init>()V

    .line 360381
    iput-object v0, v4, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 360382
    move-object v0, v4

    .line 360383
    iput-object v1, v0, LX/1u8;->d:Ljava/lang/String;

    .line 360384
    move-object v0, v0

    .line 360385
    sget-object v1, LX/0ql;->c:Ljava/lang/String;

    .line 360386
    iput-object v1, v0, LX/1u8;->i:Ljava/lang/String;

    .line 360387
    move-object v0, v0

    .line 360388
    iput-object v2, v0, LX/1u8;->c:Ljava/lang/String;

    .line 360389
    move-object v0, v0

    .line 360390
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto :goto_0

    .line 360391
    :cond_2
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;-><init>()V

    goto :goto_1

    .line 360392
    :cond_3
    const-string v1, "findfriends_feed_unit"

    goto :goto_2
.end method

.method public static c(Lcom/facebook/feed/data/FeedDataLoader;Z)V
    .locals 1

    .prologue
    .line 360358
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v0}, LX/0qy;->d()V

    .line 360359
    if-eqz p1, :cond_2

    .line 360360
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->b()V

    .line 360361
    :goto_0
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    .line 360362
    iget-object p1, v0, LX/0r0;->a:LX/0r1;

    sget-object p0, LX/0r1;->INITIAL_TAIL_LOAD_COMPLETE:LX/0r1;

    if-ne p1, p0, :cond_0

    .line 360363
    sget-object p1, LX/0r1;->INITIALIZED:LX/0r1;

    iput-object p1, v0, LX/0r0;->a:LX/0r1;

    .line 360364
    :cond_0
    iget-object p1, v0, LX/0r0;->a:LX/0r1;

    sget-object p0, LX/0r1;->INITIALIZED:LX/0r1;

    if-eq p1, p0, :cond_1

    .line 360365
    sget-object p1, LX/0r1;->INITIAL_HEAD_LOAD_COMPLETE:LX/0r1;

    iput-object p1, v0, LX/0r0;->a:LX/0r1;

    .line 360366
    :cond_1
    return-void

    .line 360367
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->g()V

    goto :goto_0
.end method

.method private c(LX/0gf;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 360356
    iget-object v1, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->v()I

    move-result v1

    if-nez v1, :cond_1

    .line 360357
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    sget-object v2, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d()I
    .locals 5

    .prologue
    .line 360351
    iget-object v0, p0, LX/0gD;->a:LX/0oy;

    .line 360352
    iget v1, v0, LX/0oy;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 360353
    iget-object v1, v0, LX/0oy;->a:LX/0W3;

    sget-wide v3, LX/0X5;->gn:J

    const/16 v2, 0xa

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JI)I

    move-result v1

    iput v1, v0, LX/0oy;->c:I

    .line 360354
    :cond_0
    iget v1, v0, LX/0oy;->c:I

    move v0, v1

    .line 360355
    return v0
.end method

.method private d(LX/0gf;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 360349
    iget-object v1, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->v()I

    move-result v1

    if-nez v1, :cond_1

    .line 360350
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    if-eq p1, v1, :cond_2

    invoke-virtual {p1}, LX/0gf;->isAutoRefresh()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e()I
    .locals 5

    .prologue
    .line 360344
    iget-object v0, p0, LX/0gD;->a:LX/0oy;

    .line 360345
    iget v1, v0, LX/0oy;->s:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 360346
    iget-object v1, v0, LX/0oy;->a:LX/0W3;

    sget-wide v3, LX/0X5;->gJ:J

    const/16 v2, 0xa

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JI)I

    move-result v1

    iput v1, v0, LX/0oy;->s:I

    .line 360347
    :cond_0
    iget v1, v0, LX/0oy;->s:I

    move v0, v1

    .line 360348
    return v0
.end method


# virtual methods
.method public final A()I
    .locals 3

    .prologue
    .line 360334
    iget-object v0, p0, LX/0gD;->a:LX/0oy;

    .line 360335
    iget-boolean v1, v0, LX/0oy;->t:Z

    if-eqz v1, :cond_1

    .line 360336
    :try_start_0
    iget-object v1, v0, LX/0oy;->u:Lorg/json/JSONObject;

    if-nez v1, :cond_0

    .line 360337
    sget-wide v1, LX/0X5;->gx:J

    invoke-static {v0, v1, v2}, LX/0oy;->a(LX/0oy;J)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, v0, LX/0oy;->u:Lorg/json/JSONObject;

    .line 360338
    :cond_0
    iget-object v1, v0, LX/0oy;->u:Lorg/json/JSONObject;

    invoke-static {v0, v1}, LX/0oy;->a(LX/0oy;Lorg/json/JSONObject;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 360339
    :goto_0
    move v0, v1

    .line 360340
    return v0

    .line 360341
    :catch_0
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0oy;->t:Z

    .line 360342
    invoke-static {v0}, LX/0oy;->v(LX/0oy;)I

    move-result v1

    goto :goto_0

    .line 360343
    :cond_1
    invoke-static {v0}, LX/0oy;->v(LX/0oy;)I

    move-result v1

    goto :goto_0
.end method

.method public final B()LX/1EM;
    .locals 1

    .prologue
    .line 360333
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ax:LX/1EM;

    return-object v0
.end method

.method public final C()V
    .locals 0

    .prologue
    .line 360332
    return-void
.end method

.method public final F()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 360331
    sget-object v0, Lcom/facebook/feed/data/FeedDataLoader;->D:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final G()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 360313
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->W()V

    .line 360314
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    invoke-virtual {v0}, LX/0r5;->b()V

    .line 360315
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    if-eqz v0, :cond_0

    .line 360316
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 360317
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    .line 360318
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    if-eqz v0, :cond_1

    .line 360319
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 360320
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->q:LX/22z;

    .line 360321
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->r:LX/1Mv;

    if-eqz v0, :cond_2

    .line 360322
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->r:LX/1Mv;

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 360323
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->r:LX/1Mv;

    .line 360324
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    if-eqz v0, :cond_3

    .line 360325
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    invoke-virtual {v0}, LX/22z;->c()V

    .line 360326
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    .line 360327
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    if-eqz v0, :cond_4

    .line 360328
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    invoke-interface {v0, v2}, LX/0YG;->cancel(Z)Z

    .line 360329
    iput-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->A:LX/0YG;

    .line 360330
    :cond_4
    return-void
.end method

.method public final a(LX/0rS;LX/0gf;Z)LX/0uO;
    .locals 6

    .prologue
    .line 360295
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    .line 360296
    iget-boolean v2, v0, LX/0qy;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/0qy;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/0qy;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 360297
    invoke-virtual {v0}, LX/0qy;->d()V

    .line 360298
    :cond_0
    iget-boolean v2, v0, LX/0qy;->b:Z

    move v0, v2

    .line 360299
    if-eqz v0, :cond_1

    .line 360300
    sget-object v0, LX/0uO;->END_OF_FEED:LX/0uO;

    .line 360301
    :goto_0
    return-object v0

    .line 360302
    :cond_1
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ah:LX/22j;

    .line 360303
    iget-object v1, v0, LX/22j;->a:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, v0, LX/22j;->c:Z

    :goto_1
    move v0, v1

    .line 360304
    if-eqz v0, :cond_2

    .line 360305
    sget-object v0, LX/0uO;->END_OF_CACHED_FEED:LX/0uO;

    goto :goto_0

    .line 360306
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/feed/data/FeedDataLoader;->b(LX/0rS;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 360307
    sget-object v0, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;

    goto :goto_0

    .line 360308
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->N()I

    move-result v0

    .line 360309
    invoke-virtual {p2}, LX/0gf;->isInitialization()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 360310
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->e()I

    move-result v0

    .line 360311
    :cond_4
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/facebook/feed/data/FeedDataLoader;->a(ILX/0rS;LX/0gf;Z)V

    .line 360312
    sget-object v0, LX/0uO;->SUCCESS:LX/0uO;

    goto :goto_0

    :cond_5
    iget-boolean v1, v0, LX/22j;->b:Z

    goto :goto_1
.end method

.method public final a(LX/1EM;)V
    .locals 0

    .prologue
    .line 360293
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader;->ax:LX/1EM;

    .line 360294
    return-void
.end method

.method public final a(LX/22w;)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 360280
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    .line 360281
    return-void
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    .line 360492
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360493
    const/4 v0, 0x0

    .line 360494
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    invoke-virtual {p0}, LX/0gD;->L()Z

    move-result v3

    invoke-virtual {p0}, LX/0gD;->M()J

    move-result-wide v4

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, LX/0r5;->b(JZJ)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/0gf;)Z
    .locals 12

    .prologue
    const/4 v4, 0x1

    .line 360404
    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360405
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    .line 360406
    iget-object v1, v0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v1}, LX/0qx;->a()V

    .line 360407
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ah:LX/22j;

    const/4 v1, 0x0

    .line 360408
    iput-boolean v1, v0, LX/22j;->b:Z

    .line 360409
    iput-boolean v1, v0, LX/22j;->c:Z

    .line 360410
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v0}, LX/0qy;->d()V

    .line 360411
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v1

    .line 360412
    iput-boolean v1, v0, LX/0pT;->a:Z

    .line 360413
    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 360414
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->R:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    invoke-virtual {v0}, LX/0pT;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360415
    sget-object p1, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    .line 360416
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    .line 360417
    iget-wide v10, v0, LX/0pT;->b:J

    iput-wide v10, v0, LX/0pT;->e:J

    .line 360418
    iget-object v10, v0, LX/0pT;->m:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    iput-wide v10, v0, LX/0pT;->b:J

    .line 360419
    :cond_2
    move-object v7, p1

    .line 360420
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ak:LX/0pT;

    .line 360421
    iget-wide v10, v0, LX/0pT;->c:J

    iput-wide v10, v0, LX/0pT;->f:J

    .line 360422
    iget-object v10, v0, LX/0pT;->m:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    iput-wide v10, v0, LX/0pT;->c:J

    .line 360423
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->f()Z

    move-result v0

    if-nez v0, :cond_6

    move v8, v4

    .line 360424
    :goto_0
    if-eqz v8, :cond_8

    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-eq v7, v0, :cond_8

    .line 360425
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0, v7}, LX/0qk;->a(LX/0gf;)V

    .line 360426
    iput-object v7, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    .line 360427
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Z()Z

    move-result v9

    .line 360428
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->j()Ljava/lang/String;

    move-result-object v2

    .line 360429
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->d()I

    move-result v3

    .line 360430
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aq:LX/0qb;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->ar:LX/0qf;

    sget-object v5, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v0, v1, v5}, LX/0qb;->a(LX/0qg;LX/0rH;)Z

    move-result v5

    .line 360431
    iget-object v0, p0, LX/0gD;->e:LX/0qz;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    sget-object v6, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual/range {v0 .. v7}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 360432
    sget-object v1, LX/0rj;->HEAD_FETCH:LX/0rj;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Freshness:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Manual: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 360433
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-direct {p0, v0, v1, v9, v7}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;ZLX/0gf;)LX/230;

    move-result-object v0

    .line 360434
    invoke-virtual {v7}, LX/0gf;->isAutoRefresh()Z

    move-result v1

    if-nez v1, :cond_7

    .line 360435
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v1}, LX/0r8;->c()V

    .line 360436
    :goto_1
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 360437
    iput-object v0, v1, LX/0r8;->d:LX/230;

    .line 360438
    :goto_2
    if-nez v8, :cond_4

    .line 360439
    sget-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    if-ne v7, v0, :cond_3

    .line 360440
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0, v7}, LX/0qk;->a(LX/0gf;)V

    .line 360441
    :cond_3
    invoke-virtual {v7}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 360442
    iput-object v7, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    .line 360443
    :cond_4
    invoke-virtual {v7}, LX/0gf;->needsReranking()Z

    move-result v0

    if-nez v0, :cond_5

    .line 360444
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {v7}, LX/0gf;->needsEmptyReranking()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 360445
    if-eqz v0, :cond_a

    .line 360446
    :cond_5
    const/4 v0, 0x1

    move v0, v0

    .line 360447
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->s:LX/22z;

    if-nez v0, :cond_a

    .line 360448
    sget-object v0, LX/0rj;->PTR_RERANKING_SCHEDULED:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    .line 360449
    sget-object v0, LX/22v;->SCHEDULED:LX/22v;

    .line 360450
    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    .line 360451
    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-ne v7, v0, :cond_9

    const-wide/16 v0, 0x0

    :goto_4
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->d()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, v7}, Lcom/facebook/feed/data/FeedDataLoader;->a(JILX/0gf;)V

    .line 360452
    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-ne v7, v0, :cond_a

    .line 360453
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->b()V

    .line 360454
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->c()V

    .line 360455
    iput-object v7, p0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    .line 360456
    :goto_5
    return v4

    .line 360457
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 360458
    :cond_7
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v1}, LX/0r8;->d()V

    goto :goto_1

    .line 360459
    :cond_8
    sget-object v0, LX/0rj;->HEAD_FETCH_ALREADY_HAPPENING:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    goto :goto_2

    .line 360460
    :cond_9
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aa:LX/0qX;

    const/4 v1, 0x5

    .line 360461
    sget-wide v10, LX/0X5;->ci:J

    invoke-static {v0, v10, v11, v1}, LX/0qX;->a(LX/0qX;JI)I

    move-result v10

    move v0, v10

    .line 360462
    int-to-long v0, v0

    goto :goto_4

    :cond_a
    move v4, v8

    .line 360463
    goto :goto_5

    :cond_b
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 360545
    if-nez p1, :cond_0

    .line 360546
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->O()Z

    move-result v0

    .line 360547
    :goto_0
    return v0

    .line 360548
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->Q()V

    .line 360549
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 360550
    :cond_1
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->P(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360551
    const/4 v0, 0x0

    goto :goto_0

    .line 360552
    :cond_2
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->b()V

    .line 360553
    iget v0, p1, Lcom/facebook/feed/loader/FeedDataLoaderInitializationParams;->a:I

    move v0, v0

    .line 360554
    if-lez v0, :cond_3

    .line 360555
    :goto_1
    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    sget-object v3, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(ILX/0rS;LX/0gf;Z)V

    move v0, v1

    .line 360556
    goto :goto_0

    .line 360557
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->e()I

    move-result v0

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 360539
    sget-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    .line 360540
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360541
    sget-object v0, LX/0rj;->COLD_START_RERANKING_START:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    .line 360542
    sget-object v0, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    .line 360543
    :cond_0
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->S()Z

    move-result v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0rS;LX/0gf;Z)LX/0uO;

    .line 360544
    return-void
.end method

.method public final b(LX/0rS;LX/0gf;Z)V
    .locals 12

    .prologue
    const/4 v5, 0x1

    .line 360531
    invoke-direct {p0, p1}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0rS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360532
    iget-object v1, p0, LX/0gD;->e:LX/0qz;

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->N()I

    move-result v4

    invoke-virtual {p0}, LX/0gD;->E()J

    move-result-wide v10

    move-object v6, p1

    move-object v7, p2

    move v8, p3

    move v9, v5

    invoke-virtual/range {v1 .. v11}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;ZZJ)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 360533
    invoke-static {p0}, Lcom/facebook/feed/data/FeedDataLoader;->T(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 360534
    invoke-direct {p0, v0, p3}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/api/feed/FetchFeedParams;Z)V

    .line 360535
    sget-object v0, LX/0rj;->TAIL_FETCH_SKIP_GAP_SCHEDULED:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    .line 360536
    sget-object v0, LX/22w;->SCHEDULED:LX/22w;

    .line 360537
    iput-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    .line 360538
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 8

    .prologue
    .line 360516
    invoke-super {p0}, LX/0gD;->g()V

    .line 360517
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->G()V

    .line 360518
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23G;

    const/4 v4, -0x1

    .line 360519
    iget-object v1, v0, LX/23G;->h:LX/23H;

    invoke-virtual {v1}, LX/23H;->b()Ljava/util/Iterator;

    move-result-object v7

    .line 360520
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360521
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/82V;

    .line 360522
    iget-object v2, v1, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/HasGapRule;

    move-object v2, v2

    .line 360523
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 360524
    const-string v3, "delayed_move_failure"

    invoke-static {v0, v2, v3, v4}, LX/23G;->a(LX/23G;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V

    .line 360525
    iget-object v2, v1, LX/82V;->b:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v2, v2

    .line 360526
    iget v3, v1, LX/82V;->c:I

    move v3, v3

    .line 360527
    const/4 v5, 0x1

    const-string v6, "session_ended"

    move-object v1, v0

    invoke-static/range {v1 .. v6}, LX/23G;->a(LX/23G;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IIZLjava/lang/String;)V

    goto :goto_0

    .line 360528
    :cond_0
    iget-object v1, v0, LX/23G;->h:LX/23H;

    .line 360529
    iget-object v2, v1, LX/23H;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 360530
    return-void
.end method

.method public final i()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 360511
    invoke-virtual {p0}, LX/0gD;->I()Z

    move-result v1

    if-nez v1, :cond_1

    .line 360512
    :cond_0
    :goto_0
    return v0

    .line 360513
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->y:Z

    if-nez v1, :cond_0

    .line 360514
    iget-object v1, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v1}, LX/0r0;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360515
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->p:LX/22z;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 360510
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(J)Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 360507
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360508
    :goto_0
    return-void

    .line 360509
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    invoke-virtual {v0}, LX/0r5;->a()V

    goto :goto_0
.end method

.method public final l()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 360495
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-short v1, LX/0fe;->b:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360496
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->j()Z

    move-result v0

    .line 360497
    :goto_0
    return v0

    .line 360498
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->i()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    .line 360499
    goto :goto_0

    .line 360500
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->H:LX/0r5;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    invoke-virtual {p0}, LX/0gD;->L()Z

    move-result v3

    invoke-virtual {p0}, LX/0gD;->M()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/0r5;->a(JZJ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 360501
    iget-object v0, p0, LX/0gD;->d:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/FeedDataLoader;->E:Ljava/lang/String;

    const-string v2, "classic warm auto refresh scheduled"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360502
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->j()Z

    move v0, v6

    .line 360503
    goto :goto_0

    .line 360504
    :cond_2
    iget-object v0, p0, LX/0gD;->d:LX/0pV;

    sget-object v1, Lcom/facebook/feed/data/FeedDataLoader;->E:Ljava/lang/String;

    const-string v2, "classic warm auto refresh immediately"

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360505
    sget-object v0, LX/0gf;->WARM_START:LX/0gf;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    .line 360506
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 360464
    sget-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    .line 360465
    return-void
.end method

.method public final n()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 360485
    iget-boolean v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->w:Z

    if-eqz v1, :cond_0

    .line 360486
    const/4 v0, 0x0

    .line 360487
    :goto_0
    return v0

    .line 360488
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->S()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v1}, LX/0pl;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360489
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/data/FeedDataLoader;->O()Z

    move-result v0

    goto :goto_0

    .line 360490
    :cond_2
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-virtual {p0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    .line 360491
    iput-boolean v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->w:Z

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 360479
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360480
    const/4 v0, 0x0

    .line 360481
    :goto_0
    return v0

    .line 360482
    :cond_0
    sget-object v0, LX/0rj;->WARM_START_RERANKING_START:LX/0rj;

    invoke-virtual {p0, v0}, LX/0gD;->a(LX/0rj;)V

    .line 360483
    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0gf;)Z

    .line 360484
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 360477
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->j()Z

    .line 360478
    return-void
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 360474
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->G()V

    .line 360475
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->az:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18E;

    invoke-virtual {v0}, LX/18E;->a()V

    .line 360476
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 360473
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->f()Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 360472
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->c()Z

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 360471
    invoke-virtual {p0}, Lcom/facebook/feed/data/FeedDataLoader;->v()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader;->v:LX/22t;

    invoke-virtual {v0}, LX/22t;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 360466
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    .line 360467
    iget-boolean v1, v0, LX/0qy;->b:Z

    move v0, v1

    .line 360468
    if-eqz v0, :cond_0

    .line 360469
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v0}, LX/0qy;->d()V

    .line 360470
    :cond_0
    return-void
.end method
