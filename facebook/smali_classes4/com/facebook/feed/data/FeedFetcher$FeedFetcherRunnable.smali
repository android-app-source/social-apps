.class public final Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/22k;

.field public final b:LX/230;


# direct methods
.method public constructor <init>(LX/22k;LX/230;)V
    .locals 0

    .prologue
    .line 361535
    iput-object p1, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361536
    iput-object p2, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361537
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 361495
    const-string v0, "FeedFetcher.run"

    const v1, 0x7bb61142

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361496
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361497
    iget-object v1, v0, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v1

    .line 361498
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361499
    sget-object v1, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne v0, v1, :cond_0

    .line 361500
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v0, v0, LX/22k;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pl;

    .line 361501
    iget-object v1, v0, LX/0pl;->k:LX/0qW;

    invoke-virtual {v1}, LX/0qW;->a()I

    .line 361502
    :cond_0
    const-string v0, "FeedFetcher.run.getQueryExecutorFetcher"

    const v1, 0x763b86ce

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 361503
    :try_start_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v0, v0, LX/22k;->g:LX/22m;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361504
    new-instance v4, LX/231;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    const/16 v5, 0x670

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v5, 0x5f2

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 361505
    new-instance v12, LX/233;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v11

    check-cast v11, LX/0kb;

    invoke-direct {v12, v5, v10, v11}, LX/233;-><init>(LX/0So;LX/0Zb;LX/0kb;)V

    .line 361506
    move-object v10, v12

    .line 361507
    check-cast v10, LX/233;

    const/16 v5, 0x149a

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0ph;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tn;->a(LX/0QB;)LX/0tn;

    move-result-object v13

    check-cast v13, LX/0tn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    move-object v5, v1

    invoke-direct/range {v4 .. v14}, LX/231;-><init>(LX/230;LX/0tX;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/233;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/0tn;LX/0ad;)V

    .line 361508
    move-object v0, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361509
    const v1, 0xe8811e1

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V

    .line 361510
    iget-object v1, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v1, v1, LX/22k;->c:LX/22l;

    iget-object v2, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361511
    iget-object v3, v2, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v2, v3

    .line 361512
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/22l;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;LX/232;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 361513
    iget-object v1, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361514
    iget-object v2, v1, LX/230;->b:LX/0rS;

    move-object v1, v2

    .line 361515
    sget-object v2, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eq v1, v2, :cond_1

    if-nez v0, :cond_1

    .line 361516
    const v0, 0x52153355

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361517
    :goto_0
    return-void

    .line 361518
    :catchall_0
    move-exception v0

    const v1, 0xf38f60f

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 361519
    :catch_0
    move-exception v0

    .line 361520
    :try_start_4
    iget-object v1, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v1, v1, LX/22k;->d:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable$2;-><init>(Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;Ljava/lang/Exception;)V

    const v0, 0x207c2b5

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 361521
    const v0, 0x2875accd

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 361522
    :cond_1
    if-nez v0, :cond_3

    .line 361523
    :try_start_5
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361524
    iget-object v1, v0, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v1

    .line 361525
    invoke-static {v0}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    move-object v1, v0

    .line 361526
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v0, v0, LX/22k;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pm;

    invoke-virtual {v0, v1}, LX/0pm;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 361527
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->b:LX/230;

    .line 361528
    iget-object v2, v0, LX/230;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v2

    .line 361529
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v2

    .line 361530
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    .line 361531
    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v2, :cond_2

    .line 361532
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v0, v0, LX/22k;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/187;

    invoke-virtual {v0, v1}, LX/187;->c(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 361533
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;->a:LX/22k;

    iget-object v0, v0, LX/22k;->d:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable$1;-><init>(Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;Lcom/facebook/api/feed/FetchFeedResult;)V

    const v1, 0x7d43efd7

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 361534
    const v0, -0x19efa2cd

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_1
    move-exception v0

    const v1, 0x641e2e96

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method
