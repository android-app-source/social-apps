.class public Lcom/facebook/feed/collage/ui/CollageAttachmentView;
.super Landroid/widget/FrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/26N;",
        ">",
        "Landroid/widget/FrameLayout;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/AmY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/26C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/Rect;

.field public j:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

.field public l:I

.field private final m:LX/1nq;

.field private final n:Landroid/graphics/Paint;

.field private o:Z

.field public p:LX/26O;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/26O",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/26Q;

.field public r:[Ljava/lang/String;

.field private s:LX/3rW;

.field private t:LX/Aj5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<TT;>.CollageGesture",
            "Listener;"
        }
    .end annotation
.end field

.field public u:LX/Aj7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Aj7",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Z

.field public x:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 371487
    const-class v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 371488
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 371489
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->m:LX/1nq;

    .line 371490
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->n:Landroid/graphics/Paint;

    .line 371491
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 371492
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->w:Z

    .line 371493
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->x:I

    .line 371494
    invoke-direct {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d()V

    .line 371495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 371496
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 371497
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->m:LX/1nq;

    .line 371498
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->n:Landroid/graphics/Paint;

    .line 371499
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 371500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->w:Z

    .line 371501
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->x:I

    .line 371502
    invoke-direct {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d()V

    .line 371503
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 371504
    const v0, 0x7f020271

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/Aj6;LX/1aZ;Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 371505
    invoke-virtual {p0}, LX/Aj6;->l()V

    .line 371506
    invoke-virtual {p0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p2}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 371507
    invoke-virtual {p0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 371508
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Aj6;->b:Z

    .line 371509
    return-void
.end method

.method private a(LX/Aj7;I)V
    .locals 2
    .param p1    # LX/Aj7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 371510
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    new-instance v1, LX/Aj4;

    invoke-direct {v1, p0, p1, p2}, LX/Aj4;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/Aj7;I)V

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371511
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 371512
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 371513
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->m:LX/1nq;

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081a3c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->a(I)LX/1nq;

    move-result-object v0

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    .line 371514
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 371515
    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 371516
    return-void
.end method

.method private static a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;Landroid/content/res/Resources;LX/1Uo;LX/03V;LX/0yc;LX/AmY;LX/26C;)V
    .locals 0

    .prologue
    .line 371517
    iput-object p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b:LX/1Uo;

    iput-object p3, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c:LX/03V;

    iput-object p4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    iput-object p5, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->e:LX/AmY;

    iput-object p6, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->f:LX/26C;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {v6}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v2

    check-cast v2, LX/1Uo;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v6}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    invoke-static {v6}, LX/AmY;->a(LX/0QB;)LX/AmY;

    move-result-object v5

    check-cast v5, LX/AmY;

    invoke-static {v6}, LX/26C;->a(LX/0QB;)LX/26C;

    move-result-object v6

    check-cast v6, LX/26C;

    invoke-static/range {v0 .. v6}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;Landroid/content/res/Resources;LX/1Uo;LX/03V;LX/0yc;LX/AmY;LX/26C;)V

    return-void
.end method

.method public static a(LX/0yc;)Z
    .locals 1

    .prologue
    .line 371518
    invoke-virtual {p0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0yc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0yc;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 371425
    const-class v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 371426
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->h:Landroid/graphics/drawable/Drawable;

    .line 371427
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    .line 371428
    new-instance v0, LX/Aj5;

    invoke-direct {v0, p0}, LX/Aj5;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->t:LX/Aj5;

    .line 371429
    new-instance v0, LX/3rW;

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->t:LX/Aj5;

    invoke-direct {v0, v1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->s:LX/3rW;

    .line 371430
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    .line 371431
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 371432
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b:LX/1Uo;

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 371433
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g(I)V

    .line 371434
    iput-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    .line 371435
    iput-boolean v3, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->o:Z

    .line 371436
    iput-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    .line 371437
    iput-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->r:[Ljava/lang/String;

    .line 371438
    invoke-virtual {p0, v3}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->setWillNotDraw(Z)V

    .line 371439
    invoke-direct {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->e()V

    .line 371440
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 371519
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->m:LX/1nq;

    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->c(I)LX/1nq;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b1083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->b(I)LX/1nq;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 371520
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->n:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a05d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 371521
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 371526
    iget-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->o:Z

    if-nez v0, :cond_0

    .line 371527
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 371528
    const/16 v1, 0x53

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 371529
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 371530
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->o:Z

    .line 371531
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 371522
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    if-eqz v0, :cond_0

    .line 371523
    :goto_0
    return-void

    .line 371524
    :cond_0
    new-instance v0, Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    .line 371525
    invoke-direct {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->f()V

    goto :goto_0
.end method

.method private g(I)V
    .locals 3

    .prologue
    .line 371592
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 371593
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b:LX/1Uo;

    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a045d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 371594
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 371595
    move-object v0, v0

    .line 371596
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 371597
    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 371598
    new-instance v1, LX/Aj6;

    invoke-direct {v1, v0}, LX/Aj6;-><init>(LX/1af;)V

    .line 371599
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    .line 371600
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->a(LX/1aX;)V

    goto :goto_0

    .line 371601
    :cond_0
    return-void
.end method

.method private h(I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 371580
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .line 371581
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 371582
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 371583
    iget-object v3, v0, LX/Aj6;->a:Landroid/graphics/Rect;

    .line 371584
    iget-boolean v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v4}, LX/0yc;->k()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v4}, LX/0yc;->n()Z

    move-result v4

    if-nez v4, :cond_0

    .line 371585
    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v4}, LX/0yc;->d()I

    move-result v4

    invoke-virtual {v3, v2, v2, p1, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 371586
    :goto_1
    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    iget-object v5, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4, v1, v5}, LX/26O;->a(II)I

    move-result v4

    .line 371587
    iget-object v5, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    iget-object v6, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {v5, v1, v6}, LX/26O;->b(II)I

    move-result v5

    .line 371588
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v6, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v6

    iget v6, v3, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v7

    iget v7, v3, Landroid/graphics/Rect;->right:I

    sub-int v5, v7, v5

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->i:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v7

    invoke-virtual {v0, v4, v6, v5, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 371589
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 371590
    :cond_0
    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v4, p1, v1, v3}, LX/26O;->a(IILandroid/graphics/Rect;)V

    goto :goto_1

    .line 371591
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 371575
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 371576
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    invoke-virtual {v0}, LX/Aj6;->l()V

    .line 371577
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 371578
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    .line 371579
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 371572
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 371573
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26N;

    invoke-interface {v1, p0, v0, p1}, LX/Aj7;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V

    .line 371574
    :cond_0
    return-void
.end method

.method public final a(IILX/Aj7;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 371602
    if-nez p1, :cond_1

    .line 371603
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    if-eqz v0, :cond_0

    .line 371604
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/Aj7;I)V

    .line 371605
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setVisibility(I)V

    .line 371606
    :cond_0
    :goto_0
    return-void

    .line 371607
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g()V

    .line 371608
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->a(II)V

    .line 371609
    invoke-direct {p0, p3, p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/Aj7;I)V

    .line 371610
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->k:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/26O;LX/1aZ;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26O",
            "<TT;>;",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 371560
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "removeControllers() must be called before setting dialtone controllers"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 371561
    iput-object p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    move v3, v2

    .line 371562
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 371563
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v3}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    iput-boolean v2, v0, LX/Aj6;->b:Z

    .line 371564
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 371565
    goto :goto_0

    .line 371566
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 371567
    invoke-virtual {v0}, LX/Aj6;->l()V

    .line 371568
    invoke-virtual {v0, p2}, LX/1aX;->a(LX/1aZ;)V

    .line 371569
    iput-boolean v1, v0, LX/Aj6;->b:Z

    .line 371570
    iput-boolean v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 371571
    return-void
.end method

.method public final a(LX/26O;[LX/1aZ;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26O",
            "<TT;>;[",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 371540
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "removeControllers() must be called before setting "

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 371541
    array-length v0, p2

    invoke-virtual {p1}, LX/26O;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 371542
    iput-object p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    .line 371543
    array-length v1, p2

    .line 371544
    invoke-direct {p0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g(I)V

    .line 371545
    iput-boolean v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 371546
    :goto_2
    if-ge v2, v1, :cond_2

    .line 371547
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    aget-object v3, p2, v2

    iget-object v4, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v4, v2}, LX/26O;->b(I)Landroid/graphics/PointF;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/Aj6;LX/1aZ;Landroid/graphics/PointF;)V

    .line 371548
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 371549
    goto :goto_0

    :cond_1
    move v1, v2

    .line 371550
    goto :goto_1

    .line 371551
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 371552
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 371553
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getWidth()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getHeight()I

    move-result v2

    if-eqz v2, :cond_3

    .line 371554
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-direct {p0, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->h(I)V

    .line 371555
    :cond_3
    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getWidth()I

    move-result v3

    sub-int v1, v3, v1

    invoke-virtual {v2, v1}, LX/26O;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 371556
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getHeight()I

    move-result v1

    if-eq v1, v0, :cond_4

    .line 371557
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->requestLayout()V

    .line 371558
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->invalidate()V

    .line 371559
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 371536
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    if-eqz v0, :cond_0

    .line 371537
    :goto_0
    return-void

    .line 371538
    :cond_0
    new-instance v0, LX/26Q;

    invoke-direct {v0, p0}, LX/26Q;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    .line 371539
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 371535
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    invoke-static {v0, v1, p1}, LX/26R;->a(III)Z

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/graphics/Rect;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371532
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 371533
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 371534
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 371482
    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    .line 371483
    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 371484
    return-void
.end method

.method public final d(I)LX/1af;
    .locals 1

    .prologue
    .line 371485
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 371486
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    return-object v0
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 371400
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371401
    const/4 v0, 0x1

    .line 371402
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 371398
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 371399
    iget-object v0, v0, LX/Aj6;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 371396
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 371397
    iget-boolean v0, v0, LX/Aj6;->b:Z

    return v0
.end method

.method public getAccessibilityHelper()LX/26Q;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 371380
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->q:LX/26Q;

    return-object v0
.end method

.method public getContentDescriptions()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 371395
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->r:[Ljava/lang/String;

    return-object v0
.end method

.method public getImageStateHolders()LX/4Ac;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371394
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    return-object v0
.end method

.method public getImageStateHoldersForTesting()LX/4Ac;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 371393
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    return-object v0
.end method

.method public getInvisiblePhotoCount()I
    .locals 1

    .prologue
    .line 371392
    iget v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    return v0
.end method

.method public getLayoutCalculator()LX/26O;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/26O",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 371391
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    return-object v0
.end method

.method public getVisibleAttachments()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 371389
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 371390
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleAttachmentsCount()I
    .locals 1

    .prologue
    .line 371387
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 371388
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x69491e6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 371384
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 371385
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 371386
    const/16 v1, 0x2d

    const v2, -0xad9eddf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1b4eed43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 371381
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 371382
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 371383
    const/16 v1, 0x2d

    const v2, -0xec647aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 371403
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-nez v0, :cond_0

    .line 371404
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c:LX/03V;

    sget-object v1, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g:Ljava/lang/String;

    const-string v2, "onDraw() called on unbound View"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371405
    :goto_0
    return-void

    .line 371406
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 371407
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 371408
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v3

    .line 371409
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 371410
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 371411
    iget-boolean v1, v0, LX/Aj6;->b:Z

    if-eqz v1, :cond_3

    .line 371412
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 371413
    invoke-virtual {p0, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b(I)Z

    move-result v4

    .line 371414
    iget-boolean v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->w:Z

    if-eqz v1, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/26N;

    invoke-interface {v1}, LX/26N;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->x:I

    if-eq v2, v1, :cond_1

    if-nez v4, :cond_1

    .line 371415
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->e:LX/AmY;

    iget-object v5, v0, LX/Aj6;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, v5}, LX/AmY;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 371416
    :cond_1
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/26N;

    invoke-interface {v1}, LX/26N;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v4, :cond_2

    .line 371417
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->f:LX/26C;

    iget-object v5, v0, LX/Aj6;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, v5}, LX/26C;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 371418
    :cond_2
    if-eqz v4, :cond_3

    .line 371419
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 371420
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 371421
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 371422
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishTemporaryDetach()V

    .line 371423
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 371424
    return-void
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 371441
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 371442
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-nez v0, :cond_0

    .line 371443
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c:LX/03V;

    sget-object v2, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g:Ljava/lang/String;

    const-string v3, "onMeasure() called on unbound View"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371444
    invoke-virtual {p0, v1, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->setMeasuredDimension(II)V

    .line 371445
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 371446
    :goto_0
    return-void

    .line 371447
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 371448
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    .line 371449
    iget-boolean v3, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-static {v3}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/0yc;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->d()I

    move-result v0

    .line 371450
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->setMeasuredDimension(II)V

    .line 371451
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0

    .line 371452
    :cond_1
    iget-object v3, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    sub-int v2, v1, v2

    invoke-virtual {v3, v2}, LX/26O;->a(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x305610dd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 371453
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 371454
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    if-nez v1, :cond_0

    .line 371455
    iget-object v1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c:LX/03V;

    sget-object v2, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->g:Ljava/lang/String;

    const-string v3, "onSizeChanged() called on unbound View"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371456
    const/16 v1, 0x2d

    const v2, -0x3853f8e7

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 371457
    :goto_0
    return-void

    .line 371458
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 371459
    sub-int v1, p1, v1

    invoke-direct {p0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->h(I)V

    .line 371460
    const v1, 0x4f0d2112

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 371461
    invoke-super {p0}, Landroid/widget/FrameLayout;->onStartTemporaryDetach()V

    .line 371462
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 371463
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x4393e175

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 371464
    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->k()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->n()Z

    move-result v2

    if-nez v2, :cond_0

    .line 371465
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x595ea94f    # 3.9171001E15f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 371466
    :goto_0
    return v0

    .line 371467
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->s:LX/3rW;

    invoke-virtual {v2, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 371468
    const v2, 0x3f92ae16

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAllowedDrawVideoPlayIcon(Z)V
    .locals 0

    .prologue
    .line 371469
    iput-boolean p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->w:Z

    .line 371470
    return-void
.end method

.method public setContentDescriptions([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 371471
    iput-object p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->r:[Ljava/lang/String;

    .line 371472
    return-void
.end method

.method public setDialtoneEnabled(Z)V
    .locals 0

    .prologue
    .line 371473
    iput-boolean p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 371474
    return-void
.end method

.method public setFirstVideoIndex(I)V
    .locals 0

    .prologue
    .line 371475
    iput p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->x:I

    .line 371476
    return-void
.end method

.method public setInvisiblePhotoCount(I)V
    .locals 0

    .prologue
    .line 371477
    iput p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 371478
    return-void
.end method

.method public setOnImageClickListener(LX/Aj7;)V
    .locals 0
    .param p1    # LX/Aj7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Aj7",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 371479
    iput-object p1, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 371480
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 371481
    iget-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
