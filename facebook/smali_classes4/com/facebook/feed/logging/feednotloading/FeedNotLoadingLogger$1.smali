.class public final Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0oG;

.field public final synthetic b:LX/0r3;


# direct methods
.method public constructor <init>(LX/0r3;LX/0oG;)V
    .locals 0

    .prologue
    .line 479013
    iput-object p1, p0, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;->b:LX/0r3;

    iput-object p2, p0, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;->a:LX/0oG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 479014
    iget-object v0, p0, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;->b:LX/0r3;

    iget-object v1, p0, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;->a:LX/0oG;

    .line 479015
    iget-object v2, v0, LX/0r3;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0u7;

    invoke-virtual {v2}, LX/0u7;->a()F

    move-result v2

    .line 479016
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_0

    .line 479017
    const-string v3, "battery_level"

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v2, v4

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 479018
    :cond_0
    iget-object v2, v0, LX/0r3;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 479019
    if-eqz v2, :cond_1

    .line 479020
    const-string v3, "network"

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 479021
    const-string v3, "network_state"

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 479022
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/logging/feednotloading/FeedNotLoadingLogger$1;->a:LX/0oG;

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 479023
    return-void

    .line 479024
    :cond_1
    const-string v2, "network"

    const-string v3, "unknown"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method
