.class public abstract Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;
.super LX/3Ga;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/Btp;

.field public c:LX/1nA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/view/View;

.field public p:Landroid/view/View;

.field public q:Landroid/view/View;

.field public r:Landroid/widget/TextView;

.field public s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public t:Landroid/widget/TextView;

.field public u:Landroid/widget/TextView;

.field public v:LX/2qV;

.field public w:Z

.field public x:Z

.field private y:Lcom/facebook/video/engine/VideoPlayerParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 541311
    const-class v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 541306
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541307
    const-class v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 541308
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gc;

    invoke-direct {v1, p0, p0}, LX/3Gc;-><init>(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541309
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Gd;

    invoke-direct {v1, p0, p0}, LX/3Gd;-><init>(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541310
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;

    invoke-static {p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v1

    check-cast v1, LX/1nA;

    const-class v2, LX/2yI;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2yI;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object p0

    check-cast p0, LX/1C2;

    iput-object v1, p1, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->c:LX/1nA;

    iput-object v2, p1, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->d:LX/2yI;

    iput-object p0, p1, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->e:LX/1C2;

    return-void
.end method

.method private static c(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2pa;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 541284
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541285
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 541286
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/03g;->a(Z)V

    move-object v0, v1

    .line 541287
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 541288
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 541289
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 541290
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v0, v0

    .line 541291
    :goto_0
    return-object v0

    .line 541292
    :cond_0
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 541293
    if-eqz v0, :cond_1

    .line 541294
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 541295
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v2, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 541296
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/03g;->a(Z)V

    move-object v0, v1

    .line 541297
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 541298
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 541299
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 541300
    instance-of v0, v2, Ljava/lang/Integer;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 541301
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 541302
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 541303
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    move-object v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v0, v0

    .line 541304
    goto :goto_0

    .line 541305
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 541280
    invoke-static {p1}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->c(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 541281
    if-eqz v0, :cond_0

    .line 541282
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 541283
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V
    .locals 3

    .prologue
    .line 541269
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b:LX/Btp;

    sget-object v1, LX/Btp;->PAUSESCREEN:LX/Btp;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->x:Z

    if-nez v0, :cond_1

    .line 541270
    :cond_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lw;

    sget-object v2, LX/7MP;->ALWAYS_HIDDEN:LX/7MP;

    invoke-direct {v1, v2}, LX/7Lw;-><init>(LX/7MP;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 541271
    :cond_1
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pW;

    sget-object v2, LX/2pO;->HIDE:LX/2pO;

    invoke-direct {v1, v2}, LX/2pW;-><init>(LX/2pO;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 541272
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2pX;

    sget-object v2, LX/2pR;->HIDE:LX/2pR;

    invoke-direct {v1, v2}, LX/2pX;-><init>(LX/2pR;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 541273
    const/4 v1, 0x0

    .line 541274
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541275
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541276
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541277
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541278
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 541279
    :cond_2
    return-void
.end method

.method private setupCallToActionEndscreen(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 5
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 541239
    if-eqz p1, :cond_0

    .line 541240
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 541241
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 541242
    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->getCallToActionEndscreenReplayClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541243
    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 541244
    const v2, -0x1e53800c

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 541245
    if-eqz v1, :cond_1

    .line 541246
    const/4 v2, 0x0

    .line 541247
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 541248
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 541249
    iget-object v3, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v4, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 541250
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->d:LX/2yI;

    invoke-virtual {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, p1, v4, v2}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541251
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->t:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541252
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2v7;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541253
    :cond_0
    :goto_2
    return-void

    .line 541254
    :cond_1
    if-eqz v0, :cond_2

    .line 541255
    const/4 v2, 0x0

    .line 541256
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 541257
    :goto_3
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 541258
    iget-object v2, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v3, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 541259
    :goto_4
    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->c:LX/1nA;

    invoke-virtual {v1, p1, v0}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 541260
    iget-object v2, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541261
    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->t:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541262
    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2v7;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541263
    goto :goto_2

    .line 541264
    :cond_2
    invoke-static {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->w(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 541265
    goto/16 :goto_0

    .line 541266
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v3, 0x7f021171

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_5
    move-object v1, v2

    .line 541267
    goto :goto_3

    .line 541268
    :cond_6
    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_4
.end method

.method public static w(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 541233
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 541234
    if-eqz v0, :cond_0

    .line 541235
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541236
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541237
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 541238
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 541218
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 541219
    invoke-static {p1}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->c(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 541220
    if-eqz p2, :cond_1

    .line 541221
    if-eqz v2, :cond_3

    .line 541222
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 541223
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 541224
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 541225
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->w:Z

    .line 541226
    invoke-static {p0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->w(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V

    .line 541227
    :cond_1
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 541228
    if-eqz v0, :cond_2

    .line 541229
    invoke-direct {p0, v2}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->setupCallToActionEndscreen(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 541230
    :cond_2
    iput-boolean v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->x:Z

    .line 541231
    return-void

    :cond_3
    move v0, v1

    .line 541232
    goto :goto_0
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 541194
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 541216
    invoke-super {p0}, LX/3Ga;->d()V

    .line 541217
    return-void
.end method

.method public abstract getCallToActionEndscreenReplayClickListener()Landroid/view/View$OnClickListener;
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 541215
    const v0, 0x7f030221

    return v0
.end method

.method public abstract i()Z
.end method

.method public final j()V
    .locals 13

    .prologue
    .line 541207
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 541208
    :cond_0
    :goto_0
    return-void

    .line 541209
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->e:LX/1C2;

    iget-object v1, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    .line 541210
    iget-object v4, v3, LX/2pb;->D:LX/04G;

    move-object v3, v4

    .line 541211
    iget-object v4, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->y:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    .line 541212
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/04A;->VIDEO_REPLAYED:LX/04A;

    iget-object v6, v6, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v6, v0

    move-object v8, v4

    move-object v9, v1

    move v10, v5

    move-object v11, v2

    move-object v12, v3

    .line 541213
    invoke-static/range {v6 .. v12}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 541214
    goto :goto_0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 1

    .prologue
    .line 541205
    invoke-static {p1}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->c(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->setupCallToActionEndscreen(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 541206
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 541195
    const v0, 0x7f0d084f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->o:Landroid/view/View;

    .line 541196
    const v0, 0x7f0d0850

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->p:Landroid/view/View;

    .line 541197
    const v0, 0x7f0d0852

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->r:Landroid/widget/TextView;

    .line 541198
    const v0, 0x7f0d0853

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    .line 541199
    const v0, 0x7f0d0854

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 541200
    const v0, 0x7f0d0855

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->t:Landroid/widget/TextView;

    .line 541201
    const v0, 0x7f0d0856

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->u:Landroid/widget/TextView;

    .line 541202
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    sget-object v1, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 541203
    iget-object v0, p0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->q:Landroid/view/View;

    const v1, 0x7f0d0083

    const-string v2, "video_cta_end_screen_click"

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 541204
    return-void
.end method
