.class public final Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 544110
    iput-object p1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 544111
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 544112
    iget-object v2, v0, LX/2pb;->D:LX/04G;

    move-object v0, v2

    .line 544113
    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 544114
    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v3, v3, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    invoke-virtual {v2, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVideoPlayerViewSize(LX/3HY;)V

    .line 544115
    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 544116
    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    .line 544117
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    sget-object v2, LX/Acd;->BROADCAST_INTERRUPTED:LX/Acd;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setLiveText(LX/Acd;)V

    .line 544118
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVideoPlayerViewSize(LX/3HY;)V

    .line 544119
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 544120
    return-void

    :cond_0
    move v0, v1

    .line 544121
    goto :goto_0

    :cond_1
    move v0, v1

    .line 544122
    goto :goto_1
.end method
