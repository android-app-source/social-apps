.class public Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;
.super LX/3Gb;
.source ""

# interfaces
.implements LX/3Ha;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/BwV;",
        ">",
        "LX/3Gb",
        "<TE;>;",
        "LX/3Ha;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final A:Ljava/lang/Runnable;

.field private final B:LX/3Hb;

.field public C:LX/0An;

.field public D:LX/3HY;

.field public E:Z

.field public F:Z

.field private G:Z

.field public H:Z

.field public I:Z

.field public J:LX/0ib;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final K:Ljava/lang/Runnable;

.field public final L:Ljava/lang/Runnable;

.field public final b:Landroid/view/View;

.field public final c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

.field private final d:Ljava/lang/Runnable;

.field public e:LX/3Hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/19j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/3Hf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ac;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0iX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public z:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 543986
    const-class v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 543987
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 543988
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 543989
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 543990
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    .line 543991
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 543992
    new-instance v0, LX/3Hb;

    invoke-direct {v0, p0}, LX/3Hb;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->B:LX/3Hb;

    .line 543993
    new-instance v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$1;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    .line 543994
    new-instance v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->L:Ljava/lang/Runnable;

    .line 543995
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-static {v0}, LX/3Hc;->b(LX/0QB;)LX/3Hc;

    move-result-object v3

    check-cast v3, LX/3Hc;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v6

    check-cast v6, LX/19j;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v7

    check-cast v7, LX/0xX;

    invoke-static {v0}, LX/3Hf;->a(LX/0QB;)LX/3Hf;

    move-result-object v8

    check-cast v8, LX/3Hf;

    invoke-static {v0}, LX/0Ac;->b(LX/0QB;)LX/0Ac;

    move-result-object p1

    check-cast p1, LX/0Ac;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p2

    check-cast p2, LX/1b4;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object p3

    check-cast p3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v0

    check-cast v0, LX/0iX;

    iput-object v3, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iput-object v4, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f:LX/0Sh;

    iput-object v5, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iput-object v6, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->p:LX/19j;

    iput-object v7, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->q:LX/0xX;

    iput-object v8, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->r:LX/3Hf;

    iput-object p1, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->s:LX/0Ac;

    iput-object p2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->t:LX/1b4;

    iput-object p3, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v0, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    .line 543996
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->getLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 543997
    const v0, 0x7f0d132e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543998
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a()V

    .line 543999
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    .line 544000
    iput-object p0, v0, LX/3Hc;->l:LX/3Ha;

    .line 544001
    const v0, 0x7f0d132d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b:Landroid/view/View;

    .line 544002
    const v0, 0x7f0d132f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 544003
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hl;

    invoke-direct {v1, p0, p0}, LX/3Hl;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544004
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hm;

    invoke-direct {v1, p0, p0}, LX/3Hm;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544005
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hn;

    invoke-direct {v1, p0}, LX/3Hn;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544006
    new-instance v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$3;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$3;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d:Ljava/lang/Runnable;

    .line 544007
    new-instance v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$4;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$4;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->A:Ljava/lang/Runnable;

    .line 544008
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 544009
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/BwV;

    if-eqz v0, :cond_0

    .line 544010
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/Bwd;->a(Ljava/lang/String;Z)V

    .line 544011
    :goto_0
    return-void

    .line 544012
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    invoke-virtual {v0, p2}, LX/3Hc;->a(Z)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V
    .locals 8

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 544013
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v3, :cond_4

    .line 544014
    sget-object v0, LX/2qV;->PLAYING:LX/2qV;

    if-eq p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->E:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 544015
    :goto_0
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->t:LX/1b4;

    .line 544016
    iget-object v5, v3, LX/1b4;->b:LX/0Uh;

    const/16 v6, 0x6c

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    move v3, v5

    .line 544017
    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->H:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    .line 544018
    iget-boolean v5, v3, LX/2pb;->B:Z

    move v3, v5

    .line 544019
    if-eqz v3, :cond_2

    move v3, v1

    .line 544020
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_3

    .line 544021
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(Ljava/lang/String;Z)V

    .line 544022
    invoke-virtual {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(LX/2qV;)V

    .line 544023
    sget-object v0, LX/2rV;->a:[I

    invoke-virtual {p1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 544024
    :goto_3
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 544025
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v0, v4}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 544026
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVideoPlayerViewSize(LX/3HY;)V

    .line 544027
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    .line 544028
    :goto_4
    return-void

    :cond_1
    move v0, v2

    .line 544029
    goto :goto_0

    :cond_2
    move v3, v2

    .line 544030
    goto :goto_1

    :cond_3
    move v1, v2

    .line 544031
    goto :goto_2

    .line 544032
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_4

    .line 544033
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 544034
    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->F:Z

    if-nez v0, :cond_7

    .line 544035
    :goto_5
    goto :goto_3

    .line 544036
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 544037
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(Ljava/lang/String;Z)V

    .line 544038
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->t:LX/1b4;

    .line 544039
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v3, 0x347

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 544040
    if-nez v0, :cond_5

    .line 544041
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_4

    .line 544042
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVideoPlayerViewSize(LX/3HY;)V

    .line 544043
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    goto :goto_4

    .line 544044
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(Ljava/lang/String;Z)V

    .line 544045
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_4

    .line 544046
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->F:Z

    .line 544047
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Z)V

    .line 544048
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->h()V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 544049
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/BwV;

    if-eqz v0, :cond_1

    .line 544050
    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->G:Z

    if-nez v0, :cond_0

    .line 544051
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v0, p1, v1, p0}, LX/Bwd;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/3Ha;)V

    .line 544052
    :cond_0
    :goto_0
    return-void

    .line 544053
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->G:Z

    if-eqz v0, :cond_2

    .line 544054
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    invoke-virtual {v0, p1}, LX/3Hc;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 544055
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    invoke-virtual {v0, p1}, LX/3Hc;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 544056
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->A:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 544057
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->C:LX/0An;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->C:LX/0An;

    .line 544058
    iget-object v1, v0, LX/0Ab;->e:Ljava/lang/String;

    move-object v0, v1

    .line 544059
    if-ne v0, p1, :cond_1

    .line 544060
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->C:LX/0An;

    .line 544061
    iget-object v1, v0, LX/0An;->g:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    if-eqz v1, :cond_0

    .line 544062
    iget-object v1, v0, LX/0An;->g:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    const-class v3, LX/2WD;

    invoke-virtual {v1, v3, v0}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 544063
    :cond_0
    iput-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->C:LX/0An;

    .line 544064
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 544065
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0, v2}, LX/2pb;->a(LX/2qG;)V

    .line 544066
    :cond_1
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/BwV;

    if-eqz v0, :cond_2

    .line 544067
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Bwd;->a(Ljava/lang/String;)V

    .line 544068
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Bwd;->b(Ljava/lang/String;)V

    .line 544069
    :goto_0
    return-void

    .line 544070
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 543976
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/BwV;

    if-eqz v0, :cond_0

    .line 543977
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Bwd;->b(Ljava/lang/String;)V

    .line 543978
    :cond_0
    return-void
.end method

.method private e(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 4

    .prologue
    .line 544071
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_1

    .line 544072
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 544073
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 544074
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 544075
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d(Ljava/lang/String;)V

    .line 544076
    :cond_0
    :goto_0
    return-void

    .line 544077
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->r:LX/3Hf;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/3Hf;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 544078
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544079
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_2

    .line 544080
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qg;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v1, v2, v3}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 544081
    :cond_2
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_4

    .line 544082
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    .line 544083
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 544084
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 544085
    invoke-static {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    .line 544086
    :cond_3
    :goto_1
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    goto :goto_0

    .line 544087
    :cond_4
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    .line 544088
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_3

    .line 544089
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 544090
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 544091
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 2
    .param p1    # Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544092
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 544093
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 544094
    :goto_0
    return-void

    .line 544095
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->r:LX/3Hf;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, p1}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public static w(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)Z
    .locals 2

    .prologue
    .line 544096
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 544097
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 544098
    :goto_0
    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->q:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 544099
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 544100
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V
    .locals 3

    .prologue
    .line 544101
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 544102
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7ME;

    sget-object v2, LX/7MD;->ALWAYS_INVISIBLE:LX/7MD;

    invoke-direct {v1, v2}, LX/7ME;-><init>(LX/7MD;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 544103
    :cond_0
    return-void
.end method

.method private z()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 543979
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 543980
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7ME;

    sget-object v2, LX/7MD;->DEFAULT:LX/7MD;

    invoke-direct {v1, v2}, LX/7ME;-><init>(LX/7MD;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 543981
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 543982
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    .line 543983
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 543984
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 543985
    return-void
.end method


# virtual methods
.method public a(LX/2oN;)V
    .locals 2

    .prologue
    .line 544104
    sget-object v0, LX/2rV;->b:[I

    invoke-virtual {p1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 544105
    :goto_0
    return-void

    .line 544106
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->LIVE:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    goto :goto_0

    .line 544107
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 544108
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_INLINE:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(LX/2pa;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 543797
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543798
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 543799
    instance-of v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v3, :cond_0

    .line 543800
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 543801
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 543802
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 543803
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543804
    iget-object v3, v0, LX/2pb;->D:LX/04G;

    move-object v0, v3

    .line 543805
    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v3, :cond_8

    .line 543806
    invoke-virtual {p1}, LX/2pa;->f()LX/3HY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    .line 543807
    :goto_0
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 543808
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    .line 543809
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 543810
    if-eqz v4, :cond_13

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 543811
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v3

    iput-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->H:Z

    .line 543812
    iget-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->H:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    .line 543813
    iget-boolean v5, v3, LX/0iX;->h:Z

    move v3, v5

    .line 543814
    if-eqz v3, :cond_2

    .line 543815
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    if-nez v3, :cond_1

    .line 543816
    new-instance v3, LX/Bwg;

    invoke-direct {v3, p0}, LX/Bwg;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    iput-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    .line 543817
    :cond_1
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    .line 543818
    iget-boolean v5, v3, LX/0iX;->i:Z

    move v3, v5

    .line 543819
    if-nez v3, :cond_16

    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->I:Z

    .line 543820
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    iget-object v5, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    invoke-virtual {v3, v5}, LX/0iX;->a(LX/0ib;)V

    .line 543821
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543822
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    .line 543823
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v3, v5, :cond_a

    move v3, v1

    .line 543824
    :goto_2
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v0

    if-nez v0, :cond_15

    .line 543825
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v5, :cond_b

    move v0, v1

    :goto_3
    or-int/2addr v0, v3

    .line 543826
    :goto_4
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsLiveNow(Z)V

    .line 543827
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543828
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_17

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->s()LX/04D;

    move-result-object v0

    sget-object v5, LX/04D;->VIDEO_HOME:LX/04D;

    if-ne v0, v5, :cond_17

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543829
    iget-object v5, v0, LX/2pb;->D:LX/04G;

    move-object v0, v5

    .line 543830
    sget-object v5, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v5, :cond_17

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 543831
    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setLiveIndicatorClickable(Z)V

    .line 543832
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->t:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 543833
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsAudioLive(Z)V

    .line 543834
    :cond_3
    if-eqz p2, :cond_14

    .line 543835
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v0

    .line 543836
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 543837
    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    if-eqz v3, :cond_18

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    sget-object v4, LX/04D;->FEED:LX/04D;

    if-ne v3, v4, :cond_18

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    .line 543838
    iget-object v4, v3, LX/2pb;->D:LX/04G;

    move-object v3, v4

    .line 543839
    sget-object v4, LX/04G;->WATCH_AND_GO:LX/04G;

    if-eq v3, v4, :cond_18

    const/4 v3, 0x1

    :goto_7
    move v3, v3

    .line 543840
    iput-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->F:Z

    .line 543841
    :goto_8
    iput-boolean v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->G:Z

    .line 543842
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v3, v4, :cond_d

    .line 543843
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    .line 543844
    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v1

    .line 543845
    if-nez p2, :cond_4

    if-eqz v1, :cond_7

    .line 543846
    :cond_4
    if-eqz v1, :cond_5

    .line 543847
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 543848
    :cond_5
    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Ljava/lang/String;)V

    .line 543849
    if-eqz v1, :cond_6

    .line 543850
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setShowViewerCount(Z)V

    .line 543851
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->A:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    const v3, -0x326fa507

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 543852
    :cond_6
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    .line 543853
    iget-object v6, p0, LX/2oy;->j:LX/2pb;

    if-eqz v6, :cond_7

    iget-object v6, p0, LX/2oy;->j:LX/2pb;

    .line 543854
    iget-object v7, v6, LX/2pb;->D:LX/04G;

    move-object v6, v7

    .line 543855
    sget-object v7, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v6, v7, :cond_7

    iget-object v6, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v6, v7, :cond_7

    .line 543856
    iget-object v6, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->s:LX/0Ac;

    int-to-long v8, v0

    new-instance v7, LX/Bwh;

    invoke-direct {v7, p0}, LX/Bwh;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    invoke-virtual {v6, v1, v8, v9, v7}, LX/0Ac;->a(Ljava/lang/String;JLX/0Ae;)LX/0An;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->C:LX/0An;

    .line 543857
    iget-object v6, p0, LX/2oy;->j:LX/2pb;

    new-instance v7, LX/Bwi;

    invoke-direct {v7, p0}, LX/Bwi;-><init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    invoke-virtual {v6, v7}, LX/2pb;->a(LX/2qG;)V

    .line 543858
    :cond_7
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543859
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 543860
    invoke-static {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    .line 543861
    :goto_9
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->u:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->B:LX/3Hb;

    .line 543862
    iget-object v3, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->m:Ljava/util/Map;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543863
    return-void

    .line 543864
    :cond_8
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543865
    iget-object v3, v0, LX/2pb;->D:LX/04G;

    move-object v0, v3

    .line 543866
    sget-object v3, LX/04G;->WATCH_AND_GO:LX/04G;

    if-ne v0, v3, :cond_9

    .line 543867
    sget-object v0, LX/3HY;->EXTRA_SMALL:LX/3HY;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    goto/16 :goto_0

    .line 543868
    :cond_9
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->D:LX/3HY;

    goto/16 :goto_0

    :cond_a
    move v3, v2

    .line 543869
    goto/16 :goto_2

    :cond_b
    move v0, v2

    .line 543870
    goto/16 :goto_3

    :cond_c
    move v0, v2

    .line 543871
    goto/16 :goto_6

    .line 543872
    :cond_d
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v3, :cond_e

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v3, :cond_11

    .line 543873
    :cond_e
    if-eqz p2, :cond_f

    .line 543874
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Ljava/lang/String;)V

    .line 543875
    :cond_f
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(Ljava/lang/String;Z)V

    .line 543876
    :cond_10
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_9

    .line 543877
    :cond_11
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 543878
    if-eqz p2, :cond_12

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->t:LX/1b4;

    .line 543879
    iget-object v3, v0, LX/1b4;->b:LX/0Uh;

    const/16 v4, 0x346

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v0, v3

    .line 543880
    if-eqz v0, :cond_12

    .line 543881
    iput-boolean v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->G:Z

    .line 543882
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(Ljava/lang/String;Z)V

    .line 543883
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Ljava/lang/String;)V

    .line 543884
    :cond_12
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 543885
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 543886
    invoke-static {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    goto :goto_9

    .line 543887
    :cond_13
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_9

    :cond_14
    move v0, v2

    goto/16 :goto_8

    :cond_15
    move v0, v3

    goto/16 :goto_4

    .line 543888
    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_7
.end method

.method public a(LX/2qV;)V
    .locals 1

    .prologue
    .line 543889
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 543890
    :cond_0
    :goto_0
    return-void

    .line 543891
    :cond_1
    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 543892
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543893
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 543894
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 543895
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543896
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 543897
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 543898
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 543899
    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, p0

    .line 543900
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 543901
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543902
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 543903
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543904
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543905
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 543906
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 543907
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 543908
    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, p0

    .line 543909
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 543910
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 543911
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 5

    .prologue
    .line 543912
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543913
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543914
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    if-eqz v1, :cond_0

    .line 543915
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v1, v0, :cond_0

    .line 543916
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2qg;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v2, v3, v4}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 543917
    :cond_0
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v2, :cond_2

    .line 543918
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    .line 543919
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 543920
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 543921
    invoke-static {p0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    .line 543922
    :goto_0
    invoke-static {v0}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543923
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->c(Ljava/lang/String;)V

    .line 543924
    :cond_1
    return-void

    .line 543925
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 543926
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    .line 543927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543928
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 543929
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qg;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v1, v2, v3}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 543930
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 543931
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 543932
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 543933
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 543934
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 4

    .prologue
    .line 543935
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    .line 543936
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543937
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_1

    .line 543938
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 543939
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qg;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v1, v2, v3}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 543940
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->c(Ljava/lang/String;)V

    .line 543941
    :cond_1
    return-void
.end method

.method public final c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 5

    .prologue
    .line 543942
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543943
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543944
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    if-eqz v1, :cond_0

    .line 543945
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v1, v0, :cond_0

    .line 543946
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2qg;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v2, v3, v4}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 543947
    :cond_0
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v2, :cond_2

    .line 543948
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V

    .line 543949
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 543950
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 543951
    invoke-static {p0, v1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->b(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;LX/2qV;)V

    .line 543952
    :goto_0
    invoke-static {v0}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543953
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 543954
    :cond_1
    return-void

    .line 543955
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 543779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543780
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 543781
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543782
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 543783
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 543784
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 543785
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 543786
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 543787
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 543788
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    if-eqz v0, :cond_1

    .line 543789
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    invoke-virtual {v0, v1}, LX/0iX;->b(LX/0ib;)V

    .line 543790
    iput-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->J:LX/0ib;

    .line 543791
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    .line 543792
    iput-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    .line 543793
    iput-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 543794
    iput-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->E:Z

    .line 543795
    iput-boolean v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->H:Z

    .line 543796
    return-void
.end method

.method public final d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 4

    .prologue
    .line 543956
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z()V

    .line 543957
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 543958
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_1

    .line 543959
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 543960
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qg;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->y:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v1, v2, v3}, LX/2qg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 543961
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->f(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 543962
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c(Ljava/lang/String;)V

    .line 543963
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d(Ljava/lang/String;)V

    .line 543964
    return-void
.end method

.method public getLayout()I
    .locals 1

    .prologue
    .line 543965
    const v0, 0x7f030a2a

    return v0
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 543966
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->j()I

    move-result v0

    if-lez v0, :cond_0

    .line 543967
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->j()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setTimeElapsed(J)V

    .line 543968
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->i()V

    .line 543969
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543970
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    const v4, 0x1b776feb

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 543971
    :cond_1
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 543972
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 543973
    return-void
.end method

.method public s_(I)V
    .locals 1

    .prologue
    .line 543974
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 543975
    return-void
.end method
