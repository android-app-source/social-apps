.class public final Lcom/facebook/feed/video/inline/VideoLiveScribeButtonPlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3Gm;


# direct methods
.method public constructor <init>(LX/3Gm;)V
    .locals 0

    .prologue
    .line 541841
    iput-object p1, p0, Lcom/facebook/feed/video/inline/VideoLiveScribeButtonPlugin$1;->a:LX/3Gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 541830
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoLiveScribeButtonPlugin$1;->a:LX/3Gm;

    const/4 v6, 0x0

    .line 541831
    iget-object v1, v0, LX/3Gm;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Gm;->w:LX/0Tn;

    invoke-interface {v1, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, LX/3Gm;->z:Z

    if-nez v1, :cond_0

    iget-object v1, v0, LX/3Gm;->C:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 541832
    :cond_0
    :goto_0
    return-void

    .line 541833
    :cond_1
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, LX/3Gm;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, LX/3Gm;->E:LX/0hs;

    .line 541834
    iget-object v1, v0, LX/3Gm;->E:LX/0hs;

    invoke-virtual {v0}, LX/3Gm;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080dd7

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, LX/3Gm;->C:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 541835
    iget-object v1, v0, LX/3Gm;->E:LX/0hs;

    iget-object v2, v0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 541836
    iget-object v1, v0, LX/3Gm;->E:LX/0hs;

    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 541837
    iget-object v1, v0, LX/3Gm;->E:LX/0hs;

    new-instance v2, LX/Bwv;

    invoke-direct {v2, v0}, LX/Bwv;-><init>(LX/3Gm;)V

    .line 541838
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 541839
    iget-object v1, v0, LX/3Gm;->q:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, v0, LX/3Gm;->F:J

    .line 541840
    iget-object v1, v0, LX/3Gm;->E:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->d()V

    goto :goto_0
.end method
