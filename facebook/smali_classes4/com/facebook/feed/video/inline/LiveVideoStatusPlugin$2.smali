.class public final Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 544123
    iput-object p1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 544124
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 544125
    :goto_0
    return-void

    .line 544126
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->r:LX/3Hf;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin$2;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v1, v2, v3}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->z:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method
