.class public Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;
.super LX/3Ga;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3AW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2xj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:Z

.field private x:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 545172
    const-class v0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 545170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545171
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545168
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545169
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 545164
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545165
    const-class v0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 545166
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3Hs;

    invoke-direct {v1, p0}, LX/3Hs;-><init>(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545167
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v2

    check-cast v2, LX/3AW;

    invoke-static {p0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v3

    check-cast v3, LX/2xj;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object v1, p1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->b:LX/1Ad;

    iput-object v2, p1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->c:LX/3AW;

    iput-object v3, p1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->d:LX/2xj;

    iput-object p0, p1, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->e:LX/1b4;

    return-void
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;II)V
    .locals 8

    .prologue
    .line 545051
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->d:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545052
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 545053
    if-eqz v0, :cond_1

    .line 545054
    iget-object v7, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->c:LX/3AW;

    new-instance v0, LX/7Qm;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545055
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 545056
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    move-object v3, p2

    move v4, p4

    move v5, p5

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/7Qm;-><init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;)V

    .line 545057
    iget-object v1, v7, LX/3AW;->g:Landroid/util/LruCache;

    iget-object v2, v0, LX/7Qm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 545058
    if-eqz p1, :cond_0

    .line 545059
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/0JT;->VIDEO_HOME_END_SCREEN_DISPLAYED:LX/0JT;

    iget-object v2, v2, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 545060
    sget-object v2, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    sget-object v3, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v3, v3, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 545061
    invoke-virtual {v0, v1}, LX/7Qm;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 545062
    invoke-static {v7, v1}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 545063
    :cond_0
    iget-object v1, v7, LX/3AW;->g:Landroid/util/LruCache;

    iget-object v2, v0, LX/7Qm;->a:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545064
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;LX/2pa;Z)V
    .locals 6
    .param p0    # Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 545135
    if-eqz p1, :cond_2

    .line 545136
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitComponentTokenKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545137
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitComponentTokenKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 545138
    instance-of v1, v0, Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 545139
    check-cast v0, Ljava/lang/String;

    .line 545140
    :goto_0
    move-object v2, v0

    .line 545141
    :goto_1
    if-eqz p1, :cond_0

    .line 545142
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitComponentTrackingDataKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 545143
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitComponentTrackingDataKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 545144
    instance-of v1, v0, Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 545145
    check-cast v0, Ljava/lang/String;

    .line 545146
    :goto_2
    move-object v3, v0

    .line 545147
    :cond_0
    if-eqz p1, :cond_3

    .line 545148
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_6

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitPositionKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 545149
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "UnitPositionKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 545150
    instance-of v1, v0, Ljava/lang/Integer;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 545151
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 545152
    :goto_3
    move v4, v0

    .line 545153
    :goto_4
    if-eqz p1, :cond_1

    .line 545154
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_7

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "PositionInUnitKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 545155
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "PositionInUnitKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 545156
    instance-of v1, v0, Ljava/lang/Integer;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 545157
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 545158
    :goto_5
    move v5, v0

    .line 545159
    :cond_1
    move-object v0, p0

    move v1, p2

    .line 545160
    invoke-direct/range {v0 .. v5}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->a(ZLjava/lang/String;Ljava/lang/String;II)V

    .line 545161
    return-void

    :cond_2
    move-object v2, v3

    .line 545162
    goto :goto_1

    :cond_3
    move v4, v5

    .line 545163
    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, -0x1

    goto :goto_3

    :cond_7
    const/4 v0, -0x1

    goto :goto_5
.end method

.method private j()V
    .locals 2

    .prologue
    .line 545131
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 545132
    if-eqz v0, :cond_0

    .line 545133
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545134
    :cond_0
    return-void
.end method

.method public static k(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;)V
    .locals 3

    .prologue
    .line 545122
    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->w:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->y:Z

    if-eqz v0, :cond_0

    const v0, 0x7f080d90

    .line 545123
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->w:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    .line 545124
    :goto_1
    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 545125
    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 545126
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545127
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545128
    return-void

    .line 545129
    :cond_0
    const v0, 0x7f080d8f

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->y:Z

    if-eqz v0, :cond_2

    const v0, 0x7f080d92

    goto :goto_0

    :cond_2
    const v0, 0x7f080d91

    goto :goto_0

    .line 545130
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 545108
    invoke-super {p0, p1, p2}, LX/3Ga;->a(LX/2pa;Z)V

    .line 545109
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->j()V

    .line 545110
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "GraphQLStoryProps"

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "GraphQLStoryProps"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 545111
    :cond_0
    :goto_0
    return-void

    .line 545112
    :cond_1
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 545113
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545114
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->e:LX/1b4;

    invoke-virtual {v0, v2}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->y:Z

    .line 545115
    invoke-static {v2}, LX/3In;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 545116
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v2, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->w:Z

    .line 545117
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->i()V

    .line 545118
    invoke-static {p0, p1, v1}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->b(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;LX/2pa;Z)V

    .line 545119
    invoke-static {p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->k(Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;)V

    goto :goto_0

    .line 545120
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 545121
    :cond_3
    iput-boolean v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->w:Z

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 545107
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 545104
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->j()V

    .line 545105
    invoke-super {p0}, LX/3Ga;->d()V

    .line 545106
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 545103
    const v0, 0x7f03159f

    return v0
.end method

.method public final i()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 545099
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 545100
    iget-object v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 545101
    iget-object v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->p:Landroid/view/View;

    iget-boolean v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->w:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 545102
    :cond_1
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 545098
    const/4 v0, 0x1

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 545074
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 545075
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_6

    .line 545076
    :cond_0
    const/4 v2, 0x0

    .line 545077
    :goto_0
    move-object v0, v2

    .line 545078
    if-eqz v0, :cond_2

    .line 545079
    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->b:LX/1Ad;

    sget-object v4, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 545080
    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 545081
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 545082
    :goto_1
    invoke-virtual {p1}, LX/2pa;->f()LX/3HY;

    move-result-object v0

    sget-object v2, LX/3HY;->REGULAR:LX/3HY;

    if-eq v0, v2, :cond_3

    const/4 v0, 0x1

    .line 545083
    :goto_2
    iget-object v4, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    if-eqz v0, :cond_4

    move v2, v3

    :goto_3
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 545084
    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->r:Landroid/view/View;

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545085
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->s()LX/04D;

    move-result-object v0

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 545086
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 545087
    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v1, :cond_1

    .line 545088
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_7

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "IsNotifVideoKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 545089
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "IsNotifVideoKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 545090
    :goto_5
    move-object v0, v0

    .line 545091
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 545092
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0683

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 545093
    :cond_1
    return-void

    .line 545094
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 545095
    goto :goto_2

    :cond_4
    move v2, v1

    .line 545096
    goto :goto_3

    :cond_5
    move v1, v3

    .line 545097
    goto :goto_4

    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_5
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 545065
    const v0, 0x7f0d306a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->p:Landroid/view/View;

    .line 545066
    const v0, 0x7f0d0b87

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 545067
    const v0, 0x7f0d0807

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->q:Landroid/view/View;

    .line 545068
    const v0, 0x7f0d30ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->r:Landroid/view/View;

    .line 545069
    const v0, 0x7f0d30b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->s:Landroid/widget/TextView;

    .line 545070
    const v0, 0x7f0d30b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->t:Landroid/view/View;

    .line 545071
    const v0, 0x7f0d30bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->u:Landroid/widget/TextView;

    .line 545072
    const v0, 0x7f0d30bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/inline/VideoInlineBroadcastEndScreenPlugin;->v:Landroid/view/View;

    .line 545073
    return-void
.end method
