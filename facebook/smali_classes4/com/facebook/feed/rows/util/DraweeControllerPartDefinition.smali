.class public Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/AmL",
        "<TV;>;",
        "LX/AmM",
        "<TV;>;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;


# instance fields
.field private final b:LX/1Ad;

.field private final c:LX/1HI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459683
    const-class v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/1HI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459628
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 459629
    iput-object p1, p0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->b:LX/1Ad;

    .line 459630
    iput-object p2, p0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->c:LX/1HI;

    .line 459631
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)LX/1aZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AmL",
            "<TV;>;",
            "LX/AmM",
            "<TV;>;)",
            "LX/1aZ;"
        }
    .end annotation

    .prologue
    .line 459675
    iget-object v0, p2, LX/AmM;->a:LX/1aZ;

    if-eqz v0, :cond_0

    .line 459676
    iget-object v0, p2, LX/AmM;->a:LX/1aZ;

    .line 459677
    :goto_0
    return-object v0

    .line 459678
    :cond_0
    iget-object v0, p2, LX/AmM;->b:LX/1bf;

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/AmL;LX/1bf;)LX/1bf;

    move-result-object v0

    .line 459679
    if-nez v0, :cond_1

    .line 459680
    const/4 v0, 0x0

    goto :goto_0

    .line 459681
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->b:LX/1Ad;

    iget-object v2, p1, LX/AmL;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 459682
    iput-object v0, p2, LX/AmM;->a:LX/1aZ;

    goto :goto_0
.end method

.method private static a(LX/AmL;LX/1bf;)LX/1bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AmL",
            "<TV;>;",
            "LX/1bf;",
            ")",
            "LX/1bf;"
        }
    .end annotation

    .prologue
    .line 459672
    if-nez p1, :cond_0

    .line 459673
    iget-object v0, p0, LX/AmL;->b:LX/AmK;

    invoke-interface {v0}, LX/AmK;->a()LX/1bf;

    move-result-object p1

    .line 459674
    :cond_0
    return-object p1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;
    .locals 5

    .prologue
    .line 459659
    sget-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    if-nez v0, :cond_1

    .line 459660
    const-class v1, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    monitor-enter v1

    .line 459661
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 459662
    if-eqz v2, :cond_0

    .line 459663
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 459664
    new-instance p0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;-><init>(LX/1Ad;LX/1HI;)V

    .line 459665
    move-object v0, p0

    .line 459666
    sput-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459667
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 459668
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 459669
    :cond_1
    sget-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->d:Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;

    return-object v0

    .line 459670
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 459671
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/AmL;LX/AmM;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AmL",
            "<TV;>;",
            "LX/AmM",
            "<TV;>;TV;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 459654
    iget-object v0, p1, LX/AmM;->c:Ljava/lang/Object;

    if-eq p2, v0, :cond_0

    .line 459655
    sget-object v0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a:Ljava/lang/String;

    const-string v1, "Attempted to unbind a wrong view! Bound: %s, Attempted: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/AmM;->c:Ljava/lang/Object;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459656
    :goto_0
    return-void

    .line 459657
    :cond_0
    iput-object v1, p1, LX/AmM;->c:Ljava/lang/Object;

    .line 459658
    iget-object v0, p0, LX/AmL;->b:LX/AmK;

    invoke-interface {v0, p2, v1}, LX/AmK;->a(Landroid/view/View;LX/1aZ;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AmL",
            "<TV;>;",
            "LX/AmM",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 459650
    iget-object v0, p2, LX/AmM;->b:LX/1bf;

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/AmL;LX/1bf;)LX/1bf;

    move-result-object v0

    iput-object v0, p2, LX/AmM;->b:LX/1bf;

    .line 459651
    iget-object v0, p2, LX/AmM;->b:LX/1bf;

    if-eqz v0, :cond_0

    .line 459652
    iget-object v0, p0, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->c:LX/1HI;

    iget-object v1, p2, LX/AmM;->b:LX/1bf;

    iget-object v2, p1, LX/AmL;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 459653
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 459644
    check-cast p2, LX/AmL;

    .line 459645
    new-instance v0, LX/AmM;

    invoke-direct {v0}, LX/AmM;-><init>()V

    .line 459646
    iget-boolean v1, p2, LX/AmL;->c:Z

    if-eqz v1, :cond_0

    .line 459647
    invoke-static {p0, p2, v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->b(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)V

    .line 459648
    :cond_0
    invoke-static {p0, p2, v0}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)LX/1aZ;

    move-result-object v1

    iput-object v1, v0, LX/AmM;->a:LX/1aZ;

    .line 459649
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xfc3d3d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 459633
    check-cast p1, LX/AmL;

    check-cast p2, LX/AmM;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 459634
    iget-object v1, p2, LX/AmM;->c:Ljava/lang/Object;

    if-ne p4, v1, :cond_0

    .line 459635
    sget-object v1, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a:Ljava/lang/String;

    const-string v2, "Attempted to bind the same view twice! View: %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p4, v4, v6

    invoke-static {v1, v2, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459636
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x50b7521e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 459637
    :cond_0
    iget-object v1, p2, LX/AmM;->c:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 459638
    sget-object v1, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a:Ljava/lang/String;

    const-string v2, "Attempted to bind already bound view! Previous: %s, New: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p2, LX/AmM;->c:Ljava/lang/Object;

    aput-object v5, v4, v6

    aput-object p4, v4, v7

    invoke-static {v1, v2, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459639
    iget-object v1, p2, LX/AmM;->c:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {p1, p2, v1}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/AmL;LX/AmM;Landroid/view/View;)V

    .line 459640
    :cond_1
    iget-boolean v1, p1, LX/AmL;->d:Z

    if-eqz v1, :cond_2

    .line 459641
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->b(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)V

    .line 459642
    :cond_2
    iput-object p4, p2, LX/AmM;->c:Ljava/lang/Object;

    .line 459643
    iget-object v1, p1, LX/AmL;->b:LX/AmK;

    invoke-static {p0, p1, p2}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;LX/AmL;LX/AmM;)LX/1aZ;

    move-result-object v2

    invoke-interface {v1, p4, v2}, LX/AmK;->a(Landroid/view/View;LX/1aZ;)V

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 459632
    check-cast p1, LX/AmL;

    check-cast p2, LX/AmM;

    invoke-static {p1, p2, p4}, Lcom/facebook/feed/rows/util/DraweeControllerPartDefinition;->a(LX/AmL;LX/AmM;Landroid/view/View;)V

    return-void
.end method
