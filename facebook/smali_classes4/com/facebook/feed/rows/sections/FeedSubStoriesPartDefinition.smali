.class public Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

.field public final b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field public final c:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field public final f:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

.field public final g:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            "Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;",
            "Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498256
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 498257
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 498258
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 498259
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 498260
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 498261
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->f:Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    .line 498262
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->g:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 498263
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    .line 498264
    move-object v0, p8

    .line 498265
    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->h:LX/0Ot;

    .line 498266
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;
    .locals 12

    .prologue
    .line 498267
    const-class v1, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    monitor-enter v1

    .line 498268
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498269
    sput-object v2, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498270
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498271
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498272
    new-instance v3, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    const/16 v11, 0x74e

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;LX/0Ot;)V

    .line 498273
    move-object v0, v3

    .line 498274
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498275
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498276
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 498278
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498279
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    new-instance v1, LX/36I;

    .line 498280
    new-instance v2, LX/36J;

    invoke-direct {v2, p0}, LX/36J;-><init>(Lcom/facebook/feed/rows/sections/FeedSubStoriesPartDefinition;)V

    move-object v2, v2

    .line 498281
    invoke-direct {v1, p2, v2}, LX/36I;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/36K;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498282
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498283
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498284
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 498285
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 498286
    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
