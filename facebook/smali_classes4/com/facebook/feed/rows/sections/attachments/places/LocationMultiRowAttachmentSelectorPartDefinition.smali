.class public Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527507
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 527508
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->a:LX/0Ot;

    .line 527509
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->b:LX/0Ot;

    .line 527510
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->c:LX/0Ot;

    .line 527511
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->d:LX/0Ot;

    .line 527512
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;
    .locals 7

    .prologue
    .line 527513
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 527514
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 527515
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 527516
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527517
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 527518
    new-instance v3, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;

    const/16 v4, 0x91c

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x91e

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x91f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x920

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 527519
    move-object v0, v3

    .line 527520
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 527521
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527522
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 527523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 527524
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 527525
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->d:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 527526
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 527527
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 527528
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 527529
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
