.class public Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# instance fields
.field private final d:LX/1We;

.field private final e:LX/1Wm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wm",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1Wn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wn",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

.field private final h:Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

.field private final i:LX/1Wi;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1We;LX/1Wm;LX/1Wn;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;LX/0ad;LX/1Wi;)V
    .locals 0
    .param p8    # LX/1Wi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498554
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 498555
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->d:LX/1We;

    .line 498556
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->e:LX/1Wm;

    .line 498557
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->f:LX/1Wn;

    .line 498558
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->g:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 498559
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    .line 498560
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->c:LX/0ad;

    .line 498561
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->i:LX/1Wi;

    .line 498562
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 498549
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->d:LX/1We;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->i:LX/1Wi;

    invoke-virtual {v0, v1}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    .line 498550
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->g:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;)I

    move-result v1

    .line 498551
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->f:LX/1Wn;

    invoke-virtual {v2, p1}, LX/1Wn;->c(LX/1De;)LX/39X;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/39X;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39X;

    move-result-object v2

    check-cast p3, LX/1Po;

    invoke-virtual {v2, p3}, LX/39X;->a(LX/1Po;)LX/39X;

    move-result-object v2

    iget-object v3, v0, LX/1Wj;->f:LX/1Wk;

    invoke-virtual {v2, v3}, LX/39X;->a(LX/1Wk;)LX/39X;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/39X;->h(I)LX/39X;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 498552
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->e:LX/1Wm;

    invoke-virtual {v2, p1}, LX/1Wm;->c(LX/1De;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/39Z;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/39Z;->a(LX/1Wj;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/39Z;->a(LX/1X1;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 498548
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 498553
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498546
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498547
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 498545
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
