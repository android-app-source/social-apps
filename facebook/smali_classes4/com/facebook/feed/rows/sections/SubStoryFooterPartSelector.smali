.class public Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

.field private final d:LX/14w;


# direct methods
.method public constructor <init>(LX/14w;Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;LX/36G;LX/36H;LX/1Wi;)V
    .locals 1
    .param p6    # LX/1Wi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498436
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 498437
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->d:LX/14w;

    .line 498438
    invoke-virtual {p5, p6}, LX/36H;->a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->a:Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;

    .line 498439
    invoke-virtual {p4, p2, p6}, LX/36G;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->b:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    .line 498440
    invoke-virtual {p4, p3, p6}, LX/36G;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->c:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    .line 498441
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 498442
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498443
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->a:Lcom/facebook/feed/rows/sections/SubStoryReactionsFooterComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->b:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;->c:Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 498444
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498445
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498446
    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
