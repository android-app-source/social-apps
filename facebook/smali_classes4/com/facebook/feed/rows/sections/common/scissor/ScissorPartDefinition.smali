.class public Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366128
    new-instance v0, LX/23v;

    invoke-direct {v0}, LX/23v;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366129
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 366130
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;
    .locals 3

    .prologue
    .line 366131
    const-class v1, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;

    monitor-enter v1

    .line 366132
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366133
    sput-object v2, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366134
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366135
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 366136
    new-instance v0, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;-><init>()V

    .line 366137
    move-object v0, v0

    .line 366138
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366139
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366140
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 366142
    sget-object v0, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 366143
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 366144
    const-string v1, "Head_Fetch_Gap"

    .line 366145
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 366146
    check-cast v0, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;

    .line 366147
    iget-object p0, v0, Lcom/facebook/feed/model/GapFeedEdge$GapFeedUnit;->a:Ljava/lang/String;

    move-object v0, p0

    .line 366148
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
