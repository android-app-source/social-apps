.class public Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/36I;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:LX/0ad;

.field private final c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

.field private final e:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

.field private final f:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

.field private final g:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

.field private final h:Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

.field private final i:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;


# direct methods
.method public constructor <init>(LX/14w;LX/0ad;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;LX/36E;Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498328
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 498329
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a:LX/14w;

    .line 498330
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->b:LX/0ad;

    .line 498331
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 498332
    sget-object v0, LX/1Wi;->SUB_STORY:LX/1Wi;

    invoke-virtual {p5, v0}, LX/36E;->a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    .line 498333
    sget-object v0, LX/1Wi;->LAST_SUB_STORY:LX/1Wi;

    invoke-virtual {p5, v0}, LX/36E;->a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    .line 498334
    sget-object v0, LX/1Wi;->SUB_STORY_BOX_WITH_COMMENTS:LX/1Wi;

    invoke-virtual {p5, v0}, LX/36E;->a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    .line 498335
    sget-object v0, LX/1Wi;->SUB_STORY_BOX_WITHOUT_COMMENTS:LX/1Wi;

    invoke-virtual {p5, v0}, LX/36E;->a(LX/1Wi;)Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    .line 498336
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    .line 498337
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->i:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    .line 498338
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;
    .locals 10

    .prologue
    .line 498317
    const-class v1, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    monitor-enter v1

    .line 498318
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498319
    sput-object v2, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498322
    new-instance v3, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    const-class v8, LX/36E;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/36E;

    invoke-static {v0}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;-><init>(LX/14w;LX/0ad;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;LX/36E;Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;)V

    .line 498323
    move-object v0, v3

    .line 498324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 498315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-nez v1, :cond_1

    .line 498316
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v1

    invoke-static {v1}, LX/14w;->a(LX/1WQ;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 498287
    check-cast p2, LX/36I;

    check-cast p3, LX/1Pf;

    const/4 v3, 0x0

    .line 498288
    iget-object v0, p2, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498289
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 498290
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 498291
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    .line 498292
    add-int/lit8 v5, v4, -0x1

    .line 498293
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a:LX/14w;

    iget-object v2, p2, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/14w;->j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    move v2, v3

    .line 498294
    :goto_0
    if-ge v2, v4, :cond_4

    .line 498295
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 498296
    iget-object v7, p2, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v7, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 498297
    iget-object v8, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->i:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    invoke-virtual {p1, v8, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 498298
    iget-object v8, p2, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v8, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 498299
    iget-object v8, p2, LX/36I;->b:LX/36K;

    invoke-interface {v8, p1, v1}, LX/36K;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 498300
    if-eqz v6, :cond_2

    .line 498301
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {v1, v7}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->b:LX/0ad;

    sget-short v8, LX/0wi;->a:S

    invoke-interface {v1, v8, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 498302
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    invoke-virtual {p1, v1, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498303
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v1, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498304
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 498305
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    invoke-virtual {p1, v1, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 498306
    :cond_2
    if-ne v2, v5, :cond_3

    .line 498307
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    invoke-virtual {p1, v1, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 498308
    :cond_3
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/SubStoryFooterPartSelector;

    invoke-virtual {p1, v1, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 498309
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->h:Lcom/facebook/feedplugins/graphqlstory/footer/components/SeeMoreFooterComponentPartDefinition;

    iget-object v1, p2, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 498310
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498311
    check-cast p1, LX/36I;

    .line 498312
    iget-object v0, p1, LX/36I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498313
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 498314
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
