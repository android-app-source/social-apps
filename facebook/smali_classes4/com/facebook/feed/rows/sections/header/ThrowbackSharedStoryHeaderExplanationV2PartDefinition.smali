.class public Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final g:LX/1Ua;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3ed00000    # -11.0f

    .line 480143
    const v0, 0x7f030126

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a:LX/1Cz;

    .line 480144
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 480145
    iput v1, v0, LX/1UY;->d:F

    .line 480146
    move-object v0, v0

    .line 480147
    iput v1, v0, LX/1UY;->c:F

    .line 480148
    move-object v0, v0

    .line 480149
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->g:LX/1Ua;

    .line 480150
    const-class v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480136
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 480137
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 480138
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 480139
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 480140
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 480141
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->f:LX/17W;

    .line 480142
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;
    .locals 9

    .prologue
    .line 480125
    const-class v1, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;

    monitor-enter v1

    .line 480126
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480127
    sput-object v2, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480130
    new-instance v3, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V

    .line 480131
    move-object v0, v3

    .line 480132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryHeader;)Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 480151
    if-nez p0, :cond_0

    move-object v0, v1

    .line 480152
    :goto_0
    return-object v0

    .line 480153
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v3

    .line 480154
    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 480155
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 480156
    if-eqz v0, :cond_1

    const-string v5, "GoodwillThrowbackSharedStoryHeaderStyleInfo"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480157
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    goto :goto_0

    .line 480158
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 480159
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 480124
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 480100
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480101
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480102
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    .line 480103
    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryHeader;)Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    move-result-object v1

    .line 480104
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 480105
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 480106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 480107
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 480108
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 480109
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 480110
    const v5, 0x7f0d05d3

    iget-object v6, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v7, LX/2f8;

    invoke-direct {v7}, LX/2f8;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v7

    sget-object v8, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 480111
    iput-object v8, v7, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 480112
    move-object v7, v7

    .line 480113
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    invoke-virtual {v7, v8, v0}, LX/2f8;->a(II)LX/2f8;

    move-result-object v0

    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v5, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480114
    const v0, 0x7f0d05d4

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v6, LX/2f8;

    invoke-direct {v6}, LX/2f8;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v6

    sget-object v7, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 480115
    iput-object v7, v6, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 480116
    move-object v6, v6

    .line 480117
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-virtual {v6, v7, v1}, LX/2f8;->a(II)LX/2f8;

    move-result-object v1

    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v0, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480118
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v5, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->g:LX/1Ua;

    invoke-direct {v1, p2, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480119
    const v0, 0x7f0d02c4

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480120
    const v0, 0x7f0d02c3

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480121
    if-eqz v4, :cond_0

    .line 480122
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/BsJ;

    invoke-direct {v1, p0, v4}, LX/BsJ;-><init>(Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480123
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7cb27c8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 480091
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 480092
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 480093
    iget-object v4, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 480094
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryHeader;)Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    move-result-object v4

    .line 480095
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 480096
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v4

    long-to-int v5, v4

    .line 480097
    const v4, 0x7f0d05d2

    invoke-virtual {p4, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 480098
    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v4, v6}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 480099
    const/16 v1, 0x1f

    const v2, -0x5aea751d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 480085
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480086
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480087
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480088
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    .line 480089
    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryHeader;)Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    move-result-object v1

    .line 480090
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY_V2:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, LX/182;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
