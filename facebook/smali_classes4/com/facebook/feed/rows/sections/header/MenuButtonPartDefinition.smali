.class public Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/24b;",
        "Landroid/view/View$OnClickListener;",
        "LX/1Pk;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364076
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 364077
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;
    .locals 3

    .prologue
    .line 364065
    const-class v1, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    monitor-enter v1

    .line 364066
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364067
    sput-object v2, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364068
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364069
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 364070
    new-instance v0, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;-><init>()V

    .line 364071
    move-object v0, v0

    .line 364072
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364073
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364074
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/View;LX/1Pk;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 364051
    invoke-interface {p1}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 364052
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 364060
    check-cast p2, LX/24b;

    check-cast p3, LX/1Pk;

    const/4 v0, 0x0

    .line 364061
    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v1

    if-nez v1, :cond_1

    .line 364062
    :cond_0
    :goto_0
    return-object v0

    .line 364063
    :cond_1
    iget-object v1, p2, LX/24b;->b:LX/1dl;

    sget-object v2, LX/1dl;->CLICKABLE:LX/1dl;

    if-ne v1, v2, :cond_0

    .line 364064
    new-instance v0, LX/24c;

    invoke-direct {v0, p0, p3, p2}, LX/24c;-><init>(Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1Pk;LX/24b;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7a53ee0c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364055
    check-cast p1, LX/24b;

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 364056
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364057
    iget-object v1, p1, LX/24b;->b:LX/1dl;

    sget-object v2, LX/1dl;->HIDDEN:LX/1dl;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364058
    const/16 v1, 0x1f

    const v2, 0x5d13f79c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 364059
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 364053
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364054
    return-void
.end method
