.class public Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final h:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x3ed00000    # -11.0f

    .line 480295
    const v0, 0x7f030125

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->a:LX/1Cz;

    .line 480296
    const-class v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 480297
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 480298
    iput v1, v0, LX/1UY;->b:F

    .line 480299
    move-object v0, v0

    .line 480300
    iput v2, v0, LX/1UY;->c:F

    .line 480301
    move-object v0, v0

    .line 480302
    iput v2, v0, LX/1UY;->d:F

    .line 480303
    move-object v0, v0

    .line 480304
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480305
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 480306
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 480307
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 480308
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 480309
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 480310
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->h:LX/17W;

    .line 480311
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;
    .locals 9

    .prologue
    .line 480312
    const-class v1, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;

    monitor-enter v1

    .line 480313
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480314
    sput-object v2, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480315
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480316
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480317
    new-instance v3, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V

    .line 480318
    move-object v0, v3

    .line 480319
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480320
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480321
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480322
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 480323
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 480324
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480325
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480326
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480327
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 480328
    const v2, 0x7f0d02c4

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480329
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 480330
    const v2, 0x7f0d02c3

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480331
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 480332
    const v2, 0x7f0d05d1

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v4, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 480333
    iput-object v4, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 480334
    move-object v1, v1

    .line 480335
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 480336
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->c:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480337
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 480338
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/BsI;

    invoke-direct {v2, p0, v0}, LX/BsI;-><init>(Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 480339
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 480340
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480341
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480342
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480343
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    .line 480344
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/182;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
