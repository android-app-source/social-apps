.class public Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final d:LX/32K;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/2y0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2y0",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/1VF;

.field private final h:LX/1VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 489777
    new-instance v0, LX/32K;

    invoke-direct {v0}, LX/32K;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->d:LX/32K;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2y0;LX/1V0;LX/1VF;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 489803
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 489804
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->e:LX/2y0;

    .line 489805
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->f:LX/1V0;

    .line 489806
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->g:LX/1VF;

    .line 489807
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->h:LX/1VI;

    .line 489808
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 489800
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->e:LX/2y0;

    invoke-virtual {v0, p1}, LX/2y0;->c(LX/1De;)LX/3gF;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/3gF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/3gF;

    move-result-object v1

    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-virtual {v1, v0}, LX/3gF;->a(LX/1Pq;)LX/3gF;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->h:LX/1VI;

    invoke-virtual {v0}, LX/1VI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/2ei;->SUGGESTED_CONTENT_SUTRO:LX/2ei;

    :goto_0
    invoke-virtual {v1, v0}, LX/3gF;->a(LX/2ei;)LX/3gF;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 489801
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {p2}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;

    move-result-object v3

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 489802
    :cond_0
    sget-object v0, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;
    .locals 9

    .prologue
    .line 489789
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;

    monitor-enter v1

    .line 489790
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 489791
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 489792
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489793
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 489794
    new-instance v3, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2y0;->a(LX/0QB;)LX/2y0;

    move-result-object v5

    check-cast v5, LX/2y0;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v7

    check-cast v7, LX/1VF;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v8

    check-cast v8, LX/1VI;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;-><init>(Landroid/content/Context;LX/2y0;LX/1V0;LX/1VF;LX/1VI;)V

    .line 489795
    move-object v0, v3

    .line 489796
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 489797
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489798
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 489799
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 489788
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 489787
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 489782
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489783
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489784
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 489785
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489786
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 489780
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489781
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 489779
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 489778
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->d:LX/32K;

    return-object v0
.end method
