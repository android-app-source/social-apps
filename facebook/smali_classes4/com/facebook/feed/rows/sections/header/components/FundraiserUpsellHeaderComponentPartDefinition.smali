.class public Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final d:LX/32I;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/32J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/32J",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 489605
    new-instance v0, LX/32I;

    invoke-direct {v0}, LX/32I;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->d:LX/32I;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/32J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 489606
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 489607
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->e:LX/1V0;

    .line 489608
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->f:LX/32J;

    .line 489609
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 489610
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->f:LX/32J;

    const/4 v1, 0x0

    .line 489611
    new-instance v2, LX/Bsc;

    invoke-direct {v2, v0}, LX/Bsc;-><init>(LX/32J;)V

    .line 489612
    iget-object v3, v0, LX/32J;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bsa;

    .line 489613
    if-nez v3, :cond_0

    .line 489614
    new-instance v3, LX/Bsa;

    invoke-direct {v3, v0}, LX/Bsa;-><init>(LX/32J;)V

    .line 489615
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bsa;->a$redex0(LX/Bsa;LX/1De;IILX/Bsc;)V

    .line 489616
    move-object v2, v3

    .line 489617
    move-object v1, v2

    .line 489618
    move-object v0, v1

    .line 489619
    iget-object v1, v0, LX/Bsa;->a:LX/Bsc;

    iput-object p2, v1, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489620
    iget-object v1, v0, LX/Bsa;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 489621
    move-object v0, v0

    .line 489622
    iget-object v1, v0, LX/Bsa;->a:LX/Bsc;

    iput-object p3, v1, LX/Bsc;->c:LX/1Pn;

    .line 489623
    iget-object v1, v0, LX/Bsa;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 489624
    move-object v0, v0

    .line 489625
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 489626
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 489627
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    monitor-enter v1

    .line 489628
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 489629
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 489630
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489631
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 489632
    new-instance p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/32J;->a(LX/0QB;)LX/32J;

    move-result-object v5

    check-cast v5, LX/32J;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/32J;)V

    .line 489633
    move-object v0, p0

    .line 489634
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 489635
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489636
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 489637
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 489638
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 489639
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 489640
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489641
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489642
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 489643
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489644
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 489645
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489646
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FUNDRAISER_UPSELL:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489647
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489648
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    .line 489649
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 489650
    if-eqz v1, :cond_1

    .line 489651
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 489652
    if-eqz v1, :cond_1

    const/4 p0, 0x0

    .line 489653
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_5

    const p1, 0x607985d5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne p1, v1, :cond_5

    .line 489654
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    .line 489655
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    .line 489656
    :goto_2
    move v1, v1

    .line 489657
    if-eqz v1, :cond_1

    .line 489658
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 489659
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 489660
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_4
    move v0, v1

    .line 489661
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_5

    :cond_1
    const/4 v1, 0x0

    goto :goto_4

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    move v1, p0

    .line 489662
    goto :goto_2

    :cond_5
    move v1, p0

    .line 489663
    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 489664
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489665
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 489666
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 489667
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->d:LX/32I;

    return-object v0
.end method
