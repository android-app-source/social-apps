.class public Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/2y1;

.field private final f:LX/1V0;

.field private final g:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 480210
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ed00000    # -11.0f

    .line 480211
    iput v1, v0, LX/1UY;->d:F

    .line 480212
    move-object v0, v0

    .line 480213
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Landroid/content/Context;LX/2y1;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480248
    invoke-direct {p0, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 480249
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->g:LX/0Uh;

    .line 480250
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->e:LX/2y1;

    .line 480251
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->f:LX/1V0;

    .line 480252
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 480234
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->e:LX/2y1;

    const/4 v1, 0x0

    .line 480235
    new-instance v2, LX/Bso;

    invoke-direct {v2, v0}, LX/Bso;-><init>(LX/2y1;)V

    .line 480236
    sget-object v3, LX/2y1;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bsn;

    .line 480237
    if-nez v3, :cond_0

    .line 480238
    new-instance v3, LX/Bsn;

    invoke-direct {v3}, LX/Bsn;-><init>()V

    .line 480239
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bsn;->a$redex0(LX/Bsn;LX/1De;IILX/Bso;)V

    .line 480240
    move-object v2, v3

    .line 480241
    move-object v1, v2

    .line 480242
    move-object v0, v1

    .line 480243
    iget-object v1, v0, LX/Bsn;->a:LX/Bso;

    iput-object p2, v1, LX/Bso;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480244
    iget-object v1, v0, LX/Bsn;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 480245
    move-object v0, v0

    .line 480246
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 480247
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->f:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;
    .locals 7

    .prologue
    .line 480223
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;

    monitor-enter v1

    .line 480224
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480225
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480228
    new-instance p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2y1;->a(LX/0QB;)LX/2y1;

    move-result-object v5

    check-cast v5, LX/2y1;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;-><init>(LX/0Uh;Landroid/content/Context;LX/2y1;LX/1V0;)V

    .line 480229
    move-object v0, p0

    .line 480230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 480222
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 480221
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 480214
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 480215
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentPartDefinition;->g:LX/0Uh;

    const/16 v2, 0x64e

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 480216
    :goto_0
    return v0

    .line 480217
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 480218
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 480219
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    .line 480220
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->THROWBACK_SHARED_STORY:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, LX/182;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
