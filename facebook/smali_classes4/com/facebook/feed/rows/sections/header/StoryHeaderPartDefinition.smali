.class public Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static final e:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field private final f:LX/1xp;

.field private final g:LX/1V0;

.field private final h:LX/0Uh;

.field private final i:LX/1VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 489852
    new-instance v0, LX/32L;

    invoke-direct {v0}, LX/32L;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->d:LX/1Cz;

    .line 489853
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 489854
    iput v1, v0, LX/1UY;->b:F

    .line 489855
    move-object v0, v0

    .line 489856
    const/high16 v1, 0x41400000    # 12.0f

    .line 489857
    iput v1, v0, LX/1UY;->d:F

    .line 489858
    move-object v0, v0

    .line 489859
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->e:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/1xp;LX/0Uh;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 489811
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 489812
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->g:LX/1V0;

    .line 489813
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->f:LX/1xp;

    .line 489814
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->h:LX/0Uh;

    .line 489815
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->i:LX/1VI;

    .line 489816
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 489843
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->f:LX/1xp;

    invoke-virtual {v0, p1}, LX/1xp;->c(LX/1De;)LX/1xt;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1xt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xt;

    move-result-object v1

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-virtual {v1, v0}, LX/1xt;->a(LX/1Po;)LX/1xt;

    move-result-object v0

    const/4 v1, 0x1

    .line 489844
    iget-object v2, v0, LX/1xt;->a:LX/1xr;

    iput-boolean v1, v2, LX/1xr;->c:Z

    .line 489845
    move-object v0, v0

    .line 489846
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->i:LX/1VI;

    invoke-virtual {v1}, LX/1VI;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489847
    const v1, 0x7f0a00aa

    invoke-virtual {v0, v1}, LX/1xt;->h(I)LX/1xt;

    .line 489848
    :cond_0
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 489849
    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->e:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 489850
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 489851
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;
    .locals 9

    .prologue
    .line 489832
    const-class v1, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;

    monitor-enter v1

    .line 489833
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 489834
    sput-object v2, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 489835
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489836
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 489837
    new-instance v3, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1xp;->a(LX/0QB;)LX/1xp;

    move-result-object v6

    check-cast v6, LX/1xp;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v8

    check-cast v8, LX/1VI;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/1xp;LX/0Uh;LX/1VI;)V

    .line 489838
    move-object v0, v3

    .line 489839
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 489840
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489841
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 489842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 489831
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 489830
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 489827
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489828
    invoke-super {p0, p1, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;)V

    .line 489829
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 489821
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 489822
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->h:LX/0Uh;

    const/16 v2, 0x3ae

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489823
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489824
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 489825
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 489826
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 489819
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489820
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 489818
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 489817
    sget-object v0, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
