.class public Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pe;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Void;",
            "LX/1Pe;",
            "TV;>;"
        }
    .end annotation
.end field

.field private final c:LX/1Wi;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Wi;)V
    .locals 0
    .param p2    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Wi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;",
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Void;",
            "LX/1Pe;",
            "TV;>;",
            "LX/1Wi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498563
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 498564
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 498565
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 498566
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->c:LX/1Wi;

    .line 498567
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 498568
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 498569
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498570
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498571
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v1, LX/20d;

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->c:LX/1Wi;

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 498572
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 498573
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498574
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoryFooterPartDefinition;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
