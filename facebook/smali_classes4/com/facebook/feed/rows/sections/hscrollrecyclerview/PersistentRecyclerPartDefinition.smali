.class public Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SubProps:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2eG",
        "<TSubProps;TE;>;",
        "LX/2ed;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/2ds;

.field public final b:LX/2dr;

.field private final c:LX/198;

.field private final d:LX/2dt;

.field private final e:LX/2du;

.field public final f:LX/03V;

.field private final g:LX/1DR;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2dr;LX/2ds;LX/198;LX/2dt;LX/2du;LX/03V;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444513
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 444514
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->b:LX/2dr;

    .line 444515
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a:LX/2ds;

    .line 444516
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->c:LX/198;

    .line 444517
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->d:LX/2dt;

    .line 444518
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->e:LX/2du;

    .line 444519
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->f:LX/03V;

    .line 444520
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->g:LX/1DR;

    .line 444521
    return-void
.end method

.method private static a(LX/1PW;)LX/1Rb;
    .locals 1

    .prologue
    .line 444512
    instance-of v0, p0, LX/1Pu;

    if-eqz v0, :cond_0

    check-cast p0, LX/1Pu;

    invoke-interface {p0}, LX/1Pu;->o()LX/1Rb;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/1Pr;LX/2eK;)LX/25c;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 444503
    iget-object v0, p1, LX/2eK;->b:LX/2eG;

    iget-object v0, v0, LX/2eG;->d:Ljava/lang/String;

    move-object v0, v0

    .line 444504
    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    .line 444505
    iget-object v1, p1, LX/2eK;->b:LX/2eG;

    iget-object v1, v1, LX/2eG;->e:LX/0jW;

    move-object v1, v1

    .line 444506
    invoke-interface {p0, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25c;

    .line 444507
    iget v1, v0, LX/25c;->a:I

    const/4 p0, -0x1

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 444508
    if-eqz v1, :cond_0

    .line 444509
    iget-object v1, p1, LX/2eK;->b:LX/2eG;

    iget v1, v1, LX/2eG;->b:I

    move v1, v1

    .line 444510
    iput v1, v0, LX/25c;->a:I

    .line 444511
    :cond_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .locals 12

    .prologue
    .line 444443
    const-class v1, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    monitor-enter v1

    .line 444444
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444445
    sput-object v2, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444446
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444447
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444448
    new-instance v3, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2dr;->a(LX/0QB;)LX/2dr;

    move-result-object v5

    check-cast v5, LX/2dr;

    invoke-static {v0}, LX/2ds;->a(LX/0QB;)LX/2ds;

    move-result-object v6

    check-cast v6, LX/2ds;

    invoke-static {v0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v7

    check-cast v7, LX/198;

    const-class v8, LX/2dt;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2dt;

    const-class v9, LX/2du;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/2du;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v11

    check-cast v11, LX/1DR;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;-><init>(Landroid/content/Context;LX/2dr;LX/2ds;LX/198;LX/2dt;LX/2du;LX/03V;LX/1DR;)V

    .line 444449
    move-object v0, v3

    .line 444450
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444451
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444452
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 444499
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 444500
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setRecycledViewPool(LX/1YF;)V

    .line 444501
    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 444502
    return-void
.end method

.method private static b(LX/2eK;LX/25c;)I
    .locals 2

    .prologue
    .line 444522
    if-eqz p1, :cond_0

    .line 444523
    iget v0, p1, LX/25c;->a:I

    move v0, v0

    .line 444524
    if-ltz v0, :cond_0

    invoke-virtual {p0}, LX/2eK;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 444525
    :goto_0
    return v0

    .line 444526
    :cond_0
    iget v0, p0, LX/2eK;->e:I

    move v0, v0

    .line 444527
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2eG;LX/1Pr;)LX/2ed;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2eG",
            "<TSubProps;TE;>;TE;)",
            "LX/2ed;"
        }
    .end annotation

    .prologue
    .line 444455
    const-string v0, "HScrollRecyclerViewBinder.prepare"

    const v1, 0x5a9e32f3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 444456
    :try_start_0
    new-instance v8, LX/2eK;

    iget-object v0, p1, LX/2eG;->c:LX/2eJ;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->f:LX/03V;

    invoke-direct {v8, v0, p1, v1}, LX/2eK;-><init>(LX/2eJ;LX/2eG;LX/03V;)V

    .line 444457
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->e:LX/2du;

    .line 444458
    new-instance v1, LX/2eL;

    const/16 v2, 0x259

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v8, v2}, LX/2eL;-><init>(LX/2eK;LX/0Ot;)V

    .line 444459
    move-object v9, v1

    .line 444460
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->d:LX/2dt;

    .line 444461
    new-instance v1, LX/2eM;

    invoke-direct {v1, p0, p1}, LX/2eM;-><init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2eG;)V

    move-object v1, v1

    .line 444462
    new-instance v3, LX/2eO;

    invoke-static {v0}, LX/2ds;->a(LX/0QB;)LX/2ds;

    move-result-object v2

    check-cast v2, LX/2ds;

    invoke-direct {v3, v2, v1, v9}, LX/2eO;-><init>(LX/2ds;LX/2eN;LX/2eL;)V

    .line 444463
    move-object v1, v3

    .line 444464
    iget-object v0, p1, LX/2eG;->d:Ljava/lang/String;

    .line 444465
    iget-object v2, v1, LX/2eO;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 444466
    invoke-virtual {v9}, LX/2eL;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 444467
    iget-object v0, v8, LX/2eK;->a:LX/2eJ;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 444468
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->f:LX/03V;

    const-string v3, "HScrollRecyclerView pageitems have 0 item"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 444469
    :cond_0
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/1PW;)LX/1Rb;

    move-result-object v0

    .line 444470
    iput-object v0, v9, LX/2eL;->d:LX/1Rb;

    .line 444471
    iput-object p2, v9, LX/2eL;->e:LX/1PW;

    .line 444472
    invoke-static {p2, v8}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/1Pr;LX/2eK;)LX/25c;

    move-result-object v6

    .line 444473
    invoke-virtual {v8}, LX/2eK;->e()I

    move-result v2

    .line 444474
    iget v0, v8, LX/2eK;->e:I

    move v3, v0

    .line 444475
    add-int/lit8 v0, v3, 0x3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v0, v3

    .line 444476
    :goto_0
    if-ge v0, v4, :cond_1

    .line 444477
    invoke-virtual {v9, v0}, LX/2eL;->c(I)V

    .line 444478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 444479
    :cond_1
    check-cast p2, LX/1Pn;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p1, LX/2eG;->f:I

    move-object v0, p0

    .line 444480
    new-instance v7, LX/2eX;

    invoke-direct {v7, v0, v4}, LX/2eX;-><init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Landroid/content/Context;)V

    .line 444481
    add-int/lit8 v10, v3, 0x2

    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 444482
    :goto_1
    if-ge v3, v10, :cond_4

    .line 444483
    invoke-virtual {v1, v3}, LX/1OM;->getItemViewType(I)I

    move-result v11

    .line 444484
    iget-object p1, v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a:LX/2ds;

    invoke-virtual {p1, v11}, LX/2ds;->a(I)LX/2eR;

    move-result-object p1

    .line 444485
    const/4 p2, -0x1

    if-eq v5, p2, :cond_2

    .line 444486
    iget-object p2, v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a:LX/2ds;

    .line 444487
    iget-object v2, p2, LX/2ds;->d:LX/2dr;

    invoke-virtual {p2, p1}, LX/2ds;->a(LX/2eR;)I

    move-result v4

    invoke-virtual {v2, v4, v5}, LX/1YF;->a(II)V

    .line 444488
    :cond_2
    iget-object p2, v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a:LX/2ds;

    .line 444489
    invoke-static {p2, p1}, LX/2ds;->d(LX/2ds;LX/2eR;)I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_5

    const/4 v2, 0x1

    :goto_2
    move p2, v2

    .line 444490
    if-eqz p2, :cond_3

    .line 444491
    invoke-virtual {v1, v7, v11}, LX/1OM;->b(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v11

    .line 444492
    iget-object p2, v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->b:LX/2dr;

    invoke-virtual {p2, v11}, LX/1YF;->a(LX/1a1;)V

    .line 444493
    iget-object v11, v0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a:LX/2ds;

    .line 444494
    iget-object p2, v11, LX/2ds;->c:Ljava/util/HashMap;

    invoke-interface {p1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v11, p1}, LX/2ds;->d(LX/2ds;LX/2eR;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444495
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 444496
    :cond_4
    new-instance v0, LX/2eb;

    invoke-direct {v0, p0, v6, v8}, LX/2eb;-><init>(Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/25c;LX/2eK;)V

    move-object v7, v0

    .line 444497
    new-instance v2, LX/2ed;

    move-object v3, v8

    move-object v4, v9

    move-object v5, v1

    invoke-direct/range {v2 .. v7}, LX/2ed;-><init>(LX/2eK;LX/2eL;LX/2eO;LX/25c;LX/2ec;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444498
    const v0, 0x348eed1c

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v2

    :catchall_0
    move-exception v0

    const v1, 0x13edd71e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_5
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 444454
    check-cast p2, LX/2eG;

    check-cast p3, LX/1Pr;

    invoke-virtual {p0, p2, p3}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/2eG;LX/1Pr;)LX/2ed;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2eG;LX/2ed;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2eG",
            "<TSubProps;TE;>;",
            "LX/2ed;",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 444423
    const-string v0, "HScrollRecyclerViewBinder.bind"

    const v1, -0x5bee0a10

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 444424
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->c:LX/198;

    invoke-static {}, LX/25e;->c()LX/25e;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/198;->c(LX/1YD;)V

    .line 444425
    iget-object v0, p1, LX/2eG;->a:LX/2eF;

    invoke-virtual {v0, p3}, LX/2eF;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 444426
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->b:LX/2dr;

    invoke-virtual {p3, v0}, Landroid/support/v7/widget/RecyclerView;->setRecycledViewPool(LX/1YF;)V

    .line 444427
    iget-object v0, p2, LX/2ed;->c:LX/2eO;

    invoke-virtual {p3, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 444428
    iget-object v0, p2, LX/2ed;->e:LX/2ec;

    .line 444429
    iput-object v0, p3, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 444430
    iget-object v0, p1, LX/2eG;->a:LX/2eF;

    invoke-virtual {v0}, LX/2eF;->a()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->g:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 444431
    iget-boolean v0, p3, LX/25R;->h:Z

    move v0, v0

    .line 444432
    if-eqz v0, :cond_0

    .line 444433
    iget-object v0, p2, LX/2ed;->a:LX/2eK;

    iget-object v1, p2, LX/2ed;->d:LX/25c;

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->b(LX/2eK;LX/25c;)I

    move-result v0

    invoke-virtual {p3, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setCurrentPosition(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444434
    :goto_0
    const v0, -0x59cf49af

    invoke-static {v0}, LX/02m;->a(I)V

    .line 444435
    return-void

    .line 444436
    :cond_0
    :try_start_1
    iget-object v0, p2, LX/2ed;->a:LX/2eK;

    iget-object v1, p2, LX/2ed;->d:LX/25c;

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->b(LX/2eK;LX/25c;)I

    move-result v0

    iget-object v1, p2, LX/2ed;->d:LX/25c;

    .line 444437
    if-eqz v1, :cond_1

    .line 444438
    iget p0, v1, LX/25c;->b:I

    move p0, p0

    .line 444439
    if-eqz p0, :cond_1

    .line 444440
    :goto_1
    move v1, p0

    .line 444441
    invoke-virtual {p3, v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->i(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 444442
    :catchall_0
    move-exception v0

    const v1, 0x5224ba35

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4ce901c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 444422
    check-cast p1, LX/2eG;

    check-cast p2, LX/2ed;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1, p2, p4}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/2eG;LX/2ed;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    const/16 v1, 0x1f

    const v2, -0x7988c186

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 444421
    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {p4}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method
