.class public Lcom/facebook/feed/rows/views/ContentTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements LX/2dX;
.implements LX/2dY;
.implements LX/0wO;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStory;

.field private c:LX/1cr;

.field private d:Z

.field private e:LX/1zR;

.field private f:LX/2db;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 443315
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 443316
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 443317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 443332
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 443333
    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/rows/views/ContentTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 443334
    return-void
.end method

.method private static a(Landroid/util/AttributeSet;)I
    .locals 3
    .param p0    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443329
    if-eqz p0, :cond_0

    .line 443330
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "textColor"

    const v2, 0x7f0a0158

    invoke-interface {p0, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 443331
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a0158

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 443318
    const-class v0, Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-static {v0, p0}, Lcom/facebook/feed/rows/views/ContentTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 443319
    invoke-virtual {p0, v2}, Lcom/facebook/feed/rows/views/ContentTextView;->setIncludeFontPadding(Z)V

    .line 443320
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setHighlightColor(I)V

    .line 443321
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextSize(IF)V

    .line 443322
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/rows/views/ContentTextView;->setLineSpacing(FF)V

    .line 443323
    invoke-virtual {p0}, Lcom/facebook/feed/rows/views/ContentTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/feed/rows/views/ContentTextView;->a(Landroid/util/AttributeSet;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextColor(I)V

    .line 443324
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 443325
    new-instance v0, LX/2dZ;

    invoke-direct {v0, p0}, LX/2dZ;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->c:LX/1cr;

    .line 443326
    iput-boolean v2, p0, Lcom/facebook/feed/rows/views/ContentTextView;->d:Z

    .line 443327
    new-instance v0, LX/2db;

    invoke-virtual {p0}, Lcom/facebook/feed/rows/views/ContentTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2db;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->f:LX/2db;

    .line 443328
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->a:LX/03V;

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 443311
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    if-eqz v0, :cond_0

    .line 443312
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    invoke-virtual {v0, v1}, LX/1zR;->a(LX/2dY;)V

    .line 443313
    :cond_0
    iput-object v1, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    .line 443314
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 443335
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->f:LX/2db;

    invoke-virtual {v0}, LX/2db;->a()V

    .line 443336
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 443308
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->c:LX/1cr;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 443309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->d:Z

    .line 443310
    return-void
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 443305
    iget-boolean v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->c:LX/1cr;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443306
    const/4 v0, 0x1

    .line 443307
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x46233a94

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 443281
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443282
    :goto_0
    const v0, 0x3e816c77

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 443283
    :catch_0
    move-exception v2

    .line 443284
    iget-object v3, p0, Lcom/facebook/feed/rows/views/ContentTextView;->a:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "ContentTextView"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    const-string v0, "_withZombie"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "JellyBean measure bug with zombie: %s"

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v6, v7

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 443285
    iput-object v2, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 443286
    move-object v0, v0

    .line 443287
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/03V;->a(LX/0VG;)V

    .line 443288
    invoke-virtual {p0}, Lcom/facebook/feed/rows/views/ContentTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setText(Ljava/lang/CharSequence;)V

    .line 443289
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V

    goto :goto_0

    .line 443290
    :cond_0
    const-string v0, ""

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 443302
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/text/BetterTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 443303
    invoke-direct {p0}, Lcom/facebook/feed/rows/views/ContentTextView;->d()V

    .line 443304
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x6af88518

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 443301
    iget-object v2, p0, Lcom/facebook/feed/rows/views/ContentTextView;->f:LX/2db;

    invoke-virtual {v2, p1}, LX/2db;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    const v2, 0x1cd4d36a

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCopyTextGestureListener(LX/BtK;)V
    .locals 1

    .prologue
    .line 443299
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->f:LX/2db;

    invoke-virtual {v0, p1}, LX/2db;->setCopyTextGestureListener(LX/BtK;)V

    .line 443300
    return-void
.end method

.method public setDebugInfo(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443297
    iput-object p1, p0, Lcom/facebook/feed/rows/views/ContentTextView;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 443298
    return-void
.end method

.method public setSpannable(Landroid/text/Spannable;)V
    .locals 1

    .prologue
    .line 443291
    if-nez p1, :cond_1

    .line 443292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    .line 443293
    :cond_0
    :goto_0
    return-void

    .line 443294
    :cond_1
    instance-of v0, p1, LX/1zR;

    if-eqz v0, :cond_0

    .line 443295
    check-cast p1, LX/1zR;

    iput-object p1, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    .line 443296
    iget-object v0, p0, Lcom/facebook/feed/rows/views/ContentTextView;->e:LX/1zR;

    invoke-virtual {v0, p0}, LX/1zR;->a(LX/2dY;)V

    goto :goto_0
.end method
