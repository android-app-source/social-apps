.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aT;",
        ":",
        "LX/1aQ;",
        "E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/24D;",
        "LX/24Q;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/1E1;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2vA;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Rg;

.field public final e:LX/24B;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366657
    const-class v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1E1;LX/0Ot;LX/1Rg;LX/24B;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1E1;",
            "LX/0Ot",
            "<",
            "LX/2vA;",
            ">;",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            "LX/24B;",
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366658
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 366659
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->b:LX/1E1;

    .line 366660
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->c:LX/0Ot;

    .line 366661
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->d:LX/1Rg;

    .line 366662
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->e:LX/24B;

    .line 366663
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->f:LX/0Ot;

    .line 366664
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->g:LX/0Ot;

    .line 366665
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;
    .locals 10

    .prologue
    .line 366666
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    monitor-enter v1

    .line 366667
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366668
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366669
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366670
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366671
    new-instance v3, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v4

    check-cast v4, LX/1E1;

    const/16 v5, 0xfd9

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v6

    check-cast v6, LX/1Rg;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v7

    check-cast v7, LX/24B;

    const/16 v8, 0x509

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x665

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;-><init>(LX/1E1;LX/0Ot;LX/1Rg;LX/24B;LX/0Ot;LX/0Ot;)V

    .line 366672
    move-object v0, v3

    .line 366673
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366674
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366675
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/24D;LX/24Q;Landroid/view/View;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/24D;",
            "LX/24Q;",
            "TV;)V"
        }
    .end annotation

    .prologue
    const v5, 0x3f4ccccd    # 0.8f

    const/16 v1, 0x8

    .line 366677
    iget-object v2, p1, LX/24D;->a:LX/AkL;

    .line 366678
    if-eqz v2, :cond_5

    move-object v0, p3

    .line 366679
    check-cast v0, LX/1aT;

    invoke-interface {v0}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v3

    .line 366680
    check-cast p3, LX/1aT;

    invoke-interface {p3}, LX/1aT;->getShimmerContainer()Lcom/facebook/widget/ShimmerFrameLayout;

    move-result-object v0

    .line 366681
    if-eqz v0, :cond_3

    iget-object v4, p2, LX/24Q;->e:LX/1aZ;

    if-eqz v4, :cond_3

    .line 366682
    iput-object v0, p2, LX/24Q;->f:Lcom/facebook/widget/ShimmerFrameLayout;

    .line 366683
    invoke-virtual {v0}, Lcom/facebook/widget/ShimmerFrameLayout;->b()V

    .line 366684
    iget-object v0, p2, LX/24Q;->e:LX/1aZ;

    invoke-virtual {v3, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 366685
    :goto_0
    iget-boolean v0, p1, LX/24D;->d:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 366686
    iget-object v0, p2, LX/24Q;->c:LX/24L;

    iget-object v1, p1, LX/24D;->c:LX/1RN;

    invoke-interface {v0, v3, v1}, LX/24L;->a(Landroid/view/View;LX/1RN;)V

    .line 366687
    iget-object v0, p1, LX/24D;->c:LX/1RN;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366688
    iget-boolean v0, p2, LX/24Q;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/24Q;->a:LX/24O;

    iget-boolean v0, v0, LX/24O;->b:Z

    if-eqz v0, :cond_1

    .line 366689
    :cond_0
    iget-object v0, p1, LX/24D;->c:LX/1RN;

    iget-object v0, v0, LX/1RN;->c:LX/32e;

    iget-object v0, v0, LX/32e;->a:LX/24P;

    sget-object v1, LX/24P;->MINIMIZED:LX/24P;

    invoke-virtual {v0, v1}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p2, LX/24Q;->a:LX/24O;

    iget-object v0, v0, LX/24O;->a:LX/24P;

    sget-object v1, LX/24P;->MINIMIZED:LX/24P;

    invoke-virtual {v0, v1}, LX/24P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 366690
    if-eqz v0, :cond_2

    .line 366691
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vA;

    iget-object v1, p1, LX/24D;->c:LX/1RN;

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    .line 366692
    if-nez v1, :cond_9

    .line 366693
    :goto_3
    invoke-virtual {v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleX(F)V

    .line 366694
    invoke-virtual {v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleY(F)V

    .line 366695
    invoke-virtual {v3}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p2, LX/24Q;->d:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/ColorFilter;)V

    .line 366696
    :cond_2
    :goto_4
    return-void

    .line 366697
    :cond_3
    invoke-interface {v2}, LX/AkL;->f()Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 366698
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-object v0, p3

    .line 366699
    check-cast v0, LX/1aT;

    invoke-interface {v0}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 366700
    const/4 v2, 0x1

    move v2, v2

    .line 366701
    if-eqz v2, :cond_7

    .line 366702
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 366703
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->b:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 366704
    check-cast p3, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;

    invoke-virtual {p3}, Lcom/facebook/feed/inlinecomposer/InlineComposerV2HeaderView;->c()V

    goto :goto_4

    .line 366705
    :cond_7
    if-eqz v0, :cond_6

    .line 366706
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 366707
    :cond_9
    iget-object v6, v0, LX/2vA;->a:LX/32g;

    .line 366708
    invoke-static {v6, v1}, LX/32g;->l(LX/32g;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 366709
    :goto_6
    goto :goto_3

    .line 366710
    :cond_a
    invoke-static {v1}, LX/32g;->f(Ljava/lang/String;)LX/0Tn;

    move-result-object v8

    .line 366711
    if-nez v8, :cond_b

    .line 366712
    :goto_7
    goto :goto_6

    .line 366713
    :cond_b
    iget-object v9, v6, LX/32g;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v9

    .line 366714
    sget-object v10, LX/0SF;->a:LX/0SF;

    move-object v10, v10

    .line 366715
    invoke-virtual {v10}, LX/0SF;->a()J

    move-result-wide v10

    invoke-interface {v9, v8, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v8

    invoke-interface {v8}, LX/0hN;->commit()V

    goto :goto_7
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 366716
    check-cast p2, LX/24D;

    check-cast p3, LX/1Pr;

    .line 366717
    new-instance v6, LX/24K;

    move-object v0, p3

    check-cast v0, LX/1Qa;

    invoke-direct {v6, p0, v0}, LX/24K;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;LX/1Qa;)V

    .line 366718
    iget-object v0, p2, LX/24D;->c:LX/1RN;

    invoke-static {v0}, LX/24B;->a(LX/1RN;)LX/1KL;

    move-result-object v0

    .line 366719
    iget-object v1, p2, LX/24D;->c:LX/1RN;

    iget-object v2, p2, LX/24D;->e:LX/0jW;

    const/4 v4, 0x0

    .line 366720
    if-nez v1, :cond_1

    move v3, v4

    .line 366721
    :goto_0
    move v7, v3

    .line 366722
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->e:LX/24B;

    iget-object v2, p2, LX/24D;->c:LX/1RN;

    iget-object v3, p2, LX/24D;->e:LX/0jW;

    const-class v4, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    new-instance v5, LX/24M;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->d:LX/1Rg;

    invoke-direct {v5, v1, v6}, LX/24M;-><init>(LX/1Rg;LX/24L;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 366723
    new-instance v1, LX/24N;

    iget-object v0, p2, LX/24D;->a:LX/AkL;

    iget-object v2, p2, LX/24D;->c:LX/1RN;

    iget-object v3, p2, LX/24D;->e:LX/0jW;

    invoke-direct {v1, v0, v2, p3, v3}, LX/24N;-><init>(LX/AkL;LX/1RN;LX/1Pr;LX/0jW;)V

    .line 366724
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366725
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 366726
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 366727
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 366728
    new-instance v0, LX/24Q;

    iget-object v1, v1, LX/24N;->c:LX/24O;

    invoke-direct {v0, v1, v7, v6, v2}, LX/24Q;-><init>(LX/24O;ZLX/24L;Landroid/graphics/ColorMatrixColorFilter;)V

    .line 366729
    iget-object v1, p2, LX/24D;->a:LX/AkL;

    if-eqz v1, :cond_0

    .line 366730
    iget-object v1, p2, LX/24D;->a:LX/AkL;

    .line 366731
    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object v3, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-interface {v1}, LX/AkL;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    new-instance v3, LX/Aju;

    invoke-direct {v3, p0, v0}, LX/Aju;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;LX/24Q;)V

    invoke-virtual {v2, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    move-object v1, v2

    .line 366732
    iput-object v1, v0, LX/24Q;->e:LX/1aZ;

    .line 366733
    :cond_0
    return-object v0

    .line 366734
    :cond_1
    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/24O;

    .line 366735
    sget-object v5, LX/24O;->c:LX/24O;

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3a5179e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 366736
    check-cast p1, LX/24D;

    check-cast p2, LX/24Q;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->a(LX/24D;LX/24Q;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, 0x60824154

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 366737
    check-cast p2, LX/24Q;

    const/4 v1, 0x0

    .line 366738
    move-object v0, p4

    check-cast v0, LX/1aT;

    invoke-interface {v0}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366739
    iget-object v0, p2, LX/24Q;->f:Lcom/facebook/widget/ShimmerFrameLayout;

    if-eqz v0, :cond_0

    .line 366740
    iput-object v1, p2, LX/24Q;->f:Lcom/facebook/widget/ShimmerFrameLayout;

    .line 366741
    check-cast p4, LX/1aT;

    invoke-interface {p4}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 366742
    :cond_0
    return-void
.end method
