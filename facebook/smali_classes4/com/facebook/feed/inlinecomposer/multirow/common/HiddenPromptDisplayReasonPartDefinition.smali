.class public Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aR;",
        ":",
        "LX/1aQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 367059
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 367060
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;
    .locals 3

    .prologue
    .line 367048
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    monitor-enter v1

    .line 367049
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 367050
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 367051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 367053
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;-><init>()V

    .line 367054
    move-object v0, v0

    .line 367055
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 367056
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367057
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 367058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 367047
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x58f5a284

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367039
    const/16 p0, 0x8

    .line 367040
    move-object v1, p4

    check-cast v1, LX/1aR;

    invoke-interface {v1}, LX/1aR;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p4

    .line 367041
    check-cast v1, LX/1aR;

    invoke-interface {v1}, LX/1aR;->getPromptDisplayReasonView()Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    move-result-object v2

    .line 367042
    invoke-virtual {v2, p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 367043
    invoke-virtual {v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 367044
    const-string v1, ""

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 367045
    :cond_0
    check-cast p4, LX/1aR;

    const/4 v1, 0x0

    invoke-interface {p4, v1}, LX/1aR;->a(Z)V

    .line 367046
    const/16 v1, 0x1f

    const v2, 0x13ccf31

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
