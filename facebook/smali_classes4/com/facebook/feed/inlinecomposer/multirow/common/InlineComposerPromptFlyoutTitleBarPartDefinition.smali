.class public Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aS;",
        "E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/24R;",
        "LX/24U;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1bQ;


# direct methods
.method public constructor <init>(LX/1bQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366976
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 366977
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a:LX/1bQ;

    .line 366978
    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 366979
    if-eqz p0, :cond_0

    .line 366980
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    mul-int/lit8 v2, p1, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v2, p1

    invoke-virtual {p0, v0, p1, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 366981
    :cond_0
    return-object p0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;
    .locals 4

    .prologue
    .line 366965
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    monitor-enter v1

    .line 366966
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366967
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366970
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    invoke-static {v0}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object v3

    check-cast v3, LX/1bQ;

    invoke-direct {p0, v3}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;-><init>(LX/1bQ;)V

    .line 366971
    move-object v0, p0

    .line 366972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 366951
    check-cast p2, LX/24R;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 366952
    iget-object v6, p2, LX/24R;->a:LX/AkL;

    .line 366953
    if-eqz v6, :cond_3

    .line 366954
    invoke-interface {v6}, LX/AkL;->c()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a:LX/1bQ;

    .line 366955
    iget-object v1, v0, LX/1bQ;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    .line 366956
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 366957
    invoke-interface {v6}, LX/AkL;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    .line 366958
    :goto_1
    invoke-interface {v6}, LX/AkL;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, ""

    .line 366959
    :goto_2
    invoke-interface {v6}, LX/AkL;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b11ba

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 366960
    invoke-interface {v6}, LX/AkL;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object v2, v1

    move-object v1, v0

    .line 366961
    :goto_3
    new-instance v0, LX/24U;

    invoke-direct/range {v0 .. v5}, LX/24U;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;I)V

    return-object v0

    .line 366962
    :cond_0
    invoke-interface {v6}, LX/AkL;->c()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 366963
    :cond_1
    invoke-interface {v6}, LX/AkL;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 366964
    :cond_2
    invoke-interface {v6}, LX/AkL;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    move-object v3, v4

    move v5, v2

    move-object v1, v4

    move-object v2, v4

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2dd1abe1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 366933
    check-cast p1, LX/24R;

    check-cast p2, LX/24U;

    check-cast p3, LX/1Pn;

    const/16 v2, 0x8

    const/4 v4, 0x0

    const/4 p0, 0x0

    .line 366934
    iget-object v1, p1, LX/24R;->a:LX/AkL;

    if-nez v1, :cond_1

    .line 366935
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x388ae513

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v1, p4

    .line 366936
    check-cast v1, LX/1aS;

    invoke-interface {v1}, LX/1aS;->getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;

    move-result-object v5

    .line 366937
    iget-object v1, p2, LX/24U;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366938
    iget-object v1, p2, LX/24U;->a:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366939
    check-cast p4, LX/1aS;

    invoke-interface {p4}, LX/1aS;->getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;

    move-result-object v1

    .line 366940
    iget-object v5, p2, LX/24U;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 366941
    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 366942
    iget-object v2, p2, LX/24U;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 366943
    iget v2, p2, LX/24U;->e:I

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 366944
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0063

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablePadding(I)V

    .line 366945
    iget-object v2, p2, LX/24U;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 366946
    iget-object v2, p2, LX/24U;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2, p0, p0, p0}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 366947
    :cond_2
    iget-object v2, p2, LX/24U;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 366948
    iget-object v2, p2, LX/24U;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, p0, p0, v2, p0}, LX/4lM;->b(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    move v1, v4

    .line 366949
    goto :goto_1

    :cond_4
    move v2, v4

    .line 366950
    goto :goto_2
.end method
