.class public Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aR;",
        ":",
        "LX/1aQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
        "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 367012
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 367013
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;
    .locals 3

    .prologue
    .line 367014
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    monitor-enter v1

    .line 367015
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 367016
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 367017
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367018
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 367019
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;-><init>()V

    .line 367020
    move-object v0, v0

    .line 367021
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 367022
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367023
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 367024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 367025
    check-cast p2, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 367026
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 367027
    :cond_0
    iget-object v0, p2, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v0, v0

    .line 367028
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x31213972

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367029
    check-cast p2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 367030
    move-object v1, p4

    check-cast v1, LX/1aR;

    invoke-interface {v1}, LX/1aR;->getPromptDisplayReasonView()Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    move-result-object v1

    .line 367031
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 367032
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 367033
    :goto_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    move-object v1, p4

    .line 367034
    check-cast v1, LX/1aR;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1aR;->a(Z)V

    .line 367035
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367036
    const/16 v1, 0x1f

    const v2, -0x5b6bd8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 367037
    :catch_0
    move-exception v2

    .line 367038
    const-string p0, "TopPromptDisplayReasonPartDefinition"

    invoke-virtual {v2}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
