.class public Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        "E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/24H;",
        "LX/24L;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

.field private final d:LX/03V;

.field private final e:LX/24B;

.field private final f:Landroid/content/Context;

.field private final g:LX/1Rg;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/24G;

.field public final j:LX/1E1;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;LX/03V;LX/24B;Landroid/content/Context;LX/1Rg;LX/0Or;LX/24G;LX/1E1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/24B;",
            "Landroid/content/Context;",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;",
            "LX/24G;",
            "LX/1E1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366848
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 366849
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    .line 366850
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    .line 366851
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    .line 366852
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->d:LX/03V;

    .line 366853
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->e:LX/24B;

    .line 366854
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->f:Landroid/content/Context;

    .line 366855
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->g:LX/1Rg;

    .line 366856
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->h:LX/0Or;

    .line 366857
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->i:LX/24G;

    .line 366858
    iput-object p10, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->j:LX/1E1;

    .line 366859
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;
    .locals 14

    .prologue
    .line 366860
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    monitor-enter v1

    .line 366861
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366862
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366865
    new-instance v3, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v8

    check-cast v8, LX/24B;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v10

    check-cast v10, LX/1Rg;

    const/16 v11, 0xfd1

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const-class v12, LX/24G;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/24G;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v13

    check-cast v13, LX/1E1;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;LX/03V;LX/24B;Landroid/content/Context;LX/1Rg;LX/0Or;LX/24G;LX/1E1;)V

    .line 366866
    move-object v0, v3

    .line 366867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;Landroid/view/View;LX/AkM;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/AkM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 366871
    check-cast p1, LX/1aQ;

    invoke-interface {p1}, LX/1aQ;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    .line 366872
    if-eqz p2, :cond_2

    .line 366873
    invoke-interface {p2}, LX/AkM;->a()Landroid/view/View;

    move-result-object v2

    .line 366874
    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 366875
    if-nez v0, :cond_0

    .line 366876
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 366877
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 366878
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 366879
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 366880
    :cond_0
    :goto_1
    return-void

    .line 366881
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->d:LX/03V;

    const-class v3, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding child view of type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to a new parent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", but child already has parent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 366882
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 366883
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 366884
    :cond_2
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 366885
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 366886
    check-cast p2, LX/24H;

    check-cast p3, LX/1Pr;

    .line 366887
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    new-instance v1, LX/24R;

    iget-object v2, p2, LX/24H;->a:LX/AkL;

    invoke-direct {v1, v2}, LX/24R;-><init>(LX/AkL;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366888
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerExposedBackgroundPartDefinition;

    const/4 v2, 0x0

    .line 366889
    new-instance v1, LX/1bP;

    invoke-direct {v1, v2, v2, v2, v2}, LX/1bP;-><init>(ZZZZ)V

    move-object v1, v1

    .line 366890
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366891
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    iget-object v0, p2, LX/24H;->a:LX/AkL;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 366892
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->i:LX/24G;

    check-cast p3, LX/1Qa;

    .line 366893
    new-instance v2, LX/24S;

    const/16 v1, 0xfe0

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v1

    check-cast v1, LX/24B;

    const/16 v4, 0x2ffa

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4, p3}, LX/24S;-><init>(LX/0Or;LX/24B;LX/0Or;LX/1Qa;)V

    .line 366894
    move-object v6, v2

    .line 366895
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->e:LX/24B;

    iget-object v2, p2, LX/24H;->c:LX/1RN;

    iget-object v3, p2, LX/24H;->d:LX/0jW;

    const-class v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v5, LX/24T;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->f:Landroid/content/Context;

    iget-object v7, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->g:LX/1Rg;

    iget-object v8, p2, LX/24H;->b:LX/AkM;

    invoke-direct {v5, v1, v7, v8, v6}, LX/24T;-><init>(Landroid/content/Context;LX/1Rg;LX/AkM;Landroid/view/View$OnClickListener;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 366896
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->j:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366897
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->e:LX/24B;

    iget-object v2, p2, LX/24H;->c:LX/1RN;

    iget-object v3, p2, LX/24H;->d:LX/0jW;

    const-class v4, LX/AkA;

    new-instance v5, LX/AkA;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->f:Landroid/content/Context;

    iget-object v7, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->g:LX/1Rg;

    invoke-direct {v5, v1, v7}, LX/AkA;-><init>(Landroid/content/Context;LX/1Rg;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 366898
    :cond_0
    return-object v6

    .line 366899
    :cond_1
    iget-object v0, p2, LX/24H;->a:LX/AkL;

    invoke-interface {v0}, LX/AkL;->h()Lcom/facebook/productionprompts/model/PromptDisplayReason;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3cad20e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 366900
    check-cast p1, LX/24H;

    check-cast p2, LX/24L;

    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 366901
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->j:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 366902
    const v1, 0x7f0d112c

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 366903
    instance-of v2, v1, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 366904
    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    move-object v1, p4

    .line 366905
    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    .line 366906
    iget-boolean v2, p1, LX/24H;->e:Z

    if-eqz v2, :cond_1

    .line 366907
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    .line 366908
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    .line 366909
    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v7

    .line 366910
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    .line 366911
    const p3, 0x7f021530

    invoke-virtual {v1, p3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 366912
    invoke-virtual {v1, v2, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 366913
    :cond_1
    iget-object v1, p1, LX/24H;->a:LX/AkL;

    if-nez v1, :cond_2

    .line 366914
    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 366915
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x6de9ca8c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 366916
    :cond_2
    iget-object v1, p1, LX/24H;->c:LX/1RN;

    invoke-interface {p2, p4, v1}, LX/24L;->a(Landroid/view/View;LX/1RN;)V

    .line 366917
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->j:LX/1E1;

    invoke-virtual {v1}, LX/1E1;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 366918
    iget-object v1, p1, LX/24H;->b:LX/AkM;

    invoke-static {p0, p4, v1}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;Landroid/view/View;LX/AkM;)V

    .line 366919
    :goto_1
    iget-object v1, p1, LX/24H;->c:LX/1RN;

    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v2

    iget-object v4, p1, LX/24H;->a:LX/AkL;

    iget-object v5, p1, LX/24H;->b:LX/AkM;

    .line 366920
    new-instance v7, LX/2xq;

    invoke-direct {v7, v2, v4, v5}, LX/2xq;-><init>(Landroid/view/View;LX/AkL;LX/AkM;)V

    .line 366921
    iget-object v6, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->h:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1RH;

    invoke-virtual {v6, v7, v1}, LX/1RI;->a(LX/2xq;LX/1RN;)V

    .line 366922
    goto :goto_0

    .line 366923
    :cond_3
    iget-object v1, p1, LX/24H;->b:LX/AkM;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    move-object v2, v1

    :goto_2
    move-object v1, p4

    .line 366924
    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v6

    if-nez v2, :cond_5

    move v1, v4

    :goto_3
    invoke-virtual {v6, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 366925
    :cond_4
    iget-object v1, p1, LX/24H;->b:LX/AkM;

    invoke-interface {v1}, LX/AkM;->a()Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    :cond_5
    move v1, v5

    .line 366926
    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 366927
    check-cast p1, LX/24H;

    .line 366928
    move-object v0, p4

    check-cast v0, LX/1aQ;

    invoke-interface {v0}, LX/1aQ;->getFlyoutXoutButton()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366929
    iget-object v0, p1, LX/24H;->c:LX/1RN;

    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p1, LX/24H;->a:LX/AkL;

    iget-object v3, p1, LX/24H;->b:LX/AkM;

    .line 366930
    new-instance p2, LX/2xq;

    invoke-direct {p2, v1, v2, v3}, LX/2xq;-><init>(Landroid/view/View;LX/AkL;LX/AkM;)V

    .line 366931
    iget-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->h:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1RH;

    invoke-virtual {p1, p2, v0}, LX/1RI;->b(LX/2xq;LX/1RN;)V

    .line 366932
    return-void
.end method
