.class public Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aR;",
        ":",
        "LX/1aQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1E1;

.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

.field private final c:Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;


# direct methods
.method public constructor <init>(LX/1E1;Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366993
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 366994
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->a:LX/1E1;

    .line 366995
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    .line 366996
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    .line 366997
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;
    .locals 6

    .prologue
    .line 366982
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    monitor-enter v1

    .line 366983
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 366984
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366985
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366986
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 366987
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v3

    check-cast v3, LX/1E1;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;-><init>(LX/1E1;Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;)V

    .line 366988
    move-object v0, p0

    .line 366989
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 366990
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366991
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 366992
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 366998
    check-cast p2, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 366999
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->a:LX/1E1;

    const/4 v1, 0x1

    .line 367000
    iget-object v2, v0, LX/1E1;->a:LX/0ad;

    sget v3, LX/1EC;->g:I

    const/4 p3, 0x0

    invoke-interface {v2, v3, p3}, LX/0ad;->a(II)I

    move-result v2

    move v2, v2

    .line 367001
    if-ne v2, v1, :cond_2

    :goto_0
    move v0, v1

    .line 367002
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 367003
    iget-object v0, p2, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v0, v0

    .line 367004
    if-eqz v0, :cond_0

    .line 367005
    iget-object v0, p2, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v0, v0

    .line 367006
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 367007
    :goto_1
    if-eqz v0, :cond_1

    .line 367008
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/TopPromptDisplayReasonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 367009
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 367010
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 367011
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptDisplayReasonPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/HiddenPromptDisplayReasonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
