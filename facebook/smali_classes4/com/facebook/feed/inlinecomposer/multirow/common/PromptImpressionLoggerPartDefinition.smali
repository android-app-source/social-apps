.class public Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/24N;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5oW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;",
            "LX/0Or",
            "<",
            "LX/5oW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 367151
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 367152
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a:LX/0Or;

    .line 367153
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->b:LX/0Or;

    .line 367154
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;
    .locals 5

    .prologue
    .line 367155
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    monitor-enter v1

    .line 367156
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 367157
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 367158
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367159
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 367160
    new-instance v3, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    const/16 v4, 0x2ffa

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x2ff9

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;-><init>(LX/0Or;LX/0Or;)V

    .line 367161
    move-object v0, v3

    .line 367162
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 367163
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367164
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 367165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/24N;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/24N;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 367166
    iget-object v0, p1, LX/24N;->a:LX/AkL;

    if-eqz v0, :cond_1

    .line 367167
    iget-object v0, p1, LX/24N;->c:LX/24O;

    iget-boolean v0, v0, LX/24O;->b:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 367168
    :goto_0
    move v0, v0

    .line 367169
    if-eqz v0, :cond_0

    .line 367170
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v2, p1, LX/24N;->b:LX/1RN;

    .line 367171
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 367172
    sget-object v10, LX/5oV;->IMPRESSION:LX/5oV;

    iget-object v4, v2, LX/1RN;->a:LX/1kK;

    invoke-interface {v4}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, LX/1RN;->a:LX/1kK;

    invoke-interface {v5}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    iget-object v8, v2, LX/1RN;->b:LX/1lP;

    iget-object v8, v8, LX/1lP;->b:Ljava/lang/String;

    iget-object v9, v2, LX/1RN;->c:LX/32e;

    iget-object v9, v9, LX/32e;->a:LX/24P;

    invoke-virtual {v9}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v4

    invoke-static {v0, v10, v4}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 367173
    move-object v2, v6

    .line 367174
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oY;

    iget-object v3, p1, LX/24N;->b:LX/1RN;

    iget-object v3, v3, LX/1RN;->a:LX/1kK;

    invoke-interface {v3}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, LX/5oY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 367175
    iget-object v0, p1, LX/24N;->c:LX/24O;

    iput-boolean v1, v0, LX/24O;->b:Z

    .line 367176
    :cond_0
    iget-object v0, p1, LX/24N;->c:LX/24O;

    iget-object v0, v0, LX/24O;->a:LX/24P;

    iget-object v1, p1, LX/24N;->b:LX/1RN;

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    if-eq v0, v1, :cond_1

    .line 367177
    iget-object v0, p1, LX/24N;->c:LX/24O;

    iget-object v1, p1, LX/24N;->b:LX/1RN;

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    iput-object v1, v0, LX/24O;->a:LX/24P;

    .line 367178
    :cond_1
    return-void

    .line 367179
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 367180
    check-cast p2, LX/24N;

    .line 367181
    iget-object v0, p2, LX/24N;->a:LX/AkL;

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/24N;->c:LX/24O;

    iget-object v0, v0, LX/24O;->a:LX/24P;

    iget-object v1, p2, LX/24N;->b:LX/1RN;

    iget-object v1, v1, LX/1RN;->c:LX/32e;

    iget-object v1, v1, LX/32e;->a:LX/24P;

    if-eq v0, v1, :cond_0

    .line 367182
    iget-object v0, p2, LX/24N;->c:LX/24O;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/24O;->b:Z

    .line 367183
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1abdfe4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367184
    check-cast p1, LX/24N;

    invoke-direct {p0, p1}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->b(LX/24N;)V

    const/16 v1, 0x1f

    const v2, 0x6e0cff66

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
