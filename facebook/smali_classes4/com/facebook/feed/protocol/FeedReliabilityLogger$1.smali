.class public final Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic b:Z

.field public final synthetic c:LX/0pX;


# direct methods
.method public constructor <init>(LX/0pX;Lcom/facebook/api/feed/FetchFeedParams;Z)V
    .locals 0

    .prologue
    .line 486382
    iput-object p1, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->c:LX/0pX;

    iput-object p2, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->a:Lcom/facebook/api/feed/FetchFeedParams;

    iput-boolean p3, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 486383
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_e2e_load_success"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_query_id"

    iget-object v2, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 486384
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v2, v3

    .line 486385
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "from_primed_result"

    iget-boolean v2, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 486386
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 486387
    move-object v0, v0

    .line 486388
    iget-object v1, p0, Lcom/facebook/feed/protocol/FeedReliabilityLogger$1;->c:LX/0pX;

    invoke-static {v1, v0}, LX/0pX;->a$redex0(LX/0pX;Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    .line 486389
    return-void
.end method
