.class public Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0hF;
.implements LX/0fj;
.implements LX/0fk;
.implements LX/21l;
.implements LX/0fw;


# annotations
.annotation build Lcom/facebook/common/init/GenerateInitializer;
    task = LX/Bti;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0fh;",
        "LX/0hF;",
        "LX/0fj;",
        "LX/0fk;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/0fw;"
    }
.end annotation


# static fields
.field public static final c:LX/0Tn;


# instance fields
.field public A:LX/99w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/5vm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/1ry;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/AjP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/1Sl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:LX/Bol;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/20r;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/3H7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public L:LX/3E1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public M:LX/BFR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public N:LX/3Cm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public O:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public Q:LX/1Db;

.field public R:LX/0pf;

.field public S:LX/0tF;

.field public T:LX/20j;

.field private U:LX/9FH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public V:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public W:LX/1Nn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public X:LX/3kp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private Y:LX/0g8;

.field public Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public a:Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

.field private aA:Z

.field public aB:J

.field public aC:Z

.field public aD:Z

.field private aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public ab:Landroid/view/ViewGroup;

.field private ac:Landroid/content/Context;

.field public ad:LX/9DG;

.field private ae:LX/Boy;

.field private af:LX/AlS;

.field public ag:LX/62C;

.field public ah:Lcom/facebook/permalink/PermalinkParams;

.field private ai:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private aj:LX/BF9;

.field public final ak:LX/1DI;

.field private al:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field public am:LX/62O;

.field public an:Z

.field public ao:Lcom/facebook/graphql/model/GraphQLStory;

.field public ap:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private aq:LX/195;

.field private ar:LX/BFU;

.field private as:Lcom/facebook/graphql/model/GraphQLComment;

.field private at:LX/99V;

.field private au:LX/162;

.field private av:Z

.field public aw:Z

.field public ax:Z

.field private ay:Z

.field private az:Z

.field public b:Landroid/support/design/widget/AppBarLayout;

.field public d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public e:LX/0oh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/9DH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/1Jn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/BFV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/1qa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/189;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/193;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/14w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ch5;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0lC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/BFI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/2yx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/AlT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/BFA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/CZm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/BFQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/BNQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/2cm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/BFM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/BFJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 493397
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "permalink_composer_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 493398
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 493399
    new-instance v0, LX/Btj;

    invoke-direct {v0, p0}, LX/Btj;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ak:LX/1DI;

    .line 493400
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aB:J

    .line 493401
    return-void
.end method

.method public static synthetic a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 493402
    invoke-static {p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2

    .prologue
    .line 493403
    invoke-static {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 493404
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 493405
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    const/4 p0, 0x0

    .line 493406
    iput-object p0, v1, LX/23u;->aF:Lcom/facebook/graphql/model/GraphQLStory;

    .line 493407
    move-object v1, v1

    .line 493408
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 493409
    :cond_0
    move-object v0, v0

    .line 493410
    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 12

    .prologue
    .line 493411
    new-instance v1, LX/BtZ;

    invoke-direct {v1, p0}, LX/BtZ;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 493412
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->w:LX/BNQ;

    const-string v3, "story_view"

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move v1, p1

    move-object v2, p2

    .line 493413
    iget-object v6, v0, LX/BNQ;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BNP;

    move v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 493414
    return-void
.end method

.method private static a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;LX/0oh;LX/0Zb;LX/9DH;LX/1Jn;LX/BFV;LX/1qa;LX/189;LX/193;LX/14w;LX/0Ot;LX/0Ot;LX/0lC;LX/BFI;LX/2yx;LX/AlT;LX/BFA;LX/CZm;LX/BFQ;LX/BNQ;LX/2cm;LX/BFM;LX/BFJ;LX/99w;LX/5vm;LX/0Or;LX/1ry;LX/0ad;LX/AjP;LX/1Sl;LX/Bol;LX/20r;LX/3H7;Ljava/util/concurrent/Executor;LX/3E1;LX/BFR;LX/9FH;LX/0Ot;LX/1Nn;LX/3kp;LX/3Cm;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;",
            "LX/0oh;",
            "LX/0Zb;",
            "LX/9DH;",
            "LX/1Jn;",
            "LX/BFV;",
            "LX/1qa;",
            "LX/189;",
            "LX/193;",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ch5;",
            ">;",
            "LX/0lC;",
            "LX/BFI;",
            "LX/2yx;",
            "LX/AlT;",
            "LX/BFA;",
            "LX/CZm;",
            "LX/BFQ;",
            "LX/BNQ;",
            "LX/2cm;",
            "LX/BFM;",
            "LX/BFJ;",
            "LX/99w;",
            "LX/5vm;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1ry;",
            "LX/0ad;",
            "LX/AjP;",
            "LX/1Sl;",
            "LX/Bol;",
            "LX/20r;",
            "LX/3H7;",
            "Ljava/util/concurrent/Executor;",
            "LX/3E1;",
            "LX/BFR;",
            "LX/9FH;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/1Nn;",
            "LX/3kp;",
            "LX/3Cm;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 493415
    iput-object p1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->e:LX/0oh;

    iput-object p2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->f:LX/0Zb;

    iput-object p3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->g:LX/9DH;

    iput-object p4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->h:LX/1Jn;

    iput-object p5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->i:LX/BFV;

    iput-object p6, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->j:LX/1qa;

    iput-object p7, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->k:LX/189;

    iput-object p8, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->l:LX/193;

    iput-object p9, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->m:LX/14w;

    iput-object p10, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->n:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->o:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->p:LX/0lC;

    iput-object p13, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->q:LX/BFI;

    iput-object p14, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->r:LX/2yx;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->s:LX/AlT;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->t:LX/BFA;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->u:LX/CZm;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->v:LX/BFQ;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->w:LX/BNQ;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->x:LX/2cm;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->y:LX/BFM;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->z:LX/BFJ;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->A:LX/99w;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->B:LX/5vm;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->C:LX/0Or;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->D:LX/1ry;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->F:LX/AjP;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->G:LX/1Sl;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->H:LX/Bol;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->I:LX/20r;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->J:LX/3H7;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->K:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->L:LX/3E1;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->M:LX/BFR;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->U:LX/9FH;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->V:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->W:LX/1Nn;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->X:LX/3kp;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->N:LX/3Cm;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->O:LX/0Ot;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->P:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 4

    .prologue
    .line 493416
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->af:LX/AlS;

    .line 493417
    iput-object p1, v0, LX/AlS;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 493418
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 493419
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-static {v0}, LX/BFO;->a(Lcom/facebook/permalink/PermalinkParams;)LX/BFO;

    move-result-object v0

    .line 493420
    iput-object p1, v0, LX/BFO;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 493421
    move-object v0, v0

    .line 493422
    invoke-virtual {v0}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493423
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493424
    iget-object v2, v1, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-object v1, v2

    .line 493425
    iput-object v1, v0, LX/9DG;->S:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 493426
    iget-object v2, v0, LX/9DG;->b:LX/9CC;

    .line 493427
    iget-object v3, v2, LX/9CC;->a:LX/9E8;

    invoke-virtual {v3, v1}, LX/9E8;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 493428
    iget-object v3, v2, LX/9CC;->b:LX/9E8;

    invoke-virtual {v3, v1}, LX/9E8;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 493429
    iget-object v2, v0, LX/9DG;->L:LX/21n;

    if-eqz v2, :cond_0

    .line 493430
    iget-object v2, v0, LX/9DG;->L:LX/21n;

    invoke-interface {v2, v1}, LX/21n;->setNotificationLogObject(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 493431
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v0, v0

    .line 493432
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->au:LX/162;

    .line 493433
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 46

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v44

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static/range {v44 .. v44}, LX/0oh;->a(LX/0QB;)LX/0oh;

    move-result-object v3

    check-cast v3, LX/0oh;

    invoke-static/range {v44 .. v44}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const-class v5, LX/9DH;

    move-object/from16 v0, v44

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9DH;

    invoke-static/range {v44 .. v44}, LX/1Jn;->a(LX/0QB;)LX/1Jn;

    move-result-object v6

    check-cast v6, LX/1Jn;

    const-class v7, LX/BFV;

    move-object/from16 v0, v44

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BFV;

    invoke-static/range {v44 .. v44}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    invoke-static/range {v44 .. v44}, LX/189;->a(LX/0QB;)LX/189;

    move-result-object v9

    check-cast v9, LX/189;

    const-class v10, LX/193;

    move-object/from16 v0, v44

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/193;

    invoke-static/range {v44 .. v44}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v11

    check-cast v11, LX/14w;

    const/16 v12, 0x2fca

    move-object/from16 v0, v44

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x31bf

    move-object/from16 v0, v44

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v44 .. v44}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v14

    check-cast v14, LX/0lC;

    invoke-static/range {v44 .. v44}, LX/BFI;->a(LX/0QB;)LX/BFI;

    move-result-object v15

    check-cast v15, LX/BFI;

    invoke-static/range {v44 .. v44}, LX/2yx;->a(LX/0QB;)LX/2yx;

    move-result-object v16

    check-cast v16, LX/2yx;

    const-class v17, LX/AlT;

    move-object/from16 v0, v44

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/AlT;

    const-class v18, LX/BFA;

    move-object/from16 v0, v44

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/BFA;

    const-class v19, LX/CZm;

    move-object/from16 v0, v44

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/CZm;

    invoke-static/range {v44 .. v44}, LX/BFQ;->a(LX/0QB;)LX/BFQ;

    move-result-object v20

    check-cast v20, LX/BFQ;

    invoke-static/range {v44 .. v44}, LX/BNQ;->a(LX/0QB;)LX/BNQ;

    move-result-object v21

    check-cast v21, LX/BNQ;

    invoke-static/range {v44 .. v44}, LX/2cm;->a(LX/0QB;)LX/2cm;

    move-result-object v22

    check-cast v22, LX/2cm;

    invoke-static/range {v44 .. v44}, LX/BFM;->a(LX/0QB;)LX/BFM;

    move-result-object v23

    check-cast v23, LX/BFM;

    const-class v24, LX/BFJ;

    move-object/from16 v0, v44

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/BFJ;

    invoke-static/range {v44 .. v44}, LX/99w;->a(LX/0QB;)LX/99w;

    move-result-object v25

    check-cast v25, LX/99w;

    invoke-static/range {v44 .. v44}, LX/5vm;->a(LX/0QB;)LX/5vm;

    move-result-object v26

    check-cast v26, LX/5vm;

    const/16 v27, 0x455

    move-object/from16 v0, v44

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    invoke-static/range {v44 .. v44}, LX/1ry;->a(LX/0QB;)LX/1ry;

    move-result-object v28

    check-cast v28, LX/1ry;

    invoke-static/range {v44 .. v44}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v29

    check-cast v29, LX/0ad;

    invoke-static/range {v44 .. v44}, LX/AjP;->a(LX/0QB;)LX/AjP;

    move-result-object v30

    check-cast v30, LX/AjP;

    invoke-static/range {v44 .. v44}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v31

    check-cast v31, LX/1Sl;

    invoke-static/range {v44 .. v44}, LX/Bol;->a(LX/0QB;)LX/Bol;

    move-result-object v32

    check-cast v32, LX/Bol;

    invoke-static/range {v44 .. v44}, LX/20r;->a(LX/0QB;)LX/20r;

    move-result-object v33

    check-cast v33, LX/20r;

    invoke-static/range {v44 .. v44}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v34

    check-cast v34, LX/3H7;

    invoke-static/range {v44 .. v44}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v35

    check-cast v35, Ljava/util/concurrent/Executor;

    invoke-static/range {v44 .. v44}, LX/3E1;->a(LX/0QB;)LX/3E1;

    move-result-object v36

    check-cast v36, LX/3E1;

    invoke-static/range {v44 .. v44}, LX/BFR;->a(LX/0QB;)LX/BFR;

    move-result-object v37

    check-cast v37, LX/BFR;

    const-class v38, LX/9FH;

    move-object/from16 v0, v44

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/9FH;

    const/16 v39, 0x2e3

    move-object/from16 v0, v44

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v39

    invoke-static/range {v44 .. v44}, LX/1Nn;->a(LX/0QB;)LX/1Nn;

    move-result-object v40

    check-cast v40, LX/1Nn;

    invoke-static/range {v44 .. v44}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v41

    check-cast v41, LX/3kp;

    invoke-static/range {v44 .. v44}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v42

    check-cast v42, LX/3Cm;

    const/16 v43, 0x259

    move-object/from16 v0, v44

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v43

    const/16 v45, 0x2eb

    invoke-static/range {v44 .. v45}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v44

    invoke-static/range {v2 .. v44}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;LX/0oh;LX/0Zb;LX/9DH;LX/1Jn;LX/BFV;LX/1qa;LX/189;LX/193;LX/14w;LX/0Ot;LX/0Ot;LX/0lC;LX/BFI;LX/2yx;LX/AlT;LX/BFA;LX/CZm;LX/BFQ;LX/BNQ;LX/2cm;LX/BFM;LX/BFJ;LX/99w;LX/5vm;LX/0Or;LX/1ry;LX/0ad;LX/AjP;LX/1Sl;LX/Bol;LX/20r;LX/3H7;Ljava/util/concurrent/Executor;LX/3E1;LX/BFR;LX/9FH;LX/0Ot;LX/1Nn;LX/3kp;LX/3Cm;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 493636
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 493637
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_4

    .line 493638
    :cond_0
    :goto_0
    move-object v1, v0

    .line 493639
    const/4 v0, 0x0

    .line 493640
    if-eqz v1, :cond_3

    .line 493641
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 493642
    :goto_2
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ai:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 493643
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_3

    .line 493644
    :cond_1
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_1

    .line 493645
    :cond_2
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_2

    .line 493646
    :cond_4
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->T:LX/20j;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v2, v0}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 493647
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    .line 493648
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 493434
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 493436
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    .line 493437
    :cond_0
    return-object p0
.end method

.method private b(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V
    .locals 4

    .prologue
    .line 493438
    if-nez p1, :cond_0

    .line 493439
    :goto_0
    return-void

    .line 493440
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/89n;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 493441
    :goto_1
    const-string v2, "story_view"

    .line 493442
    const v1, 0x403827a

    invoke-static {v0, v1}, LX/89n;->a(Lcom/facebook/graphql/model/GraphQLProfile;I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 493443
    invoke-static {v0}, LX/89n;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Ljava/lang/String;

    move-result-object v0

    .line 493444
    if-eqz p2, :cond_1

    invoke-virtual {p2}, LX/0lF;->e()I

    move-result v1

    if-nez v1, :cond_3

    .line 493445
    :cond_1
    const/4 v1, 0x0

    .line 493446
    :goto_2
    move-object v0, v1

    .line 493447
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 493448
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 493449
    :cond_3
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "story_permalink_opened"

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 493450
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 493451
    move-object v1, v1

    .line 493452
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 493453
    const-string p1, "event_id"

    invoke-virtual {v1, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 493454
    :cond_4
    invoke-static {v1, v0}, LX/2yx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private n()V
    .locals 8

    .prologue
    .line 493455
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493456
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 493457
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->U:LX/9FH;

    .line 493458
    iget-wide v6, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v6

    .line 493459
    invoke-virtual {v1, v2, v3}, LX/9FH;->a(J)LX/9FG;

    move-result-object v1

    .line 493460
    sget-object v2, LX/9FF;->TOP_LEVEL:LX/9FF;

    invoke-virtual {v1, v2}, LX/9FG;->a(LX/9FF;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493461
    :goto_0
    return-void

    .line 493462
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-static {v2}, LX/BFO;->a(Lcom/facebook/permalink/PermalinkParams;)LX/BFO;

    move-result-object v2

    invoke-static {v0}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v0

    .line 493463
    iget-wide v6, v1, LX/9FG;->b:J

    move-wide v4, v6

    .line 493464
    iput-wide v4, v0, LX/21A;->j:J

    .line 493465
    move-object v0, v0

    .line 493466
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 493467
    iput-object v0, v2, LX/BFO;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 493468
    move-object v0, v2

    .line 493469
    invoke-virtual {v0}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    goto :goto_0
.end method

.method private r()Landroid/content/Context;
    .locals 3

    .prologue
    .line 493470
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ac:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 493471
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 493472
    iget-boolean v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aw:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0e076f

    :goto_0
    move v2, v2

    .line 493473
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ac:Landroid/content/Context;

    .line 493474
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ac:Landroid/content/Context;

    return-object v0

    :cond_1
    const v2, 0x7f0e076e

    goto :goto_0
.end method

.method public static y(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 13

    .prologue
    .line 493475
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_1

    .line 493476
    :cond_0
    :goto_0
    return-void

    .line 493477
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 493478
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->q:LX/BFI;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/BtW;

    invoke-direct {v3, p0}, LX/BtW;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 493479
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 493480
    iget-object v4, v1, LX/BFI;->e:LX/3H7;

    sget-object v6, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    sget-object v7, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    sget-object v12, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v2

    move-object v10, v9

    invoke-virtual/range {v4 .. v12}, LX/3H7;->a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 493481
    iget-object v5, v1, LX/BFI;->i:LX/1Ck;

    const-string v6, "fetch_cached_feedback_permalink"

    invoke-virtual {v5, v6, v4, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 493482
    goto :goto_0

    :cond_2
    move v4, v8

    .line 493483
    goto :goto_1
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 493484
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 493485
    const-string v0, "story_view"

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 493486
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 493487
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 493488
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->e:LX/0oh;

    const-class v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v0, v1}, LX/0oh;->b(Ljava/lang/Class;)LX/1Av;

    move-result-object v0

    invoke-virtual {v0}, LX/1Av;->c()LX/1Ax;

    move-result-object v0

    check-cast v0, LX/Bti;

    .line 493489
    iget-object v1, v0, LX/Bti;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 493490
    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 493491
    iget-object v1, v0, LX/Bti;->b:LX/1Db;

    .line 493492
    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Q:LX/1Db;

    .line 493493
    iget-object v1, v0, LX/Bti;->c:LX/0pf;

    .line 493494
    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->R:LX/0pf;

    .line 493495
    iget-object v1, v0, LX/Bti;->d:LX/0tF;

    .line 493496
    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->S:LX/0tF;

    .line 493497
    iget-object v1, v0, LX/Bti;->e:LX/20j;

    .line 493498
    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->T:LX/20j;

    .line 493499
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493500
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->v:LX/BFQ;

    .line 493501
    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0008

    const-string v3, "NNF_PermalinkFromFeedLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0034

    const-string v3, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0031

    const-string v3, "NNF_PermalinkFromAndroidNotificationWarmLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 493502
    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa002f

    const-string v3, "PermalinkFromOnCreateToLoadIfNoNavigationalMetrics"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 493503
    :cond_0
    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa000a

    const-string v3, "PermalinkFromOnCreateToLoad"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 493504
    iget-object v1, v0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x350008

    const-string v3, "NotifPermalinkRefreshStoryTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 493505
    :cond_1
    if-nez p1, :cond_2

    .line 493506
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 493507
    :cond_2
    if-eqz p1, :cond_3

    .line 493508
    const-string v0, "permalink_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/permalink/PermalinkParams;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493509
    const-string v0, "loading_indicator_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->al:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 493510
    const-string v0, "feedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493511
    const-string v0, "feedback"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 493512
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 493513
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493514
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 493515
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 493516
    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->n()V

    .line 493517
    new-instance v4, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment$1;

    invoke-direct {v4, p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment$1;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 493518
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->S:LX/0tF;

    invoke-static {v0, v1, p1}, LX/BFS;->a(LX/0ad;LX/0tF;Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->az:Z

    .line 493519
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    sget-short v1, LX/0fe;->aN:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aA:Z

    .line 493520
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    sget-short v1, LX/0fe;->aL:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aD:Z

    .line 493521
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493522
    iget-boolean v1, v0, Lcom/facebook/permalink/PermalinkParams;->p:Z

    move v0, v1

    .line 493523
    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aw:Z

    .line 493524
    new-instance v0, LX/99V;

    invoke-direct {v0}, LX/99V;-><init>()V

    move-object v0, v0

    .line 493525
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->at:LX/99V;

    .line 493526
    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->r()Landroid/content/Context;

    move-result-object v0

    .line 493527
    sget-object v1, LX/37R;->a:LX/37R;

    move-object v1, v1

    .line 493528
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->at:LX/99V;

    iget-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->H:LX/Bol;

    .line 493529
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 493530
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    if-nez v7, :cond_e

    .line 493531
    :cond_4
    const/4 v5, 0x0

    .line 493532
    :cond_5
    :goto_0
    move-object v5, v5

    .line 493533
    invoke-static/range {v0 .. v5}, LX/BFJ;->a(Landroid/content/Context;LX/1PT;LX/1PY;LX/1QU;Ljava/lang/Runnable;Ljava/lang/String;)LX/3Ij;

    move-result-object v0

    .line 493534
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->h:LX/1Jn;

    .line 493535
    new-instance v2, LX/1Cq;

    invoke-direct {v2}, LX/1Cq;-><init>()V

    .line 493536
    iget-object v3, v1, LX/1Jn;->b:LX/1DS;

    iget-object v4, v1, LX/1Jn;->c:LX/0Ot;

    invoke-virtual {v3, v4, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v3

    .line 493537
    iput-object v0, v3, LX/1Ql;->f:LX/1PW;

    .line 493538
    move-object v3, v3

    .line 493539
    invoke-virtual {v3}, LX/1Ql;->e()LX/1Qq;

    move-result-object v3

    .line 493540
    iget-object v4, v1, LX/1Jn;->e:LX/1Jo;

    .line 493541
    new-instance v0, LX/Boy;

    invoke-static {v4}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v0, v3, v2, v5, v1}, LX/Boy;-><init>(LX/1Qq;LX/1Cq;LX/0Sh;LX/03V;)V

    .line 493542
    move-object v2, v0

    .line 493543
    move-object v0, v2

    .line 493544
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ae:LX/Boy;

    .line 493545
    new-instance v0, LX/62O;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->al:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ak:LX/1DI;

    invoke-direct {v0, v1, v2}, LX/62O;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->am:LX/62O;

    .line 493546
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->g:LX/9DH;

    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->r()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->u:LX/CZm;

    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->r()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/CZm;->a(Landroid/content/Context;)LX/CZl;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493547
    iget-object v5, v1, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v5, v5

    .line 493548
    iget-boolean v7, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->az:Z

    new-instance v9, LX/Bta;

    invoke-direct {v9, p0}, LX/Bta;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    new-instance v11, LX/Btb;

    invoke-direct {v11, p0}, LX/Btb;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    move-object v1, p0

    move v10, v6

    invoke-virtual/range {v0 .. v11}, LX/9DH;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    .line 493549
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 493550
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->y:LX/BFM;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 493551
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ae:LX/Boy;

    check-cast v0, LX/1Cw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    .line 493552
    iget-object v3, v2, LX/9DG;->b:LX/9CC;

    move-object v2, v3

    .line 493553
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 493554
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->az:Z

    if-eqz v0, :cond_c

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/62C;->b(Ljava/util/List;)LX/62C;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ag:LX/62C;

    .line 493555
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->s:LX/AlT;

    new-instance v1, LX/Btc;

    invoke-direct {v1, p0}, LX/Btc;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, v1, v2}, LX/AlT;->a(LX/0QK;LX/9DG;)LX/AlS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->af:LX/AlS;

    .line 493556
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->i:LX/BFV;

    new-instance v1, LX/Btd;

    invoke-direct {v1, p0}, LX/Btd;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 493557
    new-instance v4, LX/BFU;

    const-class v2, LX/9Eb;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9Eb;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v3

    check-cast v3, LX/189;

    invoke-direct {v4, v1, v2, v3}, LX/BFU;-><init>(LX/0QK;LX/9Eb;LX/189;)V

    .line 493558
    move-object v0, v4

    .line 493559
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ar:LX/BFU;

    .line 493560
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->t:LX/BFA;

    new-instance v1, LX/Bte;

    invoke-direct {v1, p0}, LX/Bte;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 493561
    new-instance v4, LX/BF9;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v2

    check-cast v2, LX/1K9;

    invoke-static {v0}, LX/6Ot;->b(LX/0QB;)LX/6Ot;

    move-result-object v3

    check-cast v3, LX/6Ot;

    invoke-direct {v4, v1, v2, v3}, LX/BF9;-><init>(LX/0QK;LX/1K9;LX/6Ot;)V

    .line 493562
    move-object v0, v4

    .line 493563
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aj:LX/BF9;

    .line 493564
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493565
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 493566
    iget-object v0, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v0, v0

    .line 493567
    if-eqz v0, :cond_d

    move v0, v8

    .line 493568
    :goto_2
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v2, v2

    .line 493569
    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_12

    .line 493570
    :cond_6
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v2, v2

    .line 493571
    :goto_3
    iget-object v3, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    move-object v3, v3

    .line 493572
    if-eqz v3, :cond_13

    .line 493573
    iget-object v3, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    move-object v3, v3

    .line 493574
    :goto_4
    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v4

    const-string v5, "story_view"

    .line 493575
    iput-object v5, v4, LX/21A;->c:Ljava/lang/String;

    .line 493576
    move-object v4, v4

    .line 493577
    const-string v5, "permalink_ufi"

    .line 493578
    iput-object v5, v4, LX/21A;->b:Ljava/lang/String;

    .line 493579
    move-object v4, v4

    .line 493580
    iput-object v2, v4, LX/21A;->a:LX/162;

    .line 493581
    move-object v2, v4

    .line 493582
    sget-object v4, LX/21B;->STORY_PERMALINK:LX/21B;

    .line 493583
    iput-object v4, v2, LX/21A;->e:LX/21B;

    .line 493584
    move-object v2, v2

    .line 493585
    iput-object v3, v2, LX/21A;->f:LX/21C;

    .line 493586
    move-object v3, v2

    .line 493587
    iget-boolean v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    move v2, v2

    .line 493588
    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v2

    if-eqz v2, :cond_15

    :cond_7
    const/4 v2, 0x1

    .line 493589
    :goto_5
    iput-boolean v2, v3, LX/21A;->h:Z

    .line 493590
    move-object v2, v3

    .line 493591
    invoke-virtual {v2}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v2

    move-object v1, v2

    .line 493592
    invoke-static {p0, v1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 493593
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    .line 493594
    iput-boolean v0, v1, LX/9DG;->T:Z

    .line 493595
    iget-object v2, v1, LX/9DG;->L:LX/21n;

    if-eqz v2, :cond_8

    .line 493596
    iget-object v2, v1, LX/9DG;->L:LX/21n;

    invoke-interface {v2, v0}, LX/21n;->setReshareButtonExperimentClicked(Z)V

    .line 493597
    :cond_8
    new-array v0, v8, [LX/21l;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    aput-object v1, v0, v6

    invoke-static {v0}, LX/2Ch;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ai:Ljava/util/Set;

    .line 493598
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->l:LX/193;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "new_permalink_scroll_perf"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aq:LX/195;

    .line 493599
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493600
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v1

    .line 493601
    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->as:Lcom/facebook/graphql/model/GraphQLComment;

    .line 493602
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->G:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->av:Z

    .line 493603
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->av:Z

    if-eqz v0, :cond_9

    .line 493604
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->F:LX/AjP;

    new-instance v1, LX/Btf;

    invoke-direct {v1, p0}, LX/Btf;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 493605
    iput-object v1, v0, LX/AjP;->c:LX/AjN;

    .line 493606
    :cond_9
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493607
    iget-boolean v1, v0, Lcom/facebook/permalink/PermalinkParams;->t:Z

    move v0, v1

    .line 493608
    if-eqz v0, :cond_a

    .line 493609
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->f:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "deferred_feedback_open_permalink"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 493610
    :cond_a
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->an:Z

    if-nez v0, :cond_b

    .line 493611
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->q:LX/BFI;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    new-instance v2, LX/BtV;

    invoke-direct {v2, p0}, LX/BtV;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    invoke-virtual {v0, v1, v2}, LX/BFI;->a(Lcom/facebook/permalink/PermalinkParams;LX/0Ve;)V

    .line 493612
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    .line 493613
    :cond_b
    return-void

    .line 493614
    :cond_c
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/62C;->a(Ljava/util/List;)LX/62C;

    move-result-object v0

    goto/16 :goto_1

    :cond_d
    move v0, v6

    .line 493615
    goto/16 :goto_2

    .line 493616
    :cond_e
    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v7, "notification_launch_source"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 493617
    if-nez v5, :cond_5

    .line 493618
    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493619
    iget-object v7, v5, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    move-object v5, v7

    .line 493620
    sget-object v7, LX/21C;->BEEPER:LX/21C;

    if-eq v5, v7, :cond_f

    sget-object v7, LX/21C;->PUSH:LX/21C;

    if-ne v5, v7, :cond_10

    .line 493621
    :cond_f
    const-string v5, "source_system_tray"

    .line 493622
    :goto_6
    move-object v5, v5

    .line 493623
    goto/16 :goto_0

    .line 493624
    :cond_10
    sget-object v7, LX/21C;->JEWEL:LX/21C;

    if-ne v5, v7, :cond_11

    .line 493625
    const-string v5, "source_jewel"

    goto :goto_6

    .line 493626
    :cond_11
    const/4 v5, 0x0

    goto :goto_6

    .line 493627
    :cond_12
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    goto/16 :goto_3

    .line 493628
    :cond_13
    iget-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493629
    iget-object v4, v3, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    move-object v3, v4

    .line 493630
    if-eqz v3, :cond_14

    .line 493631
    iget-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493632
    iget-object v4, v3, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    move-object v3, v4

    .line 493633
    goto/16 :goto_4

    .line 493634
    :cond_14
    sget-object v3, LX/21C;->UNKNOWN:LX/21C;

    goto/16 :goto_4

    .line 493635
    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_5
.end method

.method public a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 13

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 493296
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    move-object v3, v1

    .line 493297
    :goto_0
    if-eqz v3, :cond_4

    iget-boolean v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aC:Z

    if-eqz v1, :cond_4

    move v1, v0

    .line 493298
    :goto_1
    iput-boolean v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aC:Z

    .line 493299
    invoke-static {v3}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v2

    .line 493300
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 493301
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493302
    iget-object v6, v5, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-object v5, v6

    .line 493303
    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493304
    iget-object v6, v5, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-object v5, v6

    .line 493305
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k()LX/0Px;

    move-result-object v5

    if-nez v5, :cond_c

    .line 493306
    :cond_0
    :goto_2
    invoke-static {v4}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 493307
    iget-object v6, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493308
    iget-object v7, v6, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v6, v7

    .line 493309
    iget-object v7, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->as:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v8, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->D:LX/1ry;

    invoke-static {v5, v6, v7, v8}, LX/9DG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/1ry;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 493310
    if-eqz v1, :cond_5

    sget-object v6, LX/21y;->THREADED_CHRONOLOGICAL_ORDER:LX/21y;

    invoke-virtual {v6, v2}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 493311
    if-eqz v3, :cond_1

    if-nez v5, :cond_d

    .line 493312
    :cond_1
    :goto_3
    move v2, v2

    .line 493313
    if-nez v2, :cond_5

    .line 493314
    :cond_2
    :goto_4
    return-void

    .line 493315
    :cond_3
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    :cond_4
    move v1, v2

    .line 493316
    goto :goto_1

    .line 493317
    :cond_5
    iput-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    .line 493318
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->y:LX/BFM;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v4}, LX/BFM;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 493319
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v2}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 493320
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ar:LX/BFU;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v4}, LX/BFU;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 493321
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->af:LX/AlS;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v4}, LX/AlS;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 493322
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aj:LX/BF9;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v4}, LX/BF9;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 493323
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->A:LX/99w;

    .line 493324
    iget-boolean v4, v2, LX/99w;->c:Z

    move v2, v4

    .line 493325
    if-nez v2, :cond_6

    .line 493326
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->A:LX/99w;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->af:LX/AlS;

    const/4 p1, 0x0

    .line 493327
    const/16 v6, 0x8

    new-array v6, v6, [LX/0b2;

    new-instance v7, LX/AlM;

    invoke-direct {v7, v4}, LX/AlM;-><init>(LX/AlS;)V

    aput-object v7, v6, p1

    const/4 v7, 0x1

    new-instance v8, LX/AlK;

    invoke-direct {v8, v4}, LX/AlK;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, LX/AlO;

    invoke-direct {v8, v4}, LX/AlO;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-instance v8, LX/AlR;

    invoke-direct {v8, v4}, LX/AlR;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    const/4 v7, 0x4

    new-instance v8, LX/AlQ;

    invoke-direct {v8, v4}, LX/AlQ;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    const/4 v7, 0x5

    new-instance v8, LX/AlL;

    invoke-direct {v8, v4}, LX/AlL;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    const/4 v7, 0x6

    iget-object v8, v4, LX/AlS;->h:LX/1Kx;

    .line 493328
    iget-object p1, v8, LX/1Kx;->b:LX/1Ky;

    move-object v8, p1

    .line 493329
    aput-object v8, v6, v7

    const/4 v7, 0x7

    new-instance v8, LX/AlN;

    invoke-direct {v8, v4}, LX/AlN;-><init>(LX/AlS;)V

    aput-object v8, v6, v7

    .line 493330
    move-object v4, v6

    .line 493331
    invoke-virtual {v2, v4}, LX/99w;->a([LX/0b2;)V

    .line 493332
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->av:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_7

    .line 493333
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->F:LX/AjP;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v4, v0}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 493334
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 493335
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v6, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->R:LX/0pf;

    invoke-static {v0, v4, v6}, LX/BFR;->a(LX/1ZF;Lcom/facebook/graphql/model/FeedUnit;LX/0pf;)V

    .line 493336
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ax:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493337
    iget v2, v0, Lcom/facebook/permalink/PermalinkParams;->q:I

    move v0, v2

    .line 493338
    const/16 v2, 0xb

    if-ne v0, v2, :cond_8

    .line 493339
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->I:LX/20r;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ab:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, LX/20r;->a(Landroid/view/View;)V

    .line 493340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ax:Z

    .line 493341
    :cond_8
    iget-object v9, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v9}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v9

    if-nez v9, :cond_e

    .line 493342
    :cond_9
    :goto_5
    if-eqz v1, :cond_a

    .line 493343
    iput-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 493344
    :cond_a
    invoke-static {p0, v5}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a$redex0(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 493345
    if-eqz v1, :cond_2

    .line 493346
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    const/4 v1, 0x0

    .line 493347
    sget-object v2, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-static {v5}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 493348
    :cond_b
    goto/16 :goto_4

    .line 493349
    :cond_c
    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493350
    iget-object v6, v5, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-object v5, v6

    .line 493351
    invoke-static {v4, v5}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    goto/16 :goto_2

    .line 493352
    :cond_d
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 493353
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v6

    .line 493354
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v7

    .line 493355
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    if-eqz v8, :cond_1

    .line 493356
    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_3

    .line 493357
    :cond_e
    iget-object v9, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v10, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/9DG;->a(Ljava/lang/Long;)V

    goto :goto_5

    .line 493358
    :cond_f
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 493359
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v2

    .line 493360
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v4

    .line 493361
    if-eqz v2, :cond_b

    if-eqz v4, :cond_b

    .line 493362
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v6

    move v2, v1

    .line 493363
    :goto_6
    if-ge v2, v6, :cond_b

    .line 493364
    iget-object v7, v0, LX/9DG;->j:LX/9Do;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v7, v1}, LX/9Do;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 493365
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V
    .locals 2

    .prologue
    .line 493366
    invoke-virtual {p0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 493367
    invoke-direct {p0, p1, p2}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V

    .line 493368
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ay:Z

    if-nez v0, :cond_0

    .line 493369
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493370
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v1

    .line 493371
    invoke-static {v0}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v0

    .line 493372
    iput-object p2, v0, LX/21A;->a:LX/162;

    .line 493373
    move-object v0, v0

    .line 493374
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 493375
    :cond_0
    if-eqz p1, :cond_2

    .line 493376
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493377
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    move-object v0, v1

    .line 493378
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    if-nez v1, :cond_4

    .line 493379
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493380
    iget-boolean v1, v0, Lcom/facebook/permalink/PermalinkParams;->o:Z

    move v0, v1

    .line 493381
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    if-nez v0, :cond_5

    .line 493382
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_3

    .line 493383
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 493384
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->am:LX/62O;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 493385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->an:Z

    .line 493386
    return-void

    .line 493387
    :cond_4
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v1, v0}, LX/9DG;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493388
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-static {v0}, LX/BFO;->a(Lcom/facebook/permalink/PermalinkParams;)LX/BFO;

    move-result-object v0

    const/4 v1, 0x0

    .line 493389
    iput-object v1, v0, LX/BFO;->v:Lcom/facebook/tagging/model/TaggingProfile;

    .line 493390
    move-object v0, v0

    .line 493391
    invoke-virtual {v0}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    goto :goto_0

    .line 493392
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->g()Z

    move-result v0

    .line 493393
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-static {v1}, LX/BFO;->a(Lcom/facebook/permalink/PermalinkParams;)LX/BFO;

    move-result-object v1

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 493394
    :goto_2
    iput-boolean v0, v1, LX/BFO;->o:Z

    .line 493395
    move-object v0, v1

    .line 493396
    invoke-virtual {v0}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 493083
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    return-void
.end method

.method public b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493084
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 493085
    const/4 v0, 0x0

    .line 493086
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_3

    .line 493087
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/89n;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 493088
    invoke-static {v2}, LX/89n;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Ljava/lang/String;

    move-result-object v2

    .line 493089
    if-eqz v2, :cond_0

    .line 493090
    const-string v3, "source_group_id"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493091
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 493092
    if-eqz v2, :cond_1

    .line 493093
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    .line 493094
    :cond_1
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 493095
    const-string v2, "story_id"

    iget-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493096
    :cond_2
    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 493097
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 493098
    const-string v3, "author_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493099
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 493100
    if-nez v0, :cond_4

    .line 493101
    const-string v0, "content_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 493102
    :cond_4
    if-eqz v0, :cond_6

    .line 493103
    const-string v2, "content_id"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493104
    :cond_5
    :goto_0
    return-object v1

    .line 493105
    :cond_6
    const-string v0, "feedback_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 493106
    if-eqz v0, :cond_8

    .line 493107
    const-string v3, "feedback_id"

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493108
    :cond_7
    :goto_1
    const-string v0, "story_fbid"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 493109
    if-eqz v0, :cond_5

    .line 493110
    const-string v2, "story_fbid"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 493111
    :cond_8
    const-string v0, "story_id"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    .line 493112
    const-string v0, "story_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 493113
    if-eqz v0, :cond_7

    .line 493114
    const-string v3, "story_id"

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    .line 493115
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ae:LX/Boy;

    check-cast v0, LX/21l;

    invoke-interface {v0, p1}, LX/21l;->a(Ljava/lang/Object;)V

    .line 493116
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493117
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 493118
    const-string v0, "Permalink Params"

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 493119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 493120
    const-string v4, "Story Permalink Params Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493121
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    move-object v5, v5

    .line 493122
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493123
    const-string v4, "Permalink Cache Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    invoke-virtual {v5}, LX/89g;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493124
    const-string v4, "Story Fetch Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493125
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 493126
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493127
    const-string v4, "Story Cache Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493128
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v5, v5

    .line 493129
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493130
    const-string v4, "Relevant Comment Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493131
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v5, v5

    .line 493132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493133
    const-string v4, "Relevant Comment Parent Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493134
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    move-object v5, v5

    .line 493135
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493136
    const-string v4, "Default Comment Ordering: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493137
    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    move-object v5, v5

    .line 493138
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493139
    const-string v4, "Notif Log Object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual {v5}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493140
    const-string v4, "Relevant reaction key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493141
    iget v5, v2, Lcom/facebook/permalink/PermalinkParams;->q:I

    move v5, v5

    .line 493142
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493143
    iget-object v4, v2, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    if-eqz v4, :cond_0

    .line 493144
    const-string v4, "Autofill Mention Tagging Profile: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v5}, Lcom/facebook/tagging/model/TaggingProfile;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493145
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 493146
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493147
    const-string v2, "Has Fetched Story: "

    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->an:Z

    if-eqz v0, :cond_2

    const-string v0, "True"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493148
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 493149
    :try_start_0
    const-string v0, "Permalink Story"

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->p:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->g()LX/4ps;

    move-result-object v2

    invoke-virtual {v2}, LX/4ps;->a()LX/4ps;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493150
    :cond_1
    :goto_1
    return-object v1

    .line 493151
    :cond_2
    const-string v0, "False"

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public c()V
    .locals 1

    .prologue
    .line 493152
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 493153
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 493154
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->m()V

    .line 493155
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->q:LX/BFI;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    new-instance v2, LX/BtY;

    invoke-direct {v2, p0}, LX/BtY;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    invoke-virtual {v0, v1, v2}, LX/BFI;->a(Lcom/facebook/permalink/PermalinkParams;LX/0TF;)V

    .line 493156
    return-void
.end method

.method public getDebugInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493157
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 493158
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    return-object v0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 493159
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 493160
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->am:LX/62O;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 493161
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    .line 493162
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 493163
    :cond_0
    :goto_0
    return-void

    .line 493164
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 493165
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, p1, p2, p3}, LX/9DG;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 493166
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 493167
    :sswitch_1
    invoke-direct {p0, p2, p3}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 493168
    :sswitch_2
    const-string v0, "privacy_option_to_upsell"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 493169
    if-eqz v0, :cond_2

    .line 493170
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Px;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ab:Landroid/view/ViewGroup;

    sget-object v4, LX/1K2;->PERMALINK:LX/1K2;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/8Px;->a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/1K2;)V

    .line 493171
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    goto :goto_0

    .line 493172
    :sswitch_3
    const/4 v5, -0x1

    if-eq p2, v5, :cond_4

    .line 493173
    :goto_1
    goto :goto_0

    .line 493174
    :sswitch_4
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    .line 493175
    const-string v0, "story_for_social_search"

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p3, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 493176
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->x:LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 493177
    :sswitch_5
    const-string v0, "social_search_place_list_extra"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 493178
    if-eqz v0, :cond_0

    .line 493179
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    goto :goto_0

    .line 493180
    :sswitch_6
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    goto :goto_0

    .line 493181
    :cond_4
    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->B:LX/5vm;

    sget-object v6, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v7, "extra_profile_pic_expiration"

    const-wide/16 v9, 0x0

    invoke-virtual {p3, v7, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v9, "staging_ground_photo_caption"

    invoke-virtual {p3, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "existing"

    invoke-virtual/range {v5 .. v10}, LX/5vm;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 493182
    const-string v5, "force_create_new_activity"

    const/4 v7, 0x1

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 493183
    iget-object v5, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->C:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6dc -> :sswitch_0
        0x6de -> :sswitch_0
        0x6df -> :sswitch_1
        0x740 -> :sswitch_2
        0x138a -> :sswitch_4
        0x26bb -> :sswitch_3
        0xc364 -> :sswitch_5
        0xc365 -> :sswitch_6
    .end sparse-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 493184
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 493185
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->j:LX/1qa;

    invoke-virtual {v0}, LX/1qa;->a()V

    .line 493186
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ae:LX/Boy;

    invoke-virtual {v0, p1}, LX/Boy;->a(Landroid/content/res/Configuration;)V

    .line 493187
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x60fe53b6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 493188
    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->r()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 493189
    iput-object p2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ab:Landroid/view/ViewGroup;

    .line 493190
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aD:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030f27

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, -0x1f711674

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    :cond_0
    const v0, 0x7f030f28

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x228f5bed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 493191
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 493192
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ae:LX/Boy;

    .line 493193
    invoke-virtual {v1}, LX/1UF;->dispose()V

    .line 493194
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->c()V

    .line 493195
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->af:LX/AlS;

    .line 493196
    iget-object v2, v1, LX/AlS;->b:LX/3iK;

    invoke-virtual {v2}, LX/3iK;->a()V

    .line 493197
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->q:LX/BFI;

    .line 493198
    iget-object v2, v1, LX/BFI;->i:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 493199
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ar:LX/BFU;

    invoke-virtual {v1}, LX/BFU;->a()V

    .line 493200
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aj:LX/BF9;

    .line 493201
    invoke-static {v1}, LX/BF9;->c(LX/BF9;)V

    .line 493202
    iget-boolean v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->av:Z

    if-eqz v1, :cond_0

    .line 493203
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->F:LX/AjP;

    invoke-virtual {v1}, LX/AjP;->a()V

    .line 493204
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ap:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 493205
    const/16 v1, 0x2b

    const v2, 0x4ab3a51e    # 5886607.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d58dc6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 493206
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 493207
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->b()V

    .line 493208
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 493209
    const/16 v1, 0x2b

    const v2, 0x7058141c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3fa53ed4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 493210
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 493211
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->A:LX/99w;

    .line 493212
    iget-object v2, v1, LX/99w;->a:LX/1B1;

    iget-object v3, v1, LX/99w;->b:LX/0bH;

    invoke-virtual {v2, v3}, LX/1B1;->b(LX/0b4;)V

    .line 493213
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->v:LX/BFQ;

    .line 493214
    sget-object v2, LX/BFQ;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Yj;

    .line 493215
    iget-object v5, v1, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v5, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;)V

    goto :goto_0

    .line 493216
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aq:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 493217
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->d()V

    .line 493218
    iget-boolean v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aA:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->au:LX/162;

    if-eqz v1, :cond_1

    .line 493219
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->L:LX/3E1;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->au:LX/162;

    const-string v3, "story_view"

    invoke-virtual {v1, v2, v3}, LX/3E1;->a(LX/162;Ljava/lang/String;)V

    .line 493220
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x54ec1a41

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x51c398df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 493221
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 493222
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->A:LX/99w;

    .line 493223
    invoke-static {v1}, LX/99w;->d(LX/99w;)V

    .line 493224
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->v:LX/BFQ;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    .line 493225
    new-instance v4, LX/BFP;

    invoke-direct {v4, v1}, LX/BFP;-><init>(LX/BFQ;)V

    invoke-interface {v2, v4}, LX/0g8;->b(LX/0fu;)V

    .line 493226
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->j:LX/1qa;

    invoke-virtual {v1}, LX/1qa;->a()V

    .line 493227
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->e()V

    .line 493228
    iget-wide v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aB:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->V:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aB:J

    sub-long/2addr v4, v6

    iget-object v6, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    sget v7, LX/0fe;->bw:I

    const/16 v8, 0xbb8

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    sget-short v5, LX/0fe;->bv:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 493229
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aC:Z

    .line 493230
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    .line 493231
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aA:Z

    if-eqz v1, :cond_1

    .line 493232
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->L:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->b()V

    .line 493233
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x67eb80dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 493234
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 493235
    const-string v0, "permalink_params"

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 493236
    const-string v0, "loading_indicator_state"

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->al:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 493237
    const-string v0, "use_recycler_view"

    iget-boolean v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->az:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 493238
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    if-eqz v0, :cond_0

    .line 493239
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->b(Landroid/os/Bundle;)V

    .line 493240
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->S:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493241
    const-string v0, "feedback"

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 493242
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 493243
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 493244
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    invoke-virtual {v0, p2}, LX/9DG;->a(Landroid/os/Bundle;)V

    .line 493245
    const v0, 0x7f0d24c3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 493246
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0a00d1

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 493247
    const v0, 0x7f0d24c6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 493248
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->az:Z

    invoke-static {v0, p1}, LX/BFS;->a(ZLandroid/view/View;)LX/0g8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    .line 493249
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ag:LX/62C;

    invoke-virtual {v0, p1, v1, v2}, LX/9DG;->a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V

    .line 493250
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ag:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 493251
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    new-instance v1, LX/Btg;

    invoke-direct {v1, p0}, LX/Btg;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 493252
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 493253
    new-instance v0, LX/2je;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aq:LX/195;

    invoke-direct {v0, v1}, LX/2je;-><init>(LX/195;)V

    .line 493254
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    new-instance v2, LX/Bth;

    invoke-direct {v2, p0, v0}, LX/Bth;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;LX/0fx;)V

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 493255
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/BtU;

    invoke-direct {v1, p0}, LX/BtU;-><init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 493256
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->am:LX/62O;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aa:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 493257
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->at:LX/99V;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Y:LX/0g8;

    .line 493258
    iput-object v1, v0, LX/99V;->a:LX/0g8;

    .line 493259
    const/4 v2, 0x0

    .line 493260
    iget-boolean v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aD:Z

    if-nez v0, :cond_0

    .line 493261
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->c()V

    .line 493262
    return-void

    .line 493263
    :cond_0
    const v0, 0x7f0d24c1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a:Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    .line 493264
    const v0, 0x7f0d24c0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b:Landroid/support/design/widget/AppBarLayout;

    .line 493265
    const v0, 0x7f0d24c2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 493266
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uK;

    .line 493267
    new-instance v1, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    invoke-direct {v1}, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;-><init>()V

    invoke-virtual {v0, v1}, LX/1uK;->a(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 493268
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->E:LX/0ad;

    sget-short v1, LX/0fe;->aK:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493269
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b:Landroid/support/design/widget/AppBarLayout;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/AppBarLayout;->setExpanded(Z)V

    .line 493270
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a:Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    invoke-virtual {v0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uS;

    .line 493271
    const/16 v1, 0x13

    .line 493272
    iput v1, v0, LX/1uS;->a:I

    .line 493273
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->W:LX/1Nn;

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a:Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    const/4 v2, 0x1

    .line 493274
    if-nez v1, :cond_3

    .line 493275
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->X:LX/3kp;

    sget-object v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 493276
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->X:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 493277
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 493278
    const v1, 0x7f081afa

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 493279
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a:Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 493280
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->X:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 493281
    :cond_2
    goto :goto_0

    .line 493282
    :cond_3
    iget-object v3, v0, LX/1Nn;->f:LX/0WJ;

    invoke-virtual {v3}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 493283
    iget-object v4, v0, LX/1Nn;->b:LX/0ad;

    sget-char v5, LX/1EB;->x:C

    const p1, 0x7f081405

    iget-object p2, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    invoke-interface {v4, v5, p1, p2}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 493284
    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 493285
    invoke-virtual {v1, v3}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setProfilePhotoUri(Landroid/net/Uri;)V

    .line 493286
    new-instance v3, LX/Ajx;

    invoke-direct {v3, v0, v2}, LX/Ajx;-><init>(LX/1Nn;Z)V

    move-object v3, v3

    .line 493287
    invoke-virtual {v1, v3}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 493288
    iget-object v3, v0, LX/1Nn;->b:LX/0ad;

    sget-short v4, LX/1Nu;->k:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 493289
    iget-object v3, v0, LX/1Nn;->d:LX/0wM;

    iget-object v4, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const v5, 0x7f0207b3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const p1, 0x7f0a00e5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 493290
    new-instance v3, LX/Ajw;

    invoke-direct {v3, v0, v2}, LX/Ajw;-><init>(LX/1Nn;Z)V

    move-object v3, v3

    .line 493291
    invoke-virtual {v1, v3}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setIconClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 493292
    :cond_4
    new-instance v3, LX/ASa;

    iget-object v4, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const v5, 0x7f0202f9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const p1, 0x7f0202f8

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object p1, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const p2, 0x7f0202fa

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {v4, v5, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/ASa;-><init>(LX/0Px;I)V

    .line 493293
    iget-object v4, v0, LX/1Nn;->d:LX/0wM;

    iget-object v5, v0, LX/1Nn;->a:Landroid/content/res/Resources;

    const p1, 0x7f0a00e5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/ASa;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 493294
    move-object v3, v3

    .line 493295
    invoke-virtual {v1, v3}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method
