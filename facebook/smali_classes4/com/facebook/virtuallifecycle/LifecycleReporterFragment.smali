.class public Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/A7P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497259
    const-class v0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    sput-object v0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 497260
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 497261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    return-void
.end method

.method public static a(LX/0gc;LX/A7P;Ljava/lang/String;)Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;
    .locals 3

    .prologue
    .line 497262
    invoke-virtual {p0, p2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    .line 497263
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v1, :cond_1

    .line 497264
    iget-object v1, v0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-ne v1, p1, :cond_0

    .line 497265
    :goto_0
    return-object v0

    .line 497266
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LifecycleReporterFragment.ensureAttached was called twice with the same fragmentTag,but different FragmentLifecycleListeners."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 497267
    :cond_1
    new-instance v1, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;

    invoke-direct {v1}, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;-><init>()V

    .line 497268
    iput-object p1, v1, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    .line 497269
    invoke-virtual {p0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 497270
    if-eqz v0, :cond_2

    .line 497271
    invoke-virtual {v2, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 497272
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, p0

    .line 497273
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 497274
    :goto_1
    invoke-virtual {v2, v1, p2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    move-object v0, v1

    .line 497275
    goto :goto_0

    .line 497276
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 497277
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 497278
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v0, :cond_0

    .line 497279
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    .line 497280
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 497281
    invoke-virtual {v0, v1}, LX/A7P;->a(Landroid/os/Bundle;)V

    .line 497282
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x62010177

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497283
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 497284
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v1, :cond_0

    .line 497285
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x41769047

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 497297
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 497298
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 497286
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 497287
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 497288
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 497289
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v0, :cond_0

    .line 497290
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    invoke-virtual {v0}, LX/A7P;->b()V

    .line 497291
    :cond_0
    return-void
.end method

.method public final onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 497292
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v0, :cond_0

    .line 497293
    goto :goto_1

    .line 497294
    :goto_0
    return v0

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 497295
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 497296
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 497253
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 497254
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x376640f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497255
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v1, :cond_0

    .line 497256
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    invoke-virtual {v1}, LX/A7P;->d()V

    .line 497257
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 497258
    const/16 v1, 0x2b

    const v2, -0x5de3d4fa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyOptionsMenu()V
    .locals 0

    .prologue
    .line 497251
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyOptionsMenu()V

    .line 497252
    return-void
.end method

.method public final onDetach()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4834d682

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497249
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDetach()V

    .line 497250
    const/16 v1, 0x2b

    const v2, 0x1a5a1207

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 0

    .prologue
    .line 497247
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onLowMemory()V

    .line 497248
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 497244
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v0, :cond_0

    .line 497245
    goto :goto_1

    .line 497246
    :goto_0
    return v0

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 497242
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 497243
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5a987ffa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497238
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v1, :cond_0

    .line 497239
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    invoke-virtual {v1}, LX/A7P;->c()V

    .line 497240
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 497241
    const/16 v1, 0x2b

    const v2, -0x45ab00b8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 497236
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 497237
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x221753e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497232
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 497233
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v1, :cond_0

    .line 497234
    iget-object v1, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    invoke-virtual {v1}, LX/A7P;->a()V

    .line 497235
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x37d58f1d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 497226
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    if-eqz v0, :cond_0

    .line 497227
    iget-object v0, p0, Lcom/facebook/virtuallifecycle/LifecycleReporterFragment;->b:LX/A7P;

    .line 497228
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 497229
    invoke-virtual {v0, v1}, LX/A7P;->b(Landroid/os/Bundle;)V

    .line 497230
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 497231
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ebc3ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497224
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 497225
    const/16 v1, 0x2b

    const v2, -0x64abdbea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7abe9ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 497222
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 497223
    const/16 v1, 0x2b

    const v2, -0x7ff08ff3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
