.class public final Lcom/facebook/funnellogger/beacon/BeaconLogger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Zs;

.field public final synthetic b:LX/11D;


# direct methods
.method public constructor <init>(LX/11D;LX/1Zs;)V
    .locals 0

    .prologue
    .line 366627
    iput-object p1, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->b:LX/11D;

    iput-object p2, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 366628
    const-class v1, LX/11D;

    monitor-enter v1

    .line 366629
    :try_start_0
    iget-object v0, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    iget-object v0, v0, LX/1Zs;->beaconIdGenerator:LX/1Zu;

    if-nez v0, :cond_1

    .line 366630
    iget-object v0, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->b:LX/11D;

    iget-object v2, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    .line 366631
    iget-object v3, v0, LX/11D;->c:Landroid/content/Context;

    const-string v4, "funnel_beacon"

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    .line 366632
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, LX/1Zs;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 366633
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_0

    .line 366634
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-nez v4, :cond_0

    .line 366635
    const/4 v3, 0x0

    .line 366636
    :cond_0
    move-object v0, v3

    .line 366637
    if-eqz v0, :cond_3

    .line 366638
    iget-object v2, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    new-instance v3, LX/1Zu;

    invoke-direct {v3, v0}, LX/1Zu;-><init>(Ljava/io/File;)V

    iput-object v3, v2, LX/1Zs;->beaconIdGenerator:LX/1Zu;

    .line 366639
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366640
    iget-object v0, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    iget-object v0, v0, LX/1Zs;->beaconIdGenerator:LX/1Zu;

    invoke-virtual {v0}, LX/1Zu;->a()J

    move-result-wide v0

    .line 366641
    :try_start_1
    iget-object v2, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->b:LX/11D;

    iget-object v2, v2, LX/11D;->b:LX/0Zb;

    iget-object v3, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    iget-object v3, v3, LX/1Zs;->name:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 366642
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 366643
    const-string v3, "beacon_id"

    invoke-static {v0, v1}, LX/1Zu;->a(J)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 366644
    const-string v3, "beacon_session_id"

    invoke-static {v0, v1}, LX/1Zu;->b(J)I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 366645
    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 366646
    :cond_2
    :goto_0
    return-void

    .line 366647
    :cond_3
    :try_start_2
    sget-object v0, LX/11D;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t create %s beacon directory"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    invoke-virtual {v5}, LX/1Zs;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366648
    monitor-exit v1

    goto :goto_0

    .line 366649
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 366650
    :catch_0
    move-exception v0

    .line 366651
    sget-object v1, LX/11D;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t log %s event"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/funnellogger/beacon/BeaconLogger$1;->a:LX/1Zs;

    iget-object v4, v4, LX/1Zs;->name:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
