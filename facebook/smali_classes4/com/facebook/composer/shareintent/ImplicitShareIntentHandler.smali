.class public Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;
.super Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;


# static fields
.field private static S:Z


# instance fields
.field public G:LX/1Bf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/1Kj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/2Ib;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/BKR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/BKe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4hk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:Z

.field public U:LX/4hk;

.field private V:Landroid/os/Bundle;

.field private W:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 441523
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->S:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441453
    invoke-direct {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 441454
    const-string v0, "IS_NATIVE_INTENT"

    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 441455
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 441456
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 441457
    invoke-static {v2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 441458
    new-array v3, v0, [Ljava/lang/CharSequence;

    aput-object v2, v3, v1

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 441459
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 441460
    if-eqz v2, :cond_6

    const-string v3, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "content"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 441461
    :goto_0
    move v0, v0

    .line 441462
    if-eqz v0, :cond_4

    .line 441463
    const-string v0, "com.facebook.platform.extra.TITLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441464
    if-eqz v0, :cond_0

    .line 441465
    const-string v1, "TITLE"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441466
    :cond_0
    const-string v0, "com.facebook.platform.extra.SUBTITLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441467
    if-eqz v0, :cond_1

    .line 441468
    const-string v1, "SUBTITLE"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441469
    :cond_1
    const-string v0, "com.facebook.platform.extra.DESCRIPTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441470
    if-eqz v0, :cond_2

    .line 441471
    const-string v1, "DESCRIPTION"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441472
    :cond_2
    const-string v0, "com.facebook.platform.extra.IMAGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441473
    if-eqz v0, :cond_3

    .line 441474
    const-string v1, "IMAGE"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441475
    :cond_3
    const-string v0, "com.facebook.platform.extra.QUOTE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441476
    if-eqz v0, :cond_4

    .line 441477
    const-string v1, "QUOTE"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441478
    :cond_4
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 441479
    const-string v2, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    sget-object v0, LX/25y;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 441480
    const-string v0, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441481
    if-eqz p5, :cond_5

    .line 441482
    const-string v0, "composer_session_id"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441483
    :cond_5
    const-string v0, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 441484
    const-string v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 441485
    invoke-direct {p0, v1, p4}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/content/Intent;
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 441486
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->O:LX/0ad;

    sget-short v3, LX/ARI;->a:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 441487
    if-eqz v3, :cond_0

    .line 441488
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 441489
    :goto_0
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    const-string v5, "com.facebook.platform.action.request.FEED_DIALOG"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "calling_package_key"

    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "platform_launch_time_ms"

    .line 441490
    iget-wide v9, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->H:J

    move-wide v6, v9

    .line 441491
    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "platform_launch_extras"

    const-string v6, "is_action_send"

    const-string v7, "true"

    const-string v8, "bypass_platform_activity"

    if-nez v3, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v8, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "should_set_simple_result"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.platform.extra.METADATA"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 441492
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->N:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hk;

    iput-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    .line 441493
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 441494
    goto :goto_1
.end method

.method private a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 441495
    const-string v1, "com.facebook.platform.extra.METADATA"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 441496
    const-string v1, "extra_launch_uri"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441497
    if-eqz v1, :cond_6

    .line 441498
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "composer_session_id"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 441499
    :goto_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 441500
    invoke-static {p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->h(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 441501
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441502
    invoke-static {v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441503
    if-eqz v0, :cond_0

    .line 441504
    const-string v1, "link"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    .line 441505
    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 441506
    :goto_2
    return-object v0

    .line 441507
    :cond_1
    invoke-virtual {p0, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->k(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 441508
    invoke-virtual {p0, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->f(Landroid/content/Intent;)LX/0Px;

    move-result-object v6

    .line 441509
    invoke-virtual {p0, v6}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(LX/0Px;)V

    .line 441510
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 441511
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v8, :cond_4

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 441512
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 441513
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 441514
    const-string v10, "type"

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 441515
    iget-object v11, v1, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v1, v11

    .line 441516
    sget-object v11, LX/4gQ;->Video:LX/4gQ;

    if-ne v1, v11, :cond_3

    sget-object v1, LX/4hX;->VIDEO:LX/4hX;

    invoke-virtual {v1}, LX/4hX;->name()Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v9, v10, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441517
    const-string v1, "uri"

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441518
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441519
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 441520
    :cond_3
    sget-object v1, LX/4hX;->PHOTO:LX/4hX;

    invoke-virtual {v1}, LX/4hX;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 441521
    :cond_4
    const-string v0, "MEDIA"

    invoke-virtual {v3, v0, v7}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 441522
    :cond_5
    invoke-virtual {p0, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->l(Landroid/content/Intent;)V

    goto :goto_2

    :cond_6
    move-object v5, v0

    goto/16 :goto_0
.end method

.method private a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 441424
    iput-boolean v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    .line 441425
    invoke-virtual {p0, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->e(Landroid/content/Intent;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 441426
    if-nez v0, :cond_0

    .line 441427
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    .line 441428
    :goto_0
    return-void

    .line 441429
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v2

    if-nez v2, :cond_1

    .line 441430
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v2, Lcom/facebook/share/model/ComposerAppAttribution;

    const-string v3, "828784963877854"

    const-string v4, ""

    const-string v5, "androidKeyHash"

    const-string v6, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 441431
    :cond_1
    const/4 v2, 0x0

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Z)Landroid/content/Intent;

    move-result-object v2

    .line 441432
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v0

    .line 441433
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 441434
    :goto_1
    iget-object v3, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->M:LX/BKe;

    .line 441435
    iget-wide v10, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->H:J

    move-wide v4, v10

    .line 441436
    sget-boolean v6, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->S:Z

    const-string v7, "bypass_platform_composer_activity"

    iget-boolean v8, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    const-string v9, "has_media"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v8, v9, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 441437
    new-instance v7, LX/0P2;

    invoke-direct {v7}, LX/0P2;-><init>()V

    invoke-virtual {v7, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v7

    const-string v8, "is_native_intent"

    const-string v9, "true"

    invoke-virtual {v7, v8, v9}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v7

    .line 441438
    iget-object v8, v3, LX/BKe;->b:LX/4i1;

    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    invoke-virtual {v8, v4, v5, v6, v7}, LX/4i1;->a(JZLjava/util/Map;)V

    .line 441439
    invoke-static {v3}, LX/BKe;->e(LX/BKe;)V

    .line 441440
    sput-boolean v1, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->S:Z

    .line 441441
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->M:LX/BKe;

    .line 441442
    iget-object v3, v0, LX/BKe;->a:LX/11i;

    iget-object v4, v0, LX/BKe;->c:LX/BKd;

    invoke-interface {v3, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 441443
    if-eqz v3, :cond_2

    .line 441444
    const-string v4, "PlatformShareDialogSetup"

    const v5, -0x3c4f68b3

    invoke-static {v3, v4, v5}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v3

    const-string v4, "PlatformComposerLaunch"

    const v5, 0x602cc1f8

    invoke-static {v3, v4, v5}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 441445
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    if-nez v0, :cond_4

    .line 441446
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->Q:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x16

    invoke-interface {v0, v2, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 441447
    :goto_2
    invoke-virtual {p0, v1, v1}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->overridePendingTransition(II)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 441448
    goto :goto_1

    .line 441449
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->L:LX/BKR;

    const v3, 0x7f0d0553

    invoke-virtual {v0, p1, p0, v3, v2}, LX/BKR;->a(Landroid/os/Bundle;Lcom/facebook/base/activity/FbFragmentActivity;ILandroid/content/Intent;)V

    goto :goto_2
.end method

.method private static a(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;LX/1Bf;LX/1Kj;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0WJ;LX/2Ib;LX/BKR;LX/BKe;LX/0Or;LX/0ad;LX/GvB;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;",
            "LX/1Bf;",
            "LX/1Kj;",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/2Ib;",
            "LX/BKR;",
            "LX/BKe;",
            "LX/0Or",
            "<",
            "LX/4hk;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/auth/login/ipc/RedirectableLaunchAuthActivityUtil;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 441584
    iput-object p1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->G:LX/1Bf;

    iput-object p2, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->H:LX/1Kj;

    iput-object p3, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->I:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p4, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->J:LX/0WJ;

    iput-object p5, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->K:LX/2Ib;

    iput-object p6, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->L:LX/BKR;

    iput-object p7, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->M:LX/BKe;

    iput-object p8, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->N:LX/0Or;

    iput-object p9, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->O:LX/0ad;

    iput-object p10, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->P:LX/GvB;

    iput-object p11, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->Q:Lcom/facebook/content/SecureContextHelper;

    iput-object p12, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->R:LX/0kL;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;

    invoke-static {v12}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v1

    check-cast v1, LX/1Bf;

    invoke-static {v12}, LX/1Kj;->a(LX/0QB;)LX/1Kj;

    move-result-object v2

    check-cast v2, LX/1Kj;

    invoke-static {v12}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v12}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v12}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v5

    check-cast v5, LX/2Ib;

    invoke-static {v12}, LX/BKR;->b(LX/0QB;)LX/BKR;

    move-result-object v6

    check-cast v6, LX/BKR;

    invoke-static {v12}, LX/BKe;->a(LX/0QB;)LX/BKe;

    move-result-object v7

    check-cast v7, LX/BKe;

    const/16 v8, 0x2f8d

    invoke-static {v12, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v12}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v12}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v10

    check-cast v10, LX/GvB;

    invoke-static {v12}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v12}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static/range {v0 .. v12}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;LX/1Bf;LX/1Kj;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0WJ;LX/2Ib;LX/BKR;LX/BKe;LX/0Or;LX/0ad;LX/GvB;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;)J
    .locals 4

    .prologue
    .line 441524
    iget-wide v2, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->H:J

    move-wide v0, v2

    .line 441525
    return-wide v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 441526
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441527
    :goto_0
    return-object v0

    .line 441528
    :cond_0
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 441529
    const-string v1, "\"xxRESULTTOKENxx\""

    const-string v2, "%5b%5d"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 441530
    const-string v1, "  (function dispatchEvent(eventName, data){    var event = document.createEvent(\'Event\');    event.initEvent(eventName,true,true);    event.data = data;    document.dispatchEvent(event);  })(\'%s\', \'%s\');"

    const-string v2, "message"

    const/4 v3, 0x1

    const/16 p0, 0xff

    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 441531
    if-nez v0, :cond_1

    .line 441532
    const-string v4, ""

    .line 441533
    :goto_1
    move-object v0, v4

    .line 441534
    invoke-static {v1, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 441535
    :catch_0
    goto :goto_0

    .line 441536
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move v4, v5

    .line 441537
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v4, v7, :cond_6

    .line 441538
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 441539
    invoke-static {v7}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 441540
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 441541
    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 441542
    :cond_3
    if-gt v7, p0, :cond_5

    if-nez v3, :cond_4

    const/16 v8, 0x20

    if-lt v7, v8, :cond_5

    .line 441543
    :cond_4
    const-string v8, "\\x%02X"

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v5

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 441544
    :cond_5
    if-le v7, p0, :cond_2

    .line 441545
    const-string v8, "\\u%04X"

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v5

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 441546
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public static m(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;Landroid/content/Intent;)V
    .locals 9

    .prologue
    .line 441547
    const-string v0, "com.facebook.platform.extra.IN_APP_WEB_SHARE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->W:Z

    .line 441548
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->W:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->g(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 441549
    const-string v0, "extra_composer_moments_object_uuids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 441550
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->L:LX/BKR;

    .line 441551
    iget-object v1, v0, LX/BKR;->c:LX/0Uh;

    const/16 v2, 0x423

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 441552
    if-eqz v0, :cond_0

    .line 441553
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->V:Landroid/os/Bundle;

    invoke-direct {p0, v0, p1}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 441554
    :goto_0
    return-void

    .line 441555
    :cond_0
    const/16 v3, 0x6dc

    const/4 v2, 0x1

    .line 441556
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 441557
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 441558
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 441559
    if-eqz v0, :cond_3

    const-string v6, "image/"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 441560
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 441561
    instance-of v5, v0, Landroid/net/Uri;

    if-eqz v5, :cond_6

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, LX/47E;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 441562
    :goto_1
    move v0, v0

    .line 441563
    if-eqz v0, :cond_2

    .line 441564
    const v0, 0x7f081445

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 441565
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 441566
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->s:LX/0TD;

    new-instance v2, LX/ARG;

    invoke-direct {v2, p0, p1, v3}, LX/ARG;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 441567
    iget-object v2, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->u:LX/1Ck;

    const-string v3, "any"

    .line 441568
    new-instance v4, LX/ARH;

    invoke-direct {v4, p0, v0}, LX/ARH;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Landroid/support/v4/app/DialogFragment;)V

    move-object v0, v4

    .line 441569
    invoke-virtual {v2, v3, v1, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 441570
    :cond_1
    :goto_2
    goto :goto_0

    .line 441571
    :cond_2
    invoke-virtual {p0, p1, v3}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Landroid/content/Intent;I)Ljava/lang/Runnable;

    move-result-object v0

    .line 441572
    if-eqz v0, :cond_1

    .line 441573
    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 441574
    :cond_3
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 441575
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 441576
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 441577
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v5, v4

    :goto_3
    if-ge v5, v7, :cond_6

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 441578
    instance-of v8, v0, Landroid/net/Uri;

    if-eqz v8, :cond_4

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, LX/47E;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 441579
    goto :goto_1

    .line 441580
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    .line 441581
    :cond_5
    const-string v0, "is_draft"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 441582
    goto :goto_1

    :cond_6
    move v0, v4

    .line 441583
    goto :goto_1
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 441450
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->J:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    return v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 441451
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->P:LX/GvB;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, LX/GvB;->a(Landroid/app/Activity;Z)V

    .line 441452
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;I)Ljava/lang/Runnable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441336
    const-string v0, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441337
    if-nez v0, :cond_0

    .line 441338
    invoke-super {p0, p1, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Landroid/content/Intent;I)Ljava/lang/Runnable;

    move-result-object v0

    .line 441339
    :goto_0
    return-object v0

    .line 441340
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->H:LX/1Kj;

    .line 441341
    iget-object v2, v1, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0xe0006

    const-string p2, "ComposerLaunchTTIPlatformShare"

    invoke-interface {v2, v3, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;)V

    .line 441342
    invoke-direct {p0, v0, p1}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 441343
    if-nez v0, :cond_1

    .line 441344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->setResult(I)V

    .line 441345
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    .line 441346
    const/4 v0, 0x0

    goto :goto_0

    .line 441347
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 441348
    new-instance v0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler$2;

    invoke-direct {v0, p0, v1}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler$2;-><init>(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 441349
    const-string v0, "composer"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 441350
    const-class v0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 441351
    invoke-super {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b(Landroid/os/Bundle;)V

    .line 441352
    const v0, 0x7f0308dd

    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->setContentView(I)V

    .line 441353
    iput-object p1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->V:Landroid/os/Bundle;

    .line 441354
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->m()V

    .line 441355
    return-void
.end method

.method public final finish()V
    .locals 4

    .prologue
    .line 441356
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->W:Z

    if-eqz v0, :cond_0

    .line 441357
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 441358
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 441359
    const-string v2, "BrowserLiteIntent.EXTRA_JS_TO_EXECUTE"

    const-string v3, "com.facebook.platform.extra.NEXT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441360
    const-string v2, "BrowserLiteIntent.EXTRA_REFERER"

    const-string v3, "com.facebook.platform.extra.HOST_URL"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441361
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->G:LX/1Bf;

    .line 441362
    invoke-static {v0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result v2

    invoke-static {p0, v2, v1}, LX/049;->h(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 441363
    :cond_0
    invoke-super {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->finish()V

    .line 441364
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 441365
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->V:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 441366
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->V:Landroid/os/Bundle;

    const-string v1, "is_web_share_redirect"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->W:Z

    .line 441367
    :goto_0
    return-void

    .line 441368
    :cond_0
    invoke-direct {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 441369
    invoke-direct {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->r()V

    goto :goto_0

    .line 441370
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->m(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 441371
    invoke-super {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->m()V

    .line 441372
    invoke-static {p0, p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 441373
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 441374
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->onActivityResult(IILandroid/content/Intent;)V

    .line 441375
    const v0, 0x2e21e1eb

    if-ne p1, v0, :cond_1

    .line 441376
    :cond_0
    :goto_0
    return-void

    .line 441377
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->m()V

    .line 441378
    iget-object v5, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->r:LX/1Kj;

    .line 441379
    iget-object v6, v5, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v7, 0xe0005

    const-string v8, "ComposerLaunchTTIExternalShare"

    invoke-interface {v6, v7, v8}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 441380
    iget-object v5, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->r:LX/1Kj;

    .line 441381
    iget-object v6, v5, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v7, 0xe0006

    const-string v8, "ComposerLaunchTTIPlatformShare"

    invoke-interface {v6, v7, v8}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 441382
    const-wide/16 v5, -0x1

    iput-wide v5, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->H:J

    .line 441383
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->i(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 441384
    invoke-virtual {p0, p2, p3}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->setResult(ILandroid/content/Intent;)V

    .line 441385
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    goto :goto_0

    .line 441386
    :cond_2
    const/16 v0, 0x16

    if-ne p1, v0, :cond_3

    .line 441387
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    goto :goto_0

    .line 441388
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    if-eqz v0, :cond_4

    .line 441389
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    invoke-virtual {v0, p1, p2, p3}, LX/4hk;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 441390
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    if-nez v0, :cond_0

    .line 441391
    if-eq p2, v1, :cond_5

    .line 441392
    sparse-switch p1, :sswitch_data_0

    .line 441393
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    goto :goto_0

    .line 441394
    :sswitch_0
    invoke-virtual {p0, v4}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->setResult(I)V

    goto :goto_1

    .line 441395
    :cond_5
    const/16 v0, 0x8a2

    if-ne p1, v0, :cond_6

    .line 441396
    new-instance v0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler$1;

    invoke-direct {v0, p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler$1;-><init>(Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;)V

    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 441397
    :cond_6
    sparse-switch p1, :sswitch_data_1

    .line 441398
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unexpected request code received %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441399
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->finish()V

    goto :goto_0

    .line 441400
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_draft"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "is_uploading_media"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 441401
    :cond_7
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->I:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 441402
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->R:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082735

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_2

    .line 441403
    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/publish/ComposerPublishService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 441404
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 441405
    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    .line 441406
    :sswitch_2
    invoke-virtual {p0, v1}, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->setResult(I)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x6dc -> :sswitch_0
        0x8a2 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2a -> :sswitch_2
        0x6dc -> :sswitch_1
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 441407
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    if-eqz v0, :cond_0

    .line 441408
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->L:LX/BKR;

    invoke-virtual {v0}, LX/BKR;->b()V

    .line 441409
    :goto_0
    return-void

    .line 441410
    :cond_0
    invoke-super {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->onBackPressed()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x28318c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 441411
    iget-object v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    if-eqz v1, :cond_0

    .line 441412
    iget-object v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    invoke-virtual {v1}, LX/4hk;->a()V

    .line 441413
    :cond_0
    invoke-super {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->onDestroy()V

    .line 441414
    const/16 v1, 0x23

    const v2, -0x5ef5c9b7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 441415
    invoke-super {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 441416
    const-string v0, "is_web_share_redirect"

    iget-boolean v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 441417
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    if-eqz v0, :cond_0

    .line 441418
    iget-object v0, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->U:LX/4hk;

    invoke-virtual {v0, p1}, LX/4hk;->c(Landroid/os/Bundle;)V

    .line 441419
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2b0873a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 441420
    iget-boolean v1, p0, Lcom/facebook/composer/shareintent/ImplicitShareIntentHandler;->T:Z

    if-eqz v1, :cond_0

    .line 441421
    invoke-static {p0}, LX/BKR;->a(Landroid/app/Activity;)V

    .line 441422
    :cond_0
    invoke-super {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->onStop()V

    .line 441423
    const/16 v1, 0x23

    const v2, 0x78f87119

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
