.class public abstract Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:LX/3HM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/BJ7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ARK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:Z

.field public H:J

.field public I:LX/0i5;

.field private J:Ljava/lang/Runnable;

.field public p:Landroid/app/ActivityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1Kj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/47E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/74n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441793
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Landroid/app/ActivityManager;LX/0kL;LX/1Kj;LX/0TD;Ljava/util/concurrent/Executor;LX/1Ck;LX/03V;LX/47E;LX/74n;LX/1Kf;LX/0i4;LX/3HM;LX/0tX;LX/0ad;LX/BJ7;LX/0So;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;",
            "Landroid/app/ActivityManager;",
            "LX/0kL;",
            "LX/1Kj;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "LX/1Ck;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/47E;",
            "LX/74n;",
            "LX/1Kf;",
            "LX/0i4;",
            "LX/3HM;",
            "LX/0tX;",
            "LX/0ad;",
            "LX/BJ7;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "LX/ARK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441721
    iput-object p1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->p:Landroid/app/ActivityManager;

    iput-object p2, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q:LX/0kL;

    iput-object p3, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->r:LX/1Kj;

    iput-object p4, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->s:LX/0TD;

    iput-object p5, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->t:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->u:LX/1Ck;

    iput-object p7, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->v:LX/03V;

    iput-object p8, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->w:LX/47E;

    iput-object p9, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->x:LX/74n;

    iput-object p10, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->y:LX/1Kf;

    iput-object p11, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->z:LX/0i4;

    iput-object p12, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->A:LX/3HM;

    iput-object p13, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->B:LX/0tX;

    iput-object p14, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->C:LX/0ad;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->D:LX/BJ7;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->E:LX/0So;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->F:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v18

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    invoke-static/range {v18 .. v18}, LX/0VU;->a(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-static/range {v18 .. v18}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static/range {v18 .. v18}, LX/1Kj;->a(LX/0QB;)LX/1Kj;

    move-result-object v4

    check-cast v4, LX/1Kj;

    invoke-static/range {v18 .. v18}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static/range {v18 .. v18}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static/range {v18 .. v18}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static/range {v18 .. v18}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {v18 .. v18}, LX/47E;->a(LX/0QB;)LX/47E;

    move-result-object v9

    check-cast v9, LX/47E;

    invoke-static/range {v18 .. v18}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v10

    check-cast v10, LX/74n;

    invoke-static/range {v18 .. v18}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v11

    check-cast v11, LX/1Kf;

    const-class v12, LX/0i4;

    move-object/from16 v0, v18

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0i4;

    invoke-static/range {v18 .. v18}, LX/3HM;->a(LX/0QB;)LX/3HM;

    move-result-object v13

    check-cast v13, LX/3HM;

    invoke-static/range {v18 .. v18}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v14

    check-cast v14, LX/0tX;

    invoke-static/range {v18 .. v18}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {v18 .. v18}, LX/BJ7;->a(LX/0QB;)LX/BJ7;

    move-result-object v16

    check-cast v16, LX/BJ7;

    invoke-static/range {v18 .. v18}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v17

    check-cast v17, LX/0So;

    const/16 v19, 0x19d1

    invoke-static/range {v18 .. v19}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v1 .. v18}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Landroid/app/ActivityManager;LX/0kL;LX/1Kj;LX/0TD;Ljava/util/concurrent/Executor;LX/1Ck;LX/03V;LX/47E;LX/74n;LX/1Kf;LX/0i4;LX/3HM;LX/0tX;LX/0ad;LX/BJ7;LX/0So;LX/0Ot;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 441722
    if-nez p0, :cond_1

    .line 441723
    :cond_0
    :goto_0
    return-object v0

    .line 441724
    :cond_1
    sget-object v1, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    .line 441725
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 441726
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 441727
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 441728
    const-string v0, "is_draft"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static final h(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 441703
    const-string v0, "text/plain"

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final i(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 441729
    const-string v0, "application/instant-games"

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 441730
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/content/Intent;I)Ljava/lang/Runnable;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441731
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->r:LX/1Kj;

    .line 441732
    iget-object v1, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xe0005

    const-string v3, "ComposerLaunchTTIExternalShare"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;)V

    .line 441733
    const-string v0, "is_draft"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441734
    const-string v0, "story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441735
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 441736
    iget-object v2, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->D:LX/BJ7;

    .line 441737
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/BJ6;->DRAFT_NOTIFICATION_CLICKED:LX/BJ6;

    iget-object v6, v6, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "story_id"

    invoke-virtual {v4, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v2, v4}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 441738
    new-instance v2, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v4, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v2, v0, v4}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;)V

    .line 441739
    iget-object v4, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->A:LX/3HM;

    invoke-virtual {v4, v2}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0zO;

    move-result-object v2

    .line 441740
    iget-object v4, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->B:LX/0tX;

    invoke-virtual {v4, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 441741
    const v4, -0x58a34bb5

    :try_start_0
    invoke-static {v2, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 441742
    if-nez v2, :cond_2

    move-object v2, v3

    .line 441743
    :goto_0
    move-object v0, v2

    .line 441744
    move-object v1, v0

    .line 441745
    :goto_1
    if-nez v1, :cond_1

    .line 441746
    const/4 v0, 0x0

    .line 441747
    :goto_2
    return-object v0

    .line 441748
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->e(Landroid/content/Intent;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 441749
    :cond_1
    new-instance v0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler$4;

    invoke-direct {v0, p0, v1, p2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler$4;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;I)V

    goto :goto_2

    .line 441750
    :catch_0
    :goto_3
    move-object v2, v3

    goto :goto_0

    .line 441751
    :cond_2
    iget-object v4, v2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v4

    .line 441752
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 441753
    if-nez v2, :cond_3

    move-object v2, v3

    .line 441754
    goto :goto_0

    .line 441755
    :cond_3
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 441756
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v8, v3

    .line 441757
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    sget-object v4, LX/2rt;->SHARE:LX/2rt;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    sget-object v6, LX/21D;->NOTIFICATIONS:LX/21D;

    invoke-virtual {v4, v6}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    const-string v6, "draftFromNotification"

    invoke-virtual {v4, v6}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLegacyApiStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCacheId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    new-instance v4, LX/89K;

    invoke-direct {v4}, LX/89K;-><init>()V

    sget-object v4, LX/ARR;->a:LX/89L;

    invoke-static {v4}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    .line 441758
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v5

    :goto_4
    if-ge v6, v10, :cond_6

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 441759
    invoke-static {v3}, LX/1VO;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 441760
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 441761
    invoke-static {v3}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 441762
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 441763
    const-string v11, "target_url"

    invoke-virtual {v3, v11}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    move-object v3, v11

    .line 441764
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 441765
    :cond_4
    invoke-static {v3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 441766
    :cond_5
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_4

    .line 441767
    :cond_6
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 441768
    sget-object v3, LX/21D;->NOTIFICATIONS:LX/21D;

    const-string v4, "draftFromNotificationWithAttachments"

    invoke-static {v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    .line 441769
    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v4, v4

    .line 441770
    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    move-object v4, v3

    .line 441771
    :cond_7
    invoke-static {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 441772
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v3

    .line 441773
    if-eqz v3, :cond_9

    .line 441774
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 441775
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    :goto_5
    if-ge v5, v8, :cond_8

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    .line 441776
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    .line 441777
    iput-object v3, v9, LX/5Rc;->b:Ljava/lang/String;

    .line 441778
    move-object v3, v9

    .line 441779
    invoke-virtual {v3}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 441780
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5

    .line 441781
    :cond_8
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 441782
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    .line 441783
    if-eqz v2, :cond_a

    .line 441784
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    new-instance v5, LX/5m9;

    invoke-direct {v5}, LX/5m9;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v6

    .line 441785
    iput-object v6, v5, LX/5m9;->f:Ljava/lang/String;

    .line 441786
    move-object v5, v5

    .line 441787
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    .line 441788
    iput-object v2, v5, LX/5m9;->h:Ljava/lang/String;

    .line 441789
    move-object v2, v5

    .line 441790
    invoke-virtual {v2}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v2

    invoke-virtual {v2}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 441791
    :cond_a
    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    goto/16 :goto_0

    .line 441792
    :catch_1
    goto/16 :goto_3
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 441794
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v4, :cond_1

    .line 441795
    :cond_0
    :goto_0
    return-void

    .line 441796
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 441797
    invoke-static {v0}, LX/7kq;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 441798
    add-int/lit8 v0, v1, 0x1

    .line 441799
    if-gt v0, v4, :cond_3

    .line 441800
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 441801
    :cond_3
    if-lez v0, :cond_0

    .line 441802
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q:LX/0kL;

    new-instance v2, LX/27k;

    if-ne v0, v4, :cond_4

    const v0, 0x7f081410

    :goto_3
    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    :cond_4
    const v0, 0x7f081411

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 441803
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441804
    :cond_0
    :goto_0
    return-void

    .line 441805
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->G:Z

    if-nez v0, :cond_2

    .line 441806
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 441807
    :cond_2
    iput-object p1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->J:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 441704
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 441705
    new-instance v0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler$1;

    invoke-direct {v0, p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler$1;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;)V

    .line 441706
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 441707
    invoke-virtual {p0, v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->k(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 441708
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->z:LX/0i4;

    invoke-virtual {v1, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->I:LX/0i5;

    .line 441709
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v3, v1, v2

    .line 441710
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 441711
    iget-object v3, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->I:LX/0i5;

    new-instance p1, LX/ARF;

    invoke-direct {p1, p0, v2}, LX/ARF;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v3, v1, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 441712
    move-object v1, v2

    .line 441713
    move-object v1, v1

    .line 441714
    :goto_0
    move-object v1, v1

    .line 441715
    new-instance v2, LX/ARE;

    invoke-direct {v2, p0, v0}, LX/ARE;-><init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->t:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 441716
    return-void

    .line 441717
    :cond_0
    invoke-static {v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->h(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->g(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->i(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 441718
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 441719
    :cond_2
    invoke-virtual {p0, v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->l(Landroid/content/Intent;)V

    .line 441720
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method

.method public final e(Landroid/content/Intent;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 441585
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 441586
    sget-object v0, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    const-string v1, "textPostFromThirdParty"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 441587
    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v1, v1

    .line 441588
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 441589
    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->h(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 441590
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441591
    invoke-static {v1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441592
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 441593
    sget-object v0, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    const-string v2, "shareLinkFromThirdParty"

    invoke-static {v1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 441594
    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v2, v2

    .line 441595
    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v0, v0

    .line 441596
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setExternalRefName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "ANDROID_EXTERNAL_COMPOSER"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 441597
    :goto_1
    move-object v0, v0

    .line 441598
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 441599
    :cond_2
    invoke-virtual {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->k(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 441600
    invoke-virtual {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->f(Landroid/content/Intent;)LX/0Px;

    move-result-object v0

    .line 441601
    const/4 v2, 0x0

    .line 441602
    const-string v1, "extra_composer_moments_object_uuids"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 441603
    invoke-static {v4}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 441604
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v1, v5, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 441605
    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 441606
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 441607
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 441608
    iput-object v1, v5, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    .line 441609
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    move v1, v2

    .line 441610
    goto :goto_3

    .line 441611
    :cond_5
    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(LX/0Px;)V

    .line 441612
    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    const-string v2, "mediaPostFromThirdParty"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 441613
    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v2, v2

    .line 441614
    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    goto :goto_0

    .line 441615
    :cond_6
    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->i(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 441616
    const-string v0, "extras_game_share_extras"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;

    .line 441617
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->F:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ARK;

    invoke-virtual {v1, v0}, LX/ARK;->a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 441618
    if-nez v0, :cond_0

    .line 441619
    invoke-virtual {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->l(Landroid/content/Intent;)V

    move-object v0, v2

    .line 441620
    goto/16 :goto_1

    .line 441621
    :cond_7
    invoke-virtual {p0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->l(Landroid/content/Intent;)V

    move-object v0, v2

    .line 441622
    goto/16 :goto_1
.end method

.method public final f(Landroid/content/Intent;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 441623
    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q(Landroid/content/Intent;)Z

    move-result v0

    .line 441624
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 441625
    if-eqz v0, :cond_3

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 441626
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 441627
    :cond_0
    :goto_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 441628
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    .line 441629
    instance-of v4, v1, Landroid/net/Uri;

    if-eqz v4, :cond_1

    .line 441630
    iget-object v4, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->w:LX/47E;

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b()Ljava/lang/String;

    move-result-object v5

    .line 441631
    const-string v6, "content"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v1}, LX/47E;->a(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 441632
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v6

    .line 441633
    iget-object v7, v4, LX/47E;->a:Landroid/content/ContentResolver;

    invoke-virtual {v7, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 441634
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string p1, "."

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6, v7}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 441635
    :try_start_0
    new-instance v6, LX/2Qt;

    iget-object v8, v4, LX/47E;->b:Landroid/content/Context;

    iget-object p1, v4, LX/47E;->c:LX/0en;

    invoke-direct {v6, v8, p1}, LX/2Qt;-><init>(Landroid/content/Context;LX/0en;)V

    .line 441636
    iget-object v8, v4, LX/47E;->a:Landroid/content/ContentResolver;

    invoke-virtual {v8, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    .line 441637
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    .line 441638
    invoke-virtual {v6, p1, v7}, LX/2Qt;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 441639
    iget-object p1, v4, LX/47E;->c:LX/0en;

    invoke-virtual {p1, v8, v6}, LX/0en;->a(Ljava/io/InputStream;Ljava/io/File;)V

    .line 441640
    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch LX/0Fu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 441641
    :cond_2
    :goto_2
    move-object v1, v1

    .line 441642
    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 441643
    :cond_3
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 441644
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto/16 :goto_0

    .line 441645
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 441646
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 441647
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->x:LX/74n;

    invoke-static {v0, v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/74n;)LX/0Px;

    move-result-object v0

    .line 441648
    :goto_3
    return-object v0

    .line 441649
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 441650
    goto :goto_3

    .line 441651
    :catch_0
    move-exception v6

    .line 441652
    iget-object v8, v4, LX/47E;->d:LX/03V;

    const-string p1, "%s %s"

    const-string v0, "Could not open a temp image file:"

    invoke-static {p1, v0, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v5, v7, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 441653
    :catch_1
    move-exception v6

    .line 441654
    iget-object v8, v4, LX/47E;->d:LX/03V;

    const-string p1, "%s %s"

    const-string v0, "Could not write a temp image file:"

    invoke-static {p1, v0, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v5, v7, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final iy_()V
    .locals 2

    .prologue
    .line 441655
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->m()V

    .line 441656
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->E:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->H:J

    .line 441657
    return-void
.end method

.method public final k(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 441696
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    .line 441697
    :goto_0
    if-nez v2, :cond_0

    .line 441698
    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 441699
    if-nez v2, :cond_0

    .line 441700
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 441701
    if-nez v2, :cond_0

    const-string v2, "*/*"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    .line 441702
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public abstract l()V
.end method

.method public final l(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 441658
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->v:LX/03V;

    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " app: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 441659
    iput-boolean v4, v1, LX/0VK;->d:Z

    .line 441660
    move-object v1, v1

    .line 441661
    iput v4, v1, LX/0VK;->e:I

    .line 441662
    move-object v1, v1

    .line 441663
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 441664
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08140f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 441665
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->finish()V

    .line 441666
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 441667
    invoke-static {p0, p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 441668
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6302a5c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 441669
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->G:Z

    .line 441670
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 441671
    const/16 v1, 0x23

    const v2, -0x74c9c68e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPostResume()V
    .locals 2

    .prologue
    .line 441672
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostResume()V

    .line 441673
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->G:Z

    .line 441674
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->J:Ljava/lang/Runnable;

    .line 441675
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->J:Ljava/lang/Runnable;

    .line 441676
    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Ljava/lang/Runnable;)V

    .line 441677
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 441678
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->G:Z

    .line 441679
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 441680
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7788f1a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 441681
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 441682
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->u:LX/1Ck;

    if-eqz v1, :cond_0

    .line 441683
    iget-object v1, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->u:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 441684
    :cond_0
    const/16 v1, 0x23

    const v2, -0x757de6a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 441685
    invoke-virtual {p0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 441686
    if-eqz v0, :cond_0

    .line 441687
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 441688
    :goto_0
    return-object v0

    .line 441689
    :cond_0
    const-string v0, "android.permission.GET_TASKS"

    invoke-virtual {p0, v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 441690
    const-string v0, "external_ref_missing_permission"

    goto :goto_0

    .line 441691
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->p:Landroid/app/ActivityManager;

    invoke-virtual {v0, v1, v1}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v0

    .line 441692
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 441693
    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v2, :cond_2

    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 441694
    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 441695
    :cond_3
    const-string v0, "external_ref_unavailable"

    goto :goto_0
.end method
