.class public Lcom/facebook/presence/ThreadPresenceManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements Ljava/lang/Runnable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile u:Lcom/facebook/presence/ThreadPresenceManager;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:LX/0Xl;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/13Q;

.field private final g:LX/0Zb;

.field private final h:LX/0Yb;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/concurrent/ScheduledExecutorService;

.field private final m:LX/2gD;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:J

.field public r:I

.field public final s:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field private final t:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/9l9;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564801
    const-class v0, Lcom/facebook/presence/ThreadPresenceManager;

    sput-object v0, Lcom/facebook/presence/ThreadPresenceManager;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Xl;LX/0Or;LX/0Ot;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;LX/13Q;LX/0Zb;LX/0Or;LX/2gD;)V
    .locals 9
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/presence/annotations/IsThreadPresenceEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/push/prefs/IsMobileOnlineAvailabilityEnabled;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/Executor;",
            "LX/13Q;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2gD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564781
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    .line 564782
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->q:J

    .line 564783
    sget-object v2, LX/2gE;->THREAD_PRESENCE_CAPABILITY_NONE:LX/2gE;

    invoke-virtual {v2}, LX/2gE;->getValue()I

    move-result v2

    iput v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->r:I

    .line 564784
    iput-object p1, p0, Lcom/facebook/presence/ThreadPresenceManager;->b:LX/0Ot;

    .line 564785
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->c:Ljava/util/concurrent/Executor;

    .line 564786
    iput-object p2, p0, Lcom/facebook/presence/ThreadPresenceManager;->d:LX/0Xl;

    .line 564787
    iput-object p4, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    .line 564788
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->f:LX/13Q;

    .line 564789
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->g:LX/0Zb;

    .line 564790
    iput-object p3, p0, Lcom/facebook/presence/ThreadPresenceManager;->i:LX/0Or;

    .line 564791
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->s:LX/0Xu;

    .line 564792
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    .line 564793
    iput-object p6, p0, Lcom/facebook/presence/ThreadPresenceManager;->l:Ljava/util/concurrent/ScheduledExecutorService;

    .line 564794
    iput-object p5, p0, Lcom/facebook/presence/ThreadPresenceManager;->j:LX/0Or;

    .line 564795
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->k:LX/0Or;

    .line 564796
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->m:LX/2gD;

    .line 564797
    iget-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->d:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.presence.ACTION_THREAD_PRESENCE_CHANGED"

    new-instance v4, LX/2gF;

    invoke-direct {v4, p0}, LX/2gF;-><init>(Lcom/facebook/presence/ThreadPresenceManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v4, LX/2gG;

    invoke-direct {v4, p0}, LX/2gG;-><init>(Lcom/facebook/presence/ThreadPresenceManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v4, LX/2gH;

    invoke-direct {v4, p0}, LX/2gH;-><init>(Lcom/facebook/presence/ThreadPresenceManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->h:LX/0Yb;

    .line 564798
    iget-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->h:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 564799
    iget-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->l:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x5

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v3, p0

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 564800
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/presence/ThreadPresenceManager;
    .locals 15

    .prologue
    .line 564767
    sget-object v0, Lcom/facebook/presence/ThreadPresenceManager;->u:Lcom/facebook/presence/ThreadPresenceManager;

    if-nez v0, :cond_1

    .line 564768
    const-class v1, Lcom/facebook/presence/ThreadPresenceManager;

    monitor-enter v1

    .line 564769
    :try_start_0
    sget-object v0, Lcom/facebook/presence/ThreadPresenceManager;->u:Lcom/facebook/presence/ThreadPresenceManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 564770
    if-eqz v2, :cond_0

    .line 564771
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 564772
    new-instance v3, Lcom/facebook/presence/ThreadPresenceManager;

    const/16 v4, 0x271

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    const/16 v6, 0x155a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2e3

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x155d

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v11

    check-cast v11, LX/13Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    const/16 v13, 0x15e7

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v0}, LX/2gD;->a(LX/0QB;)LX/2gD;

    move-result-object v14

    check-cast v14, LX/2gD;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/presence/ThreadPresenceManager;-><init>(LX/0Ot;LX/0Xl;LX/0Or;LX/0Ot;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;LX/13Q;LX/0Zb;LX/0Or;LX/2gD;)V

    .line 564773
    move-object v0, v3

    .line 564774
    sput-object v0, Lcom/facebook/presence/ThreadPresenceManager;->u:Lcom/facebook/presence/ThreadPresenceManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564775
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 564776
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 564777
    :cond_1
    sget-object v0, Lcom/facebook/presence/ThreadPresenceManager;->u:Lcom/facebook/presence/ThreadPresenceManager;

    return-object v0

    .line 564778
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 564779
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 564646
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 564647
    iget-wide v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x61a8

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 564648
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/facebook/presence/ThreadPresenceManager;->a(Lcom/facebook/presence/ThreadPresenceManager;Ljava/lang/String;I)V

    .line 564649
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->f:LX/13Q;

    const-string v1, "thread_presence_ping_post"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 564650
    :cond_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l9;

    .line 564651
    const/4 v3, 0x0

    .line 564652
    iget-object v1, v0, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9l8;

    .line 564653
    iget-wide v8, v1, LX/9l8;->e:J

    sub-long v8, v4, v8

    .line 564654
    const-wide/16 v10, 0x7530

    cmp-long v8, v8, v10

    if-lez v8, :cond_5

    .line 564655
    iget-object v3, v1, LX/9l8;->c:Ljava/lang/String;

    iget-object v1, v1, LX/9l8;->d:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 564656
    iget-object v3, v0, LX/9l9;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 564657
    iget-object v3, v0, LX/9l9;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 564658
    :cond_2
    iget-object v3, v0, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    :goto_2
    move v3, v1

    .line 564659
    goto :goto_1

    .line 564660
    :cond_3
    if-eqz v3, :cond_1

    .line 564661
    iget-object v0, v0, LX/9l9;->a:Lcom/facebook/user/model/UserKey;

    .line 564662
    iget-object v1, p0, Lcom/facebook/presence/ThreadPresenceManager;->c:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/facebook/presence/ThreadPresenceManager$4;

    invoke-direct {v3, p0, v0}, Lcom/facebook/presence/ThreadPresenceManager$4;-><init>(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)V

    const v0, -0x394a13c3

    invoke-static {v1, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 564663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 564664
    :cond_4
    monitor-exit p0

    return-void

    :cond_5
    move v1, v3

    goto :goto_2
.end method

.method private static a(Lcom/facebook/presence/ThreadPresenceManager;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 564736
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 564737
    :cond_0
    :goto_0
    return-void

    .line 564738
    :cond_1
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 564739
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/presence/ThreadPresenceManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 564740
    const/4 v0, 0x1

    if-ne p2, v0, :cond_4

    .line 564741
    iget-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1388

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 564742
    :cond_2
    iput-object p1, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    .line 564743
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    .line 564744
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->q:J

    .line 564745
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->o:Ljava/lang/String;

    .line 564746
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564747
    new-instance v2, LX/6mg;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget v3, p0, Lcom/facebook/presence/ThreadPresenceManager;->r:I

    .line 564748
    sget-object v4, LX/2gE;->THREAD_PRESENCE_CAPABILITY_MASK:LX/2gE;

    invoke-virtual {v4}, LX/2gE;->getValue()I

    move-result v4

    and-int/2addr v4, v3

    shl-int/lit8 v4, v4, 0x2

    or-int/2addr v4, p2

    move v3, v4

    .line 564749
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v1, v0, v3}, LX/6mg;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 564750
    const/4 v0, 0x2

    if-ne p2, v0, :cond_8

    const-string v0, "thread_presence_ack_post"

    .line 564751
    :goto_2
    iget-object v1, p0, Lcom/facebook/presence/ThreadPresenceManager;->m:LX/2gD;

    invoke-virtual {v1, v2, v0}, LX/2gD;->a(LX/6mg;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 564752
    :cond_4
    if-nez p2, :cond_7

    iget-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_7

    .line 564753
    iget-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->q:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/presence/ThreadPresenceManager;->q:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1388

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 564754
    :cond_5
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 564755
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    sub-long/2addr v2, v4

    .line 564756
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "thread_presence_local_duration"

    invoke-direct {v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "duration_ms"

    invoke-virtual {v0, v4, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "other_user_id"

    invoke-virtual {v0, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "thread_presence"

    .line 564757
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 564758
    move-object v0, v0

    .line 564759
    iget-object v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->g:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 564760
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->f:LX/13Q;

    const-string v2, "thread_presence_local_duration_instance"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    .line 564761
    :cond_6
    iget-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->q:J

    .line 564762
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->o:Ljava/lang/String;

    .line 564763
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/presence/ThreadPresenceManager;->p:J

    .line 564764
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    goto/16 :goto_1

    .line 564765
    :cond_7
    const/4 v0, 0x2

    if-eq p2, v0, :cond_3

    goto/16 :goto_0

    .line 564766
    :cond_8
    const-string v0, "thread_presence_post"

    goto/16 :goto_2
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 564733
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564734
    const/4 v0, 0x0

    .line 564735
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/presence/ThreadPresenceManager;Landroid/content/Intent;)V
    .locals 14

    .prologue
    .line 564699
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 564700
    :cond_0
    :goto_0
    return-void

    .line 564701
    :cond_1
    const-string v0, "extra_device_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 564702
    const-string v0, "extra_app_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 564703
    const-string v0, "extra_new_state"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 564704
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 564705
    const-string v0, "extra_user_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/user/model/UserKey;

    .line 564706
    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 564707
    if-eqz v7, :cond_0

    .line 564708
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->f:LX/13Q;

    const-string v4, "thread_presence_received"

    invoke-virtual {v0, v4}, LX/13Q;->a(Ljava/lang/String;)V

    .line 564709
    invoke-static {p0, v7}, Lcom/facebook/presence/ThreadPresenceManager;->c(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)LX/9l9;

    move-result-object v9

    .line 564710
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 564711
    shr-int/lit8 v0, v3, 0x2

    sget-object v6, LX/2gE;->THREAD_PRESENCE_CAPABILITY_MASK:LX/2gE;

    invoke-virtual {v6}, LX/2gE;->getValue()I

    move-result v6

    and-int/2addr v0, v6

    move v6, v0

    .line 564712
    and-int/lit8 v10, v3, 0x3

    .line 564713
    if-nez v10, :cond_3

    .line 564714
    iget-object v0, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v8}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 564715
    iget-object v0, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v8}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l8;

    .line 564716
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v11, "thread_presence_remote_duration"

    invoke-direct {v3, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v11, "duration_ms"

    iget-wide v12, v0, LX/9l8;->e:J

    sub-long v12, v4, v12

    invoke-virtual {v3, v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v11, "thread_presence"

    .line 564717
    iput-object v11, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 564718
    move-object v3, v3

    .line 564719
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 564720
    const-string v11, "other_user_id"

    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 564721
    :cond_2
    iget-object v11, p0, Lcom/facebook/presence/ThreadPresenceManager;->g:LX/0Zb;

    invoke-interface {v11, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 564722
    iget-object v3, p0, Lcom/facebook/presence/ThreadPresenceManager;->f:LX/13Q;

    const-string v11, "thread_presence_remote_duration_instance"

    invoke-virtual {v3, v11}, LX/13Q;->a(Ljava/lang/String;)V

    .line 564723
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 564724
    iget-object v0, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v8}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 564725
    :cond_3
    const/4 v0, 0x1

    if-eq v10, v0, :cond_4

    const/4 v0, 0x2

    if-ne v10, v0, :cond_5

    .line 564726
    :cond_4
    iget-object v0, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v8}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 564727
    iget-object v9, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, LX/9l8;

    const/4 v3, 0x1

    invoke-direct/range {v0 .. v6}, LX/9l8;-><init>(Ljava/lang/String;Ljava/lang/String;ZJI)V

    invoke-interface {v9, v8, v0}, Ljava/util/concurrent/ConcurrentMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564728
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 564729
    const/4 v0, 0x1

    if-ne v10, v0, :cond_5

    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 564730
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/facebook/presence/ThreadPresenceManager;->a(Lcom/facebook/presence/ThreadPresenceManager;Ljava/lang/String;I)V

    .line 564731
    :cond_5
    invoke-static {p0, v7}, Lcom/facebook/presence/ThreadPresenceManager;->d(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)V

    goto/16 :goto_0

    .line 564732
    :cond_6
    iget-object v9, v9, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, LX/9l8;

    const/4 v3, 0x1

    invoke-direct/range {v0 .. v6}, LX/9l8;-><init>(Ljava/lang/String;Ljava/lang/String;ZJI)V

    invoke-interface {v9, v8, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static declared-synchronized b$redex0(Lcom/facebook/presence/ThreadPresenceManager;)V
    .locals 2

    .prologue
    .line 564695
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l9;

    .line 564696
    iget-object v0, v0, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 564697
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 564698
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized c(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)LX/9l9;
    .locals 2

    .prologue
    .line 564687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l9;

    .line 564688
    if-nez v0, :cond_0

    .line 564689
    new-instance v1, LX/9l9;

    invoke-direct {v1}, LX/9l9;-><init>()V

    .line 564690
    iput-object p1, v1, LX/9l9;->a:Lcom/facebook/user/model/UserKey;

    .line 564691
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 564692
    if-nez v0, :cond_0

    move-object v0, v1

    .line 564693
    :cond_0
    monitor-exit p0

    return-object v0

    .line 564694
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized d(Lcom/facebook/presence/ThreadPresenceManager;Lcom/facebook/user/model/UserKey;)V
    .locals 2

    .prologue
    .line 564678
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 564679
    if-nez p1, :cond_1

    .line 564680
    :cond_0
    monitor-exit p0

    return-void

    .line 564681
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->s:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564682
    invoke-virtual {p0, p1}, Lcom/facebook/presence/ThreadPresenceManager;->a(Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    .line 564683
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 564684
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 564685
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->s:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 564686
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 564676
    iput p1, p0, Lcom/facebook/presence/ThreadPresenceManager;->r:I

    .line 564677
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/user/model/UserKey;)Z
    .locals 2
    .param p1    # Lcom/facebook/user/model/UserKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 564669
    monitor-enter p0

    if-nez p1, :cond_0

    move v0, v1

    .line 564670
    :goto_0
    monitor-exit p0

    return v0

    .line 564671
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/presence/ThreadPresenceManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 564672
    goto :goto_0

    .line 564673
    :cond_1
    iget-object v0, p0, Lcom/facebook/presence/ThreadPresenceManager;->t:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l9;

    .line 564674
    if-eqz v0, :cond_2

    iget-object v0, v0, LX/9l9;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 564675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/user/model/UserKey;)Z
    .locals 2
    .param p1    # Lcom/facebook/user/model/UserKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564668
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/facebook/presence/ThreadPresenceManager;->a(Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/presence/ThreadPresenceManager;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 564667
    return-void
.end method

.method public final run()V
    .locals 0

    .prologue
    .line 564665
    invoke-direct {p0}, Lcom/facebook/presence/ThreadPresenceManager;->a()V

    .line 564666
    return-void
.end method
