.class public Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lcom/facebook/webrtc/IWebrtcConfigInterface;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile A:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final w:Lcom/facebook/common/callercontext/CallerContext;

.field public static final z:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/credentials/UserTokenCredentials;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Uh;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/telephony/TelephonyManager;

.field private final i:LX/3GO;

.field private final j:LX/34I;

.field private final k:LX/0pZ;

.field public final l:LX/11H;

.field public final m:LX/3GG;

.field private final n:LX/3Dw;

.field private final o:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3Dw;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Doy;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0W3;

.field private volatile r:Lcom/facebook/webrtc/WebrtcEngine;

.field private s:Ljava/util/Random;

.field private t:LX/00H;

.field private u:LX/0dC;

.field private final v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 565078
    const-class v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->a:Ljava/lang/Class;

    .line 565079
    const-class v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->w:Lcom/facebook/common/callercontext/CallerContext;

    .line 565080
    const-string v0, "voip_bwe_logging"

    sget-object v1, LX/2S4;->h:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->z:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0pZ;Landroid/telephony/TelephonyManager;Ljava/util/Random;LX/0Or;LX/0Or;LX/3GO;LX/34I;LX/00H;LX/0dC;LX/11H;LX/3GG;LX/3GQ;Ljava/util/Set;LX/0Or;LX/0W3;)V
    .locals 5
    .param p7    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/rtc/annotations/IsVoipVideoEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/credentials/UserTokenCredentials;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0pZ;",
            "Landroid/telephony/TelephonyManager;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3GO;",
            "LX/34I;",
            "LX/00H;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/3GG;",
            "LX/3GQ;",
            "Ljava/util/Set",
            "<",
            "LX/3Dw;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Doy;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565052
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    .line 565053
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    .line 565054
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->b:Landroid/content/Context;

    .line 565055
    iput-object p2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c:LX/0Or;

    .line 565056
    iput-object p3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 565057
    iput-object p4, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->e:LX/0Uh;

    .line 565058
    iput-object p5, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->k:LX/0pZ;

    .line 565059
    iput-object p6, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->h:Landroid/telephony/TelephonyManager;

    .line 565060
    iput-object p7, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->s:Ljava/util/Random;

    .line 565061
    iput-object p8, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->f:LX/0Or;

    .line 565062
    iput-object p9, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->g:LX/0Or;

    .line 565063
    iput-object p10, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->i:LX/3GO;

    .line 565064
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->j:LX/34I;

    .line 565065
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->t:LX/00H;

    .line 565066
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->u:LX/0dC;

    .line 565067
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->l:LX/11H;

    .line 565068
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->m:LX/3GG;

    .line 565069
    invoke-static {}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->v:Ljava/util/Map;

    .line 565070
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->n:LX/3Dw;

    .line 565071
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->p:LX/0Or;

    .line 565072
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->q:LX/0W3;

    .line 565073
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 565074
    invoke-interface/range {p17 .. p17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Dw;

    .line 565075
    invoke-interface {v1}, LX/3Dw;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 565076
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->o:LX/0P1;

    .line 565077
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;
    .locals 3

    .prologue
    .line 565041
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->A:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    if-nez v0, :cond_1

    .line 565042
    const-class v1, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    monitor-enter v1

    .line 565043
    :try_start_0
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->A:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565044
    if-eqz v2, :cond_0

    .line 565045
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->b(LX/0QB;)Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->A:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565046
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565047
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565048
    :cond_1
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->A:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    return-object v0

    .line 565049
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 565037
    invoke-static {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;)LX/3Dw;

    move-result-object v0

    .line 565038
    if-eqz v0, :cond_0

    .line 565039
    invoke-interface {v0, p2, p3}, LX/3Dw;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 565040
    :cond_0
    return-object p3
.end method

.method private static b(LX/0QB;)Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;
    .locals 22

    .prologue
    .line 565035
    new-instance v2, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x178

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v7

    check-cast v7, LX/0pZ;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v9

    check-cast v9, Ljava/util/Random;

    const/16 v10, 0x1568

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x1561

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/3GO;->a(LX/0QB;)LX/3GO;

    move-result-object v12

    check-cast v12, LX/3GO;

    invoke-static/range {p0 .. p0}, LX/34I;->a(LX/0QB;)LX/34I;

    move-result-object v13

    check-cast v13, LX/34I;

    const-class v14, LX/00H;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/00H;

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v15

    check-cast v15, LX/0dC;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v16

    check-cast v16, LX/11H;

    invoke-static/range {p0 .. p0}, LX/3GG;->a(LX/0QB;)LX/3GG;

    move-result-object v17

    check-cast v17, LX/3GG;

    invoke-static/range {p0 .. p0}, LX/3GQ;->a(LX/0QB;)LX/3GQ;

    move-result-object v18

    check-cast v18, LX/3GQ;

    invoke-static/range {p0 .. p0}, LX/3Dy;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v19

    const/16 v20, 0x2a1e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v21

    check-cast v21, LX/0W3;

    invoke-direct/range {v2 .. v21}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;-><init>(Landroid/content/Context;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0pZ;Landroid/telephony/TelephonyManager;Ljava/util/Random;LX/0Or;LX/0Or;LX/3GO;LX/34I;LX/00H;LX/0dC;LX/11H;LX/3GG;LX/3GQ;Ljava/util/Set;LX/0Or;LX/0W3;)V

    .line 565036
    return-object v2
.end method

.method private c(Ljava/lang/String;)LX/EDy;
    .locals 2

    .prologue
    .line 565031
    const/4 v0, 0x0

    move-object v0, v0

    .line 565032
    if-nez v0, :cond_0

    .line 565033
    const/4 v0, 0x0

    .line 565034
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->k:LX/0pZ;

    invoke-interface {v1, v0}, LX/0pZ;->a(LX/2Ff;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDy;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;)LX/3Dw;
    .locals 1

    .prologue
    .line 565030
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->o:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Dw;

    return-object v0
.end method

.method private static d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564893
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 564894
    const-string v1, "rtc_enable_frame_enhancement"

    const/16 v2, 0x5f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564895
    const-string v1, "rtc_force_enable_software_aec"

    const/16 v2, 0x5fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564896
    const-string v1, "rtc_force_enable_software_agc"

    const/16 v2, 0x5fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564897
    const-string v1, "rtc_force_disable_software_aec"

    const/16 v2, 0x5f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564898
    const-string v1, "rtc_force_disable_software_agc"

    const/16 v2, 0x5fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564899
    const-string v1, "rtc_push_log"

    const/16 v2, 0x606

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564900
    const-string v1, "rtc_conferencing_video_can_receive"

    const/16 v2, 0x5f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564901
    const-string v1, "rtc_video_conference_simulcast"

    const/16 v2, 0x60c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564902
    const-string v1, "voip_use_jni_audio_callee_android"

    const/16 v2, 0x673

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564903
    const-string v1, "voip_use_jni_audio_caller_android"

    const/16 v2, 0x674

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564904
    const-string v1, "rtc_offerack_engine_send"

    const/16 v2, 0x603

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564905
    const-string v1, "rtc_pushkit_no_delay"

    const/16 v2, 0x607

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564906
    const-string v1, "rtc_android_video_h264_hw"

    const/16 v2, 0x5eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564907
    const-string v1, "rtc_snake_engine_gk"

    const/16 v2, 0x609

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564908
    const-string v1, "rtc_aac_codec_support"

    const/16 v2, 0x5e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564909
    const-string v1, "rtc_h264_android_device_blacklist"

    const/16 v2, 0x5fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564910
    const-string v1, "instant_video_rollout"

    const/16 v2, 0x515

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564911
    const-string v1, "rtc_send_conferencing_video_remb"

    const/16 v2, 0x608

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564912
    const-string v1, "rtc_audio_device_default_48khz"

    const/16 v2, 0x5ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564913
    const-string v1, "rtc_opispx_callee_allowed"

    const/16 v2, 0x604

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564914
    const-string v1, "rtc_opus_bwe_callee_allowed"

    const/16 v2, 0x605

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564915
    const-string v1, "rtc_h264_android_device_whitelist"

    const/16 v2, 0x5ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564916
    const-string v1, "rtc_interruption_timestamp_fixing"

    const/16 v2, 0x602

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564917
    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 4

    .prologue
    .line 565008
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 565009
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 565010
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->p:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->q:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 565011
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->p:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 565012
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2S4;->q:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 565013
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 565014
    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    .line 565015
    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565016
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 565017
    :cond_2
    :try_start_2
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->a:Ljava/lang/Class;

    const-string v1, "Failed to read either the private key or the certificate. Reconstructing."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 565018
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->r:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0}, Lcom/facebook/webrtc/WebrtcEngine;->makeKeyPairAndCertificate()Ljava/lang/String;

    move-result-object v0

    .line 565019
    const/16 v1, 0x3a

    invoke-static {v1}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v1

    invoke-virtual {v1}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v1

    invoke-virtual {v1}, LX/2Cb;->omitEmptyStrings()LX/2Cb;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 565020
    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 565021
    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    .line 565022
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->a:Ljava/lang/Class;

    const-string v1, "Failed to create and parse key pair and certificate. Defaulting to no-DTLS."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 565023
    :cond_4
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    .line 565024
    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    .line 565025
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 565026
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2S4;->p:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2S4;->q:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public final allocateTurnServer(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/webrtc/TurnAllocatorCallback;)V
    .locals 7

    .prologue
    .line 565000
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/UserTokenCredentials;

    .line 565001
    if-nez v0, :cond_0

    const-string v2, ""

    .line 565002
    :goto_0
    new-instance v5, LX/14U;

    invoke-direct {v5}, LX/14U;-><init>()V

    .line 565003
    new-instance v0, LX/EDF;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/EDF;-><init>(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/14U;Lcom/facebook/webrtc/TurnAllocatorCallback;)V

    .line 565004
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 565005
    return-void

    .line 565006
    :cond_0
    iget-object v1, v0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    move-object v2, v1

    .line 565007
    goto :goto_0
.end method

.method public final decryptNonceWithOfferPayload(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 564974
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 564975
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 564976
    invoke-static {p1, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 564977
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doy;

    .line 564978
    :try_start_0
    new-instance v2, LX/Eao;

    iget-object v4, v0, LX/Doy;->e:LX/Doz;

    new-instance p0, LX/Eap;

    const-string p1, ""

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v2, v4, p0}, LX/Eao;-><init>(LX/DoS;LX/Eap;)V

    .line 564979
    new-instance p0, LX/EbA;

    invoke-direct {p0, v1}, LX/EbA;-><init>([B)V

    .line 564980
    invoke-virtual {v2, p0}, LX/Eao;->a(LX/EbA;)[B

    move-result-object v4

    .line 564981
    iget-object p1, v0, LX/Doy;->e:LX/Doz;

    .line 564982
    iget-object v2, p0, LX/EbA;->c:LX/Ecs;

    move-object v2, v2

    .line 564983
    invoke-virtual {v2}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, LX/Doz;->b(I)V

    .line 564984
    iget-object v2, v0, LX/Doy;->e:LX/Doz;

    .line 564985
    iget p1, p0, LX/EbA;->d:I

    move p0, p1

    .line 564986
    iget-object p1, v2, LX/Doz;->d:LX/Ecn;

    .line 564987
    iget-object p2, p1, LX/Ecn;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch LX/Eaj; {:try_start_0 .. :try_end_0} :catch_6
    .catch LX/Eai; {:try_start_0 .. :try_end_0} :catch_4
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_5
    .catch LX/Ead; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eaq; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/Eak; {:try_start_0 .. :try_end_0} :catch_0

    .line 564988
    move-object v2, v4

    .line 564989
    :goto_2
    move-object v0, v2

    .line 564990
    if-nez v0, :cond_2

    .line 564991
    const-string v0, ""

    .line 564992
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 564993
    goto :goto_0

    :cond_1
    move v1, v2

    .line 564994
    goto :goto_1

    .line 564995
    :cond_2
    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 564996
    :catch_0
    move-exception v2

    .line 564997
    :goto_4
    sget-object v4, LX/Doy;->a:Ljava/lang/Class;

    const-string p0, "Error in decrypting data"

    invoke-static {v4, p0, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 564998
    const/4 v2, 0x0

    goto :goto_2

    .line 564999
    :catch_1
    move-exception v2

    goto :goto_4

    :catch_2
    move-exception v2

    goto :goto_4

    :catch_3
    move-exception v2

    goto :goto_4

    :catch_4
    move-exception v2

    goto :goto_4

    :catch_5
    move-exception v2

    goto :goto_4

    :catch_6
    move-exception v2

    goto :goto_4
.end method

.method public final encryptNonceWithOfferPayload(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 564950
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 564951
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 564952
    invoke-static {p1, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 564953
    invoke-static {p2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 564954
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doy;

    .line 564955
    invoke-static {v2}, LX/Dpn;->c([B)LX/DpK;

    move-result-object v4

    .line 564956
    iget-object v5, v4, LX/DpK;->msg_to:LX/DpM;

    iget-object v5, v5, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v5, v4, LX/DpK;->msg_to:LX/DpM;

    iget-object v5, v5, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-static {v6, v7, v5}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 564957
    new-instance v5, LX/Eap;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, LX/Eap;-><init>(Ljava/lang/String;I)V

    .line 564958
    :try_start_0
    iget-object v6, v4, LX/DpK;->identity_key:[B

    iget-object v7, v4, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v7, v7, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v4, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->public_key:[B

    iget-object v9, v4, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v9, v9, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v9, v9, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v10, v4, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v10, v10, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v10, v10, LX/DpU;->public_key:[B

    iget-object v4, v4, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v11, v4, LX/Dpf;->signature:[B

    move-object v4, v0

    invoke-static/range {v4 .. v11}, LX/Doy;->a(LX/Doy;LX/Eap;[BI[BI[B[B)V

    .line 564959
    new-instance v4, LX/Eao;

    iget-object v6, v0, LX/Doy;->e:LX/Doz;

    invoke-direct {v4, v6, v5}, LX/Eao;-><init>(LX/DoS;LX/Eap;)V

    .line 564960
    invoke-virtual {v4, v1}, LX/Eao;->a([B)LX/Eb9;

    move-result-object v4

    .line 564961
    instance-of v5, v4, LX/EbA;

    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 564962
    invoke-interface {v4}, LX/Eb9;->a()[B
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eaq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 564963
    :goto_2
    move-object v0, v4

    .line 564964
    if-nez v0, :cond_2

    .line 564965
    const-string v0, ""

    .line 564966
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 564967
    goto :goto_0

    :cond_1
    move v1, v2

    .line 564968
    goto :goto_1

    .line 564969
    :cond_2
    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 564970
    :catch_0
    move-exception v4

    .line 564971
    :goto_4
    sget-object v5, LX/Doy;->a:Ljava/lang/Class;

    const-string v6, "Error in encrypting data"

    invoke-static {v5, v6, v4}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 564972
    const/4 v4, 0x0

    goto :goto_2

    .line 564973
    :catch_1
    move-exception v4

    goto :goto_4
.end method

.method public final generateEndToEndEncryptionPayloadForOffer()Ljava/lang/String;
    .locals 14

    .prologue
    .line 564924
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->q:LX/0W3;

    sget-wide v2, LX/0X5;->hP:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 564925
    const-string v0, ""

    .line 564926
    :goto_0
    return-object v0

    .line 564927
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doy;

    const/4 v11, 0x0

    .line 564928
    :try_start_0
    new-instance v4, LX/DpK;

    new-instance v5, LX/DpM;

    iget-object v6, v0, LX/Doy;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v7, v0, LX/Doy;->c:LX/2PJ;

    invoke-virtual {v7}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    const/4 v6, 0x0

    iget-object v7, v0, LX/Doy;->e:LX/Doz;

    invoke-virtual {v7}, LX/Doz;->a()LX/Eaf;

    move-result-object v7

    .line 564929
    iget-object v8, v7, LX/Eaf;->a:LX/Eae;

    move-object v7, v8

    .line 564930
    invoke-virtual {v7}, LX/Eae;->b()[B

    move-result-object v7

    .line 564931
    iget-object v8, v0, LX/Doy;->d:LX/Dp0;

    invoke-virtual {v8}, LX/Dp0;->b()LX/Ebk;

    move-result-object v8

    .line 564932
    invoke-virtual {v8}, LX/Ebk;->b()LX/Eau;

    move-result-object v9

    .line 564933
    iget-object v10, v9, LX/Eau;->a:LX/Eat;

    move-object v9, v10

    .line 564934
    invoke-virtual {v9}, LX/Eat;->a()[B

    move-result-object v9

    .line 564935
    new-instance v10, LX/Dpf;

    new-instance v12, LX/DpU;

    invoke-virtual {v8}, LX/Ebk;->a()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-direct {v12, v9, v13}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    invoke-virtual {v8}, LX/Ebk;->c()[B

    move-result-object v8

    invoke-direct {v10, v12, v8}, LX/Dpf;-><init>(LX/DpU;[B)V

    move-object v8, v10

    .line 564936
    iget-object v9, v0, LX/Doy;->d:LX/Dp0;

    invoke-virtual {v9}, LX/Dp0;->a()LX/Ebg;

    move-result-object v9

    .line 564937
    invoke-virtual {v9}, LX/Ebg;->b()LX/Eau;

    move-result-object v10

    .line 564938
    iget-object v12, v10, LX/Eau;->a:LX/Eat;

    move-object v10, v12

    .line 564939
    invoke-virtual {v10}, LX/Eat;->a()[B

    move-result-object v10

    .line 564940
    new-instance v12, LX/DpU;

    invoke-virtual {v9}, LX/Ebg;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v12, v10, v9}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    move-object v9, v12

    .line 564941
    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, LX/DpK;-><init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;LX/DpL;)V

    .line 564942
    invoke-static {v4}, LX/Dpn;->a(LX/1u2;)[B
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 564943
    :goto_1
    move-object v0, v4

    .line 564944
    if-nez v0, :cond_1

    .line 564945
    const-string v0, ""

    goto/16 :goto_0

    .line 564946
    :cond_1
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 564947
    :catch_0
    move-exception v4

    .line 564948
    sget-object v5, LX/Doy;->a:Ljava/lang/Class;

    const-string v6, "Error in generating payload"

    invoke-static {v5, v6, v4}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v11

    .line 564949
    goto :goto_1
.end method

.method public final getAckTimeout()I
    .locals 1

    .prologue
    .line 564923
    const/16 v0, 0x2710

    return v0
.end method

.method public final getAppConfigForIncomingCall(I)[B
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 564919
    new-instance v0, LX/7TR;

    invoke-direct {v0}, LX/7TR;-><init>()V

    .line 564920
    if-ne p1, v1, :cond_0

    .line 564921
    iput-boolean v1, v0, LX/7TR;->a:Z

    .line 564922
    :cond_0
    invoke-virtual {v0}, LX/7TR;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final getAppDataFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564918
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getAppId()J
    .locals 2

    .prologue
    .line 565084
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->t:LX/00H;

    invoke-virtual {v0}, LX/00H;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getAppTempFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 565081
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getAudioOptionOverrides()[I
    .locals 5

    .prologue
    .line 565138
    sget-object v0, LX/EDG;->NumTypes:LX/EDG;

    invoke-virtual {v0}, LX/EDG;->ordinal()I

    move-result v0

    new-array v0, v0, [I

    .line 565139
    sget-object v1, LX/EDG;->EC:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->i:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565140
    sget-object v1, LX/EDG;->AGC:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->r:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565141
    sget-object v1, LX/EDG;->HighPassFilter:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->t:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565142
    sget-object v1, LX/EDG;->CNG:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->u:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565143
    sget-object v1, LX/EDG;->ExperimentalAGC:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->v:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565144
    sget-object v1, LX/EDG;->ECMode:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->w:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565145
    sget-object v1, LX/EDG;->AGCMode:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->x:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565146
    sget-object v1, LX/EDG;->NS:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->s:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565147
    sget-object v1, LX/EDG;->NSMode:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->y:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565148
    sget-object v1, LX/EDG;->LAFNSMode:LX/EDG;

    invoke-virtual {v1}, LX/EDG;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->z:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    .line 565149
    return-object v0
.end method

.method public final getAudioOutputRoute()I
    .locals 1

    .prologue
    .line 565137
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->j:LX/34I;

    invoke-virtual {v0}, LX/34I;->c()LX/7TP;

    move-result-object v0

    invoke-virtual {v0}, LX/7TP;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getBooleanExperimentParam(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 565132
    invoke-direct {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c(Ljava/lang/String;)LX/EDy;

    move-result-object v0

    .line 565133
    if-nez v0, :cond_0

    .line 565134
    :goto_0
    return p3

    .line 565135
    :cond_0
    iget-object p0, v0, LX/EDy;->a:LX/2WT;

    move-object v0, p0

    .line 565136
    invoke-virtual {v0, p2, p3}, LX/2WT;->a(Ljava/lang/String;Z)Z

    move-result p3

    goto :goto_0
.end method

.method public final getBooleanParam(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 565124
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->z:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 565125
    if-eqz v0, :cond_0

    .line 565126
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    .line 565127
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_1

    .line 565128
    const/4 p2, 0x1

    .line 565129
    :cond_0
    :goto_0
    return p2

    .line 565130
    :cond_1
    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_0

    .line 565131
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final getCapability()I
    .locals 2

    .prologue
    .line 565119
    const-class v0, LX/1tp;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 565120
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565121
    sget-object v0, LX/1tp;->VOIP:LX/1tp;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 565122
    sget-object v0, LX/1tp;->VOIP_WEB:LX/1tp;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 565123
    :cond_0
    invoke-static {v1}, LX/1tz;->a(Ljava/util/Set;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final getConnectivityStatus()Ljava/lang/String;
    .locals 3

    .prologue
    .line 565108
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->i:LX/3GO;

    .line 565109
    iget-object v1, v0, LX/3GO;->a:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 565110
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-nez v2, :cond_1

    .line 565111
    :cond_0
    const-string v1, "none"

    .line 565112
    :goto_0
    move-object v0, v1

    .line 565113
    return-object v0

    .line 565114
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_2

    .line 565115
    const-string v1, "cell"

    goto :goto_0

    .line 565116
    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 p0, 0x1

    if-eq v2, p0, :cond_3

    const-string v2, "mobile2"

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 565117
    const-string v1, "cell"

    goto :goto_0

    .line 565118
    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 565150
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->u:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getGateKeeper(Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    .line 565104
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->v:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 565105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown gatekeeper: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565106
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->v:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 565107
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->e:LX/0Uh;

    invoke-virtual {v1, v0, p2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final getIntExperimentParam(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 565093
    invoke-static {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;)LX/3Dw;

    move-result-object v0

    .line 565094
    if-eqz v0, :cond_2

    .line 565095
    invoke-interface {v0, p2, p3}, LX/3Dw;->a(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    .line 565096
    :goto_0
    move-object v0, v0

    .line 565097
    if-eqz v0, :cond_1

    .line 565098
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 565099
    :cond_0
    :goto_1
    return p3

    .line 565100
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c(Ljava/lang/String;)LX/EDy;

    move-result-object v0

    .line 565101
    if-eqz v0, :cond_0

    .line 565102
    iget-object p0, v0, LX/EDy;->a:LX/2WT;

    move-object v0, p0

    .line 565103
    invoke-virtual {v0, p2, p3}, LX/2WT;->a(Ljava/lang/String;I)I

    move-result p3

    goto :goto_1

    :cond_2
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIntParam(Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 565086
    const/4 v1, 0x0

    .line 565087
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->z:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 565088
    if-nez v0, :cond_1

    move-object v0, v1

    .line 565089
    :goto_0
    move-object v0, v0

    .line 565090
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 565091
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    .line 565092
    :cond_0
    return p2

    :cond_1
    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIsacInitialBitrate()I
    .locals 3

    .prologue
    .line 565085
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->n:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getIspxCodecSwitchEnabled()Z
    .locals 3

    .prologue
    .line 564892
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->l:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final getIspxInitialCodec()I
    .locals 3

    .prologue
    .line 565083
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->j:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getMinVersion()I
    .locals 5

    .prologue
    .line 565027
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->i:LX/3GO;

    .line 565028
    iget-object v1, v0, LX/3GO;->b:LX/0W3;

    sget-wide v3, LX/0X5;->hN:J

    const/4 v2, 0x0

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JI)I

    move-result v1

    move v0, v1

    .line 565029
    return v0
.end method

.method public final getNumberOfRetriesOnError()I
    .locals 1

    .prologue
    .line 565082
    const/4 v0, 0x0

    return v0
.end method

.method public final getOpispxInitialCodec()I
    .locals 3

    .prologue
    .line 564811
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->k:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getRadioTechnology()Ljava/lang/String;
    .locals 2

    .prologue
    .line 564851
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->i:LX/3GO;

    .line 564852
    iget-object v1, v0, LX/3GO;->a:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 564853
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result p0

    if-nez p0, :cond_2

    .line 564854
    :cond_0
    const/4 v1, 0x0

    .line 564855
    :goto_0
    move-object v0, v1

    .line 564856
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 564857
    const-string v0, ""

    .line 564858
    :cond_1
    return-object v0

    .line 564859
    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 p0, 0x1

    if-ne v1, p0, :cond_3

    .line 564860
    const-string v1, "WIFI"

    goto :goto_0

    .line 564861
    :cond_3
    iget-object v1, v0, LX/3GO;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    invoke-static {v1}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getRecordFileDir()Ljava/lang/String;
    .locals 3

    .prologue
    .line 564850
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->I:LX/0Tn;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRecordSamplesPerSec()I
    .locals 1

    .prologue
    .line 564849
    const/16 v0, 0x3e80

    return v0
.end method

.method public final getSpeexInitialBitrate()I
    .locals 3

    .prologue
    .line 564848
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->o:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getSslCertificate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564846
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->f()V

    .line 564847
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final getSslPrivateKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564844
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->f()V

    .line 564845
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final getStringExperimentParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 564837
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 564838
    if-eqz v0, :cond_1

    move-object p3, v0

    .line 564839
    :cond_0
    :goto_0
    return-object p3

    .line 564840
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c(Ljava/lang/String;)LX/EDy;

    move-result-object v0

    .line 564841
    if-eqz v0, :cond_0

    .line 564842
    iget-object p0, v0, LX/EDy;->a:LX/2WT;

    move-object v0, p0

    .line 564843
    invoke-virtual {v0, p2, p3}, LX/2WT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method public final getThreadPresenceCapability()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 564835
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->e:LX/0Uh;

    const/16 v2, 0x515

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 564836
    if-eqz v1, :cond_0

    sget-object v0, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

    invoke-virtual {v0}, LX/2gE;->getValue()I

    move-result v0

    :cond_0
    return v0
.end method

.method public final getUploadLogLevel()I
    .locals 5

    .prologue
    .line 564825
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->e:LX/0Tn;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v0, v0

    .line 564826
    sget-object v1, LX/EDH;->None:LX/EDH;

    invoke-virtual {v1}, LX/EDH;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 564827
    :cond_0
    :goto_0
    return v0

    .line 564828
    :cond_1
    sget-object v0, LX/EDH;->None:LX/EDH;

    invoke-virtual {v0}, LX/EDH;->ordinal()I

    move-result v0

    .line 564829
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->n:LX/3Dw;

    const-string v2, "basic_log_permyriad"

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LX/3Dw;->a(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 564830
    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->n:LX/3Dw;

    const-string v3, "debug_pct"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/3Dw;->a(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 564831
    iget-object v3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->s:Ljava/util/Random;

    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    if-ge v3, v1, :cond_0

    .line 564832
    sget-object v0, LX/EDH;->Basic:LX/EDH;

    invoke-virtual {v0}, LX/EDH;->ordinal()I

    move-result v0

    .line 564833
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->s:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 564834
    sget-object v0, LX/EDH;->Debug:LX/EDH;

    invoke-virtual {v0}, LX/EDH;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getUserId()J
    .locals 2

    .prologue
    .line 564819
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/UserTokenCredentials;

    .line 564820
    if-nez v0, :cond_0

    .line 564821
    const-wide/16 v0, 0x0

    .line 564822
    :goto_0
    return-wide v0

    .line 564823
    :cond_0
    iget-object v1, v0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    move-object v0, v1

    .line 564824
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getVideoCodecOverrideMode()I
    .locals 3

    .prologue
    .line 564817
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->P:LX/0Tn;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 564818
    return v0
.end method

.method public final getVoipCodecOverrideMode()I
    .locals 3

    .prologue
    .line 564815
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->f:LX/0Tn;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 564816
    return v0
.end method

.method public final getVoipCodecOverrideRate()I
    .locals 3

    .prologue
    .line 564813
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->g:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 564814
    return v0
.end method

.method public final getVoipIspxFecOverrideMode()I
    .locals 3

    .prologue
    .line 564812
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->m:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final logExperimentObservation(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 564802
    invoke-static {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;)LX/3Dw;

    move-result-object v0

    .line 564803
    if-eqz v0, :cond_1

    .line 564804
    invoke-interface {v0}, LX/3Dw;->b()V

    .line 564805
    const/4 v0, 0x1

    .line 564806
    :goto_0
    move v0, v0

    .line 564807
    if-eqz v0, :cond_0

    .line 564808
    :goto_1
    return-void

    .line 564809
    :cond_0
    goto :goto_1

    .line 564810
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setWebrtcEngine(Lcom/facebook/webrtc/WebrtcEngine;)V
    .locals 0

    .prologue
    .line 564862
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->r:Lcom/facebook/webrtc/WebrtcEngine;

    .line 564863
    return-void
.end method

.method public final shouldClearBitrateHistory()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 564864
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->Q:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 564865
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2S4;->Q:LX/0Tn;

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 564866
    return v0
.end method

.method public final shouldEnableAutomatedTestSupport()Z
    .locals 3

    .prologue
    .line 564867
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->L:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldEnableStarveSmoothing()Z
    .locals 3

    .prologue
    .line 564868
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->A:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldEnableVideo()Z
    .locals 1

    .prologue
    .line 564869
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 564870
    return v0
.end method

.method public final shouldFailCallDueToAnotherCall(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 564871
    const-string v1, "pstn_upsell"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 564872
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->h:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->h:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final shouldLoadSelfRawVideo()Z
    .locals 3

    .prologue
    .line 564873
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->F:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldLoopVideoPlayback()Z
    .locals 3

    .prologue
    .line 564874
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->G:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldPlaySampleInputFile()Z
    .locals 3

    .prologue
    .line 564875
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->J:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldPreprocessVideoFrames()Z
    .locals 3

    .prologue
    .line 564876
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->H:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldRecordRemoteRawVideo()Z
    .locals 3

    .prologue
    .line 564877
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->C:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldRecordRemoteVideo()Z
    .locals 3

    .prologue
    .line 564878
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->B:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldRecordSelfRawVideo()Z
    .locals 3

    .prologue
    .line 564879
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->E:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final shouldRecordSelfVideo()Z
    .locals 3

    .prologue
    .line 564880
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->D:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final webRTCControlRPC_UpdateTestAudioMode(I)V
    .locals 3

    .prologue
    .line 564881
    if-ltz p1, :cond_1

    .line 564882
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->b:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 564883
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 564884
    const-string v2, "-2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 564885
    sget-object v2, LX/2S4;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 564886
    :cond_0
    sget-object v0, LX/2S4;->b:LX/0Tn;

    const-string v2, "-2"

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 564887
    sget-object v0, LX/2S4;->d:LX/0Tn;

    invoke-interface {v1, v0, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 564888
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 564889
    :goto_0
    return-void

    .line 564890
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->c:LX/0Tn;

    const-string v2, "-1"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 564891
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2S4;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2S4;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2S4;->d:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method
