.class public abstract Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Rj;
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<TP;",
        "LX/1dV;",
        "TE;",
        "Lcom/facebook/components/feed/FeedComponentView;",
        ">;",
        "LX/1Rj",
        "<TP;TE;>;",
        "LX/0Ya;"
    }
.end annotation


# instance fields
.field public a:LX/1V3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1V3",
            "<TP;TE;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 535805
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 535806
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;->b:Ljava/lang/String;

    .line 535807
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;

    invoke-static {v0}, LX/1V3;->a(LX/0QB;)LX/1V3;

    move-result-object v0

    check-cast v0, LX/1V3;

    iput-object v0, p0, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;->a:LX/1V3;

    .line 535808
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 535804
    const/4 v0, -0x1

    return v0
.end method

.method public bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 535803
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;->a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TP;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 535801
    check-cast p3, LX/1Pn;

    .line 535802
    iget-object v0, p0, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;->a:LX/1V3;

    iget-object v4, p0, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;->b:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, LX/1V3;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;Ljava/lang/String;LX/1Rj;Z)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;TP;)V"
        }
    .end annotation

    .prologue
    .line 535800
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xadc5b54

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 535797
    check-cast p2, LX/1dV;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    .line 535798
    invoke-virtual {p4, p2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 535799
    const/16 v1, 0x1f

    const v2, 0x24eb829c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 535796
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)",
            "LX/0jW;"
        }
    .end annotation

    .prologue
    .line 535795
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 535792
    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    .line 535793
    invoke-virtual {p4}, Lcom/facebook/components/ComponentView;->h()V

    .line 535794
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 535790
    const/4 v0, 0x1

    return v0
.end method

.method public final iV_()Z
    .locals 1

    .prologue
    .line 535791
    const/4 v0, 0x0

    return v0
.end method
