.class public final Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/26A;

.field public b:Z

.field private final c:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "LX/26B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/26A;Ljava/lang/ref/ReferenceQueue;)V
    .locals 1

    .prologue
    .line 371364
    iput-object p1, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->a:LX/26A;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 371365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->b:Z

    .line 371366
    iput-object p2, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->c:Ljava/lang/ref/ReferenceQueue;

    .line 371367
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 371368
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->b:Z

    if-eqz v0, :cond_0

    .line 371369
    :try_start_0
    iget-object v1, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->a:LX/26A;

    iget-object v0, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->c:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, LX/26B;

    invoke-virtual {v1, v0}, LX/26A;->a(LX/26B;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 371370
    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->b:Z

    goto :goto_0

    .line 371371
    :cond_0
    return-void
.end method

.method public final declared-synchronized start()V
    .locals 1

    .prologue
    .line 371372
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/components/feed/CacheableEntityRegistry$ReferenceQueueThread;->b:Z

    .line 371373
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371374
    monitor-exit p0

    return-void

    .line 371375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
