.class public final Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1zn;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1zn;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 496498
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496499
    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->b:Ljava/util/List;

    .line 496500
    return-void
.end method

.method private static a(LX/15i;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496464
    const/4 v0, 0x0

    .line 496465
    const/4 v2, 0x0

    .line 496466
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 496467
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    const v3, -0x7622a804

    invoke-static {v1, v3}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 496468
    if-eqz v2, :cond_0

    .line 496469
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 496470
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 496471
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 496472
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v1, v4, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 496473
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 496474
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496475
    const/4 v0, 0x1

    .line 496476
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 496477
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 496478
    :cond_1
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 496479
    :catch_0
    move-exception v1

    .line 496480
    :try_start_1
    sget-object v3, LX/1zn;->a:Ljava/lang/Class;

    const-string v4, "Failed to save image - "

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496481
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 496482
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 496483
    :catch_1
    move-exception v1

    .line 496484
    :try_start_2
    sget-object v3, LX/1zn;->a:Ljava/lang/Class;

    const-string v4, "Failed to download image - "

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 496485
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 496486
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 496487
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 496488
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    throw v0
.end method

.method private a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496489
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->l:LX/1zr;

    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v1

    .line 496490
    iget-object v6, v0, LX/1zr;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v6, :cond_0

    .line 496491
    iget-object v6, v0, LX/1zr;->a:LX/0tX;

    .line 496492
    new-instance v9, LX/3gl;

    invoke-direct {v9}, LX/3gl;-><init>()V

    move-object v9, v9

    .line 496493
    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v9

    const-wide/32 v11, 0x15180

    invoke-virtual {v9, v11, v12}, LX/0zO;->a(J)LX/0zO;

    move-result-object v9

    sget-object v10, LX/0zS;->a:LX/0zS;

    invoke-virtual {v9, v10}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    move-object v7, v9

    .line 496494
    invoke-virtual {v6, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    new-instance v7, LX/3gn;

    invoke-direct {v7, v0}, LX/3gn;-><init>(LX/1zr;)V

    iget-object v8, v0, LX/1zr;->b:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    iput-object v6, v0, LX/1zr;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 496495
    :cond_0
    iget-object v6, v0, LX/1zr;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v3, v6

    .line 496496
    new-instance v4, LX/3go;

    invoke-direct {v4, v0, v1}, LX/3go;-><init>(LX/1zr;I)V

    iget-object v5, v0, LX/1zr;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 496497
    new-instance v1, LX/3gw;

    invoke-direct {v1, p0, p2}, LX/3gw;-><init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v2, v2, LX/1zn;->k:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 496448
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 496449
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 496450
    const/4 v2, 0x0

    .line 496451
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496452
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 496453
    invoke-virtual {v1}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 496454
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    .line 496455
    :cond_0
    :goto_0
    return-void

    .line 496456
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 496457
    :goto_1
    :try_start_2
    sget-object v2, LX/1zn;->a:Ljava/lang/Class;

    const-string v3, "Failed to save vector - "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 496458
    if-eqz v1, :cond_0

    .line 496459
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    goto :goto_0

    .line 496460
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 496461
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    :cond_1
    throw v0

    .line 496462
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 496463
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zt;)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 496422
    if-nez p2, :cond_1

    .line 496423
    :cond_0
    :goto_0
    return v0

    .line 496424
    :cond_1
    iget-object v2, p2, LX/1zt;->f:Ljava/lang/String;

    move-object v2, v2

    .line 496425
    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 496426
    iget v2, p2, LX/1zt;->g:I

    move v2, v2

    .line 496427
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 496428
    iget-boolean v2, p2, LX/1zt;->h:Z

    move v2, v2

    .line 496429
    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->k()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 496430
    sget-object v3, LX/1zn;->b:[LX/1zo;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 496431
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 496432
    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v6

    invoke-static {v6}, LX/22E;->a(I)LX/22E;

    move-result-object v6

    .line 496433
    if-nez v6, :cond_4

    .line 496434
    :cond_2
    :goto_2
    move v5, v7

    .line 496435
    if-nez v5, :cond_0

    .line 496436
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 496437
    goto :goto_0

    .line 496438
    :cond_4
    iget-object v9, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v9, v9, LX/1zn;->f:LX/1zk;

    invoke-virtual {v9, p1, v5}, LX/1zk;->c(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object v9

    .line 496439
    iget-object v10, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v10, v10, LX/1zn;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v11

    invoke-virtual {v5}, LX/1zo;->name()Ljava/lang/String;

    move-result-object p2

    invoke-static {v11, p2}, LX/1zs;->a(ILjava/lang/String;)LX/0Tn;

    move-result-object v11

    const/4 p2, 0x0

    invoke-interface {v10, v11, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 496440
    invoke-static {v6, v5}, LX/1zk;->a(LX/22E;LX/1zo;)LX/22F;

    move-result-object v11

    .line 496441
    invoke-static {p1, v5}, LX/1zk;->b(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object p2

    .line 496442
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v6, v7

    .line 496443
    :goto_3
    iget-object v9, v11, LX/22F;->d:Ljava/lang/String;

    move-object v9, v9

    .line 496444
    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 496445
    if-nez v6, :cond_5

    if-eqz v9, :cond_2

    :cond_5
    move v7, v8

    .line 496446
    goto :goto_2

    :cond_6
    move v6, v8

    .line 496447
    goto :goto_3
.end method

.method public static b(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
            "LX/1zo;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496413
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->f:LX/1zk;

    invoke-virtual {v0, p1, p2}, LX/1zk;->c(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object v0

    .line 496414
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496415
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 496416
    :goto_0
    return-object v0

    .line 496417
    :cond_0
    sget-object v1, LX/3fO;->a:[I

    invoke-virtual {p2}, LX/1zo;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 496418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reaction Asset for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496419
    :pswitch_0
    invoke-static {p1, p2}, LX/1zk;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 496420
    invoke-static {v2, v1, v0}, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a(LX/15i;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 496421
    :pswitch_1
    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 496391
    const/4 v0, 0x0

    .line 496392
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [I

    .line 496393
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    .line 496394
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v0

    aput v0, v3, v1

    move v1, v2

    .line 496395
    goto :goto_0

    .line 496396
    :cond_0
    move-object v1, v3

    .line 496397
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 496398
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    .line 496399
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v4, v4, LX/1zn;->d:LX/1zf;

    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    invoke-virtual {v4, v5}, LX/1zf;->a(I)LX/1zt;

    move-result-object v4

    .line 496400
    if-eqz v4, :cond_2

    invoke-direct {p0, v0, v4}, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zt;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 496401
    :cond_2
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v4, v4, LX/1zn;->j:LX/1zq;

    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->l()I

    move-result v5

    invoke-virtual {v4, v5}, LX/1zq;->a(I)V

    .line 496402
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 496403
    sget-object v6, LX/1zn;->b:[LX/1zo;

    array-length v7, v6

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_3

    aget-object v8, v6, v4

    .line 496404
    invoke-static {p0, v0, v8}, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->b(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496405
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 496406
    :cond_3
    invoke-static {v5}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/3gx;

    invoke-direct {v5, p0}, LX/3gx;-><init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;)V

    iget-object v6, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v6, v6, LX/1zn;->k:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v4, v4

    .line 496407
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496408
    new-instance v5, LX/3gy;

    invoke-direct {v5, p0, v0, v1}, LX/3gy;-><init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;[I)V

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->k:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 496409
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 496410
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->j:LX/1zq;

    invoke-virtual {v0}, LX/1zq;->b()V

    .line 496411
    :goto_3
    return-void

    .line 496412
    :cond_5
    invoke-static {v2}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/3k4;

    invoke-direct {v1, p0}, LX/3k4;-><init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;)V

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v2, v2, LX/1zn;->k:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_3
.end method
