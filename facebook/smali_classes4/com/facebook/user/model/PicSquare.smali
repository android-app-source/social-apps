.class public Lcom/facebook/user/model/PicSquare;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/user/model/PicSquareDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/PicSquare;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/user/model/PicSquareUrlWithSize;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mPicSquareUrlsWithSizes:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "picSquareUrls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/PicSquareUrlWithSize;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 560820
    const-class v0, Lcom/facebook/user/model/PicSquareDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 560821
    new-instance v0, LX/2Yh;

    invoke-direct {v0}, LX/2Yh;-><init>()V

    sput-object v0, Lcom/facebook/user/model/PicSquare;->a:Ljava/util/Comparator;

    .line 560822
    new-instance v0, LX/2Yi;

    invoke-direct {v0}, LX/2Yi;-><init>()V

    sput-object v0, Lcom/facebook/user/model/PicSquare;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 560823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560824
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    .line 560825
    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/PicSquareUrlWithSize;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 560826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560827
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 560828
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 560829
    sget-object v1, Lcom/facebook/user/model/PicSquare;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 560830
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    .line 560831
    return-void

    .line 560832
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 560833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560834
    const-class v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    .line 560835
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V
    .locals 4
    .param p1    # Lcom/facebook/user/model/PicSquareUrlWithSize;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/user/model/PicSquareUrlWithSize;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/user/model/PicSquareUrlWithSize;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 560836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560837
    if-eqz p1, :cond_2

    move v2, v0

    .line 560838
    :goto_0
    if-eqz p2, :cond_0

    .line 560839
    add-int/lit8 v2, v2, 0x1

    .line 560840
    :cond_0
    if-eqz p3, :cond_1

    .line 560841
    add-int/lit8 v2, v2, 0x1

    .line 560842
    :cond_1
    if-eqz v2, :cond_3

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 560843
    packed-switch v2, :pswitch_data_0

    .line 560844
    iget v0, p1, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    iget v1, p2, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    if-le v0, v1, :cond_a

    .line 560845
    :goto_2
    iget v0, p1, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    iget v1, p3, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    if-le v0, v1, :cond_9

    .line 560846
    :goto_3
    invoke-static {p2, p3, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    .line 560847
    :goto_4
    return-void

    :cond_2
    move v2, v1

    .line 560848
    goto :goto_0

    :cond_3
    move v0, v1

    .line 560849
    goto :goto_1

    .line 560850
    :pswitch_0
    if-eqz p1, :cond_4

    .line 560851
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    goto :goto_4

    .line 560852
    :cond_4
    if-eqz p2, :cond_5

    .line 560853
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    goto :goto_4

    .line 560854
    :cond_5
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    goto :goto_4

    .line 560855
    :pswitch_1
    if-nez p1, :cond_7

    move-object p1, p3

    .line 560856
    :cond_6
    :goto_5
    iget v0, p1, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    iget v1, p2, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    if-ge v0, v1, :cond_8

    .line 560857
    invoke-static {p1, p2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    goto :goto_4

    .line 560858
    :cond_7
    if-nez p2, :cond_6

    move-object p2, p3

    .line 560859
    goto :goto_5

    .line 560860
    :cond_8
    invoke-static {p2, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    goto :goto_4

    :cond_9
    move-object v3, p3

    move-object p3, p1

    move-object p1, v3

    goto :goto_3

    :cond_a
    move-object v3, p2

    move-object p2, p1

    move-object p1, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/PicSquareUrlWithSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560861
    iget-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    return-object v0
.end method

.method public final a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;
    .locals 4

    .prologue
    .line 560862
    iget-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    .line 560863
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 560864
    iget-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    .line 560865
    iget v3, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    if-gt p1, v3, :cond_0

    .line 560866
    :goto_1
    return-object v0

    .line 560867
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 560868
    :cond_1
    iget-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 560869
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 560870
    iget-object v0, p0, Lcom/facebook/user/model/PicSquare;->mPicSquareUrlsWithSizes:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 560871
    return-void
.end method
