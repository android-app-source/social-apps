.class public Lcom/facebook/user/model/PicSquareUrlWithSize;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/user/model/PicSquareUrlWithSizeDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/PicSquareUrlWithSize;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final size:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size"
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 560889
    const-class v0, Lcom/facebook/user/model/PicSquareUrlWithSizeDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 560872
    new-instance v0, LX/2u1;

    invoke-direct {v0}, LX/2u1;-><init>()V

    sput-object v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 560885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560886
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    .line 560887
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 560888
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 560881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560882
    iput p1, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    .line 560883
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 560884
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 560877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560878
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    .line 560879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 560880
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 560876
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 560873
    iget v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 560874
    iget-object v0, p0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 560875
    return-void
.end method
