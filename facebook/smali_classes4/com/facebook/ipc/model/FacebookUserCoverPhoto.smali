.class public Lcom/facebook/ipc/model/FacebookUserCoverPhoto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/model/FacebookUserCoverPhotoDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/model/FacebookUserCoverPhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final coverID:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cover_id"
    .end annotation
.end field

.field public final offsetX:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "offset_x"
    .end annotation
.end field

.field public final offsetY:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "offset_y"
    .end annotation
.end field

.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566541
    const-class v0, Lcom/facebook/ipc/model/FacebookUserCoverPhotoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566540
    const-class v0, Lcom/facebook/ipc/model/FacebookUserCoverPhotoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 566539
    new-instance v0, LX/3Qu;

    invoke-direct {v0}, LX/3Qu;-><init>()V

    sput-object v0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 566542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566543
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->coverID:J

    .line 566544
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    .line 566545
    iput v2, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetX:F

    .line 566546
    iput v2, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetY:F

    .line 566547
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 566533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566534
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->coverID:J

    .line 566535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    .line 566536
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetX:F

    .line 566537
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetY:F

    .line 566538
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 566527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566528
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->coverID:J

    .line 566529
    iput-object p1, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    .line 566530
    iput v2, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetX:F

    .line 566531
    iput v2, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetY:F

    .line 566532
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 566526
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 566521
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->coverID:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 566522
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->source:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566523
    iget v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetX:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 566524
    iget v0, p0, Lcom/facebook/ipc/model/FacebookUserCoverPhoto;->offsetY:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 566525
    return-void
.end method
