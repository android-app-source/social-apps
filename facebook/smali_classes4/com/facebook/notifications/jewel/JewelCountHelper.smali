.class public Lcom/facebook/notifications/jewel/JewelCountHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile k:Lcom/facebook/notifications/jewel/JewelCountHelper;


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/0xB;

.field private final d:LX/1rj;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final g:LX/2gd;

.field public final h:LX/0TD;

.field public final i:LX/2PR;

.field public final j:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 565304
    const-class v0, Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0xB;LX/1ri;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2gd;LX/0TD;LX/2PR;LX/0ad;)V
    .locals 1
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0xB;",
            "LX/1ri;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/2gd;",
            "LX/0TD;",
            "LX/2PR;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565306
    iput-object p1, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->b:LX/0tX;

    .line 565307
    iput-object p2, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->c:LX/0xB;

    .line 565308
    iget-object v0, p3, LX/1ri;->a:LX/1rj;

    move-object v0, v0

    .line 565309
    iput-object v0, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->d:LX/1rj;

    .line 565310
    iput-object p4, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->e:LX/0Or;

    .line 565311
    iput-object p5, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 565312
    iput-object p6, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->g:LX/2gd;

    .line 565313
    iput-object p7, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->h:LX/0TD;

    .line 565314
    iput-object p8, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->i:LX/2PR;

    .line 565315
    iput-object p9, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->j:LX/0ad;

    .line 565316
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/jewel/JewelCountHelper;
    .locals 13

    .prologue
    .line 565317
    sget-object v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->k:Lcom/facebook/notifications/jewel/JewelCountHelper;

    if-nez v0, :cond_1

    .line 565318
    const-class v1, Lcom/facebook/notifications/jewel/JewelCountHelper;

    monitor-enter v1

    .line 565319
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->k:Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565320
    if-eqz v2, :cond_0

    .line 565321
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565322
    new-instance v3, Lcom/facebook/notifications/jewel/JewelCountHelper;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v5

    check-cast v5, LX/0xB;

    invoke-static {v0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v6

    check-cast v6, LX/1ri;

    const/16 v7, 0x19e

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/2gd;->b(LX/0QB;)LX/2gd;

    move-result-object v9

    check-cast v9, LX/2gd;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    invoke-static {v0}, LX/2PR;->a(LX/0QB;)LX/2PR;

    move-result-object v11

    check-cast v11, LX/2PR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/notifications/jewel/JewelCountHelper;-><init>(LX/0tX;LX/0xB;LX/1ri;LX/0Or;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2gd;LX/0TD;LX/2PR;LX/0ad;)V

    .line 565323
    move-object v0, v3

    .line 565324
    sput-object v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->k:Lcom/facebook/notifications/jewel/JewelCountHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565325
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565326
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565327
    :cond_1
    sget-object v0, Lcom/facebook/notifications/jewel/JewelCountHelper;->k:Lcom/facebook/notifications/jewel/JewelCountHelper;

    return-object v0

    .line 565328
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 565330
    iget-object v1, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->d:LX/1rj;

    iget-object v0, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    sget-object v2, LX/2ub;->BACKGROUND:LX/2ub;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 565331
    iget-object v0, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350004

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 565332
    new-instance v0, LX/3SB;

    invoke-direct {v0}, LX/3SB;-><init>()V

    move-object v0, v0

    .line 565333
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/jewel/JewelCountHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 565334
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 565335
    move-object v0, v0

    .line 565336
    iget-object v1, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 565337
    new-instance v1, LX/3SC;

    invoke-direct {v1, p0}, LX/3SC;-><init>(Lcom/facebook/notifications/jewel/JewelCountHelper;)V

    iget-object v2, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->h:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 565338
    iget-object v0, p0, Lcom/facebook/notifications/jewel/JewelCountHelper;->i:LX/2PR;

    invoke-virtual {v0}, LX/2PR;->a()V

    .line 565339
    return-void
.end method
