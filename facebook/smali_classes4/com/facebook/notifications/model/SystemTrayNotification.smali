.class public Lcom/facebook/notifications/model/SystemTrayNotification;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/SystemTrayNotificationDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/SystemTrayNotification;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J

.field public final mHref:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "href"
    .end annotation
.end field

.field public final mIsLoggedOutPush:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_logged_out_push"
    .end annotation
.end field

.field public final mMessage:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation
.end field

.field public final mParams:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "params"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mServerUtcSecs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "time"
    .end annotation
.end field

.field public final mTargetUid:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_uid"
    .end annotation
.end field

.field public final mType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final mUnreadCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "unread_count"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566086
    const-class v0, Lcom/facebook/notifications/model/SystemTrayNotificationDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 566081
    const-class v0, Lcom/facebook/notifications/model/SystemTrayNotification;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/model/SystemTrayNotification;->a:Ljava/lang/String;

    .line 566082
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "A"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "R"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->APP_REQUEST:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "C"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "E"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "F"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "K"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GIFT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "G"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "a"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "N"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "P"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PAGE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "H"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "O"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->POKE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Q"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "S"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->STREAM:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "D"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->SUPPORT_DASHBOARD:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "U"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "T"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->USER_ABOUT:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "V"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "B"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->NEARBY_FRIEND:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "9"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->PLACE_FEED:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "2"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/model/SystemTrayNotification;->b:LX/0P1;

    .line 566083
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->HOTP_LOGIN_APPROVALS:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->LA_PUSH_AUTHENTICATE:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->POST_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v4, Lcom/facebook/notifications/constants/NotificationType;->AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v5, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    const/4 v6, 0x3

    new-array v6, v6, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v7, 0x0

    sget-object v8, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/facebook/notifications/constants/NotificationType;->DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/model/SystemTrayNotification;->c:LX/0Rf;

    .line 566084
    new-instance v0, LX/3En;

    invoke-direct {v0}, LX/3En;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/SystemTrayNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 566087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566088
    iput-object v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    .line 566089
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    .line 566090
    iput-object v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    .line 566091
    iput v3, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mUnreadCount:I

    .line 566092
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mTargetUid:J

    .line 566093
    iput-object v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    .line 566094
    iput-object v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    .line 566095
    iput-boolean v3, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mIsLoggedOutPush:Z

    .line 566096
    iput-object v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->f:LX/0am;

    .line 566097
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 566098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566099
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    .line 566100
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    .line 566101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    .line 566102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mUnreadCount:I

    .line 566103
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mTargetUid:J

    .line 566104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    .line 566105
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    .line 566106
    iget-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-class v2, Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 566107
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mIsLoggedOutPush:Z

    .line 566108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->h:Ljava/lang/String;

    .line 566109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->i:Ljava/lang/String;

    .line 566110
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->j:J

    .line 566111
    return-void

    .line 566112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()LX/0am;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566113
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->d:LX/0am;

    if-nez v0, :cond_0

    .line 566114
    const-string v0, "i"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->g(Ljava/lang/String;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->d:LX/0am;

    .line 566115
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->d:LX/0am;

    return-object v0
.end method

.method public static a(Lcom/facebook/notifications/constants/NotificationType;)Z
    .locals 1

    .prologue
    .line 566116
    sget-object v0, Lcom/facebook/notifications/model/SystemTrayNotification;->c:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 566117
    invoke-direct {p0, p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->g(Ljava/lang/String;)LX/0am;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private f(Ljava/lang/String;)LX/0am;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566118
    invoke-direct {p0, p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 566119
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 566120
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 566121
    :goto_0
    return-object v0

    .line 566122
    :cond_0
    :try_start_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 566123
    :catch_0
    move-exception v0

    .line 566124
    sget-object v1, Lcom/facebook/notifications/model/SystemTrayNotification;->a:Ljava/lang/String;

    const-string v2, "NumberFormatException: %s must be a number"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566125
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)LX/0am;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566126
    invoke-direct {p0, p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 566127
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 566128
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 566129
    :goto_0
    return-object v0

    .line 566130
    :cond_0
    :try_start_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 566131
    :catch_0
    move-exception v0

    .line 566132
    sget-object v1, Lcom/facebook/notifications/model/SystemTrayNotification;->a:Ljava/lang/String;

    const-string v2, "NumberFormatException: %s must be a number"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566133
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method private h(Ljava/lang/String;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566134
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 566135
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 566136
    :goto_0
    return-object v0

    .line 566137
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 566138
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566139
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 566140
    :cond_1
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method private i(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;
    .locals 3

    .prologue
    .line 566141
    if-eqz p1, :cond_2

    .line 566142
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 566143
    if-ltz v0, :cond_0

    .line 566144
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 566145
    :cond_0
    invoke-static {p1}, Lcom/facebook/notifications/constants/NotificationType;->fromString(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    .line 566146
    if-eqz v0, :cond_2

    .line 566147
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->STALE_EMAIL:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v1, :cond_1

    .line 566148
    invoke-virtual {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h()LX/0am;

    move-result-object v1

    .line 566149
    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->CONTACT_IMPORTER:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    if-ne v1, v2, :cond_1

    .line 566150
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->STALE_CONTACT_IMPORT:Lcom/facebook/notifications/constants/NotificationType;

    .line 566151
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    goto :goto_0
.end method


# virtual methods
.method public final A()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566152
    const-string v0, "a_md"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 566199
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "subtext"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "subtext"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()J
    .locals 2

    .prologue
    .line 566153
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mTargetUid:J

    return-wide v0
.end method

.method public final D()Z
    .locals 4

    .prologue
    .line 566154
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mTargetUid:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/constants/NotificationType;
    .locals 1

    .prologue
    .line 566155
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->i(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/facebook/notifications/model/SystemTrayNotification;
    .locals 1

    .prologue
    .line 566156
    iput-wide p1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->j:J

    .line 566157
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;
    .locals 0

    .prologue
    .line 566158
    iput-object p1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->i:Ljava/lang/String;

    .line 566159
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;
    .locals 0

    .prologue
    .line 566160
    iput-object p1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->h:Ljava/lang/String;

    .line 566161
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 566162
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566163
    invoke-direct {p0, p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
    .locals 5

    .prologue
    .line 566164
    new-instance v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v0}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    iget-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    .line 566165
    iput-object v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a:Ljava/lang/String;

    .line 566166
    move-object v0, v0

    .line 566167
    invoke-virtual {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v1

    .line 566168
    iput-object v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    .line 566169
    move-object v1, v0

    .line 566170
    const-string v0, "log_data"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 566171
    iput-object v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 566172
    move-object v1, v1

    .line 566173
    invoke-direct {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->E()LX/0am;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 566174
    iput-wide v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    .line 566175
    move-object v1, v1

    .line 566176
    invoke-virtual {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 566177
    iput-object v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    .line 566178
    move-object v1, v1

    .line 566179
    const-string v0, "o"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 566180
    iput-object v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    .line 566181
    move-object v1, v1

    .line 566182
    const-string v0, "t"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 566183
    iput-object v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->h:Ljava/lang/String;

    .line 566184
    move-object v0, v1

    .line 566185
    iget-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->i:Ljava/lang/String;

    .line 566186
    iput-object v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    .line 566187
    move-object v0, v0

    .line 566188
    iget-object v1, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->h:Ljava/lang/String;

    .line 566189
    iput-object v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    .line 566190
    move-object v0, v0

    .line 566191
    iget-wide v2, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->j:J

    .line 566192
    iput-wide v2, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->t:J

    .line 566193
    move-object v0, v0

    .line 566194
    return-object v0
.end method

.method public final d()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566195
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->e:LX/0am;

    if-nez v0, :cond_0

    .line 566196
    const-string v0, "gi"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->e:LX/0am;

    .line 566197
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->e:LX/0am;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566085
    invoke-direct {p0, p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->f(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 566198
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566067
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->f:LX/0am;

    if-nez v0, :cond_0

    .line 566068
    const-string v0, "ppu"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->f:LX/0am;

    .line 566069
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->f:LX/0am;

    return-object v0
.end method

.method public final f()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566064
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->g:LX/0am;

    if-nez v0, :cond_0

    .line 566065
    const-string v0, "uid"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->g:LX/0am;

    .line 566066
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->g:LX/0am;

    return-object v0
.end method

.method public final g()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566063
    const-string v0, "o"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->g(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566059
    const-string v0, "t"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 566060
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 566061
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 566062
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/facebook/notifications/model/SystemTrayNotification;->b:LX/0P1;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566058
    const-string v0, "d"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566057
    const-string v0, "id_override"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 566040
    const-string v0, "f"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 566039
    const-string v0, "vh"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 566042
    const-string v0, "disable_light"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 566043
    const-string v0, "disable_sound"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 566041
    const-string v0, "disable_vibrate"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 566070
    iget-boolean v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mIsLoggedOutPush:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 566071
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "subtext"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 566072
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "pa_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "pa_href"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 566073
    invoke-virtual {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "sa_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    const-string v1, "sa_href"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
    .locals 1

    .prologue
    .line 566074
    const-string v0, "pa_type"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;
    .locals 1

    .prologue
    .line 566075
    const-string v0, "sa_type"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566076
    const-string v0, "p_actions"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final w()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566077
    const-string v0, "rc_url"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 566044
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566045
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 566046
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566047
    iget v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mUnreadCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 566048
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mTargetUid:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 566049
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566050
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mParams:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 566051
    iget-boolean v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mIsLoggedOutPush:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 566052
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566053
    iget-object v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566054
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 566055
    return-void

    .line 566056
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566078
    const-string v0, "sa_href"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final y()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566079
    const-string v0, "pa_text"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final z()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566080
    const-string v0, "sa_text"

    invoke-direct {p0, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->h(Ljava/lang/String;)LX/0am;

    move-result-object v0

    return-object v0
.end method
