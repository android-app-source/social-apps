.class public Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0po;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static volatile s:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;


# instance fields
.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final e:LX/2A8;

.field public final f:Landroid/content/ContentResolver;

.field private final g:LX/1qv;

.field public final h:LX/2AD;

.field public final i:LX/2AD;

.field public final j:LX/03V;

.field public final k:LX/0Sh;

.field public final l:LX/0TD;

.field private final m:LX/1rU;

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/2AJ;

.field public final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 453157
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, LX/2A7;->b:LX/0U1;

    .line 453158
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453159
    aput-object v1, v0, v3

    sget-object v1, LX/2A7;->f:LX/0U1;

    .line 453160
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453161
    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b:[Ljava/lang/String;

    .line 453162
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/2A7;->e:LX/0U1;

    .line 453163
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453164
    aput-object v1, v0, v3

    sget-object v1, LX/2A7;->b:LX/0U1;

    .line 453165
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453166
    aput-object v1, v0, v4

    sget-object v1, LX/2A7;->s:LX/0U1;

    .line 453167
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453168
    aput-object v1, v0, v5

    sget-object v1, LX/2A7;->u:LX/0U1;

    .line 453169
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453170
    aput-object v1, v0, v6

    sget-object v1, LX/2A7;->v:LX/0U1;

    .line 453171
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453172
    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2A7;->w:LX/0U1;

    .line 453173
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 453174
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2A7;->x:LX/0U1;

    .line 453175
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 453176
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2A7;->y:LX/0U1;

    .line 453177
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 453178
    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c:[Ljava/lang/String;

    .line 453179
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/2A7;->d:LX/0U1;

    .line 453180
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453181
    aput-object v1, v0, v3

    sget-object v1, LX/2A7;->e:LX/0U1;

    .line 453182
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453183
    aput-object v1, v0, v4

    sget-object v1, LX/2A7;->g:LX/0U1;

    .line 453184
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453185
    aput-object v1, v0, v5

    sget-object v1, LX/2A7;->f:LX/0U1;

    .line 453186
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453187
    aput-object v1, v0, v6

    sget-object v1, LX/2A7;->j:LX/0U1;

    .line 453188
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 453189
    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2A7;->i:LX/0U1;

    .line 453190
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453191
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2A7;->k:LX/0U1;

    .line 453192
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453193
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2A7;->l:LX/0U1;

    .line 453194
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453195
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2A7;->b:LX/0U1;

    .line 453196
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453197
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2A7;->m:LX/0U1;

    .line 453198
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453199
    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2A7;->r:LX/0U1;

    .line 453200
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453201
    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2A7;->s:LX/0U1;

    .line 453202
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453203
    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2A7;->t:LX/0U1;

    .line 453204
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453205
    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2A7;->v:LX/0U1;

    .line 453206
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453207
    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2A7;->w:LX/0U1;

    .line 453208
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453209
    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2A7;->x:LX/0U1;

    .line 453210
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 453211
    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2A8;Landroid/content/ContentResolver;LX/1qv;LX/2AD;LX/2AD;LX/03V;LX/0Sh;LX/0pq;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1qw;LX/0TD;LX/2AJ;LX/0Or;LX/1rU;)V
    .locals 1
    .param p2    # Landroid/content/ContentResolver;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .param p4    # LX/2AD;
        .annotation runtime Lcom/facebook/notifications/annotations/RegularNotificationCache;
        .end annotation
    .end param
    .param p5    # LX/2AD;
        .annotation runtime Lcom/facebook/notifications/annotations/OverflowedNotificationCache;
        .end annotation
    .end param
    .param p11    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2A8;",
            "Landroid/content/ContentResolver;",
            "LX/1qv;",
            "LX/2AD;",
            "LX/2AD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0pq;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1qw;",
            "LX/0TD;",
            "LX/2AJ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1rU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 453212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    .line 453214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->o:Ljava/util/Map;

    .line 453215
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 453216
    iput-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->r:LX/0Ot;

    .line 453217
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2A8;

    iput-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    .line 453218
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    .line 453219
    iput-object p3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->g:LX/1qv;

    .line 453220
    iput-object p4, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    .line 453221
    iput-object p5, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    .line 453222
    iput-object p6, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    .line 453223
    iput-object p7, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    .line 453224
    iput-object p9, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 453225
    iput-object p11, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->l:LX/0TD;

    .line 453226
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0}, LX/2AE;->g()V

    .line 453227
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0}, LX/2AE;->g()V

    .line 453228
    iput-object p14, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m:LX/1rU;

    .line 453229
    invoke-virtual {p8, p0}, LX/0pq;->a(LX/0po;)V

    .line 453230
    iput-object p12, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->p:LX/2AJ;

    .line 453231
    iput-object p13, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->q:LX/0Or;

    .line 453232
    new-instance v0, LX/2CG;

    invoke-direct {v0, p0}, LX/2CG;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;)V

    invoke-virtual {p10, v0}, LX/1qw;->a(LX/1rV;)V

    .line 453233
    return-void
.end method

.method private a(JI)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453234
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 453235
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/2A7;->f:LX/0U1;

    .line 453236
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 453237
    aput-object v3, v0, v2

    invoke-static {p0, p3, v0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v2

    .line 453238
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453239
    sget-object v0, LX/2A7;->f:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 453240
    :cond_0
    :try_start_1
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453241
    :goto_0
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 453242
    :cond_1
    if-eqz v2, :cond_2

    .line 453243
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 453244
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 453245
    :catch_0
    move-exception v0

    .line 453246
    :try_start_3
    const-string v4, "GraphQLNotificationsContentProviderHelper"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 453247
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    .line 453248
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;Z)LX/2nq;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 453249
    if-nez p1, :cond_1

    .line 453250
    :cond_0
    :goto_0
    return-object v0

    .line 453251
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v1, p1}, LX/2AD;->a(Landroid/database/Cursor;)LX/2nq;

    move-result-object v1

    .line 453252
    if-nez v1, :cond_2

    .line 453253
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v1, p1}, LX/2AD;->a(Landroid/database/Cursor;)LX/2nq;

    move-result-object v1

    .line 453254
    :cond_2
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_3

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private a(LX/2nq;J)Landroid/content/ContentValues;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 453255
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 453256
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 453257
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 453258
    sget-object v0, LX/2A7;->b:LX/0U1;

    .line 453259
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453260
    invoke-static {v4}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453261
    sget-object v0, LX/2A7;->d:LX/0U1;

    .line 453262
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453263
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453264
    sget-object v0, LX/2A7;->e:LX/0U1;

    .line 453265
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453266
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 453267
    sget-object v0, LX/2A7;->f:LX/0U1;

    .line 453268
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453269
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453270
    sget-object v0, LX/2A7;->c:LX/0U1;

    .line 453271
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453272
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 453273
    sget-object v0, LX/2A7;->g:LX/0U1;

    .line 453274
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453275
    invoke-interface {p1}, LX/2nq;->ia_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453276
    invoke-static {v4}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 453277
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 453278
    sget-object v2, LX/2A7;->i:LX/0U1;

    .line 453279
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 453280
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453281
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 453282
    sget-object v0, LX/2A7;->j:LX/0U1;

    .line 453283
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453284
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLIcon;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453285
    :cond_1
    sget-object v0, LX/2A7;->h:LX/0U1;

    .line 453286
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453287
    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-static {p0, v4, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453288
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 453289
    if-nez v0, :cond_2

    .line 453290
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v2, "GraphQLNotificationsContentProviderHelper_summary_null"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Notification story text is null for notification ID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453291
    const-string v0, ""

    invoke-static {v0}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 453292
    :cond_2
    sget-object v2, LX/2A7;->k:LX/0U1;

    .line 453293
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 453294
    sget-object v5, LX/16Z;->a:LX/16Z;

    invoke-static {p0, v0, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453295
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 453296
    sget-object v0, LX/2A7;->l:LX/0U1;

    .line 453297
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453298
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    sget-object v5, LX/16Z;->a:LX/16Z;

    invoke-static {p0, v2, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453299
    :cond_3
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 453300
    sget-object v0, LX/2A7;->r:LX/0U1;

    .line 453301
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453302
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-static {p0, v2, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453303
    :cond_4
    sget-object v0, LX/2A7;->s:LX/0U1;

    .line 453304
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 453305
    invoke-interface {p1}, LX/2nq;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 453306
    invoke-interface {p1}, LX/2nq;->n()LX/BAy;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 453307
    sget-object v0, LX/2A7;->t:LX/0U1;

    .line 453308
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453309
    invoke-interface {p1}, LX/2nq;->n()LX/BAy;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v2

    invoke-static {p0, v2, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453310
    :cond_5
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 453311
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 453312
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    .line 453313
    :goto_1
    if-ge v2, v7, :cond_7

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 453314
    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453315
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_6
    move v0, v1

    .line 453316
    goto :goto_0

    .line 453317
    :cond_7
    sget-object v0, LX/2A7;->v:LX/0U1;

    .line 453318
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453319
    const/4 v2, 0x0

    invoke-static {v5, v2}, LX/4Bz;->a(Ljava/util/List;LX/16a;)[B

    move-result-object v2

    move-object v2, v2

    .line 453320
    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 453321
    :cond_8
    sget-object v0, LX/2A7;->w:LX/0U1;

    .line 453322
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453323
    invoke-interface {p1}, LX/2nq;->l()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 453324
    invoke-interface {p1}, LX/2nq;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, LX/2nq;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 453325
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 453326
    invoke-interface {p1}, LX/2nq;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_2
    if-ge v1, v6, :cond_9

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 453327
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x2c

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 453328
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 453329
    :cond_9
    sget-object v0, LX/2A7;->x:LX/0U1;

    .line 453330
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 453331
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453332
    :cond_a
    sget-object v0, LX/2A7;->y:LX/0U1;

    .line 453333
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 453334
    const/4 v1, 0x0

    .line 453335
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 453336
    const-string v5, "_"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 453337
    array-length v2, v5

    const/4 v6, 0x2

    if-eq v2, v6, :cond_c

    .line 453338
    :cond_b
    :goto_3
    move v1, v1

    .line 453339
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 453340
    return-object v3

    .line 453341
    :cond_c
    invoke-static {}, LX/BAT;->values()[LX/BAT;

    move-result-object v6

    array-length v7, v6

    move v2, v1

    :goto_4
    if-ge v2, v7, :cond_b

    aget-object v8, v6, v2

    .line 453342
    invoke-virtual {v8}, LX/BAT;->toString()Ljava/lang/String;

    move-result-object v8

    aget-object v9, v5, v1

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 453343
    const/4 v1, 0x1

    goto :goto_3

    .line 453344
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method private static a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 453345
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v0, LX/2A8;->b:Landroid/net/Uri;

    .line 453346
    if-lez p1, :cond_0

    .line 453347
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "LIMIT"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 453348
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v0, p3, v2

    if-eqz v0, :cond_1

    .line 453349
    sget-object v0, LX/2A7;->c:LX/0U1;

    .line 453350
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453351
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 453352
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    .line 453353
    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    .line 453354
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 453355
    :goto_1
    return-object v0

    .line 453356
    :catch_0
    move-exception v0

    .line 453357
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v2, "GraphQLNotificationsContentProviderHelper"

    const-string v3, "failed trying to get cursor during query"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 453358
    const-string v1, "GraphQLNotificationsContentProviderHelper"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    .line 453359
    goto :goto_1

    :cond_1
    move-object v4, v6

    move-object v3, v6

    goto :goto_0
.end method

.method private static a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 453360
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v0, LX/2A8;->b:Landroid/net/Uri;

    .line 453361
    sget-object v0, LX/2A7;->b:LX/0U1;

    .line 453362
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453363
    invoke-static {v0, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 453364
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .locals 3

    .prologue
    .line 453365
    sget-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->s:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    if-nez v0, :cond_1

    .line 453366
    const-class v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    monitor-enter v1

    .line 453367
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->s:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 453368
    if-eqz v2, :cond_0

    .line 453369
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->s:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453370
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 453371
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453372
    :cond_1
    sget-object v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->s:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    return-object v0

    .line 453373
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 453374
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 0
    .param p0    # Ljava/lang/Iterable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;)",
            "Ljava/lang/Iterable",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 453375
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B
    .locals 4
    .param p1    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453376
    :try_start_0
    invoke-static {p1, p2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 453377
    :goto_0
    return-object v0

    .line 453378
    :catch_0
    move-exception v0

    .line 453379
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v2, "GraphQLNotificationsContentProviderHelper"

    const-string v3, "Failed to serialize object as flatbuffer"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 453380
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;JZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;JZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 453381
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "should be passed some notifs"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453382
    if-eqz p4, :cond_3

    .line 453383
    invoke-virtual {p0, p2, p3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1e

    .line 453384
    if-ltz v0, :cond_2

    .line 453385
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 453386
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 453387
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v3, v2}, LX/2AG;->a(Ljava/lang/Iterable;)V

    .line 453388
    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 453389
    if-nez v0, :cond_0

    .line 453390
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0}, LX/2AF;->f()V

    .line 453391
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453392
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 453393
    goto :goto_0

    :cond_2
    move v0, v1

    .line 453394
    goto :goto_1

    .line 453395
    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v4, v0, [Landroid/content/ContentValues;

    .line 453396
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 453397
    :try_start_0
    invoke-direct {p0, v0, p2, p3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/2nq;J)Landroid/content/ContentValues;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 453398
    add-int/lit8 v2, v1, 0x1

    :try_start_1
    aput-object v3, v4, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move v1, v2

    .line 453399
    goto :goto_3

    .line 453400
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 453401
    :goto_4
    const-string v2, ""

    .line 453402
    if-eqz v0, :cond_a

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 453403
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 453404
    :goto_5
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v6, "GraphQLNotificationsContentProviderHelper_content_values_error"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Exception thrown when converting GraphQL notification to ContentValues. NotificationId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v6, v0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 453405
    :cond_4
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v0, v0, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "NO_NOTIFY"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 453406
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    .line 453407
    if-nez v0, :cond_6

    .line 453408
    array-length v3, v4

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v3, :cond_6

    aget-object v5, v4, v1

    .line 453409
    iget-object v6, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v6, v2, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 453410
    add-int/lit8 v0, v0, 0x1

    .line 453411
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 453412
    :cond_6
    move v0, v0

    .line 453413
    if-lez v0, :cond_9

    .line 453414
    const/16 v3, 0x1e

    .line 453415
    invoke-virtual {p0, p2, p3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(J)LX/0Px;

    move-result-object v1

    .line 453416
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    .line 453417
    if-le v2, v3, :cond_8

    .line 453418
    invoke-virtual {v1, v3, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    .line 453419
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v2}, LX/2AG;->c()I

    move-result v2

    if-lez v2, :cond_7

    .line 453420
    invoke-static {p0, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/lang/Iterable;)V

    .line 453421
    :cond_7
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/util/List;Z)V

    .line 453422
    :cond_8
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v2, v2, LX/2A8;->b:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 453423
    :cond_9
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->g:LX/1qv;

    .line 453424
    iget-object v2, v1, LX/1qv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 453425
    sget-object v3, LX/0hM;->f:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 453426
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 453427
    goto/16 :goto_2

    .line 453428
    :catch_1
    move-exception v1

    move-object v3, v1

    move v1, v2

    goto/16 :goto_4

    :cond_a
    move-object v0, v2

    goto/16 :goto_5
.end method

.method private static b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 453429
    if-nez p2, :cond_0

    .line 453430
    :goto_0
    return-object v5

    .line 453431
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v0, LX/2A8;->b:Landroid/net/Uri;

    .line 453432
    sget-object v0, LX/2A7;->e:LX/0U1;

    .line 453433
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 453434
    invoke-static {v0, p2}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 453435
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .locals 15

    .prologue
    .line 453456
    new-instance v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/2A8;->b(LX/0QB;)LX/2A8;

    move-result-object v1

    check-cast v1, LX/2A8;

    invoke-interface {p0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v2

    invoke-static {v2}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v2

    check-cast v2, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/1qv;->b(LX/0QB;)LX/1qv;

    move-result-object v3

    check-cast v3, LX/1qv;

    invoke-static {p0}, LX/2AB;->b(LX/0QB;)LX/2AD;

    move-result-object v4

    check-cast v4, LX/2AD;

    invoke-static {p0}, LX/2AI;->b(LX/0QB;)LX/2AD;

    move-result-object v5

    check-cast v5, LX/2AD;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v8

    check-cast v8, LX/0pq;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v10

    check-cast v10, LX/1qw;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-static {p0}, LX/2AJ;->a(LX/0QB;)LX/2AJ;

    move-result-object v12

    check-cast v12, LX/2AJ;

    const/16 v13, 0x15e7

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v14

    check-cast v14, LX/1rU;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;-><init>(LX/2A8;Landroid/content/ContentResolver;LX/1qv;LX/2AD;LX/2AD;LX/03V;LX/0Sh;LX/0pq;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1qw;LX/0TD;LX/2AJ;LX/0Or;LX/1rU;)V

    .line 453457
    const/16 v1, 0x1a1

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 453458
    iput-object v1, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->r:LX/0Ot;

    .line 453459
    return-object v0
.end method

.method public static b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 453436
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    .line 453437
    iput-boolean v1, v0, LX/2AF;->b:Z

    .line 453438
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 453439
    sget-object v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a:[Ljava/lang/String;

    invoke-static {p0, v2, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 453440
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 453441
    :cond_1
    if-eqz v2, :cond_0

    .line 453442
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 453443
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v2, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;Z)LX/2nq;

    move-result-object v0

    .line 453444
    if-nez v0, :cond_3

    .line 453445
    invoke-static {p0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;)LX/2nq;

    move-result-object v0

    .line 453446
    :cond_3
    if-eqz v0, :cond_4

    .line 453447
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v3, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453448
    :cond_4
    if-eqz v2, :cond_0

    .line 453449
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 453450
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 453451
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 453452
    :cond_6
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0}, LX/2AF;->f()V

    .line 453453
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v1, 0x1

    .line 453454
    iput-boolean v1, v0, LX/2AF;->b:Z

    .line 453455
    return-void
.end method

.method public static b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 453686
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453687
    :goto_0
    return-void

    .line 453688
    :cond_0
    sget-object v0, LX/2A7;->b:LX/0U1;

    .line 453689
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 453690
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 453691
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v0, v0, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "NO_NOTIFY"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v0, v0, LX/2A8;->b:Landroid/net/Uri;

    goto :goto_1
.end method

.method private c(Landroid/database/Cursor;)LX/BAy;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 453670
    sget-object v0, LX/2A7;->t:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 453671
    if-eq v1, v5, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Passing a cursor without a notif option set column?"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453672
    :try_start_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 453673
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v2

    .line 453674
    :goto_1
    return-object v0

    .line 453675
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 453676
    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 453677
    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    const-class v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    invoke-direct {v1, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 453678
    goto :goto_1

    .line 453679
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 453680
    const-string v0, "Error reading notifOptionSets from cursor."

    .line 453681
    sget-object v3, LX/2A7;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453682
    if-eq v3, v5, :cond_3

    .line 453683
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " CacheId: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453684
    :cond_3
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v4, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v3, v4, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 453685
    goto :goto_1
.end method

.method private e(Landroid/database/Cursor;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 453659
    sget-object v0, LX/2A7;->w:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 453660
    if-eq v2, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Passing a cursor without a num impressions column?"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453661
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 453662
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 453663
    goto :goto_0

    .line 453664
    :catch_0
    move-exception v2

    .line 453665
    const-string v0, "Error reading num impressions from cursor."

    .line 453666
    sget-object v3, LX/2A7;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453667
    if-eq v3, v4, :cond_1

    .line 453668
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " CacheId: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453669
    :cond_1
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v4, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v3, v4, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static f(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;J)J
    .locals 11

    .prologue
    .line 453637
    const-wide/16 v6, -0x1

    .line 453638
    const/4 v8, 0x0

    .line 453639
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v1, LX/2A8;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2A7;->e:LX/0U1;

    .line 453640
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453641
    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2A7;->c:LX/0U1;

    .line 453642
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453643
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/2A7;->e:LX/0U1;

    .line 453644
    iget-object v10, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, v10

    .line 453645
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " asc LIMIT 1"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 453646
    if-eqz v2, :cond_3

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453647
    sget-object v0, LX/2A7;->e:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v6

    move-wide v0, v6

    .line 453648
    :goto_0
    if-eqz v2, :cond_0

    .line 453649
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 453650
    :cond_0
    :goto_1
    return-wide v0

    .line 453651
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 453652
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v3, "GraphQLNotificationsContentProviderHelper_oldest_notif_time_fetch_error"

    const-string v4, "Error fetching oldest notif"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 453653
    if-eqz v1, :cond_2

    .line 453654
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    goto :goto_1

    .line 453655
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_1

    .line 453656
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 453657
    :catchall_1
    move-exception v0

    move-object v8, v2

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v8, v1

    goto :goto_3

    .line 453658
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_2
    move-wide v0, v6

    goto :goto_1

    :cond_3
    move-wide v0, v6

    goto :goto_0
.end method

.method private f(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 453692
    sget-object v0, LX/2A7;->s:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453693
    if-eq v3, v5, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Passing a cursor without a is rich notif collapsed column?"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453694
    :try_start_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    .line 453695
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 453696
    goto :goto_0

    :cond_1
    move v1, v2

    .line 453697
    goto :goto_1

    .line 453698
    :catch_0
    move-exception v1

    .line 453699
    const-string v0, "Error reading is rich notif collapsed from cursor."

    .line 453700
    sget-object v3, LX/2A7;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453701
    if-eq v3, v5, :cond_2

    .line 453702
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " CacheId: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453703
    :cond_2
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v4, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v3, v4, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v2

    goto :goto_1
.end method

.method private h(Landroid/database/Cursor;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453591
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 453592
    sget-object v0, LX/2A7;->r:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 453593
    if-eq v2, v4, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Passing a cursor without a reaction unit column?"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453594
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 453595
    if-eqz v0, :cond_0

    array-length v2, v0

    if-nez v2, :cond_4

    :cond_0
    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 453596
    :goto_1
    move-object v0, v0

    .line 453597
    if-nez v0, :cond_1

    .line 453598
    const/4 v0, 0x0

    .line 453599
    :goto_2
    return-object v0

    :cond_1
    new-instance v1, LX/BDG;

    invoke-direct {v1}, LX/BDG;-><init>()V

    .line 453600
    iput-object v0, v1, LX/BDG;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 453601
    move-object v0, v1

    .line 453602
    const/4 v3, 0x0

    const/4 v6, -0x1

    .line 453603
    sget-object v2, LX/2A7;->v:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 453604
    if-eq v4, v6, :cond_6

    const/4 v2, 0x1

    :goto_3
    const-string v5, "Passing a cursor without a highlight operation column?"

    invoke-static {v2, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453605
    :try_start_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 453606
    if-eqz v2, :cond_2

    array-length v4, v2

    if-nez v4, :cond_7

    :cond_2
    move-object v2, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 453607
    :goto_4
    move-object v1, v2

    .line 453608
    iput-object v1, v0, LX/BDG;->a:LX/0Px;

    .line 453609
    move-object v0, v0

    .line 453610
    invoke-virtual {v0}, LX/BDG;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    goto :goto_2

    .line 453611
    :cond_3
    :try_start_2
    const/4 v0, 0x0

    goto :goto_0

    .line 453612
    :cond_4
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 453613
    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/15i;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 453614
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 453615
    const-string v0, "Error reading reaction unit from cursor."

    .line 453616
    sget-object v3, LX/2A7;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453617
    if-eq v3, v4, :cond_5

    .line 453618
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " CacheId: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453619
    :cond_5
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v4, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v3, v4, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 453620
    goto :goto_1

    .line 453621
    :cond_6
    :try_start_3
    const/4 v2, 0x0

    goto :goto_3

    .line 453622
    :cond_7
    const-class v4, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    const/4 v10, 0x0

    .line 453623
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    const-string v9, "GraphQLNotificationsContentProviderHelper"

    const/4 v12, 0x0

    move-object v8, v4

    move-object v11, v10

    move-object v13, v10

    invoke-static/range {v7 .. v13}, LX/4Bz;->a(Ljava/nio/ByteBuffer;Ljava/lang/Class;Ljava/lang/String;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)Ljava/util/List;

    move-result-object v7

    .line 453624
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 453625
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_8
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/flatbuffers/Flattenable;

    .line 453626
    if-eqz v7, :cond_8

    .line 453627
    invoke-virtual {v8, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 453628
    :cond_9
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    move-object v2, v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 453629
    goto :goto_4

    .line 453630
    :catch_1
    move-exception v2

    move-object v4, v2

    .line 453631
    const-string v2, "Error reading highlightOperations from cursor."

    .line 453632
    sget-object v5, LX/2A7;->f:LX/0U1;

    invoke-virtual {v5, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 453633
    if-eq v5, v6, :cond_a

    .line 453634
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " CacheId: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 453635
    :cond_a
    iget-object v5, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v6, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v5, v6, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    .line 453636
    goto/16 :goto_4
.end method

.method private static h(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 453568
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 453569
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v1, LX/2A8;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2A7;->e:LX/0U1;

    .line 453570
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453571
    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2A7;->c:LX/0U1;

    .line 453572
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453573
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/2A7;->d:LX/0U1;

    .line 453574
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453575
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LX/2A7;->e:LX/0U1;

    .line 453576
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 453577
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " asc LIMIT 1"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 453578
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453579
    sget-object v0, LX/2A7;->e:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    .line 453580
    :goto_0
    if-eqz v1, :cond_0

    .line 453581
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 453582
    :cond_0
    :goto_1
    return-object v0

    .line 453583
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 453584
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v3, "GraphQLNotificationsContentProviderHelper_oldest_unseen_notif_time_fetch_error"

    const-string v4, "Error fetching oldest unseen notif"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453585
    if-eqz v1, :cond_2

    .line 453586
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    .line 453587
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    .line 453588
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 453589
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    .line 453590
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method private static j(Landroid/database/Cursor;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453560
    sget-object v0, LX/2A7;->x:LX/0U1;

    invoke-virtual {v0, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 453561
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 453562
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 453563
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 453564
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 453565
    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 453566
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 453567
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private k(Landroid/database/Cursor;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v4, 0x350006

    .line 453534
    sget-object v0, LX/2A7;->d:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 453535
    sget-object v0, LX/2A7;->e:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 453536
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 453537
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "connection_controller"

    :goto_0
    invoke-interface {v3, v4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 453538
    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 453539
    sget-object v0, LX/2A7;->h:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 453540
    if-eq v5, v7, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v6, "Passing a cursor without a gql payload column?"

    invoke-static {v0, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453541
    :try_start_0
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 453542
    if-eqz v0, :cond_0

    array-length v5, v0

    if-nez v5, :cond_4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 453543
    :cond_0
    :goto_2
    move-object v0, v3

    .line 453544
    if-eqz v0, :cond_2

    .line 453545
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLStorySeenState;J)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 453546
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 453547
    :goto_3
    return-object v0

    .line 453548
    :cond_1
    const-string v0, "not_connection_controller"

    goto :goto_0

    .line 453549
    :cond_2
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x3

    invoke-interface {v1, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_3

    .line 453550
    :cond_3
    :try_start_1
    const/4 v0, 0x0

    goto :goto_1

    .line 453551
    :cond_4
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 453552
    sget-object v5, LX/16Z;->a:LX/16Z;

    const/4 v6, 0x0

    invoke-static {v0, v5, v6}, LX/15i;->a(Ljava/nio/ByteBuffer;LX/16a;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_4
    move-object v3, v0

    .line 453553
    goto :goto_2

    .line 453554
    :catch_0
    move-exception v0

    move-object v5, v0

    .line 453555
    const-string v0, "Error reading notification story from cursor."

    .line 453556
    sget-object v6, LX/2A7;->f:LX/0U1;

    invoke-virtual {v6, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 453557
    if-eq v6, v7, :cond_5

    .line 453558
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " CacheId: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453559
    :cond_5
    iget-object v6, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v7, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v6, v7, v0, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v3

    goto :goto_4
.end method

.method private l(Landroid/database/Cursor;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 453460
    sget-object v0, LX/2A7;->d:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 453461
    sget-object v1, LX/2A7;->e:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 453462
    sget-object v2, LX/2A7;->i:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 453463
    sget-object v3, LX/2A7;->j:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453464
    sget-object v4, LX/2A7;->b:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 453465
    sget-object v5, LX/2A7;->f:LX/0U1;

    invoke-virtual {v5, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 453466
    new-instance v6, LX/23u;

    invoke-direct {v6}, LX/23u;-><init>()V

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 453467
    iput-object v5, v6, LX/23u;->m:Ljava/lang/String;

    .line 453468
    move-object v5, v6

    .line 453469
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 453470
    iput-object v4, v5, LX/23u;->N:Ljava/lang/String;

    .line 453471
    move-object v4, v5

    .line 453472
    new-instance v5, LX/4Wu;

    invoke-direct {v5}, LX/4Wu;-><init>()V

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 453473
    iput-object v3, v5, LX/4Wu;->d:Ljava/lang/String;

    .line 453474
    move-object v3, v5

    .line 453475
    invoke-virtual {v3}, LX/4Wu;->a()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v3

    .line 453476
    iput-object v3, v4, LX/23u;->M:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 453477
    move-object v3, v4

    .line 453478
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 453479
    iput-object v2, v5, LX/2dc;->h:Ljava/lang/String;

    .line 453480
    move-object v2, v5

    .line 453481
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 453482
    iput-object v2, v4, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 453483
    move-object v2, v4

    .line 453484
    invoke-virtual {v2}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 453485
    iput-object v2, v3, LX/23u;->d:LX/0Px;

    .line 453486
    move-object v2, v3

    .line 453487
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    .line 453488
    iput-object v0, v2, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 453489
    move-object v0, v2

    .line 453490
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 453491
    iput-wide v2, v0, LX/23u;->v:J

    .line 453492
    move-object v0, v0

    .line 453493
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 453494
    sget-object v1, LX/2A7;->k:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453495
    if-eq v3, v5, :cond_2

    const/4 v1, 0x1

    :goto_0
    const-string v4, "Passing a cursor without a summary text gql column?"

    invoke-static {v1, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453496
    :try_start_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 453497
    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 453498
    :cond_0
    :goto_1
    move-object v1, v2

    .line 453499
    iput-object v1, v0, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 453500
    move-object v0, v0

    .line 453501
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 453502
    sget-object v1, LX/2A7;->l:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 453503
    if-eq v3, v5, :cond_5

    const/4 v1, 0x1

    :goto_2
    const-string v4, "Passing a cursor without a short summary text gql column?"

    invoke-static {v1, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 453504
    :try_start_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 453505
    if-eqz v1, :cond_1

    array-length v3, v1

    if-nez v3, :cond_6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 453506
    :cond_1
    :goto_3
    move-object v1, v2

    .line 453507
    iput-object v1, v0, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 453508
    move-object v0, v0

    .line 453509
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 453510
    const/4 v1, 0x1

    .line 453511
    invoke-static {v0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v2

    .line 453512
    iput-boolean v1, v2, LX/0x2;->q:Z

    .line 453513
    return-object v0

    .line 453514
    :cond_2
    :try_start_2
    const/4 v1, 0x0

    goto :goto_0

    .line 453515
    :cond_3
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 453516
    sget-object v3, LX/16Z;->a:LX/16Z;

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, LX/15i;->a(Ljava/nio/ByteBuffer;LX/16a;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_4
    move-object v2, v1

    .line 453517
    goto :goto_1

    .line 453518
    :catch_0
    move-exception v1

    move-object v3, v1

    .line 453519
    const-string v1, "Error reading notification text from cursor."

    .line 453520
    sget-object v4, LX/2A7;->f:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 453521
    if-eq v4, v5, :cond_4

    .line 453522
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " CacheId: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 453523
    :cond_4
    iget-object v4, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v5, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v4, v5, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_4

    .line 453524
    :cond_5
    :try_start_3
    const/4 v1, 0x0

    goto :goto_2

    .line 453525
    :cond_6
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 453526
    sget-object v3, LX/16Z;->a:LX/16Z;

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, LX/15i;->a(Ljava/nio/ByteBuffer;LX/16a;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :goto_5
    move-object v2, v1

    .line 453527
    goto :goto_3

    .line 453528
    :catch_1
    move-exception v1

    move-object v3, v1

    .line 453529
    const-string v1, "Error reading notification short summary text from cursor."

    .line 453530
    sget-object v4, LX/2A7;->f:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 453531
    if-eq v4, v5, :cond_7

    .line 453532
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " CacheId: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 453533
    :cond_7
    iget-object v4, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v5, "GraphQLNotificationsContentProviderHelper_deserialize_error"

    invoke-virtual {v4, v5, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_5
.end method

.method private static m(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;)LX/2nq;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453148
    invoke-direct {p0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->l(Landroid/database/Cursor;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 453149
    if-nez v0, :cond_0

    .line 453150
    const/4 v0, 0x0

    .line 453151
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, LX/2A7;->g:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h(Landroid/database/Cursor;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f(Landroid/database/Cursor;)Z

    move-result v3

    invoke-direct {p0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c(Landroid/database/Cursor;)LX/BAy;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e(Landroid/database/Cursor;)I

    move-result v5

    invoke-static {p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j(Landroid/database/Cursor;)LX/0Px;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;ZLX/BAy;ILX/0Px;)LX/2nq;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final U_()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 453152
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(J)LX/0Px;

    move-result-object v0

    .line 453153
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 453154
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v2, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 453155
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/util/List;Z)V

    .line 453156
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Iterable;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            "Z)I"
        }
    .end annotation

    .prologue
    .line 452883
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 452884
    sget-object v0, LX/2A7;->d:LX/0U1;

    .line 452885
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 452886
    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452887
    sget-object v0, LX/2A7;->b:LX/0U1;

    .line 452888
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 452889
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    move-object v2, v2

    .line 452890
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v0, v0, LX/2A8;->b:Landroid/net/Uri;

    .line 452891
    if-nez p3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p2, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 452892
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v0, v0, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "NO_NOTIFY"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 452893
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v1, v4, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 452894
    :goto_0
    return v0

    .line 452895
    :catch_0
    move-exception v0

    .line 452896
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v2, "GraphQLNotificationsContentProviderHelper_updateSeenState"

    const-string v3, "Exception thrown when attempting to update seen state"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 452897
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453001
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 453002
    const/4 v0, -0x1

    sget-object v2, LX/3Sw;->a:[Ljava/lang/String;

    invoke-static {p0, v0, v2, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v2

    .line 453003
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453004
    sget-object v0, LX/2A7;->b:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 453005
    :cond_0
    :try_start_1
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453006
    :goto_0
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 453007
    :cond_1
    if-eqz v2, :cond_2

    .line 453008
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 453009
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 453010
    :catch_0
    move-exception v0

    .line 453011
    :try_start_3
    const-string v4, "GraphQLNotificationsContentProviderHelper"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 453012
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    .line 453013
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453000
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;)Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;
    .locals 8
    .param p1    # Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const v6, 0x350001

    .line 452963
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 452964
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 452965
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 452966
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 452967
    iget-object v3, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "connection_controller"

    :goto_0
    invoke-interface {v3, v6, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 452968
    iget-boolean v0, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    move v0, v0

    .line 452969
    if-eqz v0, :cond_6

    .line 452970
    iget v0, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    move v0, v0

    .line 452971
    iget v3, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    move v3, v3

    .line 452972
    add-int/2addr v0, v3

    :goto_1
    sget-object v3, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v4

    invoke-static {p0, v0, v3, v4, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v3

    .line 452973
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v4, 0x2

    invoke-interface {v0, v6, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 452974
    if-eqz v3, :cond_3

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452975
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v3, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;Z)LX/2nq;

    move-result-object v0

    .line 452976
    if-nez v0, :cond_1

    .line 452977
    invoke-static {p0, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;)LX/2nq;

    move-result-object v0

    .line 452978
    if-eqz v0, :cond_1

    .line 452979
    iget-object v4, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v4, v0}, LX/2AG;->b(Ljava/lang/Object;)V

    .line 452980
    :cond_1
    if-eqz v0, :cond_2

    .line 452981
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 452982
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 452983
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 452984
    :cond_3
    if-eqz v3, :cond_4

    .line 452985
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 452986
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2AG;->a(Ljava/util/Collection;)V

    .line 452987
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 452988
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_8

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 452989
    :goto_3
    new-instance v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    new-instance v1, Lcom/facebook/notifications/model/NotificationStories;

    invoke-direct {v1, v2}, Lcom/facebook/notifications/model/NotificationStories;-><init>(LX/0Px;)V

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v3

    iget-object v6, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->g:LX/1qv;

    invoke-virtual {v6}, LX/1qv;->a()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;-><init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;J)V

    return-object v0

    .line 452990
    :cond_5
    const-string v0, "not_connection_controller"

    goto/16 :goto_0

    .line 452991
    :cond_6
    iget v0, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    move v0, v0

    .line 452992
    goto :goto_1

    .line 452993
    :catch_0
    move-exception v0

    .line 452994
    :try_start_1
    iget-object v4, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v5, "GraphQLNotificationsContentProviderHelper_fetch_error_new"

    const-string v6, "Error loading notifications"

    invoke-virtual {v4, v5, v6, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452995
    if-eqz v3, :cond_4

    .line 452996
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 452997
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_7

    .line 452998
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 452999
    :cond_8
    sget-object v5, LX/0ta;->NO_DATA:LX/0ta;

    goto :goto_3
.end method

.method public final a(LX/0o5;)V
    .locals 1

    .prologue
    .line 452961
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, p1}, LX/2AF;->a(LX/0o5;)V

    .line 452962
    return-void
.end method

.method public final a(Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;Z)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 452921
    iget-object v0, p1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    if-nez v0, :cond_0

    .line 452922
    :goto_0
    return-void

    .line 452923
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 452924
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 452925
    iget-object v0, p1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v0}, LX/3T7;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 452926
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 452927
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 452928
    :cond_1
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 452929
    :cond_2
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v7

    .line 452930
    :goto_2
    iget-object v1, p1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 452931
    iget-object v0, p1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v0}, LX/3T7;->c()LX/0Px;

    move-result-object v0

    .line 452932
    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    .line 452933
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a()LX/2nq;

    move-result-object v6

    .line 452934
    if-eqz v6, :cond_7

    .line 452935
    invoke-interface {v6}, LX/2nq;->k()Z

    move-result v0

    .line 452936
    invoke-interface {v6}, LX/2nq;->l()I

    move-result v2

    .line 452937
    invoke-interface {v6}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 452938
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 452939
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 452940
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_4
    move v1, v0

    .line 452941
    :goto_5
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->o:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 452942
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->o:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 452943
    :goto_6
    invoke-interface {v6}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    move v5, v0

    move v3, v1

    .line 452944
    :goto_7
    invoke-interface {v6}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {v6}, LX/2nq;->ia_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-interface {v6}, LX/2nq;->n()LX/BAy;

    move-result-object v4

    invoke-interface {v6}, LX/2nq;->j()LX/0Px;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;ZLX/BAy;ILX/0Px;)LX/2nq;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_4
    move v0, v8

    .line 452945
    goto/16 :goto_2

    .line 452946
    :cond_5
    invoke-interface {v6}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-interface {v6}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    .line 452947
    invoke-interface {v6}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v7

    goto :goto_4

    :cond_6
    move v0, v8

    goto :goto_4

    .line 452948
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 452949
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 452950
    :cond_8
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 452951
    :goto_8
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 452952
    invoke-virtual {p0, v9, v7}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/util/List;Z)V

    .line 452953
    :cond_9
    :goto_9
    if-eqz v7, :cond_a

    .line 452954
    iget-wide v0, p1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->b:J

    invoke-static {p0, v10, v0, v1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a$redex0(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;JZ)V

    .line 452955
    :cond_a
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->g:LX/1qv;

    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v2

    .line 452956
    iget-object v1, v0, LX/1qv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 452957
    sget-object v4, LX/0hM;->b:LX/0Tn;

    invoke-interface {v1, v4, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 452958
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 452959
    goto/16 :goto_0

    :cond_b
    move v7, v8

    .line 452960
    goto :goto_8

    :cond_c
    move v0, v2

    goto/16 :goto_6

    :cond_d
    move v1, v0

    goto/16 :goto_5

    :cond_e
    move v5, v2

    move v3, v0

    goto/16 :goto_7

    :cond_f
    move v7, v0

    goto :goto_9
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 452914
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0, p1, p2}, LX/2AD;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 452915
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v1, 0x0

    .line 452916
    iput-boolean v1, v0, LX/2AF;->b:Z

    .line 452917
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, p1, p2}, LX/2AD;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 452918
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v1, 0x1

    .line 452919
    iput-boolean v1, v0, LX/2AF;->b:Z

    .line 452920
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452910
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 452911
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v2, v0}, LX/2AG;->c(Ljava/lang/Object;)V

    .line 452912
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v2, v0}, LX/2AG;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 452913
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 452906
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452907
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->l:LX/0TD;

    new-instance v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper$2;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;Z)V

    const v2, 0x18ae1bca

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 452908
    :goto_0
    return-void

    .line 452909
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Ljava/util/List;Z)V

    goto :goto_0
.end method

.method public final a(JJ)Z
    .locals 3

    .prologue
    .line 452905
    invoke-static {p0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;J)J

    move-result-wide v0

    cmp-long v0, p3, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z
    .locals 3

    .prologue
    .line 452899
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v1, 0x0

    .line 452900
    iput-boolean v1, v0, LX/2AF;->b:Z

    .line 452901
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, p1, p2}, LX/2AD;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    move-result v0

    .line 452902
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v2, 0x1

    .line 452903
    iput-boolean v2, v1, LX/2AF;->b:Z

    .line 452904
    return v0
.end method

.method public final b(J)I
    .locals 1

    .prologue
    .line 452898
    invoke-virtual {p0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(J)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 452877
    if-nez p1, :cond_0

    move-object v0, v1

    .line 452878
    :goto_0
    return-object v0

    .line 452879
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 452880
    if-nez v0, :cond_1

    .line 452881
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 452882
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 453014
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v7

    .line 453015
    :cond_0
    :goto_0
    return-object v0

    .line 453016
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 453017
    if-eqz v8, :cond_2

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_4

    .line 453018
    :cond_2
    if-eqz v8, :cond_3

    .line 453019
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v7

    goto :goto_0

    .line 453020
    :cond_4
    :try_start_2
    invoke-direct {p0, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k(Landroid/database/Cursor;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 453021
    if-eqz v0, :cond_6

    .line 453022
    sget-object v1, LX/2A7;->g:LX/0U1;

    invoke-virtual {v1, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 453023
    if-nez p2, :cond_a

    .line 453024
    const/4 v2, 0x0

    .line 453025
    :cond_5
    :goto_1
    move-object v6, v2

    .line 453026
    if-eqz v6, :cond_7

    .line 453027
    iget-object v9, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-interface {v6}, LX/2nq;->k()Z

    move-result v3

    invoke-interface {v6}, LX/2nq;->n()LX/BAy;

    move-result-object v4

    invoke-interface {v6}, LX/2nq;->l()I

    move-result v5

    invoke-interface {v6}, LX/2nq;->j()LX/0Px;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;ZLX/BAy;ILX/0Px;)LX/2nq;

    move-result-object v1

    invoke-virtual {v9, v1}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453028
    :cond_6
    :goto_2
    if-eqz v8, :cond_0

    .line 453029
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 453030
    :cond_7
    :try_start_3
    iget-object v9, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h(Landroid/database/Cursor;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-direct {p0, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f(Landroid/database/Cursor;)Z

    move-result v3

    invoke-direct {p0, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c(Landroid/database/Cursor;)LX/BAy;

    move-result-object v4

    invoke-direct {p0, v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e(Landroid/database/Cursor;)I

    move-result v5

    invoke-static {v8}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j(Landroid/database/Cursor;)LX/0Px;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;ZLX/BAy;ILX/0Px;)LX/2nq;

    move-result-object v1

    invoke-virtual {v9, v1}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 453031
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 453032
    :goto_3
    :try_start_4
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v3, "GraphQLNotificationsContentProviderHelper_cursor_error"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 453033
    if-eqz v1, :cond_8

    .line 453034
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object v0, v7

    goto/16 :goto_0

    .line 453035
    :catchall_0
    move-exception v0

    move-object v8, v7

    :goto_4
    if-eqz v8, :cond_9

    .line 453036
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    .line 453037
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v8, v1

    goto :goto_4

    .line 453038
    :catch_1
    move-exception v0

    move-object v1, v7

    goto :goto_3

    .line 453039
    :cond_a
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v2, p2}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2nq;

    .line 453040
    if-nez v2, :cond_5

    .line 453041
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v2, p2}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2nq;

    goto/16 :goto_1
.end method

.method public final b(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;)Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 453042
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 453043
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 453044
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 453045
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 453046
    if-eqz v4, :cond_3

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453047
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v4, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;Z)LX/2nq;

    move-result-object v0

    .line 453048
    if-nez v0, :cond_1

    .line 453049
    invoke-static {p0, v4}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->m(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/database/Cursor;)LX/2nq;

    move-result-object v0

    .line 453050
    if-eqz v0, :cond_1

    .line 453051
    iget-object v5, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v5, v0}, LX/2AG;->b(Ljava/lang/Object;)V

    .line 453052
    :cond_1
    if-eqz v0, :cond_2

    .line 453053
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 453054
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 453055
    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 453056
    :cond_3
    if-eqz v4, :cond_4

    .line 453057
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 453058
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2AG;->a(Ljava/util/Collection;)V

    .line 453059
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 453060
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 453061
    :goto_1
    new-instance v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    new-instance v1, Lcom/facebook/notifications/model/NotificationStories;

    invoke-direct {v1, v3}, Lcom/facebook/notifications/model/NotificationStories;-><init>(LX/0Px;)V

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v3

    iget-object v6, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->g:LX/1qv;

    invoke-virtual {v6}, LX/1qv;->a()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;-><init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;JZ)V

    return-object v0

    .line 453062
    :catch_0
    move-exception v0

    .line 453063
    :try_start_1
    iget-object v5, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v6, "GraphQLNotificationsContentProviderHelper_unseen_fetch_error"

    const-string v7, "Error fetching unseen notifs"

    invoke-virtual {v5, v6, v7, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453064
    if-eqz v4, :cond_4

    .line 453065
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 453066
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_5

    .line 453067
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 453068
    :cond_6
    sget-object v5, LX/0ta;->NO_DATA:LX/0ta;

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 453069
    invoke-virtual {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->U_()V

    .line 453070
    return-void
.end method

.method public final b(LX/0o5;)V
    .locals 1

    .prologue
    .line 453071
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, p1}, LX/2AF;->b(LX/0o5;)V

    .line 453072
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z
    .locals 1

    .prologue
    .line 453073
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0, p1, p2}, LX/2AD;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    move-result v0

    return v0
.end method

.method public final c(J)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453074
    const/16 v0, 0x1e

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(JI)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453075
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 453076
    const/4 v0, 0x0

    .line 453077
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/2A7;->d:LX/0U1;

    .line 453078
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 453079
    aput-object v3, v1, v2

    invoke-static {p0, v1, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 453080
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 453081
    sget-object v0, LX/2A7;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 453082
    :cond_0
    if-eqz v1, :cond_1

    .line 453083
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 453084
    :cond_1
    return-object v0

    .line 453085
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 453086
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    .line 453087
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 453088
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 453089
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0}, LX/2AG;->d()V

    .line 453090
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0}, LX/2AG;->d()V

    .line 453091
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 453092
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v1, LX/2A8;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 453093
    :goto_0
    return-void

    .line 453094
    :catch_0
    move-exception v0

    .line 453095
    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v2, "GraphQLNotificationsContentProviderHelper_clearUserData"

    const-string v3, "Exception thrown when attempting to delete from db"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final d(J)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 453096
    const/4 v1, 0x0

    .line 453097
    new-array v0, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/2A7;->g:LX/0U1;

    .line 453098
    iget-object v5, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v5

    .line 453099
    aput-object v3, v0, v2

    invoke-static {p0, v4, v0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v2

    .line 453100
    if-eqz v2, :cond_1

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 453101
    :try_start_1
    sget-object v0, LX/2A7;->g:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 453102
    :goto_0
    if-eqz v2, :cond_0

    .line 453103
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 453104
    :cond_0
    return-object v0

    .line 453105
    :catch_0
    move-exception v0

    .line 453106
    :try_start_2
    const-string v3, "GraphQLNotificationsContentProviderHelper"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 453107
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 453108
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public final d()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453109
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0}, LX/2AG;->a()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 453110
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 453111
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 453112
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v1, v1, LX/2A8;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2A7;->b:LX/0U1;

    .line 453113
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453114
    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2A7;->c:LX/0U1;

    .line 453115
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453116
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LX/2A7;->d:LX/0U1;

    .line 453117
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 453118
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 453119
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453120
    sget-object v0, LX/2A7;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 453121
    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 453122
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_0

    .line 453123
    :cond_1
    if-eqz v1, :cond_2

    .line 453124
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 453125
    :cond_2
    :goto_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 453126
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 453127
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v3, "GraphQLNotificationsContentProviderHelper_unseen_ids_fetch_error"

    const-string v4, "Error fetching unseen notif Ids"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453128
    if-eqz v1, :cond_2

    .line 453129
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 453130
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_3

    .line 453131
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 453132
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_2

    .line 453133
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 453134
    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0}, LX/2AG;->c()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 453135
    const/4 v1, 0x0

    .line 453136
    new-array v2, v6, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v3, LX/2A7;->i:LX/0U1;

    .line 453137
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 453138
    aput-object v3, v2, v0

    iget-object v0, p0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 453139
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 453140
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {p0, v6, v2, v4, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;I[Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v2

    .line 453141
    if-eqz v2, :cond_2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 453142
    sget-object v0, LX/2A7;->i:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 453143
    :goto_0
    if-eqz v2, :cond_0

    .line 453144
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 453145
    :cond_0
    return-object v0

    .line 453146
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    .line 453147
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
