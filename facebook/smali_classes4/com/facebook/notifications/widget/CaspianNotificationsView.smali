.class public Lcom/facebook/notifications/widget/CaspianNotificationsView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final o:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/3Cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/2it;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:LX/1aX;

.field private final q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

.field private final r:Lcom/facebook/widget/text/BetterTextView;

.field private final s:I

.field private final t:Landroid/view/View$OnTouchListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 531062
    const-class v0, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    const-string v1, "notifications_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->o:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 531052
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531053
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 531063
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531064
    const-class v0, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-static {v0, p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 531065
    const v0, 0x7f030250

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 531066
    const v0, 0x7f0d08e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    iput-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    .line 531067
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lcom/facebook/notifications/widget/CaspianNotificationTextView;->setLineSpacing(FF)V

    .line 531068
    const v0, 0x7f0d08e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 531069
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0a74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->s:I

    .line 531070
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/4 v1, 0x1

    .line 531071
    iput v1, v0, LX/1Uo;->d:I

    .line 531072
    move-object v0, v0

    .line 531073
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 531074
    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    .line 531075
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 531076
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 531077
    const v1, 0x7f0b0a6e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 531078
    const v2, 0x7f0b0a89

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 531079
    const v3, 0x7f0a00f2

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 531080
    const v3, 0x7f0b0a8c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 531081
    const v3, 0x7f0b0a8b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 531082
    invoke-virtual {p0, v1, v2, v1, v2}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setPadding(IIII)V

    .line 531083
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOverlayGravity(I)V

    .line 531084
    new-instance v0, LX/3Cx;

    invoke-direct {v0}, LX/3Cx;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->t:Landroid/view/View$OnTouchListener;

    .line 531085
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 531086
    if-eqz p1, :cond_0

    .line 531087
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/notifications/widget/CaspianNotificationsView;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    .line 531088
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 531089
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 531090
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 531091
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 531092
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 531093
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->j:LX/11S;

    sget-object v1, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/notifications/widget/CaspianNotificationsView;LX/11S;LX/0Or;LX/03V;LX/3Cm;LX/2it;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/widget/CaspianNotificationsView;",
            "LX/11S;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/3Cm;",
            "LX/2it;",
            ")V"
        }
    .end annotation

    .prologue
    .line 531094
    iput-object p1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->j:LX/11S;

    iput-object p2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->k:LX/0Or;

    iput-object p3, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->l:LX/03V;

    iput-object p4, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->m:LX/3Cm;

    iput-object p5, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->n:LX/2it;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-static {v5}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    const/16 v2, 0x509

    invoke-static {v5, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v5}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v4

    check-cast v4, LX/3Cm;

    invoke-static {v5}, LX/2it;->a(LX/0QB;)LX/2it;

    move-result-object v5

    check-cast v5, LX/2it;

    invoke-static/range {v0 .. v5}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Lcom/facebook/notifications/widget/CaspianNotificationsView;LX/11S;LX/0Or;LX/03V;LX/3Cm;LX/2it;)V

    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;Landroid/text/style/ClickableSpan;Landroid/text/style/ClickableSpan;Landroid/text/SpannableString;)V
    .locals 6
    .param p5    # Landroid/text/style/ClickableSpan;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/text/style/ClickableSpan;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/text/SpannableString;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x11

    .line 531095
    new-instance v0, LX/3DI;

    invoke-direct {v0}, LX/3DI;-><init>()V

    .line 531096
    invoke-direct {p0, p2, p3}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 531097
    if-eqz p7, :cond_0

    .line 531098
    invoke-virtual {v0}, LX/3DI;->length()I

    move-result v1

    .line 531099
    iget-object v2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 531100
    const/4 v2, 0x0

    invoke-virtual {v0, p5, v2, v1, v4}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 531101
    invoke-virtual {v0, p7}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 531102
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, LX/3DI;->length()I

    move-result v2

    invoke-virtual {v0, p6, v1, v2, v4}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 531103
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 531104
    invoke-direct {p0, p1, p4}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 531105
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 531054
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 531055
    if-nez v0, :cond_0

    .line 531056
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 531057
    :goto_0
    return-void

    .line 531058
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setTimestampIconFromDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 531059
    :catch_0
    move-exception v0

    .line 531060
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->l:LX/03V;

    const-string v2, "notifications_view_notifications_url"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "notif_cache_id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 531061
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v5, v5, v5, v5}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setContentDescription(LX/0am;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 531106
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531107
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08117e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 531108
    :goto_0
    const-string v1, "%s %s %s"

    iget-object v2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    invoke-virtual {v2}, Lcom/facebook/notifications/widget/CaspianNotificationTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 531109
    :goto_1
    return-void

    .line 531110
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08117f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 531111
    :cond_1
    const-string v0, "%s %s"

    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    invoke-virtual {v1}, Lcom/facebook/notifications/widget/CaspianNotificationTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private setTimestampIconFromDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 531008
    iget v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->s:I

    iget v2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->s:I

    invoke-virtual {p1, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 531009
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 531010
    iget-boolean v2, v1, Lcom/facebook/widget/text/BetterTextView;->f:Z

    move v2, v2

    .line 531011
    iget-object v3, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v2, :cond_0

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {v3, v1, v0, p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 531012
    return-void

    :cond_0
    move-object v1, p1

    .line 531013
    goto :goto_0

    :cond_1
    move-object p1, v0

    goto :goto_1
.end method

.method private setTitleView(Landroid/text/Spannable;)V
    .locals 2

    .prologue
    .line 531014
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/notifications/widget/CaspianNotificationTextView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 531015
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 531016
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setBackgroundResource(I)V

    .line 531017
    invoke-virtual {p0, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 531018
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->m:LX/3Cm;

    sget-object v1, LX/3DG;->JEWEL:LX/3DG;

    iget-object v2, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->q:Lcom/facebook/notifications/widget/CaspianNotificationTextView;

    invoke-virtual {v0, p1, v1, v2}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v0

    .line 531019
    invoke-direct {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setTitleView(Landroid/text/Spannable;)V

    .line 531020
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 531021
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e05ee

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 531022
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 531023
    invoke-direct {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setTimestampIconFromDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 531024
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setContentDescription(LX/0am;)V

    .line 531025
    return-void
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/text/Spannable;JLjava/lang/String;Landroid/text/style/ClickableSpan;Landroid/text/style/ClickableSpan;Landroid/text/SpannableString;)V
    .locals 8
    .param p9    # Landroid/text/style/ClickableSpan;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Landroid/text/style/ClickableSpan;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Landroid/text/SpannableString;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    .line 531026
    if-eqz p1, :cond_0

    const v0, 0x7f0a00d5

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setBackgroundResource(I)V

    .line 531027
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_1

    const v0, 0x7f0e05ee

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 531028
    invoke-direct {p0, p5}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setTitleView(Landroid/text/Spannable;)V

    move-object v0, p0

    move-object v1, p4

    move-wide v2, p6

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    .line 531029
    invoke-direct/range {v0 .. v7}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(Ljava/lang/String;JLjava/lang/String;Landroid/text/style/ClickableSpan;Landroid/text/style/ClickableSpan;Landroid/text/SpannableString;)V

    .line 531030
    invoke-virtual {p0, p2, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 531031
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setContentDescription(LX/0am;)V

    .line 531032
    return-void

    .line 531033
    :cond_0
    const v0, 0x7f0a0481

    goto :goto_0

    .line 531034
    :cond_1
    const v0, 0x7f0e05ef

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2a71c17c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 531035
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onAttachedToWindow()V

    .line 531036
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 531037
    const/16 v1, 0x2d

    const v2, 0x1259c6b7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2ae35f4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 531038
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 531039
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 531040
    const/16 v1, 0x2d

    const v2, 0x4bee74ad    # 3.1254874E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 531041
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onFinishTemporaryDetach()V

    .line 531042
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 531043
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 531044
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onStartTemporaryDetach()V

    .line 531045
    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 531046
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1e87b9aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 531047
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->n:LX/2it;

    invoke-virtual {v1}, LX/2it;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531048
    iget-object v1, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->t:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 531049
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x428a377f

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 531050
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 531051
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/widget/CaspianNotificationsView;->p:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
