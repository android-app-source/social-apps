.class public Lcom/facebook/notifications/widget/RevealAnimationOverlayView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 531179
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 531180
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    .line 531181
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    .line 531182
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    .line 531183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->e:Z

    .line 531184
    new-instance v0, LX/3Cy;

    invoke-direct {v0, p0}, LX/3Cy;-><init>(Lcom/facebook/notifications/widget/RevealAnimationOverlayView;)V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a:Landroid/graphics/Paint;

    .line 531185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 531186
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531187
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    .line 531188
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    .line 531189
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    .line 531190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->e:Z

    .line 531191
    new-instance v0, LX/3Cy;

    invoke-direct {v0, p0}, LX/3Cy;-><init>(Lcom/facebook/notifications/widget/RevealAnimationOverlayView;)V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a:Landroid/graphics/Paint;

    .line 531192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 531193
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 531194
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    .line 531195
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    .line 531196
    iput v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    .line 531197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->e:Z

    .line 531198
    new-instance v0, LX/3Cy;

    invoke-direct {v0, p0}, LX/3Cy;-><init>(Lcom/facebook/notifications/widget/RevealAnimationOverlayView;)V

    iput-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a:Landroid/graphics/Paint;

    .line 531199
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 531200
    iput p1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    .line 531201
    iput p2, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    .line 531202
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 531203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_0

    .line 531204
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 531205
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->e:Z

    if-eqz v0, :cond_1

    .line 531206
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531207
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 531208
    return-void

    .line 531209
    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->e:Z

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 1

    .prologue
    .line 531210
    iget-object v0, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 531211
    return-void
.end method

.method public setPct(F)V
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 531212
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->getWidth()I

    move-result v0

    .line 531213
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->getHeight()I

    move-result v1

    .line 531214
    iget v2, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    if-eq v2, v3, :cond_0

    .line 531215
    iget v2, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    .line 531216
    iget v1, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->d:I

    .line 531217
    iget v3, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    iget v4, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->c:I

    sub-int/2addr v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    .line 531218
    :goto_0
    const v3, 0x3f866666    # 1.05f

    mul-float/2addr v0, v3

    .line 531219
    iget-object v3, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 531220
    iget-object v3, p0, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->b:Landroid/graphics/Path;

    int-to-float v2, v2

    int-to-float v1, v1

    mul-float/2addr v0, p1

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v0, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 531221
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->invalidate()V

    .line 531222
    return-void

    .line 531223
    :cond_0
    div-int/lit8 v2, v0, 0x2

    .line 531224
    div-int/lit8 v1, v1, 0x2

    .line 531225
    int-to-float v0, v2

    goto :goto_0
.end method
