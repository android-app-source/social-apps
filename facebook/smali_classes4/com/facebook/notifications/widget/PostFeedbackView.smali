.class public Lcom/facebook/notifications/widget/PostFeedbackView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 531242
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/notifications/widget/PostFeedbackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 531239
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531240
    invoke-direct {p0}, Lcom/facebook/notifications/widget/PostFeedbackView;->a()V

    .line 531241
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 531229
    const v0, 0x7f030ff1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 531230
    const v0, 0x7f0d2669

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/widget/PostFeedbackView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/notifications/widget/PostFeedbackView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 531231
    return-void
.end method


# virtual methods
.method public setText(Landroid/text/SpannableString;)V
    .locals 2

    .prologue
    .line 531234
    iget-object v0, p0, Lcom/facebook/notifications/widget/PostFeedbackView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 531235
    iget-object v0, p0, Lcom/facebook/notifications/widget/PostFeedbackView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 531236
    sget-object v1, LX/63B;->b:LX/63B;

    move-object v1, v1

    .line 531237
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 531238
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 531232
    iget-object v0, p0, Lcom/facebook/notifications/widget/PostFeedbackView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 531233
    return-void
.end method
