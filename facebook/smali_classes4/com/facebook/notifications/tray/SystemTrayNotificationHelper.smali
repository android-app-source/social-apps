.class public Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile A:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:LX/03V;

.field public final e:LX/3Q3;

.field public final f:LX/1HI;

.field private final g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final h:LX/2Yg;

.field private final i:LX/3Cm;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/2c4;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/3Q7;

.field public final n:LX/1rj;

.field private final o:LX/3Q9;

.field private final p:LX/0ad;

.field public final q:LX/1rU;

.field private final r:LX/1rp;

.field public final s:LX/0xX;

.field public final t:LX/3Q4;

.field public final u:LX/0Sh;

.field public final v:Ljava/util/concurrent/Executor;

.field public final w:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final x:LX/3QA;

.field public final y:LX/0TF;

.field private final z:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 562896
    const-class v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a:Ljava/lang/String;

    .line 562897
    const-class v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    const-string v1, "notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/3Q3;LX/1HI;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/2Yg;LX/3Cm;LX/0Or;LX/2c4;LX/0Or;LX/3Q7;LX/1ri;LX/3Q9;LX/0ad;LX/1rU;LX/1rp;LX/0xX;LX/3Q4;LX/0Sh;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3QA;LX/1b4;LX/1rk;)V
    .locals 3
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p20    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/3Q3;",
            "LX/1HI;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/2Yg;",
            "LX/3Cm;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/notifications/tray/SystemTrayDisplayManager;",
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;",
            "LX/3Q7;",
            "LX/1ri;",
            "LX/3Q9;",
            "LX/0ad;",
            "LX/1rU;",
            "LX/1rp;",
            "LX/0xX;",
            "LX/3Q4;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/3QA;",
            "LX/1b4;",
            "LX/1rk;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562925
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 562926
    iput-object p1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    .line 562927
    iput-object p2, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->d:LX/03V;

    .line 562928
    iput-object p3, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->e:LX/3Q3;

    .line 562929
    iput-object p4, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->f:LX/1HI;

    .line 562930
    iput-object p5, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 562931
    iput-object p6, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->h:LX/2Yg;

    .line 562932
    iput-object p7, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    .line 562933
    iput-object p8, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->j:LX/0Or;

    .line 562934
    iput-object p9, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    .line 562935
    iput-object p10, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->l:LX/0Or;

    .line 562936
    iput-object p11, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->m:LX/3Q7;

    .line 562937
    invoke-virtual {p12}, LX/1ri;->a()LX/1rj;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->n:LX/1rj;

    .line 562938
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->o:LX/3Q9;

    .line 562939
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->p:LX/0ad;

    .line 562940
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    .line 562941
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->r:LX/1rp;

    .line 562942
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->s:LX/0xX;

    .line 562943
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->t:LX/3Q4;

    .line 562944
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->u:LX/0Sh;

    .line 562945
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    .line 562946
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 562947
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->x:LX/3QA;

    .line 562948
    invoke-virtual/range {p23 .. p23}, LX/1b4;->e()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->y:LX/0TF;

    .line 562949
    return-void

    .line 562950
    :cond_0
    new-instance v1, LX/3QC;

    move-object/from16 v0, p24

    invoke-direct {v1, p0, v0}, LX/3QC;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/1rk;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/notifications/model/SystemTrayNotification;)J
    .locals 4

    .prologue
    .line 562921
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    const-wide/32 v2, 0x3ff3d380

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    const-wide/32 v2, 0x5e0c5180

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 562922
    iget-wide v0, p0, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 562923
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;Lcom/facebook/notifications/model/SystemTrayNotification;)Landroid/content/Intent;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562908
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, LX/3Cm;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v0

    .line 562909
    if-nez v0, :cond_0

    .line 562910
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    .line 562911
    iget-object v1, v0, LX/2c4;->e:LX/0hy;

    invoke-interface {v1, p2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 562912
    :cond_0
    if-nez v0, :cond_3

    .line 562913
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    .line 562914
    const/4 v1, 0x0

    .line 562915
    if-eqz p3, :cond_1

    iget-object p0, p3, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    if-eqz p0, :cond_1

    .line 562916
    iget-object v1, v0, LX/2c4;->f:LX/17Y;

    iget-object p0, v0, LX/2c4;->a:Landroid/content/Context;

    sget-object p1, LX/0ax;->do:Ljava/lang/String;

    iget-object p2, p3, Lcom/facebook/notifications/model/SystemTrayNotification;->mHref:Ljava/lang/String;

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 562917
    :cond_1
    if-nez v1, :cond_2

    .line 562918
    iget-object v1, v0, LX/2c4;->f:LX/17Y;

    iget-object p0, v0, LX/2c4;->a:Landroid/content/Context;

    sget-object p1, LX/0ax;->dd:Ljava/lang/String;

    invoke-interface {v1, p0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 562919
    :cond_2
    move-object v0, v1

    .line 562920
    :cond_3
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;
    .locals 3

    .prologue
    .line 562898
    sget-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->A:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    if-nez v0, :cond_1

    .line 562899
    const-class v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    monitor-enter v1

    .line 562900
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->A:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562901
    if-eqz v2, :cond_0

    .line 562902
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(LX/0QB;)Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->A:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562903
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562904
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562905
    :cond_1
    sget-object v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->A:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    return-object v0

    .line 562906
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/notifications/model/SystemTrayNotification;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/BAa;I)Ljava/lang/Runnable;
    .locals 9
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 562884
    new-instance v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper$6;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/notifications/model/SystemTrayNotification;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/BAa;I)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0x41e065f

    .line 562885
    if-nez p0, :cond_0

    move-object v0, v1

    .line 562886
    :goto_0
    return-object v0

    .line 562887
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 562888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 562889
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 562890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 562891
    :cond_2
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 562892
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 562893
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-ne v2, v3, :cond_3

    .line 562894
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    .line 562895
    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Landroid/graphics/Bitmap;)LX/3pw;
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 562951
    new-instance v3, LX/3pw;

    invoke-direct {v3}, LX/3pw;-><init>()V

    .line 562952
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    sget-object v2, LX/3DG;->PERMALINK:LX/3DG;

    invoke-virtual {v0, p1, v2}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;

    move-result-object v4

    .line 562953
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 562954
    if-eqz v2, :cond_0

    .line 562955
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    .line 562956
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    sget-object v5, LX/3DG;->WEARABLE_DETAIL:LX/3DG;

    invoke-virtual {v0, p1, v5}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;

    move-result-object v0

    .line 562957
    if-eqz v2, :cond_2

    .line 562958
    new-instance v5, Landroid/text/SpannableString;

    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 562959
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 562960
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v6

    invoke-virtual {v5, v2, v1, v6, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 562961
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 562962
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 562963
    const-string v6, "\n\n"

    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562964
    :cond_1
    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562965
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562966
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562967
    iget-object v5, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x1060010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 562968
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v2, v6, v0, v5, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v2

    .line 562969
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 562970
    new-instance v2, LX/2HB;

    iget-object v5, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-direct {v2, v5}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    new-instance v4, LX/3pe;

    invoke-direct {v4}, LX/3pe;-><init>()V

    invoke-virtual {v4, v0}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v0

    .line 562971
    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/3pw;->a(Landroid/app/Notification;)LX/3pw;

    .line 562972
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 562973
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v0

    .line 562974
    :goto_1
    if-eqz v0, :cond_9

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 562975
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 562976
    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v1, v5, :cond_8

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 562977
    invoke-static {v0}, LX/36l;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v6

    .line 562978
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 562979
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 562980
    const-string v7, "\n\n"

    invoke-virtual {v2, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562981
    :cond_4
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 562982
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562983
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v2, v6, v7, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 562984
    const-string v6, ": "

    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562985
    iget-object v6, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->o:LX/3Q9;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    const/4 v7, -0x1

    invoke-virtual {v6, v0, v7}, LX/3Q9;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 562986
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 562987
    :cond_6
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto/16 :goto_0

    .line 562988
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 562989
    :cond_8
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 562990
    new-instance v0, LX/2HB;

    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    const v4, 0x7f081155    # 1.80865E38f

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    invoke-virtual {v1, v2}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v0

    new-instance v1, LX/3pw;

    invoke-direct {v1}, LX/3pw;-><init>()V

    invoke-virtual {v1, v9}, LX/3pw;->a(Z)LX/3pw;

    move-result-object v1

    .line 562991
    invoke-interface {v1, v0}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 562992
    move-object v0, v0

    .line 562993
    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/3pw;->a(Landroid/app/Notification;)LX/3pw;

    .line 562994
    :cond_9
    if-eqz p2, :cond_a

    .line 562995
    iput-object p2, v3, LX/3pw;->e:Landroid/graphics/Bitmap;

    .line 562996
    :cond_a
    return-object v3
.end method

.method private b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 562717
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562718
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->r:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    invoke-static {v0, p1}, LX/3Cu;->b(LX/2kW;Ljava/lang/String;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    .line 562719
    if-nez v0, :cond_0

    .line 562720
    const/4 v0, 0x0

    .line 562721
    :goto_0
    return-object v0

    .line 562722
    :cond_0
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 562723
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;
    .locals 26

    .prologue
    .line 562724
    new-instance v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {p0 .. p0}, LX/3Q3;->a(LX/0QB;)LX/3Q3;

    move-result-object v4

    check-cast v4, LX/3Q3;

    invoke-static/range {p0 .. p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v5

    check-cast v5, LX/1HI;

    invoke-static/range {p0 .. p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {p0 .. p0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v7

    check-cast v7, LX/2Yg;

    invoke-static/range {p0 .. p0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v8

    check-cast v8, LX/3Cm;

    const/16 v9, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v10

    check-cast v10, LX/2c4;

    const/16 v11, 0x2acf

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/3Q7;->a(LX/0QB;)LX/3Q7;

    move-result-object v12

    check-cast v12, LX/3Q7;

    invoke-static/range {p0 .. p0}, LX/1ri;->a(LX/0QB;)LX/1ri;

    move-result-object v13

    check-cast v13, LX/1ri;

    invoke-static/range {p0 .. p0}, LX/3Q9;->a(LX/0QB;)LX/3Q9;

    move-result-object v14

    check-cast v14, LX/3Q9;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v16

    check-cast v16, LX/1rU;

    invoke-static/range {p0 .. p0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v17

    check-cast v17, LX/1rp;

    invoke-static/range {p0 .. p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v18

    check-cast v18, LX/0xX;

    invoke-static/range {p0 .. p0}, LX/3Q4;->a(LX/0QB;)LX/3Q4;

    move-result-object v19

    check-cast v19, LX/3Q4;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v20

    check-cast v20, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v21

    check-cast v21, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v22

    check-cast v22, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/3QA;->a(LX/0QB;)LX/3QA;

    move-result-object v23

    check-cast v23, LX/3QA;

    invoke-static/range {p0 .. p0}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v24

    check-cast v24, LX/1b4;

    invoke-static/range {p0 .. p0}, LX/1rk;->a(LX/0QB;)LX/1rk;

    move-result-object v25

    check-cast v25, LX/1rk;

    invoke-direct/range {v1 .. v25}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;-><init>(Landroid/content/Context;LX/03V;LX/3Q3;LX/1HI;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/2Yg;LX/3Cm;LX/0Or;LX/2c4;LX/0Or;LX/3Q7;LX/1ri;LX/3Q9;LX/0ad;LX/1rU;LX/1rp;LX/0xX;LX/3Q4;LX/0Sh;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/3QA;LX/1b4;LX/1rk;)V

    .line 562725
    return-object v1
.end method

.method public static b(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/notifications/model/SystemTrayNotification;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/BAa;I)V
    .locals 11
    .param p0    # Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 562726
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 562727
    :goto_0
    if-nez v1, :cond_4

    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v2, v3, :cond_4

    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->e()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 562728
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->e()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    .line 562729
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v1

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v1, v3, :cond_1

    .line 562730
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "res"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const v2, 0x7f0218e7

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 562731
    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    .line 562732
    iget-object v2, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->f:LX/1HI;

    sget-object v3, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v8

    .line 562733
    new-instance v1, LX/Drp;

    move-object v2, p0

    move/from16 v3, p7

    move-object/from16 v4, p6

    move v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v7}, LX/Drp;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;ILX/BAa;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v8, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 562734
    :goto_2
    return-void

    .line 562735
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    invoke-virtual {v1, p1}, LX/2c4;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 562736
    :cond_1
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 562737
    :cond_2
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    sget-object v5, LX/8D4;->ACTIVITY:LX/8D4;

    move v2, p3

    move-object/from16 v3, p6

    move-object v4, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 562738
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000f

    const/16 v3, 0xd5

    move/from16 v0, p7

    invoke-interface {v1, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_2

    .line 562739
    :cond_3
    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 562740
    iget-object v2, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->f:LX/1HI;

    sget-object v3, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v10

    .line 562741
    new-instance v1, LX/Drq;

    move-object v2, p0

    move/from16 v3, p7

    move-object/from16 v4, p6

    move-object/from16 v5, p5

    move-object v6, p1

    move v7, p3

    move-object v8, p4

    move-object v9, p2

    invoke-direct/range {v1 .. v9}, LX/Drq;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;ILX/BAa;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Lcom/facebook/graphql/model/GraphQLStory;ILandroid/content/Intent;Lcom/facebook/notifications/model/SystemTrayNotification;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v10, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    :cond_4
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public static c(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)LX/Drn;
    .locals 6
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 562742
    new-instance v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper$3;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper$3;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)V

    .line 562743
    new-instance v1, LX/Drn;

    invoke-direct {v1, p0, v0}, LX/Drn;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Ljava/lang/Runnable;)V

    return-object v1
.end method


# virtual methods
.method public final b(Lcom/facebook/notifications/model/SystemTrayNotification;IILandroid/content/Intent;)V
    .locals 10
    .param p4    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 562744
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v7

    .line 562745
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000f

    invoke-interface {v0, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 562746
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000f

    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "connection_controller"

    :goto_0
    invoke-interface {v1, v2, v7, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 562747
    sget-boolean v0, LX/3Cc;->a:Z

    move v0, v0

    .line 562748
    if-eqz v0, :cond_1

    .line 562749
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->h:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, LX/3B2;->DROPPED_BY_SEEN_STATE:LX/3B2;

    invoke-virtual {v0, v1, v2}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562750
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000f

    const/16 v2, 0xcd

    invoke-interface {v0, v1, v7, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 562751
    :goto_1
    return-void

    .line 562752
    :cond_0
    const-string v0, "not_connection_controller"

    goto :goto_0

    .line 562753
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->C()J

    move-result-wide v2

    iget-wide v4, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mServerUtcSecs:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 562754
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->h:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, LX/3B2;->DROPPED_BY_STALENESS:LX/3B2;

    invoke-virtual {v0, v1, v2}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562755
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000f

    const/16 v2, 0xce

    invoke-interface {v0, v1, v7, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_1

    .line 562756
    :cond_2
    const/4 v3, 0x0

    .line 562757
    const/4 v0, 0x0

    .line 562758
    if-nez p4, :cond_20

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 562759
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 562760
    const/4 v2, 0x0

    .line 562761
    invoke-virtual {p0, p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Lcom/facebook/notifications/model/SystemTrayNotification;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->s:LX/0xX;

    sget-object v4, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    invoke-virtual {v1, v4}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 562762
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->t:LX/3Q4;

    invoke-virtual {v1, v0}, LX/3Q4;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 562763
    const/16 v1, 0xd0

    .line 562764
    if-eqz v5, :cond_3

    .line 562765
    const/16 v1, 0xcf

    .line 562766
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 562767
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    .line 562768
    :cond_3
    iget-object v4, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x35000f

    move-object v6, v5

    move-object v5, v2

    move v2, v7

    .line 562769
    :goto_2
    invoke-interface {v4, v3, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 562770
    if-eqz v5, :cond_8

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 562771
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->h:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, LX/3B2;->DROPPED_BY_SEEN_STATE:LX/3B2;

    invoke-virtual {v0, v1, v2}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562772
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000f

    const/16 v2, 0xd1

    invoke-interface {v0, v1, v7, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto/16 :goto_1

    .line 562773
    :cond_4
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 562774
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->r:LX/1rp;

    invoke-virtual {v1}, LX/1rp;->a()LX/2kW;

    move-result-object v1

    invoke-static {v1, v0}, LX/3Cu;->b(LX/2kW;Ljava/lang/String;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v4

    .line 562775
    const/16 v1, 0xd0

    .line 562776
    if-eqz v4, :cond_5

    .line 562777
    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 562778
    if-eqz v3, :cond_5

    .line 562779
    const/16 v1, 0xcf

    .line 562780
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 562781
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    .line 562782
    :cond_5
    iget-object v5, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x35000f

    move-object v6, v3

    move v3, v4

    move-object v4, v5

    move-object v5, v2

    move v2, v7

    .line 562783
    goto :goto_2

    .line 562784
    :cond_6
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 562785
    invoke-direct {p0, v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 562786
    iget-object v3, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000f

    if-eqz v5, :cond_7

    const/16 v1, 0xcf

    move-object v6, v5

    move-object v5, v4

    move-object v4, v3

    move v3, v2

    move v2, v7

    goto :goto_2

    :cond_7
    const/16 v1, 0xd0

    move-object v6, v5

    move-object v5, v4

    move-object v4, v3

    move v3, v2

    move v2, v7

    goto :goto_2

    .line 562787
    :cond_8
    invoke-virtual {p0, p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Lcom/facebook/notifications/model/SystemTrayNotification;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v1, LX/89g;->VIDEOHOME_NOTIFICATION_CACHE:LX/89g;

    .line 562788
    :goto_3
    new-instance v3, LX/89k;

    invoke-direct {v3}, LX/89k;-><init>()V

    if-eqz v6, :cond_b

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    .line 562789
    :goto_4
    iput-object v2, v3, LX/89k;->c:Ljava/lang/String;

    .line 562790
    move-object v2, v3

    .line 562791
    iput-object v0, v2, LX/89k;->b:Ljava/lang/String;

    .line 562792
    move-object v2, v2

    .line 562793
    invoke-virtual {v2, v6}, LX/89k;->e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/89k;->f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/89k;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/89k;->d(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/89k;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/89k;->c(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    .line 562794
    iput-object v1, v2, LX/89k;->a:LX/89g;

    .line 562795
    move-object v1, v2

    .line 562796
    sget-object v2, LX/21C;->PUSH:LX/21C;

    .line 562797
    iput-object v2, v1, LX/89k;->k:LX/21C;

    .line 562798
    move-object v1, v1

    .line 562799
    invoke-static {v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 562800
    invoke-static {v6}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 562801
    if-eqz v2, :cond_9

    if-eqz v3, :cond_9

    .line 562802
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    .line 562803
    iput-object v2, v1, LX/89k;->f:Ljava/lang/String;

    .line 562804
    move-object v2, v1

    .line 562805
    invoke-static {v3}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v3

    .line 562806
    iput-object v3, v2, LX/89k;->i:LX/21y;

    .line 562807
    :cond_9
    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    invoke-direct {p0, v6, v1, p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;Lcom/facebook/notifications/model/SystemTrayNotification;)Landroid/content/Intent;

    move-result-object p4

    .line 562808
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000f

    const/16 v3, 0xd3

    invoke-interface {v1, v2, v7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    move-object v2, v0

    move-object v1, v6

    move-object v4, p4

    .line 562809
    :goto_5
    if-nez v4, :cond_c

    .line 562810
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->h:LX/2Yg;

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, LX/3B2;->dropped_by_invalid_intent:LX/3B2;

    invoke-virtual {v0, v1, v2}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 562811
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000f

    const/16 v2, 0xd4

    invoke-interface {v0, v1, v7, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto/16 :goto_1

    .line 562812
    :cond_a
    sget-object v1, LX/89g;->NOTIFICATION_CACHE:LX/89g;

    goto :goto_3

    .line 562813
    :cond_b
    const/4 v2, 0x0

    goto :goto_4

    .line 562814
    :cond_c
    if-eqz v1, :cond_d

    .line 562815
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 562816
    const-string v3, "source_system_tray"

    invoke-static {v1, v3, v0}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 562817
    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 562818
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v5

    .line 562819
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->j()LX/0am;

    move-result-object v0

    .line 562820
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 562821
    :goto_6
    iput-object v0, v5, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    .line 562822
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->h()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->h()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    .line 562823
    :goto_7
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;->VIDEO_LIVE:Lcom/facebook/graphql/enums/GraphQLPushNotifObjectType;

    if-ne v0, v3, :cond_1c

    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->s:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 562824
    const-string v0, "target_tab_name"

    sget-object v3, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562825
    :goto_8
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAa;

    iget-object v3, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    iget-object v3, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v6, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageItemInfo;->labelRes:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/BAa;->a(I)LX/BAa;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, LX/BAa;->a(J)LX/BAa;

    move-result-object v0

    .line 562826
    iput-object p1, v0, LX/BAa;->c:Lcom/facebook/notifications/model/SystemTrayNotification;

    .line 562827
    move-object v6, v0

    .line 562828
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->m()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 562829
    invoke-virtual {v6}, LX/BAa;->a()LX/BAa;

    .line 562830
    :cond_e
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->n()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 562831
    invoke-virtual {v6}, LX/BAa;->c()LX/BAa;

    .line 562832
    :cond_f
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->o()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 562833
    invoke-virtual {v6}, LX/BAa;->b()LX/BAa;

    .line 562834
    :cond_10
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->x:LX/3QA;

    invoke-virtual {v0, p1, v6, v4, p2}, LX/3QA;->a(Lcom/facebook/notifications/model/SystemTrayNotification;LX/BAa;Landroid/content/Intent;I)V

    .line 562835
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->q()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 562836
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->B()Ljava/lang/String;

    move-result-object v0

    .line 562837
    iget-object v3, v6, LX/BAa;->k:LX/2HB;

    .line 562838
    invoke-static {v0}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v3, LX/2HB;->n:Ljava/lang/CharSequence;

    .line 562839
    :cond_11
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v3, :cond_13

    .line 562840
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->A()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;

    move-result-object v0

    .line 562841
    if-eqz v0, :cond_12

    .line 562842
    iget-object v3, v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->h:Ljava/lang/String;

    move-object v0, v3

    .line 562843
    invoke-virtual {v6, v0}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    .line 562844
    :cond_12
    new-instance v0, LX/3pe;

    invoke-direct {v0}, LX/3pe;-><init>()V

    iget-object v3, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/BAa;->a(LX/3pc;)LX/BAa;

    .line 562845
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 562846
    :cond_13
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    if-eqz v2, :cond_17

    .line 562847
    if-nez v1, :cond_1f

    .line 562848
    invoke-direct {p0, v2}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 562849
    :goto_9
    const/4 v1, 0x0

    .line 562850
    if-eqz v0, :cond_14

    if-nez p1, :cond_21

    .line 562851
    :cond_14
    :goto_a
    move-object v1, v1

    .line 562852
    if-eqz v1, :cond_15

    .line 562853
    invoke-virtual {v6, v1}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    .line 562854
    :cond_15
    if-eqz v0, :cond_16

    .line 562855
    iget-object v1, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    sget-object v3, LX/3DG;->SYSTEM_TRAY:LX/3DG;

    invoke-virtual {v1, v0, v3}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 562856
    iget-object v3, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->p:LX/0ad;

    sget-short v8, LX/15r;->D:S

    const/4 v9, 0x0

    invoke-interface {v3, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 562857
    iget-object v3, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->i:LX/3Cm;

    sget-object v8, LX/3DG;->SYSTEM_TRAY:LX/3DG;

    invoke-virtual {v3, v0, v8}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;

    move-result-object v3

    .line 562858
    invoke-virtual {v6, v3}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    .line 562859
    :goto_b
    invoke-virtual {v6, v1}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    :cond_16
    move-object v1, v0

    .line 562860
    :cond_17
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->m:LX/3Q7;

    invoke-virtual {v0, p1}, LX/3Q7;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 562861
    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->m:LX/3Q7;

    invoke-static {p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)J

    move-result-wide v8

    invoke-virtual {v0, v5, p1, v8, v9}, LX/3Q7;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Lcom/facebook/notifications/model/SystemTrayNotification;J)V

    .line 562862
    const/4 v0, 0x1

    .line 562863
    iput-boolean v0, v6, LX/BAa;->i:Z

    .line 562864
    :cond_18
    if-eqz v2, :cond_22

    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->q:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->g()Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    :goto_c
    move v0, v0

    .line 562865
    if-eqz v0, :cond_19

    .line 562866
    const-string v0, "show_next_notification"

    const/4 v3, 0x1

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 562867
    const-string v0, "parent_control_title_bar"

    const/4 v3, 0x1

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 562868
    const-string v0, "push_notification_id"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562869
    :cond_19
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->w()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 562870
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->w()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    iget-object v9, p1, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/notifications/model/SystemTrayNotification;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/BAa;I)Ljava/lang/Runnable;

    move-result-object v0

    .line 562871
    invoke-static {v8}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 562872
    iget-object v2, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->f:LX/1HI;

    sget-object v3, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 562873
    new-instance v2, LX/Dro;

    invoke-direct {v2, p0, v6, v9, v0}, LX/Dro;-><init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/BAa;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 562874
    goto/16 :goto_1

    .line 562875
    :cond_1a
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 562876
    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 562877
    :cond_1c
    const-string v0, "target_tab_name"

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_8

    .line 562878
    :cond_1d
    invoke-virtual {v6, v1}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    goto/16 :goto_b

    :cond_1e
    move-object v0, p0

    move-object v2, p1

    move v3, p2

    .line 562879
    invoke-static/range {v0 .. v7}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->b(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/notifications/model/SystemTrayNotification;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/BAa;I)V

    goto/16 :goto_1

    :cond_1f
    move-object v0, v1

    goto/16 :goto_9

    :cond_20
    move-object v2, v0

    move-object v1, v3

    move-object v4, p4

    goto/16 :goto_5

    .line 562880
    :cond_21
    sget-object v3, LX/Drr;->a:[I

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v8

    aget v3, v3, v8

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_a

    .line 562881
    :pswitch_0
    invoke-static {v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 562882
    invoke-static {v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    :cond_22
    const/4 v0, 0x0

    goto/16 :goto_c

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/facebook/notifications/model/SystemTrayNotification;)Z
    .locals 2

    .prologue
    .line 562883
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->s:LX/0xX;

    sget-object v1, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
