.class public abstract Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# instance fields
.field public final synthetic d:LX/2Yg;


# direct methods
.method public constructor <init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 526883
    iput-object p1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;->d:LX/2Yg;

    .line 526884
    invoke-virtual {p2}, LX/3B2;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 526885
    iget-object v0, p3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    move-object v0, v0

    .line 526886
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 526887
    iget-object v0, p3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->h:Ljava/lang/String;

    move-object v0, v0

    .line 526888
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 526889
    iput-object p4, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 526890
    const-string v0, "id"

    .line 526891
    iget-wide v4, p3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    move-wide v2, v4

    .line 526892
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526893
    const-string v0, "l"

    .line 526894
    iget-object v1, p3, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    move-object v1, v1

    .line 526895
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526896
    invoke-virtual {p0, p3}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 526897
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
.end method
