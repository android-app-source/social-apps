.class public final Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/notifications/constants/NotificationType;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:I

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:J

.field public u:I

.field public v:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 526640
    new-instance v0, LX/3B1;

    invoke-direct {v0}, LX/3B1;-><init>()V

    sput-object v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526642
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 526643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526644
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a:Ljava/lang/String;

    .line 526645
    const-class v0, Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/constants/NotificationType;

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    .line 526646
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    .line 526647
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    .line 526648
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    .line 526649
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    .line 526650
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    .line 526651
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->h:Ljava/lang/String;

    .line 526652
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 526653
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 526654
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 526655
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->l:Z

    .line 526656
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->m:Z

    .line 526657
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->n:Z

    .line 526658
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->o:Z

    .line 526659
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->p:I

    .line 526660
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->q:Ljava/lang/String;

    .line 526661
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    .line 526662
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    .line 526663
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->t:J

    .line 526664
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 526665
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    .line 526666
    return-void

    :cond_0
    move v0, v2

    .line 526667
    goto :goto_0

    :cond_1
    move v0, v2

    .line 526668
    goto :goto_1

    :cond_2
    move v0, v2

    .line 526669
    goto :goto_2

    :cond_3
    move v0, v2

    .line 526670
    goto :goto_3

    :cond_4
    move v0, v2

    .line 526671
    goto :goto_4

    :cond_5
    move v1, v2

    .line 526672
    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
    .locals 1
    .param p1    # Lcom/facebook/graphql/enums/GraphQLStorySeenState;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 526673
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    .line 526674
    return-object p0

    .line 526675
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/notifications/constants/NotificationType;
    .locals 1

    .prologue
    .line 526676
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->UNKNOWN:Lcom/facebook/notifications/constants/NotificationType;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 526677
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0}, Lcom/facebook/notifications/constants/NotificationType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 526678
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 526679
    const-class v0, LX/2Yg;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "notif_type"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "client_tag"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "unread_count"

    iget v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "alert_id"

    iget-wide v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "graphql_id"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "object_id"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "is_unread"

    iget-boolean v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "system_tray_id"

    iget v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->p:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "system_tray_tag"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "push_source"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "push_id"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "seen_state"

    iget-object v2, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 526680
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526681
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 526682
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526683
    iget v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 526684
    iget-wide v4, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 526685
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526686
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526687
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526688
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526689
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526690
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526691
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526692
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526693
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->n:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526694
    iget-boolean v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->o:Z

    if-eqz v0, :cond_5

    :goto_5
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 526695
    iget v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 526696
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526697
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526698
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526699
    iget-wide v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->t:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 526700
    iget v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 526701
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 526702
    return-void

    :cond_0
    move v0, v2

    .line 526703
    goto :goto_0

    :cond_1
    move v0, v2

    .line 526704
    goto :goto_1

    :cond_2
    move v0, v2

    .line 526705
    goto :goto_2

    :cond_3
    move v0, v2

    .line 526706
    goto :goto_3

    :cond_4
    move v0, v2

    .line 526707
    goto :goto_4

    :cond_5
    move v1, v2

    .line 526708
    goto :goto_5
.end method
