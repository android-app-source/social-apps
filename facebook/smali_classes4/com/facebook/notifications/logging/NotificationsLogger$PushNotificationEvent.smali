.class public final Lcom/facebook/notifications/logging/NotificationsLogger$PushNotificationEvent;
.super Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;
.source ""


# instance fields
.field public final synthetic c:LX/2Yg;


# direct methods
.method public constructor <init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 1

    .prologue
    .line 526880
    iput-object p1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$PushNotificationEvent;->c:LX/2Yg;

    .line 526881
    const-string v0, "push_notifications_tray"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 526882
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 6

    .prologue
    .line 526845
    const-string v0, "ct"

    invoke-virtual {p1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526846
    const-string v0, "ci"

    .line 526847
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    move-object v1, v1

    .line 526848
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526849
    const-string v0, "n"

    .line 526850
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a:Ljava/lang/String;

    move-object v1, v1

    .line 526851
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526852
    const-string v0, "u"

    .line 526853
    iget v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    move v1, v1

    .line 526854
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526855
    const-string v0, "ib"

    .line 526856
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->l:Z

    move v1, v1

    .line 526857
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526858
    const-string v0, "hpp"

    .line 526859
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->m:Z

    move v1, v1

    .line 526860
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526861
    const-string v0, "it"

    .line 526862
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->n:Z

    move v1, v1

    .line 526863
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526864
    const-string v0, "cia"

    .line 526865
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->o:Z

    move v1, v1

    .line 526866
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526867
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    move-object v0, v0

    .line 526868
    if-eqz v0, :cond_0

    .line 526869
    const-string v0, "push_source"

    .line 526870
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    move-object v1, v1

    .line 526871
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526872
    const-string v0, "push_id"

    .line 526873
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    move-object v1, v1

    .line 526874
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526875
    iget-object v0, p0, Lcom/facebook/notifications/logging/NotificationsLogger$PushNotificationEvent;->c:LX/2Yg;

    iget-object v0, v0, LX/2Yg;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 526876
    iget-wide v4, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->t:J

    move-wide v2, v4

    .line 526877
    sub-long/2addr v0, v2

    .line 526878
    const-string v2, "time_to_tray_ms"

    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 526879
    :cond_0
    return-void
.end method
