.class public Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0rS;

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Z

.field public final j:Z

.field public k:I

.field public final l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 534693
    new-instance v0, LX/3CV;

    invoke-direct {v0}, LX/3CV;-><init>()V

    sput-object v0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3DL;)V
    .locals 1

    .prologue
    .line 534636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534637
    iget-object v0, p1, LX/3DL;->a:LX/0rS;

    move-object v0, v0

    .line 534638
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    .line 534639
    iget v0, p1, LX/3DL;->b:I

    move v0, v0

    .line 534640
    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    .line 534641
    iget v0, p1, LX/3DL;->c:I

    move v0, v0

    .line 534642
    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    .line 534643
    iget-object v0, p1, LX/3DL;->d:Ljava/lang/String;

    move-object v0, v0

    .line 534644
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    .line 534645
    iget-object v0, p1, LX/3DL;->e:Ljava/lang/String;

    move-object v0, v0

    .line 534646
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    .line 534647
    iget-object v0, p1, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 534648
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 534649
    iget-object v0, p1, LX/3DL;->g:Ljava/lang/String;

    move-object v0, v0

    .line 534650
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    .line 534651
    iget-object v0, p1, LX/3DL;->h:Ljava/util/List;

    move-object v0, v0

    .line 534652
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    .line 534653
    iget-boolean v0, p1, LX/3DL;->i:Z

    move v0, v0

    .line 534654
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    .line 534655
    iget-boolean v0, p1, LX/3DL;->j:Z

    move v0, v0

    .line 534656
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    .line 534657
    iget v0, p1, LX/3DL;->k:I

    move v0, v0

    .line 534658
    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    .line 534659
    iget-object v0, p1, LX/3DL;->l:Ljava/lang/String;

    move-object v0, v0

    .line 534660
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    .line 534661
    iget-boolean v0, p1, LX/3DL;->m:Z

    move v0, v0

    .line 534662
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    .line 534663
    iget-object v0, p1, LX/3DL;->n:Ljava/lang/String;

    move-object v0, v0

    .line 534664
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    .line 534665
    iget-boolean v0, p1, LX/3DL;->o:Z

    move v0, v0

    .line 534666
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    .line 534667
    iget-object v0, p1, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 534668
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 534669
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 534670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534671
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    .line 534672
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    .line 534673
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    .line 534674
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    .line 534675
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    .line 534676
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 534677
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    .line 534678
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    .line 534679
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 534680
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    .line 534681
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    .line 534682
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    .line 534683
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    .line 534684
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    .line 534685
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    .line 534686
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    .line 534687
    const-class v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 534688
    return-void

    :cond_0
    move v0, v2

    .line 534689
    goto :goto_0

    :cond_1
    move v0, v2

    .line 534690
    goto :goto_1

    :cond_2
    move v0, v2

    .line 534691
    goto :goto_2

    :cond_3
    move v1, v2

    .line 534692
    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 534694
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 534594
    instance-of v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    if-eqz v1, :cond_0

    .line 534595
    check-cast p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 534596
    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    .line 534597
    iget-boolean v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    move v2, v2

    .line 534598
    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    .line 534599
    iget-boolean v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    move v2, v2

    .line 534600
    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    .line 534601
    iget v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    move v2, v2

    .line 534602
    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    .line 534603
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    move-object v2, v2

    .line 534604
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    .line 534605
    iget-boolean v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    move v2, v2

    .line 534606
    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    .line 534607
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    move-object v2, v2

    .line 534608
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    .line 534609
    iget-boolean v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    move v2, v2

    .line 534610
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 534611
    :cond_0
    return v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 534612
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 534613
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 534614
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 534615
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534616
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534617
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534618
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534619
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534620
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 534621
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534622
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 534623
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534624
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534625
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534626
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534627
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 534628
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 534629
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 534630
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 534631
    return-void

    :cond_0
    move v0, v2

    .line 534632
    goto :goto_0

    :cond_1
    move v0, v2

    .line 534633
    goto :goto_1

    :cond_2
    move v0, v2

    .line 534634
    goto :goto_2

    :cond_3
    move v1, v2

    .line 534635
    goto :goto_3
.end method
