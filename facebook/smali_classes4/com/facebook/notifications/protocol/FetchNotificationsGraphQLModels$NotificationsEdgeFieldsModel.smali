.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/2nq;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7659fd2a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:I

.field private j:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479057
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 479058
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 479059
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479060
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 479061
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 479062
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 479063
    return-void
.end method

.method public static a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;
    .locals 4

    .prologue
    .line 479064
    if-nez p0, :cond_0

    .line 479065
    const/4 p0, 0x0

    .line 479066
    :goto_0
    return-object p0

    .line 479067
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    if-eqz v0, :cond_1

    .line 479068
    check-cast p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    goto :goto_0

    .line 479069
    :cond_1
    new-instance v1, LX/BB8;

    invoke-direct {v1}, LX/BB8;-><init>()V

    .line 479070
    invoke-interface {p0}, LX/2nq;->hZ_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->a:Ljava/lang/String;

    .line 479071
    invoke-interface {p0}, LX/2nq;->ia_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->b:Ljava/lang/String;

    .line 479072
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 479073
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p0}, LX/2nq;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 479074
    invoke-interface {p0}, LX/2nq;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 479075
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 479076
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->c:LX/0Px;

    .line 479077
    invoke-interface {p0}, LX/2nq;->k()Z

    move-result v0

    iput-boolean v0, v1, LX/BB8;->d:Z

    .line 479078
    invoke-interface {p0}, LX/2nq;->l()I

    move-result v0

    iput v0, v1, LX/BB8;->e:I

    .line 479079
    invoke-interface {p0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 479080
    invoke-interface {p0}, LX/2nq;->n()LX/BAy;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 479081
    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    iput-object v0, v1, LX/BB8;->h:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 479082
    invoke-virtual {v1}, LX/BB8;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 479125
    iput-boolean p1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->h:Z

    .line 479126
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 479127
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 479128
    if-eqz v0, :cond_0

    .line 479129
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 479130
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 479083
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479084
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 479085
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->ia_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 479086
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->c(Ljava/util/List;)I

    move-result v2

    .line 479087
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 479088
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 479089
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 479090
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 479091
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 479092
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 479093
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 479094
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 479095
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->i:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 479096
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 479097
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 479098
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 479099
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479100
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 479101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 479102
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479103
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 479104
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 479105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 479106
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 479107
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 479108
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 479109
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 479110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 479111
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 479112
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 479113
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 479114
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 479115
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 479116
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->l:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 479117
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 479118
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 479119
    new-instance v0, LX/BB9;

    invoke-direct {v0, p1}, LX/BB9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479120
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 479121
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 479122
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->h:Z

    .line 479123
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->i:I

    .line 479124
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 479048
    const-string v0, "local_is_rich_notif_collapsed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479049
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 479050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 479051
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 479052
    :goto_0
    return-void

    .line 479053
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 479025
    const-string v0, "local_is_rich_notif_collapsed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479026
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->a(Z)V

    .line 479027
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 479054
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;-><init>()V

    .line 479055
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 479056
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 479028
    const v0, 0x6909d955

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 479029
    const v0, -0x2163595b

    return v0
.end method

.method public final hZ_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479030
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->e:Ljava/lang/String;

    .line 479031
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final ia_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479032
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->f:Ljava/lang/String;

    .line 479033
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 479034
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->g:Ljava/util/List;

    .line 479035
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 479036
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 479037
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->h:Z

    return v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 479038
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 479039
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->i:I

    return v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479040
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 479041
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final synthetic n()LX/BAy;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479042
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479043
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479044
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 479045
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 479046
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->l:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->l:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 479047
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->l:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    return-object v0
.end method
