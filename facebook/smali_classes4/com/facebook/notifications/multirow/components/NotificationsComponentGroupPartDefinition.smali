.class public Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/2nq;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1rU;

.field private final d:LX/0xW;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1rU;LX/0xW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/components/NotificationsComponentPartDefinition;",
            ">;",
            "LX/1rU;",
            "LX/0xW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530309
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 530310
    iput-object p1, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->a:LX/0Ot;

    .line 530311
    iput-object p2, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->b:LX/0Ot;

    .line 530312
    iput-object p3, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->c:LX/1rU;

    .line 530313
    iput-object p4, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->d:LX/0xW;

    .line 530314
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;
    .locals 7

    .prologue
    .line 530318
    const-class v1, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;

    monitor-enter v1

    .line 530319
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 530320
    sput-object v2, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 530321
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530322
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 530323
    new-instance v5, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;

    const/16 v3, 0x2aea

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x2ad7

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v3

    check-cast v3, LX/1rU;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v4

    check-cast v4, LX/0xW;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/1rU;LX/0xW;)V

    .line 530324
    move-object v0, v5

    .line 530325
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 530326
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530327
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 530328
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 530329
    check-cast p2, LX/2nq;

    check-cast p3, LX/1Pr;

    .line 530330
    move-object v0, p2

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 530331
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 530332
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 530333
    new-instance v0, LX/3D5;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3D6;

    .line 530334
    iget-object v1, v0, LX/3D6;->a:LX/03R;

    move-object v1, v1

    .line 530335
    sget-object v3, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530336
    invoke-interface {p2}, LX/2nq;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 530337
    :goto_0
    iput-object v1, v0, LX/3D6;->a:LX/03R;

    .line 530338
    :cond_0
    iget-object v1, v0, LX/3D6;->a:LX/03R;

    move-object v0, v1

    .line 530339
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-nez v0, :cond_1

    .line 530340
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 530341
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 530342
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 530343
    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 530315
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->d:LX/0xW;

    .line 530316
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 530317
    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;->c:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
