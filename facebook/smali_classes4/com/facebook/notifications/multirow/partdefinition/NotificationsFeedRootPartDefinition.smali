.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/1PW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/components/NotificationsComponentGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketFooterComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketDividerComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530274
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 530275
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, LX/2nq;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/2nq;

    invoke-virtual {v0, v1, p2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/3Cg;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/3Ch;

    invoke-virtual {v0, v1, p4}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/3Ci;

    invoke-virtual {v0, v1, p5}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, LX/3Cj;

    invoke-virtual {v0, v1, p6}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;->a:LX/1T5;

    .line 530276
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;
    .locals 10

    .prologue
    .line 530277
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;

    monitor-enter v1

    .line 530278
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 530279
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 530280
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530281
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 530282
    new-instance v3, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;

    const/16 v4, 0xe56

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xe54

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe53

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ae6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2ae7

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ae5

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 530283
    move-object v0, v3

    .line 530284
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 530285
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530286
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 530287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 530288
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 530289
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 530290
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
