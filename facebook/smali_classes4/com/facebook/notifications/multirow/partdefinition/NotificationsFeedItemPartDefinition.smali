.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/2nq;",
        "LX/3D4;",
        "TE;",
        "LX/3Cv;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/1Qx;

.field public final c:LX/0xW;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/3Cl;

.field public final g:LX/2jO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 530344
    new-instance v0, LX/3Ck;

    invoke-direct {v0}, LX/3Ck;-><init>()V

    sput-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Qx;LX/0xW;LX/0Ot;LX/0Ot;LX/3Cl;LX/2jO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Qx;",
            "LX/0xW;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsReactionRootPartDefinition;",
            ">;",
            "Lcom/facebook/notifications/widget/NotificationsRenderer;",
            "LX/2jO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530345
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 530346
    iput-object p1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->b:LX/1Qx;

    .line 530347
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->c:LX/0xW;

    .line 530348
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->d:LX/0Ot;

    .line 530349
    iput-object p4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->e:LX/0Ot;

    .line 530350
    iput-object p5, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->f:LX/3Cl;

    .line 530351
    iput-object p6, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->g:LX/2jO;

    .line 530352
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;
    .locals 10

    .prologue
    .line 530353
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;

    monitor-enter v1

    .line 530354
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 530355
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 530356
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530357
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 530358
    new-instance v3, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v4

    check-cast v4, LX/1Qx;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v5

    check-cast v5, LX/0xW;

    const/16 v6, 0xe57

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2aea

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/3Cl;->a(LX/0QB;)LX/3Cl;

    move-result-object v8

    check-cast v8, LX/3Cl;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v9

    check-cast v9, LX/2jO;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;-><init>(LX/1Qx;LX/0xW;LX/0Ot;LX/0Ot;LX/3Cl;LX/2jO;)V

    .line 530359
    move-object v0, v3

    .line 530360
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 530361
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530362
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 530363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3Cv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 530364
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 530365
    check-cast p2, LX/2nq;

    check-cast p3, LX/1Pn;

    .line 530366
    move-object v0, p2

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 530367
    const v1, 0x7f0d2e68

    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 530368
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p3

    .line 530369
    check-cast v0, LX/1Pr;

    new-instance v1, LX/3D5;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-interface {v0, v1, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3D6;

    .line 530370
    iget-object v1, v0, LX/3D6;->a:LX/03R;

    move-object v1, v1

    .line 530371
    sget-object v3, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530372
    invoke-interface {p2}, LX/2nq;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 530373
    :goto_0
    iput-object v1, v0, LX/3D6;->a:LX/03R;

    .line 530374
    :cond_0
    iget-object v1, v0, LX/3D6;->a:LX/03R;

    move-object v0, v1

    .line 530375
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-nez v0, :cond_1

    .line 530376
    invoke-interface {p2}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 530377
    const v2, 0x7f0d2e69

    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    sget-object v3, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->a:LX/1Cz;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->b:LX/1Qx;

    invoke-static {v0, v3, v4, v5}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-interface {p1, v2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 530378
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->g:LX/2jO;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v0

    .line 530379
    new-instance v1, LX/3D4;

    invoke-direct {v1, v0}, LX/3D4;-><init>(LX/3D3;)V

    return-object v1

    .line 530380
    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7c62f9c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 530381
    check-cast p1, LX/2nq;

    check-cast p2, LX/3D4;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/3Cv;

    const/4 v2, 0x0

    .line 530382
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, LX/2nq;->k()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 530383
    :goto_0
    iget-object v4, p4, LX/3Cv;->e:Lcom/facebook/resources/ui/FbFrameLayout;

    move-object v5, v4

    .line 530384
    if-eqz v1, :cond_3

    move v4, v2

    :goto_1
    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 530385
    iget-object v4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->c:LX/0xW;

    invoke-virtual {v4}, LX/0xW;->v()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 530386
    const/4 v5, 0x0

    .line 530387
    if-eqz v1, :cond_4

    invoke-virtual {p4}, LX/3Cv;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0b0a6a

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 530388
    :goto_2
    iget-object p2, p4, LX/3Cv;->a:Landroid/widget/LinearLayout;

    invoke-virtual {p2, v5, v4, v5, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 530389
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->g:LX/2jO;

    .line 530390
    iget-object v4, v1, LX/2jO;->i:LX/DqC;

    move-object v1, v4

    .line 530391
    if-eqz v1, :cond_1

    .line 530392
    iget-object v4, v1, LX/DqC;->a:Ljava/lang/String;

    move-object v4, v4

    .line 530393
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 530394
    iget-object v4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedItemPartDefinition;->f:LX/3Cl;

    check-cast p3, LX/2kk;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3, v5}, LX/2kk;->e_(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, p4, v1, p1, v5}, LX/3Cl;->a(Landroid/view/View;LX/DqC;LX/2nq;I)V

    .line 530395
    sget-object v4, LX/3Cw;->POST_FEEDBACK:LX/3Cw;

    .line 530396
    iget-boolean v5, v1, LX/DqC;->f:Z

    move v5, v5

    .line 530397
    invoke-virtual {p4, v4, v5}, LX/3Cv;->a(LX/3Cw;Z)V

    .line 530398
    iput-boolean v2, v1, LX/DqC;->f:Z

    .line 530399
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x2a7df918

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_2
    move v1, v2

    .line 530400
    goto :goto_0

    .line 530401
    :cond_3
    const/16 v4, 0x8

    goto :goto_1

    :cond_4
    move v4, v5

    .line 530402
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 530403
    check-cast p1, LX/2nq;

    .line 530404
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 530405
    check-cast p4, LX/3Cv;

    .line 530406
    invoke-virtual {p4}, LX/3Cv;->a()V

    .line 530407
    return-void
.end method
