.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/2nq;",
        "LX/3D8;",
        "TE;",
        "Lcom/facebook/notifications/widget/CaspianNotificationsView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:LX/3D0;

.field private final d:LX/3D1;

.field private final e:LX/3Cl;

.field private final f:LX/2jO;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 531244
    const-class v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;

    const-string v1, "notification"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 531245
    new-instance v0, LX/3Cz;

    invoke-direct {v0}, LX/3Cz;-><init>()V

    sput-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/3D0;LX/3D1;LX/3Cl;LX/2jO;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 531246
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 531247
    iput-object p1, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->c:LX/3D0;

    .line 531248
    iput-object p2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->d:LX/3D1;

    .line 531249
    iput-object p3, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->e:LX/3Cl;

    .line 531250
    iput-object p4, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->f:LX/2jO;

    .line 531251
    iput-object p5, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->g:LX/0ad;

    .line 531252
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;
    .locals 9

    .prologue
    .line 531253
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;

    monitor-enter v1

    .line 531254
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 531255
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 531256
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531257
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 531258
    new-instance v3, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;

    const-class v4, LX/3D0;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/3D0;

    invoke-static {v0}, LX/3D1;->a(LX/0QB;)LX/3D1;

    move-result-object v5

    check-cast v5, LX/3D1;

    invoke-static {v0}, LX/3Cl;->a(LX/0QB;)LX/3Cl;

    move-result-object v6

    check-cast v6, LX/3Cl;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v7

    check-cast v7, LX/2jO;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;-><init>(LX/3D0;LX/3D1;LX/3Cl;LX/2jO;LX/0ad;)V

    .line 531259
    move-object v0, v3

    .line 531260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 531261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 531263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/2nq;LX/3D8;LX/1Pq;Lcom/facebook/notifications/widget/CaspianNotificationsView;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nq;",
            "LX/3D8;",
            "TE;",
            "Lcom/facebook/notifications/widget/CaspianNotificationsView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 531264
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    .line 531265
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->e:LX/3Cl;

    iget-object v2, p2, LX/3D8;->e:Ljava/lang/String;

    iget-object v3, p2, LX/3D8;->f:Ljava/lang/String;

    iget-object v4, p2, LX/3D8;->g:Ljava/lang/String;

    check-cast p3, LX/2kk;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, LX/2kk;->e_(Ljava/lang/String;)I

    iget-object v6, p2, LX/3D8;->a:Landroid/view/View$OnClickListener;

    iget-object v7, p2, LX/3D8;->c:Landroid/view/View$OnClickListener;

    move-object v1, p4

    move-object v5, p1

    invoke-virtual/range {v0 .. v7}, LX/3Cl;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2nq;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 531266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531267
    const v0, 0x7f0a00d5

    invoke-virtual {p4, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setBackgroundResource(I)V

    .line 531268
    :goto_0
    iget-object v0, p2, LX/3D8;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 531269
    const/4 v0, 0x1

    move v0, v0

    .line 531270
    if-eqz v0, :cond_0

    .line 531271
    iget-object v0, p2, LX/3D8;->b:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p4, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 531272
    :cond_0
    return-void

    .line 531273
    :cond_1
    const v0, 0x7f0a0481

    invoke-virtual {p4, v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 531274
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 531275
    check-cast p2, LX/2nq;

    check-cast p3, LX/1Pq;

    const/4 v8, 0x0

    .line 531276
    move-object v0, p3

    check-cast v0, LX/1Pr;

    new-instance v1, LX/3D5;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3D6;

    .line 531277
    new-instance v3, LX/3D7;

    invoke-direct {v3, p0, v0, p3, p2}, LX/3D7;-><init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;LX/3D6;LX/1Pq;LX/2nq;)V

    .line 531278
    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 531279
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLIcon;->j()Ljava/lang/String;

    move-result-object v5

    .line 531280
    :goto_0
    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 531281
    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v6

    .line 531282
    invoke-static {v0}, LX/1xl;->e(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v7

    .line 531283
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->g:LX/0ad;

    sget-short v2, LX/0fe;->F:S

    invoke-interface {v0, v2, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531284
    if-eqz v5, :cond_0

    move-object v0, p3

    .line 531285
    check-cast v0, LX/1Pt;

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    sget-object v4, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 531286
    :cond_0
    if-eqz v6, :cond_1

    .line 531287
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {v7}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v2

    .line 531288
    iput-object v2, v0, LX/1bX;->m:LX/1ny;

    .line 531289
    move-object v0, v0

    .line 531290
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    move-object v0, p3

    .line 531291
    check-cast v0, LX/1Pt;

    sget-object v4, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 531292
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->f:LX/2jO;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v4

    .line 531293
    new-instance v0, LX/3D8;

    move-object v1, p3

    check-cast v1, LX/2kk;

    invoke-interface {v1, p2}, LX/2kk;->b(LX/2nq;)Landroid/view/View$OnClickListener;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->c:LX/3D0;

    check-cast p3, LX/2kk;

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p3, v9}, LX/2kk;->e_(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v2, p2, v9}, LX/3D0;->a(LX/2nq;I)LX/3DD;

    move-result-object v2

    invoke-direct/range {v0 .. v8}, LX/3D8;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/view/View$OnClickListener;LX/3D3;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    return-object v0

    .line 531294
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x338c387b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 531295
    check-cast p1, LX/2nq;

    check-cast p2, LX/3D8;

    check-cast p3, LX/1Pq;

    check-cast p4, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;->a(LX/2nq;LX/3D8;LX/1Pq;Lcom/facebook/notifications/widget/CaspianNotificationsView;)V

    const/16 v1, 0x1f

    const v2, -0x7ff9c15b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 531296
    check-cast p1, LX/2nq;

    .line 531297
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 531298
    check-cast p4, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    const/4 v1, 0x0

    .line 531299
    invoke-virtual {p4, v1}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 531300
    const/4 v0, 0x1

    move v0, v0

    .line 531301
    if-eqz v0, :cond_0

    .line 531302
    invoke-virtual {p4, v1}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 531303
    :cond_0
    return-void
.end method
