.class public final enum Lcom/facebook/notifications/constants/NotificationType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/notifications/constants/NotificationType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum APP_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum AUTO_UPDATE_AVAILABLE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum BACKSTAGE_FRIEND_POSTED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum BACKSTAGE_FRIEND_REACTED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum BOOTSTRAP_UPDATED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum CLOSE_FRIEND_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum COMMENT_FAILED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum COMMENT_MENTION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ERROR_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum EVENT_INVITE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum EVENT_PHOTO_CHECK:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FBNS_MOBILE_REQUESTS_COUNT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FBNS_NOTIFICATIONS_READ:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FBNS_NOTIFICATIONS_SEEN:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FBNS_NOTIFICATIONS_SYNC:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FBPAGE_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FRIEND:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum GIFT_RECIPIENT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum GROUP_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum GROUP_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum GROUP_COMMENT_REPLY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum GROUP_POST_MENTION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum HOTP_LOGIN_APPROVALS:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum INTERNAL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ITEM_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ITEM_REPLY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LA_PUSH_AUTHENTICATE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LIKE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LIVE_VIDEO:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LIVE_VIDEO_EXPLICIT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LOGGED_OUT_BADGE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LOGGED_OUT_PUSH:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MENTION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSAGE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_EVENT_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_GROUP_JOIN_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_MONTAGE_DAILY_DIGEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_MONTAGE_FIRST_POST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_MONTAGE_MESSAGE_EXPIRING:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_REACTIONS:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_STALE_PUSH:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MESSENGER_STATUS_CHANGE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MOBILE_ZERO_FREE_FACEBOOK_LAUNCH:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum MSG:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum NEAR_SAVED_PLACE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum NEKO_INSTALL_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum NOW_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ORCA_FRIEND_MSG:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ORCA_MESSAGE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ORCA_THREAD_READ:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum P2P_PAYMENT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PAGE_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PAGE_REVIEW:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PAGE_SHARE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PAGE_TAG:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PAGE_WALL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PHOTO_TAG:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PHOTO_TAGGED_BY_NON_OWNER:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PHOTO_TAG_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PLACE_CHECKIN_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PLACE_FEED_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PLACE_FEED_NEARBY_CANCEL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PLACE_TAGGED_IN_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PLAN_USER_INVITED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum POST_FAILED:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum PUSH_REACHABILITY_CHECK:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum REQUEST_LOCATION_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum REQUEST_LOCKSCREEN_RESET:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum SHARE_WALL_CREATE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum STALE_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum STALE_CONTACT_IMPORT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum STALE_EMAIL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum STALE_NOTIFICATIONS:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum UNKNOWN:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum VOIP:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum VOIP_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum WALL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum WALLFEED_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ZERO:Lcom/facebook/notifications/constants/NotificationType;

.field public static final enum ZP:Lcom/facebook/notifications/constants/NotificationType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 526712
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "APP_REQUEST"

    invoke-direct {v0, v1, v3}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->APP_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526713
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "AUTHENTICATION_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526714
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "AUTO_UPDATE_AVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->AUTO_UPDATE_AVAILABLE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526715
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "BACKSTAGE_FRIEND_POSTED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->BACKSTAGE_FRIEND_POSTED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526716
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "BACKSTAGE_FRIEND_REACTED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->BACKSTAGE_FRIEND_REACTED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526717
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "BOOTSTRAP_UPDATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->BOOTSTRAP_UPDATED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526718
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "CLOSE_FRIEND_ACTIVITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->CLOSE_FRIEND_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526719
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "COMMENT_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526720
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "COMMENT_MENTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_MENTION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526721
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "DEFAULT_PUSH_OF_JEWEL_NOTIF"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    .line 526722
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "DEVICE_REQUEST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526723
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ERROR_CLIENT_NOTIFICATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ERROR_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526724
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "EVENT_INVITE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->EVENT_INVITE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526725
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "EVENT_PHOTO_CHECK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->EVENT_PHOTO_CHECK:Lcom/facebook/notifications/constants/NotificationType;

    .line 526726
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FBPAGE_PRESENCE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBPAGE_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526727
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FBNS_MOBILE_REQUESTS_COUNT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_MOBILE_REQUESTS_COUNT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526728
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FBNS_NOTIFICATIONS_READ"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_READ:Lcom/facebook/notifications/constants/NotificationType;

    .line 526729
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FBNS_NOTIFICATIONS_SEEN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SEEN:Lcom/facebook/notifications/constants/NotificationType;

    .line 526730
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FBNS_NOTIFICATIONS_SYNC"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SYNC:Lcom/facebook/notifications/constants/NotificationType;

    .line 526731
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FRIEND_CONFIRMED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526732
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "FRIEND"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    .line 526733
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "GIFT_RECIPIENT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->GIFT_RECIPIENT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526734
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "GROUP_ACTIVITY"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->GROUP_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526735
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "GROUP_COMMENT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->GROUP_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526736
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "GROUP_COMMENT_REPLY"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->GROUP_COMMENT_REPLY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526737
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "GROUP_POST_MENTION"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->GROUP_POST_MENTION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526738
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "HOTP_LOGIN_APPROVALS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->HOTP_LOGIN_APPROVALS:Lcom/facebook/notifications/constants/NotificationType;

    .line 526739
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "INTERNAL"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->INTERNAL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526740
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ITEM_COMMENT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ITEM_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526741
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ITEM_REPLY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ITEM_REPLY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526742
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LIKE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LIKE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526743
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LIVE_VIDEO"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LIVE_VIDEO:Lcom/facebook/notifications/constants/NotificationType;

    .line 526744
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LIVE_VIDEO_EXPLICIT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LIVE_VIDEO_EXPLICIT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526745
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LOGGED_OUT_BADGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LOGGED_OUT_BADGE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526746
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LOGGED_OUT_PUSH"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LOGGED_OUT_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    .line 526747
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LOGIN_APPROVALS_PUSH_AUTH"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

    .line 526748
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "LA_PUSH_AUTHENTICATE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->LA_PUSH_AUTHENTICATE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526749
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MENTION"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MENTION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526750
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSAGE_REQUEST"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSAGE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526751
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_EVENT_REMINDER"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_EVENT_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    .line 526752
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_GROUP_JOIN_REQUEST"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_GROUP_JOIN_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526753
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_MONTAGE_FIRST_POST"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_FIRST_POST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526754
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_MONTAGE_MESSAGE_EXPIRING"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:Lcom/facebook/notifications/constants/NotificationType;

    .line 526755
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    .line 526756
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_MONTAGE_DAILY_DIGEST"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_DAILY_DIGEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526757
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_REACTIONS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REACTIONS:Lcom/facebook/notifications/constants/NotificationType;

    .line 526758
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_REMINDER"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    .line 526759
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_STALE_PUSH"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STALE_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    .line 526760
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MESSENGER_STATUS_CHANGE"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STATUS_CHANGE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526761
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MOBILE_ZERO_FREE_FACEBOOK_LAUNCH"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MOBILE_ZERO_FREE_FACEBOOK_LAUNCH:Lcom/facebook/notifications/constants/NotificationType;

    .line 526762
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "MSG"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->MSG:Lcom/facebook/notifications/constants/NotificationType;

    .line 526763
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ORCA_FRIEND_MSG"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ORCA_FRIEND_MSG:Lcom/facebook/notifications/constants/NotificationType;

    .line 526764
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ORCA_MESSAGE"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ORCA_MESSAGE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526765
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ORCA_THREAD_READ"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ORCA_THREAD_READ:Lcom/facebook/notifications/constants/NotificationType;

    .line 526766
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PLACE_CHECKIN_NEARBY"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PLACE_CHECKIN_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526767
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "NEAR_SAVED_PLACE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->NEAR_SAVED_PLACE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526768
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "NEKO_INSTALL_REMINDER"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->NEKO_INSTALL_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    .line 526769
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "NOW_UPDATE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->NOW_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526770
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PAGE_CHECKIN"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PAGE_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

    .line 526771
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PAGE_REVIEW"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PAGE_REVIEW:Lcom/facebook/notifications/constants/NotificationType;

    .line 526772
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PAGE_SHARE"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PAGE_SHARE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526773
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PAGE_TAG"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PAGE_TAG:Lcom/facebook/notifications/constants/NotificationType;

    .line 526774
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PAGE_WALL"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PAGE_WALL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526775
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PHOTO_TAG"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG:Lcom/facebook/notifications/constants/NotificationType;

    .line 526776
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PHOTO_TAGGED_BY_NON_OWNER"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAGGED_BY_NON_OWNER:Lcom/facebook/notifications/constants/NotificationType;

    .line 526777
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PHOTO_TAG_REQUEST"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    .line 526778
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PLACE_FEED_NEARBY"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    .line 526779
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PLACE_FEED_NEARBY_CANCEL"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY_CANCEL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526780
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PLACE_TAGGED_IN_CHECKIN"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PLACE_TAGGED_IN_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

    .line 526781
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PLAN_USER_INVITED"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PLAN_USER_INVITED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526782
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "P2P_PAYMENT"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->P2P_PAYMENT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526783
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "POST_FAILED"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->POST_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    .line 526784
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PRE_REG_PUSH"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    .line 526785
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "PUSH_REACHABILITY_CHECK"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->PUSH_REACHABILITY_CHECK:Lcom/facebook/notifications/constants/NotificationType;

    .line 526786
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "REQUEST_LOCATION_UPDATE"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->REQUEST_LOCATION_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526787
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "REQUEST_LOCKSCREEN_RESET"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->REQUEST_LOCKSCREEN_RESET:Lcom/facebook/notifications/constants/NotificationType;

    .line 526788
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "SHARE_WALL_CREATE"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->SHARE_WALL_CREATE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526789
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "STALE_CLIENT_NOTIFICATION"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->STALE_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526790
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "STALE_CONTACT_IMPORT"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->STALE_CONTACT_IMPORT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526791
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "STALE_EMAIL"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->STALE_EMAIL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526792
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "STALE_NOTIFICATIONS"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->STALE_NOTIFICATIONS:Lcom/facebook/notifications/constants/NotificationType;

    .line 526793
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "TOR_STATUS"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    .line 526794
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->UNKNOWN:Lcom/facebook/notifications/constants/NotificationType;

    .line 526795
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "VOIP"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->VOIP:Lcom/facebook/notifications/constants/NotificationType;

    .line 526796
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "VOIP_PRESENCE"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->VOIP_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    .line 526797
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "WAKEUP_MQTT"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    .line 526798
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "WALL"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->WALL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526799
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "WALLFEED_NOTIFICATION"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->WALLFEED_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    .line 526800
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "WEBRTC_VOIP_CALL"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

    .line 526801
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ZERO"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ZERO:Lcom/facebook/notifications/constants/NotificationType;

    .line 526802
    new-instance v0, Lcom/facebook/notifications/constants/NotificationType;

    const-string v1, "ZP"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/notifications/constants/NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->ZP:Lcom/facebook/notifications/constants/NotificationType;

    .line 526803
    const/16 v0, 0x5b

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->APP_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->AUTHENTICATION_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->AUTO_UPDATE_AVAILABLE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->BACKSTAGE_FRIEND_POSTED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->BACKSTAGE_FRIEND_REACTED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->BOOTSTRAP_UPDATED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->CLOSE_FRIEND_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->COMMENT_MENTION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->DEFAULT_PUSH_OF_JEWEL_NOTIF:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ERROR_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->EVENT_INVITE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->EVENT_PHOTO_CHECK:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBPAGE_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_MOBILE_REQUESTS_COUNT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_READ:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SEEN:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SYNC:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FRIEND_CONFIRMED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->GIFT_RECIPIENT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->GROUP_ACTIVITY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->GROUP_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->GROUP_COMMENT_REPLY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->GROUP_POST_MENTION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->HOTP_LOGIN_APPROVALS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->INTERNAL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ITEM_COMMENT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ITEM_REPLY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LIKE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LIVE_VIDEO:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LIVE_VIDEO_EXPLICIT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LOGGED_OUT_BADGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LOGGED_OUT_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LOGIN_APPROVALS_PUSH_AUTH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->LA_PUSH_AUTHENTICATE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MENTION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSAGE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_EVENT_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_GROUP_JOIN_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_FIRST_POST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_EXPIRING:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_MESSAGE_VIEWING_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_MONTAGE_DAILY_DIGEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REACTIONS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STALE_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MESSENGER_STATUS_CHANGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MOBILE_ZERO_FREE_FACEBOOK_LAUNCH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MSG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_FRIEND_MSG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_MESSAGE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ORCA_THREAD_READ:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PLACE_CHECKIN_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->NEAR_SAVED_PLACE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->NEKO_INSTALL_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->NOW_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PAGE_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PAGE_REVIEW:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PAGE_SHARE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PAGE_TAG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PAGE_WALL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAGGED_BY_NON_OWNER:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PHOTO_TAG_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PLACE_FEED_NEARBY_CANCEL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PLACE_TAGGED_IN_CHECKIN:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PLAN_USER_INVITED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->P2P_PAYMENT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->POST_FAILED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PRE_REG_PUSH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->PUSH_REACHABILITY_CHECK:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->REQUEST_LOCATION_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->REQUEST_LOCKSCREEN_RESET:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->SHARE_WALL_CREATE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->STALE_CLIENT_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->STALE_CONTACT_IMPORT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->STALE_EMAIL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->STALE_NOTIFICATIONS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->UNKNOWN:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->VOIP:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->VOIP_PRESENCE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WAKEUP_MQTT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WALL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WALLFEED_NOTIFICATION:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->WEBRTC_VOIP_CALL:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ZERO:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ZP:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->$VALUES:[Lcom/facebook/notifications/constants/NotificationType;

    .line 526804
    new-instance v0, LX/2a6;

    invoke-direct {v0}, LX/2a6;-><init>()V

    sput-object v0, Lcom/facebook/notifications/constants/NotificationType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 526805
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 526806
    invoke-static {}, Lcom/facebook/notifications/constants/NotificationType;->values()[Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 526807
    invoke-virtual {v0, p0}, Lcom/facebook/notifications/constants/NotificationType;->equalsType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 526808
    :goto_1
    return-object v0

    .line 526809
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 526810
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;
    .locals 1

    .prologue
    .line 526811
    const-class v0, Lcom/facebook/notifications/constants/NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/constants/NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/notifications/constants/NotificationType;
    .locals 1

    .prologue
    .line 526812
    sget-object v0, Lcom/facebook/notifications/constants/NotificationType;->$VALUES:[Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/notifications/constants/NotificationType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 526813
    const/4 v0, 0x0

    return v0
.end method

.method public final equalsType(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 526814
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526815
    const/4 v0, 0x0

    .line 526816
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/constants/NotificationType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 526817
    invoke-virtual {p0}, Lcom/facebook/notifications/constants/NotificationType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 526818
    return-void
.end method
