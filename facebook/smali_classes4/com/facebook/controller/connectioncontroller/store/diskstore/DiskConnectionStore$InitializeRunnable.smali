.class public final Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public final a:LX/2jx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jx",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/15j;

.field private final e:LX/03V;

.field private final f:LX/2k1;

.field private final g:LX/2kK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kK",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final h:I


# direct methods
.method public constructor <init>(LX/2jx;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/15j;LX/03V;LX/2k1;LX/2kK;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jx",
            "<TTEdge;>;",
            "LX/2kI",
            "<TTEdge;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/15j;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2k1;",
            "LX/2kK",
            "<",
            "Ljava/lang/Runnable;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 456358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456359
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    .line 456360
    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->b:LX/2kI;

    .line 456361
    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 456362
    iput-object p4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->d:LX/15j;

    .line 456363
    iput-object p5, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->e:LX/03V;

    .line 456364
    iput-object p6, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->f:LX/2k1;

    .line 456365
    iput-object p7, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->g:LX/2kK;

    .line 456366
    iput p8, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->h:I

    .line 456367
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 456368
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->d:LX/15j;

    invoke-virtual {v0}, LX/15j;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456369
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->e:LX/03V;

    const-string v1, "ConnectionController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FlatBuffer corruption detected for session: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    .line 456370
    iget-object v4, v3, LX/2jx;->c:Ljava/lang/String;

    move-object v3, v4

    .line 456371
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 456372
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->f:LX/2k1;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    invoke-virtual {v1}, LX/2jx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2k1;->b(Ljava/lang/String;)V

    .line 456373
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->d:LX/15j;

    invoke-virtual {v0}, LX/15j;->c()V

    .line 456374
    sget-object v2, LX/2kL;->a:LX/2kM;

    .line 456375
    :cond_0
    :goto_0
    invoke-interface {v2}, LX/2kM;->c()I

    move-result v0

    if-nez v0, :cond_4

    .line 456376
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    iget v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->h:I

    const/16 v4, 0x21

    invoke-interface {v0, v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 456377
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    new-instance v1, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$2;

    invoke-direct {v1, p0, v2}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$2;-><init>(Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;LX/2kM;)V

    invoke-static {v0, v1}, LX/2jx;->a$redex0(LX/2jx;Ljava/lang/Runnable;)V

    .line 456378
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    iget v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->h:I

    invoke-static {v0, v1}, LX/2jx;->a$redex0(LX/2jx;I)V

    .line 456379
    :goto_1
    return-void

    .line 456380
    :cond_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->f:LX/2k1;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    .line 456381
    iget-object v3, v1, LX/2jx;->z:LX/2kb;

    move-object v1, v3

    .line 456382
    invoke-interface {v0, v1}, LX/2k1;->a(LX/2kb;)LX/2nf;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 456383
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->g:LX/2kK;

    invoke-virtual {v1, v0, v2}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;)V

    .line 456384
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    invoke-static {v1, v0, v2}, LX/2jx;->a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;

    move-result-object v2

    .line 456385
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 456386
    iget-wide v7, v1, LX/2jx;->e:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_5

    .line 456387
    :cond_2
    :goto_2
    move v1, v5

    .line 456388
    if-eqz v1, :cond_3

    .line 456389
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    new-instance v1, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$1;

    invoke-direct {v1, p0, v2}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$1;-><init>(Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;LX/2nh;)V

    invoke-static {v0, v1}, LX/2jx;->a$redex0(LX/2jx;Ljava/lang/Runnable;)V

    .line 456390
    sget-object v2, LX/2kL;->a:LX/2kM;

    .line 456391
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    const/4 v1, 0x1

    .line 456392
    iput-boolean v1, v0, LX/2jx;->w:Z

    .line 456393
    goto :goto_0

    .line 456394
    :cond_3
    instance-of v1, v2, LX/2ng;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 456395
    check-cast v1, LX/2ng;

    .line 456396
    invoke-virtual {v1}, LX/2ng;->c()I

    move-result v3

    iget-object v4, v1, LX/2ng;->d:LX/2nf;

    invoke-interface {v4}, LX/2nf;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_7

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 456397
    if-eqz v3, :cond_0

    .line 456398
    iget-object v2, v1, LX/2ng;->e:LX/2nn;

    move-object v1, v2

    .line 456399
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    .line 456400
    iget v3, v2, LX/2jx;->h:I

    move v2, v3

    .line 456401
    invoke-virtual {v1, v2}, LX/2nn;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 456402
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    invoke-static {v2, v0, v1}, LX/2jx;->a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;

    move-result-object v2

    goto/16 :goto_0

    .line 456403
    :cond_4
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;->a:LX/2jx;

    new-instance v1, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$3;

    invoke-direct {v1, p0, v2}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable$3;-><init>(Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;LX/2kM;)V

    invoke-static {v0, v1}, LX/2jx;->a$redex0(LX/2jx;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 456404
    :cond_5
    iget-wide v7, v1, LX/2jx;->e:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-nez v7, :cond_6

    move v5, v6

    .line 456405
    goto :goto_2

    .line 456406
    :cond_6
    invoke-virtual {v2}, LX/2nh;->l()J

    move-result-wide v7

    iget-wide v9, v1, LX/2jx;->e:J

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    add-long/2addr v7, v9

    .line 456407
    iget-object v9, v1, LX/2jx;->p:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-gez v7, :cond_2

    move v5, v6

    goto :goto_2

    :cond_7
    const/4 v3, 0x0

    goto :goto_3
.end method
