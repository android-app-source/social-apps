.class public Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;
.super Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0yi;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:LX/32P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 483812
    new-instance v0, LX/2r5;

    invoke-direct {v0}, LX/2r5;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 483813
    invoke-direct {p0, p1}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Landroid/os/Parcel;)V

    .line 483814
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    .line 483815
    invoke-static {}, LX/0yi;->values()[LX/0yi;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 483816
    invoke-virtual {v5}, LX/0yi;->getModeNumber()B

    move-result v6

    if-ne v6, v2, :cond_2

    .line 483817
    iput-object v5, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    .line 483818
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    .line 483819
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    .line 483820
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/32P;->fromString(Ljava/lang/String;)LX/32P;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    .line 483821
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    if-nez v0, :cond_3

    .line 483822
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parcel mode did not match any known token type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483823
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 483824
    :cond_3
    return-void
.end method

.method public constructor <init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;LX/0yi;ZLjava/lang/String;LX/32P;)V
    .locals 0

    .prologue
    .line 483825
    invoke-direct {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;)V

    .line 483826
    iput-object p3, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    .line 483827
    iput-boolean p4, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    .line 483828
    iput-object p5, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    .line 483829
    iput-object p6, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    .line 483830
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 483831
    const-string v0, "fetchZeroTokenRequestParams"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 483832
    instance-of v1, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    if-nez v1, :cond_1

    .line 483833
    :cond_0
    :goto_0
    return v0

    .line 483834
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    .line 483835
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v1, v1

    .line 483836
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 483837
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 483838
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 483839
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 483840
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    .line 483841
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    move-object v2, v2

    .line 483842
    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    .line 483843
    iget-boolean v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    move v2, v2

    .line 483844
    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    .line 483845
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 483846
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    .line 483847
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    move-object v2, v2

    .line 483848
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 483849
    const-class v0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "carrierAndSimMccMnc"

    .line 483850
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 483851
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "networkType"

    .line 483852
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 483853
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "tokenType"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fetchBackupRewriteRules"

    iget-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "tokenRequestReason"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    invoke-virtual {v2}, LX/32P;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 483854
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 483855
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    invoke-virtual {v0}, LX/0yi;->getModeNumber()B

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 483856
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 483857
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 483858
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    invoke-virtual {v0}, LX/32P;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 483859
    return-void

    .line 483860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
