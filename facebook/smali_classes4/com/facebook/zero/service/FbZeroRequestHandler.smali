.class public Lcom/facebook/zero/service/FbZeroRequestHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/33e;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Sh;

.field public final d:LX/0tX;

.field private final e:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Sh;LX/0tX;LX/0Uh;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0tX;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483767
    iput-object p1, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->a:LX/0Ot;

    .line 483768
    iput-object p2, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->b:LX/0Ot;

    .line 483769
    iput-object p3, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->c:LX/0Sh;

    .line 483770
    iput-object p4, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->d:LX/0tX;

    .line 483771
    iput-object p5, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->e:LX/0Uh;

    .line 483772
    return-void
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/zero/sdk/token/ZeroToken;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;",
            ">;)",
            "Lcom/facebook/zero/sdk/token/ZeroToken;"
        }
    .end annotation

    .prologue
    .line 483773
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;

    .line 483774
    invoke-virtual {v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;->j()Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;

    move-result-object v11

    .line 483775
    if-nez v11, :cond_0

    .line 483776
    sget-object v3, Lcom/facebook/zero/sdk/token/ZeroToken;->a:Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 483777
    :goto_0
    return-object v3

    .line 483778
    :cond_0
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 483779
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->m()Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 483780
    invoke-virtual {v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const/4 v8, 0x2

    invoke-virtual {v7, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 483781
    invoke-virtual {v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;->a()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 483782
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->k()Ljava/lang/String;

    move-result-object v16

    .line 483783
    invoke-virtual {v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;->a()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v9, 0x1

    invoke-virtual {v8, v3, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 483784
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->l()LX/2uF;

    move-result-object v13

    .line 483785
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->o()I

    move-result v9

    .line 483786
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->n()Ljava/lang/String;

    move-result-object v14

    .line 483787
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 483788
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->j()LX/0Px;

    move-result-object v15

    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v17

    const/4 v3, 0x0

    move v10, v3

    :goto_1
    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    invoke-virtual {v15, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 483789
    invoke-static {v3}, LX/0yY;->fromString(Ljava/lang/String;)LX/0yY;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483790
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_1

    .line 483791
    :cond_1
    invoke-static {v12}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v10

    .line 483792
    invoke-virtual {v11}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$ZeroTokenModel;->p()Lcom/facebook/graphql/enums/GraphQLZeroTokenUnregisteredReason;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    .line 483793
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 483794
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 483795
    if-eqz v13, :cond_9

    invoke-virtual {v13}, LX/39O;->c()I

    move-result v3

    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v3, v0, :cond_9

    .line 483796
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v0, v3, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    iget v3, v3, LX/1vs;->b:I

    const/16 v18, 0x3

    const v19, -0x601ce3f2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v3, v1, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_2
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    .line 483797
    new-instance v19, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 483798
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_2

    .line 483799
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v0, v3, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    iget v3, v3, LX/1vs;->b:I

    const/16 v18, 0x1

    const v19, 0x52759548

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v3, v1, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_4
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    .line 483800
    new-instance v19, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 483801
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_4

    .line 483802
    :cond_5
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v0, v3, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    iget v3, v3, LX/1vs;->b:I

    const/16 v18, 0x2

    const v19, -0x601ce3f2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v3, v1, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_6
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    .line 483803
    new-instance v19, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 483804
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_6

    .line 483805
    :cond_7
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v13, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/16 v17, 0x3

    const v18, -0x601ce3f2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v13, v3, v0, v1}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_8
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v13

    iget-object v0, v13, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    iget v13, v13, LX/1vs;->b:I

    .line 483806
    new-instance v18, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v13, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v13, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v13}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 483807
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_8

    .line 483808
    :cond_9
    invoke-static {v11}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v11

    .line 483809
    invoke-static {v15}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v13

    .line 483810
    new-instance v3, Lcom/facebook/zero/sdk/token/ZeroToken;

    const/4 v15, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v3 .. v17}, Lcom/facebook/zero/sdk/token/ZeroToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Rf;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;ILjava/lang/String;LX/0P1;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(TPARAMS;",
            "Ljava/lang/String;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "LX/0TF",
            "<TRESU",
            "LT;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483711
    new-instance v1, LX/33f;

    invoke-direct {v1, p0, p4}, LX/33f;-><init>(Lcom/facebook/zero/service/FbZeroRequestHandler;LX/0TF;)V

    .line 483712
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 483713
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 483714
    iget-object v3, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0aG;

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/zero/service/FbZeroRequestHandler;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x4907cdee    # 556254.9f

    move-object v4, p2

    move-object v5, v0

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 483715
    invoke-virtual {v3, p3}, LX/1ML;->updatePriority(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 483716
    move-object v2, v3

    .line 483717
    if-eqz p5, :cond_0

    .line 483718
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->c:LX/0Sh;

    invoke-virtual {v0, v2, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 483719
    new-instance v0, LX/0Td;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, LX/0Td;-><init>(Landroid/os/Handler;)V

    .line 483720
    :goto_0
    new-instance v1, LX/33g;

    invoke-direct {v1, p0}, LX/33g;-><init>(Lcom/facebook/zero/service/FbZeroRequestHandler;)V

    invoke-static {v2, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 483721
    return-object v0

    .line 483722
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 483723
    invoke-static {v2, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "LX/4pL;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/4pL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483759
    new-instance v0, LX/7XK;

    invoke-direct {v0}, LX/7XK;-><init>()V

    move-object v0, v0

    .line 483760
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 483761
    iget-object v1, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 483762
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 483763
    new-instance v2, LX/7Xb;

    invoke-direct {v2, p0}, LX/7Xb;-><init>(Lcom/facebook/zero/service/FbZeroRequestHandler;)V

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 483764
    invoke-static {v1, p1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 483765
    return-object v1
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroIndicatorData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483758
    const-string v2, "fetch_zero_indicator"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483811
    const-string v2, "fetch_zero_interstitial_content"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483757
    const-string v2, "fetch_zero_interstitial_eligibility"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483756
    const-string v2, "fetch_zero_optin_content_request"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/token/ZeroToken;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 483726
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->e:LX/0Uh;

    const/16 v1, 0x680

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 483727
    iget-object v0, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    move-object v0, v0

    .line 483728
    sget-object v1, LX/32P;->GRAPHQL_VERIFICATION:LX/32P;

    if-ne v0, v1, :cond_1

    .line 483729
    :cond_0
    const/4 v0, 0x1

    .line 483730
    new-instance v1, LX/7XO;

    invoke-direct {v1}, LX/7XO;-><init>()V

    const-string v2, "dialtone_enabled"

    .line 483731
    iget-object v3, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    move-object v3, v3

    .line 483732
    invoke-virtual {v3}, LX/0yi;->getModeNumber()B

    move-result v3

    if-ne v3, v0, :cond_4

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "hash"

    .line 483733
    iget-object v0, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    move-object v0, v0

    .line 483734
    sget-object v3, LX/32P;->GRAPHQL_VERIFICATION:LX/32P;

    invoke-virtual {v0, v3}, LX/32P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "needs_backup_rules"

    .line 483735
    iget-boolean v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    move v2, v2

    .line 483736
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    .line 483737
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 483738
    new-instance v1, LX/7XO;

    invoke-direct {v1}, LX/7XO;-><init>()V

    move-object v1, v1

    .line 483739
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 483740
    iget-object v1, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 483741
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 483742
    new-instance v2, LX/7Xa;

    invoke-direct {v2, p0}, LX/7Xa;-><init>(Lcom/facebook/zero/service/FbZeroRequestHandler;)V

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 483743
    invoke-static {v1, p2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 483744
    move-object v0, v1

    .line 483745
    :goto_2
    return-object v0

    .line 483746
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/service/FbZeroRequestHandler;->e:LX/0Uh;

    const/16 v1, 0x2f7

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 483747
    const-string v2, "fetch_zero_token"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 483748
    :cond_2
    const-string v2, "fetch_zero_token_not_bootstrap"

    .line 483749
    iget-object v0, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    move-object v0, v0

    .line 483750
    sget-object v1, LX/32P;->TOKEN_FETCH_FAILED_RETRY:LX/32P;

    if-ne v0, v1, :cond_3

    .line 483751
    const-string v2, "fetch_zero_token"

    .line 483752
    :cond_3
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 483753
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 483754
    :cond_5
    iget-object v0, p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 483755
    goto :goto_1
.end method

.method public final a(Lcom/facebook/zero/sdk/request/ZeroOptinParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptinParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483725
    const-string v2, "zero_optin"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/ZeroOptoutParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/zero/sdk/request/ZeroOptoutResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483724
    const-string v2, "zero_optout"

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;LX/0TF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
