.class public Lcom/facebook/zero/protocol/ZeroTokenHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static p:LX/0Xm;


# instance fields
.field public final b:LX/11H;

.field private final c:LX/33h;

.field private final d:LX/33i;

.field private final e:LX/2Zp;

.field public final f:LX/2cH;

.field public final g:LX/33l;

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/33m;

.field public final j:LX/33n;

.field public final k:LX/33o;

.field public final l:LX/33p;

.field public final m:LX/33q;

.field public final n:LX/33r;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494091
    const-class v0, Lcom/facebook/zero/protocol/ZeroTokenHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/33h;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Zp;LX/33i;LX/2cH;LX/33l;LX/33m;LX/33n;LX/33o;LX/33p;LX/33q;LX/33r;LX/0Or;)V
    .locals 0
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/annotations/DisableZeroTokenBootstrapGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/33h;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2Zp;",
            "LX/33i;",
            "LX/2cH;",
            "LX/33l;",
            "LX/33m;",
            "LX/33n;",
            "LX/33o;",
            "LX/33p;",
            "LX/33q;",
            "LX/33r;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494076
    iput-object p1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->b:LX/11H;

    .line 494077
    iput-object p2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->c:LX/33h;

    .line 494078
    iput-object p3, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 494079
    iput-object p4, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->e:LX/2Zp;

    .line 494080
    iput-object p5, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->d:LX/33i;

    .line 494081
    iput-object p6, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->f:LX/2cH;

    .line 494082
    iput-object p7, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->g:LX/33l;

    .line 494083
    iput-object p8, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->i:LX/33m;

    .line 494084
    iput-object p9, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->j:LX/33n;

    .line 494085
    iput-object p10, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->k:LX/33o;

    .line 494086
    iput-object p11, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->l:LX/33p;

    .line 494087
    iput-object p12, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->m:LX/33q;

    .line 494088
    iput-object p13, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->n:LX/33r;

    .line 494089
    iput-object p14, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    .line 494090
    return-void
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 494060
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 494061
    const-string v1, "fetchZeroIndicatorParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;

    .line 494062
    iget-object v1, v0, Lcom/facebook/zero/sdk/request/FetchZeroIndicatorRequestParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 494063
    :try_start_0
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->d:LX/33i;

    iget-object v0, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v2, v1, v0}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    .line 494064
    if-eqz v0, :cond_1

    .line 494065
    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0df;->J:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->c:LX/33h;

    invoke-virtual {v3, v0}, LX/33h;->a(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 494066
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 494067
    :goto_1
    return-object v0

    .line 494068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 494069
    :cond_1
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 494070
    :catch_0
    move-exception v0

    .line 494071
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 494072
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_2

    .line 494073
    throw v0

    .line 494074
    :cond_2
    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method

.method private a(LX/1qK;Ljava/lang/Boolean;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 494056
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 494057
    const-string v1, "fetchZeroTokenRequestParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    .line 494058
    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->e:LX/2Zp;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {p0, v1, v0, v2}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 494059
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/zero/protocol/ZeroTokenHandler;
    .locals 3

    .prologue
    .line 494048
    const-class v1, Lcom/facebook/zero/protocol/ZeroTokenHandler;

    monitor-enter v1

    .line 494049
    :try_start_0
    sget-object v0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 494050
    sput-object v2, Lcom/facebook/zero/protocol/ZeroTokenHandler;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 494051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->b(LX/0QB;)Lcom/facebook/zero/protocol/ZeroTokenHandler;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 494053
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/zero/protocol/ZeroTokenHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494054
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 494055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 3
    .param p1    # LX/0e6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;Z)TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 493937
    iget-object v0, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->b:LX/11H;

    .line 493938
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 493939
    if-eqz p3, :cond_0

    .line 493940
    sget-object v2, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v1, v2}, LX/14U;->a(LX/14V;)V

    .line 493941
    :goto_0
    move-object v1, v1

    .line 493942
    sget-object v2, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, p2, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 493943
    :cond_0
    sget-object v2, LX/14V;->DEFAULT:LX/14V;

    invoke-virtual {v1, v2}, LX/14U;->a(LX/14V;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/zero/protocol/ZeroTokenHandler;
    .locals 15

    .prologue
    .line 494016
    new-instance v0, Lcom/facebook/zero/protocol/ZeroTokenHandler;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    invoke-static {p0}, LX/33h;->b(LX/0QB;)LX/33h;

    move-result-object v2

    check-cast v2, LX/33h;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2Zp;->b(LX/0QB;)LX/2Zp;

    move-result-object v4

    check-cast v4, LX/2Zp;

    .line 494017
    new-instance v8, LX/33i;

    invoke-static {p0}, LX/33h;->b(LX/0QB;)LX/33h;

    move-result-object v5

    check-cast v5, LX/33h;

    .line 494018
    new-instance v6, LX/33j;

    invoke-direct {v6}, LX/33j;-><init>()V

    .line 494019
    move-object v6, v6

    .line 494020
    move-object v6, v6

    .line 494021
    check-cast v6, LX/33k;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    invoke-direct {v8, v5, v6, v7}, LX/33i;-><init>(LX/33h;LX/33k;LX/0W9;)V

    .line 494022
    move-object v5, v8

    .line 494023
    check-cast v5, LX/33i;

    invoke-static {p0}, LX/2cH;->b(LX/0QB;)LX/2cH;

    move-result-object v6

    check-cast v6, LX/2cH;

    .line 494024
    new-instance v8, LX/33l;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-direct {v8, v7}, LX/33l;-><init>(LX/0lC;)V

    .line 494025
    move-object v7, v8

    .line 494026
    check-cast v7, LX/33l;

    .line 494027
    new-instance v8, LX/33m;

    invoke-direct {v8}, LX/33m;-><init>()V

    .line 494028
    move-object v8, v8

    .line 494029
    move-object v8, v8

    .line 494030
    check-cast v8, LX/33m;

    .line 494031
    new-instance v10, LX/33n;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-direct {v10, v9}, LX/33n;-><init>(LX/0lC;)V

    .line 494032
    move-object v9, v10

    .line 494033
    check-cast v9, LX/33n;

    .line 494034
    new-instance v11, LX/33o;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v10

    check-cast v10, LX/0lC;

    invoke-direct {v11, v10}, LX/33o;-><init>(LX/0lC;)V

    .line 494035
    move-object v10, v11

    .line 494036
    check-cast v10, LX/33o;

    .line 494037
    new-instance v12, LX/33p;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v11

    check-cast v11, LX/0lC;

    invoke-direct {v12, v11}, LX/33p;-><init>(LX/0lC;)V

    .line 494038
    move-object v11, v12

    .line 494039
    check-cast v11, LX/33p;

    .line 494040
    new-instance v13, LX/33q;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v12

    check-cast v12, LX/0lC;

    invoke-direct {v13, v12}, LX/33q;-><init>(LX/0lC;)V

    .line 494041
    move-object v12, v13

    .line 494042
    check-cast v12, LX/33q;

    .line 494043
    new-instance v13, LX/33r;

    invoke-direct {v13}, LX/33r;-><init>()V

    .line 494044
    move-object v13, v13

    .line 494045
    move-object v13, v13

    .line 494046
    check-cast v13, LX/33r;

    const/16 v14, 0x15ab

    invoke-static {p0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/zero/protocol/ZeroTokenHandler;-><init>(LX/11H;LX/33h;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Zp;LX/33i;LX/2cH;LX/33l;LX/33m;LX/33n;LX/33o;LX/33p;LX/33q;LX/33r;LX/0Or;)V

    .line 494047
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 493944
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 493945
    const-string v1, "fetch_zero_token"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493946
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(LX/1qK;Ljava/lang/Boolean;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 493947
    :goto_0
    return-object v0

    .line 493948
    :cond_0
    const-string v1, "fetch_zero_token_not_bootstrap"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 493949
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(LX/1qK;Ljava/lang/Boolean;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 493950
    :cond_1
    const-string v1, "fetch_zero_indicator"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 493951
    invoke-direct {p0, p1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 493952
    :cond_2
    const-string v1, "fetch_zero_header_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 493953
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493954
    const-string v1, "fetchZeroHeaderRequestParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    .line 493955
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->f:LX/2cH;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_b

    const/4 v1, 0x1

    :goto_1
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;

    .line 493956
    if-eqz v0, :cond_c

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 493957
    goto :goto_0

    .line 493958
    :cond_3
    const-string v1, "fetch_zero_optin_content_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 493959
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493960
    const-string v1, "fetchZeroOptinContentRequestParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    .line 493961
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->g:LX/33l;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_3
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    .line 493962
    if-eqz v0, :cond_e

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_4
    move-object v0, v0

    .line 493963
    goto/16 :goto_0

    .line 493964
    :cond_4
    const-string v1, "send_zero_header_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 493965
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493966
    const-string v1, "sendZeroHeaderRequestParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    .line 493967
    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->b:LX/11H;

    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->i:LX/33m;

    sget-object v3, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 493968
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 493969
    move-object v0, v0

    .line 493970
    goto/16 :goto_0

    .line 493971
    :cond_5
    const-string v1, "zero_optin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 493972
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493973
    const-string v1, "zeroOptinParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroOptinParams;

    .line 493974
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->j:LX/33n;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v1, 0x1

    :goto_5
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;

    .line 493975
    if-eqz v0, :cond_10

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_6
    move-object v0, v0

    .line 493976
    goto/16 :goto_0

    .line 493977
    :cond_6
    const-string v1, "zero_optout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 493978
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493979
    const-string v1, "zeroOptoutParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroOptoutParams;

    .line 493980
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->k:LX/33o;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_11

    const/4 v1, 0x1

    :goto_7
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroOptoutResult;

    .line 493981
    if-eqz v0, :cond_12

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_8
    move-object v0, v0

    .line 493982
    goto/16 :goto_0

    .line 493983
    :cond_7
    const-string v1, "fetch_zero_interstitial_eligibility"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 493984
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493985
    const-string v1, "fetchZeroInterstitialEligibilityParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;

    .line 493986
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->l:LX/33p;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_13

    const/4 v1, 0x1

    :goto_9
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;

    .line 493987
    if-eqz v0, :cond_14

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_a
    move-object v0, v0

    .line 493988
    goto/16 :goto_0

    .line 493989
    :cond_8
    const-string v1, "fetch_zero_interstitial_content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 493990
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493991
    const-string v1, "fetchZeroInterstitialContentParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;

    .line 493992
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->m:LX/33q;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_15

    const/4 v1, 0x1

    :goto_b
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;

    .line 493993
    if-eqz v0, :cond_16

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_c
    move-object v0, v0

    .line 493994
    goto/16 :goto_0

    .line 493995
    :cond_9
    const-string v1, "zero_update_status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 493996
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 493997
    const-string v1, "zeroUpdateStatusParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/ZeroUpdateStatusParams;

    .line 493998
    iget-object v2, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->n:LX/33r;

    iget-object v1, p0, Lcom/facebook/zero/protocol/ZeroTokenHandler;->o:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_17

    const/4 v1, 0x1

    :goto_d
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(Lcom/facebook/zero/protocol/ZeroTokenHandler;LX/0e6;Ljava/lang/Object;Z)Ljava/lang/Object;

    .line 493999
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 494000
    move-object v0, v0

    .line 494001
    goto/16 :goto_0

    .line 494002
    :cond_a
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 494003
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 494004
    :cond_c
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_2

    .line 494005
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 494006
    :cond_e
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_4

    .line 494007
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 494008
    :cond_10
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_6

    .line 494009
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 494010
    :cond_12
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_8

    .line 494011
    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_9

    .line 494012
    :cond_14
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_a

    .line 494013
    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_b

    .line 494014
    :cond_16
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_c

    .line 494015
    :cond_17
    const/4 v1, 0x0

    goto :goto_d
.end method
