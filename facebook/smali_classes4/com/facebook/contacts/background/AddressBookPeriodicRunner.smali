.class public Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/12x;

.field private final e:LX/2UN;

.field private final f:LX/0WJ;

.field public final g:LX/0aG;

.field public final h:LX/0SG;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public final j:LX/0Uo;

.field private final k:LX/0W9;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final n:LX/0qK;

.field private final o:LX/03V;

.field private final p:LX/0kb;

.field private final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Uh;

.field public s:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private u:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public v:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private x:Landroid/app/PendingIntent;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 548978
    const-class v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 548979
    sput-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12x;LX/2It;LX/0WJ;LX/0aG;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/0Uo;LX/0W9;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/03V;LX/0kb;LX/0Or;LX/0Uh;)V
    .locals 6
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/annotations/IsAddressBookSyncEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "LX/2It;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0aG;",
            "LX/0SG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Uo;",
            "LX/0W9;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0kb;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 548958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548959
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    .line 548960
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->w:Ljava/lang/String;

    .line 548961
    iput-object p1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->c:Landroid/content/Context;

    .line 548962
    iput-object p2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->d:LX/12x;

    .line 548963
    new-instance v2, LX/2UN;

    move-object/from16 v0, p12

    invoke-direct {v2, p3, v0}, LX/2UN;-><init>(LX/2It;LX/03V;)V

    iput-object v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->e:LX/2UN;

    .line 548964
    iput-object p4, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->f:LX/0WJ;

    .line 548965
    iput-object p5, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->g:LX/0aG;

    .line 548966
    iput-object p6, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->h:LX/0SG;

    .line 548967
    iput-object p7, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->i:Ljava/util/concurrent/ExecutorService;

    .line 548968
    iput-object p8, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->j:LX/0Uo;

    .line 548969
    iput-object p9, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->k:LX/0W9;

    .line 548970
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->l:LX/0Or;

    .line 548971
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 548972
    new-instance v2, LX/0qK;

    const/4 v3, 0x5

    const-wide/32 v4, 0xea60

    invoke-direct {v2, p6, v3, v4, v5}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->n:LX/0qK;

    .line 548973
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->o:LX/03V;

    .line 548974
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->p:LX/0kb;

    .line 548975
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->q:LX/0Or;

    .line 548976
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->r:LX/0Uh;

    .line 548977
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .locals 3

    .prologue
    .line 548830
    sget-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    if-nez v0, :cond_1

    .line 548831
    const-class v1, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    monitor-enter v1

    .line 548832
    :try_start_0
    sget-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 548833
    if-eqz v2, :cond_0

    .line 548834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->b(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548835
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 548836
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 548837
    :cond_1
    sget-object v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    return-object v0

    .line 548838
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 548839
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .locals 17

    .prologue
    .line 548946
    new-instance v1, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v3

    check-cast v3, LX/12x;

    invoke-static/range {p0 .. p0}, LX/2It;->a(LX/0QB;)LX/2It;

    move-result-object v4

    check-cast v4, LX/2It;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v9

    check-cast v9, LX/0Uo;

    invoke-static/range {p0 .. p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v12, 0x1477

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v14

    check-cast v14, LX/0kb;

    const/16 v15, 0x438

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;-><init>(Landroid/content/Context;LX/12x;LX/2It;LX/0WJ;LX/0aG;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/0Uo;LX/0W9;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/03V;LX/0kb;LX/0Or;LX/0Uh;)V

    .line 548947
    return-object v1
.end method

.method private static declared-synchronized f(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)Z
    .locals 14
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 548928
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->f:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->p:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 548929
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 548930
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 548931
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->e:LX/2UN;

    sget-object v2, LX/3Fg;->a:LX/3OK;

    const-wide/16 v4, -0x1

    invoke-static {v1, v2, v4, v5}, LX/2UN;->a$redex0(LX/2UN;LX/3OK;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    .line 548932
    :cond_2
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->w:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 548933
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->e:LX/2UN;

    sget-object v2, LX/3Fg;->c:LX/3OK;

    iget-object v3, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->k:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/2UN;->a$redex0(LX/2UN;LX/3OK;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->w:Ljava/lang/String;
    :try_end_1
    .catch LX/6Lm; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548934
    :cond_3
    :try_start_2
    const/4 v6, 0x1

    .line 548935
    iget-object v7, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->h:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    sub-long/2addr v8, v10

    .line 548936
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gez v7, :cond_6

    .line 548937
    :cond_4
    :goto_1
    move v1, v6

    .line 548938
    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->k:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->w:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    .line 548939
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 548940
    :catch_0
    goto :goto_0

    .line 548941
    :cond_6
    iget-object v7, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->j:LX/0Uo;

    invoke-virtual {v7}, LX/0Uo;->j()Z

    move-result v7

    .line 548942
    iget-wide v10, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-eqz v10, :cond_4

    .line 548943
    if-eqz v7, :cond_7

    const-wide/32 v10, 0x5265c00

    cmp-long v10, v8, v10

    if-gez v10, :cond_4

    .line 548944
    :cond_7
    if-nez v7, :cond_8

    const-wide/32 v10, 0x36ee80

    cmp-long v7, v8, v10

    if-gez v7, :cond_4

    .line 548945
    :cond_8
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized h(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    .locals 3

    .prologue
    .line 548923
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548924
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3hS;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3hS;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 548925
    invoke-static {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->i(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548926
    monitor-exit p0

    return-void

    .line 548927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static i(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    .locals 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 548915
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    .line 548916
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    .line 548917
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(Z)V

    .line 548918
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 548919
    invoke-static {}, LX/0Oz;->values()[LX/0Oz;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 548920
    invoke-virtual {v4}, LX/0Oz;->getFullUri()Landroid/net/Uri;

    move-result-object v4

    const/4 p0, 0x0

    invoke-virtual {v0, v4, p0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 548921
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 548922
    :cond_0
    return-void
.end method

.method public static declared-synchronized j(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    .locals 4

    .prologue
    .line 548948
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548949
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548950
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3hS;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 548951
    iget-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 548952
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    .line 548953
    :goto_0
    iget-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    .line 548954
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548955
    monitor-exit p0

    return-void

    .line 548956
    :cond_0
    const-wide/16 v0, 0x2

    :try_start_1
    iget-wide v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized k(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 548900
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->k:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 548901
    const-string v0, "n/a"

    .line 548902
    const-string v1, "n/a"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548903
    :try_start_1
    iget-object v3, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->e:LX/2UN;

    sget-object v4, LX/3Fg;->a:LX/3OK;

    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/2UN;->a$redex0(LX/2UN;LX/3OK;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 548904
    iget-object v3, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->e:LX/2UN;

    sget-object v4, LX/3Fg;->c:LX/3OK;

    invoke-static {v3, v4, v2}, LX/2UN;->a$redex0(LX/2UN;LX/3OK;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch LX/6Lm; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 548905
    :goto_0
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AddressBook rate limit exceeded. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 548906
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "now="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->h:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548907
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ",nextDelayMs="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548908
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ",lastSyncTimestamp="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548909
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ",last_contact_sync_client_time_ms="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548910
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, ",lastSyncLocale="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->w:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548911
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, ",currentLocale="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548912
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",last_contacts_sync_client_locale="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548913
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catch_0
    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_0

    .line 548914
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized n(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    .locals 7

    .prologue
    .line 548890
    monitor-enter p0

    .line 548891
    :try_start_0
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 548892
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 548893
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->g:LX/0aG;

    const-string v2, "sync_contacts_partial"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v5, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v6, -0x5fedce23

    invoke-static/range {v1 .. v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    .line 548894
    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548895
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/3gk;

    invoke-direct {v2, p0}, LX/3gk;-><init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 548896
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548897
    monitor-exit p0

    return-void

    .line 548898
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 548899
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 548887
    const/4 v0, 0x1

    .line 548888
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/contacts/background/AddressBookPeriodicRunner$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner$1;-><init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;Z)V

    const v3, -0x22f5f956

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 548889
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 5

    .prologue
    .line 548873
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2Jp;->OMNISTORE_CONTACTS_COLLECTION:LX/2Jp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 548874
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 548875
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_0

    .line 548876
    :cond_2
    invoke-static {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->f(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548877
    if-eqz p1, :cond_3

    .line 548878
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    .line 548879
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/contacts/background/AddressBookPeriodicRunner$LocalBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 548880
    const-string v1, "com.facebook.orca.database.ACTION_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 548881
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->c:Landroid/content/Context;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;

    .line 548882
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    add-long/2addr v0, v2

    .line 548883
    iget-object v2, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->d:LX/12x;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;

    .line 548884
    invoke-virtual {v2, v3, v0, v1, v4}, LX/12x;->c(IJLandroid/app/PendingIntent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548885
    goto :goto_0

    .line 548886
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 548868
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 548869
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->d:LX/12x;

    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 548870
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->x:Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548871
    :cond_0
    monitor-exit p0

    return-void

    .line 548872
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 2

    .prologue
    .line 548857
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 548858
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 548859
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548860
    :cond_0
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 548861
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 548862
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548863
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->c()V

    .line 548864
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->u:J

    .line 548865
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->v:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548866
    monitor-exit p0

    return-void

    .line 548867
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 548840
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->f(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548841
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 548842
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 548843
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 548844
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->n:LX/0qK;

    invoke-virtual {v0}, LX/0qK;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 548845
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->o:LX/03V;

    const-string v1, "AddressBookPeriodicRunner"

    invoke-static {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->k(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 548847
    :cond_2
    :try_start_2
    const/4 v1, 0x0

    .line 548848
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 548849
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->r:LX/0Uh;

    const/16 v2, 0x13a

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 548850
    invoke-static {p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->n(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 548851
    :goto_2
    goto :goto_0

    :cond_3
    move v0, v1

    .line 548852
    goto :goto_1

    .line 548853
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 548854
    iget-object v1, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->g:LX/0aG;

    const-string v2, "sync_favorite_contacts"

    const v3, 0x7b237757

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 548855
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 548856
    iget-object v0, p0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/3fW;

    invoke-direct {v1, p0}, LX/3fW;-><init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_2
.end method
