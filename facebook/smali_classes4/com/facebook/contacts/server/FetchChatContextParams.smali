.class public Lcom/facebook/contacts/server/FetchChatContextParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchChatContextParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 561049
    new-instance v0, LX/3P4;

    invoke-direct {v0}, LX/3P4;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchChatContextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 561050
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/contacts/server/FetchChatContextParams;-><init>(LX/0am;Z)V

    .line 561051
    return-void
.end method

.method public constructor <init>(LX/0am;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 561052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561053
    iput-object p1, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->a:LX/0am;

    .line 561054
    iput-boolean p2, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->b:Z

    .line 561055
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 561056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561057
    const-class v0, Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 561058
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->a:LX/0am;

    .line 561059
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->b:Z

    .line 561060
    return-void

    .line 561061
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 561062
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 561063
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 561064
    iget-boolean v0, p0, Lcom/facebook/contacts/server/FetchChatContextParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 561065
    return-void

    .line 561066
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
