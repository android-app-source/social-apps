.class public Lcom/facebook/contacts/picker/DivebarSearchBarView;
.super LX/3Nf;
.source ""


# instance fields
.field private a:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 559683
    invoke-direct {p0, p1, p2}, LX/3Nf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 559684
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 559685
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarSearchBarView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 559686
    :goto_0
    return-void

    .line 559687
    :cond_0
    const/4 v0, -0x2

    .line 559688
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 559689
    invoke-virtual {p0}, Lcom/facebook/contacts/picker/DivebarSearchBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010350

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 559690
    invoke-virtual {p0}, Lcom/facebook/contacts/picker/DivebarSearchBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 559691
    :cond_1
    iget-object v1, p0, Lcom/facebook/contacts/picker/DivebarSearchBarView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 559692
    new-instance v1, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/facebook/contacts/picker/DivebarSearchBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 559693
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 559694
    invoke-virtual {p0}, Lcom/facebook/contacts/picker/DivebarSearchBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f021231

    .line 559695
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 559696
    const/16 v4, 0x15

    if-lt v3, v4, :cond_2

    .line 559697
    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object v3, v3

    .line 559698
    :goto_1
    move-object v0, v3

    .line 559699
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 559700
    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 559701
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarSearchBarView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 559702
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarSearchBarView;->a:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 559703
    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 559704
    const v0, 0x7f030d04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 559705
    const v0, 0x7f0d2085    # 1.8759E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/contacts/picker/DivebarSearchBarView;->a:Landroid/view/ViewGroup;

    .line 559706
    return-void
.end method
