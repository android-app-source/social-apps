.class public Lcom/facebook/productionprompts/model/ProductionPrompt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/model/ProductionPromptDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/model/ProductionPromptSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/productionprompts/model/ProductionPrompt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mBannerSubheader:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "banner_subheader"
    .end annotation
.end field

.field public final mBannerText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "banner_text"
    .end annotation
.end field

.field public final mCallToActionText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cta_text"
    .end annotation
.end field

.field public final mCheckinLocationId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "checkin_location_id"
    .end annotation
.end field

.field public final mCheckinLocationName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "checkin_location_name"
    .end annotation
.end field

.field public final mComposerPromptText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_prompt_text"
    .end annotation
.end field

.field public final mDismissSurveyId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "dismiss_survey_id"
    .end annotation
.end field

.field public final mEndTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_time"
    .end annotation
.end field

.field public final mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame_pack_model"
    .end annotation
.end field

.field public final mIgnoreSurveyId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ignore_survey_id"
    .end annotation
.end field

.field public final mInspirationDefaultIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_default_index"
    .end annotation
.end field

.field public final mLinkAttachmentUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_attachment_url"
    .end annotation
.end field

.field public final mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mask_model"
    .end annotation
.end field

.field public final mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "meme_category_fields_model"
    .end annotation
.end field

.field public final mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object"
    .end annotation
.end field

.field public final mOpenAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "open_action"
    .end annotation
.end field

.field public final mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "particle_effect_model"
    .end annotation
.end field

.field public final mPostWithMinutiaeSurveyId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_with_minutiae_survey_id"
    .end annotation
.end field

.field public final mPrefillText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prefill_text"
    .end annotation
.end field

.field public final mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profile_picture_overlay"
    .end annotation
.end field

.field public final mPromptConfidence:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_confidence"
    .end annotation
.end field

.field public final mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_display_reason"
    .end annotation
.end field

.field public final mPromptFeedType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_feed_type"
    .end annotation
.end field

.field public final mPromptId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final mPromptImageUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_uri"
    .end annotation
.end field

.field public final mPromptTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field

.field public final mPromptType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation
.end field

.field public final mServerRankingScore:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "server_ranking_score"
    .end annotation
.end field

.field public final mServerTrackingString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "server_tracking_string"
    .end annotation
.end field

.field public final mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shader_filter_model"
    .end annotation
.end field

.field public final mStartTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time"
    .end annotation
.end field

.field public final mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style_transfer_model"
    .end annotation
.end field

.field public final mSuggestedCoverPhotos:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggested_cover_photos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mTaggedUsers:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_Users"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final mThumbnailUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "thumbnail_uri"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 490483
    const-class v0, Lcom/facebook/productionprompts/model/ProductionPromptDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 490484
    const-class v0, Lcom/facebook/productionprompts/model/ProductionPromptSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 490485
    new-instance v0, LX/31v;

    invoke-direct {v0}, LX/31v;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/model/ProductionPrompt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 490486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490487
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    .line 490488
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    .line 490489
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    .line 490490
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    .line 490491
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    .line 490492
    iput-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    .line 490493
    iput-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    .line 490494
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    .line 490495
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 490496
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    .line 490497
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    .line 490498
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    .line 490499
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    .line 490500
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    .line 490501
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    .line 490502
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    .line 490503
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    .line 490504
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    .line 490505
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 490506
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 490507
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 490508
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 490509
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 490510
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    .line 490511
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    .line 490512
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 490513
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    .line 490514
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    .line 490515
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    .line 490516
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    .line 490517
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 490518
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    .line 490519
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    .line 490520
    iput-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    .line 490521
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    .line 490522
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 490523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490524
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    .line 490525
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    .line 490526
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    .line 490527
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    .line 490528
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    .line 490529
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    .line 490530
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    .line 490531
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    .line 490532
    const-class v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 490533
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    .line 490534
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    .line 490535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    .line 490536
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    .line 490537
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    .line 490538
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    .line 490539
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    .line 490540
    const-class v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    .line 490541
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    .line 490542
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 490543
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 490544
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 490545
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 490546
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 490547
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    .line 490548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    .line 490549
    const-class v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 490550
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    .line 490551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    .line 490552
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    .line 490553
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    .line 490554
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 490555
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 490556
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 490557
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    .line 490558
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    .line 490559
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 490560
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    iput-object v1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    .line 490561
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    .line 490562
    return-void

    .line 490563
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 490564
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/model/ProfilePictureOverlay;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;DLjava/lang/String;Lcom/facebook/productionprompts/model/PromptDisplayReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;LX/0Px;Ljava/lang/String;LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/model/ProfilePictureOverlay;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;",
            "Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;",
            "Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;",
            "Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;",
            "D",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/model/PromptDisplayReason;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 490565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490566
    iput-object p1, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    .line 490567
    iput-object p2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    .line 490568
    iput-object p3, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    .line 490569
    iput-object p4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    .line 490570
    iput-object p5, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    .line 490571
    iput-wide p6, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    .line 490572
    iput-wide p8, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    .line 490573
    iput-object p10, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    .line 490574
    iput-object p11, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 490575
    iput-object p12, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    .line 490576
    iput-object p13, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    .line 490577
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    .line 490578
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    .line 490579
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    .line 490580
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    .line 490581
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    .line 490582
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    .line 490583
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    .line 490584
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 490585
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 490586
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 490587
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 490588
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 490589
    move-wide/from16 v0, p26

    iput-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    .line 490590
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    .line 490591
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 490592
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    .line 490593
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    .line 490594
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    .line 490595
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    .line 490596
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 490597
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    .line 490598
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    .line 490599
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    .line 490600
    move/from16 v0, p38

    iput v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    .line 490601
    return-void
.end method

.method private E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490602
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    return-object v0
.end method

.method public static a(LX/2ry;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 39

    .prologue
    .line 490603
    new-instance v0, Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual/range {p0 .. p0}, LX/2ry;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, LX/2ry;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, LX/2ry;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, LX/2ry;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, LX/2ry;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, LX/2ry;->f()J

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, LX/2ry;->g()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, LX/2ry;->h()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, LX/2ry;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, LX/2ry;->j()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, LX/2ry;->l()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, LX/2ry;->k()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, LX/2ry;->m()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, LX/2ry;->n()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, LX/2ry;->o()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, LX/2ry;->p()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, LX/2ry;->q()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, LX/2ry;->r()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, LX/2ry;->s()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, LX/2ry;->t()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, LX/2ry;->u()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, LX/2ry;->v()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, LX/2ry;->w()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, LX/2ry;->x()D

    move-result-wide v26

    invoke-virtual/range {p0 .. p0}, LX/2ry;->y()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {p0 .. p0}, LX/2ry;->z()Lcom/facebook/productionprompts/model/PromptDisplayReason;

    move-result-object v29

    invoke-virtual/range {p0 .. p0}, LX/2ry;->A()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {p0 .. p0}, LX/2ry;->B()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {p0 .. p0}, LX/2ry;->C()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, LX/2ry;->D()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {p0 .. p0}, LX/2ry;->E()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v34

    invoke-virtual/range {p0 .. p0}, LX/2ry;->F()LX/0Px;

    move-result-object v35

    invoke-virtual/range {p0 .. p0}, LX/2ry;->G()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, LX/2ry;->H()LX/0Px;

    move-result-object v37

    invoke-virtual/range {p0 .. p0}, LX/2ry;->I()I

    move-result v38

    invoke-direct/range {v0 .. v38}, Lcom/facebook/productionprompts/model/ProductionPrompt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/productionprompts/model/ProfilePictureOverlay;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;DLjava/lang/String;Lcom/facebook/productionprompts/model/PromptDisplayReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;LX/0Px;Ljava/lang/String;LX/0Px;I)V

    return-object v0
.end method

.method public static a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490604
    invoke-static {p0, p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 490605
    const/4 v0, 0x0

    .line 490606
    :goto_0
    return-object v0

    .line 490607
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 490608
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 490609
    invoke-static {v0}, LX/5O7;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 490610
    invoke-static {v1}, LX/5O7;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 490611
    new-instance v4, LX/2ry;

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/2ry;-><init>(Ljava/lang/String;)V

    .line 490612
    iput-wide v2, v4, LX/2ry;->f:J

    .line 490613
    move-object v2, v4

    .line 490614
    iput-wide v0, v2, LX/2ry;->g:J

    .line 490615
    move-object v1, v2

    .line 490616
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->q()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptTitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 490617
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->q()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptTitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptTitleModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 490618
    iput-object v0, v1, LX/2ry;->b:Ljava/lang/String;

    .line 490619
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 490620
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 490621
    iput-object v0, v1, LX/2ry;->e:Ljava/lang/String;

    .line 490622
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 490623
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 490624
    iput-object v0, v1, LX/2ry;->k:Ljava/lang/String;

    .line 490625
    move-object v0, v1

    .line 490626
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 490627
    iput-object v2, v0, LX/2ry;->l:Ljava/lang/String;

    .line 490628
    move-object v0, v0

    .line 490629
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptSurveyModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 490630
    iput-object v2, v0, LX/2ry;->m:Ljava/lang/String;

    .line 490631
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->t()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel;->a()LX/0Px;

    move-result-object v0

    .line 490632
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_11

    .line 490633
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;

    .line 490634
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 490635
    iput-object v2, v1, LX/2ry;->c:Ljava/lang/String;

    .line 490636
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 490637
    iput-object v2, v1, LX/2ry;->d:Ljava/lang/String;

    .line 490638
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 490639
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;LX/2ry;)V

    .line 490640
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;LX/2ry;)V

    .line 490641
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;

    move-result-object v2

    .line 490642
    if-eqz v2, :cond_4

    .line 490643
    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel$TaggableActivityIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel$TaggableActivityIconModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MinutiaeActionModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Ljava/lang/String;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    .line 490644
    iput-object v2, v1, LX/2ry;->i:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 490645
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;

    move-result-object v2

    .line 490646
    if-eqz v2, :cond_5

    .line 490647
    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 490648
    iput-object v3, v1, LX/2ry;->n:Ljava/lang/String;

    .line 490649
    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$CheckinLocationModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 490650
    iput-object v2, v1, LX/2ry;->o:Ljava/lang/String;

    .line 490651
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;

    move-result-object v2

    .line 490652
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 490653
    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$LinkAttachmentModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 490654
    iput-object v2, v1, LX/2ry;->j:Ljava/lang/String;

    .line 490655
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->n()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v2

    .line 490656
    iput-object v2, v1, LX/2ry;->E:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 490657
    if-eqz p1, :cond_17

    .line 490658
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 490659
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->v()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$ThumbnailImageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 490660
    iput-object v2, v1, LX/2ry;->r:Ljava/lang/String;

    .line 490661
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 490662
    new-instance v2, LX/5ii;

    invoke-direct {v2}, LX/5ii;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 490663
    iput-object v3, v2, LX/5ii;->b:LX/0Px;

    .line 490664
    move-object v2, v2

    .line 490665
    invoke-virtual {v2}, LX/5ii;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v2

    .line 490666
    iput-object v2, v1, LX/2ry;->s:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 490667
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 490668
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v2

    .line 490669
    iput-object v2, v1, LX/2ry;->t:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    .line 490670
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 490671
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v2

    .line 490672
    iput-object v2, v1, LX/2ry;->u:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 490673
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 490674
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v2

    .line 490675
    iput-object v2, v1, LX/2ry;->v:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    .line 490676
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 490677
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v2

    .line 490678
    iput-object v2, v1, LX/2ry;->w:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    .line 490679
    :cond_c
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->t()LX/0Px;

    move-result-object v2

    .line 490680
    iput-object v2, v1, LX/2ry;->F:LX/0Px;

    .line 490681
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 490682
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 490683
    iput-object v2, v1, LX/2ry;->h:Ljava/lang/String;

    .line 490684
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 490685
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 490686
    iput-object v2, v1, LX/2ry;->B:Ljava/lang/String;

    .line 490687
    :cond_f
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 490688
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 490689
    iput-object v2, v1, LX/2ry;->C:Ljava/lang/String;

    .line 490690
    :cond_10
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 490691
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490692
    iput-object v0, v1, LX/2ry;->D:Ljava/lang/String;

    .line 490693
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->r()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 490694
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->r()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490695
    iput-object v0, v1, LX/2ry;->p:Ljava/lang/String;

    .line 490696
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->s()D

    move-result-wide v2

    .line 490697
    iput-wide v2, v1, LX/2ry;->x:D

    .line 490698
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 490699
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->v()Ljava/lang/String;

    move-result-object v0

    .line 490700
    iput-object v0, v1, LX/2ry;->y:Ljava/lang/String;

    .line 490701
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 490702
    new-instance v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/productionprompts/model/PromptDisplayReason;-><init>(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V

    .line 490703
    iput-object v0, v1, LX/2ry;->z:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 490704
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 490705
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490706
    iput-object v0, v1, LX/2ry;->A:Ljava/lang/String;

    .line 490707
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 490708
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490709
    iput-object v0, v1, LX/2ry;->G:Ljava/lang/String;

    .line 490710
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->k()I

    move-result v0

    .line 490711
    iput v0, v1, LX/2ry;->I:I

    .line 490712
    invoke-virtual {v1}, LX/2ry;->J()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    goto/16 :goto_0

    .line 490713
    :cond_17
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v2

    .line 490714
    iput-object v2, v1, LX/2ry;->s:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 490715
    goto/16 :goto_1
.end method

.method private static a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;LX/2ry;)V
    .locals 2

    .prologue
    .line 490716
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 490717
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->q()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    move-result-object v0

    .line 490718
    iput-object v0, p1, LX/2ry;->q:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    .line 490719
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Ljava/util/Calendar;Z)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 490720
    invoke-static {p0, p2}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 490721
    :cond_0
    :goto_0
    return v0

    .line 490722
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 490723
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 490724
    invoke-static {v1}, LX/5O7;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 490725
    invoke-static {v2}, LX/5O7;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 490726
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 490727
    cmp-long v1, v4, v6

    if-gtz v1, :cond_0

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 490762
    if-nez p1, :cond_0

    move v0, v2

    .line 490763
    :goto_0
    return v0

    .line 490764
    :cond_0
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 490765
    :goto_1
    iget-object v4, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-virtual {v4}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-virtual {v1}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 490766
    :cond_1
    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-wide v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v6, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-wide v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v6, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v4, :cond_4

    iget-object v4, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v4, :cond_5

    :cond_2
    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    if-nez v4, :cond_7

    iget-object v4, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    if-nez v4, :cond_6

    move v4, v3

    :goto_2
    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    if-nez v0, :cond_8

    if-nez v1, :cond_5

    :goto_3
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    if-nez v0, :cond_9

    iget-object v0, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    if-nez v0, :cond_5

    :goto_4
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    if-nez v0, :cond_a

    iget-object v0, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    if-nez v0, :cond_5

    :goto_5
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    if-nez v0, :cond_b

    iget-object v0, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    if-nez v0, :cond_5

    :goto_6
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    if-nez v0, :cond_c

    iget-object v0, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    if-nez v0, :cond_5

    :goto_7
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    .line 490767
    goto/16 :goto_1

    .line 490768
    :cond_4
    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v5, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v4, v5}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    move v4, v2

    goto/16 :goto_2

    :cond_7
    iget-object v4, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_3

    :cond_9
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_4

    :cond_a
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_5

    :cond_b
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_7
.end method

.method private static b(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;LX/2ry;)V
    .locals 8

    .prologue
    .line 490728
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->u()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 490729
    :cond_0
    :goto_0
    return-void

    .line 490730
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 490731
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel;->u()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;

    .line 490732
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->j()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 490733
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->k()Ljava/lang/String;

    move-result-object v6

    .line 490734
    iput-object v6, v5, LX/5Rc;->b:Ljava/lang/String;

    .line 490735
    move-object v5, v5

    .line 490736
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 490737
    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel;->l()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$TaggingActionModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 490738
    iput-object v0, v5, LX/5Rc;->c:Ljava/lang/String;

    .line 490739
    :cond_2
    invoke-virtual {v5}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 490740
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 490741
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 490742
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 490743
    iput-object v0, p1, LX/2ry;->H:LX/0Px;

    .line 490744
    goto/16 :goto_0
.end method

.method private static b(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 490753
    if-nez p0, :cond_1

    .line 490754
    :cond_0
    :goto_0
    return v2

    .line 490755
    :cond_1
    if-nez p1, :cond_5

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->o()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v4, v1

    .line 490756
    :goto_1
    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->q()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptTitleModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->q()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$PromptTitleModel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v1

    .line 490757
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->r()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPromptType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    if-ne v3, v5, :cond_7

    move v3, v1

    .line 490758
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;->u()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$TimeRangeModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    if-eqz v3, :cond_0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v4, v2

    .line 490759
    goto :goto_1

    :cond_6
    move v0, v2

    .line 490760
    goto :goto_2

    :cond_7
    move v3, v2

    .line 490761
    goto :goto_3
.end method


# virtual methods
.method public final A()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490752
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490751
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    return-object v0
.end method

.method public final C()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490750
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    return-object v0
.end method

.method public final D()I
    .locals 1

    .prologue
    .line 490749
    iget v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490748
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490747
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490746
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490745
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 490479
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490480
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 490481
    const/4 v0, 0x0

    .line 490482
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 490426
    if-ne p0, p1, :cond_1

    .line 490427
    :cond_0
    :goto_0
    return v0

    .line 490428
    :cond_1
    instance-of v2, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;

    if-nez v2, :cond_2

    move v0, v1

    .line 490429
    goto :goto_0

    .line 490430
    :cond_2
    check-cast p1, Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 490431
    invoke-direct {p0, p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-wide v4, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490424
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490423
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490422
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 490421
    const/16 v0, 0x24

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-wide v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget v2, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490420
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 490419
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 490417
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->i()Ljava/lang/String;

    move-result-object v0

    .line 490418
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 490416
    invoke-direct {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->E()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->LIVE:Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490425
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490415
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490414
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490432
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490433
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    return-object v0
.end method

.method public final r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490434
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490435
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    return-object v0
.end method

.method public final t()D
    .locals 2

    .prologue
    .line 490436
    iget-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    return-wide v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490437
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490438
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490439
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 490440
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490441
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490442
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490443
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mBannerSubheader:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490444
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptImageUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490445
    iget-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStartTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 490446
    iget-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 490447
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mComposerPromptText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490448
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 490449
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mLinkAttachmentUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490450
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mDismissSurveyId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490451
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mIgnoreSurveyId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490452
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPostWithMinutiaeSurveyId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490453
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490454
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCheckinLocationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490455
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490456
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mProfilePictureOverlay:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 490457
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mThumbnailUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490458
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mFramePackModel:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490459
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMaskEffectModel:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490460
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mParticleEffectModel:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490461
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mShaderFilterModel:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490462
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mStyleTransferModel:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490463
    iget-wide v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerRankingScore:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 490464
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mServerTrackingString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490465
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptDisplayReason:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 490466
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptConfidence:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490467
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490468
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490469
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mOpenAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490470
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 490471
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mSuggestedCoverPhotos:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 490472
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPromptFeedType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490473
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mTaggedUsers:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 490474
    iget v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mInspirationDefaultIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 490475
    return-void
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490476
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mCallToActionText:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490477
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mPrefillText:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 490478
    iget-object v0, p0, Lcom/facebook/productionprompts/model/ProductionPrompt;->mMemeCategoryFieldsModel:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    return-object v0
.end method
