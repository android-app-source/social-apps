.class public Lcom/facebook/auth/credentials/SessionCookie;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/auth/credentials/SessionCookieDeserializer;
.end annotation

.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mDomain:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "domain"
    .end annotation
.end field

.field public mExpires:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "expires"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public mPath:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "path"
    .end annotation
.end field

.field public mSecure:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secure"
    .end annotation
.end field

.field public mValue:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566431
    const-class v0, Lcom/facebook/auth/credentials/SessionCookieDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 566430
    const-class v0, Lcom/facebook/auth/credentials/SessionCookieSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 566429
    new-instance v0, LX/3Qr;

    invoke-direct {v0}, LX/3Qr;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/SessionCookie;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 566403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566404
    iput-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    .line 566405
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    .line 566406
    iput-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    .line 566407
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    .line 566408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    .line 566409
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    .line 566410
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 566420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566421
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    .line 566422
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    .line 566423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    .line 566424
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    .line 566425
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    .line 566426
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    .line 566427
    return-void

    .line 566428
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0lC;Ljava/lang/String;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 566432
    if-nez p1, :cond_1

    .line 566433
    :cond_0
    :goto_0
    return-object v0

    .line 566434
    :cond_1
    :try_start_0
    invoke-virtual {p0}, LX/0lD;->b()LX/0lp;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 566435
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_0

    .line 566436
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 566437
    :goto_1
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 566438
    invoke-static {v1}, LX/3Qs;->a(LX/15w;)Lcom/facebook/auth/credentials/SessionCookie;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 566439
    :catch_0
    goto :goto_0

    .line 566440
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0lC;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 566411
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 566412
    invoke-static {p0, p1}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 566413
    if-eqz v2, :cond_1

    .line 566414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 566415
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 566416
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566417
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 566418
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 566419
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 566402
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 566390
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 566391
    const/4 v0, 0x0

    .line 566392
    :goto_0
    return-object v0

    .line 566393
    :cond_0
    new-instance v0, LX/41E;

    invoke-direct {v0, p0}, LX/41E;-><init>(Lcom/facebook/auth/credentials/SessionCookie;)V

    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/41E;->a(Ljava/lang/String;Ljava/lang/String;)LX/41E;

    move-result-object v0

    .line 566394
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 566395
    const-string v1, "Expires"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/41E;->a(Ljava/lang/String;Ljava/lang/String;)LX/41E;

    .line 566396
    :cond_1
    const-string v1, "Domain"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/41E;->a(Ljava/lang/String;Ljava/lang/String;)LX/41E;

    move-result-object v1

    const-string v2, "Path"

    iget-object v3, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/41E;->a(Ljava/lang/String;Ljava/lang/String;)LX/41E;

    .line 566397
    iget-boolean v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    if-eqz v1, :cond_2

    .line 566398
    const-string v1, "secure"

    .line 566399
    iget-object v2, v0, LX/41E;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566400
    :cond_2
    const-string v1, "; "

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    iget-object v2, v0, LX/41E;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 566401
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 566382
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566383
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566384
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566385
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566386
    iget-boolean v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 566387
    iget-object v0, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566388
    return-void

    .line 566389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
