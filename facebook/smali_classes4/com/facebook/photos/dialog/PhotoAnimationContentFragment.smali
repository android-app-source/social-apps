.class public abstract Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:LX/9e9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 364723
    new-instance v0, LX/9e9;

    invoke-direct {v0}, LX/9e9;-><init>()V

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a:LX/9e9;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 364721
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 364722
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public a(LX/1L1;ZI)V
    .locals 0

    .prologue
    .line 364720
    return-void
.end method

.method public a(LX/9eB;)V
    .locals 0

    .prologue
    .line 364719
    return-void
.end method

.method public abstract a(LX/9eC;)Z
.end method

.method public abstract b()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public c()V
    .locals 0

    .prologue
    .line 364718
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 364717
    const/4 v0, 0x0

    return v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public k()LX/9eT;
    .locals 6

    .prologue
    .line 364716
    new-instance v0, LX/9eT;

    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a:LX/9e9;

    const-string v2, "OpenPhotoGallery"

    const v3, 0x140008

    const v4, 0x140009

    const v5, 0x14000e

    invoke-direct/range {v0 .. v5}, LX/9eT;-><init>(LX/0Pq;Ljava/lang/String;III)V

    return-object v0
.end method
