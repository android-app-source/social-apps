.class public Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0hF;


# static fields
.field private static final A:Ljava/lang/Object;

.field private static B:J

.field public static final m:Ljava/lang/String;

.field public static final n:Ljava/lang/String;

.field public static final o:Ljava/lang/String;

.field public static final p:Ljava/lang/String;

.field public static final q:Ljava/lang/String;

.field public static final z:Ljava/lang/String;


# instance fields
.field public C:Ljava/lang/Throwable;

.field public D:Landroid/widget/FrameLayout;

.field public E:Landroid/view/View;

.field public F:LX/9eQ;

.field public G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

.field public H:Landroid/widget/FrameLayout;

.field public I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/9hN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Landroid/content/DialogInterface$OnDismissListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:LX/31M;

.field private N:I

.field private O:I

.field private P:Z

.field private Q:Ljava/lang/String;

.field public R:LX/8Hs;

.field private S:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final T:LX/9eF;

.field private final U:LX/9eF;

.field public V:LX/9eK;

.field public W:Z

.field public X:Landroid/animation/ValueAnimator;

.field public r:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/75D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/9eS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 461690
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_PHOTOS_FEED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->m:Ljava/lang/String;

    .line 461691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_MEDIA_GALLERY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n:Ljava/lang/String;

    .line 461692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_PHOTOS_PRIVACY_FEED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->o:Ljava/lang/String;

    .line 461693
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_SOUVENIRS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->p:Ljava/lang/String;

    .line 461694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_SPHERICAL_PHOTOS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->q:Ljava/lang/String;

    .line 461695
    const-class v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    .line 461696
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->A:Ljava/lang/Object;

    .line 461697
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->B:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 461698
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 461699
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    .line 461700
    new-instance v0, LX/9eJ;

    invoke-direct {v0, p0}, LX/9eJ;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->T:LX/9eF;

    .line 461701
    new-instance v0, LX/9eG;

    invoke-direct {v0, p0}, LX/9eG;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->U:LX/9eF;

    .line 461702
    sget-object v0, LX/9eK;->INIT:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461703
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;)Z
    .locals 10
    .param p2    # Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 461704
    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->A:Ljava/lang/Object;

    monitor-enter v1

    .line 461705
    :try_start_0
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 461706
    invoke-virtual {v0}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v4

    .line 461707
    sget-wide v6, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->B:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0xfa

    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    .line 461708
    monitor-exit v1

    move v0, v2

    .line 461709
    :goto_0
    return v0

    .line 461710
    :cond_0
    sput-wide v4, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->B:J

    .line 461711
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461712
    invoke-virtual {p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->e()Ljava/lang/String;

    move-result-object v3

    .line 461713
    const-class v0, LX/0ew;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 461714
    const-string v1, "MediaGallery needs FragmentManager to be supported on it\'s launch-site"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461715
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 461716
    goto :goto_0

    .line 461717
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 461718
    :cond_1
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 461719
    goto :goto_0

    .line 461720
    :cond_2
    const-class v1, Landroid/app/Activity;

    invoke-static {p0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 461721
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 461722
    goto :goto_0

    .line 461723
    :cond_3
    new-instance v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {v2}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;-><init>()V

    .line 461724
    iput-object p1, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461725
    iput-object p2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    .line 461726
    iput-object p3, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->J:LX/9hN;

    .line 461727
    iput-object p4, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->L:Landroid/content/DialogInterface$OnDismissListener;

    .line 461728
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 461729
    const-string v1, "content_id"

    iget-object v5, p2, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461730
    const-string v1, "EXTRA_LAUNCH_TIMESTAMP"

    .line 461731
    sget-object v5, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v5, v5

    .line 461732
    invoke-virtual {v5}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v6

    invoke-virtual {v4, v1, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 461733
    const-string v1, "EXTRA_DEFAULT_DISMISS_DIRECTION"

    iget-object v5, p2, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->e:LX/31M;

    invoke-virtual {v5}, LX/31M;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461734
    const-string v1, "EXTRA_SWIPE_DISMISS_DIRECTION_FLAGS"

    iget v5, p2, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->f:I

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461735
    const-string v1, "EXTRA_BACKGROUND_COLOR"

    iget v5, p2, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->g:I

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461736
    const-string v1, "EXTRA_ENABLE_SWIPE_TO_DISMISS"

    iget-boolean v5, p2, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->h:Z

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 461737
    const-string v5, "EXTRA_ANALYTICS_TAG"

    instance-of v1, p1, LX/0fh;

    if-eqz v1, :cond_4

    check-cast p1, LX/0fh;

    invoke-interface {p1}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461738
    invoke-virtual {v2, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 461739
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 461740
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 461741
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 461742
    :cond_4
    const-string v1, "unknown"

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 461743
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v3, LX/9eK;->ANIMATE_OUT:LX/9eK;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 461744
    sget-object v0, LX/9eK;->ANIMATE_OUT:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461745
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461746
    iput-boolean v2, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461747
    new-array v3, v1, [Landroid/graphics/drawable/Drawable;

    .line 461748
    new-array v1, v1, [LX/9hP;

    .line 461749
    invoke-static {p0, v3, v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;[Landroid/graphics/drawable/Drawable;[LX/9hP;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 461750
    invoke-static {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->b$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V

    .line 461751
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 461752
    goto :goto_0

    .line 461753
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 461754
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v0, v2}, LX/9eQ;->setVisibility(I)V

    .line 461755
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461756
    iget-object v4, v0, LX/9eQ;->c:LX/9eP;

    move-object v0, v4

    .line 461757
    iget-object v4, v0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    move v0, v4

    .line 461758
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461759
    iget-object v4, v0, LX/9eQ;->c:LX/9eP;

    move-object v0, v4

    .line 461760
    iget-object v4, v0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461761
    iget-object v4, v0, LX/9eP;->c:LX/9hP;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461762
    iget-object v4, v0, LX/9eP;->b:LX/9hP;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461763
    new-instance v5, LX/9hP;

    invoke-direct {v5}, LX/9hP;-><init>()V

    .line 461764
    iget-object v6, v0, LX/9eP;->b:LX/9hP;

    iget-object v7, v0, LX/9eP;->c:LX/9hP;

    iget-object v4, v0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    if-eqz v4, :cond_5

    iget-object v4, v0, LX/9eP;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    :goto_3
    invoke-static {v6, v7, v4, v5}, LX/9eP;->a(LX/9hP;LX/9hP;FLX/9hP;)V

    .line 461765
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 461766
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 461767
    iget-object v6, v0, LX/9eP;->a:LX/9eQ;

    invoke-virtual {v6, v4}, LX/9eQ;->getLocationOnScreen([I)V

    .line 461768
    new-instance v6, LX/9hP;

    invoke-direct {v6}, LX/9hP;-><init>()V

    .line 461769
    iget-object v7, v6, LX/9hP;->a:Landroid/graphics/Rect;

    iget-object v8, v5, LX/9hP;->a:Landroid/graphics/Rect;

    invoke-virtual {v7, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 461770
    iget-object v7, v6, LX/9hP;->b:Landroid/graphics/Rect;

    iget-object v8, v5, LX/9hP;->b:Landroid/graphics/Rect;

    invoke-virtual {v7, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 461771
    iget-object v7, v6, LX/9hP;->a:Landroid/graphics/Rect;

    aget v8, v4, v10

    aget v9, v4, v11

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->offset(II)V

    .line 461772
    iget-object v7, v6, LX/9hP;->b:Landroid/graphics/Rect;

    aget v8, v4, v10

    aget v4, v4, v11

    invoke-virtual {v7, v8, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 461773
    move-object v4, v6

    .line 461774
    move-object v0, v4

    .line 461775
    :goto_4
    if-nez v0, :cond_3

    .line 461776
    invoke-static {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->b$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V

    goto/16 :goto_1

    .line 461777
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    aget-object v4, v3, v2

    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;

    move-result-object v0

    goto :goto_4

    .line 461778
    :cond_3
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461779
    iget-object v5, v4, LX/9eQ;->c:LX/9eP;

    move-object v4, v5

    .line 461780
    aget-object v3, v3, v2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->U:LX/9eF;

    invoke-virtual {v4, v3, v0, v1, v2}, LX/9eP;->a(Landroid/graphics/drawable/Drawable;LX/9hP;LX/9hP;LX/9eF;)V

    .line 461781
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r:LX/4mV;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    .line 461782
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 461783
    invoke-virtual {v0, p1}, LX/4mU;->e(F)V

    .line 461784
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 461785
    :cond_5
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;[Landroid/graphics/drawable/Drawable;[LX/9hP;)Z
    .locals 4
    .param p0    # Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # [Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 461786
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 461787
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->J:LX/9hN;

    if-nez v2, :cond_1

    .line 461788
    :cond_0
    :goto_1
    return v0

    .line 461789
    :cond_1
    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->J:LX/9hN;

    invoke-interface {v2, v1}, LX/9hN;->a(Ljava/lang/String;)LX/9hO;

    move-result-object v1

    .line 461790
    if-eqz v1, :cond_0

    iget-object v2, v1, LX/9hO;->a:LX/1bf;

    if-eqz v2, :cond_0

    .line 461791
    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s:LX/75D;

    iget-object v3, v1, LX/9hO;->a:LX/1bf;

    invoke-virtual {v2, v3}, LX/75D;->a(LX/1bf;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 461792
    if-eqz v2, :cond_0

    .line 461793
    if-eqz p1, :cond_2

    .line 461794
    aput-object v2, p1, v0

    .line 461795
    :cond_2
    if-eqz p2, :cond_3

    .line 461796
    iget-object v1, v1, LX/9hO;->b:LX/9hP;

    aput-object v1, p2, v0

    .line 461797
    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V
    .locals 10

    .prologue
    const-wide/16 v2, 0xfa

    .line 461798
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->M:LX/31M;

    const/4 v6, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 461799
    sget-object v1, LX/9eE;->a:[I

    invoke-virtual {v0}, LX/31M;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 461800
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    .line 461801
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-string v5, "translationY"

    new-array v6, v6, [F

    aput v7, v6, v8

    int-to-float v1, v1

    aput v1, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 461802
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    .line 461803
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 461804
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->U:LX/9eF;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 461805
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 461806
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r:LX/4mV;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    .line 461807
    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 461808
    invoke-virtual {v0, p1}, LX/4mU;->e(F)V

    .line 461809
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    .line 461810
    return-void

    .line 461811
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v1

    .line 461812
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-string v5, "translationX"

    new-array v6, v6, [F

    aput v7, v6, v8

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    goto :goto_0

    .line 461813
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v1

    .line 461814
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-string v5, "translationX"

    new-array v6, v6, [F

    aput v7, v6, v8

    int-to-float v1, v1

    aput v1, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    goto :goto_0

    .line 461815
    :pswitch_2
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    .line 461816
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-string v5, "translationY"

    new-array v6, v6, [F

    aput v7, v6, v8

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 461817
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 461818
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->C:Ljava/lang/Throwable;

    .line 461819
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->SWIPING_FRAME:LX/9eK;

    if-ne v0, v1, :cond_1

    .line 461820
    if-eqz p1, :cond_0

    .line 461821
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 461822
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 461823
    :cond_0
    sget-object v0, LX/9eK;->NORMAL:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461824
    :goto_0
    return-void

    .line 461825
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/9eQ;->setVisibility(I)V

    .line 461826
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->removeView(Landroid/view/View;)V

    .line 461827
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->D:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 461828
    if-eqz p1, :cond_2

    .line 461829
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0}, LX/998;->g()Z

    .line 461830
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 461831
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->R:LX/8Hs;

    invoke-virtual {v0, v3}, LX/8Hs;->a(Z)V

    .line 461832
    sget-object v0, LX/9eK;->NORMAL:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 461878
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v3, LX/9eK;->INIT:LX/9eK;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 461879
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->J:LX/9hN;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    .line 461880
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n()V

    .line 461881
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 461882
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 461883
    goto :goto_0

    .line 461884
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->J:LX/9hN;

    iget-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v3, v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, LX/9hN;->a(Ljava/lang/String;)LX/9hO;

    move-result-object v0

    .line 461885
    if-eqz v0, :cond_3

    iget-object v3, v0, LX/9hO;->a:LX/1bf;

    if-nez v3, :cond_4

    .line 461886
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n()V

    .line 461887
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    goto :goto_1

    .line 461888
    :cond_4
    iget-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s:LX/75D;

    iget-object v4, v0, LX/9hO;->a:LX/1bf;

    invoke-virtual {v3, v4}, LX/75D;->a(LX/1bf;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 461889
    if-nez v3, :cond_5

    .line 461890
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n()V

    .line 461891
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    goto :goto_1

    .line 461892
    :cond_5
    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;

    move-result-object v4

    .line 461893
    if-nez v4, :cond_6

    .line 461894
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n()V

    .line 461895
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    goto :goto_1

    .line 461896
    :cond_6
    sget-object v1, LX/9eK;->ANIMATE_IN:LX/9eK;

    iput-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461897
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v1, v2}, LX/9eQ;->setVisibility(I)V

    .line 461898
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461899
    iget-object v2, v1, LX/9eQ;->c:LX/9eP;

    move-object v1, v2

    .line 461900
    iget-object v0, v0, LX/9hO;->b:LX/9hP;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->T:LX/9eF;

    invoke-virtual {v1, v3, v0, v4, v2}, LX/9eP;->a(Landroid/graphics/drawable/Drawable;LX/9hP;LX/9hP;LX/9eF;)V

    .line 461901
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 461902
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r:LX/4mV;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    .line 461903
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 461904
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->e(F)V

    .line 461905
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    goto/16 :goto_1
.end method

.method private n()V
    .locals 10

    .prologue
    const-wide/16 v8, 0xfa

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 461833
    sget-object v0, LX/31M;->DOWN:LX/31M;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->M:LX/31M;

    .line 461834
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->M:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->setDirectionFlags(I)V

    .line 461835
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->o(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461836
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->W:Z

    if-eqz v0, :cond_1

    .line 461837
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s()V

    .line 461838
    :cond_1
    sget-object v0, LX/9eK;->ANIMATE_IN:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461839
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461840
    iput-boolean v5, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461841
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 461842
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-string v2, "translationY"

    new-array v3, v6, [F

    int-to-float v0, v0

    aput v0, v3, v5

    const/4 v0, 0x1

    aput v4, v3, v0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    .line 461843
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 461844
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    new-instance v1, LX/9eA;

    invoke-direct {v1, p0}, LX/9eA;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 461845
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 461846
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 461847
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r:LX/4mV;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    .line 461848
    invoke-virtual {v0, v8, v9}, LX/4mU;->a(J)V

    .line 461849
    invoke-virtual {v0, v4}, LX/4mU;->e(F)V

    .line 461850
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/4mU;->f(F)V

    .line 461851
    return-void
.end method

.method public static o(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 4

    .prologue
    .line 461906
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461907
    :goto_0
    return-void

    .line 461908
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 461909
    iget-boolean v2, v0, LX/23g;->h:Z

    if-nez v2, :cond_1

    .line 461910
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1ac1

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 461911
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 461912
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 461913
    iget-boolean v2, v0, LX/23g;->h:Z

    if-nez v2, :cond_2

    .line 461914
    :goto_2
    goto :goto_0

    .line 461915
    :cond_1
    iget-object v2, v0, LX/23g;->b:LX/0id;

    invoke-virtual {v2, v1}, LX/0id;->o(Ljava/lang/String;)V

    .line 461916
    iget-object v2, v0, LX/23g;->c:LX/23h;

    const-string v3, "Fragment"

    invoke-virtual {v2, v3}, LX/23h;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 461917
    :cond_2
    iget-object v2, v0, LX/23g;->b:LX/0id;

    invoke-virtual {v2, v1}, LX/0id;->p(Ljava/lang/String;)V

    .line 461918
    iget-object v2, v0, LX/23g;->c:LX/23h;

    const-string v3, "Fragment"

    invoke-virtual {v2, v3}, LX/23h;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private p()LX/9eB;
    .locals 1

    .prologue
    .line 461877
    new-instance v0, LX/9eB;

    invoke-direct {v0, p0}, LX/9eB;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    return-object v0
.end method

.method public static r(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 461867
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-eqz v0, :cond_0

    .line 461868
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 461869
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 461870
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0, v4}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/9eC;)Z

    .line 461871
    iput-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461872
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461873
    iget-object v1, v0, LX/9eQ;->c:LX/9eP;

    move-object v0, v1

    .line 461874
    invoke-virtual {v0}, LX/9eP;->a()V

    .line 461875
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461876
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 461862
    sget-object v1, LX/9eK;->NORMAL:LX/9eK;

    iput-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461863
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 461864
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-boolean v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->P:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 461865
    :cond_0
    iput-boolean v0, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461866
    return-void
.end method

.method public static t(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 1

    .prologue
    .line 461856
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 461857
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 461858
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 461859
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 461860
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    .line 461861
    :cond_0
    return-void
.end method

.method public static u(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 5

    .prologue
    .line 461671
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    const/4 v4, 0x1

    .line 461672
    iget-boolean v1, v0, LX/23g;->h:Z

    if-nez v1, :cond_2

    .line 461673
    :goto_0
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->o(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461674
    sget-object v0, LX/9eK;->ANIMATE_WAIT:LX/9eK;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 461675
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->W:Z

    if-eqz v0, :cond_1

    .line 461676
    :cond_0
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461677
    :cond_1
    return-void

    .line 461678
    :cond_2
    iput-boolean v4, v0, LX/23g;->i:Z

    .line 461679
    iget-object v1, v0, LX/23g;->b:LX/0id;

    .line 461680
    const v2, 0x4c000a

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 461681
    iget-object v1, v0, LX/23g;->c:LX/23h;

    const-string v2, "Animation"

    invoke-virtual {v1, v2}, LX/23h;->d(Ljava/lang/String;)V

    .line 461682
    iget-object v1, v0, LX/23g;->g:LX/9eT;

    iget-boolean v1, v1, LX/9eT;->d:Z

    if-eqz v1, :cond_3

    .line 461683
    iget-boolean v1, v0, LX/23g;->j:Z

    if-nez v1, :cond_4

    .line 461684
    iget-object v1, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, v0, LX/23g;->g:LX/9eT;

    iget v2, v2, LX/9eT;->e:I

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(I)V

    .line 461685
    invoke-static {v0}, LX/23g;->h(LX/23g;)V

    .line 461686
    :cond_3
    :goto_1
    invoke-static {v0}, LX/23g;->k(LX/23g;)V

    goto :goto_0

    .line 461687
    :cond_4
    iget-object v1, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, v0, LX/23g;->g:LX/9eT;

    iget v2, v2, LX/9eT;->e:I

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 461688
    iget-object v1, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, v0, LX/23g;->g:LX/9eT;

    iget v2, v2, LX/9eT;->f:I

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 461689
    goto :goto_1
.end method

.method public static v(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 2

    .prologue
    .line 461852
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s()V

    .line 461853
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/9eQ;->setVisibility(I)V

    .line 461854
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 461855
    return-void
.end method

.method public static x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I
    .locals 1

    .prologue
    .line 461670
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->c:I

    goto :goto_0
.end method

.method private z()Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 461667
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 461668
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v1, v0}, LX/9eQ;->getLocationOnScreen([I)V

    .line 461669
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    aget v3, v0, v6

    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v4}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v4}, LX/9eQ;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v5}, LX/9eQ;->getMeasuredHeight()I

    move-result v5

    aget v0, v0, v6

    add-int/2addr v0, v5

    iget-object v5, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v5}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->getScrollY()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 461666
    new-instance v0, LX/9eI;

    invoke-direct {v0, p0}, LX/9eI;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 461665
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 461658
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 461659
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 461660
    if-eqz v1, :cond_0

    .line 461661
    const-string v2, "content_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 461662
    if-eqz v1, :cond_0

    .line 461663
    const-string v2, "content_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461664
    :cond_0
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x35215f9b    # -7295026.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 461652
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 461653
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461654
    const/16 v0, 0x2b

    const v2, -0x2f21f589

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 461655
    :goto_0
    return-void

    .line 461656
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 461657
    const v0, -0x19927414

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5684cedf

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 461616
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 461617
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v3

    check-cast v3, LX/4mV;

    invoke-static {v0}, LX/75D;->b(LX/0QB;)LX/75D;

    move-result-object v4

    check-cast v4, LX/75D;

    invoke-static {v0}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v5

    check-cast v5, LX/23g;

    const/16 v7, 0x97

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v8

    check-cast v8, LX/1L1;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v10

    check-cast v10, LX/4mV;

    invoke-static {v0}, LX/9eS;->a(LX/0QB;)LX/9eS;

    move-result-object v0

    check-cast v0, LX/9eS;

    iput-object v3, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r:LX/4mV;

    iput-object v4, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s:LX/75D;

    iput-object v5, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    iput-object v7, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    iput-object v9, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x:LX/4mV;

    iput-object v0, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->y:LX/9eS;

    .line 461618
    if-nez p1, :cond_2

    .line 461619
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-eqz v0, :cond_5

    .line 461620
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->k()LX/9eT;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v3, v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->a:Ljava/lang/String;

    if-nez v3, :cond_4

    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 461621
    :goto_1
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 461622
    const-string v5, "EXTRA_LAUNCH_TIMESTAMP"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v10, 0x0

    .line 461623
    iput-object v1, v0, LX/23g;->d:LX/74S;

    .line 461624
    iget-object v7, v0, LX/23g;->d:LX/74S;

    invoke-static {v7}, LX/23g;->a(Ljava/lang/Object;)I

    move-result v7

    iput v7, v0, LX/23g;->f:I

    .line 461625
    const-string v8, "LoadPhotoGalleryWithPhotoFromSource_%s-MediaGallery"

    iget-object v7, v0, LX/23g;->d:LX/74S;

    if-eqz v7, :cond_9

    iget-object v7, v0, LX/23g;->d:LX/74S;

    invoke-virtual {v7}, LX/74S;->name()Ljava/lang/String;

    move-result-object v7

    :goto_2
    invoke-static {v8, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, LX/23g;->e:Ljava/lang/String;

    .line 461626
    iput-object v2, v0, LX/23g;->g:LX/9eT;

    .line 461627
    const/4 v7, 0x1

    iput-boolean v7, v0, LX/23g;->h:Z

    .line 461628
    iput-boolean v10, v0, LX/23g;->i:Z

    .line 461629
    iput-boolean v10, v0, LX/23g;->j:Z

    .line 461630
    iget-object v7, v0, LX/23g;->b:LX/0id;

    iget-object v8, v0, LX/23g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v7, v8, v4, v5}, LX/0id;->a(Ljava/lang/String;J)V

    .line 461631
    iget-object v7, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v8, v0, LX/23g;->g:LX/9eT;

    iget v8, v8, LX/9eT;->c:I

    invoke-interface {v7, v8, v10, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 461632
    iget-object v7, v0, LX/23g;->g:LX/9eT;

    iget-boolean v7, v7, LX/9eT;->d:Z

    if-eqz v7, :cond_1

    .line 461633
    iget-object v7, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v8, v0, LX/23g;->g:LX/9eT;

    iget v8, v8, LX/9eT;->e:I

    invoke-interface {v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(I)V

    .line 461634
    invoke-static {v0}, LX/23g;->h(LX/23g;)V

    .line 461635
    :cond_1
    iget-object v7, v0, LX/23g;->c:LX/23h;

    iget-object v8, v0, LX/23g;->g:LX/9eT;

    iget-object v9, v8, LX/9eT;->a:LX/0Pq;

    if-nez v1, :cond_8

    sget-object v8, LX/74S;->UNKNOWN:LX/74S;

    invoke-virtual {v8}, LX/74S;->name()Ljava/lang/String;

    move-result-object v10

    :goto_3
    move-object v8, v3

    move-wide v11, v4

    invoke-virtual/range {v7 .. v12}, LX/23h;->a(Ljava/lang/String;LX/0Pq;Ljava/lang/String;J)V

    .line 461636
    :cond_2
    :goto_4
    if-eqz p1, :cond_7

    .line 461637
    :goto_5
    const-string v0, "EXTRA_DEFAULT_DISMISS_DIRECTION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/31M;->valueOf(Ljava/lang/String;)LX/31M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->M:LX/31M;

    .line 461638
    const-string v0, "EXTRA_SWIPE_DISMISS_DIRECTION_FLAGS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->N:I

    .line 461639
    const-string v0, "EXTRA_ANALYTICS_TAG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->Q:Ljava/lang/String;

    .line 461640
    const-string v0, "EXTRA_ENABLE_SWIPE_TO_DISMISS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->P:Z

    .line 461641
    const-string v0, "EXTRA_BACKGROUND_COLOR"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->O:I

    .line 461642
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_photo"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 461643
    const v0, 0x7ddb7f1f

    invoke-static {v0, v6}, LX/02F;->f(II)V

    return-void

    .line 461644
    :cond_3
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->d:LX/74S;

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v3, v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->a:Ljava/lang/String;

    goto/16 :goto_1

    .line 461645
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    if-nez v0, :cond_6

    const-string v0, "null"

    move-object v1, v0

    .line 461646
    :goto_6
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "content fragment is null, gallery source is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 461647
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->I:Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;->d:LX/74S;

    iget-object v0, v0, LX/74S;->referrer:Ljava/lang/String;

    move-object v1, v0

    goto :goto_6

    .line 461648
    :cond_7
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 461649
    goto :goto_5

    .line 461650
    :cond_8
    invoke-virtual {v1}, LX/74S;->name()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_3

    .line 461651
    :cond_9
    sget-object v7, LX/74S;->UNKNOWN:LX/74S;

    invoke-virtual {v7}, LX/74S;->name()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x48f9ce6a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461615
    const v1, 0x7f030a80

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1fa9b437

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x138fb2da

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 461601
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_IN:LX/9eK;

    if-ne v0, v2, :cond_0

    .line 461602
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461603
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461604
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->U:LX/9eF;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 461605
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461606
    iget-object v2, v0, LX/9eQ;->c:LX/9eP;

    move-object v0, v2

    .line 461607
    invoke-virtual {v0}, LX/9eP;->a()V

    .line 461608
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v0, v3}, LX/9eQ;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 461609
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461610
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-eqz v0, :cond_2

    .line 461611
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-virtual {v0, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/9eC;)Z

    .line 461612
    :cond_2
    iput-object v3, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461613
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 461614
    const/16 v0, 0x2b

    const v2, -0x58685b5a

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 461597
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 461598
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->L:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 461599
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->L:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 461600
    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16bd665d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461592
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 461593
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    const/4 v2, 0x0

    .line 461594
    iput-boolean v2, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461595
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e()V

    .line 461596
    const/16 v1, 0x2b

    const v2, 0x22f6c583

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x717db4b6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 461537
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 461538
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d1ac1

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461539
    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v3, LX/9eK;->NORMAL:LX/9eK;

    if-ne v2, v3, :cond_0

    .line 461540
    if-nez v0, :cond_1

    .line 461541
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 461542
    :cond_0
    :goto_0
    const v0, 0x540e2e7a

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 461543
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->p()LX/9eB;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/9eB;)V

    .line 461544
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 461545
    iget-object v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->P:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 461546
    :goto_1
    iput-boolean v0, v2, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461547
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 461586
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 461587
    const-string v0, "EXTRA_DEFAULT_DISMISS_DIRECTION"

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->M:LX/31M;

    invoke-virtual {v1}, LX/31M;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461588
    const-string v0, "EXTRA_SWIPE_DISMISS_DIRECTION_FLAGS"

    iget v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461589
    const-string v0, "EXTRA_ANALYTICS_TAG"

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461590
    const-string v0, "EXTRA_BACKGROUND_COLOR"

    iget v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461591
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 461552
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 461553
    const v0, 0x7f0d1abe    # 1.8756E38f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->D:Landroid/widget/FrameLayout;

    .line 461554
    const v0, 0x7f0d1abf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/9eQ;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 461555
    const v0, 0x7f0d1ac1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    .line 461556
    new-instance v0, LX/8Hs;

    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    const-wide/16 v2, 0xc8

    iget-object v5, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x:LX/4mV;

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->R:LX/8Hs;

    .line 461557
    const v0, 0x7f0d1ac0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461558
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461559
    iput-boolean v4, v0, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 461560
    new-instance v0, LX/9eH;

    invoke-direct {v0, p0}, LX/9eH;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461561
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461562
    iput-object v0, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->e:LX/994;

    .line 461563
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461564
    iput-object v0, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->g:LX/992;

    .line 461565
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461566
    iput-object v0, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->f:LX/995;

    .line 461567
    iget-object v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    .line 461568
    iput-object v0, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->h:LX/993;

    .line 461569
    iget-boolean v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->P:Z

    if-eqz v0, :cond_0

    .line 461570
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget v1, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->N:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->setDirectionFlags(I)V

    .line 461571
    :cond_0
    const v0, 0x7f0d05d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    .line 461572
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->O:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 461573
    if-eqz p2, :cond_2

    .line 461574
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 461575
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1ac1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iput-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461576
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-nez v0, :cond_1

    .line 461577
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    const-string v2, "onCreate(): Content fragment when savedInstanceState is non-null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 461578
    :cond_1
    :goto_0
    return-void

    .line 461579
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->p()LX/9eB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/9eB;)V

    .line 461580
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 461581
    new-instance v1, LX/9eC;

    invoke-direct {v1, p0}, LX/9eC;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    move-object v1, v1

    .line 461582
    invoke-virtual {v0, v1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/9eC;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 461583
    invoke-static {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->o(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 461584
    :goto_1
    goto :goto_0

    .line 461585
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v0}, LX/9eQ;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/9eD;

    invoke-direct {v1, p0}, LX/9eD;-><init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4dd10d67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 461548
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 461549
    if-eqz p1, :cond_0

    .line 461550
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->s()V

    .line 461551
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x25c9f9d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
