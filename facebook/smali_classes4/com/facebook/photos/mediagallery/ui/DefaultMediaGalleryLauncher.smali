.class public Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/23R;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/23T;

.field private final c:LX/23g;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 364391
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    const-string v1, "photo_viewer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/23T;LX/23g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364388
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->b:LX/23T;

    .line 364389
    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->c:LX/23g;

    .line 364390
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;
    .locals 5

    .prologue
    .line 364376
    const-class v1, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    monitor-enter v1

    .line 364377
    :try_start_0
    sget-object v0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 364378
    sput-object v2, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 364379
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364380
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 364381
    new-instance p0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    invoke-static {v0}, LX/23T;->b(LX/0QB;)LX/23T;

    move-result-object v3

    check-cast v3, LX/23T;

    invoke-static {v0}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v4

    check-cast v4, LX/23g;

    invoke-direct {p0, v3, v4}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;-><init>(LX/23T;LX/23g;)V

    .line 364382
    move-object v0, p0

    .line 364383
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 364384
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364385
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 364386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V
    .locals 8
    .param p3    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364373
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 364374
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v0

    move-object v7, v6

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    .line 364375
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V
    .locals 6
    .param p3    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/9hK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364362
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->b:LX/23T;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->c:LX/23g;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, p2

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/23T;LX/23g;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    move-result-object v0

    .line 364363
    new-instance v1, LX/9eM;

    invoke-direct {v1, p2}, LX/9eM;-><init>(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;)V

    sget-object v2, LX/31M;->UP:LX/31M;

    invoke-virtual {v1, v2}, LX/9eM;->a(LX/31M;)LX/9eM;

    move-result-object v1

    sget-object v2, LX/31M;->UP:LX/31M;

    invoke-virtual {v2}, LX/31M;->flag()I

    move-result v2

    sget-object v3, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v3}, LX/31M;->flag()I

    move-result v3

    or-int/2addr v2, v3

    .line 364364
    iput v2, v1, LX/9eM;->f:I

    .line 364365
    move-object v1, v1

    .line 364366
    const/high16 v2, -0x1000000

    .line 364367
    iput v2, v1, LX/9eM;->g:I

    .line 364368
    move-object v1, v1

    .line 364369
    invoke-virtual {v1}, LX/9eM;->a()Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    move-result-object v1

    .line 364370
    invoke-static {p1, v0, v1, p3, p4}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a(Landroid/content/Context;Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 364371
    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 364372
    :cond_0
    return-void
.end method
