.class public Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;
.super Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/23S;


# static fields
.field public static final F:Ljava/lang/String;

.field private static final G:Landroid/widget/ImageView$ScaleType;


# instance fields
.field public A:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1BA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

.field public I:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Landroid/view/ViewGroup;

.field private K:Landroid/view/ViewStub;

.field private L:Landroid/view/View;

.field private M:Landroid/view/ViewStub;

.field public N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

.field public O:Landroid/support/v4/view/ViewPager;

.field public P:LX/CaZ;

.field public Q:LX/9g2;

.field private R:J

.field public S:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;>;"
        }
    .end annotation
.end field

.field public T:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public U:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/dialog/PhotoAnimationContentFragment$EmptySetListener;",
            ">;"
        }
    .end annotation
.end field

.field private V:LX/8Hs;

.field public W:LX/9eC;

.field public X:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Z

.field private final Z:LX/CaR;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8JL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final aa:LX/CaS;

.field public ab:Z

.field private ac:Z

.field private ad:Z

.field public ae:Z

.field public af:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:LX/9hK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final ah:Landroid/view/View$OnClickListener;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23T;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CaK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/745;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Cbe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/CcX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/CcS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/CbH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cao;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9iC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/3E1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/459;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/8Hb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/5vm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/7Dh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 364625
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->F:Ljava/lang/String;

    .line 364626
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->G:Landroid/widget/ImageView$ScaleType;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 364627
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;-><init>()V

    .line 364628
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    .line 364629
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    .line 364630
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->U:LX/0am;

    .line 364631
    new-instance v0, LX/CaR;

    invoke-direct {v0, p0}, LX/CaR;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    .line 364632
    new-instance v0, LX/CaS;

    invoke-direct {v0, p0}, LX/CaS;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->aa:LX/CaS;

    .line 364633
    new-instance v0, LX/CaQ;

    invoke-direct {v0, p0}, LX/CaQ;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ah:Landroid/view/View$OnClickListener;

    .line 364634
    return-void
.end method

.method private A()Z
    .locals 3

    .prologue
    .line 364635
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z:LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z:LX/7Dh;

    const/4 v1, 0x0

    .line 364636
    iget-object v2, v0, LX/7Dh;->f:LX/0Uh;

    sget p0, LX/7Dh;->c:I

    invoke-virtual {v2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/7Dh;->e:LX/0ad;

    sget-short p0, LX/7Dg;->a:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    move v0, v1

    .line 364637
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static B(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)LX/8Hs;
    .locals 6

    .prologue
    .line 364638
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->V:LX/8Hs;

    if-nez v0, :cond_0

    .line 364639
    new-instance v0, LX/8Hs;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    const-wide/16 v2, 0xc8

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->i:LX/4mV;

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->V:LX/8Hs;

    .line 364640
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->V:LX/8Hs;

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/23T;LX/23g;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;
    .locals 4
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/9hK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364641
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;-><init>()V

    .line 364642
    iput-object p4, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 364643
    iput-object p5, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ag:LX/9hK;

    .line 364644
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->c:LX/0Px;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    .line 364645
    iput-object p3, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->I:Lcom/facebook/common/callercontext/CallerContext;

    .line 364646
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-virtual {p1, v1, p3}, LX/23T;->a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364647
    invoke-virtual {p2}, LX/23g;->a()V

    .line 364648
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->d:LX/9g5;

    if-eqz v1, :cond_0

    .line 364649
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->d:LX/9g5;

    invoke-virtual {v1, v2}, LX/9g2;->a(LX/9g5;)V

    .line 364650
    :cond_0
    iget v2, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 364651
    const/16 v2, 0x80

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 364652
    iget-object v2, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/9g2;->a(ILX/0am;)V

    .line 364653
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    .line 364654
    new-instance v1, LX/9hD;

    invoke-direct {v1, p0}, LX/9hD;-><init>(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;)V

    .line 364655
    const/4 v2, 0x0

    iput-object v2, v1, LX/9hD;->c:LX/0Px;

    .line 364656
    move-object v1, v1

    .line 364657
    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 364658
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 364659
    const-string v3, "EXTRA_LAUNCH_PARAM"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 364660
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 364661
    return-object v0
.end method

.method private static a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;LX/0Ot;LX/0Ot;LX/CaK;LX/2SI;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Ot;LX/23g;LX/745;LX/4mV;LX/Cbe;LX/CcX;LX/CcS;LX/CbH;LX/0Ot;LX/0Ot;LX/1My;LX/0SG;LX/3E1;LX/459;Ljava/util/Set;LX/1L1;LX/0Or;LX/0Ot;LX/0Ot;LX/5vm;LX/7Dh;LX/03V;LX/0if;LX/1BA;LX/0TD;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;",
            "LX/0Ot",
            "<",
            "LX/8JL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/23T;",
            ">;",
            "LX/CaK;",
            "LX/2SI;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/23g;",
            "LX/745;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            "LX/Cbe;",
            "LX/CcX;",
            "LX/CcS;",
            "LX/CbH;",
            "LX/0Ot",
            "<",
            "LX/Cao;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9iC;",
            ">;",
            "LX/1My;",
            "LX/0SG;",
            "LX/3E1;",
            "LX/459;",
            "Ljava/util/Set",
            "<",
            "LX/8Hb;",
            ">;",
            "LX/1L1;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;",
            "LX/5vm;",
            "LX/7Dh;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 364662
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c:LX/CaK;

    iput-object p4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->d:LX/2SI;

    iput-object p5, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->e:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p6, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    iput-object p8, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->h:LX/745;

    iput-object p9, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->i:LX/4mV;

    iput-object p10, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->j:LX/Cbe;

    iput-object p11, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    iput-object p12, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    iput-object p13, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    iput-object p14, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q:LX/0SG;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->r:LX/3E1;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->s:LX/459;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->t:Ljava/util/Set;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->u:LX/1L1;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->v:LX/0Or;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->w:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->x:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y:LX/5vm;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z:LX/7Dh;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->A:LX/03V;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->C:LX/1BA;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->D:LX/0TD;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->E:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 364663
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->L:Landroid/view/View;

    if-nez v1, :cond_2

    .line 364664
    if-eqz p1, :cond_1

    .line 364665
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->K:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->L:Landroid/view/View;

    .line 364666
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->L:Landroid/view/View;

    .line 364667
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 364668
    :cond_1
    return-void

    .line 364669
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->L:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 34

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v33

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    const/16 v3, 0x2ec4

    move-object/from16 v0, v33

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xf2d

    move-object/from16 v0, v33

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {v33 .. v33}, LX/CaK;->a(LX/0QB;)LX/CaK;

    move-result-object v5

    check-cast v5, LX/CaK;

    invoke-static/range {v33 .. v33}, LX/2SI;->a(LX/0QB;)LX/2SI;

    move-result-object v6

    check-cast v6, LX/2SI;

    invoke-static/range {v33 .. v33}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v7

    check-cast v7, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const/16 v8, 0x97

    move-object/from16 v0, v33

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v33 .. v33}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v9

    check-cast v9, LX/23g;

    invoke-static/range {v33 .. v33}, LX/745;->a(LX/0QB;)LX/745;

    move-result-object v10

    check-cast v10, LX/745;

    invoke-static/range {v33 .. v33}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v11

    check-cast v11, LX/4mV;

    invoke-static/range {v33 .. v33}, LX/Cbe;->a(LX/0QB;)LX/Cbe;

    move-result-object v12

    check-cast v12, LX/Cbe;

    invoke-static/range {v33 .. v33}, LX/CcX;->a(LX/0QB;)LX/CcX;

    move-result-object v13

    check-cast v13, LX/CcX;

    invoke-static/range {v33 .. v33}, LX/CcS;->a(LX/0QB;)LX/CcS;

    move-result-object v14

    check-cast v14, LX/CcS;

    invoke-static/range {v33 .. v33}, LX/CbH;->a(LX/0QB;)LX/CbH;

    move-result-object v15

    check-cast v15, LX/CbH;

    const/16 v16, 0x2e73

    move-object/from16 v0, v33

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x2e85

    move-object/from16 v0, v33

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v33 .. v33}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v18

    check-cast v18, LX/1My;

    invoke-static/range {v33 .. v33}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v19

    check-cast v19, LX/0SG;

    invoke-static/range {v33 .. v33}, LX/3E1;->a(LX/0QB;)LX/3E1;

    move-result-object v20

    check-cast v20, LX/3E1;

    invoke-static/range {v33 .. v33}, LX/459;->a(LX/0QB;)LX/459;

    move-result-object v21

    check-cast v21, LX/459;

    invoke-static/range {v33 .. v33}, LX/8Hj;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v22

    invoke-static/range {v33 .. v33}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v23

    check-cast v23, LX/1L1;

    const/16 v24, 0x455

    move-object/from16 v0, v33

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    const/16 v25, 0x2f25

    move-object/from16 v0, v33

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0xf4d

    move-object/from16 v0, v33

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {v33 .. v33}, LX/5vm;->a(LX/0QB;)LX/5vm;

    move-result-object v27

    check-cast v27, LX/5vm;

    invoke-static/range {v33 .. v33}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v28

    check-cast v28, LX/7Dh;

    invoke-static/range {v33 .. v33}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v29

    check-cast v29, LX/03V;

    invoke-static/range {v33 .. v33}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v30

    check-cast v30, LX/0if;

    invoke-static/range {v33 .. v33}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v31

    check-cast v31, LX/1BA;

    invoke-static/range {v33 .. v33}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v32

    check-cast v32, LX/0TD;

    invoke-static/range {v33 .. v33}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v33

    check-cast v33, Ljava/util/concurrent/Executor;

    invoke-static/range {v2 .. v33}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;LX/0Ot;LX/0Ot;LX/CaK;LX/2SI;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Ot;LX/23g;LX/745;LX/4mV;LX/Cbe;LX/CcX;LX/CcS;LX/CbH;LX/0Ot;LX/0Ot;LX/1My;LX/0SG;LX/3E1;LX/459;Ljava/util/Set;LX/1L1;LX/0Or;LX/0Ot;LX/0Ot;LX/5vm;LX/7Dh;LX/03V;LX/0if;LX/1BA;LX/0TD;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 364670
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    invoke-virtual {v0}, LX/23g;->e()V

    .line 364671
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0991

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 364672
    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 364673
    const v1, 0x7f030a83

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 364674
    const v0, 0x7f0d1ada

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->K:Landroid/view/ViewStub;

    .line 364675
    const v0, 0x7f0d1ad9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->M:Landroid/view/ViewStub;

    .line 364676
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    invoke-virtual {v0}, LX/23g;->f()V

    .line 364677
    return-object v1
.end method

.method public static a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 364704
    const-string v1, "photos_view"

    invoke-static {p1, v1, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 364705
    const v0, 0x7f0d1ad8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    .line 364706
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 364707
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->j:LX/Cbe;

    .line 364708
    new-instance v1, LX/CaO;

    invoke-direct {v1, p0}, LX/CaO;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    move-object v1, v1

    .line 364709
    iput-object v1, v0, LX/Cbe;->a:LX/7US;

    .line 364710
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364711
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    .line 364712
    new-instance v1, LX/CaP;

    invoke-direct {v1, p0}, LX/CaP;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    move-object v1, v1

    .line 364713
    iput-object v1, v0, LX/CcX;->a:LX/CaP;

    .line 364714
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->post(Ljava/lang/Runnable;)Z

    .line 364715
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 364678
    invoke-virtual {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;LX/5kD;)V
    .locals 4

    .prologue
    .line 364679
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v0

    .line 364680
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->W:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-object v1, v1

    .line 364681
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 364682
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    if-eqz v0, :cond_1

    .line 364683
    :cond_0
    :goto_0
    return-void

    .line 364684
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Hb;

    .line 364685
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    invoke-virtual {v0, p1, v1, p0, v3}, LX/8Hb;->a(LX/5kD;Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364686
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public static c(LX/5kD;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 364687
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 364444
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    if-nez v0, :cond_2

    const/4 v4, 0x0

    .line 364445
    :goto_0
    new-instance v0, LX/CaZ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c:LX/CaK;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v5

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v6

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->A()Z

    move-result v7

    iget-object v8, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v8, v8, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    iget-object v9, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v9, v9, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v10, v10, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    iget-object v11, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v11, v11, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->D:LX/1Up;

    iget-object v12, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct/range {v0 .. v12}, LX/CaZ;-><init>(LX/0gc;LX/CaK;Ljava/lang/String;Landroid/net/Uri;ZZZILjava/lang/String;Ljava/lang/String;LX/1Up;Lcom/facebook/graphql/model/GraphQLStory;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    .line 364446
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 364447
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->aa:LX/CaS;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 364448
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    .line 364449
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364450
    iget-object v2, v1, LX/9g2;->e:LX/9g6;

    sget-object v3, LX/9g6;->CLOSED:LX/9g6;

    if-eq v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_1
    const-string v3, "Calling method of closed() fetcher"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 364451
    iget-object v2, v1, LX/9g2;->d:LX/0Px;

    move-object v1, v2

    .line 364452
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 364453
    :cond_0
    :goto_2
    move-object v1, v1

    .line 364454
    invoke-virtual {v0, v1}, LX/CaR;->a(LX/0Px;)V

    .line 364455
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364456
    iget-object v2, v1, LX/9g2;->e:LX/9g6;

    move-object v1, v2

    .line 364457
    invoke-virtual {v0, v1}, LX/CaR;->a(LX/9g6;)V

    .line 364458
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    invoke-virtual {v0, v1}, LX/9g2;->a(LX/9g5;)V

    .line 364459
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    invoke-virtual {v0, v13}, LX/23g;->a(Z)V

    .line 364460
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 364461
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ac:Z

    .line 364462
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ad:Z

    if-eqz v0, :cond_1

    .line 364463
    iput-boolean v13, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ad:Z

    .line 364464
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->r()V

    .line 364465
    :cond_1
    return-void

    .line 364466
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    .line 364467
    iget-object v1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v4, v1

    .line 364468
    goto/16 :goto_0

    .line 364469
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->X:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 364470
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->X:LX/0Px;

    goto :goto_2

    .line 364471
    :cond_4
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 364472
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    goto :goto_2

    .line 364473
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private r()V
    .locals 3

    .prologue
    .line 364688
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    invoke-virtual {v0, v1}, LX/9g2;->b(LX/9g5;)V

    .line 364689
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    invoke-virtual {v0}, LX/9g2;->c()V

    .line 364690
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23T;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->I:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/23T;->a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364691
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    invoke-virtual {v0, v1}, LX/9g2;->a(LX/9g5;)V

    .line 364692
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, LX/9g2;->a(I)V

    .line 364693
    return-void

    .line 364694
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    goto :goto_0
.end method

.method private s()LX/5kD;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 364695
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 364696
    :cond_0
    const/4 v0, 0x0

    .line 364697
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v0

    goto :goto_0
.end method

.method public static y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z
    .locals 2

    .prologue
    .line 364698
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    sget-object v1, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    sget-object v1, LX/74S;->REACTION_SHOW_MORE_PHOTOS:LX/74S;

    if-ne v0, v1, :cond_1

    .line 364699
    :cond_0
    const/4 v0, 0x0

    .line 364700
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z
    .locals 1

    .prologue
    .line 364701
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z:LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 364702
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->E:Landroid/widget/ImageView$ScaleType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->E:Landroid/widget/ImageView$ScaleType;

    :goto_0
    invoke-static {p1, p2, v0}, LX/9hP;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->G:Landroid/widget/ImageView$ScaleType;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364703
    const-string v0, "photo_viewer"

    return-object v0
.end method

.method public final a(LX/1L1;ZI)V
    .locals 1

    .prologue
    .line 364621
    new-instance v0, LX/1ep;

    invoke-direct {v0, p2, p3}, LX/1ep;-><init>(ZI)V

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 364622
    return-void
.end method

.method public final a(LX/9eB;)V
    .locals 1

    .prologue
    .line 364623
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->U:LX/0am;

    .line 364624
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 364392
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(Landroid/os/Bundle;)V

    .line 364393
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 364394
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 364395
    const-string v1, "EXTRA_LAUNCH_PARAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    .line 364396
    if-eqz p1, :cond_6

    const-string v0, "MG_EXTRA_START_MEDIA_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 364397
    :goto_0
    if-eqz v0, :cond_7

    :goto_1
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    .line 364398
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iC;

    if-nez p1, :cond_8

    move-object v1, v7

    :goto_2
    invoke-static {v1, v2}, LX/9iC;->a(Ljava/util/ArrayList;Z)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->X:LX/0Px;

    .line 364399
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    if-nez v0, :cond_0

    .line 364400
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    invoke-virtual {v0}, LX/23g;->a()V

    .line 364401
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23T;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->I:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, LX/23T;->a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364402
    if-eqz p1, :cond_9

    .line 364403
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    const-string v1, "MG_EXTRA_NUM_ITEMS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v2, v2, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, LX/9g2;->a(I)V

    .line 364404
    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_1

    .line 364405
    const-string v0, "MG_EXTRA_STORY"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 364406
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 364407
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 364408
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    new-instance v2, LX/CaL;

    invoke-direct {v2, p0}, LX/CaL;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 364409
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    if-nez v0, :cond_4

    .line 364410
    :cond_3
    sget-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->F:Ljava/lang/String;

    const-string v1, "Start media params are incomplete - flickering or other artifacts may occur"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 364411
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->h:LX/745;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/745;->a(Ljava/lang/String;)LX/745;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    invoke-virtual {v0, v1}, LX/745;->a(LX/74S;)LX/745;

    move-result-object v0

    .line 364412
    iput-object v7, v0, LX/745;->h:Ljava/lang/String;

    .line 364413
    move-object v0, v0

    .line 364414
    sget-object v1, LX/74N;->FULL_SCREEN_GALLERY:LX/74N;

    invoke-virtual {v0, v1}, LX/745;->a(LX/74N;)LX/745;

    .line 364415
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->n:Z

    .line 364416
    iget-object v2, v0, LX/CbH;->a:LX/03R;

    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq v2, v3, :cond_5

    iget-object v2, v0, LX/CbH;->a:LX/03R;

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    if-ne v2, v3, :cond_b

    :cond_5
    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 364417
    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    iput-object v2, v0, LX/CbH;->a:LX/03R;

    .line 364418
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 364419
    return-void

    :cond_6
    move-object v0, v7

    .line 364420
    goto/16 :goto_0

    .line 364421
    :cond_7
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    goto/16 :goto_1

    .line 364422
    :cond_8
    const-string v1, "MG_EXTRA_MEDIA"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto/16 :goto_2

    .line 364423
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v1, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_5
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 364424
    const/16 v1, 0x80

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 364425
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    invoke-virtual {v1, v0, v2}, LX/9g2;->a(ILX/0am;)V

    goto/16 :goto_3

    :cond_a
    move v0, v2

    .line 364426
    goto :goto_5

    .line 364427
    :cond_b
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public final a(LX/9eC;)Z
    .locals 1

    .prologue
    .line 364428
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    if-eqz v0, :cond_0

    .line 364429
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->W:LX/9eC;

    .line 364430
    const/4 v0, 0x1

    .line 364431
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 364432
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->s()LX/5kD;

    move-result-object v0

    .line 364433
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 364434
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    if-eqz v0, :cond_0

    .line 364435
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    invoke-virtual {v0}, LX/9g2;->c()V

    .line 364436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 364437
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ab:Z

    .line 364438
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 364439
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364440
    sget-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364441
    invoke-virtual {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 364442
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->s()LX/5kD;

    move-result-object v1

    .line 364443
    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364474
    const-string v0, "photo_viewer"

    return-object v0
.end method

.method public final o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 364475
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->M:Landroid/view/ViewStub;

    if-nez v0, :cond_0

    .line 364476
    const/4 v0, 0x0

    .line 364477
    :goto_0
    return-object v0

    .line 364478
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-nez v0, :cond_2

    .line 364479
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->M:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    .line 364480
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 364481
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    .line 364482
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ag:LX/9hK;

    .line 364483
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aR:LX/9hK;

    .line 364484
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->C:Z

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setShouldShowStoryCaption(Z)V

    .line 364485
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    .line 364486
    iput-boolean v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    .line 364487
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->m:Z

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setShowAttribution(Z)V

    .line 364488
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->i:Ljava/lang/String;

    .line 364489
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->av:Ljava/lang/String;

    .line 364490
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->k:Ljava/lang/String;

    .line 364491
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aw:Ljava/lang/String;

    .line 364492
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->l:Z

    .line 364493
    iput-boolean v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aC:Z

    .line 364494
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->j:Ljava/lang/String;

    .line 364495
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ax:Ljava/lang/String;

    .line 364496
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ah:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setLocationButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364497
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->o:Z

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Z)V

    .line 364498
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    .line 364499
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    .line 364500
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->h:LX/745;

    .line 364501
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aF:LX/745;

    .line 364502
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setTrackingCodes(Ljava/lang/String;)V

    .line 364503
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    const v1, 0x41e065f

    if-ne v0, v1, :cond_1

    .line 364504
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    .line 364505
    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ay:Ljava/lang/String;

    .line 364506
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a()V

    .line 364507
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 364508
    packed-switch p1, :pswitch_data_0

    .line 364509
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 364510
    goto :goto_0

    .line 364511
    :cond_0
    :goto_1
    return-void

    .line 364512
    :pswitch_0
    const/4 v2, -0x1

    if-eq p2, v2, :cond_1

    .line 364513
    :goto_2
    goto :goto_1

    .line 364514
    :pswitch_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 364515
    :goto_3
    goto :goto_1

    .line 364516
    :cond_1
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y:LX/5vm;

    sget-object v3, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v4, "extra_profile_pic_expiration"

    const-wide/16 v6, 0x0

    invoke-virtual {p3, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v6, "staging_ground_photo_caption"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "existing"

    invoke-virtual/range {v2 .. v7}, LX/5vm;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 364517
    const-string v2, "force_create_new_activity"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 364518
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->v:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    .line 364519
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1389
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 364520
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 364521
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ac:Z

    if-eqz v0, :cond_0

    .line 364522
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->r()V

    .line 364523
    :goto_0
    return-void

    .line 364524
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ad:Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x30636ff9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 364525
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    if-nez v0, :cond_0

    .line 364526
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    .line 364527
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    const v2, 0x645da85a

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 364528
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    .line 364529
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->D:LX/0TD;

    new-instance v2, LX/CaM;

    invoke-direct {v2, p0, p1, p2}, LX/CaM;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    invoke-interface {v0, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 364530
    new-instance v2, LX/CaN;

    invoke-direct {v2, p0}, LX/CaN;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->E:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 364531
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    new-instance v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$4;

    invoke-direct {v2, p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$4;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    invoke-static {v0, v2}, LX/8He;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d659a01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364532
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onDestroy()V

    .line 364533
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 364534
    invoke-virtual {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 364535
    const/16 v1, 0x2b

    const v2, 0x4676697b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4cbe1b14

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 364536
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onDestroyView()V

    .line 364537
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 364538
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 364539
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 364540
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c:LX/CaK;

    .line 364541
    iget-object v2, v0, LX/CaK;->c:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 364542
    iget-object v2, v0, LX/CaK;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 364543
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 364544
    iput-object v2, v0, LX/CaK;->b:LX/0Px;

    .line 364545
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    if-eqz v0, :cond_1

    .line 364546
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Z:LX/CaR;

    invoke-virtual {v0, v2}, LX/9g2;->b(LX/9g5;)V

    .line 364547
    :cond_1
    iput-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->M:Landroid/view/ViewStub;

    .line 364548
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-eqz v0, :cond_2

    .line 364549
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b()V

    .line 364550
    iput-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    .line 364551
    :cond_2
    iput-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    .line 364552
    iput-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->L:Landroid/view/View;

    .line 364553
    iput-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    .line 364554
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    .line 364555
    iget-boolean v2, v0, LX/23g;->h:Z

    if-nez v2, :cond_5

    .line 364556
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->j:LX/Cbe;

    .line 364557
    iput-object v3, v0, LX/Cbe;->a:LX/7US;

    .line 364558
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364559
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    .line 364560
    iput-object v3, v0, LX/CcX;->a:LX/CaP;

    .line 364561
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->u:LX/1L1;

    new-instance v2, LX/1ep;

    const/4 v3, 0x0

    .line 364562
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    if-nez v4, :cond_6

    const/4 v4, -0x1

    :goto_1
    move v4, v4

    .line 364563
    invoke-direct {v2, v3, v4}, LX/1ep;-><init>(ZI)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 364564
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Hb;

    .line 364565
    invoke-virtual {v0}, LX/8Hb;->b()V

    goto :goto_2

    .line 364566
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v2, LX/0ig;->x:LX/0ih;

    invoke-virtual {v0, v2}, LX/0if;->c(LX/0ih;)V

    .line 364567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ab:Z

    .line 364568
    const v0, -0x5088a199

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 364569
    :cond_5
    iget-object v2, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, v0, LX/23g;->g:LX/9eT;

    iget v4, v4, LX/9eT;->c:I

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 364570
    iget-object v2, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, v0, LX/23g;->g:LX/9eT;

    iget v4, v4, LX/9eT;->e:I

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 364571
    iget-object v2, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x14000b

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 364572
    invoke-static {v0}, LX/23g;->j(LX/23g;)V

    .line 364573
    iget-object v2, v0, LX/23g;->b:LX/0id;

    invoke-virtual {v2}, LX/0id;->a()V

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v4, v4, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    goto :goto_1
.end method

.method public final onPause()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, -0x75141cd9

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 364574
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onPause()V

    .line 364575
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 364576
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->R:J

    sub-long/2addr v4, v6

    .line 364577
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    .line 364578
    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->r:LX/3E1;

    const-string v6, "photo_gallery"

    .line 364579
    if-nez v0, :cond_1

    .line 364580
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->d:LX/2SI;

    .line 364581
    iput-object v1, v0, LX/2SI;->j:LX/23S;

    .line 364582
    const v0, 0x39d42e1

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    :cond_0
    move-object v0, v1

    .line 364583
    goto :goto_0

    .line 364584
    :cond_1
    const/4 v7, 0x0

    .line 364585
    :try_start_0
    iget-object v8, v3, LX/3E1;->d:LX/0lB;

    invoke-virtual {v8, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 364586
    :goto_2
    invoke-virtual {v3, v7, v4, v5, v6}, LX/3E1;->a(LX/0lF;JLjava/lang/String;)V

    goto :goto_1

    .line 364587
    :catch_0
    move-exception v8

    .line 364588
    iget-object v9, v3, LX/3E1;->e:LX/03V;

    const-string v10, "VpvEventHelper"

    const-string v11, "parse trackingCodes error"

    invoke-static {v10, v11}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v10

    .line 364589
    iput-object v8, v10, LX/0VK;->c:Ljava/lang/Throwable;

    .line 364590
    move-object v8, v10

    .line 364591
    invoke-virtual {v8}, LX/0VK;->g()LX/0VG;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/03V;->a(LX/0VG;)V

    goto :goto_2
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 364592
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->s:LX/459;

    invoke-virtual {v0}, LX/459;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364593
    :cond_0
    :goto_0
    return-void

    .line 364594
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v0

    .line 364595
    if-eqz v0, :cond_0

    .line 364596
    sget-object v1, LX/74P;->HARDWARE_CLICK:LX/74P;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/74P;LX/0am;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x316dc87e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364597
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onResume()V

    .line 364598
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->p:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 364599
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->r:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->a()V

    .line 364600
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->R:J

    .line 364601
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->d:LX/2SI;

    .line 364602
    iput-object p0, v1, LX/2SI;->j:LX/23S;

    .line 364603
    const/16 v1, 0x2b

    const v2, -0x48b7e8ec

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 364604
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iC;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c:LX/CaK;

    .line 364605
    iget-object v0, v1, LX/CaK;->b:LX/0Px;

    move-object v1, v0

    .line 364606
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/9iC;->a(LX/0Px;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 364607
    const-string v1, "MG_EXTRA_MEDIA"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 364608
    const-string v1, "MG_EXTRA_NUM_ITEMS"

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 364609
    const-string v0, "MG_EXTRA_START_MEDIA_ID"

    invoke-virtual {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 364610
    const-string v0, "MG_EXTRA_STORY"

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 364611
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 364612
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 364613
    invoke-super {p0, p1, p2}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 364614
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    if-nez v0, :cond_0

    .line 364615
    invoke-static {p0, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/View;)V

    .line 364616
    :cond_0
    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3ba2c8e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 364617
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 364618
    iget-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Y:Z

    if-nez v1, :cond_0

    .line 364619
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    .line 364620
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x430f953d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
