.class public Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0po;
.implements LX/1Iq;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile E:Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

.field public static final a:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final c:Ljava/lang/String;

.field private static final d:LX/0Tn;

.field private static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;

.field private static final i:LX/0Tn;

.field private static final j:LX/0Tn;

.field private static final k:LX/0Tn;

.field private static final l:LX/0Tn;

.field private static final m:LX/0Tn;

.field private static final n:LX/0Tn;

.field private static final o:LX/0Tn;

.field private static final p:LX/0Tn;

.field private static final q:LX/0Tn;

.field private static final r:LX/0Tn;

.field private static final s:LX/0Tn;

.field private static final t:LX/0Tn;

.field private static final u:LX/0Tn;

.field private static final v:LX/0Tn;

.field private static final w:LX/0Tn;

.field private static final x:LX/0Tn;


# instance fields
.field private final A:LX/0SI;

.field private final B:LX/0SG;

.field private final C:LX/1Ao;

.field private final D:Ljava/util/Random;

.field private final y:LX/0Zb;

.field private final z:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 490313
    const-class v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->c:Ljava/lang/String;

    .line 490314
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a:J

    .line 490315
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "photos_eviction"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 490316
    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->d:LX/0Tn;

    const-string v1, "tracking_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 490317
    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "cache_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    .line 490318
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "size_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->f:LX/0Tn;

    .line 490319
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "logout_detected"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->g:LX/0Tn;

    .line 490320
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "trim_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->h:LX/0Tn;

    .line 490321
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "min_trim_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->i:LX/0Tn;

    .line 490322
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_calling_class"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->j:LX/0Tn;

    .line 490323
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_analytics_tag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->k:LX/0Tn;

    .line 490324
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_is_prefetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->l:LX/0Tn;

    .line 490325
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_cancel_req"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->m:LX/0Tn;

    .line 490326
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_user_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->n:LX/0Tn;

    .line 490327
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "o_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    .line 490328
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->p:LX/0Tn;

    .line 490329
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_calling_class"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->q:LX/0Tn;

    .line 490330
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_analytics_tag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->r:LX/0Tn;

    .line 490331
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_is_prefetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->s:LX/0Tn;

    .line 490332
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_cancel_req"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->t:LX/0Tn;

    .line 490333
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_user_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->u:LX/0Tn;

    .line 490334
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "r_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    .line 490335
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "total_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->w:LX/0Tn;

    .line 490336
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    const-string v1, "total_requests"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->x:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SI;LX/0SG;LX/1Ao;Ljava/util/Random;LX/0pq;)V
    .locals 0
    .param p6    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490209
    iput-object p1, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->y:LX/0Zb;

    .line 490210
    iput-object p2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 490211
    iput-object p3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->A:LX/0SI;

    .line 490212
    iput-object p4, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    .line 490213
    iput-object p5, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->C:LX/1Ao;

    .line 490214
    iput-object p6, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->D:Ljava/util/Random;

    .line 490215
    invoke-virtual {p7, p0}, LX/0pq;->a(LX/0po;)V

    .line 490216
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;
    .locals 11

    .prologue
    .line 490300
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->E:Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    if-nez v0, :cond_1

    .line 490301
    const-class v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    monitor-enter v1

    .line 490302
    :try_start_0
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->E:Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 490303
    if-eqz v2, :cond_0

    .line 490304
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 490305
    new-instance v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v6

    check-cast v6, LX/0SI;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/1An;->a(LX/0QB;)LX/1An;

    move-result-object v8

    check-cast v8, LX/1Ao;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v9

    check-cast v9, Ljava/util/Random;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v10

    check-cast v10, LX/0pq;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;-><init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SI;LX/0SG;LX/1Ao;Ljava/util/Random;LX/0pq;)V

    .line 490306
    move-object v0, v3

    .line 490307
    sput-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->E:Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490308
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 490309
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 490310
    :cond_1
    sget-object v0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->E:Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;

    return-object v0

    .line 490311
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 490312
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 490295
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 490296
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->w:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    add-long/2addr v0, p1

    .line 490297
    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->x:LX/0Tn;

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 490298
    iget-object v4, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->w:LX/0Tn;

    invoke-interface {v4, v5, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->x:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 490299
    return-void
.end method

.method private a(Lcom/facebook/common/callercontext/CallerContext;ZZ)V
    .locals 5

    .prologue
    .line 490287
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 490288
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->p:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->p:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 490289
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490290
    :goto_0
    return-void

    .line 490291
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->q:LX/0Tn;

    .line 490292
    iget-object v2, p1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 490293
    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->r:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->s:LX/0Tn;

    invoke-interface {v0, v1, p2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->t:LX/0Tn;

    invoke-interface {v0, v1, p3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->u:LX/0Tn;

    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->d(Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;)I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 490294
    invoke-virtual {p1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->d(Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;JZZ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 490275
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 490276
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->D:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit8 v0, v0, 0x1e

    if-eqz v0, :cond_1

    .line 490277
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 490278
    goto :goto_0

    .line 490279
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v3, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->f:LX/0Tn;

    invoke-interface {v0, v3, p3, p4}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->j:LX/0Tn;

    .line 490280
    iget-object v4, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v4, v4

    .line 490281
    invoke-interface {v0, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->k:LX/0Tn;

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->l:LX/0Tn;

    invoke-interface {v0, v3, p5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->m:LX/0Tn;

    invoke-interface {v0, v3, p6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    iget-object v4, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v0, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->n:LX/0Tn;

    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->d(Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;)I

    move-result v4

    invoke-interface {v0, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 490282
    invoke-direct {p0, p3, p4}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(J)V

    .line 490283
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 490284
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 490285
    iget-object v2, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 490286
    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p0}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->d(Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_1
.end method

.method private c()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const-wide/16 v10, -0x1

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 490249
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 490250
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "photos_eviction_tracking"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 490251
    const-string v0, "bytes"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->f:LX/0Tn;

    invoke-interface {v3, v4, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490252
    const-string v0, "original_calling_class"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->j:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490253
    const-string v0, "original_analytics_tag"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->k:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490254
    const-string v0, "original_is_prefetch"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->l:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490255
    const-string v0, "original_cancel_requested"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->m:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490256
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490257
    const-string v0, "refetch_count"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->p:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490258
    const-string v0, "refetch_calling_class"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->q:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490259
    const-string v0, "refetch_analytics_tag"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->r:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490260
    const-string v0, "refetch_is_prefetch"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->s:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490261
    const-string v0, "refetch_cancel_requested"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->t:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490262
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    invoke-interface {v0, v3, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    invoke-interface {v0, v3, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 490263
    const-string v0, "refetched_after"

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490264
    const-string v3, "diferent_user"

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->n:LX/0Tn;

    invoke-interface {v0, v4, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    iget-object v4, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->u:LX/0Tn;

    invoke-interface {v4, v5, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    if-eq v0, v4, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490265
    :cond_0
    const-string v0, "logout_detected"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->g:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490266
    const-string v0, "trim_to_nothing_time"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->h:LX/0Tn;

    invoke-interface {v3, v4, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490267
    const-string v0, "trim_to_min_time"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->i:LX/0Tn;

    invoke-interface {v3, v4, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490268
    const-string v0, "total_bytes"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->w:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490269
    const-string v0, "total_requests"

    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->x:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 490270
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->y:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 490271
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->e:LX/0Tn;

    invoke-interface {v0, v2}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 490272
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 490273
    return-void

    :cond_2
    move v0, v1

    .line 490274
    goto :goto_0
.end method

.method private static d(Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;)I
    .locals 3

    .prologue
    .line 490241
    const/4 v0, -0x1

    .line 490242
    iget-object v1, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->A:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 490243
    if-eqz v1, :cond_0

    .line 490244
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 490245
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 490246
    iget-object v0, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    .line 490247
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 490248
    :cond_0
    return v0
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 4

    .prologue
    .line 490235
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490236
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 490237
    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 490238
    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->i:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490239
    :cond_0
    monitor-exit p0

    return-void

    .line 490240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;IZZ)V
    .locals 8

    .prologue
    .line 490223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->C:LX/1Ao;

    invoke-virtual {v0, p1, p2}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    invoke-interface {v0}, LX/1bh;->toString()Ljava/lang/String;

    move-result-object v2

    .line 490224
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 490225
    int-to-long v4, p3

    move-object v1, p0

    move-object v3, p2

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;JZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490226
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 490227
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 490228
    iget-object v3, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    sub-long/2addr v0, v4

    sget-wide v4, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 490229
    invoke-direct {p0}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->c()V

    .line 490230
    int-to-long v4, p3

    move-object v1, p0

    move-object v3, p2

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;JZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 490231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 490232
    :cond_2
    int-to-long v0, p3

    :try_start_2
    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(J)V

    .line 490233
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490234
    invoke-direct {p0, p2, p4, p5}, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->a(Lcom/facebook/common/callercontext/CallerContext;ZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 490217
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490218
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->o:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 490219
    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->B:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 490220
    iget-object v2, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->h:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490221
    :cond_0
    monitor-exit p0

    return-void

    .line 490222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 3

    .prologue
    .line 490204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->v:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 490205
    iget-object v0, p0, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/base/analytics/efficiency/PhotosEvictionLogger;->g:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490206
    :cond_0
    monitor-exit p0

    return-void

    .line 490207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
