.class public Lcom/facebook/photos/base/tagging/FaceBox;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/photos/base/tagging/TagTarget;
.implements LX/362;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/graphics/RectF;

.field public c:Landroid/graphics/RectF;

.field public d:Landroid/graphics/PointF;

.field public e:Landroid/graphics/PointF;

.field public f:Z

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/364;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field public i:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:I

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 486331
    new-instance v0, LX/363;

    invoke-direct {v0}, LX/363;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/tagging/FaceBox;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 486329
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V

    .line 486330
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 486327
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V

    .line 486328
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V
    .locals 6
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 486297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486298
    iput-boolean p4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->h:Z

    .line 486299
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    .line 486300
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    .line 486301
    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    const/high16 p4, 0x3fa00000    # 1.25f

    .line 486302
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 486303
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 486304
    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v5

    const p1, -0x42333333    # -0.1f

    mul-float/2addr v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 486305
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 486306
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 486307
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v3, p4, p4, v4, v5}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 486308
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 486309
    move-object v0, v2

    .line 486310
    :goto_0
    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    .line 486311
    new-instance v0, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    .line 486312
    new-instance v0, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    .line 486313
    const-class v0, LX/364;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    .line 486314
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->BOTTOM:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486315
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->TOP:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486316
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->LEFT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486317
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->RIGHT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486318
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->BOTTOMLEFT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486319
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->BOTTOMRIGHT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486320
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->TOPLEFT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486321
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    sget-object v2, LX/364;->TOPRIGHT:LX/364;

    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486322
    iput-boolean p3, p0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 486323
    iput-object p2, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 486324
    invoke-virtual {p0, v1, v1}, Lcom/facebook/photos/base/tagging/FaceBox;->a(II)V

    .line 486325
    return-void

    .line 486326
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 486261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486262
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    .line 486263
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    .line 486264
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    invoke-static {p1, v0}, Lcom/facebook/photos/base/tagging/FaceBox;->b(Landroid/os/Parcel;Landroid/graphics/RectF;)V

    .line 486265
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    .line 486266
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-static {p1, v0}, Lcom/facebook/photos/base/tagging/FaceBox;->b(Landroid/os/Parcel;Landroid/graphics/RectF;)V

    .line 486267
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    .line 486268
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 486269
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 486270
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    .line 486271
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 486272
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 486273
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 486274
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->h:Z

    .line 486275
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 486276
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 486277
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    .line 486278
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 486279
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->j:I

    .line 486280
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->k:I

    .line 486281
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 486282
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 486283
    if-eqz v3, :cond_1

    move v2, v1

    .line 486284
    :goto_0
    if-ge v2, v3, :cond_2

    .line 486285
    iget-object v4, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    const-class v0, Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486286
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 486287
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 486288
    :cond_2
    const-class v0, LX/364;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    .line 486289
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 486290
    :goto_1
    if-ge v1, v2, :cond_3

    .line 486291
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/364;

    .line 486292
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 486293
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 486294
    iget-object v5, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486295
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 486296
    :cond_3
    return-void
.end method

.method private static a(Landroid/os/Parcel;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 486256
    iget v0, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486257
    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486258
    iget v0, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486259
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486260
    return-void
.end method

.method private static b(Landroid/os/Parcel;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 486251
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 486252
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 486253
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 486254
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 486255
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;
    .locals 4

    .prologue
    .line 486250
    new-instance v0, Lcom/facebook/photos/base/tagging/FaceBox;

    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    iget-boolean v2, p0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    iget-boolean v3, p0, Lcom/facebook/photos/base/tagging/FaceBox;->h:Z

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public final a()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 486249
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 486245
    iput p1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->j:I

    .line 486246
    iput p2, p0, Lcom/facebook/photos/base/tagging/FaceBox;->k:I

    .line 486247
    return-void
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 486248
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 486244
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 486243
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 486242
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 486241
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final f()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 486240
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/364;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 486239
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 486238
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 486237
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 486210
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 486211
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    invoke-static {p1, v1}, Lcom/facebook/photos/base/tagging/FaceBox;->a(Landroid/os/Parcel;Landroid/graphics/RectF;)V

    .line 486212
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->c:Landroid/graphics/RectF;

    invoke-static {p1, v1}, Lcom/facebook/photos/base/tagging/FaceBox;->a(Landroid/os/Parcel;Landroid/graphics/RectF;)V

    .line 486213
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486214
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486215
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486216
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486217
    iget-boolean v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 486218
    iget-boolean v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->h:Z

    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 486219
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    if-nez v1, :cond_0

    .line 486220
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 486221
    :goto_0
    iget v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->j:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 486222
    iget v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->k:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 486223
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 486224
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    move v1, v0

    .line 486225
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 486226
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 486227
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 486228
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    array-length v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 486229
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 486230
    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 486231
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 486232
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/364;

    .line 486233
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 486234
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 486235
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/FaceBox;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    goto :goto_2

    .line 486236
    :cond_3
    return-void
.end method
