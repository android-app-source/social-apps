.class public final Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Jd;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(LX/1Jd;)V
    .locals 1

    .prologue
    .line 486541
    iput-object p1, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->a:LX/1Jd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486542
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->b:Ljava/util/Set;

    .line 486543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->c:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 1

    .prologue
    .line 486544
    iget-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 486545
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 486546
    iget-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->a:LX/1Jd;

    iget-object v1, v0, LX/1Jd;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 486547
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->c:Z

    .line 486548
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486549
    iget-object v0, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/prefetch/PrefetchParams;

    .line 486550
    iget-object v1, p0, Lcom/facebook/photos/prefetch/PrefetchListenerCaller$ListenerCallingRunnable;->a:LX/1Jd;

    iget-object v1, v1, LX/1Jd;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1BN;

    .line 486551
    const/4 v4, 0x1

    invoke-interface {v1, v0, v4}, LX/1BN;->a(Lcom/facebook/photos/prefetch/PrefetchParams;Z)V

    goto :goto_0

    .line 486552
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 486553
    :cond_1
    return-void
.end method
