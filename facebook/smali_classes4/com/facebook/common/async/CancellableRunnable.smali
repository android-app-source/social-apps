.class public Lcom/facebook/common/async/CancellableRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private volatile a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 465589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465590
    iput-object p1, p0, Lcom/facebook/common/async/CancellableRunnable;->a:Ljava/lang/Runnable;

    .line 465591
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 465592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/async/CancellableRunnable;->a:Ljava/lang/Runnable;

    .line 465593
    return-void
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 465594
    iget-object v0, p0, Lcom/facebook/common/async/CancellableRunnable;->a:Ljava/lang/Runnable;

    .line 465595
    if-eqz v0, :cond_0

    .line 465596
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 465597
    :cond_0
    return-void
.end method
