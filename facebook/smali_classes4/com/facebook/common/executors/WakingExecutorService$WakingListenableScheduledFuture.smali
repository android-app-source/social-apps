.class public Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;
.super LX/17D;
.source ""

# interfaces
.implements LX/0YG;
.implements Ljava/lang/Runnable;
.implements Ljava/util/concurrent/ScheduledFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/17D",
        "<TV;>;",
        "LX/0YG",
        "<TV;>;",
        "Ljava/lang/Runnable;",
        "Ljava/util/concurrent/ScheduledFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0Va;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Va",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/2a2;


# direct methods
.method public constructor <init>(LX/2a2;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 537413
    iput-object p1, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->b:LX/2a2;

    invoke-direct {p0}, LX/17D;-><init>()V

    .line 537414
    invoke-static {p2, p3}, LX/0Va;->a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    .line 537415
    return-void
.end method

.method public constructor <init>(LX/2a2;Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 537410
    iput-object p1, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->b:LX/2a2;

    invoke-direct {p0}, LX/17D;-><init>()V

    .line 537411
    invoke-static {p2}, LX/0Va;->a(Ljava/util/concurrent/Callable;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    .line 537412
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 537408
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    move-object v0, v0

    .line 537409
    return-object v0
.end method

.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 537405
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    move-object v0, v0

    .line 537406
    invoke-virtual {v0, p1, p2}, LX/0Va;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 537407
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 537402
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->b:LX/2a2;

    invoke-static {v0, p0}, LX/2a2;->a$redex0(LX/2a2;Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;)V

    .line 537403
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    move-object v0, v0

    .line 537404
    invoke-virtual {v0, p1}, LX/0Va;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 537401
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 537395
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    move-object v0, v0

    .line 537396
    return-object v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 1

    .prologue
    .line 537400
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 537397
    iget-object v0, p0, Lcom/facebook/common/executors/WakingExecutorService$WakingListenableScheduledFuture;->a:LX/0Va;

    move-object v0, v0

    .line 537398
    invoke-virtual {v0}, LX/0Va;->run()V

    .line 537399
    return-void
.end method
