.class public final Lcom/facebook/common/json/FbSerializerProvider$4;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Ljava/util/Map;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 528985
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Map;LX/0nX;)V
    .locals 6

    .prologue
    .line 528986
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 528987
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 528988
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 528989
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 528990
    :goto_1
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 528991
    :cond_0
    instance-of v0, v1, Ljava/lang/Enum;

    if-eqz v0, :cond_3

    .line 528992
    invoke-virtual {p1}, LX/0nX;->a()LX/0lD;

    move-result-object v0

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    .line 528993
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 528994
    invoke-virtual {v0, v3}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v4

    .line 528995
    invoke-virtual {p1}, LX/0nX;->a()LX/0lD;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0nX;->a(LX/0lD;)LX/0nX;

    .line 528996
    invoke-virtual {v4, v1}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 528997
    invoke-virtual {v4}, LX/0nX;->flush()V

    .line 528998
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 528999
    invoke-virtual {v0}, LX/15w;->e()Ljava/lang/String;

    move-result-object v4

    .line 529000
    if-eqz v4, :cond_1

    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 529001
    :cond_1
    new-instance v0, LX/4pY;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to use json as map key, but it is not a string: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529002
    :cond_2
    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 529003
    :cond_3
    new-instance v0, LX/4pY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Non-string, non-enum key ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") found in map."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4pY;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529004
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 529005
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 529006
    check-cast p1, Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/facebook/common/json/FbSerializerProvider$4;->a(Ljava/util/Map;LX/0nX;)V

    return-void
.end method
