.class public final Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/3Io;

.field private final b:Lorg/apache/http/params/HttpParams;

.field private final c:Lorg/apache/http/protocol/HttpService;

.field private final d:Ljava/net/SocketAddress;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private f:LX/3Ir;


# direct methods
.method public constructor <init>(LX/3Io;Ljava/net/SocketAddress;Lorg/apache/http/protocol/HttpService;Lorg/apache/http/params/HttpParams;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 541094
    iput-object p1, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a:LX/3Io;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 541095
    iput-object p2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->d:Ljava/net/SocketAddress;

    .line 541096
    iput-object p4, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->b:Lorg/apache/http/params/HttpParams;

    .line 541097
    iput-object p3, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->c:Lorg/apache/http/protocol/HttpService;

    .line 541098
    iput-object p5, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->e:Ljava/util/concurrent/ExecutorService;

    .line 541099
    return-void
.end method

.method public static a$redex0(Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;)Ljava/net/SocketAddress;
    .locals 4

    .prologue
    .line 541100
    const/4 v0, 0x3

    .line 541101
    :try_start_0
    invoke-static {p0}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->b(Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;)LX/3Ir;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 541102
    :goto_0
    if-lez v0, :cond_0

    .line 541103
    add-int/lit8 v1, v0, -0x1

    .line 541104
    :try_start_1
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;

    iget-object v2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->d:Ljava/net/SocketAddress;

    .line 541105
    iget-object v3, v0, LX/3Ir;->a:Ljava/net/ServerSocket;

    invoke-virtual {v3, v2}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 541106
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;

    .line 541107
    iget-object v2, v0, LX/3Ir;->a:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v2

    move-object v0, v2
    :try_end_1
    .catch Ljava/net/BindException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 541108
    :goto_1
    return-object v0

    .line 541109
    :catch_0
    move-exception v0

    .line 541110
    :try_start_2
    const-string v2, "GenericHttpServer"

    const-string v3, "Binding error, sleep 1 second ..."

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 541111
    if-nez v1, :cond_1

    .line 541112
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 541113
    :catch_1
    move-exception v0

    .line 541114
    const-string v1, "GenericHttpServer"

    const-string v2, "Could not bind to socket."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 541115
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 541116
    :cond_1
    const-wide/16 v2, 0x3e8

    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move v0, v1

    .line 541117
    goto :goto_0
.end method

.method private static b(Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;)LX/3Ir;
    .locals 2

    .prologue
    .line 541118
    new-instance v1, LX/3Ir;

    invoke-direct {v1}, LX/3Ir;-><init>()V

    move-object v0, v1

    .line 541119
    const/4 v1, 0x1

    .line 541120
    iget-object p0, v0, LX/3Ir;->a:Ljava/net/ServerSocket;

    invoke-virtual {p0, v1}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    .line 541121
    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 541122
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a:LX/3Io;

    iget-object v0, v0, LX/3Io;->d:LX/0Sj;

    const-string v1, "GenericHttpServer"

    const-string v2, "RequestListenerThread"

    invoke-interface {v0, v1, v2}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v1

    .line 541123
    if-eqz v1, :cond_0

    .line 541124
    invoke-interface {v1}, LX/0cV;->a()V

    .line 541125
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;

    .line 541126
    iget-object v2, v0, LX/3Ir;->a:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->isBound()Z

    move-result v2

    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541127
    if-nez v0, :cond_3

    .line 541128
    :cond_1
    if-eqz v1, :cond_2

    .line 541129
    invoke-interface {v1}, LX/0cV;->b()V

    .line 541130
    :cond_2
    :goto_0
    return-void

    .line 541131
    :cond_3
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->interrupted()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    .line 541132
    :try_start_2
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->f:LX/3Ir;

    .line 541133
    new-instance v2, LX/3J5;

    iget-object v3, v0, LX/3Ir;->a:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    invoke-direct {v2, v3}, LX/3J5;-><init>(Ljava/net/Socket;)V

    move-object v0, v2

    .line 541134
    iget-object v3, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->b:Lorg/apache/http/params/HttpParams;

    .line 541135
    check-cast v0, LX/3J5;

    .line 541136
    new-instance v4, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 541137
    iget-object v5, v0, LX/3J5;->a:Ljava/net/Socket;

    move-object v5, v5

    .line 541138
    invoke-virtual {v4, v5, v3}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 541139
    move-object v0, v4

    .line 541140
    new-instance v2, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;

    iget-object v3, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->c:Lorg/apache/http/protocol/HttpService;

    iget-object v4, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a:LX/3Io;

    iget-object v4, v4, LX/3Io;->d:LX/0Sj;

    iget-object v5, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a:LX/3Io;

    iget-object v5, v5, LX/3Io;->e:LX/03V;

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/facebook/common/httpserver/GenericHttpServer$WorkerThread;-><init>(Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;LX/0Sj;LX/03V;)V

    .line 541141
    iget-object v0, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->e:Ljava/util/concurrent/ExecutorService;

    const v3, -0x7615f86e

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 541142
    :catch_0
    move-exception v0

    .line 541143
    :try_start_3
    invoke-virtual {p0}, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_3

    .line 541144
    const-string v2, "GenericHttpServer"

    const-string v3, "I/O error"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 541145
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 541146
    invoke-interface {v1}, LX/0cV;->b()V

    :cond_4
    throw v0

    .line 541147
    :catch_1
    move-exception v0

    .line 541148
    :try_start_4
    const-string v2, "GenericHttpServer"

    const-string v3, "I/O error initialising connection thread"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 541149
    :cond_5
    :goto_2
    if-eqz v1, :cond_2

    .line 541150
    invoke-interface {v1}, LX/0cV;->b()V

    goto :goto_0

    .line 541151
    :catch_2
    move-exception v0

    .line 541152
    :try_start_5
    iget-object v2, p0, Lcom/facebook/common/httpserver/GenericHttpServer$RequestListenerThread;->a:LX/3Io;

    iget-object v2, v2, LX/3Io;->e:LX/03V;

    const-string v3, "GenericHttpServer"

    const-string v4, "Unexpected exception"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 541153
    :catch_3
    goto :goto_2
.end method
