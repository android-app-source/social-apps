.class public final Lcom/facebook/common/viewport/BaseViewportMonitor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0g8;

.field public final synthetic b:LX/1Kt;


# direct methods
.method public constructor <init>(LX/1Kt;LX/0g8;)V
    .locals 0

    .prologue
    .line 366479
    iput-object p1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iput-object p2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->a:LX/0g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 366480
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-boolean v0, v0, LX/1Kt;->f:Z

    if-nez v0, :cond_1

    .line 366481
    :cond_0
    :goto_0
    return-void

    .line 366482
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v1

    iput v1, v0, LX/1Kt;->d:I

    .line 366483
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget v0, v0, LX/1Kt;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 366484
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->r()I

    move-result v1

    iput v1, v0, LX/1Kt;->e:I

    .line 366485
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v0, v0, LX/1Kt;->a:LX/01J;

    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v1, v1, LX/1Kt;->g:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->a(LX/01J;)V

    .line 366486
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget v0, v0, LX/1Kt;->d:I

    .line 366487
    :goto_1
    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget v1, v1, LX/1Kt;->e:I

    if-gt v0, v1, :cond_3

    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->s()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 366488
    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->a:LX/0g8;

    invoke-virtual {v1, v2, v0}, LX/1Kt;->b(LX/0g8;I)Ljava/lang/Object;

    move-result-object v1

    .line 366489
    iget-object v2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v2, v2, LX/1Kt;->g:LX/01J;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 366490
    iget-object v2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    invoke-virtual {v2, v1}, LX/1Kt;->a(Ljava/lang/Object;)V

    .line 366491
    :cond_2
    iget-object v2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v2, v2, LX/1Kt;->a:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366492
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 366493
    :cond_3
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v1, v1, LX/1Kt;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_5

    .line 366494
    iget-object v2, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v2, v2, LX/1Kt;->a:LX/01J;

    invoke-virtual {v2, v0}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    .line 366495
    iget-object v3, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v3, v3, LX/1Kt;->g:LX/01J;

    invoke-virtual {v3, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 366496
    iget-object v3, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    invoke-virtual {v3, v2}, LX/1Kt;->b(Ljava/lang/Object;)V

    .line 366497
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 366498
    :cond_5
    iget-object v0, p0, Lcom/facebook/common/viewport/BaseViewportMonitor$1;->b:LX/1Kt;

    iget-object v0, v0, LX/1Kt;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    goto/16 :goto_0
.end method
