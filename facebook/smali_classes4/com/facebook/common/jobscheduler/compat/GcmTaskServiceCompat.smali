.class public abstract Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;
.super LX/2fD;
.source ""


# static fields
.field public static final a:J

.field private static final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 446417
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a:J

    .line 446418
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 446483
    invoke-direct {p0}, LX/2fD;-><init>()V

    .line 446484
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 446482
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/gcm/Task;I)V
    .locals 13

    .prologue
    .line 446464
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 446465
    invoke-virtual {v0, p0}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    .line 446466
    packed-switch v1, :pswitch_data_0

    .line 446467
    const/4 v2, 0x3

    if-lt p2, v2, :cond_0

    .line 446468
    const-string v2, "GcmTaskServiceCompat"

    const-string v3, "Job %s was not scheduled because Google Play Services became consistentlyunavailable after initial check: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/gms/gcm/Task;->b:Ljava/lang/String;

    move-object v6, v6

    .line 446469
    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0, v1}, LX/1od;->c(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446470
    :goto_0
    return-void

    .line 446471
    :pswitch_0
    :try_start_0
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 446472
    :catch_0
    move-exception v0

    .line 446473
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p1, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    move-object v2, v2

    .line 446474
    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, v1, v0}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    goto :goto_0

    .line 446475
    :cond_0
    invoke-virtual {v0, v1}, LX/1od;->c(I)Ljava/lang/String;

    .line 446476
    add-int/lit8 v0, p2, 0x1

    .line 446477
    invoke-static {p0, p1, v0}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->c(Landroid/content/Context;Lcom/google/android/gms/gcm/Task;I)Landroid/content/Intent;

    move-result-object v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sget-wide v11, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a:J

    add-long/2addr v9, v11

    .line 446478
    const-string v8, "alarm"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/AlarmManager;

    .line 446479
    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p0, v11, v7, v12}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    .line 446480
    const/4 v12, 0x2

    invoke-virtual {v8, v12, v9, v10, v11}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 446481
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/gcm/Task;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 446451
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/gcm/Task;->b:Ljava/lang/String;

    move-object v0, v0

    .line 446452
    iget-object v1, p1, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    move-object v1, v1

    .line 446453
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 446454
    new-instance v1, LX/45q;

    invoke-direct {v1, p1, p2}, LX/45q;-><init>(Lcom/google/android/gms/gcm/Task;I)V

    .line 446455
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 446456
    const-string p1, "job_tag"

    iget-object p2, v1, LX/45q;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 446457
    const-string p1, "task"

    iget-object p2, v1, LX/45q;->b:Lcom/google/android/gms/gcm/Task;

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 446458
    const-string p1, "num_failures"

    iget p2, v1, LX/45q;->c:I

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 446459
    move-object v1, p0

    .line 446460
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 446461
    return-object v0

    .line 446462
    :catch_0
    move-exception v0

    .line 446463
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 446448
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 446449
    new-instance v1, Ljava/lang/StringBuilder;

    const-string p2, "com.facebook.common.jobscheduler.compat.GcmTaskServiceCompat.gms.TRY_SCHEDULE-"

    invoke-direct {v1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 446450
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 446434
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 446435
    iget-object v1, p1, LX/2fH;->a:Ljava/lang/String;

    move-object v1, v1

    .line 446436
    const-string v4, "[0-9]+"

    invoke-virtual {v1, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 446437
    :cond_0
    :goto_0
    return v0

    .line 446438
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v1, v4

    .line 446439
    new-instance v4, LX/45p;

    invoke-direct {v4}, LX/45p;-><init>()V

    .line 446440
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a()LX/45y;

    move-result-object v5

    iget-object v6, p1, LX/2fH;->b:Landroid/os/Bundle;

    move-object v6, v6

    .line 446441
    invoke-virtual {v5, v1, v6, v4}, LX/45y;->a(ILandroid/os/Bundle;LX/45o;)Z

    move-result v1

    .line 446442
    if-eqz v1, :cond_2

    .line 446443
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    .line 446444
    :try_start_0
    sget-wide v6, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->b:J

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, LX/45p;->a(J)Z
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 446445
    :goto_1
    if-eqz v1, :cond_0

    .line 446446
    const/4 v0, 0x1

    goto :goto_0

    .line 446447
    :catch_0
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a()LX/45y;

    move-result-object v1

    invoke-virtual {v1}, LX/45y;->a()Z

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public abstract a()LX/45y;
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x773aac15

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 446419
    if-nez p1, :cond_0

    .line 446420
    :try_start_0
    new-instance v1, LX/45s;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, LX/45s;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, -0x5020d7de

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch LX/45s; {:try_start_0 .. :try_end_0} :catch_0

    .line 446421
    :catch_0
    move-exception v1

    .line 446422
    const-string v3, "GcmTaskServiceCompat"

    const-string v4, "Unexpected service start parameters"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446423
    const v1, -0x438b1df7

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 446424
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 446425
    const-string v3, "com.facebook.common.jobscheduler.compat.GcmTaskServiceCompat.gms.TRY_SCHEDULE-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 446426
    new-instance v1, LX/45q;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v1, v3}, LX/45q;-><init>(Landroid/os/Bundle;)V

    .line 446427
    iget-object v3, v1, LX/45q;->b:Lcom/google/android/gms/gcm/Task;

    iget v1, v1, LX/45q;->c:I

    invoke-static {p0, v3, v1}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a(Landroid/content/Context;Lcom/google/android/gms/gcm/Task;I)V
    :try_end_1
    .catch LX/45s; {:try_start_1 .. :try_end_1} :catch_0

    .line 446428
    const v1, 0x32d770fe

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 446429
    :cond_1
    :try_start_2
    const-string v3, "com.google"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 446430
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_2
    .catch LX/45s; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    const v1, 0x4c84b0e1    # 6.9568264E7f

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 446431
    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;->a()LX/45y;

    .line 446432
    const/4 v1, 0x2

    move v0, v1
    :try_end_3
    .catch LX/45s; {:try_start_3 .. :try_end_3} :catch_0

    .line 446433
    const v1, 0x2451b22e

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
