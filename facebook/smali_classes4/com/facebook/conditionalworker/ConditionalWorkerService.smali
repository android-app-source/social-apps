.class public Lcom/facebook/conditionalworker/ConditionalWorkerService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/2Zy;

.field private c:LX/0Uo;

.field private d:LX/2a0;

.field private e:LX/0WJ;

.field private f:LX/0u7;

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/30V;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0SG;

.field private i:LX/30R;

.field private j:Ljava/util/concurrent/ExecutorService;

.field private k:LX/03V;

.field private l:Lcom/facebook/performancelogger/PerformanceLogger;

.field private m:LX/30T;

.field private n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/30V;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/30V;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/30V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 537317
    const-class v0, Lcom/facebook/conditionalworker/ConditionalWorkerService;

    sput-object v0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 537315
    sget-object v0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 537316
    return-void
.end method

.method private static a(I)J
    .locals 2

    .prologue
    .line 537314
    const/4 v0, 0x1

    const/16 v1, 0x20

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    shl-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x5

    invoke-static {v0}, LX/1m9;->a(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(LX/30V;)J
    .locals 10

    .prologue
    .line 537303
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    .line 537304
    sget-object v6, LX/30R;->a:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    .line 537305
    iget-object v7, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v8, 0x0

    invoke-interface {v7, v6, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    move-wide v2, v6

    .line 537306
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v0, p1}, LX/30R;->b(LX/30V;)I

    move-result v0

    .line 537307
    if-lez v0, :cond_1

    .line 537308
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 537309
    sget-object v1, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    invoke-interface {p1}, LX/30V;->b()LX/2Jm;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/2Jm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 537310
    invoke-static {v0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(I)J

    move-result-wide v0

    .line 537311
    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    .line 537312
    :cond_0
    invoke-static {v0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(I)J

    move-result-wide v0

    invoke-interface {p1}, LX/30V;->e()J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 537313
    :cond_1
    invoke-interface {p1}, LX/30V;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Ljava/util/Iterator;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "LX/30V;",
            ">;J)J"
        }
    .end annotation

    .prologue
    .line 537290
    const-wide v0, 0x7fffffffffffffffL

    move-wide v2, v0

    .line 537291
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 537292
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30V;

    .line 537293
    invoke-interface {v0}, LX/30V;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 537294
    sget-object v1, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    invoke-interface {v0}, LX/30V;->b()LX/2Jm;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/2Jm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 537295
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v1, v0}, LX/30R;->g(LX/30V;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 537296
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v1, v0}, LX/30R;->b(LX/30V;)I

    move-result v1

    .line 537297
    if-lez v1, :cond_0

    .line 537298
    :cond_1
    invoke-direct {p0, v0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/30V;)J

    move-result-wide v0

    .line 537299
    cmp-long v4, p2, v0

    if-gez v4, :cond_0

    .line 537300
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 537301
    goto :goto_0

    .line 537302
    :cond_2
    return-wide v2
.end method

.method private a(LX/30V;Z)LX/2Jy;
    .locals 6

    .prologue
    .line 537289
    new-instance v0, LX/2Jy;

    sget-object v1, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v5, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->m:LX/30T;

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, LX/2Jy;-><init>(LX/30V;ZLjava/lang/String;Lcom/facebook/performancelogger/PerformanceLogger;LX/30T;)V

    return-object v0
.end method

.method private a(Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "LX/2Jy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537166
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 537167
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30V;

    .line 537168
    invoke-direct {p0, v0, p1}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/30V;Z)LX/2Jy;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 537169
    :cond_0
    return-object v1
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    .line 537267
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->g:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537268
    :cond_0
    :goto_0
    return-void

    .line 537269
    :cond_1
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 537270
    new-instance v0, LX/30T;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->c:LX/0Uo;

    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->d:LX/2a0;

    iget-object v3, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->e:LX/0WJ;

    iget-object v4, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->f:LX/0u7;

    invoke-direct {v0, v1, v2, v3, v4}, LX/30T;-><init>(LX/0Uo;LX/2a0;LX/0WJ;LX/0u7;)V

    iput-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->m:LX/30T;

    .line 537271
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->g:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    .line 537272
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    .line 537273
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->p:Ljava/util/List;

    .line 537274
    const-wide/16 v2, 0x0

    .line 537275
    const/4 v0, 0x0

    .line 537276
    :goto_1
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 537277
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->m:LX/30T;

    invoke-virtual {v1}, LX/30T;->a()LX/2Jj;

    move-result-object v1

    .line 537278
    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 537279
    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/2Jj;J)V

    .line 537280
    if-ge v0, v5, :cond_2

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 537281
    invoke-direct {p0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->b()V

    .line 537282
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->p:Ljava/util/List;

    iget-object v4, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 537283
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 537284
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 537285
    :cond_2
    if-lt v0, v5, :cond_3

    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 537286
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->p:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 537287
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->k:LX/03V;

    sget-object v1, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Remaining executable ConditionalWorkerInfo after 5 execution passes: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 537288
    :cond_3
    invoke-direct {p0, v2, v3}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(J)V

    goto/16 :goto_0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 537256
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(Ljava/util/Iterator;J)J

    move-result-wide v0

    .line 537257
    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-direct {p0, v2, p1, p2}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(Ljava/util/Iterator;J)J

    move-result-wide v2

    .line 537258
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 537259
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 537260
    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v2, LX/30V;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 537261
    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->b:LX/2Zy;

    .line 537262
    iget-object v3, v2, LX/2Zy;->l:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v3, :cond_0

    .line 537263
    iget-object v3, v2, LX/2Zy;->l:Ljava/util/concurrent/ScheduledFuture;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 537264
    :cond_0
    iget-object v3, v2, LX/2Zy;->f:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v4, v2, LX/2Zy;->k:Ljava/lang/Runnable;

    sget-object p0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v0, v1, p0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, v2, LX/2Zy;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 537265
    invoke-static {v0, v1}, LX/1m9;->a(J)Ljava/lang/String;

    .line 537266
    :cond_1
    return-void
.end method

.method private a(LX/2Jj;J)V
    .locals 10

    .prologue
    .line 537241
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 537242
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 537243
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30V;

    .line 537244
    invoke-interface {v0}, LX/30V;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 537245
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    goto :goto_0

    .line 537246
    :cond_1
    invoke-interface {v0}, LX/30V;->d()LX/2Jl;

    move-result-object v2

    .line 537247
    invoke-interface {v0}, LX/30V;->b()LX/2Jm;

    move-result-object v3

    .line 537248
    sget-object v4, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    invoke-virtual {v3, v4}, LX/2Jm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 537249
    invoke-direct {p0, p1, v0, v2}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/2Jj;LX/30V;LX/2Jl;)V

    .line 537250
    :cond_2
    invoke-direct {p0, v0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/30V;)J

    move-result-wide v4

    .line 537251
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v3, 0x2

    aput-object v2, v6, v3

    const/4 v3, 0x3

    invoke-interface {v0}, LX/30V;->e()J

    move-result-wide v8

    invoke-static {v8, v9}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x4

    invoke-static {v4, v5, p2, p3}, LX/1m9;->a(JJ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 537252
    cmp-long v3, p2, v4

    if-ltz v3, :cond_0

    invoke-virtual {p1, v2}, LX/2Jj;->a(LX/2Jl;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v2, v0}, LX/30R;->g(LX/30V;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 537253
    iget-object v2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537254
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 537255
    :cond_3
    return-void
.end method

.method private a(LX/2Jj;LX/30V;LX/2Jl;)V
    .locals 1

    .prologue
    .line 537159
    invoke-virtual {p1, p3}, LX/2Jj;->a(LX/2Jl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537160
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v0, p2}, LX/30R;->g(LX/30V;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537161
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    .line 537162
    sget-object p0, LX/30R;->c:LX/0Tn;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object p0

    check-cast p0, LX/0Tn;

    .line 537163
    iget-object p1, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    const/4 p3, 0x0

    invoke-interface {p1, p0, p3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 537164
    :cond_0
    :goto_0
    return-void

    .line 537165
    :cond_1
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v0, p2}, LX/30R;->d(LX/30V;)V

    goto :goto_0
.end method

.method private a(LX/2Zy;LX/0Uo;LX/2a0;LX/0WJ;LX/0u7;Ljava/util/Set;LX/0SG;LX/30R;Ljava/util/concurrent/ExecutorService;LX/03V;Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Zy;",
            "LX/0Uo;",
            "LX/2a0;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0u7;",
            "Ljava/util/Set",
            "<",
            "LX/30V;",
            ">;",
            "LX/0SG;",
            "LX/30R;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537170
    iput-object p1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->b:LX/2Zy;

    .line 537171
    iput-object p2, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->c:LX/0Uo;

    .line 537172
    iput-object p3, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->d:LX/2a0;

    .line 537173
    iput-object p4, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->e:LX/0WJ;

    .line 537174
    iput-object p5, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->f:LX/0u7;

    .line 537175
    iput-object p6, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->g:Ljava/util/Set;

    .line 537176
    iput-object p7, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->h:LX/0SG;

    .line 537177
    iput-object p8, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    .line 537178
    iput-object p9, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->j:Ljava/util/concurrent/ExecutorService;

    .line 537179
    iput-object p10, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->k:LX/03V;

    .line 537180
    iput-object p11, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 537181
    return-void
.end method

.method private a(LX/30V;ZLjava/lang/Throwable;)V
    .locals 4
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 537182
    if-eqz p2, :cond_2

    .line 537183
    sget-object v0, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    invoke-interface {p1}, LX/30V;->b()LX/2Jm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Jm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537184
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    .line 537185
    sget-object v1, LX/30R;->c:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 537186
    iget-object v2, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 537187
    :cond_0
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    invoke-virtual {v0, p1}, LX/30R;->d(LX/30V;)V

    .line 537188
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 537189
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 537190
    sget-object v1, LX/30R;->a:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 537191
    iget-object p0, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    invoke-interface {p0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 537192
    return-void

    .line 537193
    :cond_2
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->i:LX/30R;

    .line 537194
    sget-object v1, LX/30R;->b:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 537195
    iget-object v2, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 537196
    iget-object v3, v0, LX/30R;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v3, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 537197
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 537198
    if-eqz p3, :cond_1

    .line 537199
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->k:LX/03V;

    sget-object v1, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Execution failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/conditionalworker/ConditionalWorkerService;

    invoke-static {v11}, LX/2Zy;->a(LX/0QB;)LX/2Zy;

    move-result-object v1

    check-cast v1, LX/2Zy;

    invoke-static {v11}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v2

    check-cast v2, LX/0Uo;

    invoke-static {v11}, LX/2a0;->a(LX/0QB;)LX/2a0;

    move-result-object v3

    check-cast v3, LX/2a0;

    invoke-static {v11}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v11}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v5

    check-cast v5, LX/0u7;

    new-instance v6, LX/0U8;

    invoke-interface {v11}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/30Q;

    invoke-direct {v8, v11}, LX/30Q;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    invoke-static {v11}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v11}, LX/30R;->a(LX/0QB;)LX/30R;

    move-result-object v8

    check-cast v8, LX/30R;

    invoke-static {v11}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v11}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v11}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v11

    check-cast v11, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/2Zy;LX/0Uo;LX/2a0;LX/0WJ;LX/0u7;Ljava/util/Set;LX/0SG;LX/30R;Ljava/util/concurrent/ExecutorService;LX/03V;Lcom/facebook/performancelogger/PerformanceLogger;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 537200
    invoke-direct {p0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->c()Z

    move-result v0

    .line 537201
    invoke-direct {p0, v0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(Z)Ljava/util/List;

    move-result-object v5

    .line 537202
    :try_start_0
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->j:Ljava/util/concurrent/ExecutorService;

    sget-wide v6, LX/30V;->a:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v5, v6, v7, v1}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 537203
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 537204
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 537205
    const v3, -0x534bbf14

    :try_start_1
    invoke-static {v0, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2LI;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v3, v4

    .line 537206
    :goto_1
    if-nez v0, :cond_1

    .line 537207
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jy;

    .line 537208
    iget-object v7, v0, LX/2Jy;->b:LX/30V;

    move-object v0, v7

    .line 537209
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/30V;ZLjava/lang/Throwable;)V

    .line 537210
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 537211
    goto :goto_0

    .line 537212
    :catch_0
    move-exception v0

    .line 537213
    sget-object v1, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    const-string v2, "ExecutorService.invokeAll()"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 537214
    iget-object v1, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->k:LX/03V;

    sget-object v2, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ExecutorService.invokeAll()"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 537215
    :cond_0
    return-void

    .line 537216
    :catch_1
    move-exception v0

    .line 537217
    sget-object v3, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    const-string v7, "Future<ConditionalWorkerResult>.get()"

    invoke-static {v3, v7, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v0

    move-object v0, v4

    .line 537218
    goto :goto_1

    .line 537219
    :catch_2
    move-exception v0

    .line 537220
    sget-object v3, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a:Ljava/lang/Class;

    const-string v7, "Future<ConditionalWorkerResult>.get()"

    invoke-static {v3, v7, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v0

    move-object v0, v4

    goto :goto_1

    .line 537221
    :cond_1
    iget-object v3, v0, LX/2LI;->a:LX/30V;

    iget-boolean v0, v0, LX/2LI;->b:Z

    invoke-direct {p0, v3, v0, v4}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(LX/30V;ZLjava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    move-object v3, v4

    move-object v0, v4

    goto :goto_1
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 537222
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 537223
    if-eqz v0, :cond_0

    .line 537224
    iget-boolean p0, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, p0

    .line 537225
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 537226
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 537227
    iget-object v0, p0, Lcom/facebook/conditionalworker/ConditionalWorkerService;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30V;

    .line 537228
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 537229
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 537230
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 537231
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 537232
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x33dfd01a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 537233
    if-nez p1, :cond_0

    .line 537234
    const/16 v1, 0x25

    const v2, -0x55e3836f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 537235
    :goto_0
    return-void

    .line 537236
    :cond_0
    invoke-direct {p0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a()V

    .line 537237
    const v1, -0x4bb6afcf

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x142ccb0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 537238
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 537239
    invoke-static {p0, p0}, Lcom/facebook/conditionalworker/ConditionalWorkerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 537240
    const/16 v1, 0x25

    const v2, 0x60e0eebb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
