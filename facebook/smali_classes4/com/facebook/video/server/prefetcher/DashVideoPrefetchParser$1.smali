.class public final Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1M7;

.field public final synthetic c:LX/36s;

.field public final synthetic d:LX/1mB;


# direct methods
.method public constructor <init>(LX/1mB;Ljava/lang/String;LX/1M7;LX/36s;)V
    .locals 0

    .prologue
    .line 547652
    iput-object p1, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->d:LX/1mB;

    iput-object p2, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->b:LX/1M7;

    iput-object p4, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->c:LX/36s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 547630
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->d:LX/1mB;

    iget-object v1, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->b:LX/1M7;

    iget-object v3, p0, Lcom/facebook/video/server/prefetcher/DashVideoPrefetchParser$1;->c:LX/36s;

    const/4 v4, 0x0

    .line 547631
    new-instance v5, LX/0AZ;

    invoke-direct {v5}, LX/0AZ;-><init>()V

    .line 547632
    :try_start_0
    new-instance v6, Ljava/io/ByteArrayInputStream;

    const-string v7, "UTF-8"

    invoke-virtual {v1, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 547633
    const-string v7, ""

    invoke-virtual {v5, v7, v6}, LX/0AZ;->a(Ljava/lang/String;Ljava/io/InputStream;)LX/0AY;

    move-result-object v5

    .line 547634
    invoke-virtual {v5}, LX/0AY;->b()I

    move-result v6

    if-lez v6, :cond_5

    .line 547635
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0AY;->a(I)LX/0Am;

    move-result-object v5

    .line 547636
    iget-object v6, v5, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 547637
    iget-object v5, v5, LX/0Am;->c:Ljava/util/List;

    .line 547638
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v6, v4

    move-object v7, v4

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ak;

    .line 547639
    iget-object v9, v4, LX/0Ak;->c:Ljava/util/List;

    .line 547640
    if-nez v7, :cond_1

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x0

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Ah;

    iget-object v5, v5, LX/0Ah;->c:LX/0AR;

    iget-object v5, v5, LX/0AR;->b:Ljava/lang/String;

    const-string p0, "video/"

    invoke-virtual {v5, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v7, v4

    .line 547641
    goto :goto_0

    .line 547642
    :cond_1
    if-nez v6, :cond_2

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x0

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Ah;

    iget-object v5, v5, LX/0Ah;->c:LX/0AR;

    iget-object v5, v5, LX/0AR;->b:Ljava/lang/String;

    const-string v9, "audio/"

    invoke-virtual {v5, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v6, v4

    .line 547643
    goto :goto_0

    .line 547644
    :cond_2
    if-eqz v6, :cond_0

    if-eqz v7, :cond_0

    .line 547645
    :cond_3
    if-eqz v7, :cond_4

    .line 547646
    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v7, v4}, LX/1mB;->a(LX/1mB;LX/1M7;LX/36s;LX/0Ak;I)V

    .line 547647
    :cond_4
    if-eqz v6, :cond_5

    .line 547648
    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v6, v4}, LX/1mB;->a(LX/1mB;LX/1M7;LX/36s;LX/0Ak;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 547649
    :cond_5
    :goto_1
    return-void

    .line 547650
    :catch_0
    move-exception v4

    .line 547651
    sget-object v5, LX/1mB;->a:Ljava/lang/String;

    const-string v6, "Exception is thrown"

    invoke-static {v5, v6, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
