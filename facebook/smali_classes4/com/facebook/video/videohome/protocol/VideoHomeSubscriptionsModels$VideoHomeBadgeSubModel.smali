.class public final Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16d144a2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 465488
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 465489
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 465492
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 465493
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 465490
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 465491
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 465467
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 465468
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 465469
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 465470
    iget v1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 465471
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 465472
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 465473
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->h:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 465474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 465475
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 465480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 465481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 465482
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 465483
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 465484
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->e:I

    .line 465485
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->g:I

    .line 465486
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->h:I

    .line 465487
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 465477
    new-instance v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    invoke-direct {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;-><init>()V

    .line 465478
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 465479
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 465476
    const v0, 0x1022ebf3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 465466
    const v0, -0x46fa5c6b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 465464
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 465465
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 465462
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 465463
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->g:I

    return v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 465460
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 465461
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->h:I

    return v0
.end method
