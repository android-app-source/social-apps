.class public Lcom/facebook/video/engine/VideoDataSource;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Landroid/graphics/RectF;


# instance fields
.field public final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;

.field public final d:Landroid/net/Uri;

.field public final e:Ljava/lang/String;

.field public final f:LX/097;

.field public final g:Landroid/graphics/RectF;

.field public final h:LX/2oF;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 465757
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/video/engine/VideoDataSource;->a:Landroid/graphics/RectF;

    .line 465758
    new-instance v0, LX/2oD;

    invoke-direct {v0}, LX/2oD;-><init>()V

    sput-object v0, Lcom/facebook/video/engine/VideoDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2oE;)V
    .locals 1

    .prologue
    .line 465759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465760
    iget-object v0, p1, LX/2oE;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 465761
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    .line 465762
    iget-object v0, p1, LX/2oE;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 465763
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    .line 465764
    iget-object v0, p1, LX/2oE;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 465765
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    .line 465766
    iget-object v0, p1, LX/2oE;->d:Ljava/lang/String;

    move-object v0, v0

    .line 465767
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    .line 465768
    iget-object v0, p1, LX/2oE;->e:LX/097;

    move-object v0, v0

    .line 465769
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    .line 465770
    iget-object v0, p1, LX/2oE;->f:Landroid/graphics/RectF;

    move-object v0, v0

    .line 465771
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    .line 465772
    iget-object v0, p1, LX/2oE;->g:LX/2oF;

    move-object v0, v0

    .line 465773
    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    .line 465774
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 465748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465749
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    .line 465750
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    .line 465751
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    .line 465752
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    .line 465753
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/097;->valueOf(Ljava/lang/String;)LX/097;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    .line 465754
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    .line 465755
    invoke-static {}, LX/2oF;->values()[LX/2oF;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    .line 465756
    return-void
.end method

.method public static a(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 465742
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 465743
    :cond_0
    :goto_0
    return v0

    .line 465744
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 465745
    :cond_2
    invoke-static {p0}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 465746
    invoke-static {p1}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 465747
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/2oE;
    .locals 1

    .prologue
    .line 465775
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 465741
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465735
    if-ne p1, p0, :cond_1

    .line 465736
    :cond_0
    :goto_0
    return v0

    .line 465737
    :cond_1
    instance-of v2, p1, Lcom/facebook/video/engine/VideoDataSource;

    if-nez v2, :cond_2

    move v0, v1

    .line 465738
    goto :goto_0

    .line 465739
    :cond_2
    check-cast p1, Lcom/facebook/video/engine/VideoDataSource;

    .line 465740
    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/facebook/video/engine/VideoDataSource;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/facebook/video/engine/VideoDataSource;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 465734
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 465725
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 465726
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 465727
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 465728
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 465729
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465730
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    invoke-virtual {v0}, LX/097;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465731
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 465732
    iget-object v0, p0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    invoke-virtual {v0}, LX/2oF;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465733
    return-void
.end method
