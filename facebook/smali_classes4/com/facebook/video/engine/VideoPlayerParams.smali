.class public Lcom/facebook/video/engine/VideoPlayerParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/098;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:I

.field public d:Ljava/lang/String;

.field public final e:LX/162;

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:Z

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:Lcom/facebook/spherical/model/SphericalVideoParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 465857
    new-instance v0, LX/2oG;

    invoke-direct {v0}, LX/2oG;-><init>()V

    sput-object v0, Lcom/facebook/video/engine/VideoPlayerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2oH;)V
    .locals 1

    .prologue
    .line 465904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465905
    iget-object v0, p1, LX/2oH;->a:Ljava/util/List;

    move-object v0, v0

    .line 465906
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    .line 465907
    iget-object v0, p1, LX/2oH;->b:Ljava/lang/String;

    move-object v0, v0

    .line 465908
    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 465909
    iget v0, p1, LX/2oH;->c:I

    move v0, v0

    .line 465910
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    .line 465911
    iget-object v0, p1, LX/2oH;->d:Ljava/lang/String;

    move-object v0, v0

    .line 465912
    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    .line 465913
    iget-object v0, p1, LX/2oH;->e:LX/162;

    move-object v0, v0

    .line 465914
    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 465915
    iget-boolean v0, p1, LX/2oH;->f:Z

    move v0, v0

    .line 465916
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    .line 465917
    iget-boolean v0, p1, LX/2oH;->g:Z

    move v0, v0

    .line 465918
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    .line 465919
    iget v0, p1, LX/2oH;->j:I

    move v0, v0

    .line 465920
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    .line 465921
    iget v0, p1, LX/2oH;->k:I

    move v0, v0

    .line 465922
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    .line 465923
    iget-boolean v0, p1, LX/2oH;->u:Z

    move v0, v0

    .line 465924
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    .line 465925
    iget-boolean v0, p1, LX/2oH;->h:Z

    move v0, v0

    .line 465926
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    .line 465927
    iget-object v0, p1, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-object v0, v0

    .line 465928
    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 465929
    iget v0, p1, LX/2oH;->l:I

    move v0, v0

    .line 465930
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 465931
    iget-object v0, p1, LX/2oH;->m:Lcom/facebook/spherical/model/SphericalVideoParams;

    move-object v0, v0

    .line 465932
    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 465933
    iget-boolean v0, p1, LX/2oH;->n:Z

    move v0, v0

    .line 465934
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    .line 465935
    iget-boolean v0, p1, LX/2oH;->o:Z

    move v0, v0

    .line 465936
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    .line 465937
    iget v0, p1, LX/2oH;->p:I

    move v0, v0

    .line 465938
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    .line 465939
    iget v0, p1, LX/2oH;->q:I

    if-gtz v0, :cond_0

    .line 465940
    iget v0, p1, LX/2oH;->c:I

    .line 465941
    :goto_0
    move v0, v0

    .line 465942
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    .line 465943
    iget v0, p1, LX/2oH;->r:I

    move v0, v0

    .line 465944
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    .line 465945
    iget-boolean v0, p1, LX/2oH;->t:Z

    move v0, v0

    .line 465946
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    .line 465947
    iget-boolean v0, p1, LX/2oH;->v:Z

    move v0, v0

    .line 465948
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    .line 465949
    iget-boolean v0, p1, LX/2oH;->w:Z

    move v0, v0

    .line 465950
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    .line 465951
    iget-boolean v0, p1, LX/2oH;->x:Z

    move v0, v0

    .line 465952
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    .line 465953
    iget v0, p1, LX/2oH;->s:I

    move v0, v0

    .line 465954
    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    .line 465955
    iget-boolean v0, p1, LX/2oH;->y:Z

    move v0, v0

    .line 465956
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    .line 465957
    return-void

    :cond_0
    iget v0, p1, LX/2oH;->q:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 465860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465861
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 465862
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 465863
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 465864
    sget-object v4, Lcom/facebook/video/engine/VideoDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 465865
    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 465866
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    .line 465867
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 465868
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    .line 465869
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    .line 465870
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    .line 465871
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    .line 465872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 465873
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    .line 465874
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    .line 465875
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    .line 465876
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 465877
    const-class v0, Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/model/SphericalVideoParams;

    iput-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 465878
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    .line 465879
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    .line 465880
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    .line 465881
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    .line 465882
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    .line 465883
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    .line 465884
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    .line 465885
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    .line 465886
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    .line 465887
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    .line 465888
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    .line 465889
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_a

    :goto_a
    iput-boolean v1, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    .line 465890
    return-void

    .line 465891
    :catch_0
    move-exception v0

    .line 465892
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v0, v2

    .line 465893
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 465894
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 465895
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 465896
    goto :goto_3

    :cond_4
    move v0, v2

    .line 465897
    goto :goto_4

    :cond_5
    move v0, v2

    .line 465898
    goto :goto_5

    :cond_6
    move v0, v2

    .line 465899
    goto :goto_6

    :cond_7
    move v0, v2

    .line 465900
    goto :goto_7

    :cond_8
    move v0, v2

    .line 465901
    goto :goto_8

    :cond_9
    move v0, v2

    .line 465902
    goto :goto_9

    :cond_a
    move v1, v2

    .line 465903
    goto :goto_a
.end method

.method public static newBuilder()LX/2oH;
    .locals 1

    .prologue
    .line 465859
    new-instance v0, LX/2oH;

    invoke-direct {v0}, LX/2oH;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 465858
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    return v0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;)Z
    .locals 2

    .prologue
    .line 465856
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 465855
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 465854
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    return v0
.end method

.method public final d()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 465851
    invoke-virtual {p0}, Lcom/facebook/video/engine/VideoPlayerParams;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 465852
    iget-object p0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    move-object v0, p0

    .line 465853
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 465958
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 465800
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465801
    if-ne p1, p0, :cond_1

    .line 465802
    :cond_0
    :goto_0
    return v0

    .line 465803
    :cond_1
    instance-of v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 465804
    goto :goto_0

    .line 465805
    :cond_2
    check-cast p1, Lcom/facebook/video/engine/VideoPlayerParams;

    .line 465806
    invoke-virtual {p0, p1}, Lcom/facebook/video/engine/VideoPlayerParams;->a(Lcom/facebook/video/engine/VideoPlayerParams;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 465807
    invoke-virtual {p0}, Lcom/facebook/video/engine/VideoPlayerParams;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->a()LX/03z;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 465808
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 465809
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 465850
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 465810
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 465811
    invoke-virtual {p0}, Lcom/facebook/video/engine/VideoPlayerParams;->f()LX/03z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/engine/VideoPlayerParams;->f()LX/03z;

    move-result-object v0

    iget-boolean v0, v0, LX/03z;->isSpatial:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 465812
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 465813
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465814
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 465815
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465816
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465817
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465818
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465819
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465820
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 465821
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465822
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465823
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465824
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465825
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 465826
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465827
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465828
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465829
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465830
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465831
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465832
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465833
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465834
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465835
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->o:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465836
    iget v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->s:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 465837
    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->y:Z

    if-eqz v0, :cond_a

    :goto_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 465838
    return-void

    :cond_0
    move v0, v2

    .line 465839
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 465840
    goto :goto_1

    :cond_2
    move v0, v2

    .line 465841
    goto :goto_2

    :cond_3
    move v0, v2

    .line 465842
    goto :goto_3

    :cond_4
    move v0, v2

    .line 465843
    goto :goto_4

    :cond_5
    move v0, v2

    .line 465844
    goto :goto_5

    :cond_6
    move v0, v2

    .line 465845
    goto :goto_6

    :cond_7
    move v0, v2

    .line 465846
    goto :goto_7

    :cond_8
    move v0, v2

    .line 465847
    goto :goto_8

    :cond_9
    move v0, v2

    .line 465848
    goto :goto_9

    :cond_a
    move v1, v2

    .line 465849
    goto :goto_a
.end method
