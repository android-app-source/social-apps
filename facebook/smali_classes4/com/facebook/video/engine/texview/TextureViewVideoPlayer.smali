.class public Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;
.super LX/2q6;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements LX/3FJ;
.implements LX/2qI;


# static fields
.field private static final Y:Ljava/lang/Class;


# instance fields
.field private final Z:LX/0ka;

.field private aA:I

.field private final aa:LX/0TD;

.field private final ab:I

.field private final ac:LX/3FK;

.field public ad:Landroid/media/MediaPlayer;

.field private ae:F

.field public af:Landroid/net/Uri;

.field private ag:Landroid/net/Uri;

.field private ah:Landroid/net/Uri;

.field private ai:LX/09L;

.field public aj:I

.field private ak:I

.field private al:I

.field private am:LX/2oi;

.field private an:I

.field public ao:I

.field private ap:I

.field public aq:LX/19Z;

.field private ar:J

.field private as:LX/2p8;

.field private final at:LX/3FL;

.field private au:LX/7Jq;

.field private final av:Z

.field private aw:Z

.field private final ax:LX/3FM;

.field public final ay:LX/0Uh;

.field private final az:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 539074
    const-class v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    sput-object v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->Y:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/1C2;LX/2q9;LX/2pw;LX/0ka;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/2px;LX/2q3;LX/13m;ZLX/3FK;LX/0ad;LX/0Uh;LX/19j;LX/19a;LX/0Ot;)V
    .locals 23
    .param p13    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/engine/IsPausedBitmapEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/2pf;",
            "LX/2qI;",
            "LX/1C2;",
            "Lcom/facebook/video/subtitles/controller/SubtitleAdapter;",
            "LX/2pw;",
            "LX/0ka;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0TD;",
            "Ljava/lang/Boolean;",
            "Z",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/19Z;",
            "LX/0So;",
            "LX/2px;",
            "LX/2q3;",
            "LX/13m;",
            "Z",
            "LX/3FK;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/19j;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 539075
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p12

    move-object/from16 v12, p16

    move-object/from16 v13, p14

    move/from16 v14, p15

    move-object/from16 v15, p18

    move-object/from16 v16, p19

    move-object/from16 v17, p21

    move-object/from16 v18, p20

    move-object/from16 v19, p8

    move-object/from16 v20, p24

    move-object/from16 v21, p25

    move-object/from16 v22, p26

    invoke-direct/range {v2 .. v22}, LX/2q6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/2q9;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;Ljava/lang/Boolean;ZLX/0So;LX/2px;LX/13m;LX/2q3;LX/1C2;LX/0ad;LX/0Uh;LX/19j;)V

    .line 539076
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    .line 539077
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    .line 539078
    new-instance v2, LX/3FL;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/3FL;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->at:LX/3FL;

    .line 539079
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    .line 539080
    new-instance v2, LX/3FM;

    invoke-direct {v2}, LX/3FM;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    .line 539081
    sget-object v2, LX/09L;->PROGRESSIVE_DOWNLOAD:LX/09L;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    .line 539082
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->Z:LX/0ka;

    .line 539083
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aa:LX/0TD;

    .line 539084
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->b:Landroid/content/Context;

    const/high16 v3, 0x43960000    # 300.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ab:I

    .line 539085
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ac:LX/3FK;

    .line 539086
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/2q9;->a(LX/2qI;)V

    .line 539087
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/2q9;->a(LX/2pw;)V

    .line 539088
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539089
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v2

    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 539090
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aq:LX/19Z;

    .line 539091
    new-instance v2, LX/09M;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->m:LX/0So;

    move-object/from16 v0, p27

    move-object/from16 v1, p28

    invoke-direct {v2, v3, v0, v1}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->N:LX/09M;

    .line 539092
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 539093
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->E:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 539094
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->p:LX/2px;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2px;->a(LX/16V;)V

    .line 539095
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->n:LX/2q3;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2q3;->a(LX/16V;)V

    .line 539096
    move/from16 v0, p22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->av:Z

    .line 539097
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ay:LX/0Uh;

    .line 539098
    move-object/from16 v0, p28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->az:LX/0Ot;

    .line 539099
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aA:I

    .line 539100
    return-void
.end method

.method private J()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 539101
    const-string v0, "moveToNextVideoSource: %s"

    new-array v3, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539102
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    .line 539103
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    if-gez v0, :cond_0

    .line 539104
    iput v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    .line 539105
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 539106
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 539107
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 539108
    iget-object v2, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    .line 539109
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->S:LX/097;

    move v0, v1

    .line 539110
    :goto_1
    return v0

    .line 539111
    :cond_1
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    goto :goto_0

    :cond_2
    move v0, v2

    .line 539112
    goto :goto_1
.end method

.method private static L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 539113
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 539114
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LX/2qk;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 539115
    const-string v0, "release"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539116
    const-string v0, "release: %s, preserveTarget=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v5

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539117
    iput-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->F:LX/7IE;

    .line 539118
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 539119
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz v0, :cond_0

    .line 539120
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 539121
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 539122
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 539123
    if-nez p2, :cond_1

    .line 539124
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 539125
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-static {v0, v4}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/media/MediaPlayer;Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    .line 539126
    invoke-direct {p0, v4}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/view/Surface;)V

    .line 539127
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/media/MediaPlayer;Z)V

    .line 539128
    iput-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    .line 539129
    iput v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    .line 539130
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539131
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    if-eqz v0, :cond_3

    .line 539132
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    invoke-interface {v0}, LX/7Jq;->a()V

    .line 539133
    iput-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    .line 539134
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    sget v1, LX/3FM;->a:I

    invoke-virtual {v0, v1}, LX/3FM;->removeMessages(I)V

    .line 539135
    return-void
.end method

.method private static a(Landroid/media/MediaPlayer;Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V
    .locals 0

    .prologue
    .line 539136
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 539137
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 539138
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 539139
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 539140
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 539141
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 539142
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 539143
    return-void
.end method

.method private a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 539144
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/view/Surface;LX/2qE;)V

    .line 539145
    return-void
.end method

.method private a(Landroid/view/Surface;LX/2qE;)V
    .locals 3

    .prologue
    .line 539146
    iget-object v0, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$3;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/view/Surface;LX/2qE;)V

    const v2, -0x12d5e7a5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 539147
    return-void
.end method

.method private static a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/media/MediaPlayer;Z)V
    .locals 3

    .prologue
    .line 539148
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 539149
    iget-object v1, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;

    invoke-direct {v2, p0, v0, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Ljava/lang/ref/WeakReference;Z)V

    const v0, 0x6c0f0a0

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 539150
    return-void
.end method

.method private static a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/net/Uri;Z)V
    .locals 12

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 539151
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09P;

    invoke-direct {v1}, LX/09P;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 539152
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 539153
    sget-object v0, LX/2qk;->FROM_PREPARE:LX/2qk;

    invoke-direct {p0, v0, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;Z)V

    .line 539154
    :cond_0
    const-string v0, "Allocate new media player"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539155
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    invoke-virtual {v0, v1, v4}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v1

    .line 539156
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ac:LX/3FK;

    iget-boolean v0, v0, LX/3FK;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2q6;->y:LX/04G;

    sget-object v4, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v4, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ac:LX/3FK;

    iget-boolean v0, v0, LX/3FK;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-nez v0, :cond_6

    :cond_2
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-nez v0, :cond_6

    move v0, v2

    .line 539157
    :goto_0
    if-eqz v1, :cond_7

    invoke-static {p1}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_3

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    if-nez v1, :cond_7

    :cond_3
    if-eqz v0, :cond_7

    move v1, v2

    .line 539158
    :goto_1
    new-instance v4, LX/36s;

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-direct {v4, p1, v0}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 539159
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    int-to-long v6, v0

    .line 539160
    iput-wide v6, v4, LX/36s;->e:J

    .line 539161
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    int-to-long v6, v0

    .line 539162
    iput-wide v6, v4, LX/36s;->f:J

    .line 539163
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    int-to-long v6, v0

    .line 539164
    iput-wide v6, v4, LX/36s;->g:J

    .line 539165
    const-string v5, "Allocating media player to stream from %s"

    new-array v6, v2, [Ljava/lang/Object;

    if-eqz v1, :cond_8

    const-string v0, "file"

    :goto_2
    aput-object v0, v6, v3

    invoke-virtual {p0, v5, v6}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539166
    iget-object v0, p0, LX/2q6;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yk;

    invoke-virtual {v0, p1, v4, v1}, LX/1Yk;->a(Landroid/net/Uri;LX/36s;Z)LX/7Jq;

    move-result-object v0

    .line 539167
    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    .line 539168
    invoke-interface {v0}, LX/7Jq;->b()Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    .line 539169
    const-string v1, "Using MediaPlayer from pool, state (%s), prepared for (%s ms), uri (%s)"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-interface {v0}, LX/7Jq;->c()LX/2qD;

    move-result-object v5

    aput-object v5, v4, v3

    iget-object v5, p0, LX/2q6;->m:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    invoke-interface {v0}, LX/7Jq;->d()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    aput-object p1, v4, v2

    invoke-virtual {p0, v1, v4}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539170
    invoke-interface {v0}, LX/7Jq;->c()LX/2qD;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 539171
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/media/MediaPlayer;Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    .line 539172
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v10}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 539173
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ae:F

    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(F)V

    .line 539174
    iget-object v0, p0, LX/2q6;->I:Landroid/view/Surface;

    if-eqz v0, :cond_4

    .line 539175
    iget-object v0, p0, LX/2q6;->I:Landroid/view/Surface;

    sget-object v1, LX/2qE;->STATE_CREATED:LX/2qE;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/view/Surface;LX/2qE;)V

    .line 539176
    :cond_4
    iput v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    .line 539177
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09Q;

    invoke-direct {v1}, LX/09Q;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 539178
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_5

    .line 539179
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 539180
    :cond_5
    return-void

    :cond_6
    move v0, v3

    .line 539181
    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 539182
    goto/16 :goto_1

    .line 539183
    :cond_8
    const-string v0, "http"

    goto :goto_2
.end method

.method private b(F)V
    .locals 3

    .prologue
    .line 539184
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ae:F

    .line 539185
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 539186
    :goto_0
    return-void

    .line 539187
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ae:F

    iget v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ae:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method

.method private b(LX/04g;LX/7K4;)V
    .locals 16

    .prologue
    .line 539188
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 539189
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->b()V

    .line 539190
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->m(LX/04g;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 539191
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/04g;->value:Ljava/lang/String;

    sget-object v5, LX/7K4;->a:LX/7K4;

    move-object/from16 v0, p2

    if-eq v0, v5, :cond_1

    move-object/from16 v0, p2

    iget v5, v0, LX/7K4;->c:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->S:LX/097;

    iget-object v6, v6, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->w:LX/04D;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->z:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v13, v13, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v15, 0x0

    invoke-virtual/range {v1 .. v15}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;Ljava/lang/String;LX/098;Z)LX/1C2;

    .line 539192
    :cond_0
    return-void

    .line 539193
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v5

    goto :goto_0
.end method

.method private c(LX/04g;LX/7K4;)V
    .locals 1

    .prologue
    .line 539194
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->R:LX/04g;

    .line 539195
    iput-object p2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->C:LX/7K4;

    .line 539196
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 539197
    return-void
.end method

.method private d(LX/04g;LX/7K4;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    .line 539198
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 539199
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v4, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 539200
    iput-boolean v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L:Z

    .line 539201
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->M:LX/04g;

    .line 539202
    sget-object v0, LX/2qE;->STATE_UNKNOWN:LX/2qE;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->V:LX/2qE;

    .line 539203
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->K:J

    .line 539204
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v4, LX/2qN;

    iget v5, p2, LX/7K4;->c:I

    sget-object v6, LX/7IB;->b:LX/7IB;

    invoke-direct {v4, v5, v6}, LX/2qN;-><init>(ILX/7IB;)V

    invoke-virtual {v0, v4}, LX/16V;->a(LX/1AD;)V

    .line 539205
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539206
    invoke-interface {v0, p1, v1}, LX/2pf;->a(LX/04g;Z)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 539207
    goto :goto_0

    .line 539208
    :cond_1
    invoke-virtual {p0}, LX/2q6;->B()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 539209
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    if-eq v0, v3, :cond_5

    .line 539210
    const-string v0, "Seek to: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539211
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 539212
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539213
    :goto_2
    invoke-virtual {p0}, LX/2q6;->y()V

    .line 539214
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 539215
    iget-object v1, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 539216
    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v1}, LX/2q6;->a(LX/2qD;)V

    .line 539217
    iget-object v1, p0, LX/2q6;->h:LX/2q9;

    if-eqz v1, :cond_2

    .line 539218
    iget-object v1, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v1}, LX/2q9;->b()V

    .line 539219
    :cond_2
    if-ne v0, v3, :cond_8

    .line 539220
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v5

    .line 539221
    :goto_3
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    if-lez v0, :cond_3

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 539222
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    sget v1, LX/3FM;->a:I

    invoke-virtual {v0, v1}, LX/3FM;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 539223
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 539224
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 539225
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    sub-int/2addr v2, v5

    int-to-long v6, v2

    invoke-virtual {v1, v0, v6, v7}, LX/3FM;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 539226
    :cond_3
    iput v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539227
    invoke-static {p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->m(LX/04g;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 539228
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v7, p0, LX/2q6;->w:LX/04D;

    iget-object v8, p0, LX/2q6;->z:LX/04g;

    iget-object v8, v8, LX/04g;->value:Ljava/lang/String;

    iget-object v9, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v9}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 539229
    :goto_4
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/2qL;

    sget-object v2, LX/7IB;->b:LX/7IB;

    invoke-direct {v1, v5, v2}, LX/2qL;-><init>(ILX/7IB;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 539230
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 539231
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 539232
    invoke-virtual {p2}, LX/7K4;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 539233
    iget v0, p2, LX/7K4;->d:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    .line 539234
    :cond_4
    :goto_5
    iget-object v0, p0, LX/2q6;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    .line 539235
    return-void

    .line 539236
    :cond_5
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    if-lez v0, :cond_9

    .line 539237
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v4

    add-int/2addr v0, v4

    .line 539238
    const-string v4, "Seek to: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {p0, v4, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539239
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto/16 :goto_2

    .line 539240
    :cond_6
    invoke-virtual {p0, p1, v5}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/04g;I)V

    goto :goto_4

    .line 539241
    :cond_7
    invoke-static {p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->m(LX/04g;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 539242
    iput v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    goto :goto_5

    :cond_8
    move v5, v0

    goto/16 :goto_3

    :cond_9
    move v0, v3

    goto/16 :goto_2
.end method

.method private e(I)LX/7Jj;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 539003
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 539004
    sparse-switch p1, :sswitch_data_0

    .line 539005
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 539006
    goto :goto_0

    .line 539007
    :sswitch_0
    const-string v0, "onError MEDIA_ERROR_IO"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539008
    sget-object v0, LX/7Jj;->ERROR_IO:LX/7Jj;

    goto :goto_1

    .line 539009
    :sswitch_1
    const-string v0, "onError MEDIA_ERROR_MALFORMED"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539010
    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    goto :goto_1

    .line 539011
    :sswitch_2
    const-string v0, "onError MEDIA_ERROR_TIMED_OUT"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539012
    sget-object v0, LX/7Jj;->TIMED_OUT:LX/7Jj;

    goto :goto_1

    .line 539013
    :sswitch_3
    const-string v0, "onError MEDIA_ERROR_UNSUPPORTED"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539014
    sget-object v0, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f2 -> :sswitch_3
        -0x3ef -> :sswitch_1
        -0x3ec -> :sswitch_0
        -0x6e -> :sswitch_2
    .end sparse-switch
.end method

.method private i(LX/04g;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v12, 0x0

    .line 539243
    const-string v0, "resetNow: getting rid of the player in a separate thread"

    new-array v1, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539244
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-static {v0, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/media/MediaPlayer;Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    .line 539245
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/media/MediaPlayer;Z)V

    .line 539246
    iput-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    .line 539247
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    if-eqz v0, :cond_0

    .line 539248
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    invoke-interface {v0}, LX/7Jq;->a()V

    .line 539249
    iput-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    .line 539250
    :cond_0
    const-string v0, "resetNow: MediaPlayer is null (and soon to be released)"

    new-array v1, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539251
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 539252
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 539253
    iput v12, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    .line 539254
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    .line 539255
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz v0, :cond_1

    .line 539256
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 539257
    :cond_1
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-eq p1, v0, :cond_2

    .line 539258
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v5

    iget v6, p0, LX/2q6;->B:I

    iget-object v7, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p0, LX/2q6;->w:LX/04D;

    iget-object v9, p0, LX/2q6;->z:LX/04g;

    iget-object v9, v9, LX/04g;->value:Ljava/lang/String;

    iget-object v10, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v10, v10, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iget-object v11, p0, LX/2q6;->S:LX/097;

    iget-object v11, v11, LX/097;->value:Ljava/lang/String;

    invoke-virtual/range {v0 .. v11}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;ZLjava/lang/String;)LX/1C2;

    .line 539259
    :cond_2
    iput v12, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    .line 539260
    return-void
.end method

.method private static j(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V
    .locals 3

    .prologue
    .line 539261
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 539262
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->l(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V

    .line 539263
    :goto_0
    return-void

    .line 539264
    :cond_0
    iget-object v0, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$2;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V

    const v2, 0x783be7d5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private k(LX/04g;)V
    .locals 18

    .prologue
    .line 539265
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->p:LX/2px;

    invoke-virtual {v2}, LX/2px;->a()V

    .line 539266
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/2q6;->u:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    const/16 v3, 0x63

    if-le v2, v3, :cond_1

    .line 539267
    :cond_0
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539268
    invoke-static/range {p0 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->j(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V

    .line 539269
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/2q6;->D()V

    .line 539270
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    .line 539271
    return-void

    .line 539272
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v2, v3, :cond_4

    .line 539273
    const-string v2, "current state = %s, seek time = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->D:LX/2qD;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539274
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 539275
    :goto_1
    if-eqz v2, :cond_2

    .line 539276
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    .line 539277
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539278
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 539279
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_0

    .line 539280
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 539281
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, LX/04g;->value:Ljava/lang/String;

    sget-object v3, LX/04g;->BY_ANDROID:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 539282
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539283
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    move-object/from16 v0, p0

    iget v3, v0, LX/2q6;->B:I

    if-ge v2, v3, :cond_7

    .line 539284
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539285
    :cond_7
    const-string v2, "stop-for-pause: %s, seek time = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, LX/04g;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539286
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->m(LX/04g;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 539287
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 539288
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 539289
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_0
.end method

.method public static l(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 539290
    const-string v0, "pause"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539291
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539292
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_1

    .line 539293
    sget-object v0, LX/2qD;->STATE_PAUSED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 539294
    :cond_0
    :goto_0
    return-void

    .line 539295
    :cond_1
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 539296
    invoke-virtual {p0, p1}, LX/2q6;->f(LX/04g;)V

    goto :goto_0

    .line 539297
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539298
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539299
    invoke-interface {v0, p1, v4}, LX/2pf;->b(LX/04g;Z)V

    goto :goto_1

    .line 539300
    :cond_3
    invoke-virtual {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->g(LX/04g;)V

    .line 539301
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539302
    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    goto :goto_2
.end method

.method private static m(LX/04g;)Z
    .locals 1

    .prologue
    .line 539303
    sget-object v0, LX/2q6;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final E()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 539304
    const-string v0, "onCompletion"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539305
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->A:I

    .line 539306
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_0

    .line 539307
    invoke-virtual {p0}, LX/2q6;->C()V

    .line 539308
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/2qT;

    iget v2, p0, LX/2q6;->A:I

    sget-object v3, LX/7IF;->a:LX/7IF;

    invoke-direct {v1, v2, v3}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 539309
    :cond_0
    iput v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    .line 539310
    invoke-virtual {p0}, LX/2q6;->D()V

    .line 539311
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aA:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-ge v0, v1, :cond_3

    .line 539312
    :cond_1
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v4, v4}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 539313
    iput v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539314
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v4, v4}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->c(LX/04g;LX/7K4;)V

    .line 539315
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v4, v4}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->d(LX/04g;LX/7K4;)V

    .line 539316
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aA:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aA:I

    .line 539317
    :cond_2
    return-void

    .line 539318
    :cond_3
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 539319
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 539320
    iput v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539321
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    if-nez v0, :cond_4

    .line 539322
    invoke-virtual {p0}, LX/2q6;->n()V

    .line 539323
    sget-object v0, LX/2qk;->FROM_ONCOMPLETE:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 539324
    :cond_4
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539325
    iget v2, p0, LX/2q6;->A:I

    invoke-interface {v0, v2}, LX/2pf;->a(I)V

    goto :goto_0
.end method

.method public final F()V
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 539326
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_7

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 539327
    if-eqz v0, :cond_1

    .line 539328
    :goto_1
    return-void

    .line 539329
    :cond_1
    const-string v0, "Initializing media player"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v11

    .line 539330
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 539331
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    .line 539332
    const-string v0, "Set data source = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539333
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 539334
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539335
    iput v2, v0, LX/2q3;->h:I

    .line 539336
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/net/Uri;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 539337
    :catch_0
    move-exception v0

    .line 539338
    :try_start_1
    const-string v2, "dataSourceIoException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539339
    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 539340
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4

    .line 539341
    :cond_3
    :goto_2
    if-nez v1, :cond_2

    goto :goto_1

    .line 539342
    :cond_4
    :try_start_2
    const-string v0, "Data source is invalid. Try next one."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539343
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->J()Z

    move-result v1

    .line 539344
    if-nez v1, :cond_3

    .line 539345
    const-string v0, "No data source!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539346
    sget-object v0, LX/7Jj;->NO_SOURCE:LX/7Jj;

    .line 539347
    iget-object v2, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 539348
    iget-object v4, p0, LX/2q6;->G:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    goto :goto_3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 539349
    :cond_5
    goto :goto_2

    .line 539350
    :catch_1
    move-exception v0

    .line 539351
    :try_start_3
    const-string v1, "dataSourceNPE"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539352
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->J()Z

    move-result v1

    .line 539353
    if-nez v1, :cond_3

    .line 539354
    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    .line 539355
    :catch_2
    move-exception v9

    .line 539356
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539357
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539358
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0, v12, v11}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    .line 539359
    :cond_6
    :try_start_4
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->J()Z

    move-result v1

    .line 539360
    if-nez v1, :cond_3

    .line 539361
    throw v0
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    .line 539362
    :catch_3
    move-exception v9

    .line 539363
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539364
    const-string v0, "Caught IOException - Unable to open content %s"

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539365
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0, v12, v11}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    .line 539366
    :catch_4
    move-exception v9

    .line 539367
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539368
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539369
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0, v12, v11}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final G()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 539370
    const-string v0, "Retry loading video: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539371
    :try_start_0
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 539372
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539373
    iput v1, v0, LX/2q3;->h:I

    .line 539374
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ac:LX/3FK;

    iget-boolean v1, v1, LX/3FK;->g:Z

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Landroid/net/Uri;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539375
    iput-boolean v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    .line 539376
    :goto_0
    return-void

    .line 539377
    :catch_0
    :try_start_1
    const-string v0, "Caught IOException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    .line 539379
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539380
    iput-boolean v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    throw v0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 539381
    const-string v0, "setVolume"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539382
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(F)V

    .line 539383
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 539384
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 539385
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    if-lt v0, v1, :cond_1

    .line 539386
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 539387
    :cond_0
    :goto_0
    return-void

    .line 539388
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539389
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    sget v1, LX/3FM;->a:I

    invoke-virtual {v0, v1}, LX/3FM;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 539390
    iput-object p0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 539391
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 539392
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->q:I

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    sub-int/2addr v0, v1

    .line 539393
    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    if-lez v0, :cond_2

    int-to-long v0, v0

    :goto_1
    invoke-virtual {v3, v2, v0, v1}, LX/3FM;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public final a(ILX/04g;)V
    .locals 4

    .prologue
    .line 539394
    const-string v0, "seekTo"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539395
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539396
    iput p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->B:I

    .line 539397
    sget-object v0, LX/7KE;->a:[I

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    invoke-virtual {v1}, LX/2qD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 539398
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/2q6;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539399
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 539400
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 539401
    :cond_0
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz v0, :cond_1

    .line 539402
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0, p1}, LX/2q9;->a(I)V

    .line 539403
    :cond_1
    :goto_0
    return-void

    .line 539404
    :pswitch_0
    iput p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539405
    goto :goto_0

    .line 539406
    :pswitch_1
    iput p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 539407
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/04g;LX/7K4;)V

    .line 539408
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 17

    .prologue
    .line 539409
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->S:LX/097;

    iget-object v8, v7, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->z:LX/04g;

    iget-object v12, v7, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2q6;->N:LX/09M;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 539410
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 539411
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539412
    :goto_0
    invoke-virtual {p2}, LX/7K4;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v0, p2, LX/7K4;->d:I

    .line 539413
    :cond_0
    iget-object v3, p0, LX/2q6;->q:LX/16V;

    new-instance v4, LX/2qK;

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v4, v0, v5}, LX/2qK;-><init>(ILX/7IB;)V

    invoke-virtual {v3, v4}, LX/16V;->a(LX/1AD;)V

    .line 539414
    const-string v0, "play"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539415
    const-string v0, "%s, %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v4, v3, v2

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539416
    invoke-virtual {p2}, LX/7K4;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539417
    iget v0, p2, LX/7K4;->c:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539418
    :cond_1
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 539419
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->c(LX/04g;LX/7K4;)V

    .line 539420
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 539421
    if-nez v0, :cond_2

    .line 539422
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 539423
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ay:LX/0Uh;

    sget v1, LX/19n;->f:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_6

    .line 539424
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->F()V

    .line 539425
    :goto_2
    return-void

    .line 539426
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 539427
    goto :goto_1

    .line 539428
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 539429
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->d(LX/04g;LX/7K4;)V

    goto :goto_2

    .line 539430
    :cond_6
    iget-object v0, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$1;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    const v2, -0x1b2863a3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_2
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 539015
    const-string v0, "setVideoResolution"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 539016
    iget-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539017
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->am:LX/2oi;

    .line 539018
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 539019
    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_1

    .line 539020
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    .line 539021
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->al:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539022
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 539023
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 539024
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-nez v0, :cond_2

    .line 539025
    :cond_0
    :goto_1
    return-void

    .line 539026
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    .line 539027
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ak:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539028
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 539029
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 539030
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    .line 539031
    if-eqz v2, :cond_3

    .line 539032
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->c(LX/04g;)V

    .line 539033
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 539034
    :cond_3
    :try_start_0
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539035
    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    .line 539036
    :try_start_1
    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->reset()V

    .line 539037
    iget-object v3, p0, LX/2q6;->n:LX/2q3;

    invoke-virtual {v3, v0}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 539038
    iget-object v3, p0, LX/2q6;->n:LX/2q3;

    iget v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539039
    iput v4, v3, LX/2q3;->h:I

    .line 539040
    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    iget-object v4, p0, LX/2q6;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 539041
    const-string v0, "prepareAsync"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539042
    sget-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 539043
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 539044
    invoke-interface {v0}, LX/2pf;->b()V

    goto :goto_2

    .line 539045
    :cond_4
    new-instance v0, LX/7IH;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    iget-object v4, p0, LX/2q6;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-direct {v0, v3, v4}, LX/7IH;-><init>(Landroid/media/MediaPlayer;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 539046
    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 539047
    iput v3, v0, LX/7IG;->c:I

    .line 539048
    iget v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 539049
    iput v3, v0, LX/7IG;->d:I

    .line 539050
    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aq:LX/19Z;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v4}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v4

    invoke-virtual {v3, v4, v0}, LX/19Z;->a(ILX/7IG;)V

    .line 539051
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/09R;

    invoke-direct {v3}, LX/09R;-><init>()V

    invoke-virtual {v0, v3}, LX/16V;->a(LX/1AD;)V

    .line 539052
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 539053
    if-eqz v2, :cond_5

    .line 539054
    :try_start_2
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    sget-object v1, LX/7K4;->a:LX/7K4;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->c(LX/04g;LX/7K4;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_1

    .line 539055
    :catch_0
    move-exception v9

    .line 539056
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539057
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539058
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    .line 539059
    :catch_1
    move-exception v0

    .line 539060
    :try_start_3
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 539061
    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    .line 539062
    :catch_2
    move-exception v9

    .line 539063
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539064
    const-string v0, "Caught IOException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539065
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    .line 539066
    :catch_3
    move-exception v0

    .line 539067
    :try_start_4
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 539068
    throw v0
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    .line 539069
    :catch_4
    move-exception v9

    .line 539070
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 539071
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539072
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    .line 539073
    :cond_5
    :try_start_5
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->l(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;LX/04g;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 538853
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    .line 538854
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->at:LX/3FL;

    .line 538855
    iput-object v1, v0, LX/2p8;->j:LX/2qG;

    .line 538856
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538857
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->at:LX/3FL;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    .line 538858
    iget-object p0, v1, LX/2p8;->a:Landroid/view/Surface;

    move-object v1, p0

    .line 538859
    invoke-virtual {v0, v1}, LX/3FL;->a(Landroid/view/Surface;)V

    .line 538860
    :cond_0
    return-void
.end method

.method public final a(LX/2qk;)V
    .locals 1

    .prologue
    .line 538861
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;Z)V

    .line 538862
    return-void
.end method

.method public final a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .locals 0

    .prologue
    .line 538863
    return-void
.end method

.method public final a(LX/7QO;)V
    .locals 1

    .prologue
    .line 538850
    iget-object v0, p0, LX/2q6;->i:LX/2qI;

    if-eqz v0, :cond_0

    .line 538851
    iget-object v0, p0, LX/2q6;->i:LX/2qI;

    invoke-interface {v0, p1}, LX/2qI;->a(LX/7QO;)V

    .line 538852
    :cond_0
    return-void
.end method

.method public final a(LX/7QP;)V
    .locals 1

    .prologue
    .line 538847
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, LX/2q9;->a(LX/7QP;)Z

    .line 538848
    return-void

    .line 538849
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .locals 6
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 538805
    const-string v0, "bindVideoSources"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 538806
    const-string v0, "bindVideoSources: %s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538807
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 538808
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 538809
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 538810
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ak:I

    .line 538811
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->al:I

    .line 538812
    iput-object v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    .line 538813
    iput-object v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    .line 538814
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->am:LX/2oi;

    .line 538815
    iput v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aA:I

    .line 538816
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538817
    const-string v0, "bindVideoSources: No valid video paths"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538818
    sget-object v0, LX/2qk;->FROM_BIND:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 538819
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    .line 538820
    iput-object v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    .line 538821
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->S:LX/097;

    .line 538822
    :goto_0
    return-void

    .line 538823
    :cond_0
    iput v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    .line 538824
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 538825
    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    if-eqz v1, :cond_4

    move v2, v3

    .line 538826
    :goto_1
    if-eqz v2, :cond_5

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    .line 538827
    :goto_2
    if-eqz v0, :cond_1

    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-virtual {v5, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 538828
    :cond_1
    sget-object v1, LX/2qk;->FROM_BIND:LX/2qk;

    invoke-virtual {p0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 538829
    :cond_2
    if-eqz v0, :cond_3

    .line 538830
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->S:LX/097;

    .line 538831
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    .line 538832
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    .line 538833
    :cond_3
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ah:Landroid/net/Uri;

    :goto_3
    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    .line 538834
    if-eqz v2, :cond_8

    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->al:I

    :goto_4
    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 538835
    if-eqz v2, :cond_9

    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    :goto_5
    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->am:LX/2oi;

    .line 538836
    const-string v0, "Video source (%s): %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/2q6;->S:LX/097;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538837
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 538838
    iput v1, v0, LX/2px;->i:I

    .line 538839
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aj:I

    .line 538840
    iput v1, v0, LX/2px;->j:I

    .line 538841
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2px;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 538842
    goto :goto_1

    .line 538843
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    goto :goto_2

    .line 538844
    :cond_7
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ag:Landroid/net/Uri;

    goto :goto_3

    .line 538845
    :cond_8
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ak:I

    goto :goto_4

    .line 538846
    :cond_9
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_5
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 538804
    return-void
.end method

.method public final a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 538800
    const-string v0, "mute"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 538801
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(F)V

    .line 538802
    return-void

    .line 538803
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 538797
    const-string v1, "isPlaying"

    iput-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 538798
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 538799
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 538787
    const-string v0, "getCurrentPosition"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 538788
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 538789
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 538790
    :cond_0
    :goto_0
    return v1

    .line 538791
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    .line 538792
    iget v0, p0, LX/2q6;->A:I

    if-lez v0, :cond_3

    iget v0, p0, LX/2q6;->A:I

    .line 538793
    :goto_1
    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    if-lez v3, :cond_2

    .line 538794
    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->p:I

    sub-int/2addr v2, v3

    .line 538795
    :cond_2
    invoke-static {v2, v1, v0}, LX/13m;->a(III)I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 538796
    goto :goto_1
.end method

.method public final b(LX/04g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 538769
    iget-object v0, p0, LX/2q6;->p:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 538770
    const-string v0, "stop"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->G:Ljava/lang/String;

    .line 538771
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538772
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    if-eqz v0, :cond_0

    .line 538773
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    invoke-interface {v0}, LX/7Jq;->a()V

    .line 538774
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    .line 538775
    :cond_0
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 538776
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->R:LX/04g;

    .line 538777
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 538778
    :cond_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ar:J

    .line 538779
    return-void

    .line 538780
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->L(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538781
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538782
    invoke-interface {v0, p1, v4}, LX/2pf;->c(LX/04g;Z)V

    goto :goto_0

    .line 538783
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aq:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/19Z;->a(I)V

    .line 538784
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->i(LX/04g;)V

    .line 538785
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538786
    invoke-interface {v0, p1}, LX/2pf;->a(LX/04g;)V

    goto :goto_1
.end method

.method public final b(Landroid/graphics/RectF;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 538763
    iget v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 538764
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    if-eqz v0, :cond_0

    .line 538765
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Potential IndexOutOfBoundsException:mCurrentDataSourceIndex = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but the size of the datastructure = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v10, v9

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 538766
    :cond_0
    :goto_0
    return-void

    .line 538767
    :cond_1
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->an:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 538768
    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->j()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->k()I

    move-result v3

    iget-object v4, v0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    if-nez p1, :cond_2

    iget-object p1, v0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    :cond_2
    invoke-static {v2, v3, v4, p1}, LX/7K8;->a(IILX/2oF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2p8;->a(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 538758
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 538759
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 538760
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Landroid/view/Surface;)V

    .line 538761
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 538762
    :cond_0
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 538757
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public final c(LX/04g;)V
    .locals 0

    .prologue
    .line 538864
    invoke-super {p0, p1}, LX/2q6;->c(LX/04g;)V

    .line 538865
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->k(LX/04g;)V

    .line 538866
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538867
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/2oi;
    .locals 1

    .prologue
    .line 538868
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->am:LX/2oi;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 538869
    sget-object v0, LX/2qk;->EXTERNAL:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 538870
    return-void
.end method

.method public final g(LX/04g;)V
    .locals 24
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 538871
    const-string v2, "pauseNow: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, LX/04g;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538872
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ad:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    .line 538873
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 538874
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 538875
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    if-eqz v2, :cond_0

    .line 538876
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v2}, LX/2q9;->c()V

    .line 538877
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 538878
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 538879
    invoke-interface {v2}, LX/2pf;->c()V

    goto :goto_0

    .line 538880
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 538881
    invoke-virtual/range {p0 .. p0}, LX/2q6;->n()V

    .line 538882
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ab:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->h()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v4}, LX/2p8;->i()I

    move-result v4

    invoke-static {v2, v3, v4}, LX/7K8;->b(III)D

    move-result-wide v2

    .line 538883
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    invoke-virtual {v4, v2, v3, v2, v3}, LX/2p8;->a(DD)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->H:Landroid/graphics/Bitmap;

    .line 538884
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 538885
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->H:Landroid/graphics/Bitmap;

    invoke-interface {v2, v4}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 538886
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v7

    .line 538887
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->m(LX/04g;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 538888
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->z:LX/04g;

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v12, v12, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v2 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 538889
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    sget-object v4, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v7, v4}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 538890
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ax:LX/3FM;

    sget v3, LX/3FM;->a:I

    invoke-virtual {v2, v3}, LX/3FM;->removeMessages(I)V

    .line 538891
    return-void

    .line 538892
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ai:LX/09L;

    iget-object v11, v2, LX/09L;->value:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, LX/2q6;->B:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->z:LX/04g;

    iget-object v0, v2, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->S:LX/097;

    iget-object v0, v2, LX/097;->value:Ljava/lang/String;

    move-object/from16 v23, v0

    move v15, v7

    invoke-virtual/range {v8 .. v23}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    goto :goto_2
.end method

.method public final k()Landroid/view/View;
    .locals 1

    .prologue
    .line 538893
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->as:LX/2p8;

    .line 538894
    iget-object p0, v0, LX/2p8;->i:Landroid/view/TextureView;

    move-object v0, p0

    .line 538895
    goto :goto_0
.end method

.method public final onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 2

    .prologue
    .line 538896
    iput p2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    .line 538897
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538898
    invoke-interface {v0, p2}, LX/2pf;->b(I)V

    goto :goto_0

    .line 538899
    :cond_0
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 538900
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->E()V

    .line 538901
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 12

    .prologue
    .line 538902
    const-string v0, "MediaPlayerError: what=%s, extra=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538903
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->au:LX/7Jq;

    instance-of v0, v0, LX/7Jr;

    .line 538904
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ac:LX/3FK;

    iget-boolean v1, v1, LX/3FK;->g:Z

    if-eqz v1, :cond_0

    .line 538905
    const-string v0, "MediaPlayer in retry"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538906
    const/4 v0, 0x1

    .line 538907
    :goto_0
    return v0

    .line 538908
    :cond_0
    const/4 v1, 0x1

    .line 538909
    const/16 v2, -0x3ec

    if-eq p2, v2, :cond_1

    const/16 v2, -0x3ef

    if-eq p2, v2, :cond_1

    if-ne p2, v1, :cond_8

    :cond_1
    iget-object v2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_8

    :goto_1
    move v1, v1

    .line 538910
    if-eqz v1, :cond_3

    .line 538911
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aw:Z

    .line 538912
    if-eqz v0, :cond_2

    .line 538913
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 538914
    :goto_2
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$5;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$5;-><init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 538915
    const/4 v0, 0x1

    goto :goto_0

    .line 538916
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    goto :goto_2

    .line 538917
    :cond_3
    sget-object v0, LX/2qD;->STATE_ERROR:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 538918
    sget-object v0, LX/2qD;->STATE_ERROR:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 538919
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ap:I

    .line 538920
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->ao:I

    .line 538921
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    .line 538922
    sget-object v1, LX/2qk;->FROM_ERROR:LX/2qk;

    invoke-virtual {p0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(LX/2qk;)V

    .line 538923
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_4

    .line 538924
    invoke-direct {p0, p3}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->e(I)LX/7Jj;

    move-result-object v0

    .line 538925
    sget-object v1, LX/7Jj;->UNKNOWN:LX/7Jj;

    if-ne v0, v1, :cond_4

    .line 538926
    invoke-direct {p0, p2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->e(I)LX/7Jj;

    move-result-object v0

    .line 538927
    :cond_4
    iget-object v1, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v2, LX/7Jj;->UNKNOWN:LX/7Jj;

    iget-object v2, v2, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 538928
    sparse-switch p2, :sswitch_data_0

    .line 538929
    const-string v1, "onError unknown with what=%s, extra=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    move-object v11, v0

    .line 538930
    :goto_3
    sget-object v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->Y:Ljava/lang/Class;

    const-string v1, "playback failed: %d, %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538931
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    if-eqz v0, :cond_6

    .line 538932
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    const-string v1, "MediaPlayer Error: %d %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 538933
    :cond_6
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538934
    iget-object v2, p0, LX/2q6;->G:Ljava/lang/String;

    invoke-interface {v0, v2, v11}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    goto :goto_4

    .line 538935
    :sswitch_0
    const-string v1, "onError MEDIA_ERROR_UNKNOWN"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v11, v0

    .line 538936
    goto :goto_3

    .line 538937
    :sswitch_1
    const-string v0, "onError MEDIA_ERROR_SERVER_DIED"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538938
    sget-object v0, LX/7Jj;->SERVER_DIED:LX/7Jj;

    move-object v11, v0

    .line 538939
    goto :goto_3

    .line 538940
    :cond_7
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 538941
    sparse-switch p2, :sswitch_data_0

    .line 538942
    const-string v0, "onInfo unknown with codes what=%s, extra=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538943
    :goto_0
    return v4

    .line 538944
    :sswitch_0
    const-string v0, "onInfo MEDIA_INFO_UNKNOWN"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538945
    :sswitch_1
    const-string v0, "onInfo MEDIA_INFO_VIDEO_TRACK_LAGGING"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538946
    :sswitch_2
    const-string v0, "onInfo MEDIA_INFO_BUFFERING_START"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538947
    :sswitch_3
    const-string v0, "onInfo MEDIA_INFO_BUFFERING_END"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538948
    :sswitch_4
    const-string v0, "onInfo MEDIA_INFO_BAD_INTERLEAVING"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538949
    :sswitch_5
    const-string v0, "onInfo MEDIA_INFO_NOT_SEEKABLE"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 538950
    :sswitch_6
    const-string v0, "onInfo MEDIA_INFO_METADATA_UPDATE"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2bc -> :sswitch_1
        0x2bd -> :sswitch_2
        0x2be -> :sswitch_3
        0x320 -> :sswitch_4
        0x321 -> :sswitch_5
        0x322 -> :sswitch_6
    .end sparse-switch
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 7

    .prologue
    .line 538951
    const-string v0, "onPrepared"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538952
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 538953
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->A:I

    .line 538954
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->aq:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    iget v2, p0, LX/2q6;->A:I

    invoke-virtual {v0, v1, v2}, LX/19Z;->a(II)V

    .line 538955
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538956
    invoke-interface {v0}, LX/2pf;->a()V

    goto :goto_0

    .line 538957
    :cond_0
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09S;

    invoke-direct {v1}, LX/09S;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 538958
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 538959
    new-instance v0, LX/7IE;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v3

    .line 538960
    invoke-static {v4}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    .line 538961
    if-nez v5, :cond_5

    .line 538962
    const-string v5, "unknown"

    .line 538963
    :goto_1
    move-object v4, v5

    .line 538964
    const/4 v5, 0x0

    iget-object v6, p0, LX/2q6;->S:LX/097;

    iget-object v6, v6, LX/097;->value:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, LX/7IE;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->F:LX/7IE;

    .line 538965
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->av:Z

    if-eqz v0, :cond_1

    .line 538966
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 538967
    :cond_1
    :goto_2
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_3

    .line 538968
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    iget-object v1, p0, LX/2q6;->C:LX/7K4;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->d(LX/04g;LX/7K4;)V

    .line 538969
    :cond_2
    :goto_3
    return-void

    .line 538970
    :catch_0
    move-exception v0

    .line 538971
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->Y:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onPrepared"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mediaPlayer.setLooping failed"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 538972
    iput-object v0, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 538973
    move-object v0, v1

    .line 538974
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 538975
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->az:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_2

    .line 538976
    :cond_3
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_4

    .line 538977
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->c(LX/04g;)V

    goto :goto_3

    .line 538978
    :cond_4
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 538979
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->b(LX/04g;)V

    goto :goto_3

    .line 538980
    :cond_5
    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    .line 538981
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 538982
    const/4 v5, 0x0

    goto :goto_1

    .line 538983
    :cond_6
    const-string v6, ".webm"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 538984
    const-string v5, "vp9"

    goto/16 :goto_1

    .line 538985
    :cond_7
    const-string v6, ".mp4"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 538986
    const-string v5, "mp4"

    goto/16 :goto_1

    .line 538987
    :cond_8
    const-string v6, ".mpd"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 538988
    const-string v5, "dash"

    goto/16 :goto_1

    .line 538989
    :cond_9
    const-string v6, ".m3u8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string v6, ".m3u"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 538990
    :cond_a
    const-string v5, "hls"

    goto/16 :goto_1

    .line 538991
    :cond_b
    const-string v5, "unknown"

    goto/16 :goto_1
.end method

.method public final onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 538992
    const-string v0, "onSeekComplete"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538993
    return-void
.end method

.method public final onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 538994
    const-string v0, "onVideoSizeChanged: %sx%s"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538995
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 538996
    :cond_0
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    if-eqz v0, :cond_1

    .line 538997
    sget-object v0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->Y:Ljava/lang/Class;

    const-string v1, "MediaPlayer.OnVideoSizeChanged with invalid width or height. Width: %d, Height: %d, PlayerType: %s, Video Id: %s, Source %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, LX/2q6;->y:LX/04G;

    iget-object v3, v3, LX/04G;->value:Ljava/lang/String;

    aput-object v3, v2, v6

    const/4 v3, 0x3

    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;->af:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538998
    :cond_1
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 538999
    invoke-interface {v0, p2, p3}, LX/2pf;->a(II)V

    goto :goto_0

    .line 539000
    :cond_2
    return-void
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 539001
    const-string v0, "old_api_psr"

    return-object v0
.end method

.method public final z()V
    .locals 0

    .prologue
    .line 539002
    return-void
.end method
