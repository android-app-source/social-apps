.class public Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;
.super LX/2q6;
.source ""

# interfaces
.implements LX/04l;
.implements LX/09F;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static final Y:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final aX:Ljava/lang/String;

.field private static final bj:Z


# instance fields
.field public final Z:LX/2px;

.field private aA:I

.field private aB:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aC:Ljava/lang/String;

.field public aD:I

.field public aE:I

.field public aF:I

.field public aG:LX/19Z;

.field private aH:J

.field private aI:J

.field public aJ:LX/0wq;

.field public aK:LX/0wp;

.field public final aL:LX/0ka;

.field public final aM:LX/04m;

.field public final aN:LX/0oz;

.field public final aO:LX/0YR;

.field public final aP:LX/1AA;

.field public aQ:LX/04o;

.field public aR:I

.field public aS:I

.field public aT:I

.field public aU:Ljava/lang/String;

.field public aV:Ljava/lang/String;

.field private aW:LX/03z;

.field public aY:LX/2p8;

.field private final aZ:LX/2qF;

.field private final aa:I

.field private final ab:Landroid/os/Handler$Callback;

.field public final ac:Landroid/os/Handler;

.field public volatile ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

.field public volatile ae:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

.field public volatile af:Landroid/view/Surface;

.field public ag:LX/04j;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/04j;",
            ">;"
        }
    .end annotation
.end field

.field private final ai:LX/16V;

.field public aj:Lcom/facebook/exoplayer/ipc/MediaRenderer;

.field public ak:Lcom/facebook/exoplayer/ipc/MediaRenderer;

.field public al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

.field public am:I

.field public an:I

.field private ao:F

.field public ap:Landroid/net/Uri;

.field private aq:Landroid/net/Uri;

.field private ar:Landroid/net/Uri;

.field public as:Landroid/net/Uri;

.field public at:Ljava/lang/String;

.field private au:I

.field private av:I

.field private aw:I

.field public ax:I

.field public ay:Ljava/lang/String;

.field public az:LX/2oi;

.field private bA:Ljava/lang/String;

.field private bB:Z

.field public bC:LX/2q5;

.field public final ba:LX/1m6;

.field private final bb:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final bc:Z

.field private final bd:I

.field private be:I

.field private final bf:Z

.field public bg:LX/041;

.field public final bh:LX/0TD;

.field public bi:LX/2qA;

.field private final bk:I

.field public bl:I

.field private bm:Z

.field private final bn:Ljava/lang/Object;

.field private final bo:Z

.field private bp:I

.field private bq:LX/04g;

.field private br:LX/04g;

.field public bs:Z

.field public final bt:LX/2qC;

.field private final bu:Ljava/lang/Object;

.field public final bv:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field public final bw:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/04J;",
            ">;"
        }
    .end annotation
.end field

.field private bx:Z

.field private by:Z

.field public bz:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 470353
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 470354
    const-class v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    .line 470355
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bj:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/2pf;LX/1C2;LX/0TD;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/0wq;LX/2q9;LX/2pw;LX/2qI;LX/2px;LX/13m;LX/2q3;LX/0Ot;LX/0Or;LX/16V;ZLX/0wp;LX/0ka;LX/04m;LX/0oz;LX/0ad;LX/0Uh;ZLX/2qA;Landroid/os/Looper;LX/19j;LX/2qC;LX/0Ot;LX/0YR;LX/1AA;LX/19a;LX/0Ot;LX/0Ot;LX/2q5;)V
    .locals 23
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/engine/IsPausedBitmapEnabled;
        .end annotation
    .end param
    .param p32    # Landroid/os/Looper;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            "LX/2pf;",
            "LX/1C2;",
            "LX/0TD;",
            "Ljava/lang/Boolean;",
            "Z",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/19Z;",
            "LX/0So;",
            "LX/0wq;",
            "Lcom/facebook/video/subtitles/controller/SubtitleAdapter;",
            "LX/2pw;",
            "LX/2qI;",
            "LX/2px;",
            "LX/13m;",
            "LX/2q3;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/0Or",
            "<",
            "LX/04j;",
            ">;",
            "LX/16V;",
            "Z",
            "LX/0wp;",
            "LX/0ka;",
            "LX/04m;",
            "LX/0oz;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Z",
            "LX/2qA;",
            "Landroid/os/Looper;",
            "LX/19j;",
            "LX/2qC;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0YR;",
            "LX/1AA;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/04J;",
            ">;",
            "LX/2q5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470053
    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p20

    move-object/from16 v8, p5

    move-object/from16 v9, p16

    move-object/from16 v10, p14

    move-object/from16 v12, p10

    move-object/from16 v13, p8

    move/from16 v14, p9

    move-object/from16 v15, p12

    move-object/from16 v16, p17

    move-object/from16 v17, p18

    move-object/from16 v18, p19

    move-object/from16 v19, p6

    move-object/from16 v20, p28

    move-object/from16 v21, p29

    move-object/from16 v22, p33

    invoke-direct/range {v2 .. v22}, LX/2q6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/2q9;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;Ljava/lang/Boolean;ZLX/0So;LX/2px;LX/13m;LX/2q3;LX/1C2;LX/0ad;LX/0Uh;LX/19j;)V

    .line 470054
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    .line 470055
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    .line 470056
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    .line 470057
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    .line 470058
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    .line 470059
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    .line 470060
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    .line 470061
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    .line 470062
    sget-object v2, LX/03z;->UNKNOWN:LX/03z;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aW:LX/03z;

    .line 470063
    new-instance v2, LX/2qF;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/2qF;-><init>(LX/2q6;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aZ:LX/2qF;

    .line 470064
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->be:I

    .line 470065
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bk:I

    .line 470066
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    .line 470067
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bn:Ljava/lang/Object;

    .line 470068
    sget-object v2, LX/04g;->UNSET:LX/04g;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    .line 470069
    sget-object v2, LX/04g;->UNSET:LX/04g;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->br:LX/04g;

    .line 470070
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bu:Ljava/lang/Object;

    .line 470071
    const-string v2, "ExoVideoPlayerClient.initVariables"

    const v3, 0x33e9578c

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 470072
    :try_start_0
    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah:LX/0Or;

    .line 470073
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    .line 470074
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->I:Landroid/view/Surface;

    .line 470075
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->r:LX/0ad;

    sget-short v3, LX/0ws;->eG:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bz:Z

    .line 470076
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->r:LX/0ad;

    sget-short v3, LX/0ws;->eB:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bx:Z

    .line 470077
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->r:LX/0ad;

    sget-short v3, LX/0ws;->eF:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->by:Z

    .line 470078
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->r:LX/0ad;

    sget-char v3, LX/0ws;->dK:C

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bA:Ljava/lang/String;

    .line 470079
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->r:LX/0ad;

    sget-short v3, LX/0ws;->dN:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bB:Z

    .line 470080
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->af:Landroid/view/Surface;

    .line 470081
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aU:Ljava/lang/String;

    .line 470082
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aV:Ljava/lang/String;

    .line 470083
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 470084
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    .line 470085
    move/from16 v0, p23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bf:Z

    .line 470086
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x41dfffffffc00000L    # 2.147483647E9

    mul-double/2addr v2, v4

    double-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bd:I

    .line 470087
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->b:Landroid/content/Context;

    const/high16 v3, 0x43960000    # 300.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aa:I

    .line 470088
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bh:LX/0TD;

    .line 470089
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470090
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v2

    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 470091
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    .line 470092
    new-instance v2, LX/09M;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->m:LX/0So;

    move-object/from16 v0, p38

    move-object/from16 v1, p35

    invoke-direct {v2, v3, v0, v1}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N:LX/09M;

    .line 470093
    new-instance v2, LX/09M;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->m:LX/0So;

    move-object/from16 v0, p38

    move-object/from16 v1, p35

    invoke-direct {v2, v3, v0, v1}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O:LX/09M;

    .line 470094
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 470095
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->E:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 470096
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    .line 470097
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    .line 470098
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aL:LX/0ka;

    .line 470099
    move-object/from16 v0, p26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aM:LX/04m;

    .line 470100
    move-object/from16 v0, p27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aN:LX/0oz;

    .line 470101
    move-object/from16 v0, p37

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aP:LX/1AA;

    .line 470102
    move/from16 v0, p30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bc:Z

    .line 470103
    move-object/from16 v0, p36

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aO:LX/0YR;

    .line 470104
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/2q9;->a(LX/2pw;)V

    .line 470105
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    new-instance v3, LX/2qH;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/2qH;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-virtual {v2, v3}, LX/2q9;->a(LX/2qI;)V

    .line 470106
    move-object/from16 v0, p31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bi:LX/2qA;

    .line 470107
    new-instance v2, LX/1m6;

    invoke-direct {v2}, LX/1m6;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    .line 470108
    new-instance v2, LX/2qJ;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/2qJ;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ab:Landroid/os/Handler$Callback;

    .line 470109
    if-nez p32, :cond_0

    new-instance v2, Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ab:Landroid/os/Handler$Callback;

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    .line 470110
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bp:I

    .line 470111
    move-object/from16 v0, p34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bt:LX/2qC;

    .line 470112
    move-object/from16 v0, p41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bC:LX/2q5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470113
    const v2, -0x63f76f5f

    invoke-static {v2}, LX/02m;->a(I)V

    .line 470114
    const-string v2, "ExoVideoPlayerClient.registerOn"

    const v3, -0x19233ffe

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 470115
    :try_start_1
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    .line 470116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2px;->a(LX/16V;)V

    .line 470117
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->n:LX/2q3;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2q3;->a(LX/16V;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 470118
    const v2, 0x59082951

    invoke-static {v2}, LX/02m;->a(I)V

    .line 470119
    move-object/from16 v0, p13

    iget-boolean v2, v0, LX/0wq;->N:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bo:Z

    .line 470120
    move-object/from16 v0, p35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bb:LX/0Ot;

    .line 470121
    move-object/from16 v0, p39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    .line 470122
    move-object/from16 v0, p40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bw:LX/0Ot;

    .line 470123
    return-void

    .line 470124
    :cond_0
    :try_start_2
    new-instance v2, Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ab:Landroid/os/Handler$Callback;

    move-object/from16 v0, p32

    invoke-direct {v2, v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 470125
    :catchall_0
    move-exception v2

    const v3, -0x69487bf3

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 470126
    :catchall_1
    move-exception v2

    const v3, -0x135e11e3

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method

.method public static synthetic I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470052
    sget-object v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    return-object v0
.end method

.method private K()V
    .locals 3

    .prologue
    .line 470045
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bn:Ljava/lang/Object;

    monitor-enter v1

    .line 470046
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bm:Z

    if-nez v0, :cond_0

    .line 470047
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bm:Z

    .line 470048
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04j;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    .line 470049
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    const-class v2, LX/04k;

    invoke-virtual {v0, v2, p0}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 470050
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    const-class v2, LX/0Je;

    invoke-virtual {v0, v2, p0}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 470051
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private L()V
    .locals 3

    .prologue
    .line 470039
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bn:Ljava/lang/Object;

    monitor-enter v1

    .line 470040
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bm:Z

    if-eqz v0, :cond_0

    .line 470041
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    const-class v2, LX/04k;

    invoke-virtual {v0, v2, p0}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 470042
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    const-class v2, LX/0Je;

    invoke-virtual {v0, v2, p0}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 470043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bm:Z

    .line 470044
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z
    .locals 1

    .prologue
    .line 470038
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z
    .locals 1

    .prologue
    .line 470037
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z
    .locals 2

    .prologue
    .line 470036
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()V
    .locals 3

    .prologue
    .line 470032
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->by:Z

    if-eqz v0, :cond_0

    .line 470033
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470034
    :goto_0
    return-void

    .line 470035
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bh:LX/0TD;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$1;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    const v2, -0x6f7f6886

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static Q(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 11

    .prologue
    .line 470000
    const-string v0, "Initializing ExoPlayer"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470001
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->K()V

    .line 470002
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 470003
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    .line 470004
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 470005
    const-string v0, "Set data source = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470006
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 470007
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 470008
    iput v1, v0, LX/2q3;->h:I

    .line 470009
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Landroid/net/Uri;)V

    .line 470010
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 470011
    :goto_0
    return-void

    .line 470012
    :cond_1
    const-string v0, "Data source is invalid. Try next one."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470013
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->X()Z

    move-result v1

    .line 470014
    if-nez v1, :cond_2

    .line 470015
    const-string v0, "No data source!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470016
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470017
    iget-object v3, p0, LX/2q6;->l:LX/0Sh;

    new-instance v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$2;

    invoke-direct {v4, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$2;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v3, v4}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 470018
    :catch_0
    move-exception v0

    .line 470019
    :try_start_1
    const-string v1, "dataSourceNPE"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470020
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->X()Z

    move-result v1

    .line 470021
    if-nez v1, :cond_3

    .line 470022
    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    .line 470023
    :catch_1
    move-exception v9

    .line 470024
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 470025
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470026
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v9, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 470027
    :goto_2
    if-nez v0, :cond_0

    goto :goto_0

    .line 470028
    :catch_2
    move-exception v9

    .line 470029
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 470030
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470031
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v9, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private static R(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 2

    .prologue
    .line 469998
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$3;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$3;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 469999
    return-void
.end method

.method private S()Z
    .locals 2

    .prologue
    .line 469997
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private T()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 469841
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-object v1, v1, LX/0wq;->S:LX/0xT;

    sget-object v2, LX/0xT;->DEFAULT:LX/0xT;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->T:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2q6;->g:LX/1C2;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->U:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public static U(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z
    .locals 3

    .prologue
    .line 469983
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    .line 469984
    iget-object v1, v0, LX/0wq;->ae:LX/0Uh;

    sget v2, LX/19n;->n:I

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 469985
    return v0
.end method

.method public static W(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 469971
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 469972
    :cond_0
    :goto_0
    return-void

    .line 469973
    :cond_1
    const-string v0, "file"

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 469974
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    goto :goto_0

    .line 469975
    :cond_2
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 469976
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 469977
    const/16 v0, 0x18

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 469978
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469979
    :goto_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 469980
    :catch_0
    move-exception v0

    .line 469981
    sget-object v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v3, "Couldnt get media metadata, assume no rotation"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 469982
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    goto :goto_1
.end method

.method private X()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 469951
    const-string v0, "moveToNextVideoSource: %s"

    new-array v1, v2, [Ljava/lang/Object;

    iget v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469952
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    .line 469953
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    if-gez v0, :cond_0

    .line 469954
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    .line 469955
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 469956
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 469957
    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v1, :cond_5

    .line 469958
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    .line 469959
    iget-object v1, p0, LX/2q6;->S:LX/097;

    sget-object v4, LX/097;->FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

    if-eq v1, v4, :cond_1

    .line 469960
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S:LX/097;

    .line 469961
    :cond_1
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 469962
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    if-nez v1, :cond_2

    .line 469963
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    .line 469964
    :goto_1
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    if-nez v0, :cond_4

    move v0, v2

    .line 469965
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AbrManifest is null: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "PreferredVideoUri is null: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469966
    :cond_2
    :goto_3
    return v2

    :cond_3
    move v1, v3

    .line 469967
    goto :goto_1

    :cond_4
    move v0, v3

    .line 469968
    goto :goto_2

    .line 469969
    :cond_5
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    goto :goto_0

    :cond_6
    move v2, v3

    .line 469970
    goto :goto_3
.end method

.method public static Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 469949
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469950
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static Z(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 10

    .prologue
    .line 469937
    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v4, :cond_0

    .line 469938
    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bt:LX/2qC;

    iget-object v5, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2qC;->b(Ljava/lang/String;)LX/7Oe;

    move-result-object v4

    .line 469939
    if-eqz v4, :cond_0

    .line 469940
    invoke-virtual {p0}, LX/2q6;->s()I

    move-result v5

    .line 469941
    int-to-long v6, v5

    iget-wide v8, v4, LX/7Oe;->d:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    iget-object v8, p0, LX/2q6;->s:LX/19j;

    iget v8, v8, LX/19j;->t:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 469942
    const-string v6, "StreamDriedOut is triggered, internal position %d, interrupted position %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    iget-wide v8, v4, LX/7Oe;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v5

    invoke-virtual {p0, v6, v7}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469943
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->H()V

    .line 469944
    :cond_0
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469945
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$15;

    invoke-direct {v3, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$15;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 469946
    iget-boolean v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    if-eqz v2, :cond_1

    .line 469947
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$16;

    invoke-direct {v3, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$16;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 469948
    :cond_2
    return-void
.end method

.method private a(J)I
    .locals 3

    .prologue
    .line 469931
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 469932
    const/4 v0, 0x0

    .line 469933
    :goto_0
    return v0

    .line 469934
    :cond_0
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    if-eqz v0, :cond_1

    .line 469935
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    goto :goto_0

    .line 469936
    :cond_1
    long-to-int v0, p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 469928
    const/4 v0, -0x1

    .line 469929
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 469930
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method private a(LX/04g;ZI)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 469918
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->c()V

    .line 469919
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 469920
    invoke-static {p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469921
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v7, p0, LX/2q6;->w:LX/04D;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v8, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v9, v5, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    move v5, p3

    invoke-virtual/range {v0 .. v9}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 469922
    :goto_0
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 469923
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$5;

    invoke-direct {v1, p0, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$5;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;I)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 469924
    return-void

    .line 469925
    :cond_0
    if-nez v0, :cond_1

    if-nez p2, :cond_2

    .line 469926
    :cond_1
    invoke-virtual {p0, p1, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/04g;I)V

    goto :goto_0

    .line 469927
    :cond_2
    const-string v0, "InternalRetry, skip logging"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(LX/2qk;Z)V
    .locals 1

    .prologue
    .line 469916
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;ZZ)V

    .line 469917
    return-void
.end method

.method private a(LX/2qk;ZZ)V
    .locals 7

    .prologue
    .line 469889
    const-string v0, "release"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469890
    const-string v0, "%s, %s, right away? %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, LX/2qk;->value:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469891
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 469892
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 469893
    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 469894
    const-string v0, "unprepare ExoPlayer from %s"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p1, LX/2qk;->value:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469895
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    invoke-virtual {v0, v1}, LX/19Z;->a(I)V

    .line 469896
    iput v6, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    .line 469897
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    .line 469898
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469899
    :goto_0
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 469900
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    .line 469901
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 469902
    if-nez p3, :cond_0

    .line 469903
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 469904
    :cond_0
    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aV:Ljava/lang/String;

    .line 469905
    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aU:Ljava/lang/String;

    .line 469906
    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->F:LX/7IE;

    .line 469907
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    .line 469908
    iput v6, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469909
    iput-boolean v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 469910
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    .line 469911
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    .line 469912
    return-void

    .line 469913
    :cond_1
    if-eqz p2, :cond_2

    .line 469914
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-static {p0, v1, v0, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)V

    goto :goto_0

    .line 469915
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bh:LX/0TD;

    new-instance v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$9;

    invoke-direct {v2, p0, v0, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$9;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;ZZ)V

    const p1, -0x42b17333

    invoke-static {v1, v2, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private a(LX/7Jj;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 22

    .prologue
    .line 469849
    const-string v4, "handleError: %s, exception: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469850
    sget-object v4, LX/7Jj;->DISMISS:LX/7Jj;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->s:LX/19j;

    iget-boolean v4, v4, LX/19j;->ay:Z

    if-eqz v4, :cond_0

    .line 469851
    sget-object v4, LX/04g;->BY_WAIT_FOR_LIVE_VOD_TRANSITION:LX/04g;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/2q6;->e(LX/04g;)V

    .line 469852
    :goto_0
    return-void

    .line 469853
    :cond_0
    sget-object v4, LX/7Jj;->ERROR_IO:LX/7Jj;

    move-object/from16 v0, p1

    if-eq v0, v4, :cond_1

    sget-object v4, LX/7Jj;->MALFORMED:LX/7Jj;

    move-object/from16 v0, p1

    if-eq v0, v4, :cond_1

    sget-object v4, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_a

    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    const/4 v5, 0x3

    if-ge v4, v5, :cond_a

    .line 469854
    sget-object v4, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_2

    .line 469855
    sget-object v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v5, "playback exception: %s, exception: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469856
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bb:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    sget-object v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Playback exception during video play: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", stack: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 469857
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    .line 469858
    const-string v4, "Re-init ExoPlayer after malformed/io errors, try #%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469859
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v4}, LX/2q9;->c()V

    .line 469860
    if-eqz p2, :cond_6

    sget-object v4, LX/0KD;->BEHIND_LIVE_WINDOW_ERROR:LX/0KD;

    iget-object v4, v4, LX/0KD;->value:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    move/from16 v20, v4

    .line 469861
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->s:LX/19j;

    iget-boolean v4, v4, LX/19j;->l:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    move/from16 v21, v4

    .line 469862
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->D:LX/2qD;

    sget-object v5, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v4, v5, :cond_3

    if-eqz v20, :cond_4

    .line 469863
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->P:LX/09L;

    iget-object v7, v7, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v9, v0, LX/2q6;->X:I

    sget-object v10, LX/04g;->BY_PLAYER_INTERNAL_ERROR:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    if-eqz v21, :cond_8

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I

    move-result v11

    :goto_3
    move-object/from16 v0, p0

    iget v12, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v13, v13, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2q6;->z:LX/04g;

    iget-object v15, v15, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v4 .. v19}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 469864
    if-eqz v20, :cond_4

    .line 469865
    const-string v4, "onPlaybackDiscontinuity"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469866
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->P:LX/09L;

    iget-object v6, v6, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2q6;->z:LX/04g;

    iget-object v9, v9, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v10

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I

    move-result v11

    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v4 .. v12}, LX/1C2;->a(LX/04G;Ljava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;ILX/098;)LX/1C2;

    .line 469867
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/7Jj;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 469868
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v4}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    .line 469869
    invoke-direct/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 469870
    :cond_5
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 469871
    sget-object v4, LX/2qD;->STATE_IDLE:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/2q6;->a(LX/2qD;)V

    .line 469872
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->l:LX/0Sh;

    new-instance v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$12;

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, p1

    invoke-direct {v5, v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$12;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;ZLX/7Jj;)V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    invoke-static {v6}, LX/0wq;->a(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 469873
    :cond_6
    const/4 v4, 0x0

    move/from16 v20, v4

    goto/16 :goto_1

    .line 469874
    :cond_7
    const/4 v4, 0x0

    move/from16 v21, v4

    goto/16 :goto_2

    .line 469875
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v11

    goto/16 :goto_3

    .line 469876
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v11

    goto :goto_4

    .line 469877
    :cond_a
    sget-object v4, LX/7Jj;->PLAYERSERVICE_DEAD:LX/7Jj;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_b

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    const/4 v5, 0x3

    if-ge v4, v5, :cond_b

    .line 469878
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    .line 469879
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 469880
    sget-object v4, LX/2qD;->STATE_IDLE:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/2q6;->a(LX/2qD;)V

    .line 469881
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 469882
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bh:LX/0TD;

    new-instance v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$13;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$13;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    const v6, -0x5d6cf3b3

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_0

    .line 469883
    :cond_b
    sget-object v4, LX/2qD;->STATE_ERROR:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/2q6;->a(LX/2qD;)V

    .line 469884
    sget-object v4, LX/2qD;->STATE_ERROR:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/2q6;->b(LX/2qD;)V

    .line 469885
    sget-object v4, LX/2qk;->FROM_ERROR:LX/2qk;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;ZZ)V

    .line 469886
    sget-object v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v5, "playback failed: %s, exception: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469887
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->g:LX/1C2;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ExoPlayer Error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2q6;->z:LX/04g;

    iget-object v9, v9, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v13, 0x0

    move-object/from16 v14, p3

    invoke-virtual/range {v4 .. v14}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 469888
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->l:LX/0Sh;

    new-instance v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$14;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$14;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;)V

    invoke-virtual {v4, v5}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/1AD;)V
    .locals 2

    .prologue
    .line 469847
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$21;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$21;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/1AD;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 469848
    return-void
.end method

.method public static a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 469842
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 469843
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 469844
    :goto_0
    return-void

    .line 469845
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ab:Landroid/os/Handler$Callback;

    invoke-interface {v0, p1}, Landroid/os/Handler$Callback;->handleMessage(Landroid/os/Message;)Z

    .line 469846
    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method private a(LX/7Jj;I)Z
    .locals 1

    .prologue
    .line 470327
    sget-object v0, LX/7Jj;->ERROR_IO:LX/7Jj;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470154
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 470155
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    .line 470156
    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v0, v3, :cond_0

    .line 470157
    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot Play Now. In State: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/2q6;->E:LX/2qD;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    const-string v4, "Cannot play now.  Require target state %s. In target state %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, LX/2qD;->STATE_PLAYING:LX/2qD;

    aput-object v6, v5, v2

    aput-object v0, v5, v1

    invoke-static {p0, v3, v4, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470158
    :goto_0
    return v2

    .line 470159
    :cond_0
    const-string v0, "playNow"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470160
    iput-boolean v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->L:Z

    .line 470161
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M:LX/04g;

    .line 470162
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->K:J

    .line 470163
    new-instance v0, LX/2qN;

    iget v3, p2, LX/7K4;->c:I

    sget-object v4, LX/7IB;->b:LX/7IB;

    invoke-direct {v0, v3, v4}, LX/2qN;-><init>(ILX/7IB;)V

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/1AD;)V

    .line 470164
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470165
    iget-object v4, p0, LX/2q6;->l:LX/0Sh;

    new-instance v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$4;

    invoke-direct {v5, p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$4;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;LX/04g;)V

    invoke-virtual {v4, v5}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 470166
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->h(LX/04g;)Z

    move-result v3

    .line 470167
    if-eqz v3, :cond_2

    .line 470168
    const-string v0, "Seek to: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0, v0, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470169
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v4, 0x3

    iget v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    invoke-virtual {v0, v4, v5, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470170
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bf:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v4, 0x0

    .line 470171
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    sget-object v5, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    if-ne v0, v5, :cond_10

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 470172
    if-eqz v0, :cond_4

    .line 470173
    :cond_3
    invoke-virtual {p0}, LX/2q6;->y()V

    .line 470174
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470175
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v4, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v0, v4, :cond_c

    move v0, v1

    .line 470176
    :goto_3
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->T()Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v3, :cond_5

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v4, v4, LX/0wq;->Z:Z

    if-nez v4, :cond_7

    :cond_5
    if-eqz v0, :cond_7

    :cond_6
    move v2, v1

    .line 470177
    :cond_7
    if-eqz v2, :cond_d

    .line 470178
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 470179
    :cond_8
    :goto_4
    if-eqz v3, :cond_e

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470180
    :goto_5
    iget-object v3, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v3}, LX/2q9;->b()V

    .line 470181
    if-eqz v2, :cond_9

    .line 470182
    invoke-direct {p0, p1, p3, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/04g;ZI)V

    .line 470183
    :cond_9
    invoke-virtual {p2}, LX/7K4;->a()Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/2q6;->s:LX/19j;

    iget-boolean v2, v2, LX/19j;->p:Z

    if-nez v2, :cond_f

    .line 470184
    :cond_a
    iget v0, p2, LX/7K4;->d:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    .line 470185
    :cond_b
    :goto_6
    iget-object v0, p0, LX/2q6;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    move v2, v1

    .line 470186
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 470187
    goto :goto_3

    .line 470188
    :cond_d
    if-eqz v3, :cond_8

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->Z:Z

    if-eqz v0, :cond_8

    .line 470189
    sget-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    goto :goto_4

    .line 470190
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v0

    goto :goto_5

    .line 470191
    :cond_f
    invoke-static {p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 470192
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    goto :goto_6

    :cond_10
    move v0, v4

    goto/16 :goto_2
.end method

.method public static a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V
    .locals 7

    .prologue
    .line 470328
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/7Jj;Ljava/lang/String;Ljava/lang/String;J)V

    .line 470329
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 470330
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 470331
    const-string v0, "Player already released when attach surface"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470332
    :goto_0
    return-void

    .line 470333
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 470334
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 470335
    :goto_0
    return-void

    .line 470336
    :cond_0
    if-eqz p2, :cond_1

    .line 470337
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470338
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470339
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v3, 0x8

    if-eqz p3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0, v1, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470340
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->L()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 470341
    goto :goto_1
.end method

.method private ac()V
    .locals 4

    .prologue
    .line 470342
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470343
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$18;

    invoke-direct {v3, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$18;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 470344
    :cond_0
    return-void
.end method

.method private ad()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 470368
    const-string v0, "onPrepared for %s"

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470369
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 470370
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->A:I

    .line 470371
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    iget v2, p0, LX/2q6;->A:I

    invoke-virtual {v0, v1, v2}, LX/19Z;->a(II)V

    .line 470372
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->f:Z

    if-nez v0, :cond_1

    .line 470373
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-eqz v0, :cond_0

    .line 470374
    const-string v0, "mVideoSurfaceTarget.isSurfaceAllocated()? %s"

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470375
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470376
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 470377
    iget-object v1, v0, LX/2p8;->a:Landroid/view/Surface;

    move-object v0, v1

    .line 470378
    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    .line 470379
    :cond_1
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ao:F

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;F)V

    .line 470380
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470381
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$19;

    invoke-direct {v3, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$19;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 470382
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    if-eqz v0, :cond_3

    .line 470383
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0, v1}, LX/04j;->d(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;

    move-result-object v1

    .line 470384
    if-eqz v1, :cond_3

    .line 470385
    iget v0, v1, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->a:I

    move v0, v0

    .line 470386
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    .line 470387
    iget v0, v1, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->b:I

    move v0, v0

    .line 470388
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    .line 470389
    iget-object v0, v1, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    move-object v0, v0

    .line 470390
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->W:LX/0AR;

    .line 470391
    iget-object v0, p0, LX/2q6;->W:LX/0AR;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/2q6;->W:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aU:Ljava/lang/String;

    .line 470392
    iget-object v0, v1, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamMetadata;->d:Ljava/lang/String;

    move-object v0, v0

    .line 470393
    invoke-static {v0}, LX/03z;->fromString(Ljava/lang/String;)LX/03z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aW:LX/03z;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470394
    :cond_3
    :goto_2
    new-instance v0, LX/7IE;

    iget v1, p0, LX/2q6;->A:I

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aU:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aV:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->P:LX/09L;

    iget-object v6, v6, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->S:LX/097;

    iget-object v8, v8, LX/097;->value:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aW:LX/03z;

    invoke-direct/range {v0 .. v9}, LX/7IE;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/03z;)V

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->F:LX/7IE;

    .line 470395
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ay:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 470396
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ay:Ljava/lang/String;

    .line 470397
    iput-object v1, v0, LX/7IE;->j:Ljava/lang/String;

    .line 470398
    :cond_4
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    if-lez v0, :cond_8

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->l:Z

    if-nez v0, :cond_8

    :cond_5
    move v0, v10

    .line 470399
    :goto_3
    new-instance v1, LX/09S;

    invoke-direct {v1}, LX/09S;-><init>()V

    invoke-static {p0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/1AD;)V

    .line 470400
    iget-object v1, p0, LX/2q6;->E:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v1, v2, :cond_9

    .line 470401
    iget-object v1, p0, LX/2q6;->R:LX/04g;

    iget-object v2, p0, LX/2q6;->C:LX/7K4;

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;Z)Z

    .line 470402
    :cond_6
    :goto_4
    return-void

    .line 470403
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 470404
    :catch_0
    move-exception v0

    .line 470405
    const-string v1, "Service RemoteException when getVideoPlayerStreamMetadata: %s"

    new-array v2, v10, [Ljava/lang/Object;

    aput-object v0, v2, v11

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_8
    move v0, v11

    .line 470406
    goto :goto_3

    .line 470407
    :cond_9
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bc:Z

    if-nez v0, :cond_6

    .line 470408
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-ne v0, v1, :cond_6

    .line 470409
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(LX/04g;)V

    goto :goto_4
.end method

.method private static ae(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)I
    .locals 4

    .prologue
    .line 470345
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470346
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0, v1}, LX/04j;->e(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 470347
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x112a880

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 470348
    long-to-int v0, v0

    .line 470349
    :goto_0
    return v0

    .line 470350
    :catch_0
    move-exception v0

    .line 470351
    const-string v1, "Service RemoteException when getDurationUs: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470352
    :cond_0
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    goto :goto_0
.end method

.method private static af(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 470356
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 470357
    :cond_0
    :goto_0
    return-void

    .line 470358
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0, v1}, LX/04j;->h(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)I

    move-result v1

    .line 470359
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    if-le v1, v0, :cond_0

    .line 470360
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v0}, LX/1C2;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470361
    const-string v0, "BufferingUpdate: from %s to %s, sid=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v4}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470362
    :cond_2
    iput v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    .line 470363
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470364
    iget-object v3, p0, LX/2q6;->l:LX/0Sh;

    new-instance v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$20;

    invoke-direct {v4, p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$20;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;I)V

    invoke-virtual {v3, v4}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 470365
    :catch_0
    move-exception v0

    .line 470366
    const-string v1, "Service RemoteException when getBufferedPercentage"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private ag()I
    .locals 2

    .prologue
    .line 470367
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->be:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/2q6;->B:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->be:I

    goto :goto_0
.end method

.method private ah()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470273
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 470274
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    .line 470275
    :goto_0
    sget-object v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PreferredVideoUri is null and manifest is null: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " and player origin is: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2q6;->g()LX/04D;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {p0, v3, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470276
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 470277
    goto :goto_0
.end method

.method private b(Landroid/net/Uri;)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 470278
    const-string v1, "prepareAsync"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470279
    if-nez p1, :cond_1

    .line 470280
    const-string v1, "Try prepareVideo but uri is null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470281
    :cond_0
    :goto_0
    return-void

    .line 470282
    :cond_1
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 470283
    const-string v1, "Try prepareVideo but no service connected"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 470284
    :cond_2
    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    invoke-virtual {p0, v1}, LX/2q6;->a(LX/2qD;)V

    .line 470285
    iget-object v1, p0, LX/2q6;->q:LX/16V;

    new-instance v2, LX/09P;

    invoke-direct {v2}, LX/09P;-><init>()V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 470286
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    invoke-virtual {v1, v0}, LX/1m6;->a(Z)V

    .line 470287
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1m6;->a(Ljava/lang/String;)V

    .line 470288
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {v1, v0}, LX/1m6;->c(Z)V

    .line 470289
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 470290
    if-eqz p1, :cond_d

    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v3

    if-eqz v3, :cond_d

    move v3, v12

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 470291
    const-string v3, "startVideoSession for %s"

    new-array v4, v12, [Ljava/lang/Object;

    aput-object p1, v4, v13

    invoke-virtual {p0, v3, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470292
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {p1, v3}, Lcom/facebook/video/engine/VideoDataSource;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 470293
    :cond_4
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {p1, v3}, Lcom/facebook/video/engine/VideoDataSource;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 470294
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    .line 470295
    :cond_5
    iget-object v3, p0, LX/2q6;->y:LX/04G;

    sget-object v4, LX/04G;->CHANNEL_PLAYER:LX/04G;

    invoke-virtual {v3, v4}, LX/04G;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, LX/2q6;->y:LX/04G;

    sget-object v4, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v3, v4}, LX/04G;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_6
    iget-object v3, p0, LX/2q6;->y:LX/04G;

    iget-object v5, v3, LX/04G;->value:Ljava/lang/String;

    .line 470296
    :goto_2
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bi:LX/2qA;

    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    iget-object v6, v6, LX/04D;->origin:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    iget-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    iget-object v7, p0, LX/2q6;->g:LX/1C2;

    iget-object v10, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v10, v10, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v7, v10, v13}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v10

    iget-object v7, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v7, :cond_f

    move v11, v12

    :goto_3
    move-object v7, p1

    invoke-virtual/range {v3 .. v11}, LX/2qA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ZZ)Lcom/facebook/exoplayer/ipc/VideoPlayRequest;

    move-result-object v11

    .line 470297
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aL:LX/0ka;

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    invoke-virtual {v5}, LX/1m6;->f()Z

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0wp;->a(LX/0ka;Z)I

    move-result v3

    if-ltz v3, :cond_10

    iget-object v3, p0, LX/2q6;->g:LX/1C2;

    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v13}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    move v6, v12

    .line 470298
    :goto_4
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bi:LX/2qA;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2qA;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    invoke-virtual {v3}, LX/0wp;->d()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    iget-object v3, v3, LX/0wp;->l:LX/0xn;

    sget-object v4, LX/0xn;->CUSTOM_ABR:LX/0xn;

    if-eq v3, v4, :cond_7

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    iget-object v3, v3, LX/0wp;->l:LX/0xn;

    sget-object v4, LX/0xn;->MANUAL:LX/0xn;

    if-ne v3, v4, :cond_8

    :cond_7
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v3, v3, LX/0wq;->h:Z

    if-eqz v3, :cond_8

    .line 470299
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aK:LX/0wp;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aM:LX/04m;

    iget-object v7, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aL:LX/0ka;

    iget-object v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aN:LX/0oz;

    iget-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aO:LX/0YR;

    iget-object v10, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aP:LX/1AA;

    invoke-static/range {v3 .. v10}, LX/1m7;->a(LX/0wp;LX/1m6;LX/04m;ZLX/0ka;LX/0oz;LX/0YR;LX/1AA;)LX/04o;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    .line 470300
    :cond_8
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/19w;

    iget-object v4, v11, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/19w;

    iget-object v4, v11, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 470301
    sget-object v3, LX/097;->FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S:LX/097;

    .line 470302
    :cond_9
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v11}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470303
    :cond_a
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    invoke-virtual {v3, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470304
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    invoke-virtual {v3, v13, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 470305
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09R;

    invoke-direct {v1}, LX/09R;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 470306
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 470307
    invoke-interface {v0}, LX/2pf;->b()V

    goto :goto_5

    .line 470308
    :cond_b
    new-instance v0, LX/7II;

    iget-object v1, p0, LX/2q6;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/7II;-><init>(LX/2q8;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 470309
    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 470310
    iput v1, v0, LX/7IG;->c:I

    .line 470311
    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 470312
    iput v1, v0, LX/7IG;->d:I

    .line 470313
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    iput v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    .line 470314
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    invoke-virtual {v1, v2, v0}, LX/19Z;->a(ILX/7IG;)V

    .line 470315
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    .line 470316
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 470317
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bu:Ljava/lang/Object;

    monitor-enter v1

    .line 470318
    :try_start_0
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->W(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 470319
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470320
    :cond_c
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    .line 470321
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bt:LX/2qC;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2qC;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 470322
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_d
    move v3, v13

    .line 470323
    goto/16 :goto_1

    .line 470324
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_f
    move v11, v13

    .line 470325
    goto/16 :goto_3

    :cond_10
    move v6, v13

    .line 470326
    goto/16 :goto_4
.end method

.method private static b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;F)V
    .locals 3

    .prologue
    .line 470269
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ao:F

    .line 470270
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    if-nez v0, :cond_1

    .line 470271
    :cond_0
    :goto_0
    return-void

    .line 470272
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v1, 0x6

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ao:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;)V
    .locals 16

    .prologue
    .line 470260
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 470261
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->b()V

    .line 470262
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 470263
    sget-object v1, LX/7K4;->a:LX/7K4;

    move-object/from16 v0, p2

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p2

    iget v5, v0, LX/7K4;->c:I

    .line 470264
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v15, 0x1

    .line 470265
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->S:LX/097;

    iget-object v6, v6, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->w:LX/04D;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->z:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v13, v13, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v1 .. v15}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;Ljava/lang/String;LX/098;Z)LX/1C2;

    .line 470266
    :cond_0
    return-void

    .line 470267
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v5

    goto :goto_0

    .line 470268
    :cond_2
    const/4 v15, 0x0

    goto :goto_1
.end method

.method public static varargs b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 470258
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470259
    return-void
.end method

.method private static varargs b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 470255
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v0}, LX/1C2;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470256
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bd:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/2q6;->D:LX/2qD;

    iget-object v2, v2, LX/2qD;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/2q6;->E:LX/2qD;

    iget-object v2, v2, LX/2qD;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    invoke-static {v2}, LX/7K1;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 470257
    :cond_0
    return-void
.end method

.method private static b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470247
    new-instance v5, LX/0Ld;

    invoke-direct {v5}, LX/0Ld;-><init>()V

    .line 470248
    iget v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->a:I

    iput v0, v5, LX/0Ld;->a:I

    .line 470249
    iget-object v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iput-object v0, v5, LX/0Ld;->c:LX/0AR;

    .line 470250
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    check-cast v0, LX/1mA;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/1mA;->b(Ljava/util/List;J[LX/0AR;LX/0Ld;)V

    .line 470251
    iget v0, v5, LX/0Ld;->b:I

    iput v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->b:I

    .line 470252
    iget-object v0, v5, LX/0Ld;->c:LX/0AR;

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iput-object v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 470253
    iget v0, v5, LX/0Ld;->a:I

    iput v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->a:I

    .line 470254
    return-void
.end method

.method private c(LX/04g;LX/7K4;)V
    .locals 1

    .prologue
    .line 470243
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R:LX/04g;

    .line 470244
    iput-object p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->C:LX/7K4;

    .line 470245
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 470246
    return-void
.end method

.method public static c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/graphics/RectF;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 470217
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 470218
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    if-eqz v0, :cond_0

    .line 470219
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Potential IndexOutOfBoundsException:mCurrentDataSourceIndex = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but the size of the datastructure = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v10, v9

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 470220
    :cond_0
    :goto_0
    return-void

    .line 470221
    :cond_1
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 470222
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v1}, LX/2p8;->j()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->k()I

    move-result v2

    iget-object v3, v0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    if-nez p1, :cond_2

    iget-object p1, v0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    :cond_2
    invoke-static {v1, v2, v3, p1}, LX/7K8;->a(IILX/2oF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 470223
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 470224
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bu:Ljava/lang/Object;

    monitor-enter v1

    .line 470225
    :try_start_0
    const/4 v3, -0x1

    .line 470226
    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    if-ne v2, v3, :cond_3

    .line 470227
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->W(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 470228
    :cond_3
    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    if-eq v2, v3, :cond_6

    .line 470229
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 470230
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->j()I

    move-result v3

    .line 470231
    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v4}, LX/2p8;->k()I

    move-result v4

    .line 470232
    div-int/lit8 v5, v3, 0x2

    .line 470233
    div-int/lit8 v6, v4, 0x2

    .line 470234
    iget v7, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    int-to-float v7, v7

    int-to-float v8, v5

    int-to-float v9, v6

    invoke-virtual {v2, v7, v8, v9}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 470235
    iget v7, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bl:I

    rem-int/lit16 v7, v7, 0xb4

    if-eqz v7, :cond_4

    .line 470236
    int-to-float v7, v3

    int-to-float v8, v4

    div-float/2addr v7, v8

    .line 470237
    int-to-float v4, v4

    int-to-float v3, v3

    div-float v3, v4, v3

    .line 470238
    int-to-float v4, v5

    int-to-float v5, v6

    invoke-virtual {v2, v7, v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 470239
    :cond_4
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v3, v2}, LX/2p8;->a(Landroid/graphics/Matrix;)V

    .line 470240
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 470241
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v1, v0}, LX/2p8;->a(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 470242
    :cond_6
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v2, v0}, LX/2p8;->a(Landroid/graphics/Matrix;)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 470212
    const-string v0, "sendSurfaceToVideoRenderer: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470213
    if-nez p1, :cond_0

    .line 470214
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    .line 470215
    :goto_0
    return-void

    .line 470216
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bh:LX/0TD;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$10;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$10;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    const v2, 0x6f0818ab

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static varargs c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 470207
    if-eqz p1, :cond_0

    .line 470208
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Restarting player service, reason: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470209
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->K()V

    .line 470210
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ai:LX/16V;

    sget-object v1, LX/044;->a:LX/044;

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 470211
    return-void
.end method

.method public static c(Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 470206
    sget-boolean v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bj:Z

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    invoke-static {p0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;I)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 470197
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_1

    if-eq p1, v5, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 470198
    :cond_0
    const-string v0, "ExoPlayer prepared: videoMime=%s, audioMime=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aU:Ljava/lang/String;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aV:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470199
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad()V

    .line 470200
    :cond_1
    if-ne p1, v5, :cond_3

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-object v0, v0, LX/0wq;->S:LX/0xT;

    sget-object v1, LX/0xT;->STATE_READY:LX/0xT;

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->T()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->X:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v0, v1, :cond_3

    .line 470201
    :cond_2
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 470202
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->h(LX/04g;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470203
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    invoke-direct {p0, v1, v4, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/04g;ZI)V

    .line 470204
    :cond_3
    return-void

    .line 470205
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v0

    goto :goto_0
.end method

.method private h(LX/04g;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470193
    invoke-static {p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->u:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 470194
    :goto_0
    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    invoke-virtual {p0, v0}, LX/2q6;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 470195
    goto :goto_0

    :cond_1
    move v1, v2

    .line 470196
    goto :goto_1
.end method

.method private j(LX/04g;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 469986
    const-string v0, "resetNow"

    new-array v1, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469987
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469988
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 469989
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-eq p1, v0, :cond_0

    .line 469990
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v5

    iget v6, p0, LX/2q6;->B:I

    iget-object v7, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p0, LX/2q6;->w:LX/04D;

    iget-object v9, p0, LX/2q6;->z:LX/04g;

    iget-object v9, v9, LX/04g;->value:Ljava/lang/String;

    iget-object v10, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v10, v10, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iget-object v11, p0, LX/2q6;->S:LX/097;

    iget-object v11, v11, LX/097;->value:Ljava/lang/String;

    invoke-virtual/range {v0 .. v11}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;ZLjava/lang/String;)LX/1C2;

    .line 469991
    :cond_0
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 469992
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 469993
    iput v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    .line 469994
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    .line 469995
    iput v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    .line 469996
    return-void
.end method

.method private k(LX/04g;)V
    .locals 18

    .prologue
    .line 470127
    invoke-static/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 470128
    invoke-static/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->af(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 470129
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/2q6;->u:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aE:I

    const/16 v3, 0x63

    if-le v2, v3, :cond_1

    .line 470130
    :cond_0
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470131
    invoke-static/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->l(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;)V

    .line 470132
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/2q6;->D()V

    .line 470133
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    .line 470134
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 470135
    return-void

    .line 470136
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v2, v3, :cond_4

    .line 470137
    const-string v2, "seek time = %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470138
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 470139
    :goto_1
    if-eqz v2, :cond_2

    .line 470140
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    .line 470141
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470142
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 470143
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(LX/04g;)V

    goto/16 :goto_0

    .line 470144
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 470145
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, LX/04g;->value:Ljava/lang/String;

    sget-object v3, LX/04g;->BY_ANDROID:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 470146
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470147
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    move-object/from16 v0, p0

    iget v3, v0, LX/2q6;->B:I

    if-ge v2, v3, :cond_7

    .line 470148
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 470149
    :cond_7
    const-string v2, "stop-for-pause: %s , seek time = %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, LX/04g;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470150
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 470151
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 470152
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 470153
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(LX/04g;)V

    goto/16 :goto_0
.end method

.method private static l(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 469463
    const-string v0, "pause"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469464
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469465
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_1

    .line 469466
    sget-object v0, LX/2qD;->STATE_PAUSED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 469467
    :cond_0
    :goto_0
    return-void

    .line 469468
    :cond_1
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->T()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->W:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_4

    .line 469469
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->Y:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_3

    .line 469470
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v0, v1, :cond_0

    .line 469471
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    invoke-virtual {p0, p1, v0}, LX/2q6;->a(LX/04g;LX/2qD;)V

    goto :goto_0

    .line 469472
    :cond_3
    invoke-virtual {p0, p1}, LX/2q6;->f(LX/04g;)V

    goto :goto_0

    .line 469473
    :cond_4
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-eq v0, v1, :cond_0

    .line 469474
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469475
    invoke-interface {v0, p1, v4}, LX/2pf;->b(LX/04g;Z)V

    goto :goto_1

    .line 469476
    :cond_5
    invoke-virtual {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->g(LX/04g;)V

    .line 469477
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469478
    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    goto :goto_2
.end method

.method private static m(LX/04g;)Z
    .locals 1

    .prologue
    .line 469576
    sget-object v0, LX/2q6;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final E()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 469550
    const-string v0, "onCompletion"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469551
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->c()V

    .line 469552
    invoke-virtual {p0}, LX/2q6;->C()V

    .line 469553
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 469554
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->A:I

    .line 469555
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    .line 469556
    invoke-virtual {p0}, LX/2q6;->D()V

    .line 469557
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bp:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-ge v0, v1, :cond_2

    .line 469558
    :cond_0
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v3, v3}, LX/7K4;-><init>(II)V

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;)V

    .line 469559
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469560
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v3, v3}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(LX/04g;LX/7K4;)V

    .line 469561
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v3, v3}, LX/7K4;-><init>(II)V

    invoke-static {p0, v0, v1, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;Z)Z

    move-result v0

    .line 469562
    if-eqz v0, :cond_1

    .line 469563
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bp:I

    .line 469564
    :cond_1
    return-void

    .line 469565
    :cond_2
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_3

    .line 469566
    new-instance v0, LX/2qT;

    iget v1, p0, LX/2q6;->A:I

    sget-object v2, LX/7IF;->a:LX/7IF;

    invoke-direct {v0, v1, v2}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/1AD;)V

    .line 469567
    :cond_3
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 469568
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 469569
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469570
    iput-boolean v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 469571
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->t:Z

    if-nez v0, :cond_4

    .line 469572
    invoke-virtual {p0}, LX/2q6;->n()V

    .line 469573
    sget-object v0, LX/2qk;->FROM_ONCOMPLETE:LX/2qk;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    .line 469574
    :cond_4
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469575
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$11;

    invoke-direct {v3, p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$11;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;)V

    invoke-virtual {v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 469547
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    if-eqz v0, :cond_0

    .line 469548
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    invoke-interface {v0}, LX/04o;->a()V

    .line 469549
    :cond_0
    return-void
.end method

.method public final G()V
    .locals 1

    .prologue
    .line 469544
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    if-eqz v0, :cond_0

    .line 469545
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    invoke-interface {v0}, LX/04o;->b()V

    .line 469546
    :cond_0
    return-void
.end method

.method public final H()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 469534
    const-string v0, "onStreamInterrupted"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469535
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    if-nez v0, :cond_1

    .line 469536
    const-string v0, "set streamdriedout"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469537
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 469538
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->e()V

    .line 469539
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 469540
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->an:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 469541
    :cond_0
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$24;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$24;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 469542
    :cond_1
    return-void

    .line 469543
    :cond_2
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->e()V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 469531
    const-string v0, "setVolume"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469532
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;F)V

    .line 469533
    return-void
.end method

.method public final a(I)V
    .locals 8

    .prologue
    .line 469529
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->y:LX/04G;

    iget-object v2, p0, LX/2q6;->w:LX/04D;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v6

    move v7, p1

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/04G;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/0lF;II)LX/1C2;

    .line 469530
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 469517
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    if-ne v0, p1, :cond_1

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    if-ne v0, p2, :cond_1

    .line 469518
    :cond_0
    return-void

    .line 469519
    :cond_1
    const-string v0, "onVideoSizeChanged: w=%s, h=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469520
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    .line 469521
    iput p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    .line 469522
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    if-eqz v0, :cond_2

    .line 469523
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aS:I

    .line 469524
    iput v1, v0, LX/7IE;->f:I

    .line 469525
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aT:I

    .line 469526
    iput v1, v0, LX/7IE;->g:I

    .line 469527
    :cond_2
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469528
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$22;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$22;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;II)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 10

    .prologue
    .line 469515
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->y:LX/04G;

    iget-object v2, p0, LX/2q6;->w:LX/04D;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v6

    move v7, p1

    move-wide v8, p2

    invoke-virtual/range {v0 .. v9}, LX/1C2;->a(LX/04G;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/0lF;IIJ)LX/1C2;

    .line 469516
    return-void
.end method

.method public final a(ILX/04g;)V
    .locals 4

    .prologue
    .line 469503
    const-string v0, "seekTo"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469504
    const-string v0, "seekTo %s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469505
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->B:I

    .line 469506
    sget-object v0, LX/7KN;->a:[I

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    invoke-virtual {v1}, LX/2qD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 469507
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/2q6;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469508
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 469509
    :cond_0
    :goto_0
    return-void

    .line 469510
    :pswitch_0
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469511
    goto :goto_0

    .line 469512
    :pswitch_1
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    goto :goto_0

    .line 469513
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469514
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0, p1}, LX/2q9;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V
    .locals 18

    .prologue
    .line 469483
    invoke-static/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 469484
    :goto_0
    return-void

    .line 469485
    :cond_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p6

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I

    move-result v9

    .line 469486
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    if-eqz v3, :cond_1

    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    iget-object v3, v0, LX/0AR;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->W:LX/0AR;

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 469487
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->P:LX/09L;

    iget-object v6, v6, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v8, v8, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->z:LX/04g;

    iget-object v12, v12, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->W:LX/0AR;

    sget-object v15, LX/0JM;->STREAM_SWITCH:LX/0JM;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bB:Z

    move/from16 v16, v0

    move-object/from16 v14, p2

    invoke-virtual/range {v3 .. v16}, LX/1C2;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/04G;Ljava/lang/String;ILjava/lang/String;IILX/04D;Ljava/lang/String;LX/0AR;LX/0AR;LX/0JM;Z)LX/1C2;

    .line 469488
    :cond_1
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->W:LX/0AR;

    .line 469489
    move-object/from16 v0, p0

    iput v9, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->be:I

    .line 469490
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    if-eqz v3, :cond_2

    .line 469491
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->W:LX/0AR;

    invoke-virtual {v3, v4}, LX/1m6;->a(LX/0AR;)V

    .line 469492
    :cond_2
    invoke-virtual/range {p0 .. p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v4

    .line 469493
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->F:LX/7IE;

    if-eqz v3, :cond_3

    .line 469494
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->F:LX/7IE;

    invoke-virtual {v3, v4}, LX/7IE;->a(Ljava/lang/String;)V

    .line 469495
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->F:LX/7IE;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    iget v3, v3, LX/0AR;->f:I

    :goto_1
    invoke-virtual {v5, v3}, LX/7IE;->a(I)V

    .line 469496
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->F:LX/7IE;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    iget v3, v3, LX/0AR;->g:I

    :goto_2
    invoke-virtual {v5, v3}, LX/7IE;->b(I)V

    .line 469497
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2pf;

    .line 469498
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->l:LX/0Sh;

    new-instance v7, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$23;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v3, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$23;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 469499
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 469500
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 469501
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->n:LX/2q3;

    move-object/from16 v0, p2

    iget v5, v0, LX/0AR;->c:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ax:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5, v9}, LX/2q3;->a(II)V

    .line 469502
    const-string v3, "onDownstreamFormatChanged: %s, sourceId: %s, trigger: %s, mediaTimeMs: %s, videoBitrate: %d, audioBitrate: %d"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v4, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x3

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x4

    move-object/from16 v0, p2

    iget v6, v0, LX/0AR;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ax:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(LX/04G;)V
    .locals 1

    .prologue
    .line 469479
    invoke-super {p0, p1}, LX/2q6;->a(LX/04G;)V

    .line 469480
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    if-eqz v0, :cond_0

    .line 469481
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    invoke-virtual {v0, p1}, LX/1m6;->a(LX/04G;)V

    .line 469482
    :cond_0
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 469334
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/04g;LX/7K4;)V

    .line 469335
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 17

    .prologue
    .line 469338
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->P:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->S:LX/097;

    iget-object v8, v7, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->z:LX/04g;

    iget-object v12, v7, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2q6;->N:LX/09M;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 469339
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 469340
    const-string v0, "play"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469341
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    .line 469342
    sget-object v0, LX/04g;->UNSET:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->br:LX/04g;

    .line 469343
    const-string v0, "%s, %s, position: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v4, v3, v2

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    aput-object p2, v3, v4

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469344
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v3, LX/2qD;->STATE_ERROR:LX/2qD;

    if-ne v0, v3, :cond_1

    .line 469345
    const-string v0, "This player enters final error target state, should not be used anymore"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469346
    :cond_0
    :goto_0
    return-void

    .line 469347
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    sget-object v4, LX/7IJ;->START:LX/7IJ;

    invoke-virtual {v0, v3, v4}, LX/19Z;->a(ILX/7IJ;)V

    .line 469348
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qK;

    iget v4, p2, LX/7K4;->c:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qK;-><init>(ILX/7IB;)V

    invoke-virtual {v0, v3}, LX/16V;->a(LX/1AD;)V

    .line 469349
    invoke-virtual {p2}, LX/7K4;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 469350
    iget v0, p2, LX/7K4;->c:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469351
    :cond_2
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 469352
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(LX/04g;LX/7K4;)V

    .line 469353
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 469354
    if-nez v0, :cond_3

    .line 469355
    invoke-static {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;)V

    .line 469356
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/2q6;->U:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->av:Z

    if-eqz v0, :cond_3

    .line 469357
    invoke-virtual {p0, p1}, LX/2q6;->e(LX/04g;)V

    .line 469358
    :cond_3
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S()Z

    move-result v0

    if-nez v0, :cond_5

    .line 469359
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->P()V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 469360
    goto :goto_1

    .line 469361
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    if-nez v0, :cond_0

    .line 469362
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    goto :goto_0

    .line 469363
    :cond_6
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->d()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 469364
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 469365
    iget-object v4, v3, LX/2p8;->a:Landroid/view/Surface;

    move-object v3, v4

    .line 469366
    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    .line 469367
    :cond_7
    iget-object v3, p0, LX/2q6;->I:Landroid/view/Surface;

    if-eqz v3, :cond_8

    .line 469368
    iget-object v3, p0, LX/2q6;->I:Landroid/view/Surface;

    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    .line 469369
    :cond_8
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    if-nez v3, :cond_9

    .line 469370
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469371
    :cond_9
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->T()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->V:Z

    if-eqz v1, :cond_a

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v0, v1, :cond_b

    .line 469372
    :cond_a
    invoke-static {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;)V

    .line 469373
    :cond_b
    invoke-static {p0, p1, p2, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;LX/7K4;Z)Z

    goto/16 :goto_0
.end method

.method public final a(LX/04k;)V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 469374
    const-string v0, "onVideoServiceConnected"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469375
    iget-object v0, p1, LX/04k;->a:Ljava/lang/Object;

    check-cast v0, LX/04j;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    .line 469376
    iget-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 469377
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    sub-long v4, v0, v4

    .line 469378
    iput-wide v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    .line 469379
    :goto_0
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->w:LX/04D;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 469380
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/0AA;->VIDEO_PLAYER_SERVICE_RECONNECTED:LX/0AA;

    iget-object p1, p1, LX/0AA;->value:Ljava/lang/String;

    invoke-direct {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p1, LX/04F;->STALL_TIME:LX/04F;

    iget-object p1, p1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v6, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 469381
    invoke-static {v0, v6, v3, v1, v2}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/04D;LX/04G;)LX/1C2;

    .line 469382
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/2q6;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469383
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->P()V

    .line 469384
    :cond_0
    return-void

    :cond_1
    move-wide v4, v2

    goto :goto_0
.end method

.method public final a(LX/0Je;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 469578
    const-string v0, "onVideoServiceDisconnected"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469579
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->w:LX/04D;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {p1}, LX/0Je;->a()J

    move-result-wide v4

    invoke-virtual {p1}, LX/0Je;->b()J

    move-result-wide v6

    invoke-virtual {p1}, LX/0Je;->c()J

    move-result-wide v8

    .line 469580
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/0AA;->VIDEO_PLAYER_SERVICE_DISCONNECTED:LX/0AA;

    iget-object p1, p1, LX/0AA;->value:Ljava/lang/String;

    invoke-direct {v10, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object p1, LX/04F;->UP_TIME:LX/04F;

    iget-object p1, p1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v10, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p1, LX/04F;->AVG_UP_TIME:LX/04F;

    iget-object p1, p1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v10, p1, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object p1, LX/04F;->RESTART_COUNT:LX/04F;

    iget-object p1, p1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {v10, p1, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    .line 469581
    invoke-static {v0, v10, v3, v1, v2}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/04D;LX/04G;)LX/1C2;

    .line 469582
    iget-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 469583
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aH:J

    .line 469584
    :cond_0
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 469585
    :cond_1
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ExoPlayer Error: video player service disconnected; current state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2q6;->D:LX/2qD;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v9, v12

    move-object v10, v12

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 469586
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 469587
    const-string v0, "onVideoServiceDisconnected, notify paused"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469588
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/2qT;

    sget-object v2, LX/7IB;->b:LX/7IB;

    invoke-direct {v1, v11, v2}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 469589
    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->am:I

    .line 469590
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 469591
    iput-object v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 469592
    iput-object v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    .line 469593
    const-string v0, "Service disconnected"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469594
    return-void
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 469385
    const-string v0, "setVideoResolution"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469386
    iget-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469387
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    .line 469388
    sget-object v0, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_0

    .line 469389
    if-eqz p3, :cond_2

    .line 469390
    iput-object p3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aC:Ljava/lang/String;

    .line 469391
    :cond_0
    :goto_0
    sget-object v0, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LX/2q6;->P:LX/09L;

    sget-object v1, LX/09L;->DASH:LX/09L;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/2q6;->P:LX/09L;

    sget-object v1, LX/09L;->DASH_LIVE:LX/09L;

    if-ne v0, v1, :cond_3

    .line 469392
    :cond_1
    :goto_1
    return-void

    .line 469393
    :cond_2
    sget-object v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v1, "CustomVideoQuality is null when videoresolution is CUSTOM_DEFINITION"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 469394
    :cond_3
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 469395
    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_5

    .line 469396
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aw:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 469397
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ar:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 469398
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 469399
    :goto_2
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac()V

    .line 469400
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469401
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    iget-object v2, p0, LX/2q6;->D:LX/2qD;

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    .line 469402
    :goto_3
    if-eqz v0, :cond_4

    .line 469403
    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v2}, LX/2q6;->c(LX/04g;)V

    .line 469404
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v2

    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469405
    :cond_4
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 469406
    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 469407
    if-eqz v4, :cond_4

    .line 469408
    const-string v2, "set video resolution with uri: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469409
    :try_start_0
    sget-object v2, LX/2qk;->FROM_SET_VIDEO_RESOLUTION:LX/2qk;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    .line 469410
    iget-object v2, p0, LX/2q6;->n:LX/2q3;

    invoke-virtual {v2, v4}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 469411
    iget-object v2, p0, LX/2q6;->n:LX/2q3;

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 469412
    iput v3, v2, LX/2q3;->h:I

    .line 469413
    invoke-direct {p0, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Landroid/net/Uri;)V

    .line 469414
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 469415
    if-eqz v0, :cond_7

    .line 469416
    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    sget-object v3, LX/7K4;->a:LX/7K4;

    invoke-direct {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(LX/04g;LX/7K4;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 469417
    :catch_0
    move-exception v9

    .line 469418
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 469419
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 469420
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469421
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v9, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    goto/16 :goto_1

    .line 469422
    :cond_5
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->av:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 469423
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 469424
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ar:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 469425
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 469426
    :cond_7
    :try_start_1
    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-static {p0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->l(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/04g;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 469427
    :catch_1
    move-exception v9

    .line 469428
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 469429
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469430
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v9, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    goto/16 :goto_1
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 469431
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 469432
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aZ:LX/2qF;

    .line 469433
    iput-object v1, v0, LX/2p8;->j:LX/2qG;

    .line 469434
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469435
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aZ:LX/2qF;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 469436
    iget-object p0, v1, LX/2p8;->a:Landroid/view/Surface;

    move-object v1, p0

    .line 469437
    invoke-virtual {v0, v1}, LX/2qF;->a(Landroid/view/Surface;)V

    .line 469438
    :cond_0
    return-void
.end method

.method public final a(LX/2qk;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 469439
    const-string v0, "Release action agent %s, pause action agent %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    aput-object v4, v3, v1

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->br:LX/04g;

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469440
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->br:LX/04g;

    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bq:LX/04g;

    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    if-ne v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 469441
    :goto_0
    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/2q6;->s:LX/19j;

    iget-boolean v3, v3, LX/19j;->E:Z

    if-nez v3, :cond_1

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->F:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 469442
    :goto_1
    sget-object v3, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    invoke-virtual {v3, p1}, LX/2qk;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v0, :cond_4

    .line 469443
    invoke-direct {p0, p1, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    .line 469444
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 469445
    goto :goto_0

    :cond_3
    move v0, v1

    .line 469446
    goto :goto_1

    .line 469447
    :cond_4
    invoke-direct {p0, p1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    goto :goto_2
.end method

.method public final a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .locals 0

    .prologue
    .line 469448
    return-void
.end method

.method public final a(LX/7KG;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 469449
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bx:Z

    if-eqz v0, :cond_0

    .line 469450
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 469451
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 469452
    sget-object v0, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;)V

    .line 469453
    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->I:Landroid/view/Surface;

    .line 469454
    sget-object v0, LX/2qE;->STATE_DESTROYED:LX/2qE;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->V:LX/2qE;

    .line 469455
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469456
    :goto_0
    return-void

    .line 469457
    :cond_0
    invoke-super {p0, p1}, LX/2q6;->a(LX/7KG;)V

    goto :goto_0
.end method

.method public final a(LX/7QP;)V
    .locals 1

    .prologue
    .line 469458
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, LX/2q9;->a(LX/7QP;)Z

    .line 469459
    return-void

    .line 469460
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
    .locals 2

    .prologue
    .line 469461
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469462
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
    .locals 2

    .prologue
    .line 469336
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469337
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .locals 12
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 469657
    const-string v0, "bindVideoSources"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469658
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 469659
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469660
    const-string v0, "GraphQLVideoBroadcastStatus: %s -> %s, LiveStatus: %s -> %s;"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v2}, Lcom/facebook/video/engine/VideoPlayerParams;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v10

    iget-boolean v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469661
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/2q6;->w:LX/04D;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, p1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v6, p0, LX/2q6;->y:LX/04G;

    move-object v4, p1

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/04D;LX/098;LX/098;LX/0lF;LX/04G;)V

    .line 469662
    :cond_0
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469663
    const-string v0, "live->VOD transition occurred; clean up player state if necessary"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469664
    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->H:Z

    if-eqz v0, :cond_1

    .line 469665
    sget-object v0, LX/04g;->BY_LIVE_POLLER_TRANSITION:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->e(LX/04g;)V

    .line 469666
    :cond_1
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_2

    .line 469667
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->av:I

    .line 469668
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aw:I

    .line 469669
    :cond_2
    iput-boolean v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->U:Z

    .line 469670
    iput v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ax:I

    .line 469671
    iput-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    .line 469672
    iput-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ar:Landroid/net/Uri;

    .line 469673
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    .line 469674
    iput-object v9, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->W:LX/0AR;

    .line 469675
    iput v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aR:I

    .line 469676
    iput v7, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bp:I

    .line 469677
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    if-eqz v0, :cond_3

    .line 469678
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ba:LX/1m6;

    invoke-virtual {v0, v9}, LX/1m6;->a(LX/0AR;)V

    .line 469679
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    .line 469680
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 469681
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->A:I

    .line 469682
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 469683
    const-string v0, "bindVideoSources: No valid video paths"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469684
    sget-object v1, LX/2qk;->FROM_BIND:LX/2qk;

    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bo:Z

    if-nez v0, :cond_4

    move v0, v7

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    .line 469685
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    .line 469686
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    .line 469687
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S:LX/097;

    .line 469688
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 469689
    :goto_1
    return-void

    :cond_4
    move v0, v8

    .line 469690
    goto :goto_0

    .line 469691
    :cond_5
    iput v8, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    .line 469692
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aA:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 469693
    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    if-eqz v1, :cond_11

    if-eqz v0, :cond_11

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    if-eqz v1, :cond_11

    move v4, v7

    .line 469694
    :goto_2
    if-eqz v4, :cond_12

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    move-object v2, v1

    .line 469695
    :goto_3
    const-string v1, "BindVideoSources, uriToTry: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v8

    invoke-virtual {p0, v1, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469696
    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/facebook/video/engine/VideoDataSource;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 469697
    :cond_6
    sget-object v3, LX/2qk;->FROM_BIND:LX/2qk;

    iget-boolean v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bo:Z

    if-nez v1, :cond_14

    move v1, v7

    :goto_4
    invoke-direct {p0, v3, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    move v3, v7

    .line 469698
    :goto_5
    if-eqz v0, :cond_8

    .line 469699
    iget-object v1, p0, LX/2q6;->S:LX/097;

    sget-object v5, LX/097;->FROM_SAVED_OFFLINE_LOCAL_FILE:LX/097;

    if-eq v1, v5, :cond_7

    .line 469700
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S:LX/097;

    .line 469701
    :cond_7
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    .line 469702
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ar:Landroid/net/Uri;

    .line 469703
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/19w;

    iget-object v5, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 469704
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/19w;

    iget-object v5, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/19w;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    .line 469705
    :goto_6
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    .line 469706
    :cond_8
    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    .line 469707
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ah()V

    .line 469708
    if-eqz v4, :cond_16

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aw:I

    :goto_7
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 469709
    if-eqz v4, :cond_17

    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    :goto_8
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    .line 469710
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac()V

    .line 469711
    const-string v0, "bindVideoSources: (%s): %s: %s"

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v2, p0, LX/2q6;->S:LX/097;

    iget-object v2, v2, LX/097;->value:Ljava/lang/String;

    aput-object v2, v1, v8

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ap:Landroid/net/Uri;

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->as:Landroid/net/Uri;

    aput-object v2, v1, v10

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469712
    if-nez p3, :cond_9

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->z:Z

    if-eqz v0, :cond_10

    .line 469713
    :cond_9
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aL:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    .line 469714
    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    .line 469715
    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->C:Z

    if-nez v1, :cond_b

    :cond_a
    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->E:Z

    if-eqz v0, :cond_19

    :cond_b
    move v0, v7

    .line 469716
    :goto_9
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget v1, v1, LX/0wq;->L:I

    if-ltz v1, :cond_1e

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v1, v1, LX/04D;->origin:Ljava/lang/String;

    iget-object v2, p0, LX/2q6;->w:LX/04D;

    iget-object v2, v2, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 469717
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bw:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/04J;

    iget-object v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/04J;->a(Ljava/lang/String;)LX/0P1;

    move-result-object v2

    .line 469718
    if-eqz v2, :cond_1e

    .line 469719
    sget-object v1, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object v1, v1, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 469720
    sget-object v4, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    iget-object v4, v4, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 469721
    invoke-static {v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;)I

    move-result v1

    .line 469722
    invoke-static {v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;)I

    move-result v2

    .line 469723
    if-ltz v1, :cond_1e

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget v4, v4, LX/0wq;->L:I

    if-gt v1, v4, :cond_1e

    if-ltz v2, :cond_1e

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget v1, v1, LX/0wq;->M:I

    if-gt v2, v1, :cond_1e

    .line 469724
    const/4 v1, 0x1

    .line 469725
    :goto_a
    move v1, v1

    .line 469726
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-object v4, p0, LX/2q6;->w:LX/04D;

    iget-object v4, v4, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/0wq;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    if-eqz v1, :cond_1a

    :cond_c
    move v1, v7

    :goto_b
    and-int/2addr v1, v0

    .line 469727
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->J:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    if-eqz v0, :cond_1b

    :cond_d
    move v0, v7

    :goto_c
    and-int/2addr v0, v1

    .line 469728
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->D:Z

    if-eqz v1, :cond_e

    if-nez v3, :cond_e

    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->S()Z

    move-result v1

    if-nez v1, :cond_1c

    .line 469729
    :cond_e
    :goto_d
    if-nez p3, :cond_f

    if-eqz v0, :cond_10

    :cond_f
    if-eqz v7, :cond_10

    .line 469730
    const-string v0, "Starting to prepare the player"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469731
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->P()V

    .line 469732
    :cond_10
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 469733
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 469734
    iput v1, v0, LX/2px;->i:I

    .line 469735
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->au:I

    .line 469736
    iput v1, v0, LX/2px;->j:I

    .line 469737
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Z:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2px;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_11
    move v4, v8

    .line 469738
    goto/16 :goto_2

    .line 469739
    :cond_12
    if-eqz v0, :cond_13

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    move-object v2, v1

    goto/16 :goto_3

    :cond_13
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aq:Landroid/net/Uri;

    move-object v2, v1

    goto/16 :goto_3

    :cond_14
    move v1, v8

    .line 469740
    goto/16 :goto_4

    .line 469741
    :cond_15
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->at:Ljava/lang/String;

    goto/16 :goto_6

    .line 469742
    :cond_16
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->av:I

    goto/16 :goto_7

    .line 469743
    :cond_17
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto/16 :goto_8

    .line 469744
    :cond_18
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->E:Z

    if-nez v0, :cond_b

    :cond_19
    move v0, v8

    goto/16 :goto_9

    :cond_1a
    move v1, v8

    .line 469745
    goto :goto_b

    :cond_1b
    move v0, v8

    .line 469746
    goto :goto_c

    :cond_1c
    move v7, v8

    .line 469747
    goto :goto_d

    :cond_1d
    move v3, v8

    goto/16 :goto_5

    :cond_1e
    const/4 v1, 0x0

    goto/16 :goto_a
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 469838
    invoke-static {p1}, LX/7Jj;->valueOf(Ljava/lang/String;)LX/7Jj;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 469839
    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/7Jj;Ljava/lang/String;Ljava/lang/String;J)V

    .line 469840
    return-void
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 469836
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469837
    return-void
.end method

.method public final a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 469797
    const-string v0, "onStreamEvaluate"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469798
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 469799
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bv:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19w;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 469800
    invoke-virtual {v0, v3}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v4

    .line 469801
    if-nez v4, :cond_e

    .line 469802
    const/4 v4, 0x0

    .line 469803
    :goto_0
    move-object v3, v4

    .line 469804
    if-eqz v3, :cond_2

    .line 469805
    array-length v4, p4

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, p4, v0

    .line 469806
    iget-object v6, v5, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 469807
    iput-object v5, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 469808
    :cond_0
    :goto_2
    return-void

    .line 469809
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 469810
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    if-eqz v0, :cond_0

    .line 469811
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 469812
    array-length v3, p4

    move v0, v1

    :goto_3
    if-ge v0, v3, :cond_3

    aget-object v4, p4, v0

    .line 469813
    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    iget-object v4, v4, LX/0AR;->d:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469814
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 469815
    :cond_3
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469816
    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    invoke-interface {v0, v4}, LX/2pf;->a(Ljava/util/List;)V

    goto :goto_4

    .line 469817
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bA:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    sget-object v3, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-eq v0, v3, :cond_6

    .line 469818
    array-length v3, p4

    move v0, v1

    :goto_5
    if-ge v0, v3, :cond_6

    aget-object v4, p4, v0

    .line 469819
    iget-object v5, v4, LX/0AR;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bA:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 469820
    iput-object v4, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    goto :goto_2

    .line 469821
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 469822
    :cond_6
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    sget-object v3, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aC:Ljava/lang/String;

    const-string v3, "Auto"

    if-ne v0, v3, :cond_9

    move v0, v2

    .line 469823
    :goto_6
    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    sget-object v3, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    if-ne v0, v3, :cond_a

    :cond_7
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aQ:LX/04o;

    instance-of v0, v0, LX/1mA;

    if-eqz v0, :cond_a

    .line 469824
    invoke-static/range {p0 .. p5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V

    .line 469825
    :cond_8
    :goto_7
    const-string v0, "%d formats, max bitrate=%d, min bitrate=%d, chose %d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    array-length v4, p4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    aget-object v1, p4, v1

    iget v1, v1, LX/0AR;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v2

    const/4 v1, 0x2

    array-length v2, p4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, p4, v2

    iget v2, v2, LX/0AR;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x3

    iget-object v2, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v2, v2, LX/0AR;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_9
    move v0, v1

    .line 469826
    goto :goto_6

    .line 469827
    :cond_a
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    sget-object v3, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-ne v0, v3, :cond_d

    .line 469828
    array-length v3, p4

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_b

    aget-object v4, p4, v0

    .line 469829
    iget-object v5, v4, LX/0AR;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aC:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 469830
    iput-object v4, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    .line 469831
    :cond_b
    iget-object v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    if-nez v0, :cond_8

    .line 469832
    sget-object v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v3, "Error setting the CUSTOM_DEFINITION format to %s.Falling back to DASH"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aC:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469833
    invoke-static/range {p0 .. p5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V

    goto :goto_7

    .line 469834
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 469835
    :cond_d
    aget-object v0, p4, v1

    iput-object v0, p5, Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;->c:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    goto :goto_7

    :cond_e
    iget-object v4, v4, LX/7Jg;->i:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 469793
    const-string v0, "mute"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469794
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;F)V

    .line 469795
    return-void

    .line 469796
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 469786
    :try_start_0
    const-string v2, "isPlaying"

    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469787
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v2, v3}, LX/04j;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, v0, :cond_0

    .line 469788
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 469789
    goto :goto_0

    .line 469790
    :catch_0
    move-exception v0

    .line 469791
    const-string v2, "Service RemoteException when getPlayWhenReady"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 469792
    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 469779
    const-string v1, "getCurrentPosition"

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469780
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469781
    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469782
    :cond_0
    :goto_0
    return v0

    .line 469783
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v1, v2}, LX/04j;->f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 469784
    :catch_0
    move-exception v1

    .line 469785
    const-string v2, "Service RemoteException when getCurrentPositionMs"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(LX/04g;)V
    .locals 4

    .prologue
    .line 469762
    const-string v0, "stop"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469763
    const-string v1, "%s, %s"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    iget-object v0, p1, LX/04g;->value:Ljava/lang/String;

    :goto_0
    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469764
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 469765
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 469766
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->R:LX/04g;

    .line 469767
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 469768
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aI:J

    .line 469769
    return-void

    .line 469770
    :cond_1
    const-string v0, "none"

    goto :goto_0

    .line 469771
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469772
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469773
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$6;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$6;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;LX/04g;)V

    invoke-virtual {v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 469774
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    invoke-virtual {v0, v1}, LX/19Z;->a(I)V

    .line 469775
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    .line 469776
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->j(LX/04g;)V

    .line 469777
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469778
    iget-object v2, p0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$7;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$7;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2pf;LX/04g;)V

    invoke-virtual {v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method public final b(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 469758
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469759
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/graphics/RectF;)V

    .line 469760
    :goto_0
    return-void

    .line 469761
    :cond_0
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$8;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient$8;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/graphics/RectF;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 469753
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->O(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 469754
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_0

    .line 469755
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->P()V

    .line 469756
    :cond_0
    :goto_0
    return-void

    .line 469757
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469752
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aB:Ljava/util/List;

    return-object v0
.end method

.method public final c(LX/04g;)V
    .locals 0

    .prologue
    .line 469749
    invoke-super {p0, p1}, LX/2q6;->c(LX/04g;)V

    .line 469750
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->k(LX/04g;)V

    .line 469751
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469748
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aC:Ljava/lang/String;

    return-object v0
.end method

.method public final e()LX/2oi;
    .locals 1

    .prologue
    .line 469577
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->az:LX/2oi;

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 469655
    sget-object v0, LX/2qk;->EXTERNAL:LX/2qk;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(LX/2qk;Z)V

    .line 469656
    return-void
.end method

.method public final g(LX/04g;)V
    .locals 24
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 469632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469633
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v2}, LX/2q9;->c()V

    .line 469634
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 469635
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 469636
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->br:LX/04g;

    .line 469637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aG:LX/19Z;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aF:I

    sget-object v4, LX/7IJ;->PAUSED:LX/7IJ;

    invoke-virtual {v2, v3, v4}, LX/19Z;->a(ILX/7IJ;)V

    .line 469638
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 469639
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 469640
    invoke-interface {v2}, LX/2pf;->c()V

    goto :goto_0

    .line 469641
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 469642
    invoke-virtual/range {p0 .. p0}, LX/2q6;->n()V

    .line 469643
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aa:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->h()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v4}, LX/2p8;->i()I

    move-result v4

    invoke-static {v2, v3, v4}, LX/7K8;->b(III)D

    move-result-wide v2

    .line 469644
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    invoke-virtual {v4, v2, v3, v2, v3}, LX/2p8;->a(DD)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->H:Landroid/graphics/Bitmap;

    .line 469645
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 469646
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->H:Landroid/graphics/Bitmap;

    invoke-interface {v2, v4}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 469647
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v7

    .line 469648
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v2}, LX/09M;->d()V

    .line 469649
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->m(LX/04g;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 469650
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->z:LX/04g;

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v12, v12, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v2 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 469651
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    sget-object v4, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v7, v4}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 469652
    return-void

    .line 469653
    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->P:LX/09L;

    iget-object v11, v2, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v13, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v14, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, LX/2q6;->B:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->z:LX/04g;

    iget-object v0, v2, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->O:LX/09M;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->S:LX/097;

    iget-object v0, v2, LX/097;->value:Ljava/lang/String;

    move-object/from16 v23, v0

    move v15, v7

    invoke-virtual/range {v8 .. v23}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 469654
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v2}, LX/09M;->a()V

    goto :goto_2
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 469630
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ac:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/os/Message;)V

    .line 469631
    return-void
.end method

.method public final k()Landroid/view/View;
    .locals 1

    .prologue
    .line 469627
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aY:LX/2p8;

    .line 469628
    iget-object p0, v0, LX/2p8;->i:Landroid/view/TextureView;

    move-object v0, p0

    .line 469629
    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469626
    const-string v0, "old_api_exo"

    return-object v0
.end method

.method public final s()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 469619
    const-string v1, "getAbsolutePlaybackPosition"

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469620
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469621
    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aD:I

    .line 469622
    :cond_0
    :goto_0
    return v0

    .line 469623
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/04j;->c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 469624
    :catch_0
    move-exception v1

    .line 469625
    const-string v2, "Service RemoteException when getInternalCurrentPositionMs"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final t()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 469613
    const-string v1, "getCurrentBroadcastTimeMs"

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->G:Ljava/lang/String;

    .line 469614
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Y(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 469615
    :goto_0
    return v0

    .line 469616
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v3, p0, LX/2q6;->s:LX/19j;

    iget-boolean v3, v3, LX/19j;->aa:Z

    invoke-interface {v1, v2, v3}, LX/04j;->c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 469617
    :catch_0
    move-exception v1

    .line 469618
    const-string v2, "Service RemoteException when getInternalCurrentPositionMs"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 469612
    const/4 v0, 0x1

    return v0
.end method

.method public final v()J
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const-wide/16 v8, 0x0

    const-wide/16 v0, -0x1

    .line 469600
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 469601
    :cond_0
    :goto_0
    return-wide v0

    .line 469602
    :cond_1
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    if-eqz v2, :cond_0

    .line 469603
    :try_start_0
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v2, v3}, LX/04j;->f(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v4

    .line 469604
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v2, v3}, LX/04j;->g(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)J

    move-result-wide v6

    .line 469605
    cmp-long v2, v6, v0

    if-eqz v2, :cond_0

    .line 469606
    cmp-long v2, v4, v8

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget v2, v2, LX/0wq;->m:I

    int-to-long v2, v2

    .line 469607
    :goto_1
    sub-long v4, v6, v4

    mul-long/2addr v4, v10

    div-long v2, v4, v2

    .line 469608
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const-wide/16 v4, 0x64

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 469609
    :cond_2
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aJ:LX/0wq;

    iget v2, v2, LX/0wq;->n:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v2, v2

    goto :goto_1

    .line 469610
    :catch_0
    move-exception v2

    .line 469611
    const-string v3, "Failed to fetch video buffer position"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 469597
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bs:Z

    .line 469598
    invoke-super {p0}, LX/2q6;->x()V

    .line 469599
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 469595
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Landroid/view/Surface;)V

    .line 469596
    return-void
.end method
