.class public Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3FQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3FV;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/23q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/23q",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/23q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/23q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365325
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 365326
    iput-object p1, p0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->a:LX/0Ot;

    .line 365327
    iput-object p2, p0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->b:LX/23q;

    .line 365328
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;
    .locals 5

    .prologue
    .line 365329
    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    monitor-enter v1

    .line 365330
    :try_start_0
    sget-object v0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365331
    sput-object v2, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365332
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365333
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365334
    new-instance v4, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    const/16 v3, 0x848

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/23q;->a(LX/0QB;)LX/23q;

    move-result-object v3

    check-cast v3, LX/23q;

    invoke-direct {v4, p0, v3}, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;-><init>(LX/0Ot;LX/23q;)V

    .line 365335
    move-object v0, v4

    .line 365336
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365337
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365338
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 365340
    check-cast p2, LX/3FV;

    check-cast p3, LX/1Po;

    .line 365341
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->b:LX/23q;

    invoke-virtual {v0, p2, p3}, LX/23q;->a(LX/3FV;LX/1Po;)LX/3Qx;

    move-result-object v1

    .line 365342
    if-eqz v1, :cond_0

    .line 365343
    iget-object v0, p0, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3FZ;

    invoke-direct {v2, v1}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 365344
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
