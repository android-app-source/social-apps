.class public Lcom/facebook/video/player/RichVideoPlayer;
.super LX/2oX;
.source ""


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/2pb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:LX/3It;

.field private G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:LX/04D;

.field private I:LX/04G;

.field private J:LX/04H;

.field private K:LX/04g;

.field private L:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:LX/1aZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:I

.field private O:LX/2oi;

.field public P:Z

.field private Q:LX/2pG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Z

.field private S:Z

.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2oj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2om;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/13l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/video/player/plugins/VideoPlugin;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final n:LX/2oo;

.field private final o:LX/2oZ;

.field private final p:LX/2ob;

.field private final q:LX/2oc;

.field private final r:LX/2od;

.field private final s:LX/2oe;

.field private final t:LX/2of;

.field private final u:LX/2og;

.field private final v:LX/2oh;

.field public final w:LX/2on;

.field private final x:Landroid/media/AudioManager;

.field public y:LX/3Iv;

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 466814
    const-class v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/RichVideoPlayer;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 466812
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 466813
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466810
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 466811
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 466772
    invoke-direct {p0, p1, p2, p3}, LX/2oX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 466773
    new-instance v0, LX/2oZ;

    invoke-direct {v0, p0}, LX/2oZ;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->o:LX/2oZ;

    .line 466774
    new-instance v0, LX/2ob;

    invoke-direct {v0, p0}, LX/2ob;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->p:LX/2ob;

    .line 466775
    new-instance v0, LX/2oc;

    invoke-direct {v0, p0}, LX/2oc;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->q:LX/2oc;

    .line 466776
    new-instance v0, LX/2od;

    invoke-direct {v0, p0}, LX/2od;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->r:LX/2od;

    .line 466777
    new-instance v0, LX/2oe;

    invoke-direct {v0, p0}, LX/2oe;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->s:LX/2oe;

    .line 466778
    new-instance v0, LX/2of;

    invoke-direct {v0, p0}, LX/2of;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->t:LX/2of;

    .line 466779
    new-instance v0, LX/2og;

    invoke-direct {v0, p0}, LX/2og;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->u:LX/2og;

    .line 466780
    new-instance v0, LX/2oh;

    invoke-direct {v0, p0}, LX/2oh;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->v:LX/2oh;

    .line 466781
    iput-boolean v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->C:Z

    .line 466782
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    .line 466783
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    .line 466784
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->J:LX/04H;

    .line 466785
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->K:LX/04g;

    .line 466786
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->O:LX/2oi;

    .line 466787
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    .line 466788
    const-class v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 466789
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->x:Landroid/media/AudioManager;

    .line 466790
    new-instance v0, LX/2on;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->e:LX/0SG;

    invoke-direct {v0, p0, v3}, LX/2on;-><init>(Lcom/facebook/video/player/RichVideoPlayer;LX/0SG;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->w:LX/2on;

    .line 466791
    sget-object v0, LX/03r;->RichVideoPlayer:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 466792
    :try_start_0
    const/16 v0, 0x0

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->C:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466793
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 466794
    new-instance v0, LX/2oo;

    invoke-direct {v0, p0}, LX/2oo;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->n:LX/2oo;

    .line 466795
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->q:LX/2oc;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466796
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->p:LX/2ob;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466797
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->r:LX/2od;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466798
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->s:LX/2oe;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466799
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->o:LX/2oZ;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466800
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->t:LX/2of;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466801
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->u:LX/2og;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466802
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->h:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-eqz v0, :cond_0

    .line 466803
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->v:LX/2oh;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466804
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->n:LX/2oo;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 466805
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->d:LX/13l;

    invoke-virtual {v0, p0}, LX/13l;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 466806
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentVolume()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->S:Z

    .line 466807
    return-void

    .line 466808
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    :cond_1
    move v0, v2

    .line 466809
    goto :goto_0
.end method

.method private A()Z
    .locals 6

    .prologue
    .line 466759
    const/4 v0, 0x0

    .line 466760
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v1, :cond_0

    .line 466761
    const/4 v0, 0x1

    .line 466762
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->B()Z

    move-result v1

    .line 466763
    const-string v2, "RichVideoPlayer.PlaybackControllerProvider.get"

    const v3, -0x455c0e40

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466764
    :try_start_0
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->c:LX/2om;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->C()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    iget-boolean v5, p0, Lcom/facebook/video/player/RichVideoPlayer;->C:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v1, v3, v4, v5}, LX/2om;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;LX/04D;Ljava/lang/Boolean;)LX/2pb;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466765
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->K:LX/04g;

    invoke-virtual {v1, v2}, LX/2pb;->c(LX/04g;)V

    .line 466766
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    invoke-virtual {v1, v2}, LX/2pb;->a(LX/04G;)V

    .line 466767
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->J:LX/04H;

    invoke-virtual {v1, v2}, LX/2pb;->a(LX/04H;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466768
    const v1, 0x318c382f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 466769
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 466770
    return v0

    .line 466771
    :catchall_0
    move-exception v0

    const v1, -0x26c1e19c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private B()Z
    .locals 1

    .prologue
    .line 466758
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()Z
    .locals 1

    .prologue
    .line 466757
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 9

    .prologue
    .line 466750
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_1

    .line 466751
    :cond_0
    sget-object v0, Lcom/facebook/video/player/RichVideoPlayer;->m:Ljava/lang/String;

    const-string v1, "videoPlayerParams is null"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 466752
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 466753
    :goto_0
    return-void

    .line 466754
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->i:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentVolume()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v6, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 466755
    iget-object v7, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v7, v7

    .line 466756
    iget-object v8, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v8}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method private a(LX/2pa;ZZ)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 466726
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-nez v2, :cond_3

    .line 466727
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    .line 466728
    :goto_0
    invoke-direct {p0, p2}, Lcom/facebook/video/player/RichVideoPlayer;->b(Z)V

    .line 466729
    if-eqz v0, :cond_2

    .line 466730
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->w:LX/2on;

    invoke-virtual {v0}, LX/2on;->b()V

    .line 466731
    const-string v0, "RichVideoPlayer.PlaybackController.bindVideoSources"

    const v1, -0x7e1a23e6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466732
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1}, Lcom/facebook/video/engine/VideoPlayerParams;->a(Lcom/facebook/video/engine/VideoPlayerParams;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v1, 0x0

    .line 466733
    if-nez v0, :cond_6

    .line 466734
    :cond_0
    :goto_1
    move v0, v1

    .line 466735
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->g:LX/0ad;

    sget-short v1, LX/0ws;->eO:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466736
    sget-object v0, LX/04g;->BY_WAIT_FOR_LIVE_VOD_TRANSITION:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 466737
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1, p3}, LX/2pb;->a(Lcom/facebook/video/engine/VideoPlayerParams;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466738
    const v0, -0x12e32004

    invoke-static {v0}, LX/02m;->a(I)V

    .line 466739
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v2, LX/2ou;

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    :goto_2
    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466740
    iget-object p0, v3, LX/2pb;->y:LX/2qV;

    move-object v3, p0

    .line 466741
    invoke-direct {v2, v0, v3}, LX/2ou;-><init>(Ljava/lang/String;LX/2qV;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 466742
    return-void

    .line 466743
    :cond_3
    iget-object v2, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v2, :cond_4

    iget-object v2, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v2, v3}, Lcom/facebook/video/engine/VideoPlayerParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 466744
    :goto_3
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    invoke-virtual {v1}, LX/2pa;->g()LX/2pZ;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/2pZ;->b(LX/2pa;)LX/2pZ;

    move-result-object v1

    invoke-virtual {v1}, LX/2pZ;->b()LX/2pa;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 466745
    goto :goto_3

    .line 466746
    :catchall_0
    move-exception v0

    const v1, -0x6f7df3bb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 466747
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 466748
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 466749
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v2, v3, :cond_7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v2, v3, :cond_0

    :cond_7
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/video/player/RichVideoPlayer;LX/0Sh;LX/2oj;LX/2om;LX/13l;LX/0SG;LX/0Uh;LX/0ad;LX/2ml;LX/1C2;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/2oj;",
            "LX/2om;",
            "LX/13l;",
            "LX/0SG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/2ml;",
            "LX/1C2;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 466725
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    iput-object p3, p0, Lcom/facebook/video/player/RichVideoPlayer;->c:LX/2om;

    iput-object p4, p0, Lcom/facebook/video/player/RichVideoPlayer;->d:LX/13l;

    iput-object p5, p0, Lcom/facebook/video/player/RichVideoPlayer;->e:LX/0SG;

    iput-object p6, p0, Lcom/facebook/video/player/RichVideoPlayer;->f:LX/0Uh;

    iput-object p7, p0, Lcom/facebook/video/player/RichVideoPlayer;->g:LX/0ad;

    iput-object p8, p0, Lcom/facebook/video/player/RichVideoPlayer;->h:LX/2ml;

    iput-object p9, p0, Lcom/facebook/video/player/RichVideoPlayer;->i:LX/1C2;

    iput-object p10, p0, Lcom/facebook/video/player/RichVideoPlayer;->j:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v10}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    new-instance v3, LX/2oj;

    invoke-static {v10}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-direct {v3, v2}, LX/2oj;-><init>(LX/0Sh;)V

    move-object v2, v3

    check-cast v2, LX/2oj;

    const-class v3, LX/2om;

    invoke-interface {v10, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2om;

    invoke-static {v10}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v4

    check-cast v4, LX/13l;

    invoke-static {v10}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v10}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v10}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v8

    check-cast v8, LX/2ml;

    invoke-static {v10}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v9

    check-cast v9, LX/1C2;

    const/16 v11, 0x259

    invoke-static {v10, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/facebook/video/player/RichVideoPlayer;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/0Sh;LX/2oj;LX/2om;LX/13l;LX/0SG;LX/0Uh;LX/0ad;LX/2ml;LX/1C2;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    .line 466717
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_1

    .line 466718
    :cond_0
    sget-object v0, Lcom/facebook/video/player/RichVideoPlayer;->m:Ljava/lang/String;

    const-string v1, "videoPlayerParams is null"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 466719
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 466720
    :goto_0
    return-void

    .line 466721
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->i:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentVolume()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v6, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 466722
    iget-object v7, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v7, v7

    .line 466723
    iget-object v8, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v8}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 466710
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->A()Z

    move-result v0

    .line 466711
    invoke-direct {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(Z)V

    .line 466712
    if-eqz v0, :cond_0

    .line 466713
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    .line 466714
    iget-object p0, v0, LX/2pb;->H:LX/2oj;

    iget-object p1, v0, LX/2pb;->b:Ljava/util/List;

    invoke-static {v1, p0, p1}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 466715
    iput-object v1, v0, LX/2pb;->H:LX/2oj;

    .line 466716
    :cond_0
    return-void
.end method

.method private c(LX/2oy;)V
    .locals 2

    .prologue
    .line 466702
    invoke-virtual {p1, p0}, LX/2oy;->a(Landroid/view/ViewGroup;)V

    .line 466703
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    invoke-virtual {p1, v0}, LX/2oy;->setEventBus(LX/2oj;)V

    .line 466704
    instance-of v0, p1, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_1

    .line 466705
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 466706
    :goto_0
    instance-of v0, p1, LX/2pG;

    if-eqz v0, :cond_0

    .line 466707
    check-cast p1, LX/2pG;

    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    .line 466708
    :cond_0
    return-void

    .line 466709
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private c(Z)V
    .locals 4

    .prologue
    .line 466688
    if-eqz p1, :cond_1

    .line 466689
    const-string v0, "RichVideoPlayer.RichVideoPlayerPlugins.reload"

    const v1, 0x479d1c82

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466690
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466691
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    invoke-virtual {v0, v2, p0, v3}, LX/2oy;->a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 466692
    :catchall_0
    move-exception v0

    const v1, -0x35c49a90    # -3070300.0f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, -0x795560ca

    invoke-static {v0}, LX/02m;->a(I)V

    .line 466693
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466694
    invoke-virtual {v0}, LX/2oy;->q()V

    goto :goto_2

    .line 466695
    :cond_1
    const-string v0, "RichVideoPlayer.RichVideoPlayerPlugins.load"

    const v1, -0x5860ebc3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466696
    :try_start_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466697
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    invoke-virtual {v0, v2, p0, v3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 466698
    :catchall_1
    move-exception v0

    const v1, 0x5a81c7fb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    const v0, -0x4fbb02ab

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 466699
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 466700
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    invoke-virtual {v0}, LX/2pa;->g()LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->a()LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    .line 466701
    :cond_4
    return-void
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 466687
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->f:LX/0Uh;

    const/16 v1, 0x2c1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 466663
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 466664
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    invoke-virtual {v0}, LX/2pa;->g()LX/2pZ;

    move-result-object v2

    .line 466665
    iget v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    if-lez v0, :cond_0

    .line 466666
    const-string v0, "SeekPositionMsKey"

    iget v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 466667
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->O:LX/2oi;

    sget-object v3, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    if-eq v0, v3, :cond_1

    .line 466668
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->O:LX/2oi;

    sget-object v4, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v0, v4, :cond_4

    const/4 v0, 0x1

    .line 466669
    :goto_0
    iput-boolean v0, v3, LX/2oH;->n:Z

    .line 466670
    move-object v0, v3

    .line 466671
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 466672
    iput-object v0, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 466673
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->O:LX/2oi;

    .line 466674
    :cond_1
    invoke-virtual {v2}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 466675
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 466676
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->M:LX/1aZ;

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 466677
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->M:LX/1aZ;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 466678
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 466679
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 466680
    :cond_2
    iget v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    if-lez v0, :cond_3

    .line 466681
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    iget v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    sget-object v3, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {v0, v2, v3}, LX/2pb;->a(ILX/04g;)V

    .line 466682
    :cond_3
    iput-object v5, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    .line 466683
    iput-object v5, p0, Lcom/facebook/video/player/RichVideoPlayer;->M:LX/1aZ;

    .line 466684
    iput v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    .line 466685
    return-void

    :cond_4
    move v0, v1

    .line 466686
    goto :goto_0
.end method

.method public static d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V
    .locals 1

    .prologue
    .line 466656
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 466657
    instance-of v0, p1, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466658
    invoke-direct {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2oy;)V

    .line 466659
    check-cast p1, Lcom/facebook/video/player/plugins/VideoPlugin;

    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466660
    const v0, 0x7f0d0950

    invoke-virtual {p0, v0}, LX/2oX;->setInnerResource(I)V

    .line 466661
    :goto_0
    return-void

    .line 466662
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2oy;)V

    goto :goto_0
.end method

.method private z()V
    .locals 1

    .prologue
    .line 466648
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->M:LX/1aZ;

    .line 466649
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->N:I

    .line 466650
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->O:LX/2oi;

    .line 466651
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    .line 466652
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 466653
    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    .line 466654
    return-void

    .line 466655
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/2oy;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/2oy;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466645
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466646
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 466647
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/04g;)V
    .locals 2

    .prologue
    .line 466641
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->B()Z

    move-result v0

    .line 466642
    if-nez v0, :cond_0

    .line 466643
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2qc;

    invoke-direct {v1, p1, p2}, LX/2qc;-><init>(ILX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466644
    :cond_0
    return-void
.end method

.method public a(LX/04g;)V
    .locals 1

    .prologue
    .line 466639
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 466640
    return-void
.end method

.method public a(LX/04g;I)V
    .locals 2

    .prologue
    .line 466635
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    invoke-interface {v0}, LX/2pG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466636
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    invoke-interface {v0, p1}, LX/2pG;->a(LX/04g;)V

    .line 466637
    :goto_0
    return-void

    .line 466638
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2qa;

    invoke-direct {v1, p1, p2}, LX/2qa;-><init>(LX/04g;I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final a(LX/2oa;)V
    .locals 1

    .prologue
    .line 466820
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    invoke-virtual {v0, p1}, LX/0b4;->a(LX/0b2;)Z

    .line 466821
    return-void
.end method

.method public final a(LX/2oi;LX/04g;)V
    .locals 2

    .prologue
    .line 466818
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/2pb;->a(LX/2oi;LX/04g;Ljava/lang/String;)V

    .line 466819
    return-void
.end method

.method public final declared-synchronized a(LX/2pa;)V
    .locals 2

    .prologue
    .line 466891
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466892
    monitor-exit p0

    return-void

    .line 466893
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2pa;LX/0Px;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2pa;",
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2oy;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .line 466894
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 466895
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 466896
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    .line 466897
    if-eqz v0, :cond_0

    .line 466898
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466899
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 466900
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466901
    if-eqz p3, :cond_2

    .line 466902
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 466903
    :cond_2
    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, v3, p0, p1}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    goto :goto_1

    .line 466904
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466905
    invoke-virtual {v0}, LX/2oy;->q()V

    goto :goto_2

    .line 466906
    :cond_4
    return-void
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V
    .locals 2

    .prologue
    .line 466907
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466908
    iget-object v1, v0, LX/2pb;->p:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466909
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    sget-object p0, LX/2qV;->PLAYING:LX/2qV;

    if-ne v1, p0, :cond_0

    .line 466910
    invoke-static {v0}, LX/2pb;->D(LX/2pb;)V

    .line 466911
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 466912
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466913
    :cond_0
    return-void

    .line 466914
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466915
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 466916
    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 466917
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/3JB;

    invoke-direct {v1, p1}, LX/3JB;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466918
    return-void
.end method

.method public a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 466919
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_1

    .line 466920
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466921
    iput-boolean p1, v0, LX/2pb;->B:Z

    .line 466922
    iget-boolean p0, v0, LX/2pb;->B:Z

    if-eqz p0, :cond_2

    .line 466923
    invoke-static {v0}, LX/2pb;->I(LX/2pb;)V

    .line 466924
    :cond_0
    :goto_0
    iget-object p0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {p0, p1, p2}, LX/2q7;->a(ZLX/04g;)V

    .line 466925
    :cond_1
    return-void

    .line 466926
    :cond_2
    iget-object p0, v0, LX/2pb;->y:LX/2qV;

    invoke-virtual {p0}, LX/2qV;->isPlayingState()Z

    move-result p0

    if-nez p0, :cond_3

    invoke-virtual {v0}, LX/2pb;->o()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 466927
    :cond_3
    invoke-static {v0}, LX/2pb;->H(LX/2pb;)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 466928
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 466929
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 466930
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466931
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 466932
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 466933
    :cond_0
    instance-of v4, v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v4, :cond_1

    .line 466934
    iput-object v5, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466935
    :cond_1
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 466936
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    .line 466937
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    .line 466938
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 466939
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 466940
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 466941
    iput-object v5, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    .line 466942
    return-object v2
.end method

.method public b(LX/04g;)V
    .locals 2

    .prologue
    .line 466943
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    invoke-interface {v0}, LX/2pG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466944
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    invoke-interface {v0, p1}, LX/2pG;->b(LX/04g;)V

    .line 466945
    :goto_0
    return-void

    .line 466946
    :cond_0
    const-string v0, "RichVideoPlayer.pause"

    const v1, -0x18332348

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466947
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2qb;

    invoke-direct {v1, p1}, LX/2qb;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466948
    const v0, -0x7691f491

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x71c20d66

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(LX/2oa;)V
    .locals 1

    .prologue
    .line 466949
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    invoke-virtual {v0, p1}, LX/0b4;->b(LX/0b2;)Z

    .line 466950
    return-void
.end method

.method public final declared-synchronized b(LX/2pa;)V
    .locals 2

    .prologue
    .line 466951
    monitor-enter p0

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466952
    monitor-exit p0

    return-void

    .line 466953
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/7QP;)V
    .locals 2
    .param p1    # LX/7QP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466889
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2qf;

    invoke-direct {v1, p1}, LX/2qf;-><init>(LX/7QP;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466890
    return-void
.end method

.method public final b(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V
    .locals 1

    .prologue
    .line 466954
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466955
    iget-object p0, v0, LX/2pb;->o:Landroid/os/Handler;

    invoke-static {p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 466956
    iget-object p0, v0, LX/2pb;->p:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 466957
    return-void
.end method

.method public final b(LX/2oy;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466882
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 466883
    if-eqz v0, :cond_1

    .line 466884
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-ne p1, v1, :cond_0

    .line 466885
    iput-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466886
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    if-ne p1, v1, :cond_1

    .line 466887
    iput-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    .line 466888
    :cond_1
    return v0
.end method

.method public final b(Ljava/lang/Class;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 466868
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 466869
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466870
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 466871
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 466872
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    .line 466873
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    .line 466874
    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 466875
    instance-of v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466876
    iput-object v4, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466877
    :cond_0
    const/4 v2, 0x1

    .line 466878
    :cond_1
    return v2

    .line 466879
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 466880
    iput-object v4, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    .line 466881
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 466866
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/7MJ;

    invoke-direct {v1, p1}, LX/7MJ;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466867
    return-void
.end method

.method public declared-synchronized c(LX/2pa;)V
    .locals 2

    .prologue
    .line 466863
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466864
    monitor-exit p0

    return-void

    .line 466865
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 466851
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 466852
    :cond_0
    :goto_0
    return-void

    .line 466853
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentVolume()I

    move-result v0

    .line 466854
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 466855
    :sswitch_0
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(I)V

    .line 466856
    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->S:Z

    if-eqz v0, :cond_0

    .line 466857
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->S:Z

    .line 466858
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->b()V

    goto :goto_0

    .line 466859
    :sswitch_1
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(I)V

    .line 466860
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->S:Z

    if-nez v0, :cond_0

    .line 466861
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->S:Z

    .line 466862
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->a()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0xa4 -> :sswitch_1
    .end sparse-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 466848
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    .line 466849
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->z()V

    .line 466850
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 466845
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    if-eqz v0, :cond_0

    .line 466846
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->d()V

    .line 466847
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 466838
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466839
    iget-boolean v2, v0, LX/2oy;->b:Z

    move v2, v2

    .line 466840
    if-eqz v2, :cond_0

    .line 466841
    invoke-virtual {v0}, LX/2oy;->s()V

    goto :goto_0

    .line 466842
    :cond_1
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_2

    .line 466843
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->z()V

    .line 466844
    :cond_2
    return-void
.end method

.method public getAdjustedVideoSize()Landroid/graphics/RectF;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466835
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466836
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->getAdjustedVideoSize()Landroid/graphics/RectF;

    move-result-object v0

    .line 466837
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466826
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 466827
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 466828
    :goto_0
    return-object v0

    .line 466829
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466830
    instance-of v2, v0, LX/2pH;

    if-eqz v2, :cond_1

    .line 466831
    check-cast v0, LX/2pH;

    .line 466832
    iget-object v1, v0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v1

    .line 466833
    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 466834
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    goto :goto_0
.end method

.method public getCropRect()Landroid/graphics/RectF;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466822
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466823
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466824
    iget-object p0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->t:Landroid/graphics/RectF;

    move-object v0, p0

    .line 466825
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 466724
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentVolume()I
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 466815
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->x:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 466816
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->x:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 466817
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method

.method public getIsPausedByCommercialBreak()Z
    .locals 1

    .prologue
    .line 466534
    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    return v0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 466553
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->t()I

    move-result v0

    goto :goto_0
.end method

.method public getPlaybackController()LX/2pb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466552
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    return-object v0
.end method

.method public getPlaybackPercentage()F
    .locals 2

    .prologue
    .line 466548
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoDurationMs()I

    move-result v0

    .line 466549
    if-gtz v0, :cond_0

    .line 466550
    const/4 v0, 0x0

    .line 466551
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0
.end method

.method public getPlayerActivityManager()LX/13l;
    .locals 1

    .prologue
    .line 466547
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->d:LX/13l;

    return-object v0
.end method

.method public getPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 466546
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    return-object v0
.end method

.method public getPlayerState()LX/2qV;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466543
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466544
    iget-object p0, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p0

    .line 466545
    goto :goto_0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 466542
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    return-object v0
.end method

.method public getRichVideoPlayerCallbackListener()LX/3It;
    .locals 1

    .prologue
    .line 466541
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    return-object v0
.end method

.method public getRichVideoPlayerParams()LX/2pa;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466540
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    return-object v0
.end method

.method public getVideoDurationMs()I
    .locals 1

    .prologue
    .line 466539
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->k()I

    move-result v0

    goto :goto_0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466536
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_1

    .line 466537
    :cond_0
    const/4 v0, 0x0

    .line 466538
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoPlugin()Lcom/facebook/video/player/plugins/VideoPlugin;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 466535
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    return-object v0
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 466459
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    if-eqz v0, :cond_0

    .line 466460
    invoke-direct {p0}, Lcom/facebook/video/player/RichVideoPlayer;->d()V

    .line 466461
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->P:Z

    if-eqz v0, :cond_1

    .line 466462
    iput-boolean v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->P:Z

    .line 466463
    iput-boolean v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    .line 466464
    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    if-eqz v0, :cond_1

    .line 466465
    sget-object v0, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 466466
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466467
    iget-boolean v2, v0, LX/2oy;->b:Z

    move v2, v2

    .line 466468
    if-eqz v2, :cond_2

    .line 466469
    invoke-virtual {v0}, LX/2oy;->dH_()V

    goto :goto_0

    .line 466470
    :cond_3
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 466477
    const-string v0, "RichVideoPlayer.unload"

    const v1, -0x5dbbdf30

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466478
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    .line 466479
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->L:LX/2pa;

    .line 466480
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466481
    invoke-virtual {v0}, LX/2oy;->im_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 466482
    :catchall_0
    move-exception v0

    const v1, -0x1617dea9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const v0, -0x25c8429b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 466483
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 466484
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 466485
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466486
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466487
    iget-boolean p0, v0, LX/2pb;->B:Z

    move v0, p0

    .line 466488
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 466489
    const-class v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    move-result v0

    .line 466490
    if-eqz v0, :cond_0

    .line 466491
    iget-object v1, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 466492
    :cond_0
    return v0
.end method

.method public final m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466493
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466494
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 466495
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    .line 466496
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    goto :goto_0

    .line 466497
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 466498
    iput-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->Q:LX/2pG;

    .line 466499
    iput-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466500
    return-void
.end method

.method public final n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466501
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466502
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 466503
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 466504
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466505
    instance-of v3, v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-nez v3, :cond_0

    .line 466506
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 466507
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    .line 466508
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    .line 466509
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 466510
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 466511
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_2

    .line 466512
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466513
    :cond_2
    return-object v1
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 466514
    invoke-super {p0, p1}, LX/2oX;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 466515
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/3JD;

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {v1, v2}, LX/3JD;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466516
    return-void
.end method

.method public final onFinishInflate()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x33cdd5b4    # -4.6704944E7f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 466517
    invoke-super {p0}, LX/2oX;->onFinishInflate()V

    .line 466518
    iget-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->z:Z

    if-nez v0, :cond_3

    .line 466519
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 466520
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 466521
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 466522
    instance-of v4, v0, LX/2oy;

    if-eqz v4, :cond_0

    .line 466523
    check-cast v0, LX/2oy;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466524
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 466525
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466526
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 466527
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_1

    .line 466528
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->z:Z

    .line 466529
    :cond_3
    const v0, -0x678909b4

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 466530
    invoke-super/range {p0 .. p5}, LX/2oX;->onLayout(ZIIII)V

    .line 466531
    if-eqz p1, :cond_0

    .line 466532
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/RichVideoPlayer$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/RichVideoPlayer$1;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 466533
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 466471
    const-string v0, "RichVideoPlayer.onMeasure"

    const v1, -0x2fb8722a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 466472
    :try_start_0
    invoke-super {p0, p1, p2}, LX/2oX;->onMeasure(II)V

    .line 466473
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2ql;

    invoke-direct {v1}, LX/2ql;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466474
    const v0, 0x1ea04c3f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 466475
    return-void

    .line 466476
    :catchall_0
    move-exception v0

    const v1, 0x23e9b15f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 466590
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466591
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    sget-object p0, LX/2qV;->ERROR:LX/2qV;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 466592
    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 466634
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 466633
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->o()Z

    move-result v0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 466632
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->p()Z

    move-result v0

    goto :goto_0
.end method

.method public setChannelEligibility(LX/04H;)V
    .locals 1

    .prologue
    .line 466628
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->J:LX/04H;

    .line 466629
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466630
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, p1}, LX/2pb;->a(LX/04H;)V

    .line 466631
    :cond_0
    return-void
.end method

.method public setCropRect(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 466616
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 466617
    if-nez p1, :cond_1

    .line 466618
    :cond_0
    :goto_0
    return-void

    .line 466619
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;->setCropRect(Landroid/graphics/RectF;)V

    .line 466620
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    if-eqz v0, :cond_0

    .line 466621
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 466622
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 466623
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 466624
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466625
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466626
    iget-object v1, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v1, p1}, LX/2q7;->a(Landroid/graphics/RectF;)V

    .line 466627
    goto :goto_0
.end method

.method public setInstreamVideoAdBreakCallbackListener(LX/3Iv;)V
    .locals 0

    .prologue
    .line 466614
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->y:LX/3Iv;

    .line 466615
    return-void
.end method

.method public setIsInstreamVideoAdPlayer(Z)V
    .locals 1

    .prologue
    .line 466607
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466608
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466609
    iput-boolean p1, v0, LX/2pb;->N:Z

    .line 466610
    if-nez p1, :cond_1

    const/4 p0, 0x1

    .line 466611
    :goto_0
    iput-boolean p0, v0, LX/2pb;->O:Z

    .line 466612
    :cond_0
    return-void

    .line 466613
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public setIsPausedByCommercialBreak(Z)V
    .locals 0

    .prologue
    .line 466605
    iput-boolean p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 466606
    return-void
.end method

.method public setNeedCentering(Z)V
    .locals 1

    .prologue
    .line 466601
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466602
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466603
    iput-boolean p1, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->u:Z

    .line 466604
    :cond_0
    return-void
.end method

.method public setOriginalPlayReason(LX/04g;)V
    .locals 1

    .prologue
    .line 466597
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->K:LX/04g;

    .line 466598
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466599
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, p1}, LX/2pb;->c(LX/04g;)V

    .line 466600
    :cond_0
    return-void
.end method

.method public setPlayerOrigin(LX/04D;)V
    .locals 1

    .prologue
    .line 466593
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    .line 466594
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466595
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, p1}, LX/2pb;->a(LX/04D;)V

    .line 466596
    :cond_0
    return-void
.end method

.method public setPlayerType(LX/04G;)V
    .locals 1

    .prologue
    .line 466554
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->I:LX/04G;

    .line 466555
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466556
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, p1}, LX/2pb;->a(LX/04G;)V

    .line 466557
    :cond_0
    return-void
.end method

.method public setPluginEnvironment(LX/7Lf;)V
    .locals 3

    .prologue
    .line 466586
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466587
    instance-of v2, v0, LX/3Gb;

    if-eqz v2, :cond_0

    .line 466588
    check-cast v0, LX/3Gb;

    invoke-virtual {v0, p1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    goto :goto_0

    .line 466589
    :cond_1
    return-void
.end method

.method public setRichVideoPlayerCallbackListener(LX/3It;)V
    .locals 0

    .prologue
    .line 466584
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 466585
    return-void
.end method

.method public setShouldCropToFit(Z)V
    .locals 1

    .prologue
    .line 466580
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v0, :cond_0

    .line 466581
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 466582
    iput-boolean p1, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->d:Z

    .line 466583
    :cond_0
    return-void
.end method

.method public setUseOneVideoPolicy(Z)V
    .locals 0

    .prologue
    .line 466578
    iput-boolean p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->C:Z

    .line 466579
    return-void
.end method

.method public setVideoPluginAlignment(LX/7OE;)V
    .locals 1

    .prologue
    .line 466576
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->l:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;->setVideoPluginAlignment(LX/7OE;)V

    .line 466577
    return-void
.end method

.method public setVolume(F)V
    .locals 1

    .prologue
    .line 466573
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-eqz v0, :cond_0

    .line 466574
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    invoke-virtual {v0, p1}, LX/2pb;->a(F)V

    .line 466575
    :cond_0
    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 466570
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 466571
    iget-object p0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {p0}, LX/2q7;->u()Z

    move-result p0

    move v0, p0

    .line 466572
    goto :goto_0
.end method

.method public final u()V
    .locals 3

    .prologue
    .line 466568
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2os;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/2os;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466569
    return-void
.end method

.method public final v()V
    .locals 3

    .prologue
    .line 466566
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2os;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/2os;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466567
    return-void
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 466564
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2ot;

    invoke-direct {v1}, LX/2ot;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 466565
    return-void
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 466561
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466562
    invoke-virtual {v0}, LX/2oy;->dI_()V

    goto :goto_0

    .line 466563
    :cond_0
    return-void
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 466558
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466559
    invoke-virtual {v0}, LX/2oy;->t()V

    goto :goto_0

    .line 466560
    :cond_0
    return-void
.end method
