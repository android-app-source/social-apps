.class public Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:LX/15W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

.field public c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

.field public d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

.field public e:LX/7DE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 546501
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546502
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546499
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546500
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 546514
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546515
    const/4 p3, 0x0

    .line 546516
    const-class p1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-static {p1, p0}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 546517
    const p1, 0x7f031396

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 546518
    const p1, 0x7f0d2d41

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    .line 546519
    const p1, 0x7f0d2d42

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    .line 546520
    iget-object p1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    invoke-virtual {p1, p3}, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;->setVisibility(I)V

    .line 546521
    new-instance p1, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-direct {p1}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;-><init>()V

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 546522
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3Ib;

    invoke-direct {p2, p0}, LX/3Ib;-><init>(Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546523
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3Ic;

    invoke-direct {p2, p0}, LX/3Ic;-><init>(Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546524
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/3Id;

    invoke-direct {p2, p0}, LX/3Id;-><init>(Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546525
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-static {p0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object p0

    check-cast p0, LX/15W;

    iput-object p0, p1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->a:LX/15W;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 10

    .prologue
    const-wide/16 v6, 0x7d0

    const-wide/16 v4, 0x12c

    .line 546506
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 546507
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    if-eqz v0, :cond_0

    .line 546508
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->c:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    const-wide/16 v2, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a(Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;JJJI)V

    .line 546509
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->d:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const-wide/16 v8, 0x1518

    move-wide v2, v4

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a(Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;JJJJ)V

    .line 546510
    :cond_0
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->e:LX/7DE;

    .line 546511
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    new-instance v1, LX/7Nl;

    invoke-direct {v1, p0}, LX/7Nl;-><init>(Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;)V

    .line 546512
    iput-object v1, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b:LX/7Nl;

    .line 546513
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 546503
    invoke-super {p0}, LX/2oy;->d()V

    .line 546504
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;->b:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a()V

    .line 546505
    return-void
.end method
